<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
</div>

<div class="table-title">초대장 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit content-form">
    <input type="hidden" name="pageNum" value="<?= Request::get()->get('pageNum', 20); ?>"/>
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>분류</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="category" value="" <?php if($req['category']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($category as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="category" value="<?php echo $dKey;?>" <?php if($req['category']==$dKey){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>상태</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="state" value="" <?php if($req['state']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($state as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="state" value="<?php echo $dKey;?>" <?php if($req['state']==(string)$dKey){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'odno' => '주문번호', 'odsno' => '상품주문번호', 'odname' => '주문자명', 'odtel' => '주문자핸드폰'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th>기간검색(행사일)</th>
                <td class="form-inline">
                    <div class="input-group js-datepicker">
                        <input type="text" name="treatDate[start]" value="<?php echo $req['treatDate']['start'];?>" class="form-control width-xs" placeholder="수기입력 가능" autocomplete="off" />
                        <span class="input-group-addon"><span class="btn-icon-calendar"></span></span>
                    </div>
                    ~
                    <div class="input-group js-datepicker">
                        <input type="text" name="treatDate[end]" value="<?php echo $req['treatDate']['end'];?>" class="form-control width-xs" placeholder="수기입력 가능"  autocomplete="off" />
                        <span class="input-group-addon"><span class="btn-icon-calendar"></span></span>
                    </div>
                    <?= gd_search_date(gd_isset($req['searchPeriod'], -1), 'treatDate') ?>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>


<form id="frmList" action="" method="get" target="ifrmProcess">
    <input type="hidden" name="state" value="">

    <div class="table-header form-inline">
        <div class="pull-right">
            <?= gd_select_box_by_page_view_count(Request::get()->get('pageNum', 20)); ?>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th>
                <th>번호</th>
                <th>주문번호</th>
                <th>초대장종류</th>
                <th>초대장스킨</th>
                <th style="min-width: 300px !important;">초대장주소</th>
                <th style="min-width: 200px !important;">비메오</th>
                <th>주문자정보</th>
                <th>행사일</th>
                <th>등록일</th>
                <th>수정일</th>
                <th>관리</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {

                    if ($val['url']!="") {
                        $url = '<a href=https://app.aileenstory.com/'.$val['url'].' target="_blank">https://app.aileenstory.com/'.$val['url'].'</a>';
                    } else {
                        $url = '-';
                    }

                    $category = array('', '청첩장', '돌잔치', '고희/환갑');

                    if ($val['state'] == '1') {
                        $logCheck = "text-myclose";
                    } else {
                        $logCheck = "";
                    }

                    ?>
                    <tr class="<?=$logCheck?>">
                        <!--선택-->
                        <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td>
                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>

                        <td class="center number"><?=$val['odno'];?></td>
                        <td class="center number"><?=$category[$val['category']];?></td>
                        <td class="center number"><?=$val['skin'];?></td>
                        <td class="center number"><?=$url;?></td>
                        <td class="center number"><?=$val['vimeoUri']?$val['vimeoUri']:'-';?></td>

                        <td class="center number"><?=$val['odname'];?>(<?=$val['odtel'];?>)</td>
                        <td class="center number"><?=$val['eventday'];?>(<?=$val['apm']?> <?=$val['hour']?>:<?=$val['minute']?>)</td>

                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d', $val['regDt']); ?></td>
                        <!--수정일-->
                        <td class="center date"><?=gd_date_format('Y-m-d', $val['modDt']); ?></td>

                        <td class="center padlr10">
                            <a href="./card_register.php?no=<?=$val['no']; ?>&mode=update" class="btn btn-white btn-sm">수정</a>
                        </td>

                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnOk">활성화</button>
            <button type="button" class="btn btn-white" id="btnCancel">비활성화</button>
            <button type="button" class="btn btn-white" id="btnDel">초대장삭제</button>
        </div>
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">
    // 클래스명 js-page-number 셀렉트박스의 이벤트
    var $js_page_number = $('select.js-page-number');
    if ($js_page_number.length > 0) {
        $(document).on('change', 'select.js-page-number', function (e) {
            e.preventDefault();
            var $input = $('input[name=\'pageNum\']');
            console.log($input);
            $input.val($js_page_number.find(':selected').val());
            $('.content-form[method="get"]:eq(0)').submit();
        });
    }

    $(document).ready(function () {
        $('#btnOk').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 초대장이 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개의 초대장 상태를 활성화 하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'state\']').val('active');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './log_ps.php');
                    $('#frmList').submit();
                }
            });

        });

        $('#btnCancel').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 초대장이 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개의 초대장 상태를 비활성화 하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'state\']').val('disabled');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './log_ps.php');
                    $('#frmList').submit();
                }
            });

        });

        $('#btnDel').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 초대장이 없습니다.');
                return;
            }

            dialog_confirm('초대장을 삭제하면 초대장이 비활성화 되며 초대장주소 및 저장된 파일이 삭제됩니다<br>선택한 ' + chkCnt + '개의 초대장을 삭제 하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'state\']').val('del');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './log_ps.php');
                    $('#frmList').submit();
                }
            });

        });

    });
</script>