<form id="frmMobilecard" name="frmMobilecard" action="card_ps.php" method="post" class="content_form" enctype="multipart/form-data" target="ifrmProcess">
    <input type="hidden" name="type" value="log">
    <input type="hidden" name="mode" value="<?=$getData['mode']?>">
    <input type="hidden" name="no" value="<?=$getLog['no']?>">

    <div class="page-header js-affix">
        <h3><?php echo end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="goList('./card_list.php?page=1');">
            <input type="submit" value="저장" class="btn btn-red">
        </div>
    </div>

    <div class="table-title gd-help-manual">
        <div class="flo-left">로그상세정보</div>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>주문번호</th>
                <td><?=$getLog['odno']?></td>
            </tr>
            <tr>
                <th>초대장종류</th>
                <td>
                    <?=$getLog['category']==1?"청첩장":"";?>
                    <?=$getLog['category']==2?"돌잔치":"";?>
                    <?=$getLog['category']==3?"고희/환갑":"";?>
                </td>
            </tr>
            <tr>
                <th>초대장스킨</th>
                <td>
                    <?=$getLog['skin']?>
                </td>
            </tr>
            <tr>
                <th>초대장주소</th>
                <td>
                    <?=$getLog['url']?>
                </td>
            </tr>
            <tr>
                <th>주문자정보</th>
                <td><?=$getLog['odname']?>(<?=$getLog['odtel']?>)</td>
            </tr>
            <tr>
                <th>행사일</th>
                <td><?=$getLog['eventday']?>(<?=$getLog['apm']?> <?=$getLog['hour']?>:<?=$getLog['minute']?>)</td>
            </tr>
            <tr>
                <th>등록일(기간만료체크일)</th>
                <td>
                    <input type="text" name="regDt" class="form-control" value="<?=$getLog['regDt']?>">
                </td>
            </tr>
            <tr>
                <th>초대장상태</th>
                <td>
                    <input type="radio" name="state" id="state1" value="0" <?=$getLog['state']==0?"checked":"";?>><label for="state1">활성</label>
                    <input type="radio" name="state" id="state2" value="1" <?=$getLog['state']==1?"checked":"";?>><label for="state2">비활성</label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>