<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
    <div class="btn-group">
        <a href="card_skin_register.php" class="btn btn-red-line">스킨추가</a>
    </div>
</div>

<div class="table-title">스킨 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit">
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>분류</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="category" value="" <?php if($req['category']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($category as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="category" value="<?php echo $dKey;?>" <?php if($req['category']==$dKey){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'name' => '스킨명', 'code' => '스킨코드'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>



<form id="frmList" action="" method="get" target="ifrmProcess">

    <input type="hidden" name="type" value="skin">
    <input type="hidden" name="mode" value="">
    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th>
                <th>번호</th>
                <th>분류</th>
                <th>스킨명</th>
                <th>스킨코드</th>
                <th>썸네일</th>
                <th>등록일</th>
                <th>수정일</th>
                <th>관리</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {
                    $category = array('', '청첩장', '돌잔치', '고희/환갑');
                    ?>
                    <tr>
                        <!--선택-->
                        <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td>

                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>

                        <td class="center number"><?=$category[$val['category']];?></td>
                        <td class="center number"><?=$val['name'];?></td>
                        <td class="center number"><?=$val['code'];?></td>
                        <td class="center number"><img src="/<?=$val['thumb'];?>" style="width:100px;"></td>

                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d', $val['regDt']); ?></td>

                        <!--수정일-->
                        <td class="center date"><?=gd_date_format('Y-m-d', $val['modDt']); ?></td>

                        <td class="center padlr10">
                            <a href="./card_skin_register.php?no=<?=$val['no']; ?>&mode=update" class="btn btn-white btn-sm">수정</a>
                        </td>

                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnDelete">선택 삭제</button>
        </div>
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#btnDelete').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 스킨이 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개 스킨을  정말로 삭제하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'mode\']').val('delete');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './card_ps.php');
                    $('#frmList').submit();
                }
            });

        });
    });
</script>