<form id="frmMobilecard" name="frmMobilecard" action="card_ps.php" method="post" class="content_form" enctype="multipart/form-data" target="ifrmProcess">
    <input type="hidden" name="type" value="skin">
    <input type="hidden" name="mode" value="<?=$getData['mode']?>">
    <input type="hidden" name="no" value="<?=$getData['no']?>">

    <div class="page-header js-affix">
        <h3><?php echo end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="goList('./card_skin_list.php?page=1');">
            <input type="submit" value="저장" class="btn btn-red">
        </div>
    </div>

    <div class="table-title gd-help-manual">
        <div class="flo-left">상품정보</div>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>상품구분</th>
                <td>
                    <input type="radio" name="category" id="cardType1" value="1" <?=$getSkin['category']==1?"checked":"";?>><label for="cardType1">청첩장</label>
                    <input type="radio" name="category" id="cardType2" value="2" <?=$getSkin['category']==2?"checked":"";?>><label for="cardType2">돌잔치</label>
                    <input type="radio" name="category" id="cardType3" value="3" <?=$getSkin['category']==3?"checked":"";?>><label for="cardType3">고희/환갑</label>
                </td>
            </tr>

            <tr>
                <th>스킨명</th>
                <td>
                    <input type="text" name="name" class="form-control input-sm" value="<?=$getSkin['name']?>">
                </td>
            </tr>

            <tr>
                <th>스킨코드(영문,숫자만)</th>
                <td>
                    <input type="text" name="code" class="form-control input-sm" value="<?=$getSkin['code']?>">
                </td>
            </tr>

            <tr>
                <th>메인 틀 사용여부</th>
                <td>
                    <input type="checkbox" name="main_img_background_use" id="main_img_background_use" value="1" <?=$getSkin['main_img_background_use']==1?"checked":"";?> onclick="main_img_use_change(this)"><label for="main_img_background_use">사용</label>
                </td>
            </tr>

            <tr class="main_img_class">
                <th>메인 틀 좌표 정보</th>
                <td>
                    <label for="width">width</label>
                    <input type="text" class="form-control width-sm" placeholder="width" name="main_img_info[width]" id="width" value="<?=$getSkin['main_img_info']['width'];?>">
                    <label for="">left</label>
                    <input type="text" class="form-control width-sm" placeholder="left" name="main_img_info[left]" id="left" value="<?=$getSkin['main_img_info']['left'];?>">
                    <label for="">top</label>
                    <input type="text" class="form-control width-sm" placeholder="top" name="main_img_info[top]" id="top" value="<?=$getSkin['main_img_info']['top'];?>">
                    <label for="">padding-bottom</label>
                    <input type="text" class="form-control width-sm" placeholder="padding-bottom" name="main_img_info[padding-bottom]" id="padding-bottom" value="<?=$getSkin['main_img_info']['padding-bottom'];?>">
                </td>
            </tr>

            <tr class="main_img_class">
                <th>메인 틀 이미지</th>
                <td>
                    <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                        <?php if($getSkin['main_background_img']){?>
                            <img src="/<?=$getSkin['main_background_img']?>" style="width:150px;">
                        <?php }?>
                        <div style="display:inline-block;vertical-align:bottom;">
                            <input type="hidden" name="main_background_img" value="">
                            <input type="file" name="main_background_img" class="form-control input-sm">
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <th>썸네일</th>
                <td>
                    <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                        <?php if($getSkin['thumb']){?>
                            <img src="/<?=$getSkin['thumb']?>" style="width:150px;">
                        <?php }?>
                        <div style="display:inline-block;vertical-align:bottom;">
                            <input type="hidden" name="thumb_img" value="">
                            <input type="file" name="thumb_img" class="form-control input-sm">
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <th>미리보기</th>
                <td>
                    <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                        <?php if($getSkin['preview']){?>
                            <img src="/<?=$getSkin['preview']?>" style="width:150px;">
                        <?php }?>
                        <div style="display:inline-block;vertical-align:bottom;">
                            <input type="hidden" name="preview_img" value="">
                            <input type="file" name="preview_img" class="form-control input-sm">
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


<!---->
<!--    <div class="table-title gd-help-manual">-->
<!--        <div class="flo-left" style="margin-right:10px;">기본정보</div>-->
<!--        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_basic2();">항목추가</button>-->
<!--    </div>-->
<!---->
<!--    <div id="layoutOrderViewOrderInfoModify" class="">-->
<!--        <table class="table table-cols">-->
<!--            <colgroup>-->
<!--                <col class="width-md">-->
<!--                <col>-->
<!--            </colgroup>-->
<!--            <tbody id="basic">-->
<!---->
<!--            --><?php //foreach($getSkin['basic_info'] as $v){?>
<!--                <tr>-->
<!--                    <th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>-->
<!--                    <td><input type="text" name="basic_text[]" class="form-control width-3xl" placeholder="-를 입력하면 위아래 구분 간격이 추가됩니다." value="--><?//=$v?><!--"></td>-->
<!--                </tr>-->
<!--            --><?php //}?>
<!---->
<!--            </tbody>-->
<!--        </table>-->
<!--    </div>-->
<!---->
<!---->
<!---->
<!--    <div class="table-title gd-help-manual">-->
<!--        <div class="flo-left" style="margin-right:10px;">관계정보</div>-->
<!--        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_relation();">항목추가</button>-->
<!--    </div>-->
<!---->
<!--    <div id="layoutOrderViewOrderInfoModify" class="">-->
<!--        <table class="table table-cols">-->
<!--            <colgroup>-->
<!--                <col class="width-md">-->
<!--                <col>-->
<!--            </colgroup>-->
<!--            <tbody id="relation">-->
<!--            --><?php //foreach($getSkin['relation_info'] as $v){?>
<!--                <tr>-->
<!--                    <th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>-->
<!--                    <td><input type="text" name="relation_text[]" class="form-control width-3xl" placeholder="-를 입력하면 위아래 구분 간격이 추가됩니다." value="--><?//=$v?><!--"></td>-->
<!--                </tr>-->
<!--            --><?php //}?>
<!--            </tbody>-->
<!--        </table>-->
<!--    </div>-->
