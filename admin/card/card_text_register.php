<form id="frmMobilecard" name="frmMobilecard" action="card_ps.php" method="post" class="content_form" enctype="multipart/form-data" target="ifrmProcess">
    <input type="hidden" name="type" value="text">
    <input type="hidden" name="mode" value="<?=$getData['mode']?>">
    <input type="hidden" name="no" value="<?=$getData['no']?>">

    <div class="page-header js-affix">
        <h3><?php echo end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="goList('./card_text_list.php?page=1');">
            <input type="submit" value="저장" class="btn btn-red">
        </div>
    </div>

    <div class="table-title gd-help-manual">
        <div class="flo-left">예문정보</div>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>상품구분</th>
                <td>
                    <input type="radio" name="category" id="cardType1" value="1" <?=$getText['category']==1?"checked":"";?>><label for="cardType1">청첩장</label>
                    <input type="radio" name="category" id="cardType2" value="2" <?=$getText['category']==2?"checked":"";?>><label for="cardType2">돌잔치</label>
                    <input type="radio" name="category" id="cardType3" value="3" <?=$getText['category']==3?"checked":"";?>><label for="cardType3">고희/환갑</label>
                </td>
            </tr>
            <tr>
                <th>예문명</th>
                <td>
                    <input type="text" name="name" class="form-control" value="<?=$getText['name']?>">
                </td>
            </tr>
            <tr>
                <th>예문내용</th>
                <td>
                    <div style="margin:5px 0;">
                        <textarea style="height:400px;" name="body" class="form-control" placeholder="예시글을 입력해 주세요."><?=$getText['body']?></textarea>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>