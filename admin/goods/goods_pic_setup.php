<form id="frmGoods" name="frmGoods" action="./goods_pic_setup_upsert.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="mode" value="<?=$mode?>"/>
    <input type="hidden" name="goodsNo" value="<?=$goodsNo?>"/>
    <?php if(empty($defaultCount)) $defaultCount = 2; ?>
    <input type="hidden" name="defaultCount" value="<?=$defaultCount?>"/>
    <input type="hidden" name="extraCount" value="<?=$extraCount?>"/>
    <div class="page-header js-affix">
        <h3><?=end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="window.location.href='./goods_list.php?page=<?=$page?>';" />
            <input type="submit" value="저장" class="btn btn-red"/>
        </div>
    </div><!-- .page-header -->
    <div class="table-title">
        기본 정보
        <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="defaultSetup"><span>닫힘</span></button></span>
    </div>
    <div id="depth-toggle-line-defaultSetup" class="depth-toggle-line display-none"></div>
    <div id="depth-toggle-layer-defaultSetup" >
        <table class="table table-cols">
            <colgroup>
                <col class="width-lg">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <th>기본 항목</th>
                    <td>
                        <table class="table table-rows" id="DefaultAddTable">
                            <thead>
                                <tr>
                                    <th class="width-2xs">순서</th>
                                    <th class="width-2xs">타입</th>
                                    <th>입력값</th>
                                    <th class="width-2xs"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(empty($default_data))
                                {
                                ?>
                                    <tr>
                                        <td class="no center">1</td>
                                        <td>
                                            <select class="form-control" name="defaultType[1]">
                                                <option value="single" selected="selected">한줄 입력</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="defaultInput[1]" value="주문자 성함" readonly="readonly" class="form-control"/>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="no center">2</td>
                                        <td>
                                            <select class="form-control" name="defaultType[2]">
                                                <option value="single" selected="selected">한줄 입력</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="defaultInput[2]" value="연락처" readonly="readonly" class="form-control"/>
                                        </td>
                                        <td></td>
                                    </tr>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php
                                    for($i = 1; $i <= $defaultCount; $i++)
                                    {
                                        $default = unserialize($default_data[$i - 1]['value']);
                                    ?>
                                        <tr>
                                            <td class="no center"><?=$i?></td>
                                            <td>
                                                <select class="form-control" name="defaultType[<?=$i?>]">
                                                    <?php
                                                    if($i > 2)
                                                    {
                                                    ?>
                                                        <option value="" <?php if(empty($default['type'])) { ?>selected="selected"<?php } ?>>=타입=</option>
                                                    <?php
                                                    }
                                                    ?>
                                                    <option value="single" <?php if($default['type'] == 'single') { ?>selected="selected"<?php } ?>>한줄 입력</option>
                                                    <?php
                                                    if($i > 2)
                                                    {
                                                    ?>
                                                        <option value="multi" <?php if($default['type'] == 'multi') { ?>selected="selected"<?php } ?>>여러줄 입력</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <?php
                                                if($i > 2)
                                                {
                                                    $readonly = '';
                                                }
                                                else
                                                {
                                                    $readonly = '';
                                                }
                                                ?>
                                                <input type="text" name="defaultInput[<?=$i?>]" value="<?=$default['input']?>" <?=$readonly?> class="form-control"/>
                                            </td>
                                            <td class="center">
                                                <?php
                                                if($i > 2)
                                                {
                                                ?>
                                                    <button type="button" class="delete-btn btn btn-sm btn-white">삭제</button>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="center"><button type="button" class="btn btn-sm btn-black" id="DefaultAddButton">추가</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-title">
        추가 정보
        <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="uploadSetup"><span>닫힘</span></button></span>
    </div>
    <div id="depth-toggle-line-uploadSetup" class="depth-toggle-line display-none"></div>
    <div id="depth-toggle-layer-uploadSetup" >
        <table class="table table-cols">
            <colgroup>
                <col class="width-lg">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <th>추가 항목</th>
                    <td>
                        <table class="table table-rows" id="ExtraAddTable">
                            <thead>
                                <tr>
                                    <th class="width-2xs">순서</th>
                                    <th class="width-2xs">타입</th>
                                    <th class="width-lg">이미지</th>
                                    <th class="width-lg">제목</th>
                                    <th>이미지 설명</th>
                                    <th>글귀</th>
                                    <th class="width-xs">업로드수</th>
                                    <th class="width-xs">글자수</th>
                                    <th class="width-2xs"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for($i = 1; $i <= $extraCount; $i++)
                                {
                                    $extra = unserialize($extra_data[$i - 1]['value']);
                                ?>
                                <tr>
                                    <td class="center"><?=$i?></td>
                                    <td>
                                        <select class="form-control" name="extraType[<?=$i?>]">
                                            <option value="" <?php if(empty($extra['type'])) { ?>selected="selected"<?php } ?>>=타입=</option>
                                            <option value="single" <?php if($extra['type'] == 'single') { ?>selected="selected"<?php } ?>>한줄 입력</option>
                                            <option value="multi" <?php if($extra['type'] == 'multi') { ?>selected="selected"<?php } ?>>여러줄 입력</option>
                                            <option value="picture1" <?php if($extra['type'] == 'picture1') { ?>selected="selected"<?php } ?>>단일사진(한줄)</option>
                                            <option value="picture2" <?php if($extra['type'] == 'picture2') { ?>selected="selected"<?php } ?>>단일사진(여러줄)</option>
                                            <option value="picture3" <?php if($extra['type'] == 'picture3') { ?>selected="selected"<?php } ?>>다중사진(여러줄)</option>
                                            <option value="video" <?php if($extra['type'] == 'video') { ?>selected="selected"<?php } ?>>영상(여러줄)</option>
                                            <option value="file" <?php if($extra['type'] == 'file') { ?>selected="selected"<?php } ?>>파일(여러줄)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="file" name="extraImage[<?=$i?>]" class="form-control width80p"/>
                                        <?php
                                        if(!empty($extra['image']))
                                        {
                                        ?>
                                        <div style="margin-top:5px;">
                                            <input type="hidden" name="extraImagePath[<?=$i?>]" value="<?=$extra['image']?>" />
                                            <img src="<?=$url?><?=$extra['image']?>" alt="" style="width:70px;" />
                                            <label for="ExtraImageDelete<?=$i?>" style="display:inline-block; margin-left:10px;">
                                                <input type="checkbox" name="extraImageDelete[<?=$i?>]" value="y" id="ExtraImageDelete<?=$i?>" />삭제
                                            </label>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td><input type="text" name="extraTitle[<?=$i?>]" value="<?=$extra['title']?>" class="form-control"/></td>
                                    <td><textarea name="extraImageText[<?=$i?>]" class="form-control"><?=$extra['image_text']?></textarea></td>
                                    <td><textarea name="extraText[<?=$i?>]" class="form-control"><?=$extra['text']?></textarea></td>
                                    <td><input type="number" name="extraUploadCount[<?=$i?>]" value="<?=$extra['upload_count']?>" class="form-control"/></td>
                                    <td><input type="number" name="extraTextCount[<?=$i?>]" value="<?=$extra['text_count']?>" class="form-control"/></td>
                                    <td class="center"><button type="button" class="delete-btn btn btn-sm btn-white">삭제</button></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="center"><button type="button" class="btn btn-sm btn-black" id="ExtraAddButton">추가</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</form>
<script type="text/template" id="DefaultTemplate">
<tr>
    <td class="no center">-</td>
    <td>
        <select name="{default_type}" class="form-control">
            <option value="">=타입=</option>
            <option value="single">한줄 입력</option>
            <option value="multi">여러줄 입력</option>
        </select>
    </td>
    <td>
        <input type="text" name="{default_input}" class="form-control"/>
    </td>
    <td class="center"><button type="button" class="delete-btn btn btn-sm btn-white">삭제</button></td>
</tr>
</script>
<script type="text/template" id="ExtraTemplate">
<tr>
    <td class="no center">-</td>
    <td>
        <select name="{extra_type}" class="form-control">
            <option value="">=타입=</option>
            <option value="single">한줄 입력</option>
            <option value="multi">여러줄 입력</option>
            <option value="picture1">단일사진(한줄)</option>
            <option value="picture2">단일사진(여러줄)</option>
            <option value="picture3">다중사진(여러줄)</option>
            <option value="video">영상(여러줄)</option>
            <option value="file">파일(여러줄)</option>
        </select>
    </td>
    <td><input type="file" name="{extra_image}" class="form-control width80p"/></td>
    <td><input type="text" name="{extra_title}" class="form-control"/></td>
    <td><input type="text" name="{extra_image_text}" class="form-control"/></td>
    <td><input type="text" name="{extra_text}" class="form-control"/></td>
    <td><input type="number" name="{extra_upload_count}" class="form-control"/></td>
    <td><input type="number" name="{extra_text_count}" class="form-control"/></td>
    <td class="center"><button type="button" class="delete-btn btn btn-sm btn-white">삭제</button></td>
</tr>
</script>

<script>
jQuery(function($) {
    $(document).ready(function() {
        var $formSetup = $('#frmGoods');
        var validation = {
            debug: false,
            dialog: false,
            rules: {
                goodsNo: 'required'
            },
            submitHandler: function(form) {
                form.target = 'ifrmProcess';
                form.submit();
            }
        }
        
        $formSetup.validate(validation);
    });

    // #기본 항목 추가
    $(document).on('click', '#DefaultAddButton', function() {
        var no = parseInt($('input[name=defaultCount]').val()) + 1;
        var $default_template = $('#DefaultTemplate').html();
        $default_template = $default_template.replace('{default_type}', 'defaultType[' + no + ']');
        $default_template = $default_template.replace('{default_input}', 'defaultInput[' + no + ']');
        $('input[name=defaultCount]').val(no);
        $('#DefaultAddTable > tbody').append($default_template);
    });

    // #기본 항목 삭제
    $(document).on('click', '#DefaultAddTable .delete-btn', function() {
        var no = parseInt($('input[name=defaultCount]').val()) - 1;
        $('input[name=defaultCount]').val(no);
        $(this).closest('tr').remove();
    });

     // #추가 항목 추가
     $(document).on('click', '#ExtraAddButton', function() {
        var no = parseInt($('input[name=extraCount]').val()) + 1;
        var $extra_template = $('#ExtraTemplate').html();
        $extra_template = $extra_template.replace('{extra_type}', 'extraType[' + no + ']');
        $extra_template = $extra_template.replace('{extra_image}', 'extraImage[' + no + ']');
        $extra_template = $extra_template.replace('{extra_title}', 'extraTitle[' + no + ']');
        $extra_template = $extra_template.replace('{extra_image_text}', 'extraImageText[' + no + ']');
        $extra_template = $extra_template.replace('{extra_text}', 'extraText[' + no + ']');
        $extra_template = $extra_template.replace('{extra_upload_count}', 'extraUploadCount[' + no + ']');
        $extra_template = $extra_template.replace('{extra_text_count}', 'extraTextCount[' + no + ']');
        $('input[name=extraCount]').val(no);
        $('#ExtraAddTable > tbody').append($extra_template);
    });

    // #추가 항목 삭제
    $(document).on('click', '#ExtraAddTable .delete-btn', function() {
        var no = parseInt($('input[name=extraCount]').val()) - 1;
        var image_path = $(this).closest('tr').find('input[name="extraImagePath[]"]').val();
        var image_path_html = '<input type="hidden" name="deleteImagePath[]" value="' + image_path + '"/>';
        $('input[name=extraCount]').val(no);
        $('#frmGoods').append(image_path_html);
        $(this).closest('tr').remove();
    });
});
</script>