<div class="page-header js-affix">
    <h3>
        <?php echo end($naviMenu->location); ?>
    </h3>
</div>
<!--// #검색 -->
<form id="frmSearchOrder" method="get" class="js-form-enter-submit">
    <div class="table-title">
        업로드 검색
    </div>
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm">
                <col>
                <col class="width-sm">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="form-inline">
                            <?=gd_select_box('uploadStatus', 'uploadStatus', [''=>'상태선택','1'=>'준비중','2'=>'업로드중','3'=>'업로드완료','4'=>'제작중','5'=>'제작완료','6'=>'수정요청중','7'=>'수정요청','8'=>'수정중','9'=>'수정완료','10'=>'시안확정'], null, $search['uploadStatus']);?>
                            <?=gd_select_box('target', 'target', ['orderName'=>'주문자','orderNo'=>'주문번호','goodsNm'=>'주문상품'], null, $search['target']);?>
                            <input type="text" name="keyword" value="<?=$search['keyword'];?>" class="form-control width300"/>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div><!-- .search-detail-box -->
    <div class="table-btn">
        <input type="submit" value="검색" class="btn btn-lg btn-black">
        <a href="./goods_pic_list.php" class="btn btn-lg btn-white">취소</a>
    </div>
    <div class="table-header <?=$tableHeaderClass?>">
        <div class="pull-left">
            검색 <strong class="text-danger"><?=number_format(gd_isset($page->recode['total'], 0));?></strong>개 /
            전체 <strong class="text-danger"><?=number_format(gd_isset($page->recode['amount'], 0));?></strong>개
        </div>
        <div class="pull-right">
            <div class="form-inline">
                <?=gd_select_box('pageNum', 'pageNum', gd_array_change_key_value([10,20,30,40,50,60,70,80,90,100,200,300,500,]), '개 보기', $page->page['list']);?>
            </div>
        </div>
    </div>
</form>
<div class="table-responsive">
    <table class="table table-rows order-list">
        <colgroup>
            <col width="78" />
            <col width="178" />
            <col width="160" />
            <col width="168" />
            <col />
            <col width="110" />
            <col width="100" />
        </colgroup>
        <thead>
            <tr>
                <th>번호</th>
                <th>업로드일시</th>
                <th>주문번호</th>
                <th>주문자</th>
                <th>주문상품</th>
                <th>업로드상태</th>
                <th>확인</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(!empty($list['data']))
            {
            ?>
                <?php
                foreach($list['data'] as $no => $data)
                {
                ?>
                <tr class="text-center">
                    <td class="font-num"><small><?=$page->idx--;?></small></td>
                    <td><?=str_replace(' ', '<br>', gd_date_format('Y-m-d H:i:s', $data['update']));?></td>
                    <td class="order-no"><a href="./order_view.php?orderNo=<?=$data['orderNo']?>" target="_blank" class="font-num"><?=$data['orderNo']?></a></td>
                    <td><?=$data['orderName']?></td>
                    <td class="text-left"><?=$data['goodsNm']?></td>
                    <td>
                        <?php
                        if($data['status'] == 1) echo '준비중';
                        elseif($data['status'] == 2) echo '업로드중';
                        elseif($data['status'] == 3) echo '업로드완료';
                        elseif($data['status'] == 4) echo '제작중';
                        elseif($data['status'] == 5) echo '제작완료';
                        elseif($data['status'] == 6) echo '수정요청중';
                        elseif($data['status'] == 7) echo '수정요청';
                        elseif($data['status'] == 8) echo '수정중';
                        elseif($data['status'] == 9) echo '수정완료';
                        elseif($data['status'] == 10) echo '시안확정';
                        ?>
                    </td>
                    <td><a href="./goods_pic_view.php?goodsPicNo=<?=$data['goodsPicNo']?>&status=<?=$data['status']?>" class="btn btn-sm btn-white">자세히보기</a></td>
                </tr>
                <?php    
                }
                ?>
            <?php
            }
            else
            {
            ?>
            <tr>
                <td colspan="7" class="no-data">
                    검색된 업로드 내역이 없습니다.
                </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div><!-- .table-responsive -->
<div class="text-center"><?=$page->getPage();?></div>