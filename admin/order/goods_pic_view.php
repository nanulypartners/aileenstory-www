<?php
    $url = 'http://leveltest1.cafe24.com/files';
?>

<style type="text/css">
#extraData .inline-area > li {display:inline-block; margin-right:10px; width:200px;}
#extraData .inline-area > li a {display:block; padding:0 20px; width:200px; height:113px; line-height:113px; text-align:center; color:#fff; word-break:break-all; box-sizing:border-box; background-repeat:no-repeat; background-position:center center; background-size:contain; background-color:#666; overflow:hidden;}
</style>

<div class="page-header js-affix">
    <h3><?=end($naviMenu->location);?></h3>
    <div class="btn-group">
        <a href="#" onclick="history.back(); return false;" class="btn btn-white">이전</a>
        <a href="#" onclick="return false;" class="delete-btn btn btn-black">파일삭제</a>
        <a href="#" onclick="return false;" class="download-btn btn btn-red">다운로드</a>
    </div>
</div>
<a target="_blank" id="DownloadLink" style="display:none;"></a>
<div class="table-title">
    제작 정보
    <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="MakingLayer"><span>닫힘</span></button></span>
</div>
<div id="MakingLayerLine" class="depth-toggle-line display-none"></div>
<div id="MakingLayer">
    <form id="MakingForm" name="making_form" action="./goods_pic_make_upsert.php" method="post">
        <input type="hidden" name="goodsPicNo" value="<?=$picInfo['goodsPicNo']?>" />
        <input type="hidden" name="picType" value="<?=$picInfo['picType']?>" />
        <table class="table table-cols">
            <colgroup>
                <col class="width-lg">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <th>제작 영상 (플레이)</th>
                    <td><input type="text" name="playVideo" value="<?=$makeInfo['playVideo']?>" class="form-control" style="width:100%;" /></td>
                </tr>
                <tr>
                    <th>제작 영상 (다운로드)</th>
                    <td><input type="text" name="downVideo" value="<?=$makeInfo['downVideo']?>" class="form-control" style="width:100%;" /></td>
                </tr>
                <tr>
                    <th>상태 변경</th>
                    <td>
                        <?=gd_select_box('status', 'status', [''=>'상태선택','1'=>'준비중','2'=>'업로드중','3'=>'업로드완료','4'=>'제작중','5'=>'제작완료','6'=>'수정요청중','7'=>'수정요청','8'=>'수정중','9'=>'수정완료','10'=>'시안확정'], null, $picInfo['status']);?>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="table-btn" style="margin:20px 0 0;">
            <input type="submit" value="저장" class="btn btn-lg btn-red"/>
        </div>
    </form>
</div>
<div class="table-title">
    주문 정보
    <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="OrderLayer"><span>닫힘</span></button></span>
</div>
<div id="OrderLayerLine" class="depth-toggle-line display-none"></div>
<div id="OrderLayer">
    <table class="table table-cols">
        <colgroup>
            <col class="width-lg">
            <col>
        </colgroup>
        <tbody>
            <tr>
                <th>업로드 위치</th>
                <td><?=$picInfo['picType']?></td>
            </tr>
            <?php
            if($picInfo['picType'] == 'aileen')
            {
            ?>
            <tr>
                <th>주문번호</th>
                <td><?=$picInfo['orderNo']?></td>
            </tr>
            <?php
            }
            else
            {
            ?>
            <tr>
                <th>주문자 성명</th>
                <td><?=$picInfo['orderName']?></td>
            </tr>
            <tr>
                <th>주문자 연락처</th>
                <td><?=$picInfo['orderPhone']?></td>
            </tr>
            <?php
            }
            ?>
            <tr>
                <th>상품번호</th>
                <td><?=$picInfo['goodsNo']?></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-title" id="ModifyTitle">
    수정 요청
    <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="ModifyLayer"><span>닫힘</span></button></span>
</div>
<div id="ModifyLayerLine" class="depth-toggle-line display-none"></div>
<div id="ModifyLayer">
    <table class="table table-cols" id="extraData">
        <colgroup>
            <col class="width-lg">
            <col>
        </colgroup>
        <tbody>
            <?php
            foreach($modifyCont as $key => $value)
            {
            ?>
            <tr>
                <th>수정 [<?=$key + 1?>]</th>
                <td>
                    <table class="table table-cols" style="margin-bottom:0;">
                        <colgroup>
                            <col class="width-sm">
                            <col>
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>수정 내용</th>
                                <td><?=$value['cont']?></td>
                            </tr>
                            <tr>
                                <th>이미지</th>
                                <td>
                                    <ul class="inline-area">
                                        <?php
                                        foreach($modifyFile as $key1 => $value1)
                                        {
                                            if($value1['order'] == $value['order'])
                                            {
                                        ?>
                                            <li>
                                                <?php
                                                // *파일 타입
                                                $file_ext = substr($value1['path'], strrpos($value1['path'], '.') + 1);
                                                if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                                                {
                                                    $file_type = 'image';
                                                }
                                                elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                                                {
                                                    $file_type = 'video';
                                                }
                                                else
                                                {
                                                    $file_type = 'etc';
                                                }
                                                $value1['path'] = str_replace('../', '/', $value1['path']);
                                                if($file_type == 'video' || $file_type == 'etc')
                                                {
                                                    echo '<a href="' . $url . $value1['path'] . '" target="_blank" class="file">' . $value1['name'] . '</a>';
                                                }
                                                else
                                                {
                                                    echo '<a href="' . $url . $value1['path'] . '" target="_blank" class="image" style="background-image:url(' . $url . $value1['path'] . ');"></a>';
                                                }
                                                ?>
                                            </li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
<div class="table-title">
    기본 정보
    <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="DefaultLayer"><span>닫힘</span></button></span>
</div>
<div id="DefaultLayerLine" class="depth-toggle-line display-none"></div>
<div id="DefaultLayer">
    <table class="table table-cols" id="defaultData">
        <colgroup>
            <col class="width-lg">
            <col>
        </colgroup>
        <tbody>
            <?php
            foreach($defaultData as $key => $value)
            {
            ?>
            <tr>
                <th><?=$value['input']?></th>
                <td><?=$value['cont']?></td>
            </tr>
            <?php
            }
            ?>
            <tr>
                <th>기타 파일</th>
                <td>
                    <ul class="inline-area">
                        <?php
                        foreach($defaultFile as $key => $value)
                        {
                        ?>
                        <li>
                            <?php
                            if($value['type'] == 'video' || $value['type'] == 'etc')
                            {
                                echo '<a href="' . $url . $value['path'] . '" target="_blank" class="file">' . $value['name'] . '</a>';
                            }
                            else
                            {
                                echo '<a href="' . $url . $value['path'] . '" target="_blank" class="image" style="background-image:url(' . $url . $value['path'] . ');"></a>';
                            }
                            ?>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-title">
    추가 정보
    <span class="depth-toggle"><button type="button" class="btn btn-sm btn-link bold depth-toggle-button" depth-name="AddLayer"><span>닫힘</span></button></span>
</div>
<div id="AddLayerLine" class="depth-toggle-line display-none"></div>
<div id="AddLayer">
    <table class="table table-cols" id="extraData">
        <colgroup>
            <col class="width-lg">
            <col>
        </colgroup>
        <tbody>
            <?php
            foreach($extraData as $key => $value)
            {
            ?>
            <tr>
                <th>[<?=$value['order']?>] <?=$value['title']?></th>
                <td>
                    <table class="table table-cols" style="margin-bottom:0;">
                        <colgroup>
                            <col class="width-sm">
                            <col>
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>글귀</th>
                                <td><?=$value['cont']?></td>
                            </tr>
                            <?php
                            if($value['type'] == 'picture1' || $value['type'] == 'picture2' || $value['type'] == 'picture3' || $value['type'] == 'video' || $value['type'] == 'file')
                            {
                            ?>
                            <tr>
                                <th>이미지</th>
                                <td>
                                    <ul class="inline-area">
                                        <?php
                                        foreach($value['files'] as $key1 => $value1)
                                        {
                                        ?>
                                        <li>
                                            <?php
                                            if($value1['type'] == 'video' || $value1['type'] == 'etc')
                                            {
                                                echo '<a href="' . $url . $value1['path'] . '" target="_blank" class="file">' . $value1['name'] . '</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="' . $url . $value1['path'] . '" target="_blank" class="image" style="background-image:url(' . $url . $value1['path'] . ');"></a>';
                                            }
                                            ?>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script>
jQuery(function($) {
    $(document).ready(function() {
        var $MakingForm = $('#MakingForm');
        var validation = {
            debug: false,
            dialog: false,
            submitHandler: function(form) {
                form.target = 'ifrmProcess';
                form.submit();
            }
        }
        
        $MakingForm.validate(validation);

        // *상태에 따라 닫기, 펼침
        var status = "<?=$picInfo['status']?>";
        if(status == '1' || status == '2' || status == '3' || status == '4' || status == '5') {
            $('#ModifyTitle').hide();
            $('#ModifyLayer').hide();
            $('#ModifyLayerLine').hide();
        }
        else if(status == '6' || status == '7' || status == '8' || status == '9' || status == '10') {
            $('#DefaultLayer').hide();
            $('#DefaultLayerLine').show();
            $('#AddLayer').hide();
            $('#AddLayerLine').show();
        }
    });

    // #닫기, 펼침
    $(document).on('click', '.depth-toggle-button', function() {
        $('#' + $(this).attr('depth-name')).toggle();
        $('#' + $(this).attr('depth-name') + 'Line').toggle();
    });

    // #다운로드 클릭
    $(document).on('click', '.download-btn', function() {
        var goodsPicNo = $('input[name=goodsPicNo]').val();
        var picType = $('input[name=picType]').val();
        BootstrapDialog.show({
            title: '압축중',
            message: '<img src="<?=PATH_ADMIN_GD_SHARE?>script/slider/slick/ajax-loader.gif"> <b>압축하는 중 입니다.</b>',
            closable: false
        });
        $.ajax({
            type : "GET",
            url: "<?=$url?>/upload/download.php?goodsPicNo=" + goodsPicNo + "&picType=" + picType,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                BootstrapDialog.closeAll();
                $('#DownloadLink').attr('href', data);
                $('#DownloadLink').get(0).click();
            },
            err: function(err) {
                alert(err.status);
            }
        });
    });

    // #파일 삭제 클릭
    $(document).on('click', '.delete-btn', function() {
        if(confirm('파일을 삭제하면 복구하지 못합니다.\n정말 삭제 하시겠습니까?')) {
            var goodsPicNo = $('input[name=goodsPicNo]').val();
            var picType = $('input[name=picType]').val();
            BootstrapDialog.show({
                title: '삭제중',
                message: '<img src="<?=PATH_ADMIN_GD_SHARE?>script/slider/slick/ajax-loader.gif"> <b>삭제하는 중 입니다.</b>',
                closable: false
            });
            $.ajax({
                type : "GET",
                url: "<?=$url?>/upload/delete.php?goodsPicNo=" + goodsPicNo + "&picType=" + picType,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    BootstrapDialog.closeAll();
                    if(data == 'success') alert('삭제 되었습니다.');
                    else alert('이미 삭제 되었습니다.');
                },
                err: function(err) {
                    alert(err.status);
                }
            });
        }
    });
});
</script>