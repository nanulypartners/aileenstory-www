/**
 * 추가 스크립트 - 추가적인 javascript 는 여기에 작성을 해주세요.
 *
 */


$(document).on('keyup', '.js-maxlength2', function(){

    var content = $(this);

    var regText = content.val();    // 문자열을 regText변수에 저장함
    var text_cnt = regText.replace(/ /gi, "");    // 모든 공백을 제거
    var space_cnt = (regText.match(/ /gi) || []).length;

    if(text_cnt.length > content.data('maxlength')){
        content.val(content.val().substring(0,content.data('maxlength')+space_cnt));

        content.parent().append('<span class="label label-success" style="display: block; position: absolute; white-space: nowrap; z-index: 999; top: 113px; left: 15px;font-size: 10px;border: 1px solid #eeeeee;background-color: #f8f8f8;color: #666;"> <em style="color:red">'+text_cnt.length+'</em> / '+content.attr('maxlength')+' </span>');

    }else{

        content.parent().append('<span class="label label-success" style="display: block; position: absolute; white-space: nowrap; z-index: 999; top: 113px; left: 15px;font-size: 10px;border: 1px solid #eeeeee;background-color: #f8f8f8;color: #666;"> <em style="color:red">'+text_cnt.length+'</em> / '+content.attr('maxlength')+' </span>');

    }

});



$(document).ready(function () {

    $('.js-maxlength2').keyup();

    $('.js-super-video-file').on({
        'click': function(e){
            var orderNo = $(this).closest('td').data('order-no');
            var goodsSno = $(this).closest('td').data('goods-sno');
            var regDt = $(this).closest('td').data('reg-date');
            var goodsNo = $(this).closest('td').data('goods-no');
            var goodsType = $(this).closest('td').data('goods-type');

            window.open('../order/popup_admin_order_goods_video_file.php?popupMode=yes&orderNo='+ orderNo +'&goodsSno=' + goodsSno + '&regDt=' + regDt + '&goodsNo=' + goodsNo + '&goodsType=' + goodsType, 'popup_super_video', 'width=1200,height=850,scrollbars=yes');
            return false;
        }
    });


    $('input.js-number').one('keyup', function (e) {
        $(this).number_only();
    });

    $("#main_img_background_use").change(function () {
        main_img_check()
    });
    main_img_check()
});

function main_img_check(){
    if ($("#main_img_background_use")[0]) {
        if ($("#main_img_background_use")[0].checked==true) {
            $(".main_img_class").css('display', '');
        } else {
            $(".main_img_class").css('display', 'none');
        }
    }
}

function add_basic(basic_cnt){

    var basic = '<tr>';
    basic += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    basic += '<td style="position: relative;">';
    basic += '<div style="margin:5px 0;">';
    basic += '<input type="text" name="basic[goods'+basic_cnt+'][text]" class="form-control width-3xl" placeholder="기본정보항목을 입력해주세요.">';
    basic += '</div>';
    basic += '</td>';
    basic += '</tr>';

    $("#basic_area").append(basic);

    $('input.js-number').one('keyup', function (e) {
        $(this).number_only();
    });

}

function add_letter(letter_cnt){

    var letter = '<tr>';
    letter += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    letter += '<td style="position: relative;">';
    letter += '<div style="margin:5px 0;">';
    letter += '제목';
    letter += '<input type="text" name="info[goods'+letter_cnt+'][orderTitle]" class="form-control width-3xl" placeholder="">';
    letter += '</div>';

    letter += '<div style="margin:5px 0;">';
    letter += '편지글 가이드 영상 URL';
    letter += '<input type="text" name="info[goods'+letter_cnt+'][letterGuide]" class="form-control width-3xl" placeholder="" value="https://player.vimeo.com/video/323370760?title=0&byline=0&portrait=0">';
    letter += '</div>';

    letter += '<div style="margin:5px 0;">';
    letter += '글자수 제한';
    letter += '<input type="text" name="info[goods'+letter_cnt+'][limitNm]" class="form-control width-sm js-number" placeholder="숫자만입력">';
    letter += '</div>';
    letter += '<div style="margin:5px 0;">';
    letter += '예시글';
    letter += '<textarea maxlength="1000" style="height:200px;" name="info[goods'+letter_cnt+'][requestMemo]" class="form-control" placeholder="예시글을 입력해 주세요."></textarea>';
    letter += '</div>';
    letter += '</td>';
    letter += '</tr>';

    $("#letter_area").append(letter);

    $('input.js-number').one('keyup', function (e) {
        $(this).number_only();
    });

}

function add_video(video_cnt){

    var video = '<tr>';
    video += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    video += '<td>';
    video += '<div style="margin:10px 0;">';
    video += '<p>예시 사진</p>';
    video += '<div class="bootstrap-filestyle input-group">';
    video += '<input type="file" name="video[files'+video_cnt+']" class="form-control input-sm">';
    video += '</div>';
    video += '</div>        ';
    video += '<div style="margin:10px 0;">';
    video += '제목';
    video += '<input type="text" name="video[goods'+video_cnt+'][title]" class="form-control width-3xl" placeholder="">';
    video += '</div>';
    video += '<div style="margin:10px 0;">';
    video += '설명문구';
    video += '<input type="text" name="video[goods'+video_cnt+'][content]" class="form-control width-3xl" placeholder="">';
    video += '</div>';
    video += '<div style="margin:10px 0;">';
//    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checkupload]" id="ck7_'+video_cnt+'"><label for="ck7_'+video_cnt+'">단일파일 업로드를 노출하실 경우 체크해 주세요.</label><br>';

    video += '<div>';
    video += '<span>단일파일여부</span> ';
    video += '<input type="radio" name="video[goods'+video_cnt+'][checkupload]" id="video[goods'+video_cnt+'][checkupload]_1" value="0"><label for="video[goods'+video_cnt+'][checkupload]_1">없음</label> ';
    video += '<input type="radio" name="video[goods'+video_cnt+'][checkupload]" id="video[goods'+video_cnt+'][checkupload]_2" value="1"><label for="video[goods'+video_cnt+'][checkupload]_2">사진</label> ';
    video += '<input type="radio" name="video[goods'+video_cnt+'][checkupload]" id="video[goods'+video_cnt+'][checkupload]_3" value="2"><label for="video[goods'+video_cnt+'][checkupload]_3">영상</label> ';
    video += '</div>';

    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checkheight]" id="ck5_'+video_cnt+'"><label for="ck5_'+video_cnt+'">세로사진 2장 대체 체크박스를 노출하실 경우 체크해 주세요.</label><br>';
    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checklength]" onclick="video_check(this);" id="ck1_'+video_cnt+'"><label for="ck1_'+video_cnt+'">동영상 사용길이를 노출하실 경우 체크해 주세요.</label><br>';
    video += '<input style="display:none;" type="text" name="video[goods'+video_cnt+'][textlength]" class="form-control width-3xl" placeholder="동영상 사용길이 안내문구를 입력해 주세요.">';
    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checksample]" id="ck3_'+video_cnt+'"><label for="ck3_'+video_cnt+'">샘플영상 대체 체크박스를 노출하실 경우 체크해 주세요.</label><br>';
    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checkimg]" id="ck4_'+video_cnt+'"><label for="ck4_'+video_cnt+'">사진대체 체크박스를 노출하실 경우 체크해 주세요.</label><br>';
    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checkword]" onclick="letter_check(this);" id="ck2_'+video_cnt+'"><label for="ck2_'+video_cnt+'">글귀를 노출하실 경우 체크해 주세요.</label><br>';
    video += '<div style="display:none;"><input type="text" name="video[goods'+video_cnt+'][textword]" class="form-control width-3xl" placeholder="예시 글귀를 입력해 주세요.">';
    video += '글자수 제한';
    video += '<input style="display:inline" type="text" name="video[goods'+video_cnt+'][lengthword]" class="form-control width-sm js-number" placeholder="숫자만입력"></div>';
    video += '<input type="checkbox" name="video[goods'+video_cnt+'][checkcare]" id="ck6_'+video_cnt+'"><label for="ck6_'+video_cnt+'">멀티업로드를 노출하실 경우 체크해 주세요.</label><br>';
    video += '</div>    ';
    video += '</td>';
    video += '</tr>';

    $("#video_area").append(video);

    $('input.js-number').one('keyup', function (e) {
        $(this).number_only();
    });

    var files = $(":file").not('.no-filestyle');
    if (files.length > 0) {
        files.filestyle({
            icon: false,
            buttonText: '찾아보기',
            buttonName: 'btn-gray',
            buttonBefore: true,
            size: 'sm'
        });
    }
}


$(document).on('click', 'input[name="goodsType"]', function(){
    if($(this).val()=="goods"){
        $(".goodsAdd").css('display', 'none');
        $(".goodsBasic").css('display', 'block');

    }else if($(this).val()=="addGoods"){
        $(".goodsBasic").css('display', 'none');
        $(".goodsAdd").css('display', 'inline-block');
    }
});


function video_check(index){
    if($(index).prop('checked')){
        $(index).next().next().next().css('display','');
    }else{
        $(index).next().next().next().css('display','none');
        $(index).next().next().next().val('');
    }
}

function letter_check(index){
    if($(index).prop('checked')){
        $(index).next().next().next().css('display','');
    }else{
        $(index).next().next().next().css('display','none');
        $(index).next().next().next().children().val('');
    }
}

function add_basic2(){

    var data = '<tr>';
    data += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    data += '<td><input type="text" name="basic_text[]" class="form-control width-3xl" placeholder="-를 입력하면 위아래 구분 간격이 추가됩니다."></td>';
    data += '</tr>';

    $("#basic").append(data);

}

function add_relation(){

    var data = '<tr>';
    data += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    data += '<td><input type="text" name="relation_text[]" class="form-control width-3xl" placeholder="-를 입력하면 위아래 구분 간격이 추가됩니다."></td>';
    data += '</tr>';

    $("#relation").append(data);

}

function add_scene(){

    $cnt = $('.scene_box').length+1;
    /*
        var data = `
                   <tr class="scene_box">
                        <th>
                            <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제">
                        </th>
                        <td style="position: relative;">

                            <div style="margin:5px 0;">
                                순서 <input type="text" name="scene[${$cnt}][seq]" class="form-control width-sm js-number" placeholder="순서" value="${$cnt}">
                            </div>

                            <div style="margin:5px 0;">
                                제목 <input type="text" name="scene[${$cnt}][subject]" class="form-control width-3xl" placeholder="장면 제목을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                설명 <input type="text" name="scene[${$cnt}][help]" class="form-control width-3xl" placeholder="장면 설명을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                장면시간 <input type="text" name="scene[${$cnt}][scene_time]" class="form-control width-3xl" placeholder="장면 노출 시간을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                노래가사 <input type="text" name="scene[${$cnt}][bgm_text]" class="form-control width-3xl" placeholder="장면 노래 가사를 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                <span>미리보기</span><br>
                                <input type="checkbox" name="scene[${$cnt}][preview]" id="preview_${$cnt}" class="form-control" value="yes" > <label for="preview_${$cnt}">미리보기를 사용할 경우 체크해주세요.</label>
                            </div>

                            <div style="margin:5px 0;">
                                <span>예시 사진</span><br>
                                <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                                    <input type="file" name="scene[${$cnt}][sample_image]" class="form-control input-sm">
                                </div>
                            </div>

                            <div style="margin:5px 0;">
                                <span>합성할 사진</span><br>
                                <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                                    <input type="file" name="scene[${$cnt}][sample_image_merge]" class="form-control input-sm">
                                </div>
                            </div>

                            <div style="margin:5px 0;">
                                <span>장면타입</span><br>
                                <select name="scene[${$cnt}][type]" class="form-control" onchange="type_change(this, this.value)">
                                    <option value="none">없음</option>
                                    <option value="photo">사진</option>
                                    <option value="video">영상</option>
                                </select>
                            </div>

                            <div style="margin:5px 0;display: none;" class="layer_name_box">
                                레이어이름 <input type="text" name="scene[${$cnt}][layer_name]" class="form-control width-sm" placeholder="레이어이름">
                                구분탭 <input type="text" name="scene[${$cnt}][tab_name]" class="form-control width-sm" placeholder="구분탭">
                            </div>

                            <div style="margin:5px 0;display: none;" class="photo_box">
                                <table class="table table-cols">
                                    <tr>
                                        <th>이미지 가로</th>
                                        <td><input type="text" name="scene[${$cnt}][image_width]" class="form-control width-sm js-number" placeholder="이미지 가로"></td>
                                        <th>이미지 세로</th>
                                        <td><input type="text" name="scene[${$cnt}][image_height]" class="form-control width-sm js-number" placeholder="이미지 세로"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 X좌표</th>
                                        <td><input type="text" name="scene[${$cnt}][image_x]" class="form-control width-sm js-number" placeholder="이미지 X좌표"></td>
                                        <th>이미지 Y좌표</th>
                                        <td><input type="text" name="scene[${$cnt}][image_y]" class="form-control width-sm js-number" placeholder="이미지 Y좌표"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌상X 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_1]" class="form-control width-sm js-number" placeholder="좌상X 시작점"></td>
                                        <th>이미지 좌상Y 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_2]" class="form-control width-sm js-number" placeholder="좌상Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우상X 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_3]" class="form-control width-sm js-number" placeholder="우상X 시작점"></td>
                                        <th>이미지 우상Y 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_4]" class="form-control width-sm js-number" placeholder="우상Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우하X 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_5]" class="form-control width-sm js-number" placeholder="우하X 시작점"></td>
                                        <th>이미지 우하Y 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_6]" class="form-control width-sm js-number" placeholder="우하Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌하X 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_7]" class="form-control width-sm js-number" placeholder="좌하X 시작점"></td>
                                        <th>이미지 좌하Y 시작점</th>
                                        <td><input type="text" name="scene[${$cnt}][start_8]" class="form-control width-sm js-number" placeholder="좌하Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌상X 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_1]" class="form-control width-sm js-number" placeholder="좌상X 끝나는점"></td>
                                        <th>이미지 좌상Y 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_2]" class="form-control width-sm js-number" placeholder="좌상Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우상X 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_3]" class="form-control width-sm js-number" placeholder="우상X 끝나는점"></td>
                                        <th>이미지 우상Y 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_4]" class="form-control width-sm js-number" placeholder="우상Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우하X 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_5]" class="form-control width-sm js-number" placeholder="우하X 끝나는점"></td>
                                        <th>이미지 우하Y 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_6]" class="form-control width-sm js-number" placeholder="우하Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌하X 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_7]" class="form-control width-sm js-number" placeholder="좌하X 끝나는점"></td>
                                        <th>이미지 좌하Y 끝나는점</th>
                                        <td><input type="text" name="scene[${$cnt}][end_8]" class="form-control width-sm js-number" placeholder="좌하Y 끝나는점"></td>
                                    </tr>
                                </table>
                            </div>`;

                    data += `
                            <div style="margin:5px 0;display: none;" class="video_box">
                                <table class="table table-cols">
                                    <tr>
                                        <th>영상 가로</th>
                                        <td><input type="text" name="scene[${$cnt}][video_width]" class="form-control width-sm js-number" placeholder="영상 가로"></td>
                                        <th>영상 세로</th>
                                        <td><input type="text" name="scene[${$cnt}][video_height]" class="form-control width-sm js-number" placeholder="영상 세로"></td>
                                        <th>영상 재생시간(초단위)</th>
                                        <td><input type="text" name="scene[${$cnt}][video_time]" class="form-control width-sm js-number" placeholder="재생시간"></td>
                                    </tr>
                                    <tr>
                                        <th>영상 X좌표</th>
                                        <td><input type="text" name="scene[${$cnt}][video_x]" class="form-control width-sm js-number" placeholder="영상 X좌표"></td>
                                        <th>영상 Y좌표</th>
                                        <td><input type="text" name="scene[${$cnt}][video_y]" class="form-control width-sm js-number" placeholder="영상 Y좌표"></td>
                                    </tr>
                                </table>
                            </div>

                            <div style="margin:5px 0;">
                                <span>글귀여부</span><br>
                                <select name="scene[${$cnt}][text_use]" class="form-control" onchange="text_use_change(this, this.value)">
                                    <option value="no">없음</option>
                                    <option value="yes">있음</option>
                                </select>
                            </div>

                            <div style="margin:5px 0;display: none;" class="text_box">
                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box(${$cnt}, 1, this);">단문추가</button>
                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box(${$cnt}, 2, this);">장문추가</button>
                                <br><br>
                                <div class="text_box_list">
                                <div>
                            </div>


                        </td>
                    </tr>`;
    */

    var data = "\n                   <tr class=\"scene_box\">\n                        <th>\n                            <input type=\"button\" class=\"btn btn-sm btn-white btn-icon-minus\" onclick=\"$(this).parent().parent().remove();\" value=\"삭제\">\n                        </th>\n                        <td style=\"position: relative;\">\n\n                            <div style=\"margin:5px 0;\">\n                                순서 <input type=\"text\" name=\"scene[" + $cnt + "][seq]\" class=\"form-control width-sm js-number\" placeholder=\"순서\" value=\"" + $cnt + "\">\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                제목 <input type=\"text\" name=\"scene[" + $cnt + "][subject]\" class=\"form-control width-3xl\" placeholder=\"장면 제목을 입력해주세요.\">\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                설명 <input type=\"text\" name=\"scene[" + $cnt + "][help]\" class=\"form-control width-3xl\" placeholder=\"장면 설명을 입력해주세요.\">\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                장면시간 <input type=\"text\" name=\"scene[" + $cnt + "][scene_time]\" class=\"form-control width-3xl\" placeholder=\"장면 노출 시간을 입력해주세요.\">\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                노래가사 <input type=\"text\" name=\"scene[" + $cnt + "][bgm_text]\" class=\"form-control width-3xl\" placeholder=\"장면 노래 가사를 입력해주세요.\">\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                <span>미리보기</span><br>\n                                <input type=\"checkbox\" name=\"scene[" + $cnt + "][preview]\" id=\"preview_" + $cnt + "\" class=\"form-control\" value=\"yes\" > <label for=\"preview_" + $cnt + "\">미리보기를 사용할 경우 체크해주세요.</label>\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                <span>예시 사진</span><br>\n                                <div class=\"bootstrap-filestyle input-group\" style=\"display:inline-block;vertical-align:bottom;\">\n                                    <input type=\"file\" name=\"scene[" + $cnt + "][sample_image]\" class=\"form-control input-sm\">\n                                </div>\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                <span>합성할 사진</span><br>\n                                <div class=\"bootstrap-filestyle input-group\" style=\"display:inline-block;vertical-align:bottom;\">\n                                    <input type=\"file\" name=\"scene[" + $cnt + "][sample_image_merge]\" class=\"form-control input-sm\">\n                                </div>\n                            </div>\n\n                            <div style=\"margin:5px 0;\">\n                                <span>장면타입</span><br>\n                                <select name=\"scene[" + $cnt + "][type]\" class=\"form-control\" onchange=\"type_change(this, this.value)\">\n                                    <option value=\"none\">없음</option>\n                                    <option value=\"photo\">사진</option>\n                                    <option value=\"video\">영상</option>\n                                </select>\n                            </div>\n\n                            <div style=\"margin:5px 0;display: none;\" class=\"layer_name_box\">\n                                레이어이름 <input type=\"text\" name=\"scene[" + $cnt + "][layer_name]\" class=\"form-control width-sm\" placeholder=\"레이어이름\">\n                                구분탭 <input type=\"text\" name=\"scene[" + $cnt + "][tab_name]\" class=\"form-control width-sm\" placeholder=\"구분탭\">\n                            </div>\n\n                            <div style=\"margin:5px 0;display: none;\" class=\"photo_box\">\n                                <table class=\"table table-cols\">\n                                    <tr>\n                                        <th>이미지 가로</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][image_width]\" class=\"form-control width-sm js-number\" placeholder=\"이미지 가로\"></td>\n                                        <th>이미지 세로</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][image_height]\" class=\"form-control width-sm js-number\" placeholder=\"이미지 세로\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 X좌표</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][image_x]\" class=\"form-control width-sm js-number\" placeholder=\"이미지 X좌표\"></td>\n                                        <th>이미지 Y좌표</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][image_y]\" class=\"form-control width-sm js-number\" placeholder=\"이미지 Y좌표\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 좌상X 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_1]\" class=\"form-control width-sm js-number\" placeholder=\"좌상X 시작점\"></td>\n                                        <th>이미지 좌상Y 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_2]\" class=\"form-control width-sm js-number\" placeholder=\"좌상Y 시작점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 우상X 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_3]\" class=\"form-control width-sm js-number\" placeholder=\"우상X 시작점\"></td>\n                                        <th>이미지 우상Y 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_4]\" class=\"form-control width-sm js-number\" placeholder=\"우상Y 시작점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 우하X 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_5]\" class=\"form-control width-sm js-number\" placeholder=\"우하X 시작점\"></td>\n                                        <th>이미지 우하Y 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_6]\" class=\"form-control width-sm js-number\" placeholder=\"우하Y 시작점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 좌하X 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_7]\" class=\"form-control width-sm js-number\" placeholder=\"좌하X 시작점\"></td>\n                                        <th>이미지 좌하Y 시작점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][start_8]\" class=\"form-control width-sm js-number\" placeholder=\"좌하Y 시작점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 좌상X 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_1]\" class=\"form-control width-sm js-number\" placeholder=\"좌상X 끝나는점\"></td>\n                                        <th>이미지 좌상Y 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_2]\" class=\"form-control width-sm js-number\" placeholder=\"좌상Y 끝나는점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 우상X 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_3]\" class=\"form-control width-sm js-number\" placeholder=\"우상X 끝나는점\"></td>\n                                        <th>이미지 우상Y 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_4]\" class=\"form-control width-sm js-number\" placeholder=\"우상Y 끝나는점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 우하X 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_5]\" class=\"form-control width-sm js-number\" placeholder=\"우하X 끝나는점\"></td>\n                                        <th>이미지 우하Y 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_6]\" class=\"form-control width-sm js-number\" placeholder=\"우하Y 끝나는점\"></td>\n                                    </tr>\n                                    <tr>\n                                        <th>이미지 좌하X 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_7]\" class=\"form-control width-sm js-number\" placeholder=\"좌하X 끝나는점\"></td>\n                                        <th>이미지 좌하Y 끝나는점</th>\n                                        <td><input type=\"text\" name=\"scene[" + $cnt + "][end_8]\" class=\"form-control width-sm js-number\" placeholder=\"좌하Y 끝나는점\"></td>\n                                    </tr>\n                                </table>\n                            </div>";



    data += "\n                        <div style=\"margin:5px 0;display: none;\" class=\"video_box\">\n                            <table class=\"table table-cols\">\n                                <tr>\n                                    <th>영상 가로</th>\n                                    <td><input type=\"text\" name=\"scene[" + $cnt + "][video_width]\" class=\"form-control width-sm js-number\" placeholder=\"영상 가로\"></td>\n                                    <th>영상 세로</th>\n                                    <td><input type=\"text\" name=\"scene[" + $cnt + "][video_height]\" class=\"form-control width-sm js-number\" placeholder=\"영상 세로\"></td>\n                                    <th>영상 재생시간(초단위)</th>\n                                    <td><input type=\"text\" name=\"scene[" + $cnt + "][video_time]\" class=\"form-control width-sm js-number\" placeholder=\"재생시간\"></td>\n                                </tr>\n                                <tr>\n                                    <th>영상 X좌표</th>\n                                    <td><input type=\"text\" name=\"scene[" + $cnt + "][video_x]\" class=\"form-control width-sm js-number\" placeholder=\"영상 X좌표\"></td>\n                                    <th>영상 Y좌표</th>\n                                    <td><input type=\"text\" name=\"scene[" + $cnt + "][video_y]\" class=\"form-control width-sm js-number\" placeholder=\"영상 Y좌표\"></td>\n                                </tr>\n                            </table>\n                        </div>\n\n                        <div style=\"margin:5px 0;\">\n                            <span>글귀여부</span><br>\n                            <select name=\"scene[" + $cnt + "][text_use]\" class=\"form-control\" onchange=\"text_use_change(this, this.value)\">\n                                <option value=\"no\">없음</option>\n                                <option value=\"yes\">있음</option>\n                            </select>\n                        </div>\n\n                        <div style=\"margin:5px 0;display: none;\" class=\"text_box\">\n                            <button type=\"button\" class=\"btn btn-sm btn-white btn-icon-plus\" onclick=\"add_text_box(" + $cnt + ", 1, this);\">단문추가</button>\n                            <button type=\"button\" class=\"btn btn-sm btn-white btn-icon-plus\" onclick=\"add_text_box(" + $cnt + ", 2, this);\">장문추가</button>\n                            <br><br>\n                            <div class=\"text_box_list\">\n                            <div>\n                        </div>\n\n\n                    </td>\n                </tr>";


    $("#scene_area").append(data);

}


function add_text_box($cnt, $type, $dom){
    /*
        var data = `<table class="table table-cols">
                        <tr>
                            <td rowspan="4">
                                <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().parent().parent().remove();" value="삭제">
                            </td>
                            <th>레이어이름</th>
                            <td><input type="text" name="scene[${$cnt}][text_layer_name][]" class="form-control width-sm" placeholder="레이어이름"></td>
                            <th>최대 글자수</th>
                            <td><input type="text" name="scene[${$cnt}][text_max_length][]" class="form-control width-sm js-number" placeholder="최대 글자수"></td>
                            <th>회전각도</th>
                            <td><input type="text" name="scene[${$cnt}][text_rotate][]" class="form-control width-sm" placeholder="회전각도"></td>
                        </tr>
                        <tr>
                            <th>텍스트 X좌표</th>
                            <td><input type="text" name="scene[${$cnt}][text_x][]" class="form-control width-sm js-number" placeholder="텍스트 X좌표"></td>
                            <th>텍스트 Y좌표</th>
                            <td><input type="text" name="scene[${$cnt}][text_y][]" class="form-control width-sm js-number" placeholder="텍스트 Y좌표"></td>
                            <th>텍스트 크기</th>
                            <td><input type="text" name="scene[${$cnt}][text_size][]" class="form-control width-sm js-number" placeholder="텍스트 크기"></td>
                        </tr>
                        <tr>
                            <th>텍스트 색상</th>
                            <td><input type="text" name="scene[${$cnt}][text_color][]" class="form-control width-sm" placeholder="텍스트 색상"></td>
                            <th>텍스트 종류</th>
                            <td><input type="text" name="scene[${$cnt}][text_family][]" class="form-control width-sm" placeholder="텍스트 종류"></td>
                            <th>텍스트 정렬</th>
                            <td>
                                <select name="scene[${$cnt}][text_align][]" class="form-control">
                                    <option value="left">왼쪽</option>
                                    <option value="center">가운데</option>
                                    <option value="right">오른쪽</option>
                                </select>
                            </td>
                        </tr>`;

        if ($type==1) {
                data +=`<tr>
                            <th>글귀 예시</th>
                            <td colspan="5">
                                <input type="hidden" name="scene[${$cnt}][text_type][]" value="text">
                                <input type="text" name="scene[${$cnt}][text_sample][]" class="form-control width-4xl" placeholder="예시글을 입력해 주세요.">
                            </td>
                        </tr>
                    </table> `;
        } else {
                data +=`<tr>
                            <th>글귀 예시</th>
                            <td colspan="5">
                                <input type="hidden" name="scene[${$cnt}][text_type][]" value="textarea">
                                <textarea style="height:100px;" maxlength="1000" name="scene[${$cnt}][text_sample][]" class="form-control" placeholder="예시글을 입력해 주세요."></textarea>
                            </td>
                        </tr>
                    </table> `;
        }
    */

    var data = "<table class=\"table table-cols\">\n                    <tr>\n                        <td rowspan=\"4\">\n                            <input type=\"button\" class=\"btn btn-sm btn-white btn-icon-minus\" onclick=\"$(this).parent().parent().parent().parent().remove();\" value=\"삭제\">\n                        </td>\n                        <th>레이어이름</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_layer_name][]\" class=\"form-control width-sm\" placeholder=\"레이어이름\"></td>\n                        <th>최대 글자수</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_max_length][]\" class=\"form-control width-sm js-number\" placeholder=\"최대 글자수\"></td>\n                        <th>회전각도</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_rotate][]\" class=\"form-control width-sm\" placeholder=\"회전각도\"></td>\n                    </tr>\n                    <tr>\n                        <th>텍스트 X좌표</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_x][]\" class=\"form-control width-sm js-number\" placeholder=\"텍스트 X좌표\"></td>\n                        <th>텍스트 Y좌표</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_y][]\" class=\"form-control width-sm js-number\" placeholder=\"텍스트 Y좌표\"></td>\n                        <th>텍스트 크기</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_size][]\" class=\"form-control width-sm js-number\" placeholder=\"텍스트 크기\"></td>\n                    </tr>\n                    <tr>\n                        <th>텍스트 색상</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_color][]\" class=\"form-control width-sm\" placeholder=\"텍스트 색상\"></td>\n                        <th>텍스트 종류</th>\n                        <td><input type=\"text\" name=\"scene[" + $cnt + "][text_family][]\" class=\"form-control width-sm\" placeholder=\"텍스트 종류\"></td>\n                        <th>텍스트 정렬</th>\n                        <td>\n                            <select name=\"scene[" + $cnt + "][text_align][]\" class=\"form-control\">\n                                <option value=\"left\">왼쪽</option>\n                                <option value=\"center\">가운데</option>\n                                <option value=\"right\">오른쪽</option>\n                            </select>\n                        </td>\n                    </tr>";

    if ($type == 1) {
        data += "<tr>\n                        <th>글귀 예시</th>\n                        <td colspan=\"5\">\n                            <input type=\"hidden\" name=\"scene[" + $cnt + "][text_type][]\" value=\"text\">\n                            <input type=\"text\" name=\"scene[" + $cnt + "][text_sample][]\" class=\"form-control width-4xl\" placeholder=\"예시글을 입력해 주세요.\">\n                        </td>\n                    </tr>\n                </table> ";
    } else {
        data += "<tr>\n                        <th>글귀 예시</th>\n                        <td colspan=\"5\">\n                            <input type=\"hidden\" name=\"scene[" + $cnt + "][text_type][]\" value=\"textarea\">\n                            <textarea style=\"height:100px;\" maxlength=\"1000\" name=\"scene[" + $cnt + "][text_sample][]\" class=\"form-control\" placeholder=\"예시글을 입력해 주세요.\"></textarea>\n                        </td>\n                    </tr>\n                </table> ";
    }

    $($dom).parent().find('.text_box_list').append(data);

}


function type_change($dom, $type){
    if ($type=='photo') {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'block');
        $($dom).parent().parent().find('.photo_box').css('display', 'block');
        $($dom).parent().parent().find('.video_box').css('display', 'none');
    } else if ($type=='video') {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'block');
        $($dom).parent().parent().find('.photo_box').css('display', 'none');
        $($dom).parent().parent().find('.video_box').css('display', 'block');
    } else {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'none');
        $($dom).parent().parent().find('.photo_box').css('display', 'none');
        $($dom).parent().parent().find('.video_box').css('display', 'none');
    }
}

function text_use_change($dom, $type){
    if ($type=='yes') {
        $($dom).parent().parent().find('.text_box').css('display', 'block');
    } else {
        $($dom).parent().parent().find('.text_box').css('display', 'none');
    }
}


function add_basic3(){

    var data = '<tr>';
    data += '<th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>';
    data += '<td><input type="text" name="basic[]" class="form-control width-3xl" placeholder="추가할 기본정보를 입력해 주세요."></td>';
    data += '</tr>';

    $("#basic").append(data);

}




/*업로드관리 시작*/

function add_text_box_upload($cnt, $type, $dom){
    var data = `<table class="table table-cols">`;

    if ($type==1) {
            data +=`<tr>
                        <th>글귀 예시</th>
                        <td>
                            <input type="hidden" name="scene[${$cnt}][text_type][]" value="text">
                            <input type="text" name="scene[${$cnt}][text_sample][]" class="form-control width-3xl" placeholder="예시글을 입력해 주세요.">
                        </td>
                    </tr>
                </table> `;
    } else {
            data +=`<tr>
                        <th>글귀 예시</th>
                        <td>
                            <input type="hidden" name="scene[${$cnt}][text_type][]" value="textarea">
                            <textarea style="height:100px;" maxlength="1000" name="scene[${$cnt}][text_sample][]" class="form-control width-3xl" placeholder="예시글을 입력해 주세요."></textarea>
                        </td>
                    </tr>
                </table> `;
    }

    $($dom).parent().find('.text_box_list').append(data);

}

function type_change_upload($dom, $type){
    if ($type=='photo') {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'block');
        $($dom).parent().parent().find('.text_box').css('display', 'none');
    } else if ($type=='video') {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'block');
        $($dom).parent().parent().find('.text_box').css('display', 'none');
    } else {
        $($dom).parent().parent().find('.layer_name_box').css('display', 'none');
        $($dom).parent().parent().find('.text_box').css('display', 'block');
    }
}

function add_scene_upload(){

    $cnt = $('.scene_box').length+1;
    var data = `
               <tr class="scene_box">
                    <th>
                        <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제">
                    </th>
                    <td style="position: relative;">

                        <div style="margin:5px 0;">
                            순서 <input type="text" name="scene[${$cnt}][seq]" class="form-control width-sm js-number" placeholder="순서" value="${$cnt}">
                        </div>

                        <div style="margin:5px 0;">
                            <span>장면타입</span><br>
                            <select name="scene[${$cnt}][type]" class="form-control" onchange="type_change_upload(this, this.value)">
                                <option value="text">텍스트</option>
                                <option value="photo">사진</option>
                                <option value="video">영상</option>
                            </select>
                        </div>
                        
                        탭이름 <input type="text" name="scene[${$cnt}][tabname]" class="form-control width-sm" placeholder="탭이름">
                        설명 <input type="text" name="scene[${$cnt}][title]" class="form-control width-3xl" placeholder="설명">    
                        <div style="margin:5px 0;display: none;" class="layer_name_box">
                            필요한 파일수 <input type="text" name="scene[${$cnt}][file_count]" class="form-control width-sm" placeholder="필요한 파일수">
                        </div>
                        `;

                data += `
                        <div style="margin:5px 0;" class="text_box">
                            <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box_upload(${$cnt}, 1, this);">단문추가</button>
                            <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box_upload(${$cnt}, 2, this);">장문추가</button>
                            <br><br>
                            <div class="text_box_list">
                            <div>
                        </div>


                    </td>
                </tr>`;




    $("#scene_area").append(data);

}