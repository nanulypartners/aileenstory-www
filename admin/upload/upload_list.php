<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
    <div class="btn-group">
        <a href="upload_register.php" class="btn btn-red-line">업로드파일 추가</a>
    </div>
</div>

<div class="table-title">업로드 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit">
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'goodsNm' => '상품명', 'goodsCd' => '상품코드'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>

<form id="frmList" action="" method="get" target="ifrmProcess">

    <input type="hidden" name="videoMode" value="">
    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th>
                <th>번호</th>
                <th>상품구분</th>
                <th>상품코드</th>
                <th style="min-width: 300px !important;">상품명</th>
                <th>등록일</th>
                <th>수정일</th>
                <th>관리</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {
                    $valGoodsNm = htmlspecialchars($val['goodsNm']);
                    $valGoodsType = htmlspecialchars($val['goodsType']);
                    ?>
                    <tr>
                        <!--선택-->
                        <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td>

                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>
                        <!-- 상품구분 -->
                        <td class="center number"><?=$val['goodsType']=="goods"?"일반상품":"추가상품";?></td>
                        <!--상품코드번호-->
                        <td class="center number"><?=$val['goodsCd'];?></td>
                        <!--상품명-->
                        <td>
                            <div>
                                <a class="text-blue hand" onclick="goods_register_popup(<?=$val['goodsCd']?>);">
                                    <?=$val['goodsNm'];?>
                                </a>
                            </div>
                        </td>

                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['regDt']); ?></td>

                        <!--수정일-->
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['modDt']); ?></td>

                        <td class="center padlr10">
                            <a href="./upload_register.php?goodsNo=<?=$val['goodsCd']; ?>&goodsNm=<?=urlencode($valGoodsNm)?>&goodsType=<?=$valGoodsType?>&videoMode=modify" class="btn btn-white btn-sm">수정</a>

                        </td>

                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnDelete">선택 삭제</button>
        </div>
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#btnDelete').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 상품이 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개 영상을  정말로 삭제하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'videoMode\']').val('delete');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './upload_ps.php');
                    $('#frmList').submit();
                }
            });

        });
    });
</script>