<form id="frmVideo" name="frmVideo" action="upload_ps.php" method="post" class="content_form" enctype="multipart/form-data" target="ifrmProcess">
    <input type="hidden" name="videoGoodsNm" class="videoGoodsNm" value="<?=urldecode($goodsNm)?>">
    <input type="hidden" name="getGoodsNo" class="getGoodsNo" value="<?=$getGoodsNo?>">
    <input type="hidden" name="videoMode" class="videoMode" value="<?=$videoMode?>">
    <?php if(gd_isset($getVideoAdmin)){?>
        <input type="hidden" name="goodsType" value="<?=$getVideoAdmin['goodsType']?>">
    <?php }?>

    <div class="page-header js-affix">
        <h3><?php echo end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="goList('./upload_list.php?page=1');">
            <input type="submit" value="저장" class="btn btn-red">
        </div>
    </div>


    <div class="table-title gd-help-manual">
        <div class="flo-left">상품 선택</div>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>상품구분</th>
                <?php if(gd_isset($getVideoAdmin)){?>
                    <td>
                        <span><?=$getVideoAdmin['goodsType']=="goods"?"일반상품":"추가상품";?></span>
                    </td>
                <?php } else {?>
                    <td>
                        <input type="radio" name="goodsType" id="goodsType1" value="goods"><label for="goodsType1">일반상품</label>
                        <input type="radio" name="goodsType" id="goodsType2" value="addGoods"><label for="goodsType2">추가상품</label>
                    </td>
                <?php }?>
            </tr>
            <tr>
                <th>상품선택</th>
                <?php
                if(gd_isset($getVideoAdmin)){
                    ?>
                    <td><span><?php echo $getVideoAdmin['goodsNm']; ?></span></td>
                    <?php
                } else {
                    ?>
                    <td class="goodsBasic">

                        <select name="" class="getCate" onchange="get_cate_search('getCate', this.value);">
                            <option value="">카테고리를 선택해 주세요</option>
                            <?php
                            foreach ($cateDepth as $key => $val) {
                                echo "<option value='{$val['cateCd']}'>{$val['cateNm']}</option>";
                            }
                            ?>
                        </select>

                        <select name="" class="getCateSub" onchange="get_cate_search('getCateSub', this.value);">
                            <option value="">카테고리를 선택해 주세요</option>
                        </select>

                        <select name="videoGoodsNo" class="videoGoodsNo">
                            <option value="">상품을 선택해 주세요</option>
                            <?php
                            foreach ($videoInfo as $key => $val) {
                                echo "<option value='{$val['goodsNo']}'>{$val['goodsNm']}</option>";
                            }

                            ?>
                        </select>

                    </td>
                    <td class="goodsAdd" style="width:100%;">
                        <select name="videoGoodsAddNo" class="videoAddGoodsNo ">
                            <option value="">상품을 선택해 주세요</option>
                            <?php
                            foreach ($addGoods as $key => $val) {
                                echo "<option value='{$val['addGoodsNo']}'>{$val['goodsNm']}({$val['addGoodsNo']})</option>";
                            }

                            ?>
                        </select>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <th>노출여부</th>
                <td>
                    <input type="radio" name="useYn" id="use1" value="1" <?=$getVideoAdmin['useYn']=="1"?"checked":"";?>><label for="use1">노출함</label>
                    <input type="radio" name="useYn" id="use2" value="0" <?=$getVideoAdmin['useYn']!="1"?"checked":"";?>><label for="use2">노출안함</label>
                </td>
            </tr>

            <tr>
                <th>샘플영상주소<br>(유튜브, 비메오 등)</th>
                <td>
                    <input type="text" name="sampleVideoUrl" class="form-control width-4xl" value="<?=$getVideoAdmin['sampleVideoUrl']?>">
                </td>
            </tr>

            <tr>
                <th>배경음 이름</th>
                <td><input type="text" name="bgmName" class="form-control width-sm" value="<?=$getVideoAdmin['bgmName']?>" placeholder="배경음 이름"></td>
            </tr>
            <tr>
                <th>배경음 변경여부</th>
                <td>
                    <input type="radio" name="bgmChange" id="bgmChange1" value="Y" <?=$getVideoAdmin['bgmChange']=="Y"?"checked":"";?>><label for="bgmChange1">변경가능</label>
                    <input type="radio" name="bgmChange" id="bgmChange2" value="N" <?=$getVideoAdmin['bgmChange']!="Y"?"checked":"";?>><label for="bgmChange2">변경불가</label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="table-title gd-help-manual">
        <div class="flo-left" style="margin-right:10px;">기본정보입력</div>
        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_basic3();">항목추가</button>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody id="basic">

            <?php foreach($getVideoAdmin['basicInfo'] as $v){?>
                <tr>
                    <th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>
                    <td><input type="text" name="basic[]" class="form-control width-3xl" placeholder="추가할 기본정보를 입력해 주세요." value="<?=$v?>"></td>
                </tr>
            <?php }?>

            </tbody>

            <tr>
                <td colspan="2"><button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_basic3();">항목추가</button></td>
            </tr>

        </table>
    </div>


    <div class="table-title gd-help-manual">
        <div class="flo-left" style="margin-right:10px;">장면정보입력</div>
        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_scene_upload();">항목추가</button>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody id="scene_area">
            <?php
            if(gd_isset($getGoodsNo)){
                foreach ($getVideoAdmin['sceneInfo'] as $key => $value) {
                    ?>
                    <tr class="scene_box">
                        <th>
                            <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제">
                        </th>
                        <td style="position: relative;">

                            <div style="margin:5px 0;">
                                순서 <input type="text" name="scene[<?=$key?>][seq]" class="form-control width-sm js-number" value="<?=$value['seq']?>" placeholder="순서">
                            </div>

                            <div style="margin:5px 0;">
                                <span>장면타입</span><br>
                                <select name="scene[<?=$key?>][type]" class="form-control" onchange="type_change_upload(this, this.value)">
                                    <option value="text" <?=$value['type']=="text"?"selected":""?>>텍스트</option>
                                    <option value="photo" <?=$value['type']=="photo"?"selected":""?>>사진</option>
                                    <option value="video" <?=$value['type']=="video"?"selected":""?>>영상</option>
                                </select>
                            </div>

                            탭이름 <input type="text" name="scene[<?=$key?>][tabname]" class="form-control width-sm" placeholder="탭이름" value="<?=htmlspecialchars($value['tabname'])?>">
                            설명 <input type="text" name="scene[<?=$key?>][title]" class="form-control width-3xl" value="<?=htmlspecialchars($value['title'])?>" placeholder="설명">
                            <div style="margin:5px 0;<?=$value['type']=="text"?"display: none;":""?>" class="layer_name_box">
                                필요한 파일수 <input type="text" name="scene[<?=$key?>][file_count]" class="form-control width-sm" value="<?=htmlspecialchars($value['file_count'])?>"placeholder="필요한 파일수">
                            </div>

                            <div style="margin:5px 0;<?=$value['type']=="text"?"":"display: none;"?>" class="text_box">

                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box_upload(<?=$key?>, 1, this);">단문추가</button>
                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box_upload(<?=$key?>, 2, this);">장문추가</button>
                                <br><br>

                                <div class="text_box_list">
                                    <?php foreach ($value['text_type'] as $k => $v) {?>
                                        <table class="table table-cols">
                                            <tr>
                                                <th>글귀 예시</th>
                                                <td colspan="5">
                                                    <?php if ($value['text_type'][$k]=="text") {?>
                                                        <input type="hidden" name="scene[<?=$key?>][text_type][]" value="text">
                                                        <input type="text" name="scene[<?=$key?>][text_sample][]" class="form-control width-3xl" value="<?=htmlspecialchars($value['text_sample'][$k])?>"placeholder="텍스트 색상">
                                                    <?php } else {?>
                                                        <input type="hidden" name="scene[<?=$key?>][text_type][]" value="textarea">
                                                        <textarea style="height:100px;" maxlength="1000" name="scene[<?=$key?>][text_sample][]" class="form-control width-3xl" placeholder="예시글을 입력해 주세요."><?=htmlspecialchars($value['text_sample'][$k])?></textarea>
                                                    <?php }?>
                                                </td>
                                            </tr>

                                        </table>
                                    <?php }?>

                                    <div>
                                    </div>


                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
            <tr>
                <td colspan="2"><button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_scene_upload();">항목추가</button></td>
            </tr>
        </table>
    </div>

    </div>
</form>

<script type="text/javascript">
    $('.videoGoodsNo').change(function(){
        var videoGoodsNm = $('.videoGoodsNo option:checked').text();
        $('.videoGoodsNm').val(videoGoodsNm);
    });

    $('.videoAddGoodsNo').change(function(){
        var videoGoodsNm = $('.videoAddGoodsNo option:checked').text();
        $('.videoGoodsNm').val(videoGoodsNm);
    });

    function get_cate_search(mode, value) {

        var getCate = value;

        console.log(getCate);

        $.ajax({
            url:"./upload_register.php",
            type:'GET',
            data: {'data':getCate, 'mode':mode, 'type':'create'},
            success:function(data){
                // console.log(JSON.parse(data));
                data = JSON.parse(data);
                // console.log(data.cate);

                $(".getCateSub").html(data.cate);

                $(".videoGoodsNo").html(data.goods);
            },
            error:function(jqXHR, textStatus, errorThrown){
                // alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
                // self.close();
            }
        });
    }

</script>