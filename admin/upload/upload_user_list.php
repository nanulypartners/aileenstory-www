<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
</div>

<div class="table-title">업로드 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit content-form">
    <input type="hidden" name="pageNum" value="<?= Request::get()->get('pageNum', 20); ?>"/>
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>상태</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="state" value="" <?php if($req['state']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($state as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="state" value="<?php echo $dKey;?>" <?php if($req['state']=="{$dKey}"){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'createNm' => '제작번호', 'odNo' => '주문번호', 'odSno' => '상품주문번호', 'odName' => '주문자명', 'odId' => '주문자 아이디'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>



<form id="frmList" action="" method="get" target="ifrmProcess">
    <input type="hidden" name="mode" value="">

    <div class="table-header form-inline">
        <div class="pull-right">
            <?= gd_select_box_by_page_view_count(Request::get()->get('pageNum', 20)); ?>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <!-- <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th> -->
                <th>번호</th>
                <th>제작번호</th>
                <th>주문번호(상품주문번호)</th>
                <th>주문자(주문자아이디)</th>
                <th>시안영상</th>
                <th>원본영상</th>
                <th>등록일</th>
                <th>수정일</th>
                <th>제작상태</th>
                <th>상세</th>
                <th>삭제</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {
                    if ($val['state'] == '4') {
                        $check = "text-myclose";
                    } else {
                        $check = "";
                    }

                    if ($val['state'] == '3') {
                        $tr_style = "color:#01c12c";
                    } else if ($val['state'] == '2') {
                        $tr_style = "color:#3339f3";
                    } else if ($val['state'] == '1') {
                        $tr_style = "color:#e82727";
                    } else {
                        $tr_style = "";
                    }


                    ?>
                    <tr class="<?=$check?>">
                        <!--선택-->
                        <!-- <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td> -->

                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>
                        <td class="center number"><?=$val['createNm'];?></td>
                        <td class="center number">
                            <?php if ($val['odNo']) {?>
                                <?=$val['odNo'];?>(<?=$val['odSno'];?>)
                            <?php } else {?>
                                -
                            <?php }?>
                        </td>
                        <td class="center number"><?=$val['odName'];?>(<?=$val['odId'];?>)</td>
                        <td class="center number">
                            <?php if ($val['sianPath']!="") { $sianPath = explode("/", $val['sianPath']); ?>
                                <a href="https://card.studioium.com<?=$val['sianPath']?>" target="_blank"><?=$sianPath[count($sianPath)-1]?></a>
                            <?php }?>
                        </td>
                        <td class="center number">
                            <?php if ($val['moviePath']!="") { $moviePath = explode("/", $val['moviePath']);?>
                                <a href="https://card.studioium.com<?=$val['moviePath']?>" target="_blank"><?=$moviePath[count($moviePath)-1]?></a>
                            <?php }?>
                        </td>
                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['regDt']); ?></td>
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['modDt']); ?></td>
                        <td class="center number">
                            <select id="<?=$val['no'];?>_state" style="display: inline-block; vertical-align: middle; width: 50px !important;" class="form-control">
                                <option value="">선택</option>
                                <option value="0" <?=$val['state']==0?"selected":"";?>>준비중</option>
                                <option value="1" <?=$val['state']==1?"selected":"";?>>제작요청</option>
                                <option value="2" <?=$val['state']==2?"selected":"";?>>제작중</option>
                                <option value="3" <?=$val['state']==3?"selected":"";?>>제작완료</option>
                                <option value="4" <?=$val['state']==4?"selected":"";?>>자동삭제</option>
                                <option value="5" <?=$val['state']==5?"selected":"";?>>수정요청</option>
                                <option value="6" <?=$val['state']==6?"selected":"";?>>수정중</option>
                                <option value="7" <?=$val['state']==7?"selected":"";?>>수정완료</option>
                                <option value="8" <?=$val['state']==8?"selected":"";?>>시안확정</option>
                            </select>
                            <button type="button" onclick="stateChange('<?=$val['no'];?>');" class="btn btn-white btn-sm">변경</button>
                        </td>
                        <td class="center date">
                            <a href="javascript:window.open('./render_user_detail.php?no=<?=$val['no']; ?>' ,'render_user_detail', 'width=1024, height=768');" class="btn btn-white btn-sm">보기</a>
                        </td>
                        <td class="center date">
                            <?php if ($val['state']==0) {?>
                                <button type="button" onclick="deleteData('<?=$val['createNm'];?>');" class="btn btn-white btn-sm">삭제</button>
                            <?php } else {?>
                                -
                            <?php }?>

                        </td>
                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <!-- <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnDelete">선택 삭제</button>
        </div> -->
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">

    // 클래스명 js-page-number 셀렉트박스의 이벤트
    var $js_page_number = $('select.js-page-number');
    if ($js_page_number.length > 0) {
        $(document).on('change', 'select.js-page-number', function (e) {
            e.preventDefault();
            var $input = $('input[name=\'pageNum\']');
            console.log($input);
            $input.val($js_page_number.find(':selected').val());
            $('.content-form[method="get"]:eq(0)').submit();
        });
    }

    function stateChange(no){
        var type = $('#'+no+'_state option:selected').val();

        if (type==="") {
            alert('변경할 상태를 선택해 주세요.');
        } else {
            dialog_confirm('제작 상태를 변경 하시겠습니까?', function (result) {
                if (result) {
                    var url = "./upload_state_change.php"
                    $.post(url, {'no':no, 'type':type}, function(data) {
                        alert(data);
                    });
                }
            });
        }

    }

    function deleteData(createNm){

        dialog_confirm('선택한 데이터를 정말로 삭제하시겠습니까?', function (result) {
            if (result) {
                var url = "./upload_delete.php";
                $.post(url, {'createNm':createNm}, function(data) {

                    var data = JSON.parse(data);

                    if (data.state=="ok") {
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                });
            }
        });
    }

</script>