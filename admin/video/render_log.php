<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
</div>

<div class="table-title">로그 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit content-form">
    <input type="hidden" name="pageNum" value="<?= Request::get()->get('pageNum', 20); ?>"/>
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>상태</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="state" value="" <?php if($req['state']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($state as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="state" value="<?php echo $dKey;?>" <?php if($req['state']=="{$dKey}"){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>결제구분</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="payment" value="" <?php if($req['payment']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($payment as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="payment" value="<?php echo $dKey;?>" <?php if($req['payment']=="{$dKey}"){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'createNm' => '제작번호', 'odNo' => '주문번호', 'odSno' => '상품주문번호', 'odName' => '주문자명', 'odId' => '주문자 아이디'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>



<form id="frmList" action="" method="get" target="ifrmProcess">
    <input type="hidden" name="searchField" value="<?=gd_isset($req['searchField']); ?>" class="form-control"/>
    <input type="hidden" name="useYn" value="<?=gd_isset($req['useYn']); ?>" class="form-control"/>
    <input type="hidden" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
    <input type="hidden" name="mode" value="">

    <div class="table-header form-inline">
        <div class="pull-right">
            <?= gd_select_box_by_page_view_count(Request::get()->get('pageNum', 20)); ?>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th>
                <th>번호</th>
                <th>결제구분</th>
                <th>렌더링타입</th>
                <th>제작번호</th>
                <th>주문번호(상품주문번호)</th>
                <th>내용</th>
                <th>등록일</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {
                    ?>
                    <tr>
                        <!--선택-->
                        <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td>

                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>
                        <td class="center number"><?=$val['payment']>0?"유료":"무료";?></td>
                        <td class="center number"><?=$val['renderType']==0?"저화질":"고화질";?></td>
                        <td class="center number"><a href="./render_user_list.php?searchField=createNm&keyword=<?=$val['createNm'];?>"><?=$val['createNm'];?></a></td>
                        <td class="center number">
                            <?php if ($val['odNo']) {?>
                                <?=$val['odNo'];?>(<?=$val['odSno'];?>)
                            <?php } else {?>
                                -
                            <?php }?>
                        </td>
                        <td class="center number">
                            <?php if ($val['type']==1) {?>
                                <?=$val['odName'];?>(<?=$val['odId'];?>)님의 영상이 [<?=$val['state']==1?"<span style='color:#e82727'>제작요청</span>":"<span style='color:#01c12c'>제작완료</span>";?>] 되었습니다.
                            <?php } else {?>
                                <?=$val['odName'];?>(<?=$val['odId'];?>)님이 제휴코드 [<span style="color:#3339f3"><?=$val['code']?></span>] 를 등록하였습니다.
                            <?php }?>
                        </td>
                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['regDt']); ?></td>
                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnDelete">선택 삭제</button>
        </div>
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">

    // 클래스명 js-page-number 셀렉트박스의 이벤트
    var $js_page_number = $('select.js-page-number');
    if ($js_page_number.length > 0) {
        $(document).on('change', 'select.js-page-number', function (e) {
            e.preventDefault();
            var $input = $('input[name=\'pageNum\']');
            console.log($input);
            $input.val($js_page_number.find(':selected').val());
            $('.content-form[method="get"]:eq(0)').submit();
        });
    }

    $(document).ready(function () {

        $('#btnDelete').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 로그가 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개 로그를 정말로 삭제하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'mode\']').val('delete');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './render_log_ps.php');
                    $('#frmList').submit();
                }
            });

        });
    });

</script>