<form id="frmVideo" name="frmVideo" action="render_ps.php" method="post" class="content_form" enctype="multipart/form-data" target="ifrmProcess">
    <input type="hidden" name="videoGoodsNm" class="videoGoodsNm" value="<?=urldecode($goodsNm)?>">
    <input type="hidden" name="getGoodsNo" class="getGoodsNo" value="<?=$getGoodsNo?>">
    <input type="hidden" name="videoMode" class="videoMode" value="<?=$videoMode?>">
    <?php if(gd_isset($getVideoAdmin)){?>
        <input type="hidden" name="goodsType" value="<?=$getVideoAdmin['goodsType']?>">
    <?php }?>

    <div class="page-header js-affix">
        <h3><?php echo end($naviMenu->location); ?></h3>
        <div class="btn-group">
            <input type="button" value="목록" class="btn btn-white btn-icon-list" onclick="goList('./render_list.php?page=1');">
            <input type="submit" value="저장" class="btn btn-red">
        </div>
    </div>


    <div class="table-title gd-help-manual">
        <div class="flo-left">상품 선택</div>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>상품구분</th>
                <?php if(gd_isset($getVideoAdmin)){?>
                    <td>
                        <span><?=$getVideoAdmin['goodsType']=="goods"?"일반상품":"추가상품";?></span>
                    </td>
                <?php } else {?>
                    <td>
                        <input type="radio" name="goodsType" id="goodsType1" value="goods"><label for="goodsType1">일반상품</label>
                        <input type="radio" name="goodsType" id="goodsType2" value="addGoods" disabled=""><label for="goodsType2">추가상품</label>
                    </td>
                <?php }?>
            </tr>
            <tr>
                <th>상품선택</th>
                <?php
                if(gd_isset($getVideoAdmin)){
                    ?>
                    <td><span><?php echo $getVideoAdmin['goodsNm']; ?></span></td>
                    <?php
                } else {
                    ?>
                    <td class="goodsBasic">

                        <select name="" class="getCate" onchange="get_cate_search('getCate', this.value);">
                            <option value="">카테고리를 선택해 주세요</option>
                            <?php
                            foreach ($cateDepth as $key => $val) {
                                echo "<option value='{$val['cateCd']}'>{$val['cateNm']}</option>";
                            }
                            ?>
                        </select>

                        <select name="" class="getCateSub" onchange="get_cate_search('getCateSub', this.value);">
                            <option value="">카테고리를 선택해 주세요</option>
                        </select>

                        <select name="videoGoodsNo" class="videoGoodsNo">
                            <option value="">상품을 선택해 주세요</option>
                            <?php
                            foreach ($videoInfo as $key => $val) {
                                echo "<option value='{$val['goodsNo']}'>{$val['goodsNm']}</option>";
                            }

                            ?>
                        </select>

                    </td>
                    <td class="goodsAdd" style="width:100%;">
                        <select name="videoGoodsAddNo" class="videoAddGoodsNo ">
                            <option value="">상품을 선택해 주세요</option>
                            <?php
                            foreach ($addGoods as $key => $val) {
                                echo "<option value='{$val['addGoodsNo']}'>{$val['goodsNm']}({$val['addGoodsNo']})</option>";
                            }

                            ?>
                        </select>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <th>노출여부</th>
                <td>
                    <input type="radio" name="useYn" id="use1" value="1" <?=$getVideoAdmin['useYn']=="1"?"checked":"";?>><label for="use1">노출함</label>
                    <input type="radio" name="useYn" id="use2" value="0" <?=$getVideoAdmin['useYn']!="1"?"checked":"";?>><label for="use2">노출안함</label>
                </td>
            </tr>

            <tr>
                <th>샘플영상주소<br>(유튜브, 비메오 등)</th>
                <td>
                    <input type="text" name="sampleVideoUrl" class="form-control width-4xl" value="<?=$getVideoAdmin['sampleVideoUrl']?>">
                </td>
            </tr>

            </tbody>
        </table>
    </div>

    <div class="table-title gd-help-manual">
        <div class="flo-left">템플릿 정보</div>
    </div>


    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>템플릿 아이디</th>
                <td><input type="text" name="templateId" class="form-control width-sm" value="<?=$getVideoAdmin['templateId']?>" placeholder="템플릿 아이디"></td>
            </tr>
            <tr>
                <th>템플릿 이름</th>
                <td><input type="text" name="templateName" class="form-control width-sm" value="<?=$getVideoAdmin['templateName']?>" placeholder="템플릿 이름"></td>
            </tr>
            <tr>
                <th>템플릿 경로</th>
                <td><input type="text" name="templatePath" class="form-control width-sm" value="<?=$getVideoAdmin['templatePath']?>" placeholder="템플릿 경로"></td>
            </tr>
            <tr>
                <th>재생시간(초단위)</th>
                <td><input type="text" name="playTime" class="form-control width-sm" value="<?=$getVideoAdmin['playTime']?>" placeholder="재생시간"></td>
            </tr>
            <tr>
                <th>배경음 레이어이름</th>
                <td><input type="text" name="bgmLayerName" class="form-control width-sm" value="<?=$getVideoAdmin['bgmLayerName']?>" placeholder="배경음 레이어이름"></td>
            </tr>
            <tr>
                <th>배경음 이름</th>
                <td><input type="text" name="bgmName" class="form-control width-sm" value="<?=$getVideoAdmin['bgmName']?>" placeholder="배경음 이름"></td>
            </tr>
            <tr>
                <th>배경음 변경여부</th>
                <td>
                    <input type="radio" name="bgmChange" id="bgmChange1" value="Y" <?=$getVideoAdmin['bgmChange']=="Y"?"checked":"";?>><label for="bgmChange1">변경가능</label>
                    <input type="radio" name="bgmChange" id="bgmChange2" value="N" <?=$getVideoAdmin['bgmChange']!="Y"?"checked":"";?>><label for="bgmChange2">변경불가</label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="table-title gd-help-manual">
        <div class="flo-left" style="margin-right:10px;">기본정보입력</div>
        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_basic3();">항목추가</button>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody id="basic">

            <?php foreach($getVideoAdmin['basicInfo'] as $v){?>
                <tr>
                    <th><input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제"></th>
                    <td><input type="text" name="basic[]" class="form-control width-3xl" placeholder="추가할 기본정보를 입력해 주세요." value="<?=$v?>"></td>
                </tr>
            <?php }?>

            </tbody>

            <tr>
                <td colspan="2"><button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_basic3();">항목추가</button></td>
            </tr>

        </table>
    </div>


    <div class="table-title gd-help-manual">
        <div class="flo-left" style="margin-right:10px;">장면정보입력</div>
        <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_scene();">항목추가</button>
    </div>

    <div id="layoutOrderViewOrderInfoModify" class="">
        <table class="table table-cols">
            <colgroup>
                <col class="width-md">
                <col>
            </colgroup>
            <tbody id="scene_area">
            <?php
            if(gd_isset($getGoodsNo)){
                foreach ($getVideoAdmin['sceneInfo'] as $key => $value) {
                    ?>
                    <tr class="scene_box">
                        <th>
                            <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().remove();" value="삭제">
                        </th>
                        <td style="position: relative;">

                            <div style="margin:5px 0;">
                                순서 <input type="text" name="scene[<?=$key?>][seq]" class="form-control width-sm js-number" value="<?=$value['seq']?>" placeholder="순서">
                            </div>

                            <div style="margin:5px 0;">
                                제목 <input type="text" name="scene[<?=$key?>][subject]" class="form-control width-3xl" value="<?=htmlspecialchars($value['subject'])?>"placeholder="장면 제목을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                설명 <input type="text" name="scene[<?=$key?>][help]" class="form-control width-3xl" value="<?=htmlspecialchars($value['help'])?>"placeholder="장면 설명을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                장면시간 <input type="text" name="scene[<?=$key?>][scene_time]" class="form-control width-3xl" value="<?=htmlspecialchars($value['scene_time'])?>"placeholder="장면 노출 시간을 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                노래가사 <input type="text" name="scene[<?=$key?>][bgm_text]" class="form-control width-3xl" value="<?=htmlspecialchars($value['bgm_text'])?>"placeholder="장면 노래 가사를 입력해주세요.">
                            </div>

                            <div style="margin:5px 0;">
                                <span>미리보기</span><br>
                                <input type="checkbox" name="scene[<?=$key?>][preview]" id="preview_<?=$key?>" class="form-control" value="yes" <?=$value['preview']=="yes"?"checked":"";?>> <label for="preview_<?=$key?>">미리보기를 사용할 경우 체크해주세요.</label>
                            </div>

                            <div style="margin:5px 0;">
                                <span>예시 사진</span><br>
                                <?php if($value['dbPath']){?>
                                    <img src ="/<?=$value['dbPath']?>" width="100">
                                <?php }?>
                                <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                                    <input type="hidden" name="scene[<?=$key?>][sample_image_db]" value="<?=$value['dbPath']?>">
                                    <input type="file" name="scene[<?=$key?>][sample_image]" class="form-control input-sm">
                                </div>
                            </div>

                            <div style="margin:5px 0;">
                                <span>합성할 사진</span><br>
                                <?php if($value['dbPathMerge']){?>
                                    <img src ="/<?=$value['dbPathMerge']?>" width="100">
                                <?php }?>
                                <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                                    <input type="hidden" name="scene[<?=$key?>][sample_image_merge_db]" value="<?=$value['dbPathMerge']?>">
                                    <input type="file" name="scene[<?=$key?>][sample_image_merge]" class="form-control input-sm">
                                </div>
                            </div>
                            <!--
                        <div style="margin:5px 0;">
                            <span>합성될 사진</span><br>
                            <?php if($value['dbPath']){?>
                                <img src ="/<?=$value['dbPath']?>" width="100">
                            <?php }?>
                            <div class="bootstrap-filestyle input-group" style="display:inline-block;vertical-align:bottom;">
                                <input type="hidden" name="video[files<?=$k?>]" value="<?=$value['dbPath']?>">
                                <input type="file" name="scene[<?=$key?>][background_image]" class="form-control input-sm">
                            </div>
                        </div>
 -->
                            <div style="margin:5px 0;">
                                <span>장면타입</span><br>
                                <select name="scene[<?=$key?>][type]" class="form-control" onchange="type_change(this, this.value)">
                                    <option value="none" <?=$value['type']=="none"?"selected":""?>>없음</option>
                                    <option value="photo" <?=$value['type']=="photo"?"selected":""?>>사진</option>
                                    <option value="video" <?=$value['type']=="video"?"selected":""?>>영상</option>
                                </select>
                            </div>

                            <div style="margin:5px 0;<?=$value['type']=="none"?"display: none;":""?>" class="layer_name_box">
                                레이어이름 <input type="text" name="scene[<?=$key?>][layer_name]" class="form-control width-sm" value="<?=htmlspecialchars($value['layer_name'])?>"placeholder="레이어이름">
                                구분탭 <input type="text" name="scene[<?=$key?>][tab_name]" class="form-control width-sm" value="<?=htmlspecialchars($value['tab_name'])?>"placeholder="구분탭">
                            </div>

                            <div style="margin:5px 0;<?=$value['type']=="photo"?"":"display: none;"?>" class="photo_box">
                                <table class="table table-cols">
                                    <tr>
                                        <th>이미지 가로</th>
                                        <td><input type="text" name="scene[<?=$key?>][image_width]" class="form-control width-sm js-number" value="<?=$value['image_width']?>" placeholder="이미지 가로"></td>
                                        <th>이미지 세로</th>
                                        <td><input type="text" name="scene[<?=$key?>][image_height]" class="form-control width-sm js-number" value="<?=$value['image_height']?>" placeholder="이미지 세로"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 X좌표</th>
                                        <td><input type="text" name="scene[<?=$key?>][image_x]" class="form-control width-sm js-number" value="<?=$value['image_x']?>" placeholder="이미지 X좌표"></td>
                                        <th>이미지 Y좌표</th>
                                        <td><input type="text" name="scene[<?=$key?>][image_y]" class="form-control width-sm js-number" value="<?=$value['image_y']?>" placeholder="이미지 Y좌표"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌상X 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_1]" class="form-control width-sm js-number" value="<?=$value['start_1']?>" placeholder="좌상X 시작점"></td>
                                        <th>이미지 좌상Y 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_2]" class="form-control width-sm js-number" value="<?=$value['start_2']?>" placeholder="좌상Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우상X 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_3]" class="form-control width-sm js-number" value="<?=$value['start_3']?>" placeholder="우상X 시작점"></td>
                                        <th>이미지 우상Y 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_4]" class="form-control width-sm js-number" value="<?=$value['start_4']?>" placeholder="우상Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우하X 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_5]" class="form-control width-sm js-number" value="<?=$value['start_5']?>" placeholder="우하X 시작점"></td>
                                        <th>이미지 우하Y 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_6]" class="form-control width-sm js-number" value="<?=$value['start_6']?>" placeholder="우하Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌하X 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_7]" class="form-control width-sm js-number" value="<?=$value['start_7']?>" placeholder="좌하X 시작점"></td>
                                        <th>이미지 좌하Y 시작점</th>
                                        <td><input type="text" name="scene[<?=$key?>][start_8]" class="form-control width-sm js-number" value="<?=$value['start_8']?>" placeholder="좌하Y 시작점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌상X 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_1]" class="form-control width-sm js-number" value="<?=$value['end_1']?>" placeholder="좌상X 끝나는점"></td>
                                        <th>이미지 좌상Y 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_2]" class="form-control width-sm js-number" value="<?=$value['end_2']?>" placeholder="좌상Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우상X 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_3]" class="form-control width-sm js-number" value="<?=$value['end_3']?>" placeholder="우상X 끝나는점"></td>
                                        <th>이미지 우상Y 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_4]" class="form-control width-sm js-number" value="<?=$value['end_4']?>" placeholder="우상Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 우하X 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_5]" class="form-control width-sm js-number" value="<?=$value['end_5']?>" placeholder="우하X 끝나는점"></td>
                                        <th>이미지 우하Y 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_6]" class="form-control width-sm js-number" value="<?=$value['end_6']?>" placeholder="우하Y 끝나는점"></td>
                                    </tr>
                                    <tr>
                                        <th>이미지 좌하X 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_7]" class="form-control width-sm js-number" value="<?=$value['end_7']?>" placeholder="좌하X 끝나는점"></td>
                                        <th>이미지 좌하Y 끝나는점</th>
                                        <td><input type="text" name="scene[<?=$key?>][end_8]" class="form-control width-sm js-number" value="<?=$value['end_8']?>" placeholder="좌하Y 끝나는점"></td>
                                    </tr>
                                </table>
                            </div>

                            <div style="margin:5px 0;<?=$value['type']=="video"?"":"display: none;"?>" class="video_box">
                                <table class="table table-cols">
                                    <tr>
                                        <th>영상 가로</th>
                                        <td><input type="text" name="scene[<?=$key?>][video_width]" class="form-control width-sm js-number" value="<?=$value['video_width']?>" placeholder="영상 가로"></td>
                                        <th>영상 세로</th>
                                        <td><input type="text" name="scene[<?=$key?>][video_height]" class="form-control width-sm js-number" value="<?=$value['video_height']?>" placeholder="영상 세로"></td>
                                        <th>영상 재생시간(초단위)</th>
                                        <td><input type="text" name="scene[<?=$key?>][video_time]" class="form-control width-sm js-number" value="<?=$value['video_time']?>" placeholder="재생시간"></td>
                                    </tr>
                                    <tr>
                                        <th>영상 X좌표</th>
                                        <td><input type="text" name="scene[<?=$key?>][video_x]" class="form-control width-sm js-number" value="<?=$value['video_x']?>" placeholder="영상 X좌표"></td>
                                        <th>영상 Y좌표</th>
                                        <td><input type="text" name="scene[<?=$key?>][video_y]" class="form-control width-sm js-number" value="<?=$value['video_y']?>" placeholder="영상 Y좌표"></td>
                                    </tr>
                                </table>
                            </div>

                            <div style="margin:5px 0;">
                                <span>글귀여부</span><br>
                                <select name="scene[<?=$key?>][text_use]" class="form-control" onchange="text_use_change(this, this.value)">
                                    <option value="no" <?=$value['text_use']=="no"?"selected":"";?>>없음</option>
                                    <option value="yes" <?=$value['text_use']=="yes"?"selected":"";?>>있음</option>
                                </select>
                            </div>

                            <div style="margin:5px 0;<?=$value['text_use']=="yes"?"":"display: none;"?>" class="text_box">

                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box(<?=$key?>, 1, this);">단문추가</button>
                                <button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_text_box(<?=$key?>, 2, this);">장문추가</button>
                                <br><br>

                                <div class="text_box_list">
                                    <?php foreach ($value['text_layer_name'] as $k => $v) {?>
                                        <table class="table table-cols">
                                            <tr>
                                                <td rowspan="4">
                                                    <input type="button" class="btn btn-sm btn-white btn-icon-minus" onclick="$(this).parent().parent().parent().parent().remove();" value="삭제">
                                                </td>
                                                <th>레이어이름</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_layer_name][]" class="form-control width-sm" value="<?=htmlspecialchars($value['text_layer_name'][$k])?>"placeholder="레이어이름"></td>
                                                <th>최대 글자수</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_max_length][]" class="form-control width-sm js-number" value="<?=htmlspecialchars($value['text_max_length'][$k])?>"placeholder="최대 글자수"></td>
                                                <th>회전각도</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_rotate][]" class="form-control width-sm" value="<?=htmlspecialchars($value['text_rotate'][$k])?>"placeholder="회전각도"></td>
                                            </tr>
                                            <tr>
                                                <th>텍스트 X좌표</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_x][]" class="form-control width-sm js-number" value="<?=htmlspecialchars($value['text_x'][$k])?>"placeholder="텍스트 X좌표"></td>
                                                <th>텍스트 Y좌표</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_y][]" class="form-control width-sm js-number" value="<?=htmlspecialchars($value['text_y'][$k])?>"placeholder="텍스트 Y좌표"></td>
                                                <th>텍스트 크기</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_size][]" class="form-control width-sm js-number" value="<?=htmlspecialchars($value['text_size'][$k])?>"placeholder="텍스트 크기"></td>
                                            </tr>
                                            <tr>
                                                <th>텍스트 색상</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_color][]" class="form-control width-sm" value="<?=htmlspecialchars($value['text_color'][$k])?>"placeholder="텍스트 색상"></td>
                                                <th>텍스트 종류</th>
                                                <td><input type="text" name="scene[<?=$key?>][text_family][]" class="form-control width-sm" value="<?=htmlspecialchars($value['text_family'][$k])?>"placeholder="텍스트 종류"></td>
                                                <th>텍스트 정렬</th>
                                                <td>
                                                    <select name="scene[<?=$key?>][text_align][]" class="form-control">
                                                        <option value="left" <?=$value['text_align'][$k]=="left"?"selected":"";?>>왼쪽</option>
                                                        <option value="center" <?=$value['text_align'][$k]=="center"?"selected":"";?>>가운데</option>
                                                        <option value="right" <?=$value['text_align'][$k]=="right"?"selected":"";?>>오른쪽</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>글귀 예시</th>
                                                <td colspan="5">
                                                    <?php if ($value['text_type'][$k]=="text") {?>
                                                        <input type="hidden" name="scene[<?=$key?>][text_type][]" value="text">
                                                        <input type="text" name="scene[<?=$key?>][text_sample][]" class="form-control width-4xl" value="<?=htmlspecialchars($value['text_sample'][$k])?>"placeholder="텍스트 색상">
                                                    <?php } else {?>
                                                        <input type="hidden" name="scene[<?=$key?>][text_type][]" value="textarea">
                                                        <textarea style="height:100px;" maxlength="1000" name="scene[<?=$key?>][text_sample][]" class="form-control" placeholder="예시글을 입력해 주세요."><?=htmlspecialchars($value['text_sample'][$k])?></textarea>
                                                    <?php }?>
                                                </td>
                                            </tr>

                                        </table>
                                    <?php }?>

                                    <div>
                                    </div>


                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
            <tr>
                <td colspan="2"><button type="button" class="btn btn-sm btn-white btn-icon-plus" onclick="add_scene();">항목추가</button></td>
            </tr>
        </table>
    </div>

    </div>
</form>

<script type="text/javascript">
    $('.videoGoodsNo').change(function(){
        var videoGoodsNm = $('.videoGoodsNo option:checked').text();
        $('.videoGoodsNm').val(videoGoodsNm);
    });

    $('.videoAddGoodsNo').change(function(){
        var videoGoodsNm = $('.videoAddGoodsNo option:checked').text();
        $('.videoGoodsNm').val(videoGoodsNm);
    });

    function get_cate_search(mode, value) {

        var getCate = value;

        console.log(getCate);

        $.ajax({
            url:"./render_register.php",
            type:'GET',
            data: {'data':getCate, 'mode':mode},
            success:function(data){
                // console.log(JSON.parse(data));
                data = JSON.parse(data);
                // console.log(data.cate);

                $(".getCateSub").html(data.cate);

                $(".videoGoodsNo").html(data.goods);
            },
            error:function(jqXHR, textStatus, errorThrown){
                // alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
                // self.close();
            }
        });
    }

</script>