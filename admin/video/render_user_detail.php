<div class="table-title gd-help-manual">
</div>
<div id="layoutOrderViewOrderInfoModify" class="">
    <table class="table table-cols">
        <colgroup>
            <col class="width-md">
            <col>
        </colgroup>
        <tbody id="scene_area">
        <tr>
            <th>제작번호</th>
            <td><?=$data['createNm']?></td>
        </tr>
        <tr>
            <th>주문번호(상품주문번호)</th>
            <td><?=$data['odNo']?>(<?=$data['odSno']?>)</td>
        </tr>
        <tr>
            <th>주문자(아이디)</th>
            <td><?=$data['odName']?>(<?=$data['odId']?>)</td>
        </tr>
        <tr>
            <th>상품정보</th>
            <td><?=$data['goodsNm']?>(<?=$data['goodsCd']?>)</td>
        </tr>
        <tr>
            <th>기본정보</th>
            <td>
                <table class="table table-cols" style="margin-bottom:0;">
                    <?php foreach ($data['basicInfoAdmin'] as $key=>$val) {?>
                        <tr>
                            <th><?=$val?></th>
                            <td><?=$data['basicInfo'][$key]?></td>
                        </tr>
                    <?php }?>
                </table>
            </td>
        </tr>
        <tr>
            <th>장면정보</th>
            <td>
                <table class="table table-cols" style="margin-bottom:0;">
                    <?php foreach ($data['sceneInfoUser']['scene'] as $key=>$scene) {?>
                        <?php if ($scene['type']!="mp3") {?>
                            <tr>
                                <th><?=$key?>번 장면</th>
                                <td>
                                    <table class="table table-cols" style="margin-bottom:0;">

                                        <tr>
                                            <th>타입</th>
                                            <td>
                                                <?php if ($scene['type']=="photo") {?>
                                                    사진
                                                <?php } else if ($scene['type']=="video") {?>
                                                    비디오
                                                <?php } else {?>
                                                    없음
                                                <?php }?>
                                            </td>
                                        </tr>

                                        <?php if ($scene['file_path']) {
                                            $filePath = explode("/", $scene['file_path']);
                                            ?>
                                            <tr>
                                                <th>저장파일</th>
                                                <td>
                                                    <a href="https://app.aileenstory.com<?=$scene['file_path']?>" target="_blank"><?=$filePath[count($filePath)-1]?></a>
                                                </td>
                                            </tr>
                                        <?php }?>

                                        <?php if ($scene['type']=="video") {?>
                                            <tr>
                                                <th>비디오정보</th>
                                                <td><?=$scene['cut_info']?></td>
                                            </tr>
                                        <?php }?>

                                        <?php if ($scene['text']) {?>
                                            <tr>
                                                <th>텍스트정보</th>
                                                <td>
                                                    <table class="table table-cols" style="margin-bottom:0;">
                                                        <?php foreach ($scene['text'] as $key=>$text) {?>
                                                            <tr>
                                                                <th><?=$key+1?></th>
                                                                <td><?=nl2br($text['text'])?></td>
                                                            </tr>
                                                        <?php }?>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php }?>


                                    </table>

                                </td>
                            </tr>
                        <?php } else {
                            $mp3 = $scene['file_path'];
                        }?>
                    <?php }?>
                </table>
            </td>
        </tr>
        <tr>
            <th>배경음</th>
            <td>
                <?php if ($mp3) {
                    $mp3Path = explode("/", $mp3);
                    ?>
                    <a href="https://app.aileenstory.com<?=$mp3?>" target="_blank"><?=$mp3Path[count($mp3Path)-1]?></a>
                <?php } else {?>
                    기본 배경음
                <?php }?>
            </td>
        </tr>
        <tr>
            <th>시안경로</th>
            <td>
                <?php if ($data['sianPath']!="") {
                    $sianPath = explode("/", $data['sianPath']);
                    ?>
                    <a href="https://app.aileenstory.com<?=$data['sianPath']?>" target="_blank"><?=$sianPath[count($sianPath)-1]?></a>
                <?php }?>
            </td>
        </tr>
        <tr>
            <th>영상경로</th>
            <td>
                <?php if ($data['moviePath']!="") {
                    $moviePath = explode("/", $data['moviePath']);
                    ?>
                    <a href="https://app.aileenstory.com<?=$data['moviePath']?>" target="_blank"><?=$moviePath[count($moviePath)-1]?></a>
                <?php }?>
            </td>
        </tr>
        <tr>
            <th>결제구분</th>
            <td><?=$data['payment']==1?"유료":"무료"?></td>
        </tr>
        <tr>
            <th>상태</th>
            <td><?=$state[$data['state']]?></td>
        </tr>
        <tr>
            <th>등록일</th>
            <td><?=gd_date_format('Y-m-d', $data['regDt']); ?></td>
        </tr>
        <tr>
            <th>수정일</th>
            <td><?=gd_date_format('Y-m-d', $data['modDt']); ?></td>
        </tr>
        </tbody>
    </table>
</div>