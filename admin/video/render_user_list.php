<div class="page-header js-affix">
    <h3><?php echo end($naviMenu->location); ?></h3>
</div>

<div class="table-title">업로드 검색</div>
<form id="frmSearch" method="get" class="js-form-enter-submit content-form">
    <input type="hidden" name="pageNum" value="<?= Request::get()->get('pageNum', 20); ?>"/>
    <div class="search-detail-box">
        <table class="table table-cols">
            <colgroup>
                <col class="width-sm"/>
                <col/>
            </colgroup>
            <tr>
                <th>상태</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="state" value="" <?php if($req['state']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($state as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="state" value="<?php echo $dKey;?>" <?php if($req['state']=="{$dKey}"){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>결제구분</th>
                <td class="form-inline">
                    <label class="radio-inline"><input type="radio" name="payment" value="" <?php if($req['payment']==""){?>checked<?php }?> />전체</label>
                    <?php foreach ($payment as $dKey => $dVal) {?>
                        <label class="radio-inline"><input type="radio" name="payment" value="<?php echo $dKey;?>" <?php if($req['payment']=="{$dKey}"){?>checked<?php }?> /><?php echo $dVal;?></label>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="form-inline">
                        <?=gd_select_box('searchField', 'searchField', array( 'createNm' => '제작번호', 'odNo' => '주문번호', 'odSno' => '상품주문번호', 'odName' => '주문자명', 'odId' => '주문자 아이디'), '', gd_isset($req['searchField'])); ?>
                        <input type="text" name="keyword" value="<?=gd_isset($req['keyword']); ?>" class="form-control"/>
                    </div>
                </td>
            </tr>
        </table>
        <div class="table-btn">
            <input type="submit" value="검색" class="btn btn-lg btn-black">
        </div>
    </div>
</form>



<form id="frmList" action="" method="get" target="ifrmProcess">
    <input type="hidden" name="mode" value="">

    <div class="table-header form-inline">
        <div class="pull-right">
            <?= gd_select_box_by_page_view_count(Request::get()->get('pageNum', 20)); ?>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-rows">
            <thead>
            <tr>
                <!-- 상품리스트 그리드 항목 시작-->
                <!-- <th><input type="checkbox" value="y" class="js-checkall" data-target-name="chk"></th> -->
                <th>번호</th>
                <th>결제구분</th>
                <th>결제횟수</th>
                <th>제작번호</th>
                <th>주문번호(상품주문번호)</th>
                <th>주문자(주문자아이디)</th>
                <th>시안영상</th>
                <th>원본영상</th>
                <th>상태</th>
                <th>등록일</th>
                <th>수정일</th>
                <th>제작가능횟수</th>
                <th>수동렌더</th>
                <th>상세</th>
                <th>삭제</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (gd_isset($data)) {

                foreach ($data as $key => $val) {
                    if ($val['state'] == '4') {
                        $check = "text-myclose";
                    } else {
                        $check = "";
                    }

                    if ($val['state'] == '3') {
                        $tr_style = "color:#01c12c";
                    } else if ($val['state'] == '2') {
                        $tr_style = "color:#3339f3";
                    } else if ($val['state'] == '1') {
                        $tr_style = "color:#e82727";
                    } else {
                        $tr_style = "";
                    }


                    ?>
                    <tr class="<?=$check?>">
                        <!--선택-->
                        <!-- <td class="center"><input type="checkbox" name="chk[]" value="<?=$val['no'] ?>"></td> -->

                        <!--번호-->
                        <td class="center number"><?=number_format($page->idx--);?></td>
                        <td class="center number"><?=$val['payment']>0?"유료":"무료";?></td>
                        <td class="center number">
                            <?php if ($val['payment']>0) {?>
                                <a href="javascript:window.open('./create_pay_list.php?createNm=<?=$val['createNm']?>' ,'create_pay_list', 'width=640, height=480');"><?=$val['payment']?>회</a>
                            <?php }?>
                        </td>
                        <td class="center number"><?=$val['createNm'];?></td>
                        <td class="center number">
                            <?php if ($val['odNo']) {?>
                                <?=$val['odNo'];?>(<?=$val['odSno'];?>)
                            <?php } else {?>
                                -
                            <?php }?>
                        </td>
                        <td class="center number"><?=$val['odName'];?>(<?=$val['odId'];?>)</td>
                        <td class="center number">
                            <?php if ($val['sianPath']!="") { $sianPath = explode("/", $val['sianPath']); ?>
                                <a href="https://app.aileenstory.com<?=$val['sianPath']?>" target="_blank"><?=$sianPath[count($sianPath)-1]?></a>
                            <?php }?>
                        </td>
                        <td class="center number">
                            <?php if ($val['moviePath']!="") { $moviePath = explode("/", $val['moviePath']);?>
                                <a href="https://app.aileenstory.com<?=$val['moviePath']?>" target="_blank"><?=$moviePath[count($moviePath)-1]?></a>
                            <?php }?>
                        </td>
                        <td class="center number"><span style="<?=$tr_style?>"><?=$state[$val['state']];?></span></td>
                        <!--등록일-->
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['regDt']); ?></td>
                        <td class="center date"><?=gd_date_format('Y-m-d H:i:s', $val['modDt']); ?></td>
                        <td class="center number">
                            <?php if ($val['payment']>0) {?>
                                <select id="<?=$val['createNm'];?>_modify" style="display: inline-block; vertical-align: middle; width: 45px !important;" class="form-control"> <option value="2" <?=$val['modifyCount']==2?"selected":"";?>>0회</option>
                                    <option value="1" <?=$val['modifyCount']==1?"selected":"";?>>1회</option>
                                    <option value="0" <?=$val['modifyCount']==0?"selected":"";?>>2회</option>
                                </select>
                                <button type="button" onclick="modifyCount('<?=$val['createNm'];?>');" class="btn btn-white btn-sm">수정</button>
                            <?php } else {?>
                                -
                            <?php }?>
                        </td>
                        <td class="center number">
                            <select id="<?=$val['createNm'];?>_render" style="display: inline-block; vertical-align: middle; width: 50px !important;" class="form-control">
                                <option value="">선택</option>
                                <option value="0">저화질</option>
                                <option value="1">고화질</option>
                            </select>
                            <button type="button" onclick="render('<?=$val['createNm'];?>');" class="btn btn-white btn-sm">진행</button>
                        </td>
                        <td class="center date">
                            <a href="javascript:window.open('./render_user_detail.php?no=<?=$val['no']; ?>' ,'render_user_detail', 'width=1024, height=768');" class="btn btn-white btn-sm">보기</a>
                        </td>
                        <td class="center date">
                            <?php if ($val['state']==0) {?>
                                <button type="button" onclick="deleteData('<?=$val['createNm'];?>');" class="btn btn-white btn-sm">삭제</button>
                            <?php } else {?>
                                -
                            <?php }?>

                        </td>
                    </tr>
                    <?php
                }

            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="table-action">
        <!-- <div class="pull-left">
            <button type="button" class="btn btn-white" id="btnDelete">선택 삭제</button>
        </div> -->
    </div>
</form>
<div class="center"><?=$page->getPage();?></div>

<script type="text/javascript">

    // 클래스명 js-page-number 셀렉트박스의 이벤트
    var $js_page_number = $('select.js-page-number');
    if ($js_page_number.length > 0) {
        $(document).on('change', 'select.js-page-number', function (e) {
            e.preventDefault();
            var $input = $('input[name=\'pageNum\']');
            console.log($input);
            $input.val($js_page_number.find(':selected').val());
            $('.content-form[method="get"]:eq(0)').submit();
        });
    }

    /*
    $(document).ready(function () {

        $('#btnDelete').click(function(){

            var chkCnt = $('input[name*="chk"]:checked').length;

            if (chkCnt == 0) {
                alert('선택된 로그가 없습니다.');
                return;
            }

            dialog_confirm('선택한 ' + chkCnt + '개 로그를 정말로 삭제하시겠습니까?', function (result) {
                if (result) {
                    $('#frmList input[name=\'mode\']').val('delete');
                    $('#frmList').attr('method', 'post');
                    $('#frmList').attr('action', './render_log_ps.php');
                    $('#frmList').submit();
                }
            });

        });
    });
    */

    function modifyCount(createNm){
        var cnt = $('#'+createNm+'_modify option:selected').val();

        var url = "./render_modify_count.php";
        $.post(url, {'createNm':createNm, 'cnt':cnt}, function(data) {
            alert(data);
        });

    }

    function render(createNm){
        var type = $('#'+createNm+'_render option:selected').val();

        if (type==="") {
            alert('렌더링 타입을 선택해 주세요.');
        } else {
            if (type==1) {
                var type_ = "고화질";
            } else {
                var type_ = "저화질";
            }
            dialog_confirm('제작번호 - ' + createNm + ' 의 렌더링을 (' + type_ + ') 수동으로 진행 하시겠습니까?', function (result) {
                if (result) {
                    var url = "./render_render.php"
                    $.post(url, {'createNm':createNm, 'type':type}, function(data) {
                        alert(data);
                    });
                }
            });
        }

    }

    function deleteData(createNm){

        dialog_confirm('선택한 데이터를 정말로 삭제하시겠습니까?', function (result) {
            if (result) {
                var url = "./render_delete.php";
                $.post(url, {'createNm':createNm}, function(data) {

                    var data = JSON.parse(data);

                    if (data.state=="ok") {
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                });
            }
        });
    }

</script>