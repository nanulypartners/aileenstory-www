/*
    ---- 함수 코드 시작 ----
 */


const binaryStringToArrayBuffer = (binary) => {
    const length = binary.length;
    const buf = new ArrayBuffer(length);
    const arr = new Uint8Array(buf);
    for (let i = 0; i < length; i++) {
        arr[i] = binary.charCodeAt(i);
    }
    return buf;
};
const base64StringToBlob = (base64) => {
    const type = base64.match(/data:([^;]+)/)[1];
    base64 = base64.replace(/^[^,]+,/g, '');
    const options = {};
    if (type) {
        options.type = type;
    }
    const binaryArrayBuffer = [binaryStringToArrayBuffer(window.atob(base64))];
    return new Blob(binaryArrayBuffer, options);
};
const getOrientation = (file, callback) => {
    const reader = new FileReader();
    reader.onload = (event) => {
        const view = new DataView(event.target.result);

        if (view.getUint16(0, false) != 0xffd8) return callback(-2);

        const length = view.byteLength;
        let offset = 2;

        while (offset < length) {
            const marker = view.getUint16(offset, false);
            offset += 2;

            if (marker == 0xffe1) {
                if (view.getUint32((offset += 2), false) != 0x45786966) {
                    return callback(-1);
                }
                const little = view.getUint16((offset += 6), false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                const tags = view.getUint16(offset, little);
                offset += 2;

                for (let i = 0; i < tags; i++) if (view.getUint16(offset + i * 12, little) == 0x0112) return callback(view.getUint16(offset + i * 12 + 8, little));
            } else if ((marker & 0xff00) != 0xff00) break;
            else offset += view.getUint16(offset, false);
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
};

const resetOrientation = (srcBase64, srcOrientation, callback) => {
    const _img = new Image();

    _img.onload = () => {
        const width = _img.width;
        const height = _img.height;
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        // set proper canvas dimensions before transform & export
        // console.log('aa' , srcOrientation)
        if (4 < srcOrientation && srcOrientation < 9) {
            canvas.width = height;
            canvas.height = width;
        } else {
            canvas.width = width;
            canvas.height = height;
        }
        // transform context before drawing image
        switch (srcOrientation) {
            case 2:
                ctx.transform(-1, 0, 0, 1, width, 0);
                break;
            case 3:
                ctx.transform(-1, 0, 0, -1, width, height);
                break;
            case 4:
                ctx.transform(1, 0, 0, -1, 0, height);
                break;
            case 5:
                ctx.transform(0, 1, 1, 0, 0, 0);
                break;
            case 6:
                ctx.transform(0, 1, -1, 0, height, 0);
                break;
            case 7:
                ctx.transform(0, -1, -1, 0, height, width);
                break;
            case 8:
                ctx.transform(0, -1, 1, 0, 0, width);
                break;
            default:
                break;
        }

        // draw image
        ctx.drawImage(_img, 0, 0);

        // export base64
        callback(canvas.toDataURL('image/jpeg'));
    };

    _img.src = srcBase64;
};


const ChangeEvent = (imgInput, callback) => {
    imgInput.onchange = (evt) => {
        const files = evt.target.files; // FileList object
        const file = files[0];
        if (file.type.match('image.*')) {
            const _canvas = document.createElement('canvas');
            const _ctx = _canvas.getContext('2d');
            const maxW = 1920;
            const maxH = 1920;
            const reader = new Image();

            getOrientation(file, (orientation) => {
                reader.onload = () => {
                    const iw = reader.width;
                    const ih = reader.height;
                    const _scale = Math.min(maxW / iw, maxH / ih);
                    const iwScaled = iw * _scale;
                    const ihScaled = ih * _scale;
                    _canvas.width = iwScaled;
                    _canvas.height = ihScaled;

                    // const src = base64StringToBlob(reader.result);
                    _ctx.drawImage(reader, 0, 0, iwScaled, ihScaled);
                    resetOrientation(_canvas.toDataURL('image/jpeg', 1), orientation, (resetBase64Image) => {
                        // console.log(resetBase64Image);
                        // const src = window.URL.createObjectURL(base64StringToBlob(resetBase64Image));
                        const src = resetBase64Image;
                        // console.log(src);
                        // console.log(base64StringToBlob(resetBase64Image));
                        let name = file.name.split('.');
                        callback([src, name[1], _canvas.width, _canvas.height]);
                        // this.coverImg.src = src;
                        // this.imgEditor.style.display = 'block';
                    });
                };
                reader.src = URL.createObjectURL(file);
            });
        }
    };
};

const getUrlParams = () => {
    const params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
        params[key] = value;
    });
    return params;
};


/*
    ---- 함수 코드 끝 ----
 */


/*
    ---- 카테고리에 따라 보이는 화면 코드 시작 ----
 */

const oParams = getUrlParams();
const state = {
    category: oParams.category,
    skin: oParams.skin,
    mainImage: document.querySelector('#main_image_box'),
    informationBox: document.querySelector('#information'),
    weddingNameBox: document.querySelector('.wedding_name_box'),
    babyNameBox: document.querySelector('.baby_name_box'),
    plusNameBtn: document.querySelector('.plus_name_btn'),
    galleryBox: document.querySelector('#gallery'),
    videoBox: document.querySelector('#video'),
    thankBox: document.querySelector('.option_box .thank_box'),
    stateDisplay: function () {
        if (this.category === 'wedding') {
            this.babyNameBox.style.display = 'none';
        } else if (this.category === 'baby') {
            this.weddingNameBox.style.display = 'none';
            this.plusNameBtn.style.display = 'none';
        } else if (this.category === 'old') {
            this.mainImage.style.display = 'none';
            this.informationBox.style.display = 'none';
            this.galleryBox.style.display = 'none';
            this.videoBox.style.display = 'none';
            this.thankBox.style.display = 'none';
        }
    }

};


state.stateDisplay();


/*
    ---- 카테고리에 따라 보이는 화면 코드 끝 ----
 */


/*
    == close btn 클릭 ==
 */


// const headerCloseBtn = document.querySelector('.header_close_btn_box');
//
// headerCloseBtn.addEventListener('click', () => {
//     window.close();
// });

/*
    == close btn 클릭 끝 ==
 */


/*
    ---- 초대장주소 코드 시작 ----
 */


const webNameController = {
    input: document.querySelector('#web_name'),
    checkBox: document.querySelector('.name_check_box'),
    customAddress: document.querySelector('.custom_address')
};

webNameController.input.addEventListener('keyup', (e) => {
    if (!(e.keyCode >= 37 && e.keyCode <= 40)) {
        const inputVal = e.target.value;
        e.target.value = inputVal.replace(/[^a-z0-9]/gi, '');
    }
    const defaultAddress = document.querySelector('.default_address');
    const webName = document.querySelector('#web_name').value;
    const orderNo = document.querySelector('#orderNo').value;
    // console.log(webName)


    let params = {web_name:webName, orderNo:orderNo};
    let query = Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&');
    let url = './goods_card_name_check.php?' + query;
    fetch(url, {
        method: 'GET',
    }).then(res => {
        res.text().then(function (text) {
            console.log(text)
            if (text === 'Y') {
                defaultAddress.classList.add('active');
            } else {
                defaultAddress.classList.remove('active');
            }
        });


    }).catch(function (error) {
        console.log(error);
    });


    webNameController.customAddress.textContent = e.target.value;
});


/*
    ---- 초대장주소 코드 끝 ----
 */


/*
    ---- 메인이미지 코드 시작 ----
 */


const mainImageController = {
    input: document.querySelector('#main_img_input'),
    image: document.querySelector('.main_img .main_image_content img'),
    type: document.querySelector('#main_img_type'),
    mainImageModal: document.querySelector('.main_image_modal'),
    modifyBtn: document.querySelector('.main_image_modify_btn'),
    centerImg: document.querySelector('.center_img'),
    sell: document.querySelector('.img_sell_box .sell'),
    imgSave: document.querySelector('.main_image_modal .img_save'),
    reset: document.querySelector('.main_image_modal .reset'),
    rotate: document.querySelector('.main_image_modal .rotate'),
    sizeUp: document.querySelector('.main_image_modal .size_up'),
    sizeDown: document.querySelector('.main_image_modal .size_down'),
    close: document.querySelector('.main_image_modal .close'),
    type2 : null,
    label: document.querySelector('.main_img label'),
    imgBox: document.querySelector('.main_img .img_box'),
};

const centerImg = {
    rotateNumber: 0,
    scale: 1,
};


if (mainImageController.type.value) {
    mainImageController.label.style.display = 'none';
    mainImageController.imgBox.style.display = 'block';
    mainImageController.modifyBtn.style.display = 'block';
}


ChangeEvent(mainImageController.input, (r) => {
    centerImg.scale = 1;
    centerImg.rotateNumber = 0;
    mainImageController.sell.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.sell.style.left = '0';
    mainImageController.sell.style.top = '0';
    mainImageController.centerImg.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.centerImg.style.left = '0';
    mainImageController.centerImg.style.top = '0';
    ImageModalOpen(r[0], r[1]);

    // mainImageController.image.src = r[0];
    // mainImageController.type.value = r[1];
    // coverImage.src = r[0]
    // coverType.value = r[1]
});


function dragElement(elmnt , elmnt2) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

        elmnt2.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt2.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
        console.log("현재 요소의 위치 y는 " + elmnt.style.top + ", x는" + elmnt.style.left + "입니다.");
    }
}

dragElement(mainImageController.sell , mainImageController.centerImg);



function ImageModalOpen(src, type) {
    mainImageController.centerImg.src = src;
    mainImageController.mainImageModal.style.display = 'block';
    mainImageController.type2 = type;
    setTimeout(function(){
        mainImageController.sell.style.height = `${mainImageController.centerImg.clientHeight}px`;
    },100);
}

mainImageController.imgSave.addEventListener('click', () => {
    const centerImgW = document.querySelector('.center_img_box').clientWidth;
    const centerLeft = mainImageController.centerImg.style.left.replace('px','');
    const imgX = Number(centerLeft)/centerImgW*100;

    const centerImgH = document.querySelector('.center_img_box').clientHeight;
    const centerTop = mainImageController.centerImg.style.top.replace('px','');
    const imgY = Number(centerTop)/centerImgH*100;

    mainImageController.type.value = mainImageController.type2;
    mainImageController.mainImageModal.style.display = 'none';
    mainImageController.image.src = mainImageController.centerImg.src;
    mainImageController.image.style.left = `${imgX}%`;
    mainImageController.image.style.top = `${imgY}%`;
    mainImageController.image.style.transform = mainImageController.centerImg.style.transform;

    document.querySelector('#img_left').value = imgX;
    document.querySelector('#img_top').value = imgY;
    document.querySelector('#img_transform').value = mainImageController.centerImg.style.transform;

    mainImageController.label.style.display = 'none';
    mainImageController.imgBox.style.display = 'block';
    mainImageController.modifyBtn.style.display = 'block';
});


mainImageController.reset.addEventListener('click', () => {
    centerImg.scale = 1;
    centerImg.rotateNumber = 0;
    mainImageController.sell.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.centerImg.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
});

mainImageController.rotate.addEventListener('click' , () => {
    centerImg.rotateNumber += 90;
    if (centerImg.rotateNumber === 360) {
        centerImg.rotateNumber = 0;
    }
    mainImageController.sell.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.centerImg.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
});

mainImageController.sizeUp.addEventListener('click', () => {
    centerImg.scale += 0.1;
    mainImageController.sell.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.centerImg.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
});

mainImageController.sizeDown.addEventListener('click', () => {
    centerImg.scale -= 0.1;
    mainImageController.sell.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
    mainImageController.centerImg.style.transform = `scale(${centerImg.scale}) rotate(${centerImg.rotateNumber}deg)`;
});

mainImageController.close.addEventListener('click', () => {
   mainImageController.mainImageModal.style.display = 'none';
});

mainImageController.imgBox.addEventListener('click', () => {
    mainImageController.input.click();
});

mainImageController.modifyBtn.addEventListener('click', () => {
    const src =  mainImageController.image.src;
    const type = mainImageController.type.value;
    ImageModalOpen(src, type);
});


/*
    ---- 메인이미지 코드 끝 ----
 */


/*
    ---- 관계정보 입력 코드 시작 ----
 */


const stateToggleBtn = document.querySelector('#state_toggle_btn');
if (stateToggleBtn) {
    const plusName = document.querySelector('.plus_name');
    stateToggleBtn.addEventListener('change', () => {
        // console.log(stateToggleBtn.checked)
        if (stateToggleBtn.checked) {
            plusName.classList.add('active');
        } else {
            plusName.classList.remove('active');
        }
    });
    if (stateToggleBtn.checked) {
        plusName.classList.add('active');
    }
}

/*
    ---- 관계정보 입력 코드 끝 ----
 */


/*
    ---- 달련관련 코드 시작 ----
 */


const dateInput = document.querySelector('.date_input');
const pickerBox = document.querySelector('.picker_box');
const pickerBack = document.querySelector('.picker_box .back');
dateInput.addEventListener('click', () => {
    pickerBox.classList.add('active');
});

pickerBack.addEventListener('click', () => {
    pickerBox.classList.remove('active');
});

let today = new Date();//오늘 날짜//내 컴퓨터 로컬을 기준으로 today에 Date 객체를 넣어줌
let date = new Date();//today의 Date를 세어주는 역할

const prevCalendar = () => {//이전 달
    // 이전 달을 today에 값을 저장하고 달력에 today를 넣어줌
    //today.getFullYear() 현재 년도//today.getMonth() 월  //today.getDate() 일
    //getMonth()는 현재 달을 받아 오므로 이전달을 출력하려면 -1을 해줘야함
    today = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
    // buildCalendar(); //달력 cell 만들어 출력

    if (today.getMonth() + 1 < 10) {
        setCalendarData(today.getFullYear(), "0" + (today.getMonth() + 1));
    } else {
        setCalendarData(today.getFullYear(), "" + (today.getMonth() + 1));
    }
};

const nextCalendar = () => {//다음 달
    // 다음 달을 today에 값을 저장하고 달력에 today 넣어줌
    //today.getFullYear() 현재 년도//today.getMonth() 월  //today.getDate() 일
    //getMonth()는 현재 달을 받아 오므로 다음달을 출력하려면 +1을 해줘야함
    today = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
    // buildCalendar();//달력 cell 만들어 출력
    if (today.getMonth() + 1 < 10) {
        setCalendarData(today.getFullYear(), "0" + (today.getMonth() + 1));
    } else {
        setCalendarData(today.getFullYear(), "" + (today.getMonth() + 1));
    }
};


const setCalendarData = (year, month) => {
    const tbCalendarYM = document.getElementById("tbCalendarYM");
    //테이블에 정확한 날짜 찍는 변수
    //innerHTML : js 언어를 HTML의 권장 표준 언어로 바꾼다
    //new를 찍지 않아서 month는 +1을 더해줘야 한다.
    tbCalendarYM.innerHTML = today.getFullYear() + "년 " + (today.getMonth() + 1) + "월";

    //빈 문자열을 만들어줍니다.
    let calHtml = "";
    //getMonth(): Get the month as a number (0-11)
    //month 인자는 getMonth로 구한 결과 값에 1을 더한 상태이므로
    //다시 1을 뺀 값을 Date 객체의 인자로 넘겨줍니다.
    //그러면 오늘 날짜의 Date 객체가 반환됩니다.
    const setDate = new Date(year, month - 1, 1);

    //getDate(): Get the day as a number (1-31)
    //이번 달의 첫째 날을 구합니다.
    const firstDay = setDate.getDate();

    //getDay(): Get the weekday as a number (0-6)
    //이번 달의 처음 요일을 구합니다.
    const firstDayName = setDate.getDay();

    //new Date(today.getFullYear(), today.getMonth(), 0);
    //Date객체의 day 인자에 0을 넘기면 지난달의 마지막 날이 반환됩니다.
    //new Date(today.getFullYear(), today.getMonth(), 1);
    //Date객체의 day 인자에 1을 넘기면 이번달 첫째 날이 반환됩니다.
    //이번 달의 마지막 날을 구합니다.

    const lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
    //지난 달의 마지막 날을 구합니다.
    const prevLastDay = new Date(today.getFullYear(), today.getMonth(), 0).getDate();

    //매월 일수가 달라지므로 이번 달 날짜 개수를 세기 위한 변수를 만들고 초기화 합니다.
    let startDayCount = 1;
    let lastDayCount = 1;

    //1~6주차를 위해 6번 반복합니다.
    for (let i = 0; i < 6; i++) {
        //일요일~토요일을 위해 7번 반복합니다.
        for (let j = 0; j < 7; j++) {
            // i == 0: 1주차일 때
            // j < firstDayName: 이번 달 시작 요일 이전 일 때
            if (i === 0 && j < firstDayName) {
                //일요일일 때, 토요일일 때, 나머지 요일 일 때
                if (j === 0) {
                    calHtml += `<div class="not_click"><span>${(prevLastDay - (firstDayName - 1) + j)}</span><span></span></div>`;
                } else if (j === 6) {
                    calHtml += `<div class="not_click"><span>${(prevLastDay - (firstDayName - 1) + j)}</span><span></span></div>`;
                } else {
                    calHtml += `<div class="not_click"><span>${(prevLastDay - (firstDayName - 1) + j)}</span><span></span></div>`;
                }
            }
                // i == 0: 1주차일 때
            // j == firstDayName: 이번 달 시작 요일일 때
            else if (i === 0 && j === firstDayName) {
                //일요일일 때, 토요일일 때, 나머지 요일 일 때
                if (j === 0) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else if (j === 6) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                }
            }
                // i == 0: 1주차일 때
            // j > firstDayName: 이번 달 시작 요일 이후 일 때
            else if (i === 0 && j > firstDayName) {
                //일요일일 때, 토요일일 때, 나머지 요일 일 때
                if (j === 0) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else if (j === 6) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                }
            }
            // startDayCount <= lastDay: 이번 달의 마지막 날이거나 이전일 때
            else if (i > 0 && startDayCount <= lastDay) {
                //일요일일 때, 토요일일 때, 나머지 요일 일 때
                if (j === 0) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else if (j === 6) {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                } else {
                    calHtml += `<div><span>${startDayCount}</span><span class="day">${year}-${month}-${setFixDayCount(startDayCount++)}</span></div>`;
                }
            }
            // startDayCount > lastDay: 이번 달의 마지막 날 이후일 때
            else if (startDayCount > lastDay) {
                if (j === 0) {
                    calHtml += `<div class="not_click"><span>${lastDayCount++}</span><span></span></div>`;
                } else if (j === 6) {
                    calHtml += `<div class="not_click"><span>${lastDayCount++}</span><span></span></div>`;
                } else {
                    calHtml += `<div class="not_click"><span>${lastDayCount++}</span><span></span></div>`;
                }
            }
        }
    }
    const calendarBody = document.querySelector("#calendar .calendar_body");
    const divs = document.querySelectorAll("#calendar .calendar_body div");
    divs.forEach((e) => calendarBody.removeChild(e));
    calendarBody.insertAdjacentHTML("beforeend", calHtml);

    // 날짜를 클릭했을때 코드
    const days = document.querySelectorAll("#calendar .calendar_body div");
    const dateInput = document.querySelector('.date_input');
    days.forEach((e, i) => {
        e.addEventListener('click', (t) => {
            const target = t.currentTarget;
            dateInput.value = target.lastChild.textContent;
            pickerBox.classList.remove('active');
        });
    });

};


const setFixDayCount = number => {
    //캘린더 하루마다 아이디를 만들어주기 위해 사용
    let fixNum = "";
    if (number < 10) {
        fixNum = "0" + number;
    } else {
        fixNum = number;
    }
    return fixNum;
};

if (today.getMonth() + 1 < 10) {
    setCalendarData(today.getFullYear(), "0" + (today.getMonth() + 1));
} else {
    setCalendarData(today.getFullYear(), "" + (today.getMonth() + 1));
}


const buildCalendar = () => {//현재 달 달력 만들기
    const doMonth = new Date(today.getFullYear(), today.getMonth(), 1);
    //이번 달의 첫째 날,
    //new를 쓰는 이유 : new를 쓰면 이번달의 로컬 월을 정확하게 받아온다.
    //new를 쓰지 않았을때 이번달을 받아오려면 +1을 해줘야한다.
    //왜냐면 getMonth()는 0~11을 반환하기 때문
    const lastDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    //이번 달의 마지막 날
    //new를 써주면 정확한 월을 가져옴, getMonth()+1을 해주면 다음달로 넘어가는데
    //day를 1부터 시작하는게 아니라 0부터 시작하기 때문에
    //대로 된 다음달 시작일(1일)은 못가져오고 1 전인 0, 즉 전달 마지막일 을 가져오게 된다
    const tbCalendar = document.getElementById("calendar");
    //날짜를 찍을 테이블 변수 만듬, 일 까지 다 찍힘
    const tbCalendarYM = document.getElementById("tbCalendarYM");
    //테이블에 정확한 날짜 찍는 변수
    //innerHTML : js 언어를 HTML의 권장 표준 언어로 바꾼다
    //new를 찍지 않아서 month는 +1을 더해줘야 한다.
    tbCalendarYM.innerHTML = today.getFullYear() + "년 " + (today.getMonth() + 1) + "월";

    /*while은 이번달이 끝나면 다음달로 넘겨주는 역할*/
    while (tbCalendar.rows.length > 2) {
        //열을 지워줌
        //기본 열 크기는 body 부분에서 2로 고정되어 있다.
        tbCalendar.deleteRow(tbCalendar.rows.length - 1);
        //테이블의 tr 갯수 만큼의 열 묶음은 -1칸 해줘야지
        //30일 이후로 담을달에 순서대로 열이 계속 이어진다.
    }
    let row = null;
    row = tbCalendar.insertRow();
    //테이블에 새로운 열 삽입//즉, 초기화
    let cnt = 0;// count, 셀의 갯수를 세어주는 역할
    // 1일이 시작되는 칸을 맞추어 줌
    let cell;

    for (let i = 0; i < doMonth.getDay(); i++) {
        /*이번달의 day만큼 돌림*/
        cell = row.insertCell();//열 한칸한칸 계속 만들어주는 역할
        cnt = cnt + 1;//열의 갯수를 계속 다음으로 위치하게 해주는 역할
    }
    /*달력 출력*/
    for (let i = 1; i <= lastDate.getDate(); i++) {
        //1일부터 마지막 일까지 돌림
        cell = row.insertCell();//열 한칸한칸 계속 만들어주는 역할
        cell.innerHTML = i;//셀을 1부터 마지막 day까지 HTML 문법에 넣어줌
        cnt = cnt + 1;//열의 갯수를 계속 다음으로 위치하게 해주는 역할
        if (cnt % 7 === 1) {/*일요일 계산*/
            //1주일이 7일 이므로 일요일 구하기
            //월화수목금토일을 7로 나눴을때 나머지가 1이면 cnt가 1번째에 위치함을 의미한다
            cell.innerHTML = `<span>${i}</span>`;
            //1번째의 cell에만 색칠
        }
        if (cnt % 7 === 0) {/* 1주일이 7일 이므로 토요일 구하기*/
            //월화수목금토일을 7로 나눴을때 나머지가 0이면 cnt가 7번째에 위치함을 의미한다
            cell.innerHTML = `<span>${i}</span>`;
            //7번째의 cell에만 색칠
            row = calendar.insertRow();
            //토요일 다음에 올 셀을 추가
        }
        /*오늘의 날짜에 노란색 칠하기*/
        if (today.getFullYear() === date.getFullYear() && today.getMonth() === date.getMonth() && i === date.getDate()) {
            //달력에 있는 년,달과 내 컴퓨터의 로컬 년,달이 같고, 일이 오늘의 일과 같으면
            // cell.classList.add('today');//셀의 배경색을 노랑으로
        }
    }
    // 날짜를 클릭했을때 코드
    const days = document.querySelectorAll('#calendar tbody td');
    days.forEach((e, i) => {
        e.addEventListener('click', () => {
            for (var i = 0; i < invitation_modal_txt.length; i++) {
                invitation_modal_txt[i].style.border = '1px solid #e9e9e9';
            }
            var txt = Etxt.currentTarget;
            txt.style.border = '1px solid #ff9900';
            invitation_select_txt = txt.textContent;
        });
    });

};



const dayInputBox = document.querySelector('.day_input_box .day_input');
const dayModal = document.querySelector('.day_modal');
const dayModalBack = document.querySelector('.day_modal .back');

dayInputBox.addEventListener('click', function(){
    dayModal.classList.add('active');
});

dayModalBack.addEventListener('click', function(){
    dayModal.classList.remove('active');
});

const dayModalController = {
    hour: document.querySelector('#hour'),
    minute: document.querySelector('#minute'),
    apm: document.getElementsByName("card[date][apm]"),
    modalHeader: document.querySelector('.day_modal .header p'),
    dayCreate() {
        const APM = this.apm[0].checked ? 'AM' : 'PM';
        let HOUR = this.hour.value;
        let MIMUTE = this.minute.value;
        if (HOUR === '') {
            HOUR = '00';
        }
        if (MIMUTE === '') {
            MIMUTE = '00';
        }
        this.modalHeader.textContent = `${APM} ${HOUR}:${MIMUTE}`;
        dayInputBox.value = `${APM} ${HOUR}:${MIMUTE}`;
    },
    dayChange() {
        const THIS = this;
        this.hour.addEventListener('input', function(){
            THIS.dayCreate();
        });
        this.minute.addEventListener('input', function(){
            THIS.dayCreate();
        });
        this.apm[0].addEventListener('input', function(){
            THIS.dayCreate();
        });
        this.apm[1].addEventListener('input', function(){
            THIS.dayCreate();
        });
    }
};

dayModalController.dayCreate();

dayModalController.dayChange();


// function dayCreate() {
//     const hour = document.querySelector('#hour').value;
//     const minute = document.querySelector('#minute').value;
//     let apm = document.getElementsByName("card[date][apm]");
//     const modalHeader = document.querySelector('.day_modal .header p');
//
//     apm = apm[0].checked ? 'AM' : 'PM';
//
//     modalHeader.textContent = `${apm} ${hour} : ${minute}`;
//
// }





/*
    ---- 달련관련 코드 끝 ----
 */


/*
    ---- 초대글 관련 코드 시작 ----
 */


const invitationPreviewBtn = document.querySelector('.invitation_preview_btn');
const invitationModal = document.querySelector('.invitation_modal');
const invitationCloseBtn = document.querySelector('.invitation_modal_close_btn');
const invitationSaveBtn = document.querySelector('.invitation_modal_save_btn');
const invitationLi = document.querySelectorAll('.invitation_modal .modal_content li');
const invitationData = document.querySelector('#invitation_data');
let invitationSelectTxt = '';

invitationPreviewBtn.addEventListener('click', () => {
    invitationModal.style.display = 'flex';
});

invitationCloseBtn.addEventListener('click', () => {
    invitationModal.style.display = 'none';
});


invitationLi.forEach((target, index) => {
    target.addEventListener('click', (e) => {
        for (let i = 0; i < invitationLi.length; i++) {
            invitationLi[i].style.border = '1px solid #e9e9e9';
        }
        const txt = e.currentTarget;
        txt.style.border = '1px solid #ff9900';
        invitationSelectTxt = txt.textContent;
    });
});

invitationSaveBtn.addEventListener('click', () => {
    invitationData.value = invitationSelectTxt;
    invitationModal.style.display = 'none';
});


/*
    ---- 초대글 관련 코드 끝 ----
 */


const location_map = document.querySelector('#location');
let lat = document.getElementById('lat').value;
let lng = document.getElementById('lng').value;
let road = document.getElementById('road');
let autocomplete;
let map;

function kakao_map_init() {
    lat = document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    const container = document.getElementById('daum_map_box');
    const options = {
        center: new kakao.maps.LatLng(lat, lng),
        level: 3
    };
    const daum_map_box = new kakao.maps.Map(container, options);
    // 마커가 표시될 위치입니다
    const markerPosition = new kakao.maps.LatLng(lat, lng);
    // 마커를 생성합니다
    const marker = new kakao.maps.Marker({
        position: markerPosition
    });
    // 마커가 지도 위에 표시되도록 설정합니다
    marker.setMap(daum_map_box);
    daum_map_box.setZoomable();
    daum_map_box.setDraggable();

    //주소-좌표 변환 객체를 생성
    const geocoder = new daum.maps.services.Geocoder();
}

kakao_map_init()

const map_input = document.querySelector('#map_input');
const naver_map_box = document.getElementById("naver_map_box");
map_input.addEventListener('click', function () {
    daum_search();
});

function daum_search() {
    new daum.Postcode({
        oncomplete: function (data) {
            const addr = data.address; // 최종 주소 변수
            // 주소 정보를 해당 필드에 넣는다.
            map_input.value = addr;
            road.value = addr;
            // 주소로 상세 정보를 검색
            const geocoder = new daum.maps.services.Geocoder();
            geocoder.addressSearch(data.address, function (results, status) {
                //정상적으로 검색이 완료됐으면
                if (status === daum.maps.services.Status.OK) {
                    var result = results[0]; //첫번째 결과의 값을 활용
                    // 해당 주소에 대한 좌표를 받아서
                    var coords = new daum.maps.LatLng(result.y, result.x);


                    const point = new naver.maps.Point(result.x, result.y);
                    naverMap.setCenter(point);
                    naverMarker.setPosition(point);

                    document.getElementById('lat').value = result.y;
                    document.getElementById('lng').value = result.x;
                    kakao_map_init();
                    initMap();
                }
            });
        }
    }).open();
}


let naverMap = null;
let naverMarker = null

function initMap() {
    lat = document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    naverMap = new naver.maps.Map('naver_map_box', {
        center: new naver.maps.LatLng(lat, lng),
        zoom: 17,
        draggable: false,
        pinchZoom: false,
        scrollWheel: false,
        keyboardShortcuts: false,
        disableDoubleTapZoom: true,
        disableDoubleClickZoom: true,
        disableTwoFingerTapZoom: true
    });

    naverMarker = new naver.maps.Marker({
        position: new naver.maps.LatLng(lat, lng),
        map: naverMap
    });
}


const google_btn = document.querySelector('.google_btn');
const daum_btn = document.querySelector('.daum_btn');


google_btn.addEventListener('click', function () {
    document.getElementById('map_type').value = 'google';
    location_map.classList.add('Ac_naver');
    location_map.classList.remove('Ac_daum');
    initMap();
});

daum_btn.addEventListener('click', function () {
    document.getElementById('map_type').value = 'daum';
    location_map.classList.add('Ac_daum');
    location_map.classList.remove('Ac_naver');
    kakao_map_init();
});


// 추가 교통정보 관련 스크립트

const trafficToggleBtn = document.querySelector('.location #location_option');

if (trafficToggleBtn.checked) {
    location_map.classList.add('active');
}

trafficToggleBtn.addEventListener('change', function () {
    if (trafficToggleBtn.checked) {
        location_map.classList.add('active');
    } else {
        location_map.classList.remove('active');
    }
});


// 추가 교통정보 관련 스크립트 끝


// 갤러리 관련 코드 시작

const photoBoxUl = document.querySelector('.photo_box_ul');
const createPhotoBox = document.querySelector('#photo .create_photo_box');
const createPhotoInput = document.querySelector('#create_photo_input');

createPhotoInput.addEventListener('change', function(e){
    // console.log(e.target.files)
    for (let i = 0; i < e.target.files.length; i += 1) {
        const file = e.target.files[i];

        if (file.type.match('image.*')) {
            const _canvas = document.createElement('canvas');
            const _ctx = _canvas.getContext('2d');
            const maxW = 1920;
            const maxH = 1920;
            const reader = new Image();

            getOrientation(file, (orientation) => {
                reader.onload = () => {
                    const iw = reader.width;
                    const ih = reader.height;
                    const _scale = Math.min(maxW / iw, maxH / ih);
                    const iwScaled = iw * _scale;
                    const ihScaled = ih * _scale;
                    _canvas.width = iwScaled;
                    _canvas.height = ihScaled;

                    // const src = base64StringToBlob(reader.result);
                    _ctx.drawImage(reader, 0, 0, iwScaled, ihScaled);
                    resetOrientation(_canvas.toDataURL('image/jpeg', 1), orientation, (resetBase64Image) => {
                        const src = resetBase64Image;
                        let name = file.name.split('.');
                        const detailPhotoBox = document.querySelectorAll('.detail_photo_box');
                        if (detailPhotoBox.length >= 15) {
                            return false;
                        }

                        createPhotoLi(src, name, _canvas.width);
                        photoMax();
                    });
                };
                reader.src = URL.createObjectURL(file);
            });
        }
    };


});

// ChangeEvent(createPhotoInput, (r) => {
//     console.log(r)
//     const detailPhotoBox = document.querySelectorAll('.detail_photo_box');
//     if (detailPhotoBox.length > 15) {
//         alert('사진은 15장이 최대입니다.');
//     } else {
//         createPhotoLi(r[0], r[2], r[3]);
//         photoMax();
//     }
// });


function createPhotoLi(src, sizeX, sizeY) {
    const detailPhotoLi = document.createElement('li');
    detailPhotoLi.classList.add('detail_photo_box', 'draggable');
    const attr = document.createAttribute('draggable');
    attr.value = 'true';
    detailPhotoLi.setAttributeNode(attr);
    addEventsDragAndDrop(detailPhotoLi);
    const detailPhotoImg = document.createElement('img');
    detailPhotoImg.src = src;
    const detailPhotoRemove = document.createElement('button');
    detailPhotoRemove.classList.add('detailPhotoRemove');
    const detailPhotoRemoveIcon = document.createElement('i');
    detailPhotoRemoveIcon.classList.add('fas', 'fa-times-circle', 'fa-2x', 'pohto_remove');
    createPhotoBox.insertAdjacentElement('beforebegin', detailPhotoLi);

    detailPhotoLi.appendChild(detailPhotoImg);
    detailPhotoLi.appendChild(detailPhotoRemove);
    detailPhotoRemove.appendChild(detailPhotoRemoveIcon);

}

document.querySelector('body').addEventListener('click', function (event) {
    if (event.target.classList.contains('pohto_remove')) {
        // console.log(event.target.parentNode.parentNode)
        event.target.parentNode.parentNode.remove();
        photoMax();
    }
});

function photoMax() {
    const detailPhotoBox = document.querySelectorAll('.detail_photo_box');
    if (detailPhotoBox.length >= 15) {
        createPhotoBox.classList.add('hidden_label');
    } else {
        createPhotoBox.classList.remove('hidden_label');
    }
}

photoMax();


// 갤러리 관련 코드 끝


// 포트 드래그 앤 드랍 코드

let dragSrcEl;

function dragStart(e) {
    this.style.opacity = '0.4';
    dragSrcEl = this;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.innerHTML);
}

// function dragEnter(e) {
//     this.classList.add('over');
// }

// function dragLeave(e) {
//     e.stopPropagation();
//     this.classList.remove('over');
// }

function dragOver(e) {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
    return false;
}

function dragDrop(e) {
    if (dragSrcEl != this) {
        dragSrcEl.innerHTML = this.innerHTML;
        this.innerHTML = e.dataTransfer.getData('text/html');
    }
    const listItens = document.querySelectorAll('.draggable');
    [].forEach.call(listItens, function (item) {
        item.style.opacity = '1';
    });
    return false;
}

function dragEnd(e) {
    const listItens = document.querySelectorAll('.draggable');
    [].forEach.call(listItens, function (item) {
        item.style.opacity = '1';
    });
}

function addEventsDragAndDrop(el) {
    el.addEventListener('dragstart', dragStart, false);
    // el.addEventListener('dragenter', dragEnter, false);
    el.addEventListener('dragover', dragOver, false);
    // el.addEventListener('dragleave', dragLeave, false);
    el.addEventListener('drop', dragDrop, false);
    el.addEventListener('dragend', dragEnd, false);
}

const listItens = document.querySelectorAll('.draggable');
[].forEach.call(listItens, function (item) {
    addEventsDragAndDrop(item);
});


// 포트 드래그 앤 드랍 코드 끝


// 동영상 등록 관련 코드 시작


const videoToggleBtn = document.querySelector('#video_toggle_btn');
const videoInputBox = document.querySelector('.video_input');
const videoLoadingBox = document.querySelector('.video_input .loading_box');

// if (videoToggleBtn.checked) {
//     videoInputBox.classList.add('active');
// }

videoToggleBtn.addEventListener('change', function () {
    if (videoToggleBtn.checked) {
        videoInputBox.classList.add('active');
    } else {
        videoInputBox.classList.remove('active');
    }
});

const videoFile = document.querySelector('#video_file');
videoFile.addEventListener('change', (e) => {
    videoLoadingBox.style.display = 'flex';
    console.log(e.target.files)
    const formData = new FormData(); // Currently empty
    formData.append('type', 'vimeo_upload');
    formData.append('file', e.target.files[0]);
    formData.append('pw', 'nanuly2020!');
    formData.append('orderNo', document.querySelector('#orderNo').value);
    formData.append('odName',  document.querySelector('#odName').value);
    formData.append('odMemNo',  document.querySelector('#odMemNo').value);
    formData.append('category', document.querySelector('#theme').value);
    formData.append('skin', document.querySelector('#skin').value);

    const $url = 'https://app.aileenstory.com/api/mobilecard.php';

    fetch($url, {
        method: 'POST',
        headers: {
            // "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        },
        body: formData
    }).then(res => {
        res.json().then(data => {
            console.log(data);
            videoLoadingBox.style.display = 'none';
            videoInputBox.classList.remove('active');
            videoCreate(data.link, data.uri);
        }); // 텍스트 출력

    }).catch(function (error) {
        console.log(error);
        videoLoadingBox.style.display = 'none';
    });
});



const videoContainer = document.querySelector('.video_container');

function videoCreate(dataLink, dataUri) {
    const videoPlayBox = document.createElement('div');
    const videoPlayCloseBtn = document.createElement('button');
    const videoPlayCloseLine1 = document.createElement('span');
    const videoPlayCloseLine2 = document.createElement('span');
    const iframeBox = document.createElement('div');
    const iframe = document.createElement('iframe');

    const uri = document.querySelector('#uri');

    videoPlayBox.classList.add('video_play_box');
    videoPlayCloseBtn.classList.add('video_play_close_btn');
    videoPlayCloseLine1.classList.add('line1');
    videoPlayCloseLine2.classList.add('line2');
    iframeBox.classList.add('iframe_box');
    uri.value = dataUri;
    iframe.src = `https://player.vimeo.com/video/${dataUri.split('/')[2]}`;

    videoPlayCloseBtn.setAttribute('type', 'button');

    videoPlayCloseBtn.addEventListener('click', () => {
        videoRemove(uri.value);
    });

    videoPlayBox.append(videoPlayCloseBtn, iframeBox);
    videoPlayCloseBtn.append(videoPlayCloseLine1, videoPlayCloseLine2);
    iframeBox.append(iframe);
    videoContainer.append(videoPlayBox);


}

function videoRemove(uri) {
    const videoPlayBox = document.querySelector('.video_play_box');
    // videoPlayBox.remove();
    // videoInputBox.classList.add('active');

    const formData = new FormData(); // Currently empty
    formData.append('type', 'vimeo_delete');
    formData.append('uri', uri);
    formData.append('pw', 'nanuly2020!');
    formData.append('orderNo', document.querySelector('#orderNo').value);
    const $url = 'https://app.aileenstory.com/api/mobilecard.php';
    fetch($url, {
        method: 'POST',
        headers: {
            // "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        },
        body: formData
    }).then(res => {
        document.querySelector('#uri').value = '';
        videoPlayBox.remove();
        videoInputBox.classList.add('active');
        // res.json().then(data => {
        //     console.log(data);
        //
        // }); // 텍스트 출력

    }).catch(function (error) {
        console.log(error);

    });
}

document.querySelector('body').addEventListener('click', function (event) {
    if (event.target.classList.contains('video_play_close_btn')) {
        const uri = document.querySelector('#uri').value
        videoRemove(uri);
    }
});


// 동영상 등록 관련 코드 끝


const saveBtn = document.querySelector('.card_save');
const saveBtn2 = document.querySelector('.card_save2');
const saveBtn3 = document.querySelector('.card_save3');
const saveLoadingBox = document.querySelector('.save_loading_box');
// const freeViewImge = document.querySelector('.cover_box .free_view');
// const freeIframe = document.querySelector('.cover_box .free_iframe');

// if (document.querySelector('#web_name').value !== '') {
//     freeViewImge.style.display = 'none';
//     freeIframe.style.display = 'block';
//     freeIframe.src = `https://app.aileenstory.com/${document.querySelector('#web_name').value}`;
// }

saveBtn.addEventListener('click', (e) => {
    CardAjax(res => {
        saveLoadingBox.style.display = 'none';
        // freeViewImge.style.display = 'none';
        // freeIframe.style.display = 'block';
        // freeIframe.src = res;
    });
});

saveBtn2.addEventListener('click', () => {
    CardAjax(res => {
        const answer = window.confirm("초대장 생성이 완료되었습니다.\n생성된 초대장을 확인하시겠습니까?")
        if (answer) {
            //some code
            location.href = `https://app.aileenstory.com/${document.querySelector('#web_name').value}`;
        }
    });
});


const freeViewBox = document.querySelector('.free_view_box');
const freeIframe = document.querySelector('.free_view_box .free_iframe');
const frreViewClose = document.querySelector('.free_view_box .modal_close_btn')



saveBtn3.addEventListener('click', function(){
    CardAjax(res => {
        saveLoadingBox.style.display = 'none';
        freeViewBox.style.display = 'block';
        freeIframe.src = `https://app.aileenstory.com/${document.querySelector('#web_name').value}`;
        // location.href = `https://app.aileenstory.com/${document.querySelector('#web_name').value}`;
    });
});

frreViewClose.addEventListener('click', function(){
    freeViewBox.style.display = 'none';
    freeIframe.src = '';
});


function CardAjax(callback) {
    const datepicker = document.querySelector('#datepicker').value;
    const hour = document.querySelector('#hour').value;
    const minute = document.querySelector('#minute').value;
    // let apm = document.getElementsByName("apm");
    // apm = apm[0].checked ? 'am' : 'pm';

    const saveData = {
        orderNo: document.querySelector('#orderNo').value,
        skin: document.querySelector('#skin').value,
        category: document.querySelector('#theme').value,
        cover_url: '',
        card: {
            it_en_name: document.querySelector('#skin').value,
            it_cate_name: document.querySelector('#theme').value,
            title: document.querySelector('#title_name').value,
            cardId: document.querySelector('#web_name').value,
            cover_img: {
                name: 'cover',
                type: document.querySelector('#main_img_type').value,
            },
            photo: {
                photoData: [],
            },
            state: {
                groom_name: document.querySelector('#groom_name') ? document.querySelector('#groom_name').value : null,
                groom_phone: document.querySelector('#groom_phone') ? document.querySelector('#groom_phone').value : null,
                priest_name: document.querySelector('#priest_name') ? document.querySelector('#priest_name').value : null,
                priest_phone: document.querySelector('#priest_phone') ? document.querySelector('#priest_phone').value : null,
                mom_name: document.querySelector('#mom_name') ? document.querySelector('#mom_name').value : null,
                mom_phone: document.querySelector('#mom_phone') ? document.querySelector('#mom_phone').value : null,
                dad_name: document.querySelector('#dad_name') ? document.querySelector('#dad_name').value : null,
                dad_phone: document.querySelector('#dad_phone') ? document.querySelector('#dad_phone').value : null,
                groom_dad_name: document.querySelector('#groom_dad_name') ? document.querySelector('#groom_dad_name').value : null,
                groom_dad_phone: document.querySelector('#groom_dad_phone') ? document.querySelector('#groom_dad_phone').value : null,
                groom_mom_name: document.querySelector('#groom_mom_name') ? document.querySelector('#groom_mom_name').value : null,
                groom_mom_phone: document.querySelector('#groom_mom_phone').value,
                priest_dad_name: document.querySelector('#groom_mom_phone') ? document.querySelector('#priest_dad_name').value : null,
                priest_dad_phone: document.querySelector('#priest_dad_phone') ? document.querySelector('#priest_dad_phone').value : null,
                priest_mom_name: document.querySelector('#priest_mom_name') ? document.querySelector('#priest_mom_name').value : null,
                priest_mom_phone: document.querySelector('#priest_mom_phone') ? document.querySelector('#priest_mom_phone').value : null,
            },
            plusState: document.querySelector('#state_toggle_btn') ? document.querySelector('#state_toggle_btn').checked : null,
            invitation: document.querySelector('#invitation_data').value,
            location: {
                ver: document.querySelector('#map_type').value,
                lat: document.querySelector('#lat').value,
                lng: document.querySelector('#lng').value,
                road: document.querySelector('#map_type').value,
                lotDetail: document.querySelector('#lot_detail').value,
                tell: document.querySelector('#address_tel').value,
                option: document.querySelector('#location_option').checked,
                optionData: document.querySelector('#traffic_sub_data').value,
            },
            date: {
                date: `${datepicker}`,
                hour: `${hour}`,
                minute: `${minute}`,
                datetime: `${datepicker}.${hour}${minute}`,
                // apm: `${apm}`
            },
            videoCheck: document.querySelector('#video_toggle_btn') ? document.querySelector('#video_toggle_btn').checked : null,
            video: document.querySelector('.video_play_box') ? document.querySelector('.video_play_box iframe').value : null,
            options: {
                thankYou: document.querySelector('#thank_you_toggle_btn').checked,
                navi: document.querySelector('#navi_toggle_btn').checked,
                // music: document.querySelector('#music_toggle_btn').checked,
                // sns: document.querySelector('#sns_toggle_btn').checked,
            }
        }
    };

    console.log(saveData);
    console.log(state.category);
    console.log(state.skin);

    // console.log(coverType.value)

    if (document.querySelector('.default_address').classList.contains('active')) {
        alert('이미 사용중인 주소이거나 주소형태가 올바르지 않습니다.');
        return false;
    }

    if (saveData.card.title === '') {
        alert('제목을 입력해 주세요.');
        return false;
    }

    if (state.category !== 'old') {
        if (saveData.card.cover_img.type === '') {
            alert('메인이미지를 등록해 주세요.');
            return false;
        }
    }

    if (state.category === 'wedding') {
        if (saveData.card.state.groom_name === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.groom_phone === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.priest_name === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.priest_phone === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }
    }

    if (state.category === 'baby') {
        if (saveData.card.state.mom_name === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.mom_phone === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.dad_name === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }

        if (saveData.card.state.dad_phone === '') {
            alert('기본정보를 입력해 주세요.');
            return false;
        }
    }

    if (document.querySelector('#datepicker').value === '') {
        alert('행사일의 날짜를 입력해 주세요.');
        return false;
    }

    if (saveData.card.invitation === '') {
        alert('초대글을 입력해 주세요.');
        return false;
    }

    if (saveData.card.location.road === '') {
        alert('주소를 입력해 주세요.');
        return false;
    }

    if (saveData.card.location.lotDetail === '') {
        alert('상세주소를 입력해 주세요.');
        return false;
    }

    if (saveData.card.location.tell === '') {
        alert('장소 전화번호를 입력해 주세요.');
        return false;
    }

    if (document.querySelector('#video_toggle_btn').checked && document.querySelector('#uri').value === '') {
        alert('동영상을 등록해주세요.')
        return false
    }


    // if (document.querySelector('#timepicker').value === '') {
    //     alert('행사일의 날짜와 시간을 입력해 주세요.');
    //     return false;
    // }

    saveLoadingBox.style.display = 'flex';


    const form = document.querySelector('#FormData');
    const formData = new FormData(form); // Currently empty

    // formData.append('saveData', JSON.stringify(saveData));
    formData.append('card[main_img][main_url]', document.querySelector('#main_image_box .main_image_content img').src);
    formData.append('card[main_img][main_type]', document.querySelector('#main_img_type').value);
    // formData.append('cover_img', base64StringToBlob(document.querySelector('#main_image_box img').src));

    const photoImages = document.querySelectorAll('#photo .detail_photo_box');
    let photo_data_num = 0;
    if (photoImages.length >= 1) {
        for (let i = 0; i < photoImages.length; i += 1) {
            photo_data_num += 1;
            const ImgJSON = {};
            // console.log(subImg[i]);
            // console.log(i + 1);
            formData.append('card[photo][photo_url' + photo_data_num + ']', photoImages[i].querySelector('img').src);
            // formData.append('card[photo][photo_size'+photo_data_num+']', photoImages[i].querySelector('.detail_photo_size').value);
            // formData.append(`photo[${i + 1}]`, base64StringToBlob(photoImages[i].querySelector('img').src));
        }
    }

    const videoCheck = document.querySelector('#video_toggle_btn');
    videoCheck.checked ? formData.append('card[video][video_check]', videoCheck.checked) : formData.append('card[video][video_check]', '');

    const videoBox = document.querySelector('.video_play_box');
    videoBox ? formData.append('card[video][video_src]', document.querySelector('.video_play_box iframe').src) : formData.append('card[video][video_src]', '');

    // const musicToggle = document.querySelector('#music_toggle_btn');
    // musicToggle.checked ? formData.append('card[option][music]', musicToggle.checked) : formData.append('card[option][music]', '');

    const basicToggle = document.querySelector('#state_toggle_btn');
    basicToggle.checked ? formData.append('card[basic][toggle]', basicToggle.checked) : formData.append('card[basic][toggle]', '');

    const locationToggle = document.querySelector('#location_option');
    locationToggle.checked ? formData.append('card[location][toggle]', locationToggle.checked) : formData.append('card[location][toggle]', '');

    const thankToggle = document.querySelector('#thank_you_toggle_btn');
    thankToggle.checked ? formData.append('card[option][thank]', thankToggle.checked) : formData.append('card[option][thank]', '');

    const naviToggle = document.querySelector('#navi_toggle_btn');
    naviToggle.checked ? formData.append('card[option][navi]', naviToggle.checked) : formData.append('card[option][navi]', '');


    console.log(formData);
    const $url = 'http://aileenstory.com/goods/goods_card_ps.php';

    fetch($url, {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        console.log(res)
        res.text().then(function (text) {
            if (text === 'Y') {
                return callback(`https://app.aileenstory.com/${saveData.card.cardId}`);
            } else {
                alert('저장에 실패했습니다. 다시 저장을 해주세요.');
                saveLoadingBox.style.display = 'none';
            }
        });

        // res.json().then(text => {
        //     console.log(text)
        //     if (text.state === 'error') {
        //
        //     } else if (text.state === 'ok') {
        //
        //
        //     }
        // }); // 텍스트 출력

    }).catch(function (error) {
        console.log(error);
    });

    // $.ajax({
    //     url: $url,
    //     data: formData,
    //     type: 'POST',
    //     enctype: 'multipart/form-data',
    //     processData: false,
    //     contentType: false,
    //     cache: false,
    //     success: function (data) {
    //         // console.log(data);
    //         alert('초대장 저장이 완료되었습니다.');
    //         callback(`https://videomaker.nanuly.kr/card/${saveData.card.cardId}`)
    //     },
    // });
}


