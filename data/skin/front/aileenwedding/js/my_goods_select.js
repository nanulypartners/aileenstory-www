
const controller = {
    closeBtn : document.querySelector('header .close_btn_box'),
    category : document.querySelector('#category'),
    skin : document.querySelector('#skin'),
    categoryButtons : document.querySelectorAll('.category_select li'),
    skinList : document.querySelectorAll('.skin_list_box ul'),
    skinItems : document.querySelectorAll('.skin_list_box ul li'),
    weddingItems : document.querySelectorAll('.skin_list_box ul li.wedding'),
    babyItems : document.querySelectorAll('.skin_list_box ul li.baby'),
    oldItems : document.querySelectorAll('.skin_list_box ul li.old'),
    skinValues : document.querySelectorAll('.skin_list_box ul li input'),
    freeViewBox : document.querySelector('.card_skin_select_modal .right .img_content'),
    freeViewImage : document.querySelector('.free_view'),
    skinOkBtn : document.querySelector('.skin_ok_btn button'),
    searchIcons : document.querySelectorAll('.search_icon'),
    contentRight : document.querySelector('.card_skin_select_modal .right'),
    modalCloseBtn : document.querySelector('.modal_close_btn'),

    windowClose() {
        this.closeBtn.addEventListener('click', () => {
            window.close();
        });
    }
};

controller.windowClose();


/*
    ---- 함수 목록 시작 ----
 */

const targetActive = (index, lists) => {
    for (let i = 0; i < lists.length; i+=1) {
        lists[i].classList.remove('active');
    }
    lists[index].classList.add('active');
};

const skinSelector = (index, items, skin) => {
    skin.value = items[index].value;
};

const freeView = (src, image) => {
    image.setAttribute('src' , src);
    controller.freeViewBox.scrollTop = 0;
};

const getUrlParams = () => {
    const params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) { params[key] = value; });
    return params;
};

const locationMove = (oParams, category, skin) => {
    if (skin.value === '') {
        alert('스킨을 선택해 주세요.');
        return
    }
    localStorage.removeItem('card_category');
    localStorage.removeItem('card_skin');
    localStorage.setItem('card_category' , controller.category.value);
    localStorage.setItem('card_skin' , controller.skin.value);
    location.href = "goods_card_write.php?category=" + category.value + "&skin=" + skin.value;
};



/*
    ---- 함수 목록 끝 ----
 */







function initCategory() {
    const oParams = getUrlParams();
    const category =  oParams.category;
    if (category === 'wedding') {
        targetActive(0, controller.categoryButtons);
        targetActive(0, controller.skinList);
        targetActive(0, controller.skinItems);
        targetActive(0, controller.searchIcons);
        skinSelector(0, controller.skinValues, controller.skin);
        freeView(controller.weddingItems[0].getAttribute('data-skin'), controller.freeViewImage);
        controller.category.value = category;
    } else if (category === 'baby') {
        const index = controller.weddingItems.length;
        targetActive(1, controller.categoryButtons);
        targetActive(1, controller.skinList);
        targetActive(index, controller.skinItems);
        targetActive(index, controller.searchIcons);
        skinSelector(index, controller.skinValues, controller.skin);
        freeView(controller.babyItems[0].getAttribute('data-skin'), controller.freeViewImage);
        controller.category.value = category;
    } else if (category === 'old') {
        const index = controller.weddingItems.length + controller.babyItems.length;
        targetActive(2, controller.categoryButtons);
        targetActive(2, controller.skinList);
        targetActive(index, controller.skinItems);
        targetActive(index, controller.searchIcons);
        skinSelector(index, controller.skinValues, controller.skin);
        freeView(controller.oldItems[0].getAttribute('data-skin'), controller.freeViewImage);
        controller.category.value = category;
    }
}

initCategory();


controller.categoryButtons.forEach((v,index) => {
    v.addEventListener('click', (e) => {
        targetActive(index, controller.categoryButtons);
        targetActive(index, controller.skinList);
        if (index === 0) {
            controller.category.value = 'wedding';
        } else if (index === 1) {
            controller.category.value = 'baby';
        } else if (index === 2) {
            controller.category.value = 'old';
        }

    });
});

controller.skinItems.forEach((target, index) => {
    target.addEventListener('click', (e) => {
        const ele = e.currentTarget;
        targetActive(index, controller.skinItems);
        targetActive(index, controller.searchIcons);
        skinSelector(index, controller.skinValues, controller.skin);
        freeView(ele.getAttribute('data-skin'), controller.freeViewImage);
    });
});

controller.searchIcons.forEach((target, index) => {
   target.addEventListener('click', () => {
       controller.contentRight.style.display = 'block';
   }) ;
});

controller.modalCloseBtn.addEventListener('click', () => {
    controller.contentRight.style.display = 'none';
});


controller.skinOkBtn.addEventListener('click', () => {
    const oParams = getUrlParams();
    const category = controller.category;
    const skin = controller.skin;

    locationMove(oParams, category, skin);

});


