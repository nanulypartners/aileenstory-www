<?php

namespace Component\Autorender;

use Component\Database\DBTableField;
use Request;
use Framework\Debug\Exception\AlertBackException;
use Component\Storage\Storage;

class AutorenderAdmin
{
    public function __construct()
    {
        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }
    }


    public function deleteData($data)
    {

        $bsSQL = " SELECT * FROM es_autorenderUser WHERE createNm = " . $data['createNm'];
        $bsResult = $this->db->query_fetch($bsSQL);

        if ($bsResult['state']==0) {

            /*파일서버쪽 전송*/
            $arrData['nanuly'] = "sksnfl2020@";
            $arrData['nanuly_type'] = "deleteData";
            $arrData['data'] = $data;
            $url = "https://app.aileenstory.com/api/autorender_api.php";
            $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                      'Content-Type: application/json',
                                      'Content-Length: '.strlen($json_data)));
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output, true);

            if ($output['state']!="success") {
                $result['state'] = "error";
                $result['msg'] = "서버전송 오류가 발생했습니다. 해당 데이터를 다시 삭제해 주세요.";
            } else {
                $query = "DELETE FROM es_autorenderUser WHERE createNm = " . $data['createNm'];
                $this->db->query($query);                
                $result['state'] = "ok";
            }
            /*파일서버쪽 전송*/

        } else {
            $result['state'] = "error";
            $result['msg'] = "제작대기 상태가 아닙니다.";            
        }

        return $result;

    }


    public function render($data)
    {

        /*파일서버쪽 전송*/
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "AdminRender";
        $arrData['data'] = $data;
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);
        if ($output['state']!="success") {
            $result = "서버전송 오류가 발생했습니다. 수동 렌더링을 다시 진행해 주세요.";
            return $result;
        }
        /*파일서버쪽 전송*/

        $result = "수동 렌더링 요청이 완료되었습니다.";
        return $result;

    }

    public function modifyCount($data)
    {

        $query = "UPDATE es_autorenderUser SET modifyCount='{$data['cnt']}' WHERE createNm='{$data['createNm']}' ";
        $this->db->query($query);

        /*파일서버쪽 전송*/
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "AdminModifyCount";
        $arrData['data'] = $data;
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);
        if ($output['state']!="success") {
            $result = "서버전송 오류가 발생했습니다. 수정하기로 해당 영상을 다시 저장해 주세요.";
            return $result;
        }
        /*파일서버쪽 전송*/

        $result = "수정이 완료되었습니다.";
        return $result;

    }

    public function getListByOrder($data)
    {

        $createNm = $data['createNm'];

        $sort['fieldName'] = 'regDt';
        $sort['sortMode'] = 'desc';

        $arrWhere[] = " createNm = ? ";
        $this->db->bind_param_push($arrBind, 's', $createNm);

        $arrWhere[] = " payment = ? ";
        $this->db->bind_param_push($arrBind, 's', 1);

        $addWhere = implode(' AND ', gd_isset($arrWhere));

        $this->db->strField = " * ";
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strWhere = $addWhere;

        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderByOrderNo ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        unset($arrBind);
        unset($arrWhere);

        $sort['fieldName'] = 'modDt';
        $sort['sortMode'] = 'desc';
        
        $arrWhere[] = " useCreateNm = ? ";
        $this->db->bind_param_push($arrBind, 's', $createNm);

        $arrWhere[] = " useYn = ? ";
        $this->db->bind_param_push($arrBind, 's', 1);

        $addWhere = implode(' AND ', gd_isset($arrWhere));

        $this->db->strField = " es_autorenderCode.*, t1.company AS company_name, t1.companyCode, t1.companyId";
        $this->db->strJoin = "LEFT JOIN es_autorenderCodeCompany AS t1 ON t1.no=es_autorenderCode.company";
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strWhere = $addWhere;

        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderCode ' . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                array_push($getData['data'], gd_htmlspecialchars_stripslashes(gd_isset($value)));    
            }
        }

        return $getData;

    }

    public function getGoodsNoNm($type='0')
    {

        $bsSQL = " SELECT goodsCd FROM es_autorenderAdmin WHERE goodsType = 'goods' AND type='{$type}' ";
        $bsResult = $this->db->query_fetch($bsSQL);

        $postArr = array_map('array_filter', $bsResult);
        $postArr = array_filter($postArr);
        array_multisort($postArr, SORT_DESC, SORT_NUMERIC);

        $new_array = array();
        for($i=0; $i<sizeof($postArr); $i++) {
            $new_array[$i] = $postArr[$i]['goodsCd'];
        }
        $in_list = empty($new_array)?'NULL':"'".join("','", $new_array)."'";

        if(empty($new_array)){
            $strSQL = " SELECT goodsNo,goodsNm FROM es_goods WHERE delDt = '0000-00-00 00:00:00' OR delDt is Null ";
        } else {
            $strSQL = " SELECT goodsNo,goodsNm FROM es_goods WHERE goodsNo NOT IN({$in_list}) AND (delDt = '0000-00-00 00:00:00' OR delDt is Null) ";
        }
        $result = $this->db->query_fetch($strSQL);

        // 슬래시 제거
        return gd_htmlspecialchars_stripslashes($result);
        
    }

    public function getAddGoods(){
    	
        $bsSQL = " SELECT goodsCd FROM es_autorenderAdmin WHERE goodsType = 'addGoods' ";
        $bsResult = $this->db->query_fetch($bsSQL);

        $postArr = array_map('array_filter', $bsResult);
        $postArr = array_filter($postArr);
        array_multisort($postArr, SORT_DESC, SORT_NUMERIC);

        $new_array = array();
        for($i=0; $i<sizeof($postArr); $i++) {
            $new_array[$i] = $postArr[$i]['goodsCd'];
        }
        $in_list = empty($new_array)?'NULL':"'".join("','", $new_array)."'";

        if(empty($new_array)){
            $strSQL = " SELECT addGoodsNo,goodsNm FROM es_addGoods ";
        } else {
            $strSQL = " SELECT addGoodsNo,goodsNm FROM es_addGoods WHERE addGoodsNo NOT IN({$in_list}) ";
        }

        $result = $this->db->query_fetch($strSQL);

        // 슬래시 제거
        return gd_htmlspecialchars_stripslashes($result);
        
    }

    public function getGoodsCateNm($data, $type='0')
    {

        $bsSQL = " SELECT goodsCd FROM es_autorenderAdmin WHERE goodsType = 'goods' AND type='{$type}' ";
        $bsResult = $this->db->query_fetch($bsSQL);

        $postArr = array_map('array_filter', $bsResult);
        $postArr = array_filter($postArr);
        array_multisort($postArr, SORT_DESC, SORT_NUMERIC);

        $new_array = array();
        for($i=0; $i<sizeof($postArr); $i++) {
            $new_array[$i] = $postArr[$i]['goodsCd'];
        }
        $in_list = empty($new_array)?'NULL':"'".join("','", $new_array)."'";

        if(empty($new_array)){
            $strSQL = " SELECT goodsNo,goodsNm FROM es_goods WHERE cateCd LIKE '{$data}%' AND (delDt = '0000-00-00 00:00:00' OR delDt is Null)";
        } else {
            $strSQL = " SELECT goodsNo,goodsNm FROM es_goods WHERE cateCd LIKE '{$data}%' AND (delDt = '0000-00-00 00:00:00' OR delDt is Null) AND goodsNo NOT IN({$in_list}) ";
        }

        // echo  $strSQL;
        $result = $this->db->query_fetch($strSQL);

        // 슬래시 제거
        return gd_htmlspecialchars_stripslashes($result);
        
    }

    public function insertVideoAdmin($postValue, $type=0)
    {
    	

        $filesValue = Request::files()->toArray();

        if( !$postValue['goodsType'] || (!$postValue['videoGoodsNo'] && $postValue['goodsType']=="goods") || (!$postValue['videoGoodsAddNo'] && $postValue['goodsType']=="addGoods") ) {

            throw new AlertBackException('상품을 선택해주세요.');

        } else if($postValue['goodsType']=="goods"){

            $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$postValue['videoGoodsNo']}' AND goodsType = 'goods' AND type='{$type}'";
            $result = $this->db->fetch($strSQL);
            // print_r($result);

            if($result) {

                throw new AlertBackException('중복된 상품코드입니다.');
            }

        } else if($postValue['goodsType']=="addGoods"){
            $postValue['videoGoodsNo'] = $postValue['videoGoodsAddNo'];
            $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$postValue['videoGoodsNo']}' AND goodsType = 'addGoods' AND type='{$type}' ";
            $result = $this->db->fetch($strSQL);

            if($result) {

                throw new AlertBackException('중복된 상품코드입니다.');
            }

        }

        $fileDbPath = array();

        foreach($filesValue['scene']['name'] as $key => $nameValue){
            $filetype = $filesValue['scene']['type'][$key]['sample_image']; // 파일타입
            $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image'];  // 파일경로
            $original = $filesValue['scene']['name'][$key]['sample_image']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            $filePath = '/goods/autorender/'.$postValue['videoGoodsNo'].'/';
            $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로

            // 폴더 생성
            if (!is_dir($uploadDir)) {
                $old = umask(0);
                mkdir($uploadDir, 0707, true);
                umask($old);
            }

            // 최종 경로
            $fnDir = $uploadDir.$fileRename;

            // 파일 업로드
            //move_uploaded_file($tmp, $fnDir);
            $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
            $storage->upload($tmp, $filePath.$fileRename, ['width' => 475, 'height' => 267]);


            // DB 저장 경로
            if($nameValue['sample_image']){
                $fileDbPath[$key] = 'data'.$filePath.$fileRename;
                //array_push($fileDbPath, $filePath.$fileRename);    
            }else{
                $fileDbPath[$key] = '';
            }
            
        }


        $fileDbPathMerge = array();

        foreach($filesValue['scene']['name'] as $key => $nameValue){
            $filetype = $filesValue['scene']['type'][$key]['sample_image_merge']; // 파일타입
            $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image_merge'];  // 파일경로
            $original = $filesValue['scene']['name'][$key]['sample_image_merge']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            $filePath = '/goods/autorender/'.$postValue['videoGoodsNo'].'/';
            $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로

            // 폴더 생성
            if (!is_dir($uploadDir)) {
                $old = umask(0);
                mkdir($uploadDir, 0707, true);
                umask($old);
            }

            // 최종 경로
            $fnDir = $uploadDir.$fileRename;

            // 파일 업로드
            //move_uploaded_file($tmp, $fnDir);
            $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
            $storage->upload($tmp, $filePath.$fileRename, ['width' => 1920, 'height' => 1080]);

            // DB 저장 경로
            if($nameValue['sample_image_merge']){
                $fileDbPathMerge[$key] = 'data'.$filePath.$fileRename;
            }else{
                $fileDbPathMerge[$key] = '';
            }
            
        }



        // print_r($postValue['video']);
        $new_array = array();
        $i = 1;
        foreach ($postValue['scene'] as $key => $value) {
            $value['dbPath'] = $fileDbPath[$i];
            $value['dbPathMerge'] = $fileDbPathMerge[$i];
            foreach ($value as $k => $v) {
                if (is_array($v)) {
                    $value[$k] = (object)$v;
                }
            }
            $new_array[$i] = $value;
            $i++;
        }

        // 장면정보입력
        foreach ($new_array as $scenes) {
            $scene[$scenes['seq']] = (object)$scenes;
        }

        $basicInfo = json_encode((object) $postValue['basic'], JSON_UNESCAPED_UNICODE);
        $json_scene = json_encode((object) $scene, JSON_UNESCAPED_UNICODE);

        $arrData = [
            "type" => $type,
            "goodsType" => $postValue['goodsType'],
            "goodsNm" => $postValue['videoGoodsNm'],
            "goodsCd" => $postValue['videoGoodsNo'],
            "useYn" => $postValue['useYn'],
            "sampleVideoUrl" => $postValue['sampleVideoUrl'],
            "templateId" => $postValue['templateId'],
            "templateName" => $postValue['templateName'],
            "templatePath" => $postValue['templatePath'],
            "playTime" => $postValue['playTime'],
            "bgmLayerName" => $postValue['bgmLayerName'],
            "bgmName" => $postValue['bgmName'],
            "bgmChange" => $postValue['bgmChange'],
            "basicInfo" => $basicInfo,
            "sceneInfo" => $json_scene,
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableAutorenderAdmin(), $arrData, 'insert');
        $this->db->set_insert_db('es_autorenderAdmin', $arrBind['param'], $arrBind['bind'], 'y');

        /*파일서버쪽 전송*/
        $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$postValue['videoGoodsNo']}' AND goodsType='{$postValue['goodsType']}' AND type='{$type}' ";
        $result = $this->db->fetch($strSQL);
        $arrData['no'] = $result['no'];
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "AdminInsert";
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);
        if ($output['state']!="success") {
            throw new AlertBackException('서버전송 오류가 발생했습니다. 수정하기로 해당 영상을 다시 저장해 주세요');
        }

        /*파일서버쪽 전송*/


		unset($arrData);
        unset($arrBind);
    }

    public function updateVideoAdmin($postValue, $type=0)
    {


        $filesValue = Request::files()->toArray();

        // 기존 파일첨부 삭제 //
        $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$postValue['getGoodsNo']}' AND goodsType='{$postValue['goodsType']}' AND type='{$type}' ";
        $result = $this->db->fetch($strSQL);

        $dataArray = json_decode($result['sceneInfo'], true);
        $path_array = array();
        foreach ($dataArray as $key => $value) {
            array_push($path_array, $value['dbPath']);
            array_push($path_array, $value['dbPathMerge']);
        }

        $post_path_array = array();
        foreach ($postValue['scene'] as $key => $value) {
            if(!is_array($value['sample_image_db'])) {
                array_push($post_path_array, $value['sample_image_db']);
            }

            if(!is_array($value['sample_image_merge_db'])) {
                array_push($post_path_array, $value['sample_image_merge_db']);
            }
        }

        $del = array_diff($path_array, $post_path_array);
        foreach ($del as $key => $value) {
            $delLoadDir = USERPATH.$value;
            @unlink($delLoadDir);
        }
        // 기존 파일첨부 삭제 //


        $fileDbPath = array();
        $i = 1;
        foreach ($filesValue['scene']['name'] as $key => $value) {
            // $postValue['video'][$key] : 기존파일
            // $value : 수정파일

            if($postValue['scene'][$key]['sample_image_db'] && $value['sample_image']){
                // 수정

                $delLoadDir = USERPATH.$postValue['scene'][$key]['sample_image_db'];    // 삭제할 경로
                // 기존파일 삭제
                @unlink($delLoadDir);

                $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image'];  // 파일경로
                $original = $filesValue['scene']['name'][$key]['sample_image']; // 파일이름
                $pathParts = pathinfo($original);  // 확장자 추출
                $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

                $filePath = '/goods/autorender/'.$postValue['getGoodsNo'].'/';
                $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로


                // 폴더 생성
                if (!is_dir($uploadDir)) {
                    $old = umask(0);
                    mkdir($uploadDir, 0707, true);
                    umask($old);
                }

                // 최종 경로
                $fnDir = $uploadDir.$fileRename;

                // 파일 업로드
                //move_uploaded_file($tmp, $fnDir);
                $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
                $storage->upload($tmp, $filePath.$fileRename, ['width' => 475, 'height' => 267]);

                // DB 저장 경로
                $fileDbPath[$i] = 'data'.$filePath.$fileRename;

            } elseif($postValue['scene'][$key]['sample_image_db'] && !$value['sample_image']) {
                // 유지

                // DB 저장 경로
                $fileDbPath[$i] = $postValue['scene'][$key]['sample_image_db'];

            } elseif(!$postValue['scene'][$key]['sample_image_db'] && $value['sample_image']) {
                // 추가

                $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image'];  // 파일경로
                $original = $filesValue['scene']['name'][$key]['sample_image']; // 파일이름
                $pathParts = pathinfo($original);  // 확장자 추출
                $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

                $filePath = '/goods/autorender/'.$postValue['getGoodsNo'].'/';
                $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로


                // 폴더 생성
                if (!is_dir($uploadDir)) {
                    $old = umask(0);
                    mkdir($uploadDir, 0707, true);
                    umask($old);
                }

                // 최종 경로
                $fnDir = $uploadDir.$fileRename;

                // 파일 업로드
                //move_uploaded_file($tmp, $fnDir);
                $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
                $storage->upload($tmp, $filePath.$fileRename, ['width' => 475, 'height' => 267]);

                // DB 저장 경로
                $fileDbPath[$i] = 'data'.$filePath.$fileRename;

            } else {
                $fileDbPath[$i] = '';
            }
            $i++;
        }



        $fileDbPathMerge = array();
        $i = 1;
        foreach ($filesValue['scene']['name'] as $key => $value) {
            // $postValue['video'][$key] : 기존파일
            // $value : 수정파일

            if($postValue['scene'][$key]['sample_image_merge_db'] && $value['sample_image_merge']){
                // 수정

                $delLoadDir = USERPATH.$postValue['scene'][$key]['sample_image_merge_db'];    // 삭제할 경로
                // 기존파일 삭제
                @unlink($delLoadDir);

                $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image_merge'];  // 파일경로
                $original = $filesValue['scene']['name'][$key]['sample_image_merge']; // 파일이름
                $pathParts = pathinfo($original);  // 확장자 추출
                $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

                $filePath = '/goods/autorender/'.$postValue['getGoodsNo'].'/';
                $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로

                // 폴더 생성
                if (!is_dir($uploadDir)) {
                    $old = umask(0);
                    mkdir($uploadDir, 0707, true);
                    umask($old);
                }

                // 최종 경로
                $fnDir = $uploadDir.$fileRename;

                // 파일 업로드
                //move_uploaded_file($tmp, $fnDir);
                $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
                $storage->upload($tmp, $filePath.$fileRename, ['width' => 1920, 'height' => 1080]);

                // DB 저장 경로
                $fileDbPathMerge[$i] = 'data'.$filePath.$fileRename;

            } elseif($postValue['scene'][$key]['sample_image_merge_db'] && !$value['sample_image_merge']) {
                // 유지

                // DB 저장 경로
                $fileDbPathMerge[$i] = $postValue['scene'][$key]['sample_image_merge_db'];

            } elseif(!$postValue['scene'][$key]['sample_image_merge_db'] && $value['sample_image_merge']) {
                // 추가

                $tmp = $filesValue['scene']['tmp_name'][$key]['sample_image_merge'];  // 파일경로
                $original = $filesValue['scene']['name'][$key]['sample_image_merge']; // 파일이름
                $pathParts = pathinfo($original);  // 확장자 추출
                $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

                $filePath = '/goods/autorender/'.$postValue['getGoodsNo'].'/';
                $uploadDir = USERPATH.'data'.$filePath;    // 저장할 경로

                // 폴더 생성
                if (!is_dir($uploadDir)) {
                    $old = umask(0);
                    mkdir($uploadDir, 0707, true);
                    umask($old);
                }

                // 최종 경로
                $fnDir = $uploadDir.$fileRename;

                // 파일 업로드
                //move_uploaded_file($tmp, $fnDir);
                $storage = Storage::disk(Storage::PATH_CODE_DEFAULT, 'local');
                $storage->upload($tmp, $filePath.$fileRename, ['width' => 1920, 'height' => 1080]);

                // DB 저장 경로
                $fileDbPathMerge[$i] = 'data'.$filePath.$fileRename;

            } else {
                $fileDbPathMerge[$i] = '';
            }
            $i++;
        }


        $new_array = array();
        $i = 1;
        foreach ($postValue['scene'] as $key => $value) {
            $value['dbPath'] = $fileDbPath[$i];
            $value['dbPathMerge'] = $fileDbPathMerge[$i];
            foreach ($value as $k => $v) {
                if (is_array($v)) {
                    $value[$k] = (object)$v;
                }
            }
            $new_array[$i] = $value;
            $i++;
        }

        // 장면정보입력
        foreach ($new_array as $scenes) {
            $scene[$scenes['seq']] = (object)$scenes;
        }

        $basicInfo = json_encode((object) $postValue['basic'], JSON_UNESCAPED_UNICODE);
        $json_scene = json_encode((object) $scene, JSON_UNESCAPED_UNICODE);

        $arrData = [
            "type" => $type,
            "goodsType" => $postValue['goodsType'],
            "goodsNm" => $postValue['videoGoodsNm'],
            "goodsCd" => $postValue['getGoodsNo'],
            "useYn" => $postValue['useYn'],
            "sampleVideoUrl" => $postValue['sampleVideoUrl'],
            "templateId" => $postValue['templateId'],
            "templateName" => $postValue['templateName'],
            "templatePath" => $postValue['templatePath'],
            "playTime" => $postValue['playTime'],
            "bgmLayerName" => $postValue['bgmLayerName'],
            "bgmName" => $postValue['bgmName'],
            "bgmChange" => $postValue['bgmChange'],
            "basicInfo" => $basicInfo,
            "sceneInfo" => $json_scene,
        ];

        $arrExclude = [
            'no',
            'goodsType',
            'goodsNm',
            'goodsCd'
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableAutorenderAdmin(), $arrData, 'update', '', $arrExclude);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['goodsCd']);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['goodsType']);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['type']);
        $this->db->set_update_db('es_autorenderAdmin', $arrBind['param'], 'goodsCd = ? AND goodsType = ? AND type = ?', $arrBind['bind']);

        /*파일서버쪽 전송*/

        //번호 불러오기
        $arrData['no'] = $result['no'];
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "AdminInsert";
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode((object)$arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);
        if ($output['state']!="success") {
            throw new AlertBackException('서버전송 오류가 발생했습니다. 해당 영상을 다시 저장해 주세요');
        }

        /*파일서버쪽 전송*/

        unset($arrData);
        unset($arrBind);

    }

    public function getVideoAdmin($getValue = null, $type=0)
    {

        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$getValue['goodsNo']}' AND goodsType = '{$getValue['goodsType']}' AND type='{$type}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['goodsType'] = $v['goodsType'];
                $data['goodsNm'] = $v['goodsNm'];
                $data['useYn'] = $v['useYn'];
                $data['sampleVideoUrl'] = $v['sampleVideoUrl'];
                $data['templateId'] = $v['templateId'];
                $data['templateName'] = $v['templateName'];
                $data['templatePath'] = $v['templatePath'];
                $data['playTime'] = $v['playTime'];
                $data['bgmLayerName'] = $v['bgmLayerName'];
                $data['bgmName'] = $v['bgmName'];
                $data['bgmChange'] = $v['bgmChange'];
                $json['basicInfo'] = $v['basicInfo'];
                $json['sceneInfo'] = $v['sceneInfo'];
            }
            $data['basicInfo'] = json_decode($json['basicInfo'], true);
            $data['sceneInfo'] = json_decode($json['sceneInfo'], true);
            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

            return $getData;
        }
        
    }

    public function getListVideoAdmin($getValue = null, $type=0)
    {

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['goodsNm', 'goodsCd'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($type==1) {
            $arrWhere[] = " type = ? ";
            $this->db->bind_param_push($arrBind, 's', 1);
        } else {
            $arrWhere[] = " type = ? ";
            $this->db->bind_param_push($arrBind, 's', 0);
        }

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderAdmin WHERE $addWhere AND goodsType!='sianDate'";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $addWhere = $addWhere." AND goodsType!='sianDate'";
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderAdmin ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 검색 레코드 수 *borad_theme.php*
        // $page->recode['total'] = $this->db->query_fetch('SELECT COUNT(*) as cnt FROM es_videoAdmin WHERE $addWhere', $arrBind, false)['cnt'];
        // $page->setPage();

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }

    public function deleteListVideoAdmin($chk)
    {
        /* 서버저장된 이미지파일 삭제 */

        // 체크된 값 저장경로 SELECT
        $new = array();
        foreach ($chk['chk'] as $key => $value) {

            $strSQL = " SELECT sceneInfo FROM es_autorenderAdmin WHERE no = '{$value}' ";
            $result = $this->db->query_fetch($strSQL);

            array_push($new, $result[0]);
        }

        // json값 배열에 저장
        $data = array();
        $dbPath = array();
        foreach ($new as $key => $value) {

            $data = json_decode($value['sceneInfo'], true);

            foreach ($data as $key2 => $value2) {
                array_push($dbPath, $value2['dbPath']);
                array_push($dbPath, $value2['dbPathMerge']);
            }

        }

        // 서버 이미지파일 삭제
        foreach ($dbPath as $key => $value) {
            $uploadDir = USERPATH.$value;    // 삭제할 경로
            @unlink($uploadDir);
        }


        /*파일서버쪽 전송*/
        $arrData = array();
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "AdminDelete";
        $arrData['chk'] = $chk;
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);



        if ($output['state']!="success") {
            throw new AlertBackException('서버전송 오류가 발생했습니다. 해당 영상을 다시 삭제해 주세요');
        }

        /*파일서버쪽 전송*/

        // DB 값 삭제
        foreach ($chk['chk'] as $key => $value) {

            $query = "DELETE FROM es_autorenderAdmin WHERE no = " . $value;
            $this->db->query($query);

        }

    }


    public function getListCompanyAll($getValue = null)
    {
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $this->db->strField = " * ";
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderCodeCompany ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }

    public function getListCompany($getValue = null)
    {

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['company', 'companyCode'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderCodeCompany WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderCodeCompany ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


    public function getCompany($getValue = null)
    {
        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_autorenderCodeCompany WHERE no = '{$getValue['no']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['no'] = $v['no'];
                $data['company'] = $v['company'];
                $data['companyId'] = $v['companyId'];
                $data['companyCode'] = $v['companyCode'];
                $data['mode'] = "update";
            }

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }


    public function insertCompany($postValue)
    {

        if(!$postValue['company'] || !$postValue['companyCode'] ) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        //제휴사 코드 중복확인
        $strSQL = " SELECT * FROM es_autorenderCodeCompany WHERE companyCode = '{$postValue['companyCode']}'";
        $result = $this->db->fetch($strSQL);

        if($result) {
            throw new AlertBackException('중복된 제휴사코드 입니다.');
        }

        $arrData = [
            "company" => $postValue['company'],
            "companyId" => $postValue['companyId'],
            "companyCode" => strtoupper($postValue['companyCode']),
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableAutorenderCodeCompany(), $arrData, 'insert');
        $this->db->set_insert_db('es_autorenderCodeCompany', $arrBind['param'], $arrBind['bind'], 'y');
        unset($arrData);
        unset($arrBind);

    }


    public function updateCompany($postValue)
    {

        if(!$postValue['company'] || !$postValue['companyCode'] || !$postValue['no'] ) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        //초대장 코드 중복확인
        $strSQL = " SELECT * FROM es_autorenderCodeCompany WHERE companyCode = '{$postValue['companyCode']}' AND no != '{$postValue['no']}'";
        $result = $this->db->fetch($strSQL);

        if($result) {
            throw new AlertBackException('중복된 제휴사코드 입니다.');
        }


        $arrData = [
            "no" => $postValue['no'],
            "company" => $postValue['company'],
            "companyId" => $postValue['companyId'],
            "companyCode" => strtoupper($postValue['companyCode']),
        ];

        $arrInclude = [
            "company", "companyId", "companyCode"
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableAutorenderCodeCompany(), $arrData, 'update', $arrInclude);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['no']);
        $this->db->set_update_db('es_autorenderCodeCompany', $arrBind['param'], 'no = ?', $arrBind['bind']);
        unset($arrData);
        unset($arrBind);
    }


    public function deleteCompany($chk)
    {
        foreach ($chk['chk'] as $key => $value) {

            /* DB 값 삭제 */

            $query = "DELETE FROM es_autorenderCodeCompany WHERE no = " . $value;
            $this->db->query($query);

            $query = "DELETE FROM es_autorenderCode WHERE company = " . $value;
            $this->db->query($query);

        }
    }

    public function getListCodeAll($getValue = null)
    {

        $sort['fieldName'] = 'no';
        $sort['sortMode'] = 'desc';

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['userName', 'useId', 'useCreateNm'])) {

                if ($getValue['searchField']=="userName") {
                    $getValue['searchField'] = "useId";
                }

                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                if ($getValue['searchField']=='code' && strpos($getValue['keyword'], "-") !== false) {
                    $getValue['keyword'] = explode("-", $getValue['keyword']);
                    $this->db->bind_param_push($arrBind, 's', $getValue['keyword'][1]);    
                } else {
                    $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);    
                }
                
            }
        }

        if($getValue['treatDate']['start']){
            $arrWhere[] = " useDt >= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['start']);
        }

        if($getValue['treatDate']['end']){
            $arrWhere[] = " useDt <= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['end']);
        }
        
        if ($getValue['useYn']!="") {
            $arrWhere[] = " useYn = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['useYn']);
        }

        if ($getValue['calculate']!="") {
            $arrWhere[] = " calculate = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['calculate']);
        }

        if ($getValue['company_name']!="") {
            $arrWhere[] = " t1.company = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['company_name']);
        }

        if ($getValue['code']!="") {
            $code = explode("-", $getValue['code']);
            $arrWhere[] = " t1.companyCode = ? ";
            $this->db->bind_param_push($arrBind, 's', $code[0]);
            $arrWhere[] = " code = ? ";
            $this->db->bind_param_push($arrBind, 's', $code[1]);
        }

        if ($getValue['goodsName']!="") {
            $arrWhere[] = " goodsName like concat('%',?,'%')  ";
            $this->db->bind_param_push($arrBind, 's', $getValue['goodsName']);
        }

        $manager = \Session::get('manager');
        if ($manager['sno']!=1) {
            $arrWhere[] = " t1.companyId = ? ";
            $this->db->bind_param_push($arrBind, 's', $manager['managerId']);            
        }
        
        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        // 현 페이지 결과
        $this->db->strField = " es_autorenderCode.*, t1.company AS company_name, t1.companyCode, t1.companyId";
        $this->db->strJoin = "LEFT JOIN es_autorenderCodeCompany AS t1 ON t1.no=es_autorenderCode.company";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderCode ' . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }

    public function getListCode($getValue = null)
    {

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'no';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['userName', 'useId', 'useCreateNm'])) {

                if ($getValue['searchField']=="userName") {
                    $getValue['searchField'] = "useId";
                }

                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                if ($getValue['searchField']=='code' && strpos($getValue['keyword'], "-") !== false) {
                    $getValue['keyword'] = explode("-", $getValue['keyword']);
                    $this->db->bind_param_push($arrBind, 's', $getValue['keyword'][1]);    
                } else {
                    $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);    
                }
                
            }
        }

        if($getValue['treatDate']['start']){
            $arrWhere[] = " useDt >= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['start']);
        }

        if($getValue['treatDate']['end']){
            $arrWhere[] = " useDt <= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['end']);
        }

        $manager = \Session::get('manager');
        if ($manager['sno']!=1) {
            $arrWhere[] = " t1.companyId = ? ";
            $this->db->bind_param_push($arrBind, 's', $manager['managerId']);            
        }

        if ($getValue['useYn']!="") {
            $arrWhere[] = " useYn = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['useYn']);
        }

        if ($getValue['calculate']!="") {
            $arrWhere[] = " calculate = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['calculate']);
        }

        if ($getValue['company_name']!="") {
            $arrWhere[] = " t1.company = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['company_name']);
        }

        if ($getValue['code']!="") {
            $code = explode("-", $getValue['code']);
            $arrWhere[] = " t1.companyCode = ? ";
            $this->db->bind_param_push($arrBind, 's', $code[0]);
            $arrWhere[] = " code = ? ";
            $this->db->bind_param_push($arrBind, 's', $code[1]);
        }

        if ($getValue['goodsName']!="") {
            $arrWhere[] = " goodsName like concat('%',?,'%')  ";
            $this->db->bind_param_push($arrBind, 's', $getValue['goodsName']);
        }


        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderCode LEFT JOIN es_autorenderCodeCompany AS t1 ON t1.no=es_autorenderCode.company WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " es_autorenderCode.*, t1.company AS company_name, t1.companyCode, t1.companyId";
        $this->db->strJoin = "LEFT JOIN es_autorenderCodeCompany AS t1 ON t1.no=es_autorenderCode.company";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderCode ' . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


    public function getCompanyNo($postValue){

        $strSQL = " SELECT * FROM es_autorenderCodeCompany WHERE companyId = '{$postValue}'";
        $result = $this->db->fetch($strSQL);

        return $result['no'];

    }

    public function insertCompanyCode($postValue)
    {
        if(!$postValue['company'] || !$postValue['code_cnt'] ) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }


        for ($i=0; $i < $postValue['code_cnt']; $i++) { 
            //랜덤코드 발행

            while(1){
                $code = $this->coupon_generator();

                //중복코드 체크
                $strSQL = " SELECT * FROM es_autorenderCode WHERE code = '{$code}' AND company = '{$postValue['company']}'";
                $result = $this->db->fetch($strSQL);

                if(!$result) {
                    break;
                }
            }
            

            $arrData = [
                "company" => $postValue['company'],
                "code" => $code,
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderCode(), $arrData, 'insert');
            $this->db->set_insert_db('es_autorenderCode', $arrBind['param'], $arrBind['bind'], 'y');
            unset($arrData);
            unset($arrBind);

        }

    }


    public function deleteCompanyCode($chk)
    {

        foreach ($chk['chk'] as $key => $value) {

            /* DB 값 삭제 */
            $query = "DELETE FROM es_autorenderCode WHERE no = " . $value;
            $this->db->query($query);

        }
    }

    public function stateUpdateCompany($chk)
    {
        foreach ($chk['chk'] as $key => $value) {

            $query = "UPDATE es_autorenderCode SET calculate='{$chk['orderStatusBottom']}' WHERE no = " . $value;
            $this->db->query($query);

        }
    }
    
    public function coupon_generator()
    {
        $len = 12;
        $chars = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789";

        srand((double)microtime()*1000000);

        $i = 0;
        $str = "";

        while ($i < $len) {
            $num = rand() % strlen($chars);
            $tmp = substr($chars, $num, 1);
            $str .= $tmp;
            $i++;
        }

        $str = preg_replace("/([0-9A-Z]{4})([0-9A-Z]{4})([0-9A-Z]{4})([0-9A-Z]{4})/", "\1-\2-\3-\4", $str);

        return $str;
    }


    public function getListLog($getValue = null)
    {


        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'no';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['createNm', 'odNo', 'odSno', 'odName', 'odId'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['type']!="") {
            $arrWhere[] = " type = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['type']);
        }

        if ($getValue['state']!="") {
            $arrWhere[] = " state = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['state']);
        }

        if ($getValue['payment']!="") {
            $arrWhere[] = " payment = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['payment']);
        }


        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderLog WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderLog ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;

    }


    public function deleteLog($chk)
    {
        foreach ($chk['chk'] as $key => $value) {

            /* DB 값 삭제 */
            $query = "DELETE FROM es_autorenderLog WHERE no = " . $value;
            $this->db->query($query);

        }
    }


    public function getListUser($getValue = null)
    {

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'no';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['createNm', 'odNo', 'odSno', 'odName', 'odId'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['type']!="") {
            $arrWhere[] = " type = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['type']);
        }

        if ($getValue['state']!="") {
            $arrWhere[] = " state = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['state']);
        }

        if ($getValue['payment']!="") {
            $arrWhere[] = " payment = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['payment']);
        }


        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderUser WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_autorenderUser ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


    public function getUserDetail($getValue)
    {

        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_autorenderUser WHERE no = '{$getValue['no']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['no'] = $v['no'];
                $data['createNm'] = $v['createNm'];
                $data['odNo'] = $v['odNo'];
                $data['odSno'] = $v['odSno'];
                $data['odId'] = $v['odId'];
                $data['odName'] = $v['odName'];
                $data['goodsType'] = $v['goodsType'];
                $data['goodsNm'] = $v['goodsNm'];
                $data['goodsCd'] = $v['goodsCd'];
                $json['basicInfo'] = $v['basicInfo'];
                $json['sceneInfoUser'] = $v['sceneInfoUser'];
                $data['sianPath'] = $v['sianPath'];
                $data['moviePath'] = $v['moviePath'];
                $data['payment'] = $v['payment'];
                $data['modifyCount'] = $v['modifyCount'];
                $data['state'] = $v['state'];
                $data['regDt'] = $v['regDt'];
                $data['modDt'] = $v['modDt'];
            }

            $admin = array();
            $admin['goodsNo'] = $data['goodsCd'];
            $admin['goodsType'] = $data['goodsType'];
            $adminData = $this->getVideoAdmin($admin)['data'];

            $data['basicInfoAdmin'] = $adminData['basicInfo'];
            $data['basicInfo'] = json_decode($json['basicInfo'], true);
            $data['sceneInfoUser'] = json_decode($json['sceneInfoUser'], true);

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

            return $getData;
        }
    }


    public function getUploadListUser($getValue = null)
    {

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'no';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['createNm', 'odNo', 'odSno', 'odName', 'odId'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['type']!="") {
            $arrWhere[] = " type = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['type']);
        }

        if ($getValue['state']!="") {
            $arrWhere[] = " state = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['state']);
        }

        if ($getValue['payment']!="") {
            $arrWhere[] = " payment = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['payment']);
        }


        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_create_uploads WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_create_uploads ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }
}