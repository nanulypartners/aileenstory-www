<?php

namespace Component\Autorender;

use Component\Database\DBTableField;
use Request;
use Framework\Debug\Exception\AlertBackException;
use Component\Storage\Storage;
use Session;
use Component\Sms\SmsAuto;
use Component\Sms\SmsAutoCode;

class AutorenderUser
{
    public function __construct()
    {
        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }
    }

    public function check3rd($postValue){

        $result = false;

        $memInfo = Session::get('member');

        $strSQL = " SELECT count(*) as cnt FROM es_autorenderUser WHERE goodsCd = '{$postValue['goodsNo']}' AND state='0' AND odId='{$memInfo['memId']}'";
        $rst = $this->db->fetch($strSQL);        

        if ($rst['cnt']>=3) {
            $result = true;
        }

        return $result;
    }


    //관리자에 자동렌더링 상품이 등록되어 있고 노출중인지 체크
    public function checkAdminRegister($postValue)
    {

        if (empty($postValue['goodsType'])) {
            $postValue['goodsType'] = 'goods';
        }

        $strSQL = " SELECT * FROM es_autorenderAdmin WHERE goodsCd = '{$postValue['goodsNo']}' AND type='0' AND goodsType='{$postValue['goodsType']}' limit 1";
        $result = $this->db->fetch($strSQL);

        return $result;
    }

	//제작번호가 DB에 존재하는지 체크
    public function checkUserRegister($postValue)
    {

        $strSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$postValue['createNo']}' limit 1";
        $result = $this->db->fetch($strSQL);

        return $result;

    }

    //제작번호 생성
    public function createNm($adminData)
    {

        $memInfo = Session::get('member');

    	//제작번호 생성
    	$createNm = time().mt_rand(000, 999);

    	//제작번호 파일서버 전송
    	$arrData['createNm'] = $createNm;

    	$arrData['odId'] = $memInfo['memId'];
    	$arrData['odName'] = $memInfo['memNm'];

    	$arrData['goodsCd'] = $adminData['goodsCd'];
    	$arrData['goodsNm'] = $adminData['goodsNm'];
    	$arrData['goodsType'] = $adminData['goodsType'];

        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['nanuly_type'] = "UserCreateNmInsert";
        $url = "https://app.aileenstory.com/api/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);


        if ($output['state']!="success") {
            $createNm = '';
        }
        //제작번호 파일서버 전송

    	return $createNm;

    }



    public function delReset($data, $type=0)
    {
        $strSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$data->createNm}' limit 1";
        $result = $this->db->fetch($strSQL);

        $arrBind = [];

        if (!empty($result['createNm'])) {
        //제작번호가 있으면 업데이트
            $arrData = [
                "createNm" => $data->createNm,
                "odNo" => $data->odNo,
                "odSno" => $data->odSno,
                "odId" => $data->odId,
                "odName" => $data->odName,
                "goodsType" => $data->goodsType,
                "goodsNm" => $data->goodsNm,
                "goodsCd" => $data->goodsCd,
                "basicInfo" => $data->basicInfo,
                "sceneInfoUser" => $data->sceneInfoUser,
                "sianPath" => $data->sianPath,
                "moviePath" => $data->moviePath,
                "payment" => $data->payment,
                "renderType" => $data->renderType,
                "modifyCount" => $data->modifyCount,
                "state" => $data->state
            ];


            $arrExclude = [
                'no',
                'goodsType',
                'goodsNm',
                'goodsCd'
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'update', '', $arrExclude);
            $this->db->bind_param_push($arrBind['bind'], 's', $arrData['createNm']);
            $this->db->set_update_db('es_autorenderUser', $arrBind['param'], 'createNm = ?', $arrBind['bind']);

        }
    }

    public function deleteData($data, $type=0)
    {
        $strSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$data->createNm}' limit 1";
        $result = $this->db->fetch($strSQL);

        $arrBind = [];

        if (!empty($result['createNm'])) {
        //제작번호가 있으면 업데이트
            $arrData = [
                "createNm" => $data->createNm,
                "odNo" => $data->odNo,
                "odSno" => $data->odSno,
                "odId" => $data->odId,
                "odName" => $data->odName,
                "goodsType" => $data->goodsType,
                "goodsNm" => $data->goodsNm,
                "goodsCd" => $data->goodsCd,
                "basicInfo" => $data->basicInfo,
                "sceneInfoUser" => $data->sceneInfoUser,
                "sianPath" => $data->sianPath,
                "moviePath" => $data->moviePath,
                "payment" => $data->payment,
                "renderType" => $data->renderType,
                "modifyCount" => $data->modifyCount,
                "state" => $data->state
            ];


            $arrExclude = [
                'no',
                'goodsType',
                'goodsNm',
                'goodsCd'
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'update', '', $arrExclude);
            $this->db->bind_param_push($arrBind['bind'], 's', $arrData['createNm']);
            $this->db->set_update_db('es_autorenderUser', $arrBind['param'], 'createNm = ?', $arrBind['bind']);

        }
    }

    public function insertData($data, $type=0)
    {

        $strSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$data->createNm}' limit 1";
        $result = $this->db->fetch($strSQL);

        $arrBind = [];

        if (empty($result['createNm'])) {
        //제작번호가 없으면 입력

            $arrData = [
                "createNm" => $data->createNm,
                "odNo" => $data->odNo,
                "odSno" => $data->odSno,
                "odId" => $data->odId,
                "odName" => $data->odName,
                "goodsType" => $data->goodsType,
                "goodsNm" => $data->goodsNm,
                "goodsCd" => $data->goodsCd,
                "basicInfo" => $data->basicInfo,
                "sceneInfoUser" => $data->sceneInfoUser,
                "sianPath" => $data->sianPath,
                "moviePath" => $data->moviePath,
                "payment" => $data->payment,
                "renderType" => $data->renderType,
                "modifyCount" => $data->modifyCount,
                "state" => $data->state
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'insert');
            $this->db->set_insert_db('es_autorenderUser', $arrBind['param'], $arrBind['bind'], 'y');

        } else {
        //제작번호가 있으면 업데이트
            $arrData = [
                "createNm" => $data->createNm,
                "odNo" => $data->odNo,
                "odSno" => $data->odSno,
                "odId" => $data->odId,
                "odName" => $data->odName,
                "goodsType" => $data->goodsType,
                "goodsNm" => $data->goodsNm,
                "goodsCd" => $data->goodsCd,
                "basicInfo" => $data->basicInfo,
                "sceneInfoUser" => $data->sceneInfoUser,
                "sianPath" => $data->sianPath,
                "moviePath" => $data->moviePath,
                "payment" => $data->payment,
                "renderType" => $data->renderType,
                "modifyCount" => $data->modifyCount,
                "state" => $data->state
            ];


            $arrExclude = [
                'no',
                'goodsType',
                'goodsNm',
                'goodsCd'
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'update', '', $arrExclude);
            $this->db->bind_param_push($arrBind['bind'], 's', $arrData['createNm']);
            $this->db->set_update_db('es_autorenderUser', $arrBind['param'], 'createNm = ?', $arrBind['bind']);


            if ($type!=1) {
                if ($data->state=="1" || $data->state=="3") {
                    //$this->insertLog($data, 1);
                    if ($data->state=="3") {
                        $this->sendKakao($data);
                    }
                }
            }

        }

    }



    public function insertDataImsi($data)
    {

        $strSQL = " SELECT * FROM es_autorenderUserImsi WHERE createNm = '{$data->createNm}' limit 1";
        $result = $this->db->fetch($strSQL);

        $arrBind = [];

        if (empty($result['createNm'])) {
        //제작번호가 없으면 입력

            $arrData = [
                "createNm" => $data->createNm,
                "odNo" => $data->odNo,
                "odSno" => $data->odSno,
                "odId" => $data->odId,
                "odName" => $data->odName,
                "goodsType" => $data->goodsType,
                "goodsNm" => $data->goodsNm,
                "goodsCd" => $data->goodsCd,
                "basicInfo" => $data->basicInfo,
                "sceneInfoUser" => $data->sceneInfoUser,
                "sianPath" => $data->sianPath,
                "moviePath" => $data->moviePath,
                "payment" => $data->payment,
                "modifyCount" => $data->modifyCount,
                "state" => $data->state
            ];

            $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUserImsi(), $arrData, 'insert');
            $this->db->set_insert_db('es_autorenderUserImsi', $arrBind['param'], $arrBind['bind'], 'y');

        } 

    }


    public function insertLog($data, $type=0)
    {

        if ($type==0) {
            $data->state = 0;
        } else {
            $data->code = '';
        }

        if (!empty($data->oldRenderType)) {
            $data->renderType = $data->oldRenderType;
        }

        $arrData = [
            "type" => $type,
            "renderType" => $data->renderType,
            "payment" => $data->payment,
            "createNm" => $data->createNm,
            "odNo" => $data->odNo,
            "odSno" => $data->odSno,
            "odId" => $data->odId,
            "odName" => $data->odName,
            "goodsType" => $data->goodsType,
            "goodsNm" => $data->goodsNm,
            "goodsCd" => $data->goodsCd,
            "code" => $data->code,
            "state" => $data->state
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableAutorenderLog(), $arrData, 'insert');
        $this->db->set_insert_db('es_autorenderLog', $arrBind['param'], $arrBind['bind'], 'y');

    }


    public function sendKakao($data)
    {

        if (!empty($data->odId)) {

            $strSQL = " SELECT * FROM es_member WHERE memId = '{$data->odId}' limit 1";
            $result = $this->db->fetch($strSQL);

            if (!empty($result['cellPhone'])) {

                $cellPhone = str_replace("-", "", $result['cellPhone']);

                $contents = "[#{rc_mallNm}]\n#{orderName}님이 요청하신 영상이 렌더링 완료되었습니다.\n\n마이페이지에서 완성된 영상 확인 및 다운로드 가능합니다.\n\n이용해주셔서 감사합니다:)\n\n▷[#{rc_mallNm}] 바로가기 :\n#{shopUrl}";

                $aBasicInfo = gd_policy('basic.info');
                $replaceArguments = ['rc_mallNm' => \Globals::get('gMall.mallNm'), 'shopUrl' => $aBasicInfo['mallDomain'], 'orderName'=>$result['memNm']];

                $sms = new \Component\Sms\SmsAuto;
                $sms->setReplaceArguments($replaceArguments);
                $contents = $sms->replaceContentsKakao($contents);
         
                $oKakao = new \Component\Member\KakaoAlrim;

                $smsUtil = \App::load('Component\\Sms\\SmsUtil');
                $aSender = $smsUtil->getSender();

                $receiverData = [];
                $receiverData[0]['scmNo'] = DEFAULT_CODE_SCMNO;
                $receiverData[0]['memNo'] = $result['memNo'];
                $receiverData[0]['memNm'] = $result['memNm'];
                $receiverData[0]['smsFl'] = 'n';
                $receiverData[0]['cellPhone'] = $cellPhone;

                $logData = [];
                $logData['receiver']['smsAutoType'] = 'member';
                $logData['receiver']['type'] = 'each';
                $logData['receiver']['scmNo'] = DEFAULT_CODE_SCMNO;

                $logData['receiver']['dbTable'] = 'member';
                $logData['receiver']['date'] = date('Y-m-d H:i:s');
                $logData['smsAutoCode'] = 'ORDER';

                $logData['reserve']['mode'] = 'reserve';
                $logData['reserve']['date'] = date('Y-m-d H:i:s');
                $logData['reserve']['dbTable'] = 'member';
                $logData['reserve']['smsAutoCode'] = 'ORDER';

                $aSmsLog = [
                    'sendFl'            => 'kakao',
                    'smsType'           => 'order',
                    'smsDetailType'     => 'ORDER',
                    'sendType'          => 'send',
                    'subject'           => '영상완료',
                    'contents'          => $contents,
                    'receiverCnt'       => count($receiverData),
                    'replaceCodeType'   => '',
                    'sendDt'            => date('Y-m-d H:i:s'),
                    'smsSendKey'        => '',
                    'smsAutoSendOverFl' => 'none',
                    'code'              => 'order_64'
                ];
                $aLogData = $logData;
                $receiverForSaveSmsSendList = $receiverData;

                $oKakao->sendKakaoAlrim($aSmsLog, $aSender, $aLogData, $receiverForSaveSmsSendList, $replaceArguments, $contents);

            }

        }
        
    }


    public function getListOrder($getValue = null)
    {

        $memInfo = Session::get('member');

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 10);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        $arrWhere[] = " T3.memNo = ? ";
        $this->db->bind_param_push($arrBind, 's', $memInfo['memNo']);

        $arrWhere[] = 'T1.orderStatus != ?';
        $this->db->bind_param_push($arrBind, 's', 'o1');

        $arrWhere[] = " (T1.goodsType='goods' or (T1.goodsType='addGoods' AND T1.goodsModelNo!='')) ";

        $arrWhere[] = "T1.orderStatus not like 'c%' ";
        $arrWhere[] = "T1.orderStatus not like 'f%' ";
        $arrWhere[] = "T1.orderStatus not like 'b%' ";
        $arrWhere[] = "T1.orderStatus not like 'r%' ";

        $arrWhere[] = " T2.odNo is NULL ";

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_order T1 LEFT JOIN es_autorenderByOrderNo T2 ON T1.orderNo=T2.odNo WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        //$this->db->strField = " T1.*, T4.imageName, T5.imagePath, T6.status ";
        $this->db->strField = " T1.*, T4.imageName, T5.imagePath, T6.state";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        //$strSQL = "SELECT " . array_shift($query) ." FROM es_orderGoods T1 LEFT JOIN es_autorenderByOrderNo T2 ON T1.orderNo=T2.odNo LEFT JOIN es_order T3 ON T1.orderNo=T3.orderNo LEFT JOIN es_goodsImage T4 ON T1.goodsNo=T4.goodsNo AND T4.imageKind='main' LEFT JOIN es_goods T5 ON T1.goodsNo=T5.goodsNo LEFT JOIN es_goodsPic T6 ON T1.orderNo=T6.orderNo " . implode(' ', $query);
        $strSQL = "SELECT " . array_shift($query) ." FROM es_orderGoods T1 LEFT JOIN es_autorenderByOrderNo T2 ON T1.orderNo=T2.odNo LEFT JOIN es_order T3 ON T1.orderNo=T3.orderNo LEFT JOIN es_goodsImage T4 ON T1.goodsNo=T4.goodsNo AND T4.imageKind='main' LEFT JOIN es_goods T5 ON T1.goodsNo=T5.goodsNo LEFT JOIN es_create_uploads T6 ON T6.odSno=T1.sno " . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


    public function getListUser($getValue = null)
    {

        $memInfo = Session::get('member');

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'no';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 10);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        $arrWhere[] = " odId = ? ";
        $this->db->bind_param_push($arrBind, 's', $memInfo['memId']);

        $arrWhere[] = " state != ? ";
        $this->db->bind_param_push($arrBind, 's', 4);

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_autorenderUser WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " T1.*, T2.imageName, T3.imagePath ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = "SELECT " . array_shift($query) ." FROM es_autorenderUser T1 LEFT JOIN es_goodsImage T2 ON T1.goodsCd=T2.goodsNo AND T2.imageKind='main' LEFT JOIN es_goods T3 ON T1.goodsCd=T3.goodsNo " . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


    public function getUserDetail($postValue)
    {
        if(gd_isset($postValue)) {
            $strSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$postValue['createNm']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['no'] = $v['no'];
                $data['createNm'] = $v['createNm'];
                $data['odNo'] = $v['odNo'];
                $data['odSno'] = $v['odSno'];
                $data['odId'] = $v['odId'];
                $data['odName'] = $v['odName'];
                $data['goodsType'] = $v['goodsType'];
                $data['goodsNm'] = $v['goodsNm'];
                $data['goodsCd'] = $v['goodsCd'];
                $data['sianPath'] = $v['sianPath'];
                $data['moviePath'] = $v['moviePath'];
                $data['payment'] = $v['payment'];
                $data['modifyCount'] = $v['modifyCount'];
                $data['state'] = $v['state'];
                $data['regDt'] = $v['regDt'];
                $data['modDt'] = $v['modDt'];
            }

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

            return $getData;
        }
    }


    public function codeCheck($postValue)
    {

        $code = explode("-", $postValue['code']);

        $return = "";

        if (empty($code[0]) || empty($code[1])) {
            $return = "error";
        } else {
            $this->db->strField .= 't1.*';
            $this->db->strJoin = ' LEFT JOIN es_autorenderCodeCompany AS t2 ON t1.company = t2.no';
            $this->db->strWhere = 't1.useYn=0 AND t2.companyCode = ? AND t1.code = ?';
            $this->db->strLimit = '1';

            $bindParam = [];

            $this->db->bind_param_push($bindParam, 's', $code[0]);
            $this->db->bind_param_push($bindParam, 's', $code[1]);

            $query = $this->db->query_complete();
            $strSQL = 'SELECT ' . array_shift($query) . ' FROM es_autorenderCode AS t1' . implode(' ', $query);

            $result = $this->db->query_fetch($strSQL, $bindParam, false);

            if (!empty($result['no'])) {

                //제작코드 유저업로드 불러오기
                $userData = $this->getUserDetail($postValue)['data'];

                //재결제일 경우
                if ($userData['payment']>0) {
                    $modifyCount = 0;
                    $state = 0;
                    $orderNo = $userData['odNo'];
                    $goodsSno = $userData['odSno'];
                } else {
                    $modifyCount = 0;
                    $state = 1;
                    $orderNo = "";
                    $goodsSno = "";
                }

                $data = array();
                $data['renderType'] = 1;
                $data['payment'] = $userData['payment']+1;
                $data['createNm'] = $postValue['createNm'];
                $data['odNo'] = $orderNo;
                $data['odSno'] = $goodsSno;
                $data['odId'] = $userData['odId'];
                $data['odName'] = $userData['odName'];
                $data['goodsType'] = $userData['goodsType'];
                $data['goodsNm'] = $userData['goodsNm'];
                $data['goodsCd'] = $userData['goodsCd'];
                $data['code'] = $postValue['code'];
                $data['modifyCount'] = $modifyCount;
                $data['moviePath'] = "";
                $data['state'] = $state;

                //파일서버 전송
                $data['nanuly'] = "sksnfl2020@";
                $data['nanuly_type'] = "UserCodeInsert";
                $url = "https://app.aileenstory.com/api/autorender_api.php";
                $json_data = json_encode((object)$data, JSON_UNESCAPED_UNICODE);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                          'Content-Type: application/json',
                                          'Content-Length: '.strlen($json_data)));
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POST, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $output = json_decode($output, true);

                if ($output['state']!="success") {
                    $return = 'apiError';
                } else {
                    //로그에 기록
                    //$this->insertLog((object)$data);
                    //$this->insertLog((object)$data, 1);

                    //제작정보 불러오기
                    $userSQL = " SELECT * FROM es_autorenderUser WHERE createNm = '{$postValue['createNm']}'";
                    $userResult = $this->db->query_fetch($userSQL);

                    //상품정보 불러오기
                    $goodsSQL = " SELECT * FROM es_goods WHERE goodsNo = '{$userResult[0]['goodsCd']}'";
                    $goodsResult = $this->db->query_fetch($goodsSQL);         

                    //제휴코드에 업데이트
                    $arrData = [
                        "no" => $result['no'],
                        "useYn" => "1",
                        "useId" => $userData['odId']."(".$userData['odName'].")",
                        "useCreateNm" => $postValue['createNm'],
                        "goodsName" => $goodsResult[0]['goodsNm'],
                        "price" => $goodsResult[0]['goodsPrice'],
                        "useDt" => date("Y-m-d H:i:s")
                    ];

                    $arrExclude = [
                        'no',
                        'company',
                        'code'
                    ];

                    $arrBind = $this->db->get_binding(DBTableField::tableAutorenderCode(), $arrData, 'update', '', $arrExclude);
                    $this->db->bind_param_push($arrBind['bind'], 's', $arrData['no']);
                    $this->db->set_update_db('es_autorenderCode', $arrBind['param'], 'useYn = 0 AND no = ?', $arrBind['bind']);

                    //업로드 제작번호 업데이트
                    $arrData = [
                        "renderType" => 1,
                        "payment" => $userData['payment']+1,
                        "modifyCount" => $modifyCount,
                        "state" => $state,
                        "moviePath" => "",
                        "odNo" => $orderNo,
                        "odSno" => $goodsSno,
                        "createNm" => $postValue['createNm']
                    ];

                    $arrExclude = [
                        'no',
                        'goodsType',
                        'goodsNm',
                        'goodsCd',
                        'createNm',
                        'odId',
                        'odName',
                        'basicInfo',
                        'sceneInfoUser'
                    ];

                    $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'update', '', $arrExclude);
                    $this->db->bind_param_push($arrBind['bind'], 's', $arrData['createNm']);
                    $this->db->set_update_db('es_autorenderUser', $arrBind['param'], 'createNm = ?', $arrBind['bind']);

                    $return = "ok";
                }
                //파일서버 전송
                
            } else {
                $return = "notFound";
            }
        }

        return $return;

    }

}