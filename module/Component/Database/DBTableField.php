<?php

namespace Component\Database;

use Session;

class DBTableField extends \Bundle\Component\Database\DBTableField
{

    /**
     * 생성잝
     */
    public function __construct()
    {
    }

    public static function tableGoodsPicSetup()
    {
        $arrField = [
            ['val' => 'goodsPicSetupNo', 'typ' => 'i', 'def' => null],
            ['val' => 'goodsNo', 'typ' => 'i', 'def' => null],
            ['val' => 'type', 'typ' => 's', 'def' => null],
            ['val' => 'value', 'typ' => 's', 'def' => null],
            ['val' => 'order', 'typ' => 'i', 'def' => null],
        ];
        return $arrField;
    }

    public static function tableGoodsPic()
    {
        $arrField = [
            ['val' => 'goodsPicNo', 'typ' => 'i', 'def' => null],
            ['val' => 'picType', 'typ' => 's', 'def' => 'aileen'],
            ['val' => 'orderNo', 'typ' => 's', 'def' => null],
            ['val' => 'goodsNo', 'typ' => 'i', 'def' => null],
            ['val' => 'orderName', 'typ' => 's', 'def' => null],
            ['val' => 'orderPhone', 'typ' => 's', 'def' => null],
            ['val' => 'status', 'typ' => 's', 'def' => null],
            ['val' => 'update', 'typ' => 's', 'def' => null],
            ['val' => 'regdate', 'typ' => 's', 'def' => null]
        ];
        return $arrField;
    }

    public static function tableGoodsPicFile()
    {
        $arrField = [
            ['val' => 'goodsPicFileNo', 'typ' => 'i', 'def' => null],
            ['val' => 'goodsPicNo', 'typ' => 'i', 'def' => null],
            ['val' => 'order', 'typ' => 'i', 'def' => null],
            ['val' => 'path', 'typ' => 's', 'def' => null],
            ['val' => 'name', 'typ' => 's', 'def' => null]
        ];
        return $arrField;
    }

    public static function tableGoodsPicCont()
    {
        $arrField = [
            ['val' => 'goodsPicContNo', 'typ' => 'i', 'def' => null],
            ['val' => 'goodsPicNo', 'typ' => 'i', 'def' => null],
            ['val' => 'type', 'typ' => 's', 'def' => null],
            ['val' => 'order', 'typ' => 'i', 'def' => null],
            ['val' => 'cont', 'typ' => 's', 'def' => null]
        ];
        return $arrField;
    }

    public static function tableGoodsPicMake()
    {
        $arrField = [
            ['val' => 'goodsPicMakeNo', 'typ' => 'i', 'def' => null],
            ['val' => 'goodsPicNo', 'typ' => 'i', 'def' => null],
            ['val' => 'playVideo', 'typ' => 's', 'def' => null],
            ['val' => 'downVideo', 'typ' => 's', 'def' => null]
        ];
        return $arrField;
    }

    public static function tableAutorenderAdmin()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'type', 'typ' => 'i', 'def' => 0, 'name' => '제작타입(0:셀프, 1:제작요청)'], // 제작타입(0:셀프, 1:제작요청)
            ['val' => 'goodsType', 'typ' => 's', 'def' => '', 'name' => '상품구분'], // 상품구분
            ['val' => 'goodsNm', 'typ' => 's', 'def' => '', 'name' => '상품명'], // 상품명
            ['val' => 'goodsCd', 'typ' => 's', 'def' => null, 'name' => '상품코드'], // 상품코드
            ['val' => 'useYn', 'typ' => 'i', 'def' => null, 'name' => '노출여부(0:비노출, 1:노출)'], // 노출여부
            ['val' => 'sampleVideoUrl', 'typ' => 's', 'def' => null, 'name' => '샘플 영상 주소'], // 샘플 영상 주소
            ['val' => 'templateId', 'typ' => 's', 'def' => null, 'name' => '템플릿 아이디'], // 템플릿 아이디
            ['val' => 'templateName', 'typ' => 's', 'def' => null, 'name' => '템플릿 이름'], // 템플릿 이름
            ['val' => 'templatePath', 'typ' => 's', 'def' => null, 'name' => '템플릿 경로'], // 템플릿 경로
            ['val' => 'playTime', 'typ' => 's', 'def' => null, 'name' => '재생시간'], // 재생시간
            ['val' => 'bgmLayerName', 'typ' => 's', 'def' => null, 'name' => '배경음 레이어이름'], // 배경음 레이어이름
            ['val' => 'bgmName', 'typ' => 's', 'def' => null, 'name' => '배경음 이름'], // 배경음 이름
            ['val' => 'bgmChange', 'typ' => 's', 'def' => null, 'name' => '배경음 변경가능'], // 배경음 변경가능
            ['val' => 'basicInfo', 'typ' => 'j', 'def' => null, 'name' => '기본정보'], // 기본정보
            ['val' => 'sceneInfo', 'typ' => 'j', 'def' => null, 'name' => '장면정보'], // 장면정보
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableAutorenderUser()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'createNm', 'typ' => 's', 'def' => null, 'name' => '제작번호'], // 제작번호
            ['val' => 'odNo', 'typ' => 's', 'def' => null, 'name' => '주문번호'], // 주문번호
            ['val' => 'odSno', 'typ' => 's', 'def' => null, 'name' => '상품주문번호'], // 상품주문번호
            ['val' => 'odId', 'typ' => 's', 'def' => null, 'name' => '주문자아이디'], // 주문자아이디
            ['val' => 'odName', 'typ' => 's', 'def' => null, 'name' => '주문자명'], // 주문자명
            ['val' => 'goodsType', 'typ' => 's', 'def' => '', 'name' => '상품구분'], // 상품구분
            ['val' => 'goodsNm', 'typ' => 's', 'def' => '', 'name' => '상품명'], // 상품명
            ['val' => 'goodsCd', 'typ' => 's', 'def' => null, 'name' => '상품코드'], // 상품코드
            ['val' => 'basicInfo', 'typ' => 'j', 'def' => null, 'name' => '기본정보'], // 기본정보
            ['val' => 'sceneInfoUser', 'typ' => 'j', 'def' => null, 'name' => '장면정보'], // 장면정보
            ['val' => 'sianPath', 'typ' => 's', 'def' => null, 'name' => '시안경로'], // 시안경로
            ['val' => 'moviePath', 'typ' => 's', 'def' => null, 'name' => '영상경로'], // 영상경로
            ['val' => 'payment', 'typ' => 'i', 'def' => 0, 'name' => '결제여부(0:미결제, 1:결제완료)'], // 결제여부
            ['val' => 'renderType', 'typ' => 'i', 'def' => 0, 'name' => '렌더링타입'], // 렌더링타입
            ['val' => 'modifyCount', 'typ' => 'i', 'def' => 0, 'name' => '수정가능횟수(0:가능, 1:불가능)'], // 수정가능횟수
            ['val' => 'state', 'typ' => 'i', 'def' => 0, 'name' => '상태(0:제작대기, 1:제작요청, 2:제작중, 3:제작완료, 4:자동삭제)'], // 상태
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableAutorenderLog()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'type', 'typ' => 'i', 'def' => null, 'name' => '분류(0:제휴코드, 1:제작상태)'], // 분류(제휴코드, 제작상태)
            ['val' => 'renderType', 'typ' => 'i', 'def' => null, 'name' => '렌더링타입'], // 렌더링타입
            ['val' => 'payment', 'typ' => 'i', 'def' => null, 'name' => '결제(0:무료, 1:유료)'], // 결제(유료/무료)
            ['val' => 'createNm', 'typ' => 's', 'def' => null, 'name' => '제작번호'], // 제작번호
            ['val' => 'odNo', 'typ' => 's', 'def' => null, 'name' => '주문번호'], // 주문번호
            ['val' => 'odSno', 'typ' => 's', 'def' => null, 'name' => '상품주문번호'], // 상품주문번호
            ['val' => 'odId', 'typ' => 's', 'def' => null, 'name' => '주문자아이디'], // 주문자아이디
            ['val' => 'odName', 'typ' => 's', 'def' => null, 'name' => '주문자명'], // 주문자명
            ['val' => 'goodsType', 'typ' => 's', 'def' => '', 'name' => '상품구분'], // 상품구분
            ['val' => 'goodsNm', 'typ' => 's', 'def' => '', 'name' => '상품명'], // 상품명
            ['val' => 'goodsCd', 'typ' => 's', 'def' => null, 'name' => '상품코드'], // 상품코드
            ['val' => 'code', 'typ' => 's', 'def' => null, 'name' => '제휴코드'], // 제휴코드
            ['val' => 'state', 'typ' => 'i', 'def' => null, 'name' => '상태(0:해당없음, 1:제작요청, 3:제작완료)'], // 상태
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableAutorenderCode()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'company', 'typ' => 's', 'def' => null, 'name' => '제휴사'], // 제휴사
            ['val' => 'code', 'typ' => 's', 'def' => null, 'name' => '제휴코드'], // 제휴코드
            ['val' => 'useYn', 'typ' => 'i', 'def' => null, 'name' => '사용여부(0:미사용, 1:사용)'], // 사용여부
            ['val' => 'useId', 'typ' => 's', 'def' => null, 'name' => '사용아이디'], // 사용아이디
            ['val' => 'useCreateNm', 'typ' => 's', 'def' => null, 'name' => '사용제작번호'], // 사용제작번호
            ['val' => 'goodsName', 'typ' => 's', 'def' => null, 'name' => '상품이름'], // 상품이름
            ['val' => 'price', 'typ' => 's', 'def' => null, 'name' => '상품가격'], // 상품가격
            ['val' => 'calculate', 'typ' => 'i', 'def' => 0, 'name' => '정산여부(0:미정산, 1:정산)'], // 정산여부
            ['val' => 'useDt', 'typ' => 's', 'def' => null, 'name' => '사용일'], // 사용일
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableAutorenderCodeCompany()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'company', 'typ' => 's', 'def' => null, 'name' => '제휴사'], // 제휴사
            ['val' => 'companyId', 'typ' => 's', 'def' => null, 'name' => '제휴사아이디'], // 제휴사
            ['val' => 'companyCode', 'typ' => 's', 'def' => null, 'name' => '제휴사코드'], // 제휴사코드
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }


    public static function tableAutorenderByOrderNo()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'type', 'typ' => 's', 'def' => null, 'name' => '타입'], // 주문번호
            ['val' => 'odNo', 'typ' => 's', 'def' => null, 'name' => '주문번호'], // 주문번호
            ['val' => 'odSno', 'typ' => 's', 'def' => null, 'name' => '상품주문번호'], // 상품주문번호
            ['val' => 'createNm', 'typ' => 's', 'def' => null, 'name' => '제작번호'], // 제작번호
            ['val' => 'payment', 'typ' => 's', 'def' => null, 'name' => '결제여부(0:미결제, 1:결제완료)'], // 결제여부(0:미결제, 1:결제완료)
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }


    public static function tableAutorenderUserImsi()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'createNm', 'typ' => 's', 'def' => null, 'name' => '제작번호'], // 제작번호
            ['val' => 'odNo', 'typ' => 's', 'def' => null, 'name' => '주문번호'], // 주문번호
            ['val' => 'odSno', 'typ' => 's', 'def' => null, 'name' => '상품주문번호'], // 상품주문번호
            ['val' => 'odId', 'typ' => 's', 'def' => null, 'name' => '주문자아이디'], // 주문자아이디
            ['val' => 'odName', 'typ' => 's', 'def' => null, 'name' => '주문자명'], // 주문자명
            ['val' => 'goodsType', 'typ' => 's', 'def' => '', 'name' => '상품구분'], // 상품구분
            ['val' => 'goodsNm', 'typ' => 's', 'def' => '', 'name' => '상품명'], // 상품명
            ['val' => 'goodsCd', 'typ' => 's', 'def' => null, 'name' => '상품코드'], // 상품코드
            ['val' => 'basicInfo', 'typ' => 'j', 'def' => null, 'name' => '기본정보'], // 기본정보
            ['val' => 'sceneInfoUser', 'typ' => 'j', 'def' => null, 'name' => '장면정보'], // 장면정보
            ['val' => 'moviePath', 'typ' => 's', 'def' => null, 'name' => '영상경로'], // 영상경로
            ['val' => 'payment', 'typ' => 'i', 'def' => 0, 'name' => '결제여부(0:미결제, 1:결제완료)'], // 결제여부
            ['val' => 'modifyCount', 'typ' => 'i', 'def' => 0, 'name' => '수정가능횟수(0:가능, 1:불가능)'], // 수정가능횟수
            ['val' => 'state', 'typ' => 'i', 'def' => 0, 'name' => '상태(0:제작대기, 1:제작요청, 2:제작중, 3:제작완료, 4:자동삭제)'], // 상태
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }


    public static function tableMobileCardskin()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'category', 'typ' => 's', 'def' => null, 'name' => '분류'], // 분류
            ['val' => 'name', 'typ' => 's', 'def' => null, 'name' => '스킨명'], // 스킨명
            ['val' => 'code', 'typ' => 's', 'def' => null, 'name' => '스킨코드'], // 스킨코드
            ['val' => 'main_img_background_use', 'typ' => 'i', 'def' => null, 'name' => '메인틀이미지사용여부'], // 메인틀이미지사용여부
            ['val' => 'main_img_info', 'typ' => 'j', 'def' => null, 'name' => '메인틀이미지좌표'], // 메인틀이미지좌표
            ['val' => 'main_background_img', 'typ' => 's', 'def' => null, 'name' => '메인틀이미지'], // 메인틀이미지
            ['val' => 'thumb', 'typ' => 's', 'def' => null, 'name' => '썸네일'], // 썸네일
            ['val' => 'preview', 'typ' => 's', 'def' => null, 'name' => '미리보기이미지'], // 미리보기이미지
            ['val' => 'basic_info', 'typ' => 'j', 'def' => null, 'name' => '기본정보'], // 기본정보
            ['val' => 'relation_info', 'typ' => 'j', 'def' => null, 'name' => '관계정보'], // 관계정보
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableMobileCardtext()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'category', 'typ' => 's', 'def' => null, 'name' => '분류'], // 분류
            ['val' => 'name', 'typ' => 's', 'def' => null, 'name' => '예문명'], // 예문명
            ['val' => 'body', 'typ' => 's', 'def' => null, 'name' => '내용'], // 내용
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }

    public static function tableMobileCardorder()
    {
        $arrField = [
            ['val' => 'no', 'typ' => 'i', 'def' => null, 'name' => '번호'], // 번호
            ['val' => 'odno', 'typ' => 's', 'def' => null, 'name' => '주문번호'], // 주문번호
            ['val' => 'odsno', 'typ' => 's', 'def' => null, 'name' => '상품주문번호'], // 상품주문번호
            ['val' => 'odname', 'typ' => 's', 'def' => null, 'name' => '주문자명'], // 주문자명
            ['val' => 'odtel', 'typ' => 's', 'def' => null, 'name' => '주문자연락처'], // 주문자연락처
            ['val' => 'odMemNo', 'typ' => 'i', 'def' => null, 'name' => '회원번호'], // 주문자연락처
            ['val' => 'vimeoUri', 'typ' => 's', 'def' => null, 'name' => '비메오uri'], // 비메오uri
            ['val' => 'eventday', 'typ' => 's', 'def' => null, 'name' => '행사일'], // 행사일
            ['val' => 'eventtime', 'typ' => 's', 'def' => null, 'name' => '행사시간'], // 행사시간
            ['val' => 'apm', 'typ' => 's', 'def' => null, 'name' => '행사시간'], // 행사시간
            ['val' => 'hour', 'typ' => 's', 'def' => null, 'name' => '행사시간'], // 행사시간
            ['val' => 'minute', 'typ' => 's', 'def' => null, 'name' => '행사시간'], // 행사시간
            ['val' => 'url', 'typ' => 's', 'def' => null, 'name' => '초대장주소'], // 초대장주소
            ['val' => 'category', 'typ' => 's', 'def' => null, 'name' => '초대장종류'], // 초대장종류
            ['val' => 'skin', 'typ' => 's', 'def' => null, 'name' => '초대장스킨'], // 초대장스킨
            ['val' => 'state', 'typ' => 'i', 'def' => null, 'name' => '초대장상태(0:활성화, 1:비활성화)'], // 초대장스킨
            ['val' => 'regDt', 'typ' => 's', 'def' => null, 'name' => '등록일'], // 등록일
            ['val' => 'modDt', 'typ' => 's', 'def' => null, 'name' => '수정일'], // 수정일
        ];
        return $arrField;
    }
}
