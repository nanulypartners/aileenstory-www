<?php

namespace Component\Mobilecard;

use Component\Database\DBTableField;
use Request;
use Framework\Debug\Exception\AlertBackException;

class MobilecardAdmin
{
    public function __construct()
    {
        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }
    }

    public function orderCheck($postValue){

        $strSQL = " SELECT * FROM es_orderGoods WHERE sno = '{$postValue}' AND orderStatus!='o1' AND orderStatus NOT LIKE 'c%' ";
        $result = $this->db->query_fetch($strSQL);

        if($result){
            return false;
        }else{
            return true;            
        }
        
    }

    public function getOrderInfo($postValue){

        $strSQL = " SELECT * FROM es_orderInfo WHERE orderNo = '{$postValue}'";
        $result = $this->db->query_fetch($strSQL);

        return $result;
    }

    public function insertSkin($postValue){

        if(!$postValue['category'] || !$postValue['name'] || !$postValue['code']) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        //초대장 코드 중복확인
        $strSQL = " SELECT * FROM es_mobileCardskin WHERE code = '{$postValue['code']}'";
        $result = $this->db->fetch($strSQL);

        if($result) {
            throw new AlertBackException('중복된 스킨코드입니다.');
        }

        $filesValue = Request::files()->toArray();
        $filePath = 'data/goods/mobilecard/'.$postValue['category'].'/';
        $uploadDir = USERPATH.$filePath;    // 저장할 경로

        // 폴더 생성
        if (!is_dir($uploadDir)) {
            $old = umask(0);
            mkdir($uploadDir, 0707, true);
            umask($old);
        }

        $main_background_img = "";
        if($filesValue['main_background_img']['name']){
            $tmp = $filesValue['main_background_img']['tmp_name'];  // 파일경로
            $original = $filesValue['main_background_img']['name']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            // 최종 경로
            $main_background_img = $filePath.$fileRename;

            // 파일 업로드
            move_uploaded_file($tmp, $uploadDir.$fileRename);
        }

        $thumb_img = "";
        if($filesValue['thumb_img']['name']){
            $tmp = $filesValue['thumb_img']['tmp_name'];  // 파일경로
            $original = $filesValue['thumb_img']['name']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            // 최종 경로
            $thumb_img = $filePath.$fileRename;

            // 파일 업로드
            move_uploaded_file($tmp, $uploadDir.$fileRename);
        }

        $preview_img = "";
        if($filesValue['preview_img']['name']){
            $tmp2 = $filesValue['preview_img']['tmp_name'];  // 파일경로
            $original2 = $filesValue['preview_img']['name']; // 파일이름
            $pathParts2 = pathinfo($original2);  // 확장자 추출
            $fileRename2 =  uniqid("").".".$pathParts2['extension']; // 변경파일명

            // 최종 경로
            $preview_img = $filePath.$fileRename2;
        
            // 파일 업로드
            move_uploaded_file($tmp2, $uploadDir.$fileRename2);            
        }


        // 기본정보
        $postValue['basic_text'] = (object) $postValue['basic_text'];
        $basic_info = json_encode($postValue['basic_text']);

        // 관계정보
        $postValue['relation_text'] = (object) $postValue['relation_text'];
        $relation_info = json_encode($postValue['relation_text']);

        // 메인 이미지 좌표정보
        $postValue['main_img_info'] = (object) $postValue['main_img_info'];
        $main_img_info = json_encode($postValue['main_img_info']);

        $arrData = [
            "category" => $postValue['category'],
            "name" => $postValue['name'],
            "code" => $postValue['code'],
            "main_img_background_use" => $postValue['main_img_background_use'],
            "main_img_info" => $main_img_info,
            "main_background_img" => $main_background_img,
            "thumb" => $thumb_img,
            "preview" => $preview_img,
            "basic_info" => $basic_info,
            "relation_info" => $relation_info,
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableMobileCardskin(), $arrData, 'insert');
        $this->db->set_insert_db('es_mobileCardskin', $arrBind['param'], $arrBind['bind'], 'y');

    }

    public function updateSkin($postValue){

        if(!$postValue['category'] || !$postValue['name'] || !$postValue['code']) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        //초대장 코드 중복확인
        $strSQL = " SELECT * FROM es_mobileCardskin WHERE code = '{$postValue['code']}' AND no != '{$postValue['no']}'";
        $result = $this->db->fetch($strSQL);

        if($result) {
            throw new AlertBackException('중복된 스킨코드입니다.');
        }

        $filesValue = Request::files()->toArray();
        $filePath = 'data/goods/mobilecard/'.$postValue['category'].'/';
        $uploadDir = USERPATH.$filePath;    // 저장할 경로

        $strSQL = " SELECT * FROM es_mobileCardskin WHERE no = '{$postValue['no']}'";
        $result = $this->db->fetch($strSQL);

        $main_background_img = "";
        if($filesValue['main_background_img']['name']){
            if($result['main_background_img']){
                $delDir = USERPATH.$result['main_background_img'];    // 삭제할 경로
                @unlink($delDir);
            }

            $tmp = $filesValue['main_background_img']['tmp_name'];  // 파일경로
            $original = $filesValue['main_background_img']['name']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            // 최종 경로
            $main_background_img = $filePath.$fileRename;

            // 파일 업로드
            move_uploaded_file($tmp, $uploadDir.$fileRename);
        }

        $thumb_img = "";
        if($filesValue['thumb_img']['name']){
            if($result['thumb']){
                $delDir = USERPATH.$result['thumb'];    // 삭제할 경로
                @unlink($delDir);                
            }

            $tmp = $filesValue['thumb_img']['tmp_name'];  // 파일경로
            $original = $filesValue['thumb_img']['name']; // 파일이름
            $pathParts = pathinfo($original);  // 확장자 추출
            $fileRename =  uniqid("").".".$pathParts['extension']; // 변경파일명

            // 최종 경로
            $thumb_img = $filePath.$fileRename;
        
            // 파일 업로드
            move_uploaded_file($tmp, $uploadDir.$fileRename);            
        }

        $preview_img = "";
        if($filesValue['preview_img']['name']){
            if($result['preview']){
                $delDir = USERPATH.$result['preview'];    // 삭제할 경로
                @unlink($delDir);
            }

            $tmp2 = $filesValue['preview_img']['tmp_name'];  // 파일경로
            $original2 = $filesValue['preview_img']['name']; // 파일이름
            $pathParts2 = pathinfo($original2);  // 확장자 추출
            $fileRename2 =  uniqid("").".".$pathParts2['extension']; // 변경파일명

            // 최종 경로
            $preview_img = $filePath.$fileRename2;
        
            // 파일 업로드
            move_uploaded_file($tmp2, $uploadDir.$fileRename2);            
        }


        // 기본정보
        $postValue['basic_text'] = (object) $postValue['basic_text'];
        $basic_info = json_encode($postValue['basic_text']);

        // 관계정보
        $postValue['relation_text'] = (object) $postValue['relation_text'];
        $relation_info = json_encode($postValue['relation_text']);

        // 관계정보
        $postValue['main_img_info'] = (object) $postValue['main_img_info'];
        $main_img_info = json_encode($postValue['main_img_info']);

        $arrData = [
            "no" => $postValue['no'],
            "category" => $postValue['category'],
            "name" => $postValue['name'],
            "code" => $postValue['code'],
            "main_img_background_use" => $postValue['main_img_background_use'],
            "main_img_info" => $main_img_info,
            "basic_info" => $basic_info,
            "relation_info" => $relation_info,
        ];

        $arrInclude = [
            "category", "name", "code", "basic_info", "relation_info", "main_img_background_use", "main_img_info"
        ];

        if($main_background_img!=""){
            $arrData['main_background_img'] = $main_background_img;
            array_push($arrInclude, "main_background_img");
        }

        if($thumb_img!=""){
            $arrData['thumb'] = $thumb_img;
            array_push($arrInclude, "thumb");
        }

        if($preview_img!=""){
            $arrData['preview'] = $preview_img;    
            array_push($arrInclude, "preview");
        }

        $arrBind = $this->db->get_binding(DBTableField::tableMobileCardskin(), $arrData, 'update', $arrInclude);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['no']);
        $this->db->set_update_db('es_mobileCardskin', $arrBind['param'], 'no = ?', $arrBind['bind']);

    }

    public function deleteSkin($chk){

        foreach ($chk['chk'] as $key => $value) {

            $strSQL = " SELECT * FROM es_mobileCardskin WHERE no = '{$value}' ";
            $result = $this->db->query_fetch($strSQL);

            $data = $result[0];

            /* 파일 삭제 */
            $uploadDir = USERPATH.$data['main_background_img'];    // 삭제할 경로
            @unlink($uploadDir);

            $uploadDir = USERPATH.$data['thumb'];    // 삭제할 경로
            @unlink($uploadDir);

            $uploadDir = USERPATH.$data['preview'];    // 삭제할 경로
            @unlink($uploadDir);

            /* DB 값 삭제 */
            $query = "DELETE FROM es_mobileCardskin WHERE no = " . $value;
            $this->db->query($query);

        }

    }

    public function getListSkin($getValue = null){

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['name'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['category']) {
            $arrWhere[] = " category = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['category']);
        }

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 10);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_mobileCardskin WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_mobileCardskin ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;

    }

    public function getSkin($getValue = null){
        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_mobileCardskin WHERE no = '{$getValue['no']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['category'] = $v['category'];
                $data['name'] = $v['name'];
                $data['code'] = $v['code'];
                $data['main_img_background_use'] = $v['main_img_background_use'];
                $data['main_background_img'] = $v['main_background_img'];
                $data['thumb'] = $v['thumb'];
                $data['preview'] = $v['preview'];
                $data['basic_info'] = $v['basic_info'];
                $data['relation_info'] = $v['relation_info'];
                $data['main_img_info'] = $v['main_img_info'];
            }

            $data['basic_info'] = json_decode($data['basic_info'], true);
            $data['relation_info'] = json_decode($data['relation_info'], true);
            $data['main_img_info'] = json_decode($data['main_img_info'], true);

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }


    public function insertText($postValue){

        if(!$postValue['category'] || !$postValue['body'] || !$postValue['name']) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        $arrData = [
            "category" => $postValue['category'],
            "name" => $postValue['name'],
            "body" => $postValue['body'],
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableMobileCardtext(), $arrData, 'insert');
        $this->db->set_insert_db('es_mobileCardtext', $arrBind['param'], $arrBind['bind'], 'y');

    }

    public function updateText($postValue){

        if(!$postValue['category'] || !$postValue['body'] || !$postValue['name']) {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        $arrData = [
            "no" => $postValue['no'],
            "category" => $postValue['category'],
            "name" => $postValue['name'],
            "body" => $postValue['body'],
        ];

        $arrBind = $this->db->get_binding(DBTableField::tableMobileCardtext(), $arrData, 'update');
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['no']);
        $this->db->set_update_db('es_mobileCardtext', $arrBind['param'], 'no = ?', $arrBind['bind']);

    }

    public function deleteText($chk){
        /* DB 값 삭제 */
        foreach ($chk['chk'] as $key => $value) {

            $query = "DELETE FROM es_mobileCardtext WHERE no = " . $value;
            $this->db->query($query);

        }
    }

    public function getListText($getValue = null){

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['name', 'body'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['category']) {
            $arrWhere[] = " category = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['category']);
        }

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 10);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_mobileCardtext WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_mobileCardtext ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;

    }

    public function getText($getValue = null){
        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_mobileCardtext WHERE no = '{$getValue['no']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['category'] = $v['category'];
                $data['name'] = $v['name'];
                $data['body'] = $v['body'];
            }
            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }


    public function getLog($getValue = null){
        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_mobileCardorder WHERE no = '{$getValue['no']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['no'] = $v['no'];
                $data['odno'] = $v['odno'];
                $data['odsno'] = $v['odsno'];
                $data['odname'] = $v['odname'];
                $data['odtel'] = $v['odtel'];
                $data['eventday'] = $v['eventday'];
                $data['apm'] = $v['apm'];
                $data['hour'] = $v['hour'];
                $data['minute'] = $v['minute'];
                $data['url'] = $v['url'];
                $data['category'] = $v['category'];
                $data['skin'] = $v['skin'];
                $data['state'] = $v['state'];
                $data['regDt'] = $v['regDt'];
            }

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }

    public function updateLog($postValue){

        if(!$postValue['regDt'] || $postValue['state']=="") {
            throw new AlertBackException('누락된 필드가 있습니다.');
        }

        $strSQL = " UPDATE es_mobileCardorder SET state='{$postValue['state']}', regDt='{$postValue['regDt']}' WHERE no = '{$postValue['no']}' ";
        $result = $this->db->query($strSQL);

    }

    public function getListOrder($getValue = null){

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        if ($getValue['keyword']) {
            if (in_array($getValue['searchField'], ['odno', 'odsno', 'odname', 'odtel'])) {
                $arrWhere[] = $getValue['searchField'] . " like concat('%',?,'%') ";
                $this->db->bind_param_push($arrBind, 's', $getValue['keyword']);
            }
        }

        if ($getValue['category']) {
            $arrWhere[] = " category = ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['category']);
        }

        if ($getValue['state']!='') {
            $arrWhere[] = " state = ? ";
            $this->db->bind_param_push($arrBind, 's', (string)$getValue['state']);
        }

        if($getValue['treatDate']['start']){
            $arrWhere[] = " eventday >= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['start']);
        }

        if($getValue['treatDate']['end']){
            $arrWhere[] = " eventday <= ? ";
            $this->db->bind_param_push($arrBind, 's', $getValue['treatDate']['end']);
        }

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 20);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_mobileCardorder WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " * ";
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_mobileCardorder ' . implode(' ', $query);
        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;

    }

}