<?php

namespace Component\Mobilecard;

use Component\Database\DBTableField;
use Request;
use Framework\Debug\Exception\AlertBackException;

class MobilecardLog
{
    public function __construct()
    {
        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }
    }

    public function updateCardLog($postValue)
    {
        // print_r($postValue);
        if ($postValue['state'] == 'active') {
            $cardLogCheck = '0';
        } else if($postValue['state'] == 'disabled') {
            $cardLogCheck = '1';
        }

        foreach ($postValue['chk'] as $key => $value) {
            $query = " UPDATE es_mobileCardorder SET state = '".$cardLogCheck."' WHERE no = '".$value."' ";
            $this->db->query($query);
        }
    }

}