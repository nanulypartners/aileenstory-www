<?php

namespace Component\Mobilecard;

use Component\Database\DBTableField;
use Request;
use Session;
use Framework\Debug\Exception\AlertBackException;

class MobilecardUser
{
    public function __construct()
    {
        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }
    }

    function vimeoUpdate($postValue, $type){

        if ($postValue->orderNo!="") {

            if ($type==1) {
                $arrData = [
                    "odno" => $postValue->orderNo,
                    "odname" => $postValue->odName,
                    "odMemNo" => $postValue->odMemNo,
                    "category" => $postValue->category,
                    "skin" => $postValue->skin,
                    "vimeoUri" => $postValue->vimeoUri
                ];
            } else if ($type==2) {
                $arrData = [
                    "odno" => $postValue->orderNo,
                    "vimeoUri" => ''
                ];
            }

            $arrInclude = ['vimeoUri'];

            $info = $this->getCardOrder($postValue->orderNo);
            if($info){
                $arrBind = $this->db->get_binding(DBTableField::tableMobileCardorder(), $arrData, 'update', $arrInclude);
                $this->db->bind_param_push($arrBind['bind'], 's', $arrData['odno']);
                $this->db->set_update_db('es_mobileCardorder', $arrBind['param'], 'odno = ?', $arrBind['bind']);
                return "ok";
            }else{
                $arrBind = $this->db->get_binding(DBTableField::tableMobileCardorder(), $arrData, 'insert');
                $this->db->set_insert_db('es_mobileCardorder', $arrBind['param'], $arrBind['bind'], 'y');
                return "ok";
            }

        }

    }

    function getCardOrderDelList(){
        $strSQL = " SELECT * FROM es_mobileCardorder WHERE ( url!='' or vimeoUri!='' ) AND regDt < now() - INTERVAL 150 DAY ";
        $result = $this->db->query_fetch($strSQL);

        return $result;
    }

    function delCardOrder($postValue){

        $arrData = [
            "no" => $postValue,
            "url" => "",
            "vimeoUri" => "",
            "state" => 1
        ];

        $arrInclude = ['state', 'url', 'vimeoUri'];

        $arrBind = $this->db->get_binding(DBTableField::tableMobileCardorder(), $arrData, 'update', $arrInclude);
        $this->db->bind_param_push($arrBind['bind'], 's', $arrData['no']);
        $this->db->set_update_db('es_mobileCardorder', $arrBind['param'], 'no = ?', $arrBind['bind']);

    }

    function getCardOrder($postValue){
        $strSQL = " SELECT * FROM es_mobileCardorder WHERE odno = '{$postValue}'";
        $result = $this->db->query_fetch($strSQL);

        return $result;
    }

    function getCardOrderNo($postValue){
        $strSQL = " SELECT * FROM es_mobileCardorder WHERE no = '{$postValue}'";
        $result = $this->db->query_fetch($strSQL);

        return $result;
    }

    function setCardOrder($postValue){

        $cardInfo = $this->getCardOrder($postValue['orderNo'])[0];
        $memInfo = Session::get('member');

        if($cardInfo){
            if( substr($cardInfo['regDt'], 0, 10) < date("Y-m-d", strtotime("-5 month", time()))  ){
                return "expiration";
            }            
        }

        $arrData = [
            "odno" => $postValue['orderNo'],
            "odsno" => '',
            "odname" => $memInfo['memNm'],
            "odtel" => $memInfo['phone'],
            "odMemNo" => $memInfo['memNo'],
            "eventday" => substr($postValue['card']['date']['datepicker'], 0, 10),
            "apm" => $postValue['card']['date']['apm'],
            "hour" => $postValue['card']['date']['hour'],
            "minute" => $postValue['card']['date']['minute'],
            "url" => $postValue['card']['card_title'],
            "category" => $postValue['category'],
            "skin" => $postValue['skin']
        ];

        $arrInclude = ['url', 'category', 'skin', 'odname', 'odtel', 'odMemNo', 'eventday', 'apm', 'hour', 'minute'];

        $info = $this->getCardOrder($postValue['orderNo']);
        if($info){
            $arrBind = $this->db->get_binding(DBTableField::tableMobileCardorder(), $arrData, 'update', $arrInclude);
            $this->db->bind_param_push($arrBind['bind'], 's', $arrData['odno']);
            $this->db->set_update_db('es_mobileCardorder', $arrBind['param'], 'odno = ?', $arrBind['bind']);
            if($info[0]['url'] != $postValue['card']['card_title']){
                return $info[0]['url'];
            }
            return "url_ok";
        }else{
            $arrBind = $this->db->get_binding(DBTableField::tableMobileCardorder(), $arrData, 'insert');
            $this->db->set_insert_db('es_mobileCardorder', $arrBind['param'], $arrBind['bind'], 'y');
            return "url_ok";
        }

    }

    public function getOrderInfo($getValue = null)
    {
        if(gd_isset($getValue)) {

            $strSQL = " SELECT * FROM es_orderInfo WHERE orderNo = '{$getValue}' ";
            $result = $this->db->fetch($strSQL);
            return $result;
        }
    }

    function checkCardName($getValue){

        if (empty($getValue['web_name']) || $getValue['web_name']=="") {
            return "NOT web_name";
        }

        if (empty($getValue['orderNo']) || $getValue['orderNo']=="") {
            return "NOT orderNo";
        }

        $strSQL = " SELECT * FROM es_mobileCardorder WHERE url = '{$getValue['web_name']}' AND ( odno != '{$getValue['orderNo']}' ) ";
        $result = $this->db->query_fetch($strSQL);

        if($result){
            return "Y";
        }else{
            return "N";
        }

    }


    public function getSkin($getValue = null){
        if(gd_isset($getValue)) {
            $strSQL = " SELECT * FROM es_mobileCardskin WHERE code = '{$getValue['skin']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data['category'] = $v['category'];
                $data['name'] = $v['name'];
                $data['code'] = $v['code'];
                $data['thumb'] = $v['thumb'];
                $data['preview'] = $v['preview'];
                $data['basic_info'] = $v['basic_info'];
                $data['relation_info'] = $v['relation_info'];
                $data['main_img_background_use'] = $v['main_img_background_use'];
                $data['main_img_info'] = $v['main_img_info'];
                $data['main_background_img'] = $v['main_background_img'];
            }

            $data['basic_info'] = json_decode($data['basic_info'], true);
            $data['relation_info'] = json_decode($data['relation_info'], true);
            $data['main_img_info'] = json_decode($data['main_img_info'], true);

            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }


    public function getText($getValue = null){
        if(gd_isset($getValue)) {
            if ($getValue['category']=='wedding') {
                $getValue['category'] = 1;
            } else if ($getValue['category']=='baby') {
                $getValue['category'] = 2;
            } else if ($getValue['category']=='old') {
                $getValue['category'] = 3;
            }
            $strSQL = " SELECT * FROM es_mobileCardtext WHERE category = '{$getValue['category']}'";
            $result = $this->db->query_fetch($strSQL);

            foreach ($result as $k => $v) {
                $data[$k]['category'] = $v['category'];
                $data[$k]['name'] = $v['name'];
                $data[$k]['body'] = $v['body'];
            }
            $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            return $getData;
        }
    }

    public function getcateCd($goodsNo){

        $strSQL = " SELECT cateCd FROM es_goods WHERE goodsNo = '{$goodsNo}'";
        $rst = $this->db->fetch($strSQL);

        if($rst){
            return $rst;
        }else{
            $strSQL = " SELECT cateCd FROM es_goodsLinkCategory WHERE goodsNo = '{$goodsNo}' AND CHAR_LENGTH(cateCd) = '6' ";
            $rst = $this->db->fetch($strSQL);

            return $rst;            
        }
    }

    public function getCardListUser($getValue = null){

        $memInfo = Session::get('member');

        // --- 정렬 설정
        $sort['fieldName'] = $getValue['sort.name'];
        if (empty($sort['fieldName'])) {
            $sort['fieldName'] = 'regDt';
            $sort['sortMode'] = 'desc';
        }

        $arrBind = [];

        // --- 페이지 기본설정
        gd_isset($getValue['page'], 1);
        gd_isset($getValue['pageNum'], 10);

        $page = \App::load('\\Component\\Page\\Page', $getValue['page']);
        $page->page['list'] = $getValue['pageNum']; // 페이지당 리스트 수

        $arrWhere[] = " odMemNo = ? ";
        $this->db->bind_param_push($arrBind, 's', $memInfo['memNo']);

        $arrWhere[] = " state = ? ";
        $this->db->bind_param_push($arrBind, 's', 0);

        if ($arrWhere) {
            $addWhere = implode(' AND ', gd_isset($arrWhere));
        } else {
            $addWhere = 1;
        }

        $countSQL = " SELECT COUNT(*) AS cnt FROM es_mobileCardorder WHERE $addWhere";
        $searchCnt = $this->db->query_fetch($countSQL, $arrBind, false)['cnt'];
        $page->recode['amount'] = $searchCnt; // 전체 레코드 수
        $page->recode['total'] = $searchCnt; // 검색 레코드 수
        $page->setPage();
        $page->setUrl(\Request::getQueryString());

        // 현 페이지 결과
        $this->db->strField = " t1.*, t2.thumb ";
        $this->db->strJoin = ' LEFT JOIN es_mobileCardskin AS t2 ON t1.skin = t2.code';
        $this->db->strWhere = $addWhere;
        $this->db->strOrder = $sort['fieldName'] . ' ' . $sort['sortMode'];
        $this->db->strLimit = $page->recode['start'] . ',' . $getValue['pageNum'];
        $query = $this->db->query_complete();
        $strSQL = 'SELECT ' . array_shift($query) .' FROM es_mobileCardorder AS t1 ' . implode(' ', $query);

        $data = $this->db->query_fetch($strSQL, $arrBind);

        // 각 데이터 배열화
        $getData['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));

        return $getData;
    }


}