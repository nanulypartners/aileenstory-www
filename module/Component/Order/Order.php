<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright Copyright (c) 2017 GodoSoft.
 * @link http://www.godo.co.kr
 */
namespace Component\Order;

use Component\Database\DBTableField;
use Session;

use Component\Autorender\AutorenderUser;

class Order extends \Bundle\Component\Order\Order
{

    public $orderLogCount = 0;

    public function orderLog($orderNo, $goodsSno, $logCode01 = null, $logCode02 = null, $logDesc = null, $userOrder = false)
    {

        parent::orderLog($orderNo, $goodsSno, $logCode01, $logCode02, $logDesc, $userOrder);

        $member = \App::load('\\Component\\Member\\Member');
        $memInfo = Session::get('member');

        $cmSQL = " SELECT * FROM es_autorenderCreateNmSave WHERE memId = '{$memInfo['memId']}' ORDER BY regDt Desc limit 1";
        $cmResult = $this->db->fetch($cmSQL);
        $createNm = $cmResult['createNm'];

        if ( $logDesc=='초기주문' ) {

            if (!empty($createNm)) {
                //세션에 제작번호가 있을 경우
                $strSQL = " SELECT * FROM es_autorenderByOrderNo WHERE type = 'session' AND odNo = '{$orderNo}' AND odSno = '{$goodsSno}' AND createNm = '{$createNm}' limit 1";
                $result = $this->db->fetch($strSQL);

                $arrBind = [];

                if (empty($result['createNm'])) {
                    //제작번호가 없으면 입력

                    $arrData = [
                        "type" => 'session',
                        "odNo" => $orderNo,
                        "odSno" => $goodsSno,
                        "createNm" => $createNm,
                        "payment" => 0
                    ];

                    $arrBind = $this->db->get_binding(DBTableField::tableAutorenderByOrderNo(), $arrData, 'insert');
                    $this->db->set_insert_db('es_autorenderByOrderNo', $arrBind['param'], $arrBind['bind'], 'y');

                }

                Session::set('createNm', '');

            }
        }



        if ($logCode02=='결제완료(p1)') {

            //주문번호와 상품주문번호로 제작번호 불러오기
            $strSQL = " SELECT * FROM es_autorenderByOrderNo WHERE odNo = '{$orderNo}' AND odSno = '{$goodsSno}' AND payment = '0' limit 1";
            $result = $this->db->fetch($strSQL);

            if (!empty($result['odNo']) && !empty($result['odSno']) && !empty($result['createNm'])) {

                $query = "UPDATE es_autorenderByOrderNo SET payment='1' WHERE odNo = '{$orderNo}' AND odSno = '{$goodsSno}' AND payment = '0' limit 1";
                $this->db->query($query);

                $postValue = array();
                $postValue['createNm'] = $result['createNm'];

                //제작번호 불러오기
                $autorender = new AutorenderUser();
                $userData = $autorender->getUserDetail($postValue)['data'];

                if ($userData['state']=='3') {
                    $state = 1;
                } else {
                    $state = 0;
                }

                //재결제일 경우
                if ($userData['payment']>0) {
                    $modifyCount = 0;
                    $state = 0;
                    $orderNo = $userData['odNo'];
                    $goodsSno = $userData['odSno'];
                } else {
                    $modifyCount = 0;
                }

                //파일서버 전송
                $data = array();
                $data['renderType'] = 1;
                $data['payment'] = $userData['payment']+1;
                $data['createNm'] = $userData['createNm'];
                $data['odNo'] = $orderNo;
                $data['odSno'] = $goodsSno;
                $data['odId'] = $userData['odId'];
                $data['odName'] = $userData['odName'];
                $data['goodsType'] = $userData['goodsType'];
                $data['goodsNm'] = $userData['goodsNm'];
                $data['goodsCd'] = $userData['goodsCd'];
                $data['code'] = "";
                $data['modifyCount'] = $modifyCount;
                $data['moviePath'] = "";
                $data['state'] = $state;



                $data['nanuly'] = "sksnfl2020@";
                $data['nanuly_type'] = "UserCodeInsert";
                $url = "https://app.aileenstory.com/api/autorender_api.php";
                $json_data = json_encode((object)$data, JSON_UNESCAPED_UNICODE);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: '.strlen($json_data)));
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POST, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $output = json_decode($output, true);

                if ($output['state']!="success") {
                    $return = 'apiError';
                } else {

                    if ($state==1) {
                        //로그기록
                        $autorender->insertLog((object)$data, 1);
                    }

                    //업로드 제작번호 업데이트
                    $arrData = [
                        "odNo" => $orderNo,
                        "odSno" => $goodsSno,
                        "payment" => $userData['payment']+1,
                        "renderType" =>1,
                        "modifyCount" => $modifyCount,
                        "state" => $state,
                        "moviePath" => "",
                        "createNm" => $userData['createNm']
                    ];

                    $arrExclude = [
                        'no',
                        'goodsNm',
                        'goodsCd',
                        'createNm',
                        'odId',
                        'odName',
                        'goodsType',
                        'basicInfo',
                        'sceneInfoUser'
                    ];

                    $arrBind = $this->db->get_binding(DBTableField::tableAutorenderUser(), $arrData, 'update', '', $arrExclude);
                    $this->db->bind_param_push($arrBind['bind'], 's', $arrData['createNm']);
                    $this->db->set_update_db('es_autorenderUser', $arrBind['param'], 'createNm = ?', $arrBind['bind']);

                }




            }

        }



    }

}
