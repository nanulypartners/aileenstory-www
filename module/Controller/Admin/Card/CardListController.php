<?php
namespace Controller\Admin\Card;

use Component\Page\Page;

class CardListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('card', 'cardset', 'cardlist');

        // --- 모듈 호출
        $mobilecardtext = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');

        try {

            $getData = $mobilecardtext->getListOrder(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정
            $category = array('1'=>'청첩장', '2'=>'돌잔치', '3'=>'고희/환갑');
            $state = array('0'=>'활성', '1'=>'비활성');

            $this->setData('data', $getData['data']);
            $this->setData('page', $page);
            $this->setData('category', $category);
            $this->setData('state', $state);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
