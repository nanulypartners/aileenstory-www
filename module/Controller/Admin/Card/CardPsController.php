<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Admin\Card;

use Component\Mobilecard\MobilecardAdmin;
use Framework\Debug\Exception\LayerNotReloadException;
use Framework\Debug\Exception\LayerException;
use Message;
use Request;

class CardPsController extends \Controller\Admin\Controller
{
    public function index()
    {
        try {

            $mobilecardAdmin = new MobilecardAdmin();

            // Post Data
            $postData =  Request::post()->toArray();

            if ($postData['type'] == 'skin') {

                if ($postData['mode'] == 'update') {
                    $mobilecardAdmin->updateSkin($postData);
                    $this->layer(__("스킨이 수정되었습니다."), 'parent.location.replace("./card_skin_register.php?no='.$postData['no'].'&mode=update")');
                } elseif ($postData['mode'] == 'delete') {
                    $mobilecardAdmin->deleteSkin($postData);
                    $this->layer(__("스킨이 삭제되었습니다."), 'parent.location.replace("./card_skin_list.php")');
                } else {
                    $mobilecardAdmin->insertSkin($postData);
                    $this->layer(__("스킨이 등록되었습니다."), 'parent.location.replace("./card_skin_list.php")');
                }

            } elseif ($postData['type'] == 'text') {

                if ($postData['mode'] == 'update') {
                    $mobilecardAdmin->updateText($postData);
                    $this->layer(__("예문이 수정되었습니다."), 'parent.location.replace("./card_text_register.php?no='.$postData['no'].'&mode=update")');
                } elseif ($postData['mode'] == 'delete') {
                    $mobilecardAdmin->deleteText($postData);
                    $this->layer(__("예문이 삭제되었습니다."), 'parent.location.replace("./card_text_list.php")');
                } else {
                    $mobilecardAdmin->insertText($postData);
                    $this->layer(__("예문이 등록되었습니다."), 'parent.location.replace("./card_text_list.php")');
                }

            } elseif ($postData['type'] == 'log') {
                $mobilecardAdmin->updateLog($postData);
                $this->layer(__("초대장 로그가 수정되었습니다."), 'parent.location.replace("./card_register.php?no='.$postData['no'].'&mode=update")');
            }

        } catch (\Exception $e) {
            throw new LayerNotReloadException($e->getMessage()); //새로고침안됨
        }
    }
}
