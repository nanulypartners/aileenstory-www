<?php
namespace Controller\Admin\Card;

use Component\Page\Page;

class CardRegisterController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('card', 'cardset', 'cardlist');

        // --- 모듈 호출
        $mobilecardtext = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');

        try {

            $getLog = $mobilecardtext->getLog(\Request::get()->toArray());
            $this->setData('getLog', $getLog['data']);
            $this->setData('getData', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
