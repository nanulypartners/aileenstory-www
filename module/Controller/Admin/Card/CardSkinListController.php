<?php
namespace Controller\Admin\Card;

use Component\Page\Page;

class CardSkinListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('card', 'cardset', 'cardskinlist');

        // --- 모듈 호출
        $mobilecardtext = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');

        try {

            $getData = $mobilecardtext->getListSkin(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정
            $category = array('1'=>'청첩장', '2'=>'돌잔치', '3'=>'고희/환갑');

            $this->setData('data', $getData['data']);
            $this->setData('page', $page);
            $this->setData('category', $category);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
