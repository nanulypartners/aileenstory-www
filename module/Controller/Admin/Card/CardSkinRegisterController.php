<?php
namespace Controller\Admin\Card;

use Component\Page\Page;

class CardSkinRegisterController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('card', 'cardset', 'cardskinlist');

        // --- 모듈 호출
        $card = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');

        try {

            $getSkin = $card->getSkin(\Request::get()->toArray());
            $this->setData('getSkin', $getSkin['data']);
            $this->setData('getData', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
