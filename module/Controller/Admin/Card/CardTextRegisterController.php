<?php
namespace Controller\Admin\Card;

use Component\Page\Page;

class CardTextRegisterController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('card', 'cardset', 'cardtextlist');

        // 모듈호출
        $card = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');

        try {

            $getText = $card->getText(\Request::get()->toArray());
            $this->setData('getText', $getText['data']);
            $this->setData('getData', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
