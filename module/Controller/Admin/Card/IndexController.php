<?php

namespace Controller\Admin\Card;

class IndexController extends \Controller\Admin\Controller
{
    public function index()
    {
        $this->redirect('./card_skin_list.php');
    }
}
