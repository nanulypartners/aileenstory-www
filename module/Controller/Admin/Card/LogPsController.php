<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Admin\Card;

use Component\Mobilecard\MobilecardLog;
use Framework\Debug\Exception\LayerNotReloadException;
use Framework\Debug\Exception\LayerException;
use Message;
use Request;

class LogPsController extends \Controller\Admin\Controller
{
    public function index()
    {

        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $mobileCardLog = new MobilecardLog();

            // Post Data
            $postData =  Request::post()->toArray();

            if($postData['state']=="del"){
                foreach ($postData['chk'] as $key => $value) {
                    $card = $cardUser->getCardOrderNo($value)[0];

                    //초대장 삭제 api로 전송
                    $data['type'] = "del_card";
                    $data['card']['card_title'] = $card['url'];
                    $url = "https://app.aileenstory.com/api/mobilecard.php";
                    $json_data = json_encode($data);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: '.strlen($json_data)));
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    $output = curl_exec($ch);
                    curl_close($ch);

                    if($output=="Y"){
                        $cardUser->delCardOrder($value);
                    }else{
                        $this->layer(__("삭제에 실패하였습니다. 다시 시도해 주세요."), 'parent.location.reload()');
                    }
                }

                $this->layer(__("삭제 되었습니다."), 'parent.location.reload()');

            }else{
                $mobileCardLog->updateCardLog($postData);
                $this->layer(__("처리 되었습니다."), 'parent.location.reload()');
            }

        } catch (\Exception $e) {
            throw new LayerNotReloadException($e->getMessage()); //새로고침안됨
        }
    }
}
