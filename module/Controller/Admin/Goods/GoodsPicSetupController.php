<?php

namespace Controller\Admin\Goods;

use Exception;
use Framework\Utility\ImageUtils;
use Component\Member\Group\Util;
use Globals;
use Request;
use Session;

class GoodsPicSetupController extends \Controller\Admin\Controller
{
    public function index()
    {
        // *메뉴 설정
        $this->callMenu('goods', 'goods', 'goodsPicSetup');
        // *db 설정
        $db = \App::load('DB');

        $goodsNo = Request::get()->get('goodsNo');
        $page = Request::get()->get('page');
        $mode = Request::get()->get('mode');
        $this->setData('goodsNo', $goodsNo);
        $this->setData('page', $page);
        $this->setData('mode', $mode);
        $this->setData('url', 'http://leveltest1.cafe24.com/files');

        try
        {
            // *default
            $query = "SELECT * FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'default' ORDER BY `order` ASC";
            $default_data = $db->query_fetch($query, null);
            if(!empty($default_data))
            {
                $this->setData('defaultCount', count($default_data));
                $this->setData('default_data', $default_data);
            }

            // *extra
            $query = "SELECT * FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'extra' ORDER BY `order` ASC";
            $extra_data = $db->query_fetch($query, null);
            if(empty($extra_data))
            {
                $this->setData('extraCount', 0);
            }
            else
            {
                $this->setData('extraCount', count($extra_data));
                $this->setData('extra_data', $extra_data);
            }
        } 
        catch(\Exception $e) 
        {
            throw $e;
        }
    }
}