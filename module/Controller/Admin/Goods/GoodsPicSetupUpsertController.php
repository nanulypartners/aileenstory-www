<?php

namespace Controller\Admin\Goods;

use Component\Storage\Storage;
use Component\Database\DBTableField;
use Framework\Utility\ImageUtils;
use Framework\Debug\Exception\LayerException;
use Framework\Debug\Exception\LayerNotReloadException;
use Exception;
use Message;
use Request;
use Session;

class GoodsPicSetupUpsertController extends \Controller\Admin\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');
        // *Storage 설정
        $storage = Storage::disk(Storage::PATH_CODE_GOODS, 'http://leveltest1.cafe24.com');

        $mode = Request::post()->get('mode');
        $goodsNo = Request::post()->get('goodsNo');
        $defaultCount = Request::post()->get('defaultCount');
        $extraCount = Request::post()->get('extraCount');
        $defaultType = Request::post()->toArray()["defaultType"];
        $defaultInput = Request::post()->toArray()["defaultInput"];
        $extraTitle = Request::post()->toArray()["extraTitle"];
        $extraType = Request::post()->toArray()["extraType"];
        $extraImage = Request::files()->toArray()["extraImage"];
        $extraImagePath = Request::post()->toArray()["extraImagePath"];
        $extraImageDelete = Request::post()->toArray()["extraImageDelete"];
        $extraImageText = Request::post()->toArray()["extraImageText"];
        $extraText = Request::post()->toArray()["extraText"];
        $extraUploadCount = Request::post()->toArray()["extraUploadCount"];
        $extraTextCount = Request::post()->toArray()["extraTextCount"];
        $deleteImagePath = Request::post()->toArray()["deleteImagePath"];

        try
        {
            if($mode == 'update')
            {
                // *default
                $query = "DELETE FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'default'";
                $result = $db->query($query);

                // *extra
                $query = "DELETE FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'extra'";
                $result = $db->query($query);

                // *이미지 삭제
                foreach($deleteImagePath as $key => $value)
                {
                    $storage->delete(str_replace('/data/goods/', '', $value));
                }
            }

            foreach($defaultType as $key => $value)
            {
                $tmp_data = array();
                $tmp_data['type'] = $defaultType[$key];
                $tmp_data['input'] = $defaultInput[$key];
                $query = "INSERT INTO `es_goodsPicSetup` SET ";
                $query .= "`goodsNo` = " . $goodsNo;
                $query .= ", `type` = 'default', `order` = " . $key . ", `value` = '" . serialize($tmp_data) . "'";
                $result = $db->query($query);
            }
            
            foreach($extraType as $key => $value)
            {
                $image_path = '';

                if($extraImageDelete[$key] !== 'y' && !empty($extraImagePath[$key]))
                {
                    $image_path = $extraImagePath[$key];
                }
                else
                {
                    if($extraImageDelete[$key] === 'y')
                    {
                        $storage->delete(str_replace('/data/goods/', '', $extraImagePath[$key]));
                    }

                    if($extraImage['size'][$key] > 0)
                    {
                        // *파일 삭제
                        $storage->delete(str_replace('/data/goods/', '', $extraImagePath[$key]));

                        // *파일 저장
                        $image_ext = strrchr($extraImage['name'][$key], '.');
                        $image_path = 'pic_setup/' . $goodsNo . '/' . md5($extraImage['name'][$key]) . $image_ext;
                        $storage->upload($extraImage['tmp_name'][$key], $image_path);
                        $image_path = '/data/goods/' . $image_path;
                    }   
                }

                $tmp_data = array();
                $tmp_data['title'] = $extraTitle[$key];
                $tmp_data['type'] = $extraType[$key];
                $tmp_data['image'] =  $image_path;
                $tmp_data['image_text'] = $extraImageText[$key];
                $tmp_data['text'] = $extraText[$key];
                $tmp_data['text_count'] = $extraTextCount[$key];
                $tmp_data['upload_count'] = $extraUploadCount[$key];
                $query = "INSERT INTO `es_goodsPicSetup` SET ";
                $query .= "`goodsNo` = " . $goodsNo;
                $query .= ", `type` = 'extra', `order` = " . $key . ", `value` = '" . serialize($tmp_data) . "'";
                $result = $db->query($query);
            }
            
            $this->layer(__('저장이 완료되었습니다.'));
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }
}