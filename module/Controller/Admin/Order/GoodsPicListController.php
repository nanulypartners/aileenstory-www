<?php

namespace Controller\Admin\Order;

use Component\Order\OrderAdmin;
use Exception;
use Globals;
use Request;

class GoodsPicListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // *메뉴 설정
        $this->callMenu('order', 'order', 'goodsPicList');
        // *db 설정
        $db = \App::load('DB');
        $order = \App::load('\\Component\\Order\\OrderAdmin');
        $goods = \App::load('\\Component\\Goods\\Goods');
        $this->setData('order', $order);
        $this->setData('goods', $goods);

        $request = Request::get()->toArray();

        try
        {
            // 페이지 기본설정
            gd_isset($request['page'], 1);
            gd_isset($request['pageNum'], 10);

            $page = \App::load('\\Component\\Page\\Page', $request['page']);
            $page->page['list'] = $request['pageNum']; // 페이지당 리스트 수
            $total = $db->fetch('SELECT count(*) as total FROM es_goodsPic');
            $page->recode['amount'] = $total['total']; // 전체 레코드 수
            $page->setPage();
            $page->setUrl(\Request::getQueryString());

            // 기본 쿼리
            $strTable = 'es_goodsPic AS GoodsPic';
            $arrJoin[0] = 'LEFT JOIN es_goods AS GoodsData ON GoodsPic.goodsNo = GoodsData.goodsNo';

            // 검색
            $search['uploadStatus'] = $request['uploadStatus']; 
            if(!empty($search['uploadStatus'])) $arrWhere[0] = 'GoodsPic.status = ' . $request['uploadStatus'];
            $search['target'] = $request['target'];
            $search['keyword'] = $request['keyword'];
            if(!empty($search['target']) && !empty($search['keyword']))
            {
                if($search['target'] == 'orderName')
                {
                    $arrWhere[1] = 'GoodsPic.orderName LIKE "%' . $search['keyword'] . '%")';
                }
                elseif($search['target'] == 'orderNo')
                {
                    $arrWhere[1] = 'GoodsPic.orderNo = ' . $search['keyword'];
                }
                elseif($search['target'] == 'goodsNm')
                {
                    $arrWhere[1] = 'GoodsData.goodsNm LIKE "%' . $search['keyword'] . '%"';
                }
            }
            $this->setData('search', $search);

            // 현 페이지 결과
            $db->strField = 'GoodsPic.*, GoodsData.*';
            $db->strJoin = implode(' ', gd_isset($arrJoin));
            $db->strWhere = implode(' AND ', gd_isset($arrWhere));
            $db->strOrder = 'GoodsPic.status DESC, GoodsPic.update DESC';
            $db->strLimit = $page->recode['start'] . ',' . $request['pageNum'];

            $query = $db->query_complete();
            $str_sql = 'SELECT ' . array_shift($query) . ' FROM ' . $strTable . implode(' ', $query);
            $data = $db->query_fetch($str_sql, null);

            // 레코드 수
            unset($query['group'], $query['order'], $query['limit']);
            $str_cnt_sql = 'SELECT COUNT(*) AS total, GoodsPic.*, GoodsData.goodsNo FROM ' . $strTable . implode(' ', $query);;
            $page->recode['total'] = $db->query_fetch($str_cnt_sql, null, false)['total'];
            $this->setData('query', $str_cnt_sql);
            $page->setPage();

            // 각 데이터 배열화
            $list['data'] = gd_htmlspecialchars_stripslashes(gd_isset($data));
            $this->setData('list', $list);
            $this->setData('page', $page);
            //$list['sort'] = $search['sort'];
            //$list['search'] = gd_htmlspecialchars($search);
            //$list['checked'] = $checked;
        } 
        catch(\Exception $e) 
        {
            throw $e;
        }
    }
}