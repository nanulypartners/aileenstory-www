<?php

namespace Controller\Admin\Order;

use Component\Database\DBTableField;
use Framework\Debug\Exception\LayerException;
use Framework\Debug\Exception\LayerNotReloadException;
use Exception;
use Message;
use Request;
use Session;
use Component\Order\OrderAdmin;

class GoodsPicMakeUpsertController extends \Controller\Admin\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');
        $order = new OrderAdmin();

        $status = Request::post()->get('status');
        $goodsPicNo = Request::post()->get('goodsPicNo');
        $picType = Request::post()->get('picType');
        $playVideo = Request::post()->get('playVideo');
        $downVideo = Request::post()->get('downVideo');
        $now = date('YmdHis', time());

        try
        {
            // *제작 저장
            $query = "SELECT * FROM `es_goodsPicMake` WHERE `goodsPicNo` = " . $goodsPicNo;
            $make_data = $db->query_fetch($query, null);
            $make = $make_data[0];
            if(empty($make))
            {
                $query = "INSERT INTO `es_goodsPicMake` SET ";
                $query .= "`goodsPicNo` = " . $goodsPicNo . ", `playVideo` = '" . $playVideo . "', `downVideo` = '" . $downVideo . "'";
                $output = $db->query($query);
            }
            else
            {
                $query = "UPDATE `es_goodsPicMake` SET ";
                $query .= "`playVideo` = '" . $playVideo . "', `downVideo` = '" . $downVideo . "' ";
                $query .= "WHERE `goodsPicMakeNo` = " . $make['goodsPicMakeNo'];
                $output = $db->query($query);
            }
            
            // *상태 및 수정일 저장
            $query = "UPDATE `es_goodsPic` SET ";
            $query .= "`status` = '" . $status . "', `update` = '" . $now . "' ";
            $query .= "WHERE `goodsPicNo` = " . $goodsPicNo;
            $output = $db->query($query);

            if($picType == 'aileen')
            {
                 // *goodsPic 정보
                 $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
                 $pic_data = $db->query_fetch($query, null);
                 $pic = $pic_data[0];
                 if($status == 5 || $status == 9)
                 {
                    if($status == 5) 
                    {
                        $godo_status = 'g7';
                    }
                    elseif($status == 9)
                    {
                        $godo_status = 'g8';
                    }
                    $order->updateStatusPreprocess($pic['orderNo'],  $order->getOrderGoodsByGoodsNo($pic['orderNo'], $pic['goodsNo']), 'g', $godo_status, __('관리자의 '), true);
                 }
            }
            
            $this->layer(__('저장이 완료되었습니다.'));
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }
}