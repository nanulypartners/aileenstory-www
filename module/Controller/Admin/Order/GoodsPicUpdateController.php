<?php

namespace Controller\Admin\Order;

use Component\Database\DBTableField;
use Framework\Debug\Exception\LayerException;
use Framework\Debug\Exception\LayerNotReloadException;
use Exception;
use Message;
use Request;
use Session;

class GoodsPicUpdate extends \Controller\Admin\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');

        $status = Request::post()->get('status');
        $playVideo = Request::post()->get('playVideo');
        $downVideo = Request::post()->get('downVideo');

        try
        {
            if($mode == 'update')
            {
                // *default
                $query = "DELETE FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'default'";
                $result = $db->query($query);

                // *extra
                $query = "DELETE FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " AND `type` = 'extra'";
                $result = $db->query($query);

                // *이미지 삭제
                foreach($deleteImagePath as $key => $value)
                {
                    $storage->delete(str_replace('/data/goods/', '', $value));
                }
            }

            foreach($defaultType as $key => $value)
            {
                $tmp_data = array();
                $tmp_data['type'] = $defaultType[$key];
                $tmp_data['input'] = $defaultInput[$key];
                $query = "INSERT INTO `es_goodsPicSetup` SET ";
                $query .= "`goodsNo` = " . $goodsNo;
                $query .= ", `type` = 'default', `order` = " . $key . ", `value` = '" . serialize($tmp_data) . "'";
                $result = $db->query($query);
            }
            
            foreach($extraType as $key => $value)
            {
                $image_path = '';

                if($extraImageDelete[$key] !== 'y' && !empty($extraImagePath[$key]))
                {
                    $image_path = $extraImagePath[$key];
                }
                else
                {
                    if($extraImageDelete[$key] === 'y')
                    {
                        $storage->delete(str_replace('/data/goods/', '', $extraImagePath[$key]));
                    }

                    if($extraImage['size'][$key] > 0)
                    {
                        // *파일 삭제
                        $storage->delete(str_replace('/data/goods/', '', $extraImagePath[$key]));

                        // *파일 저장
                        $image_ext = strrchr($extraImage['name'][$key], '.');
                        $image_path = 'pic_setup/' . $goodsNo . '/' . md5($extraImage['name'][$key]) . $image_ext;
                        $storage->upload($extraImage['tmp_name'][$key], $image_path);
                        $image_path = '/data/goods/' . $image_path;
                    }   
                }

                $tmp_data = array();
                $tmp_data['title'] = $extraTitle[$key];
                $tmp_data['type'] = $extraType[$key];
                $tmp_data['image'] =  $image_path;
                $tmp_data['image_text'] = $extraImageText[$key];
                $tmp_data['text'] = $extraText[$key];
                $tmp_data['text_count'] = $extraTextCount[$key];
                $tmp_data['upload_count'] = $extraUploadCount[$key];
                $query = "INSERT INTO `es_goodsPicSetup` SET ";
                $query .= "`goodsNo` = " . $goodsNo;
                $query .= ", `type` = 'extra', `order` = " . $key . ", `value` = '" . serialize($tmp_data) . "'";
                $result = $db->query($query);
            }
            
            $this->layer(__('저장이 완료되었습니다.'));
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }
}