<?php

namespace Controller\Admin\Order;

use Component\Order\OrderAdmin;
use Exception;
use Globals;
use Request;

class GoodsPicViewController extends \Controller\Admin\Controller
{
    public function index()
    {
        // *메뉴 설정
        $this->callMenu('order', 'order', 'goodsPicView');
        // *모듈 설정
        $order = \App::load('\\Component\\Order\\Order');
        $goods = \App::load('\\Component\\Goods\\Goods');
        // *db 설정
        $db = \App::load('DB');

        $goodsPicNo = Request::get()->get('goodsPicNo');
        $status = Request::get()->get('status');
        $defaultData = array();
        $extraData = array();

        try
        {
            // *goodsPic에 등록 확인
            $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
            $pic_data = $db->query_fetch($query, null);
            $pic_info = $pic_data[0];
            $this->setData('picInfo', $pic_info);

            // *제작 내용
            $query = "SELECT * FROM `es_goodsPicMake` WHERE `goodsPicNo` = " . $goodsPicNo;
            $make_data = $db->query_fetch($query, null);
            $make_info = $make_data[0];
            $this->setData('makeInfo', $make_info);

            // *등록 된 내용
            $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` < 1000";;
            $cont_data = $db->query_fetch($query, null);
            
            // *업로드 된 파일
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` < 1000";
            $file_data = $db->query_fetch($query, null);

            // *설정 된 내용
            $query = "SELECT * FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $pic_info['goodsNo'] . " ORDER BY `type` ASC, `order` ASC";
            $upload_data = $db->query_fetch($query, null);

            if($status == '6' || $status == '7' || $status == '8' || $status == '9' || $status == '10')
            {
                // *등록 된 수정 내용
                $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` >= 1000";;
                $modify_cont_data = $db->query_fetch($query, null);
                $this->setData('modifyCont', $modify_cont_data);
                
                // *업로드 된 수정 파일
                $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` >= 1000";
                $modify_file_data = $db->query_fetch($query, null);
                $this->setData('modifyFile', $modify_file_data);
            }
            
            foreach($upload_data as $no => $data)
            {
                // *default 항목
                if($data['type'] == 'default')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['type']) && !empty($tmp_value['input']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont =  $value['cont'];
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['input'] = $tmp_value['input'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        array_push($defaultData, $tmp_data);
                    }
                }

                // *extra 항목
                if($data['type'] == 'extra')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['title']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont = $value['cont'];
                            }
                        }
                        // *업로드 된 파일 적용
                        $tmp_files = array();
                        foreach($file_data as $key => $value)
                        {
                            if($value['order'] == $data['order'])
                            {
                                // *파일 타입
                                $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                                if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                                {
                                    $file_type = 'image';
                                }
                                elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                                {
                                    $file_type = 'video';
                                }
                                else
                                {
                                    $file_type = 'etc';
                                }
                                $tmp_file = array();
                                $tmp_file['goodsPicFileNo'] = $value['goodsPicFileNo'];
                                $tmp_file['path'] = str_replace('../', '/', $value['path']);
                                $tmp_file['name'] = $value['name'];
                                $tmp_file['type'] = $file_type;
                                array_push($tmp_files, $tmp_file);
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['no'] = str_pad($extra_no, 2, '0', STR_PAD_LEFT);
                        $tmp_data['title'] = $tmp_value['title'];
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['image'] = $tmp_value['image'];
                        $tmp_data['image_text'] = $tmp_value['image_text'];
                        $tmp_data['text'] = $tmp_value['text'];
                        $tmp_data['text_count'] = $tmp_value['text_count'];
                        $tmp_data['upload_count'] = $tmp_value['upload_count'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        $tmp_data['files'] = $tmp_files;
                        $tmp_data['upload_file_count'] = count($tmp_files);
                        array_push($extraData, $tmp_data);
                        $extra_no++;
                    }
                }
            }

            // *기본 데이터에 등록 된 파일
            foreach($file_data as $key => $value)
            {
                if($value['order'] == 0)
                {
                    // *파일 타입
                    $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                    if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                    {
                        $file_type = 'image';
                    }
                    elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                    {
                        $file_type = 'video';
                    }
                    else
                    {
                        $file_type = 'etc';
                    }
                    $defaultFile = array();
                    $defaultFile['goodsPicFileNo'] = $value['goodsPicFileNo'];
                    $defaultFile['path'] = str_replace('../', '/', $value['path']);
                    $defaultFile['name'] = $value['name'];
                    $defaultFile['type'] = $file_type;
                }
            }

            $this->setData('defaultData', $defaultData);
            $this->setData('extraData', $extraData);
            $this->setData('uploadCount', count($extraData));
            $this->setData('defaultFile', $defaultFile);
        } 
        catch(\Exception $e) 
        {
            throw $e;
        }
    }
}