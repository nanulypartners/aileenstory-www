<?php

namespace Controller\Admin\Upload;

class IndexController extends \Controller\Admin\Controller
{
    public function index()
    {
        $this->redirect('./upload_list.php');
    }
}