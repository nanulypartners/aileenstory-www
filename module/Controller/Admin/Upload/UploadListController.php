<?php
namespace Controller\Admin\Upload;

use Component\Page\Page;

class UploadListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('upload', 'uploadset', 'uploadlist');

        // --- 모듈 호출
        $videoGoods = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        try {

            $getData = $videoGoods->getListVideoAdmin(\Request::get()->toArray(), 1);
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $this->setData('data', $getData['data']);
            $this->setData('page', $page);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
