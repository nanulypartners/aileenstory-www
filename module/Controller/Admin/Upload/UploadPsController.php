<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Admin\Upload;

use Component\Autorender\AutorenderAdmin;
use Framework\Debug\Exception\LayerNotReloadException;
use Framework\Debug\Exception\LayerException;
use Message;
use Request;

class UploadPsController extends \Controller\Admin\Controller
{
    public function index()
    {
        try {

            $autorenderAdmin = new AutorenderAdmin();

            // Post Data
            $postData =  Request::post()->toArray();

            if($postData['videoMode'] == 'modify'){
                $autorenderAdmin->updateVideoAdmin($postData, 1);
                $this->layer(__('수정되었습니다.'), 'parent.location.replace("../upload/upload_register.php?goodsNo=' .$postData['getGoodsNo']. '&goodsNm='.urlencode(htmlspecialchars($postData['videoGoodsNm'])).'&videoMode='.$postData['videoMode'].'&goodsType='.$postData['goodsType'].' ")');
            } elseif($postData['videoMode'] == 'delete') {
                $autorenderAdmin->deleteListVideoAdmin($postData);
                $this->layer(__('삭제 되었습니다.'), 'parent.location.replace("./upload_list.php")');
            } else {
                $autorenderAdmin->insertVideoAdmin($postData, 1);
                $this->layer(__("등록되었습니다."), 'parent.location.replace("../upload/upload_list.php")');
            }

        } catch (\Exception $e) {
            throw new LayerNotReloadException($e->getMessage()); //새로고침안됨
        }
    }
}

