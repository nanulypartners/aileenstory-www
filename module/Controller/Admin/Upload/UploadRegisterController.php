<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link      http://www.godo.co.kr
 */
namespace Controller\Admin\Upload;

use Request;


class UploadRegisterController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('upload', 'uploadset', 'uploadlist');

        // 모듈호출
        $video = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        // 카테고리
        $cate = \App::load('\\Component\\Category\\CategoryAdmin');

        try {

            $mode = Request::get()->get('mode');

            if ($mode == "getCate") {

                $data = Request::get()->get('data');

                if(gd_isset($data)) {
                    $cateDepth2 = $cate->getCategoryTreeData($data);
                } else {
                    $cateDepth2 = "";
                }

                $result['cate'] = "<option value=''>카테고리를 선택해 주세요.</option>";

                foreach ($cateDepth2 as $key => $value) {
                    $result['cate'] .= "<option value='".$value['cateCd']."'>".$value['cateNm']."</option>";
                }

                $cateTitle = $video->getGoodsCateNm($data, 1);

                $result['goods'] = "<option value=''>상품을 선택해 주세요.</option>";

                foreach ($cateTitle as $key => $value) {
                    $result['goods'] .= "<option value='".$value['goodsNo']."'>".$value['goodsNm']."</option>";
                }

                echo json_encode($result);

            } elseif ($mode == "getCateSub") {

                $data = Request::get()->get('data');

                $cateTitle = $video->getGoodsCateNm($data, 1);

                $result['goods'] = "<option value=''>상품을 선택해 주세요.</option>";

                foreach ($cateTitle as $key => $value) {
                    $result['goods'] .= "<option value='".$value['goodsNo']."'>".$value['goodsNm']."</option>";
                }

                echo json_encode($result);


            } else {

                // 상품 1차 카테고리
                $cateDepth = $cate->getCategoryTreeData();
                $this->setData('cateDepth', $cateDepth);

                // 상품선택 selectbox 값
                $getVideo = $video->getGoodsNoNm(1);
                $this->setData('videoInfo', $getVideo);

                //추가상품
                $addGoods = $video->getAddGoods();
                $this->setData('addGoods', $addGoods);

                // get 값
                $goodsNo = Request::get()->get('goodsNo');
                $videoMode = Request::get()->get('videoMode');
                $goodsNm = Request::get()->get('goodsNm');
                $this->setData('getGoodsNo', $goodsNo);
                $this->setData('videoMode', $videoMode);
                $this->setData('goodsNm', htmlspecialchars($goodsNm));

                $getVideoAdmin = $video->getVideoAdmin(\Request::get()->toArray(), 1);
                $this->setData('getVideoAdmin', $getVideoAdmin['data']);

            }


        } catch (\Exception $e) {
            throw $e;
        }

    }
}
