<?php
namespace Controller\Admin\Upload;

use Component\Page\Page;

class UploadUserListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('upload', 'uploadset', 'uploaduser');

        // --- 모듈 호출
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        try {

            $getData = $Autorender->getUploadListUser(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $payment = array('0'=>'무료', '1'=>'유료');
            $this->setData('payment', $payment);

            $state = array('0'=>'준비중', '1'=>'제작요청', '2'=>'제작중', '3'=>'제작완료', '4'=>'자동삭제', '5'=>'수정요청', '6'=>'수정중', '7'=>'수정완료', '8'=>'시안확정');
            $this->setData('state', $state);

            $this->setData('page', $page);
            $this->setData('data', $getData['data']);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
