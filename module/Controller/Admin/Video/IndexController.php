<?php

namespace Controller\Admin\Video;

class IndexController extends \Controller\Admin\Controller
{
    public function index()
    {
        $this->redirect('./render_list.php');
    }
}
