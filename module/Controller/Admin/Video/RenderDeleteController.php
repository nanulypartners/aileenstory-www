<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Admin\Video;

use Component\Autorender\AutorenderAdmin;
use Framework\Debug\Exception\LayerNotReloadException;
use Framework\Debug\Exception\LayerException;
use Message;
use Request;

class RenderDeleteController extends \Controller\Admin\Controller
{
    public function index()
    {
        try {

            $autorenderAdmin = new AutorenderAdmin();

            // Post Data
            $postData =  Request::post()->toArray();

            $result = $autorenderAdmin->deleteData($postData);

            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit;

        } catch (\Exception $e) {
            throw new LayerNotReloadException($e->getMessage()); //새로고침안됨
        }
    }
}

