<?php
namespace Controller\Admin\Video;

use Component\Page\Page;

class RenderLogController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('video', 'videoset', 'renderlog');

        // --- 모듈 호출
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        try {

            $getData = $Autorender->getListLog(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $payment = array('0'=>'무료', '1'=>'유료');
            $this->setData('payment', $payment);

            $state = array('1'=>'제작요청', '3'=>'제작완료');
            $this->setData('state', $state);

            $this->setData('page', $page);
            $this->setData('data', $getData['data']);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
