<?php
namespace Controller\Admin\Video;

use Component\Page\Page;

class RenderUserDetailController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('video', 'videoset', 'renderuserlist');

        // --- 모듈 호출
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        try {

            $getData = $Autorender->getUserDetail(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $state = array('0'=>'제작대기', '1'=>'제작요청', '2'=>'제작중', '3'=>'제작완료', '4'=>'자동삭제');
            $this->setData('state', $state);

            $this->getView()->setDefine('layout', 'layout_blank.php');
            $this->setData('page', $page);
            $this->setData('data', $getData['data']);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
