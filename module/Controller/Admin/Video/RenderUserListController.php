<?php
namespace Controller\Admin\Video;

use Component\Page\Page;

class RenderUserListController extends \Controller\Admin\Controller
{
    public function index()
    {
        // --- 메뉴 설정
        $this->callMenu('video', 'videoset', 'renderuserlist');

        // --- 모듈 호출
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderAdmin');

        try {

            $getData = $Autorender->getListUser(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $payment = array('0'=>'무료', '1'=>'유료');
            $this->setData('payment', $payment);

            $state = array('0'=>'제작대기', '1'=>'제작요청', '2'=>'제작중', '3'=>'제작완료', '4'=>'자동삭제');
            $this->setData('state', $state);

            $this->setData('page', $page);
            $this->setData('data', $getData['data']);
            $this->setData('req', \Request::get()->toArray());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
