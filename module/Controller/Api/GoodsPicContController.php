<?php
namespace Controller\Api;

use Request;
use Exception;

class GoodsPicContController extends \Controller\Api\Controller
{
    public function index()
    {
        $header[] = 'Access-Control-Allow-Origin: *';
        $header[] = 'Access-Control-Allow-Credentials: true';
        $header[] = 'Access-Control-Allow-Headers: X-Requested-With, access-control-allow-methods, access-control-allow-headers, access-control-allow-origin';
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $this->setHeader($header);

        // *모듈 설정
        $goods = \App::load('\\Component\\Goods\\Goods');

        // *db 설정
        $db = \App::load('DB');

        $orderNo = Request::post()->get('orderNo');
        $orderName = Request::post()->get('orderName');
        $orderPhone = Request::post()->get('orderPhone');
        $goodsNo = Request::post()->get('goodsNo');
        $goodsPicNo = Request::post()->get('goodsPicNo');
        $picType = Request::post()->get('picType');
        $req = Request::post()->toArray();
        $defaultData = array();
        $extraData = array();
        $now = date('YmdHis', time());

        try
        {
            $goodsData = $goods->getGoodsView($goodsNo);

            // *goodsPic에 등록 확인
            if($picType == 'naver')
            {
                $query = "SELECT * FROM `es_goodsPic` WHERE `orderName` = '" . $orderName . "' AND `orderPhone` = '" . $orderPhone . "' AND `goodsNo` = " . $goodsNo . " AND `picType` = '" . $picType . "'";
            }
            else
            {
                // *회원정보 확인
                $tmp_query = "SELECT memNo FROM `es_order` WHERE `orderNo` = '" . $orderNo . "'";
                $tmp_data = $db->query_fetch($tmp_query, null)[0];
                $tmp_query = "SELECT memNm FROM `es_member` WHERE `memNo` = " . $tmp_data['memNo'];
                $tmp_data = $db->query_fetch($tmp_query, null)[0];
                $orderName = empty($tmp_data['memNm']) ? '' : $tmp_data['memNm'];
                $query = "SELECT * FROM `es_goodsPic` WHERE `orderNo` = '" . $orderNo . "' AND `goodsNo` = " . $goodsNo . " AND `picType` = '" . $picType . "'";
            }
            $pic_data = $db->query_fetch($query, null);
            $pic = $pic_data[0];
            $status = $pic['status'];
            $goodsPicNo = $pic['goodsPicNo'];
            if(empty($pic))
            {
                $query = "INSERT INTO `es_goodsPic` SET ";
                if($picType == 'naver')
                {
                    $query .= "`orderName` = '" . $orderName . "', `orderPhone` = '" . $orderPhone . "'";
                }
                else
                {
                    $query .= "`orderNo` = '" . $orderNo . "', `orderName` = '" . $orderName . "'";
                }
                $query .= ", `picType` = '" . $picType . "', `goodsNo` = " . $goodsNo . ", `status` = '1', `update` = '" . $now . "', `regdate` = '" . $now . "'";
                $output = $db->query($query);
                $goodsPicNo = $db->insert_id();
                $status = '1';
            }

            // *등록 된 내용
            $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo;
            $cont_data = $db->query_fetch($query, null);
            
            // *업로드 된 파일
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo;
            $file_data = $db->query_fetch($query, null);

            // *설정 된 내용
            $query = "SELECT * FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " ORDER BY `type` ASC, `order` ASC";
            $upload_data = $db->query_fetch($query, null);
            $extra_no = 1;
            foreach($upload_data as $no => $data)
            {
                // *default 항목
                if($data['type'] == 'default')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['type']) && !empty($tmp_value['input']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont =  $value['cont'];
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['input'] = $tmp_value['input'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        array_push($defaultData, $tmp_data);
                    }
                }

                // *extra 항목
                if($data['type'] == 'extra')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['title']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont = $value['cont'];
                            }
                        }
                        // *업로드 된 파일 적용
                        $tmp_files = array();
                        foreach($file_data as $key => $value)
                        {
                            if($value['order'] == $data['order'])
                            {
                                // *파일 타입
                                $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                                if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                                {
                                    $file_type = 'image';
                                }
                                elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                                {
                                    $file_type = 'video';
                                }
                                else
                                {
                                    $file_type = 'etc';
                                }
                                $tmp_file = array();
                                $tmp_file['goodsPicFileNo'] = $value['goodsPicFileNo'];
                                $tmp_file['path'] = str_replace('../', '/', $value['path']);
                                $tmp_file['name'] = $value['name'];
                                $tmp_file['type'] = $file_type;
                                array_push($tmp_files, $tmp_file);
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['no'] = str_pad($extra_no, 2, '0', STR_PAD_LEFT);
                        $tmp_data['title'] = $tmp_value['title'];
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['image'] = $tmp_value['image'];
                        $tmp_data['image_text'] = $tmp_value['image_text'];
                        $tmp_data['text'] = $tmp_value['text'];
                        $tmp_data['text_count'] = $tmp_value['text_count'];
                        $tmp_data['upload_count'] = $tmp_value['upload_count'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        $tmp_data['files'] = $tmp_files;
                        $tmp_data['upload_file_count'] = count($tmp_files);
                        array_push($extraData, $tmp_data);
                        $extra_no++;
                    }
                }
            }
            $uploadCount = count($extraData) + 1;

            // *기본 데이터에 등록 된 파일
            $defaultFiles = array();
            foreach($file_data as $key => $value)
            {
                if($value['order'] == 0)
                {
                    // *파일 타입
                    $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                    if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                    {
                        $file_type = 'image';
                    }
                    elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                    {
                        $file_type = 'video';
                    }
                    else
                    {
                        $file_type = 'etc';
                    }
                    $defaultFile = array();
                    $defaultFile['goodsPicFileNo'] = $value['goodsPicFileNo'];
                    $defaultFile['path'] = str_replace('../', '/', $value['path']);
                    $defaultFile['name'] = $value['name'];
                    $defaultFile['type'] = $file_type;
                    array_push($defaultFiles, $defaultFile);
                }
            }

            $return = array(
                'success' => true,
                'msg' => '정상적으로 호출 되었습니다.',
                'orderData' => $orderData,
                'goodsData' => $goodsData,
                'goodsPicNo' => $goodsPicNo,
                'defaultData' => $defaultData,
                'defaultFiles' => $defaultFiles,
                'extraData' => $extraData,
                'uploadCount' => $uploadCount,
                'orderName' => $orderName,
                'orderPhone' => $orderPhone,
                'status' => $status,
            );
            echo json_encode($return);
            exit;
        }
        catch(Exception $e)
        {
            $return = array(
                'success' => false,
                'msg' => $e->getMessage(),
            );
            echo json_encode($return);
            exit;
        }
    }
}