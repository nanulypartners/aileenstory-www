<?php
namespace Controller\Api;

use App;
use Session;
use Request;
use Exception;
use Component\Order\OrderAdmin;

class GoodsPicContUpsertController extends \Controller\Api\Controller
{
    public function index()
    {
        $header[] = 'Access-Control-Allow-Origin: *';
        $header[] = 'Access-Control-Allow-Credentials: true';
        $header[] = 'Access-Control-Allow-Headers: X-Requested-With, access-control-allow-methods, access-control-allow-headers, access-control-allow-origin';
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $this->setHeader($header);

        // *db 설정
        $db = \App::load('DB');
        $order = new OrderAdmin();

        try
        {
            $goodsPicNo = Request::post()->get('goodsPicNo');
            $orderNo = Request::post()->get('orderNo');
            $goodsNo = Request::post()->get('goodsNo');
            $status = Request::post()->get('status');
            $now = date('YmdHis', time());

            if($status == '2' || $status == '3')
            {
                $default_input = Request::post()->get('default_input');
                $extra_text = Request::post()->get('extra_text');

                // *등록 된 내용
                $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo;
                $cont_data = $db->query_fetch($query, null);

                foreach($default_input as $no => $data)
                {
                    if(empty($cont_data))
                    {
                        $query = "INSERT INTO `es_goodsPicCont` SET ";
                        $query .= "`goodsPicNo` = " . $goodsPicNo . ", `type` = 'default', `order` = " . $no . ", `cont` = '" . $data . "'";
                        $output = $db->query($query);
                    }
                    else
                    {
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == 'default')
                            {
                                if($value['order'] == $no)
                                {
                                    $query = "UPDATE `es_goodsPicCont` SET ";
                                    $query .= "`cont` = '" . $data . "' ";
                                    $query .= "WHERE `goodsPicContNo` = " . $value['goodsPicContNo'];
                                    $output = $db->query($query);
                                }
                            }
                        }
                    }
                }

                foreach($extra_text as $no => $data)
                {
                    if(empty($cont_data))
                    {
                        $query = "INSERT INTO `es_goodsPicCont` SET ";
                        $query .= "`goodsPicNo` = " . $goodsPicNo . ", `type` = 'extra', `order` = " . $no . ", `cont` = '" . $data . "'";
                        $output = $db->query($query);
                    }
                    else
                    {
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == 'extra')
                            {
                                if($value['order'] == $no)
                                {
                                    $query = "UPDATE `es_goodsPicCont` SET ";
                                    $query .= "`cont` = '" . $data . "' ";
                                    $query .= "WHERE `goodsPicContNo` = " . $value['goodsPicContNo'];
                                    $output = $db->query($query);
                                }
                            }
                        }
                    }
                }

                // *주문상태 변경
                if($status == '3')
                {
                    // *goodsPic 정보
                    $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
                    $pic_data = $db->query_fetch($query, null);
                    $pic = $pic_data[0];
                    if($pic['picType'] == 'aileen')
                    {
                        $order->updateStatusPreprocess($orderNo,  $order->getOrderGoodsByGoodsNo($orderNo, $goodsNo), 'p', 'g5', __('고객의 '), false);
                    }
                }
            }
            elseif($status == '6' || $status == '7')
            {
                $modify_order = Request::post()->get('order');
                $modify_text = Request::post()->get('modify_text');

                // *등록 된 내용
                $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` = " . $modify_order;
                $cont_data = $db->query_fetch($query, null);
                $cont_info = $cont_data[0];

                if(empty($cont_info))
                {
                    $query = "INSERT INTO `es_goodsPicCont` SET ";
                    $query .= "`goodsPicNo` = " . $goodsPicNo . ", `type` = 'modify', `order` = " . $modify_order . ", `cont` = '" . $modify_text . "'";
                    $output = $db->query($query);
                }
                else
                {
                    $query = "UPDATE `es_goodsPicCont` SET ";
                    $query .= "`cont` = '" . $modify_text . "' ";
                    $query .= "WHERE `goodsPicContNo` = " . $cont_info['goodsPicContNo'];
                    $output = $db->query($query);
                }

                // *주문상태 변경
                if($status == '7')
                {
                    // *goodsPic 정보
                    $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
                    $pic_data = $db->query_fetch($query, null);
                    $pic = $pic_data[0];
                    if($pic['picType'] == 'aileen')
                    {
                        $order->updateStatusPreprocess($orderNo,  $order->getOrderGoodsByGoodsNo($orderNo, $goodsNo), 'g', 'g6', __('고객의 '), true);
                    }
                }
            }
            elseif($status == '10')
            {
                // *goodsPic 정보
                $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
                $pic_data = $db->query_fetch($query, null);
                $pic = $pic_data[0];
                if($pic['picType'] == 'aileen')
                {
                    $order->updateStatusPreprocess($orderNo,  $order->getOrderGoodsByGoodsNo($orderNo, $goodsNo), 'g', 'g9', __('고객의 '), true);
                }
            }
            
            // *수정일 저장
            $query = "UPDATE `es_goodsPic` SET ";
            $query .= "`status` = '" . $status . "', `update` = '" . $now . "' ";
            $query .= "WHERE `goodsPicNo` = " . $goodsPicNo;
            $output = $db->query($query);

            $return = array(
                'success' => true,
                'msg' => '저장 되었습니다.',
                'post' => Request::post()->toArray(),
                'goods_info' => $goods_info,
            );
            echo json_encode($return);
            exit;
        }
        catch(Exception $e)
        {
            $return = array(
                'success' => false,
                'msg' => $e->getMessage(),
            );
            echo json_encode($return);
            exit;
        }
    }
}
