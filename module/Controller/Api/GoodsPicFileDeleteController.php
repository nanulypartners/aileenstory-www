<?php
namespace Controller\Api;

use App;
use Request;
use Exception;
use Component\Database\DBTableField;

class GoodsPicFileDeleteController extends \Controller\Api\Controller
{
    public function index()
    {
        $header[] = 'Access-Control-Allow-Origin: *';
        $header[] = 'Access-Control-Allow-Credentials: true';
        $header[] = 'Access-Control-Allow-Headers: X-Requested-With, access-control-allow-methods, access-control-allow-headers, access-control-allow-origin';
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $this->setHeader($header);

        // *db 설정
        $db = \App::load('DB');

        try
        {
            $goodsPicFileNo = Request::post()->get('goodsPicFileNo');

            // *File 정보
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicFileNo` = " . $goodsPicFileNo;
            $file_data = $db->query_fetch($query, null);
            $file = $file_data[0];

            // *DB에서 삭제
            $query = "DELETE FROM `es_goodsPicFile` WHERE `goodsPicFileNo` = " . $goodsPicFileNo;
            $output = $db->query($query);

            $return = array(
                'success' => true,
                'msg' => '정상적으로 삭제 되었습니다.',
                'file' => $file,
                'goodsPicFileNo' => $goodsPicFileNo,
            );
            echo json_encode($return);
            exit;
        }
        catch(Exception $e)
        {
            $this->json([
                'error' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
