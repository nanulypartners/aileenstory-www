<?php
namespace Controller\Api;

use App;
use Request;
use Exception;
use Component\Database\DBTableField;

class GoodsPicFileUploadController extends \Controller\Api\Controller
{
    public function index()
    {
        $header[] = 'Access-Control-Allow-Origin: *';
        $header[] = 'Access-Control-Allow-Credentials: true';
        $header[] = 'Access-Control-Allow-Headers: X-Requested-With, access-control-allow-methods, access-control-allow-headers, access-control-allow-origin';
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $this->setHeader($header);

        // *db 설정
        $db = \App::load('DB');

        try
        {
            $goodsPicNo = Request::post()->get('goodsPicNo');
            $order = Request::post()->get('order');
            $upload_files = Request::post()->get('upload_files');
            $now = date('YmdHis', time());
            $file_list = array();

            // *goodsPic에 등록 확인
            $query = "SELECT * FROM `es_goodsPic` WHERE `goodsPicNo` = " . $goodsPicNo;
            $pic_data = $db->query_fetch($query, null);
            $pic = $pic_data[0];
            if(empty($pic))
            {
                $return = array(
                    'success' => false,
                    'msg' => '등록된 goodsPic이 없습니다.',
                );
                echo json_encode($return);
                exit;
            }
            else
            {
                $query = "UPDATE `es_goodsPic` SET ";
                $query .= "`update` = '" . $now . "' ";
                $query .= "WHERE `goodsPicNo` = " . $goodsPicNo;
                $output = $db->query($query);
            }

            foreach($upload_files as $key => $value)
            {
                $query = "INSERT INTO `es_goodsPicFile` SET ";
                $query .= "`goodsPicNo` = " . $goodsPicNo . ", `order` = " . $order . ", `path` = '" . $value['file_path'] . "', `name` = '" . $value['file_name'] . "'";
                $output = $db->query($query);
                $tmp_file = array(
                    'goodsPicFileNo' => $db->insert_id(),
                    'file_name' => $value['file_name'],
                    'file_path' => $value['file_path'],
                    'file_type' => $value['file_type'],
                );
                array_push($file_list, $tmp_file);
            }
            
            $return = array(
                'success' => true,
                'msg' => '정상적으로 저장 되었습니다.',
                'file_list' => $file_list,
                'order' => $order,
            );
            echo json_encode($return);
            exit;
        }
        catch(Exception $e)
        {
            $return = array(
                'success' => false,
                'msg' => $e->getMessage(),
            );
            echo json_encode($return);
            exit;
        }
    }
}
