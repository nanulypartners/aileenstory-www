<?php
namespace Controller\Api;

use Request;
use Exception;

class GoodsPicMakeController extends \Controller\Api\Controller
{
    public function index()
    {
        $header[] = 'Access-Control-Allow-Origin: *';
        $header[] = 'Access-Control-Allow-Credentials: true';
        $header[] = 'Access-Control-Allow-Headers: X-Requested-With, access-control-allow-methods, access-control-allow-headers, access-control-allow-origin';
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $this->setHeader($header);

        // *db 설정
        $db = \App::load('DB');

        $orderNo = Request::post()->get('orderNo');
        $orderName = Request::post()->get('orderName');
        $orderPhone = Request::post()->get('orderPhone');
        $goodsNo = Request::post()->get('goodsNo');
        $goodsPicNo = Request::post()->get('goodsPicNo');
        $picType = Request::post()->get('picType');
        $req = Request::post()->toArray();
        $modifyFile = array();

        try
        {
            // *goodsPic에 등록 확인
            if($picType == 'naver')
            {
                $query = "SELECT * FROM `es_goodsPic` WHERE `orderName` = '" . $orderName . "' AND `orderPhone` = '" . $orderPhone . "' AND `goodsNo` = " . $goodsNo . " AND `picType` = '" . $picType . "'";
            }
            else
            {
                $query = "SELECT * FROM `es_goodsPic` WHERE `orderNo` = '" . $orderNo . "' AND `goodsNo` = " . $goodsNo . " AND `picType` = '" . $picType . "'";
            }
            $pic_data = $db->query_fetch($query, null);
            $pic = $pic_data[0];
            $status = $pic['status'];
            $goodsPicNo = $pic['goodsPicNo'];

            // *제작 내용
            $query = "SELECT * FROM `es_goodsPicMake` WHERE `goodsPicNo` = " . $goodsPicNo;
            $make_data = $db->query_fetch($query, null);
            $make_info = $make_data[0];

            $down_video = '';
            if($pic['status'] == 10)
            {
                $down_video = $make_info['downVideo'];
            }

            // *수정 요청 파일 정보
            $defaultFiles = array();
            $query = "SELECT max(`order`) AS maxOrder FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` >= 1000";
            $file_data = $db->query_fetch($query, null);
            $max_order = $file_data[0]['maxOrder'];
            // *등록 된 내용
            $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` = " . $max_order;
            $cont_data = $db->query_fetch($query, null);
            $cont_info = $cont_data[0];
            // *등록 된 파일
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo . " AND `order` = " . $max_order;
            $file_data = $db->query_fetch($query, null);
            foreach($file_data as $key => $value)
            {
                // *파일 타입
                $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                {
                    $file_type = 'image';
                }
                elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                {
                    $file_type = 'video';
                }
                else
                {
                    $file_type = 'etc';
                }
                $defaultFile = array();
                $defaultFile['goodsPicFileNo'] = $value['goodsPicFileNo'];
                $defaultFile['path'] = str_replace('../', '/', $value['path']);
                $defaultFile['name'] = $value['name'];
                $defaultFile['type'] = $file_type;
                array_push($defaultFiles, $defaultFile);
            }
            
            $return = array(
                'success' => true,
                'msg' => '정상적으로 호출 되었습니다.',
                'playVideo' => $make_info['playVideo'],
                'downVideo' => $down_video,
                'maxOrder' => $max_order,
                'defaultFiles' => $defaultFiles,
                'cont' => $cont_info['cont'],
                'goodsPicNo' => $goodsPicNo,
                'status' => $status,
            );
            echo json_encode($return);
            exit;
        }
        catch(Exception $e)
        {
            $return = array(
                'success' => false,
                'msg' => $e->getMessage(),
            );
            echo json_encode($return);
            exit;
        }
    }
}