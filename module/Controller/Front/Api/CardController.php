<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Api;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;
use Component\Autorender\AutorenderUser;

class CardController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $data = file_get_contents('php://input');
            $json = json_decode($data);

            if ($json->nanuly!="sksnfl2020@" || !$json->nanuly_type) {
                $result['state'] = "error";
                $result['msg'] = "잘못된 접근입니다.";
            } else {
                if ($json->nanuly_type=="vimeoUpload") {
                    $cardUser->vimeoUpdate($json, 1);
                    $result['state'] = "success";
                } else if ($json->nanuly_type=="vimeoDelete") {
                    $cardUser->vimeoUpdate($json, 2);
                    $result['state'] = "success";
                }
            }

            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit;

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
