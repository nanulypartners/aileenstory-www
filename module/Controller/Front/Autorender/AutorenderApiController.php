<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Autorender;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;
use Component\Autorender\AutorenderUser;

class AutorenderApiController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $autorenderUser = new AutorenderUser();

        try {

            $data = file_get_contents('php://input');
            $json = json_decode($data);

            if ($json->nanuly!="sksnfl2020@" || !$json->nanuly_type) {
                $result['state'] = "error";
                $result['msg'] = "잘못된 접근입니다.";
            } else {
                if ($json->nanuly_type=="userInsert") {
                    $autorenderUser->insertData($json->uploads, 1);
                    $result['state'] = "success";
                } else if ($json->nanuly_type=="autoRenderStart") {
                    $autorenderUser->insertData($json->uploads);
                    $result['state'] = "success";
                } else if ($json->nanuly_type=="autoDel") {
                    foreach ($json->uploads as $value) {
                        $autorenderUser->deleteData($value);
                    }
                    $result['state'] = "success";
                } else if ($json->nanuly_type=="autoRenderEnd") {
                    foreach ($json->uploads as $value) {
                        $autorenderUser->insertData($value);
                    }
                    $result['state'] = "success";
                } else if ($json->nanuly_type=="delReset") {
                    foreach ($json->uploads as $value) {
                        $autorenderUser->delReset($value);
                    }
                    $result['state'] = "success";
                }
            }

            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit;

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
