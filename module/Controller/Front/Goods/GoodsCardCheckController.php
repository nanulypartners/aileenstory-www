<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class GoodsCardCheckController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $data = file_get_contents('php://input');
            $json = json_decode($data, true);

            //초대장 정보 가져오기
            $cardInfo = $cardUser->getCardOrder($json['odsno'])[0];
            $timestamp = strtotime("+5 months");

            //초대장 유효 체크
            if(!empty($cardInfo) && $cardInfo['state']!="1" && $cardInfo['regDt'] < date("Y-m-d H:i:s", $timestamp)){
                echo "Y";
                exit;
            }else{
                echo "N";
                exit;
            }

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
