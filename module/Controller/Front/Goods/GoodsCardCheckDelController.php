<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class GoodsCardCheckDelController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {
            $type = Request::get()->get('type');

            if ($type=='nanuly') {
                //초대장 정보 가져오기
                $cardInfo = $cardUser->getCardOrderDelList();
                $delList = array();
                foreach($cardInfo as $key=>$val){
                    $delList[$key]['url'] = $val['url'];
                    $delList[$key]['vimeoUri'] = $val['vimeoUri'];
                    $cardUser->delCardOrder($val['no']);
                }
                echo json_encode($delList, JSON_UNESCAPED_UNICODE);
                exit;
            }
            exit;
        } catch (\Exception $e) {
            throw $e;
        }

    }
}
