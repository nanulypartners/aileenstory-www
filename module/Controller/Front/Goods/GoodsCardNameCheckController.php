<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class GoodsCardNameCheckController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardAdmin = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $getData = Request::get()->toArray();

            $check = $cardUser->checkCardName($getData);
            echo $check;
            exit;

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
