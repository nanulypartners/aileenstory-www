<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class GoodsCardPsController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardAdmin = \App::load('\\Component\\Mobilecard\\MobilecardAdmin');
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $postData = Request::post()->toArray();
            $fileData = Request::files()->all();

            /*메인이미지 blob->base64변환*/
            if($fileData['card']['tmp_name']['main_img']['main_url']){
                $path = $fileData['card']['tmp_name']['main_img']['main_url'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);

                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $postData['card']['main_img']['main_url'] = $base64;
            }
            /*메인이미지 blob->base64변환*/

            /*서브이미지 blob->base64변환*/
            foreach($fileData['card']['tmp_name']['photo'] as $k=>$v){
                $path = $v;
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);

                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $postData['card']['photo'][$k] = $base64;
            }
            /*서브이미지 blob->base64변환*/

            //초대장 주소 체크
            $postData['web_name'] = $postData['card']['card_title'];
            $check = $cardUser->checkCardName($postData);

            if($check=="Y" || $postData['web_name']==""){
                echo "web_name_error";
            }else{
                $result = $cardUser->setCardOrder($postData);
                if($result){

                    if($result=="expiration"){
                        echo $result;
                        exit;
                    }

                    if($result!="url_ok"){
                        $postData['old_folder'] = $result;
                    }

                    //초대장 전송 api로 전송
                    $url = "https://app.aileenstory.com/api/mobilecard.php";
                    $json_data = json_encode($postData);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: '.strlen($json_data)));
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    $output = curl_exec($ch);
                    curl_close($ch);

                    echo $output;

                    exit;

                }else{
                    echo "fail";
                    exit;
                }
            }

            exit;

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
