<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;
use Component\Member\Member;

class GoodsCardSkinSelectController extends \Controller\Front\Controller
{
    public function index()
    {

        $session = \App::getInstance('session');
        $memberService = new Member();
        $odId = $memberService->getMemberId($session->get(Member::SESSION_MEMBER_LOGIN . '.memNo'));
        if (empty($odId)) {
            echo '<script>alert("로그인 후 이용해 주세요.");opener.location.href="/member/login.php";self.close();</script>';
            exit;
        }

        $db = \App::load('DB');
        $strSQL = " SELECT * FROM es_mobileCardskin ";
        $result = $db->query_fetch($strSQL);

        $this->setData('cards', $result);
    }
}
