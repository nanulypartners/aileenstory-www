<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Component\Database\DBTableField;
use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class GoodsCardWriteController extends \Controller\Front\Controller
{

    public function index()
    {

        // 모듈호출
        $cardUser = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            $orderNo = Request::get()->get('orderNo');
            if (empty($orderNo)) {
                $orderNo = time().mt_rand(000, 999);
            }

            $getCategory = Request::get()->get('category');
            if ($getCategory=='wedding') {
                $getCategory = 1;
            } else if ($getCategory=='baby') {
                $getCategory = 2;
            } else if ($getCategory=='old') {
                $getCategory = 3;
            }
            $getData = Request::get()->toArray();
            $getSkin = Request::get()->get('skin');

            //모바일 초대장 저장정보 가져오기
            $cardOrder = $cardUser->getCardOrder($orderNo)[0];

            //스킨정보 가져오기
            $skinInfo = $cardUser->getSkin($getData)['data'];

            //예문정보 가져오기
            $textInfo = $cardUser->getText($getData)['data'];

            if ($cardOrder) {
                if( substr($cardOrder['regDt'], 0, 10) < date("Y-m-d", strtotime("-5 month", time()))  ){
                    throw new AlertCloseException('초대장 이용기간이 만료되었습니다. 초대장을 새로 생성해 주세요.');
                }

                if ($cardOrder['state']==1) {
                    throw new AlertCloseException('초대장이 비활성화 상태이므로 수정할 수 없습니다.');
                }
            }

            if($cardOrder['url'] && $getCategory==$cardOrder['category'] && $getSkin==$cardOrder['skin']){

                if( substr($cardOrder['regDt'], 0, 10) < date("Y-m-d", strtotime("-5 month", time()))  ){
                    throw new AlertCloseException('초대장 이용기간이 만료되었습니다. 초대장을 새로 생성해 주세요.');
                } else {
                    //초대장 전송 api로 전송
                    $data['type'] = "get_card";
                    $data['card']['card_title'] = $cardOrder['url'];
                    $url = "https://app.aileenstory.com/api/mobilecard.php";
                    $json_data = json_encode($data);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: '.strlen($json_data)));
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    $output = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    $card_data = $output['yaml_data']['card'];

                    foreach($card_data['photo'] as $k=>$v){
                        $card_data['photo'][$k] = str_replace('data:image/;', 'data:image/png;', $v);
                    }

                    $this->setData('card_data', $card_data);
                }

            }

            $memInfo = Session::get('member');
            $this->setData('memInfo', $memInfo);

            $setData = '초대장관리';
            $this->setData('setData', $setData);
            $this->setData('cardOrder', $cardOrder);
            $this->setData('skinInfo', $skinInfo);
            $this->setData('textInfo', $textInfo);
            $this->setData('orderNo', $orderNo);
            $this->setData('category', $getCategory);

        } catch (\Exception $e) {
            throw $e;
        }


    }
}
