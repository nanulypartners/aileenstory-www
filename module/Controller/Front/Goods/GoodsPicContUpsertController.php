<?php
namespace Controller\Front\Goods;

use App;
use Session;
use Request;
use Exception;
use Component\Order\OrderAdmin;

class GoodsPicContUpsertController extends \Controller\Front\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');
        $order = new OrderAdmin();

        try
        {
            $orderNo = Request::post()->get('orderNo');
            $goodsNo = Request::post()->get('goodsNo');
            $goodsPicNo = Request::post()->get('goodsPicNo');
            $status = Request::post()->get('status');
            $default_input = Request::post()->get('default_input');
            $extra_text = Request::post()->get('extra_text');
            $now = date('YmdHis', time());

            // *등록 된 내용
            $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo;
            $cont_data = $db->query_fetch($query, null);

            foreach($default_input as $no => $data)
            {
                if(empty($cont_data))
                {
                    $query = "INSERT INTO `es_goodsPicCont` SET ";
                    $query .= "`orderNo` = '" . $orderNo . "', `goodsNo` = " . $goodsNo . ", `goodsPicNo` = " . $goodsPicNo . ", `type` = 'default', `order` = " . $no . ", `cont` = '" . $data . "'";
                    $output = $db->query($query);
                }
                else
                {
                    foreach($cont_data as $key => $value)
                    {
                        if($value['type'] == 'default')
                        {
                            if($value['order'] == $no)
                            {
                                $query = "UPDATE `es_goodsPicCont` SET ";
                                $query .= "`cont` = '" . $data . "' ";
                                $query .= "WHERE `goodsPicContNo` = " . $value['goodsPicContNo'];
                                $output = $db->query($query);
                            }
                        }
                    }
                }
            }

            foreach($extra_text as $no => $data)
            {
                if(empty($cont_data))
                {
                    $query = "INSERT INTO `es_goodsPicCont` SET ";
                    $query .= "`orderNo` = '" . $orderNo . "', `goodsNo` = " . $goodsNo . ", `goodsPicNo` = " . $goodsPicNo . ", `type` = 'extra', `order` = " . $no . ", `cont` = '" . $data . "'";
                    $output = $db->query($query);
                }
                else
                {
                    foreach($cont_data as $key => $value)
                    {
                        if($value['type'] == 'extra')
                        {
                            if($value['order'] == $no)
                            {
                                $query = "UPDATE `es_goodsPicCont` SET ";
                                $query .= "`cont` = '" . $data . "' ";
                                $query .= "WHERE `goodsPicContNo` = " . $value['goodsPicContNo'];
                                $output = $db->query($query);
                            }
                        }
                    }
                }
            }

            // *수정일 저장
            $query = "UPDATE `es_goodsPic` SET ";
            $query .= "`status` = '" . $status . "', `update` = '" . $now . "' ";
            $query .= "WHERE `goodsPicNo` = " . $goodsPicNo;
            $output = $db->query($query);

            // *주문상태 변경
            if($status == 2)
            {
                $order->updateStatusPreprocess($orderNo, $order->getOrderGoods($orderNo), 'g', 'g5', __('고객요청에 의해'), true);
            }

            $this->json([
                'success' => true,
                'msg' => __('저장 되었습니다.'),
                'req' => Request::post()->toArray(),
            ]);
        }
        catch(Exception $e)
        {
            $this->json([
                'success' => false,
                'msg' => $e->getMessage(),
            ]);
        }
    }
}
