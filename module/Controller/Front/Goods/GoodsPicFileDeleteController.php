<?php
namespace Controller\Api;

use App;
use Request;
use Exception;
use Component\Database\DBTableField;

class GoodsPicFileDeleteController extends \Controller\Api\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');

        try
        {
            $goodsPicFileNo = Request::post()->get('goodsPicFileNo');
            $result = array();
            $result['success'] = true;
            $result['msg'] = '정상적으로 삭제 되었습니다.';

            // *FTP 연결
            $ftp_path = '/www/files/upload';
            $ftp_conf['server'] = 'leveltest1.cafe24.com';
            $ftp_conf['port'] = '21';
            $ftp_conf['user'] = 'leveltest1';
            $ftp_conf['pass'] = 'Leveltest11!';
            $ftp_conn = ftp_connect($ftp_conf['server'], $ftp_conf['port']);
            $ftp_login = ftp_login($ftp_conn, $ftp_conf['user'], $ftp_conf['pass']);
            // *check connection
            if((!$ftp_conn) || (!$ftp_login))
            {
                $result['success'] = false;
                $result['msg'] = 'FTP 연결 시 오류가 발생했습니다.';
                $this->json($result);
                return;
            }
            else
            {
                ftp_pasv($ftp_conn, true);
            }

            // *File 정보
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicFileNo` = " . $goodsPicFileNo;
            $file_data = $db->query_fetch($query, null);
            $file = $file_data[0];

            // *File 삭제
            $delete = ftp_delete($ftp_conn, $file['path']);
            if(!$delete)
            {
                $result['success'] = false;
                $result['msg'] = '삭제 중 문제가 발생했습니다. 다시 시도해주세요.';
                $this->json($result);
                return;
            }
            else
            {
                // *DB에서 삭제
                $query = "DELETE FROM `es_goodsPicFile` WHERE `goodsPicFileNo` = " . $goodsPicFileNo;
                $output = $db->query($query);
            }
            $this->json($result);
        }
        catch(Exception $e)
        {
            $this->json([
                'error' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
