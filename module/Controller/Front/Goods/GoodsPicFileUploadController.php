<?php
namespace Controller\Front\Goods;

use App;
use Request;
use Exception;
use Component\Database\DBTableField;
use Component\Storage\Storage;

class GoodsPicFileUploadController extends \Controller\Front\Controller
{
    public function index()
    {
        // *db 설정
        $db = \App::load('DB');
        // *Storage 설정
        $storage = Storage::disk(Storage::PATH_CODE_GOODS, 'http://leveltest1.cafe24.com');

        try
        {
            $orderNo = Request::post()->get('orderNo');
            $goodsNo = Request::post()->get('goodsNo');
            $order = Request::post()->get('order');
            $files = Request::files()->toArray();
            $now = date('YmdHis', time());
            $result = array();
            $result['success'] = true;
            $result['msg'] = '정상적으로 업로드 되었습니다.';
            foreach($files['files']['tmp_name'] as $key => $value)
            {
                $file_ext = substr($files['files']['name'][$key], strrpos($files['files']['name'][$key], '.') + 1);
                $file_name = strtotime(date('Y-m-d H:i:s')) . round(microtime(true) * 1000) . '.' . $file_ext;
                $file_path = 'aa/' . $file_name;
                $storage->upload($value, $file_path);
            }
            /* 
            // *FTP 연결
            $ftp_path = '/www/files/upload';
            $ftp_conf['server'] = 'leveltest1.cafe24.com';
            $ftp_conf['port'] = '21';
            $ftp_conf['user'] = 'leveltest1';
            $ftp_conf['pass'] = 'Leveltest11!';
            $ftp_conn = ftp_connect($ftp_conf['server'], $ftp_conf['port']);
            $ftp_login = ftp_login($ftp_conn, $ftp_conf['user'], $ftp_conf['pass']);
            // *check connection
            if((!$ftp_conn) || (!$ftp_login))
            {
                $result['success'] = false;
                $result['msg'] = 'FTP 연결 시 오류가 발생했습니다.';
                $this->json($result);
                return;
            }
            else
            {
                ftp_pasv($ftp_conn, true);
            }

            // *goodsPic에 등록 확인
            $query = "SELECT * FROM `es_goodsPic` WHERE `orderNo` = '" . $orderNo . "' AND `goodsNo` = " . $goodsNo;
            $pic_data = $db->query_fetch($query, null);
            $pic = $pic_data[0];
            $goodsPicNo = $pic['goodsPicNo'];
            if(empty($pic))
            {
                $query = "INSERT INTO `es_goodsPic` SET ";
                $query .= "`orderNo` = '" . $orderNo . "', `goodsNo` = " . $goodsNo . ", `status` = '1', `update` = '" . $now . "', `regdate` = '" . $now . "'";
                $output = $db->query($query);
                $goodsPicNo = $db->insert_id();
            }
            else
            {
                $query = "UPDATE `es_goodsPic` SET ";
                $query .= "`update` = '" . $now . "' ";
                $query .= "WHERE `goodsPicNo` = " . $goodsPicNo;
                $output = $db->query($query);
            }

            // *orderNo 폴더가 만들어졌는지 확인
            ftp_chdir($ftp_conn, $ftp_path);
            $dir_list = ftp_nlist($ftp_conn, '.');
            //해당 디렉토리에 orderNo 폴더가 없다면 생성
            if(!in_array($orderNo, $dir_list))
            {
                ftp_mkdir($ftp_conn, $orderNo);
            }
            $ftp_path = $ftp_path . '/' . $orderNo;
            ftp_chdir($ftp_conn, $ftp_path);
            $dir_list = ftp_nlist($ftp_conn, '.');
            //해당 디렉토리에 goodsNo 폴더가 없다면 생성
            if(!in_array($goodsNo, $dir_list))
            {
                ftp_mkdir($ftp_conn, $goodsNo);
            }
            $ftp_path = $ftp_path . '/' . $goodsNo;
            ftp_chdir($ftp_conn, $ftp_path);
            $dir_list = ftp_nlist($ftp_conn, '.');
            //해당 디렉토리에 order 폴더가 없다면 생성
            if(!in_array($order, $dir_list))
            {
                ftp_mkdir($ftp_conn, $order);
            }
            $ftp_path = $ftp_path . '/' . $order . '/';

            foreach($files['files']['tmp_name'] as $key => $value)
            {
                $file_ext = substr($files['files']['name'][$key], strrpos($files['files']['name'][$key], '.') + 1);
                $file_name = strtotime(date('Y-m-d H:i:s')) . round(microtime(true) * 1000) . '.' . $file_ext;
                $file_path = $ftp_path . $file_name;
                $upload = ftp_put($ftp_conn, $file_path , $value, FTP_BINARY);
                if(!$upload)
                {
                    $result['success'] = false;
                    $result['msg'] = '업로드 중 문제가 발생했습니다. 다시 시도해주세요.';
                    $result['upload'] = $upload;
                    $this->json($result);
                    return;
                }
                else
                {
                    $query = "INSERT INTO `es_goodsPicFile` SET ";
                    $query .= "`orderNo` = '" . $orderNo . "', `goodsNo` = " . $goodsNo . ", `goodsPicNo` = " . $goodsPicNo . ", `order` = " . $order . ", `path` = '" . $file_path . "', `name` = '" . $file_name . "'";
                    $output = $db->query($query);
                    $result['files'][$db->insert_id()]['path'] = str_replace('/www', 'http://leveltest1.cafe24.com', $file_path);
                    if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                    {
                        $file_type = 'image';
                    }
                    elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                    {
                        $file_type = 'video';
                    }
                    else
                    {
                        $file_type = 'etc';
                    }
                    $result['files'][$db->insert_id()]['type'] = $file_type;
                }
            } */
            $this->json($result);
        }
        catch(Exception $e)
        {
            $this->json([
                'error' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
