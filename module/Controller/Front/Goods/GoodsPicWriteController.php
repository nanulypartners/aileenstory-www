<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2020. withylab
 * @link http://www.withylab.com
 */
namespace Controller\Front\Goods;

use Framework\Debug\Exception\AlertRedirectException;
use Framework\Debug\Exception\Except;
use Framework\Debug\Exception\AlertBackException;
use Message;
use Globals;
use Request;
use Logger;
use Session;
use Exception;

class GoodsPicWriteController extends \Controller\Front\Controller
{
    public function index()
    {
        // *모듈 설정
        $order = \App::load('\\Component\\Order\\Order');
        $goods = \App::load('\\Component\\Goods\\Goods');

        // *db 설정
        $db = \App::load('DB');

        $orderNo = Request::get()->get('orderNo');
        $goodsNo = Request::get()->get('goodsNo');
        $goodsPicNo = Request::post()->get('goodsPicNo');
        $type = Request::get()->get('type');
        $req = Request::post()->toArray();
        $defaultData = array();
        $extraData = array();

        try 
        {
            if(empty($orderNo)) $orderNo = Request::post()->get('orderNo');
            if(empty($goodsNo)) $goodsNo = Request::post()->get('goodsNo');

            // *연결한 위치 설정
            if(!empty($type))
            {
                $orderData['orderNo'] = $type;
            }
            else
            {
                // *주문, 상품 정보
                $orderData = $order->getOrderView($orderNo);
            }
            $this->setData('orderData', $orderData);
            $goodsData = $goods->getGoodsView($goodsNo);
            $this->setData('goodsData', $goodsData);

            // *goodsPic에 등록 확인
            $query = "SELECT * FROM `es_goodsPic` WHERE `orderNo` = '" . $orderNo . "' AND `goodsNo` = " . $goodsNo;
            $pic_data = $db->query_fetch($query, null);
            $pic = $pic_data[0];
            $goodsPicNo = $pic['goodsPicNo'];
            if(empty($pic))
            {
                $query = "INSERT INTO `es_goodsPic` SET ";
                $query .= "`orderNo` = '" . $orderNo . "', `goodsNo` = " . $goodsNo . ", `status` = '1', `update` = '" . $now . "', `regdate` = '" . $now . "'";
                $output = $db->query($query);
                $goodsPicNo = $db->insert_id();
            }
            $this->setData('goodsPicNo', $goodsPicNo);

            // *등록 된 내용
            $query = "SELECT * FROM `es_goodsPicCont` WHERE `goodsPicNo` = " . $goodsPicNo;
            $cont_data = $db->query_fetch($query, null);
            
            // *업로드 된 파일
            $query = "SELECT * FROM `es_goodsPicFile` WHERE `goodsPicNo` = " . $goodsPicNo;
            $file_data = $db->query_fetch($query, null);

            // *설정 된 내용
            $query = "SELECT * FROM `es_goodsPicSetup` WHERE `goodsNo` = " . $goodsNo . " ORDER BY `type` ASC, `order` ASC";
            $upload_data = $db->query_fetch($query, null);
            $extra_no = 1;
            foreach($upload_data as $no => $data)
            {
                // *default 항목
                if($data['type'] == 'default')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['type']) && !empty($tmp_value['input']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont =  $value['cont'];
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['input'] = $tmp_value['input'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        array_push($defaultData, $tmp_data);
                    }
                }

                // *extra 항목
                if($data['type'] == 'extra')
                {
                    $tmp_value = unserialize($data['value']);
                    if(!empty($tmp_value['title']))
                    {
                        // *등록 된 내용 적용
                        $tmp_cont = '';
                        foreach($cont_data as $key => $value)
                        {
                            if($value['type'] == $data['type'] && $value['order'] == $data['order'])
                            {
                                $tmp_cont = $value['cont'];
                            }
                        }
                        // *업로드 된 파일 적용
                        $tmp_files = array();
                        foreach($file_data as $key => $value)
                        {
                            if($value['order'] == $data['order'])
                            {
                                // *파일 타입
                                $file_ext = substr($value['path'], strrpos($value['path'], '.') + 1);
                                if($file_ext == 'jfif' || $file_ext == 'pjpeg' || $file_ext == 'pjp' || $file_ext == 'jpeg' || $file_ext == 'jpg' || $file_ext == 'png' || $file_ext == 'gif')
                                {
                                    $file_type = 'image';
                                }
                                elseif($file_ext == 'ogm' || $file_ext == 'wmv' || $file_ext == 'mpg' || $file_ext == 'webm' || $file_ext == 'ogv' || $file_ext == 'mov' || $file_ext == 'asx' || $file_ext == 'mpeg' || $file_ext == 'mp4' || $file_ext == 'm4v' || $file_ext == 'avi')
                                {
                                    $file_type = 'video';
                                }
                                else
                                {
                                    $file_type = 'etc';
                                }
                                $tmp_file = array();
                                $tmp_file['goodsPicFileNo'] = $value['goodsPicFileNo'];
                                $tmp_file['path'] = $value['path'];
                                $tmp_file['name'] = $value['name'];
                                $tmp_file['type'] = $file_type;
                                array_push($tmp_files, $tmp_file);
                            }
                        }
                        $tmp_data = array();
                        $tmp_data['no'] = str_pad($extra_no, 2, '0', STR_PAD_LEFT);
                        $tmp_data['title'] = $tmp_value['title'];
                        $tmp_data['type'] = $tmp_value['type'];
                        $tmp_data['image'] = $tmp_value['image'];
                        $tmp_data['image_text'] = $tmp_value['image_text'];
                        $tmp_data['text'] = $tmp_value['text'];
                        $tmp_data['text_count'] = $tmp_value['text_count'];
                        $tmp_data['upload_count'] = $tmp_value['upload_count'];
                        $tmp_data['order'] = $data['order'];
                        $tmp_data['cont'] = $tmp_cont;
                        $tmp_data['files'] = $tmp_files;
                        $tmp_data['upload_file_count'] = count($tmp_files);
                        array_push($extraData, $tmp_data);
                        $extra_no++;
                    }
                }
            }
            $this->setData('defaultData', $defaultData);
            $this->setData('extraData', $extraData);
            $this->setData('uploadCount', count($extraData) + 1);
        }
        catch(Exception $e)
        {
            throw new AlertBackException($e->getMessage());
        }
    }
}
