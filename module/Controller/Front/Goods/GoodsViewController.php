<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Front\Goods;

use Component\Autorender\AutorenderUser;
use Request;
use Session;
use Framework\Debug\Exception\AlertRedirectException;
use Framework\Debug\Exception\AlertBackException;

class GoodsViewController extends \Bundle\Controller\Front\Goods\GoodsViewController
{
    public function pre()
    {

        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }

        $getData =  Request::get()->toArray();

        //영상관리에 등록되고 노출중인지 체크
        $db = \App::load('DB');
        $strSQL = " SELECT * FROM es_autorenderAdmin WHERE useYn='1' AND goodsCd = '{$getData['goodsNo']}' ";
        $result = $db->fetch($strSQL);

        if (!empty($result['no'])) {
            $this->setData('freeSian', 'Y');
        } else {
            $this->setData('freeSian', 'N');
        }

        if (!empty($getData['createNm'])) {

            //본인 제작건인지 체크
            $AutorenderUser = new AutorenderUser();
            $getCreateData = $AutorenderUser->getUserDetail($getData)['data'];
            $memInfo = Session::get('member');

            if ($getCreateData['odId']==$memInfo['memId']) {

                //결제 진행중인건 확인
                $db = \App::load('DB');
                $strSQL = " SELECT * FROM es_autorenderByOrderNo WHERE createNm = '{$getData['createNm']}' AND payment = '0' limit 1";
                $result = $db->fetch($strSQL);

                if (!empty($result['createNm'])) {

                    $strSQL = " SELECT * FROM es_order WHERE orderNo = '{$result['odNo']}' AND orderStatus!='f1' AND orderStatus!='f2' AND orderStatus!='f3' limit 1";
                    $result = $db->fetch($strSQL);

                    if (!empty($result['orderNo'])) {
                        throw new AlertRedirectException(__('이미 결제 진행중인 주문이 있습니다.'), 302, null, '../mypage/order_view.php?orderNo='.$result['orderNo'], 'top');
                    } else {
                        $query = "DELETE FROM es_autorenderByOrderNo WHERE createNm = '{$getData['createNm']}' AND payment = '0'";
                        $this->db->query($query);
                    }

                }


                //세션이 결제중 삭제될경우를 위해 DB에 입력
                $query = " INSERT INTO es_autorenderCreateNmSave SET createNm = '{$getData['createNm']}', memId='{$memInfo['memId']}', goodsNo='{$getData['goodsNo']}', regDt=now()";
                $this->db->query($query);

                $this->setData('myCreateNm', $getData['createNm']);

            } else {
                throw new AlertBackException('본인 제작건이 아닙니다.');
            }

        } else {

            $memInfo = Session::get('member');

            if (!empty($memInfo['memId']) && $memInfo['memId']!="") {
                $query = " DELETE FROM es_autorenderCreateNmSave WHERE memId='{$memInfo['memId']}'";
                $this->db->query($query);
            }

        }

    }
}