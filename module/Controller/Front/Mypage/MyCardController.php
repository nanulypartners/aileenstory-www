<?php

namespace Controller\Front\Mypage;

use Session;
use Framework\Debug\Exception\AlertRedirectException;

class MyCardController extends \Controller\Front\Controller
{
    public function index()
    {

        // 모듈호출
        $cards = \App::load('\\Component\\Mobilecard\\MobilecardUser');

        try {

            if(!Session::has('member')) {
                throw new AlertRedirectException(__('로그인하셔야 해당 서비스를 이용하실 수 있습니다.'), 302, null, '../member/login.php', 'top');
            }

            $getData = $cards->getCardListUser(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $this->setData('page', $page);
            $this->setData('data', $getData['data']);

        } catch (\Exception $e) {
            throw $e;
        }

    }
}
