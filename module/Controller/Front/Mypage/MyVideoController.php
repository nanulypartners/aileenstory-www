<?php

namespace Controller\Front\Mypage;

use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;
use Framework\Debug\Exception\AlertRedirectException;
use Component\Page\Page;

class MyVideoController extends \Controller\Front\Controller
{
    public function index()
    {
        // 모듈호출
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderUser');

        try {

            if(!Session::has('member')) {
                throw new AlertRedirectException(__('로그인하셔야 해당 서비스를 이용하실 수 있습니다.'), 302, null, '../member/login.php', 'top');
            }

            $getData = $Autorender->getListUser(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $auth_ = array();
            $auth_key = "!44fGB~assE6ML5868P#B$dds^@*"; //인증키
            $auth_time = time()+1200; //유효시간 20분

            $auth = hash('sha256', $auth_key.$auth_time);
            $auth_['auth_time'] = $auth_time;
            $auth_['auth'] = $auth;

            foreach ($getData['data'] as $key => $value) {

                /*파일서버쪽 전송*/
                $url = "https://app.aileenstory.com/!/uploads/days/".$value['createNm'];
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $getData['data'][$key]['expiration_period'] = $output;

            }

            $this->setData('auth_', $auth_);
            $this->setData('page', $page);
            $this->setData('data', $getData['data']);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
