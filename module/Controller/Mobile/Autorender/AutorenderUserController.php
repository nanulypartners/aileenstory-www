<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Mobile\Autorender;

use Request;
use Session;
use Framework\Debug\Exception\LayerNotReloadException;
use Framework\Debug\Exception\LayerException;
use Message;
use Framework\Debug\Exception\AlertCloseException;
use Component\Autorender\AutorenderUser;
use Component\Member\Member;

class AutorenderUserController extends \Controller\Mobile\Controller
{

    public function index()
    {

        // 모듈호출
        $autorenderUser = new AutorenderUser();

        try {

            $postData =  Request::post()->toArray();
            $result = array();

            $session = \App::getInstance('session');
            $memberService = new Member();
            $odId = $memberService->getMemberId($session->get(Member::SESSION_MEMBER_LOGIN . '.memNo'));

            if (empty($odId)) {
                $result['state'] = "notlogin";
            } else {

                if (!empty($postData['goodsNo'])) {

                    // 관리자에 자동렌더링 상품이 등록되어 있고 노출중인지 체크
                    $adminCheck = $autorenderUser->checkAdminRegister($postData);

                    if ($adminCheck['useYn']=='1') {
                        $result['no'] = $adminCheck['no'];

                        // POST 제작번호가 존재하는지 체크
                        if (!empty($postData['createNo'])) {

                            //제작번호가 DB에 존재하는지 체크
                            $userCheck = $autorenderUser->checkUserRegister($postData);

                            if ($userCheck['createNm']) {
                                $result['state'] = "ok";
                                $result['createNm'] = $userCheck['createNm'];
                            } else {
                                $result['state'] = "error";
                                $result['msg'] = "존재하지 않는 제작번호 입니다.";
                            }

                        } else {

                            /*
                                                        //동일제품 제작대기 3건 이상인지 확인
                                                        $check3rd = $autorenderUser->check3rd($postData);

                                                        if ($check3rd) {
                                                            $result['state'] = "error";
                                                            $result['msg'] = "해당 상품의 제작 수량 한도를 초과했습니다.\n마이페이지에서 제작대기 중인 상품의 제작을 완료해 주세요.";
                                                        } else {
                                                            //제작번호가 없으면 생성
                                                            $result['state'] = "ok";
                                                            $result['createNm'] = $autorenderUser->createNm($adminCheck);
                                                            if ($result['createNm']=='') {
                                                                $result['state'] = "error";
                                                                $result['msg'] = "제작 번호 생성에 실패했습니다. 다시 시도해 주세요.";
                                                            }
                                                        }
                            */

                            //제작번호가 없으면 생성
                            $result['state'] = "ok";
                            $result['createNm'] = $autorenderUser->createNm($adminCheck);
                            if ($result['createNm']=='') {
                                $result['state'] = "error";
                                $result['msg'] = "제작 번호 생성에 실패했습니다. 다시 시도해 주세요.";
                            }


                        }
                    } else {
                        $result['state'] = "error";
                        $result['msg'] = "영상 제작을 할 수 없는 상품 입니다.";
                    }

                } else {
                    $result['state'] = "error";
                    $result['msg'] = "잘못된 접근입니다.";
                }

            }

            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit;


        } catch (\Exception $e) {
            throw $e;
        }


    }
}
