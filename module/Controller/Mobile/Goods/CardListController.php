<?php

namespace Controller\Mobile\Goods;

use Component\Member\Member;
use Request;
use Session;
use Framework\Debug\Exception\AlertCloseException;

class CardListController extends \Controller\Mobile\Controller
{
    public function index()
    {
        $db = \App::load('DB');
        $strSQL = " SELECT * FROM es_mobileCardskin ";
        $result = $db->query_fetch($strSQL);

        $session = \App::getInstance('session');
        $memberService = new Member();
        $odId = $memberService->getMemberId($session->get(Member::SESSION_MEMBER_LOGIN . '.memNo'));

        $this->setData('login', 'login');
        if (empty($odId)) {
            $this->setData('login', 'notlogin');
        }

        $this->setData('cards', $result);
    }
}
