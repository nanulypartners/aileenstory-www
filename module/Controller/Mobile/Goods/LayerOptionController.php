<?php

/**
 * This is commercial software, only users who have purchased a valid license
 * and accept to the terms of the License Agreement can install and use this
 * program.
 *
 * Do not edit or add to this file if you wish to upgrade Godomall5 to newer
 * versions in the future.
 *
 * @copyright ⓒ 2016, NHN godo: Corp.
 * @link http://www.godo.co.kr
 */
namespace Controller\Mobile\Goods;

use Component\Autorender\AutorenderUser;
use Request;
use Session;
use Framework\Debug\Exception\AlertRedirectException;
use Framework\Debug\Exception\AlertBackException;

class LayerOptionController extends \Bundle\Controller\Mobile\Goods\LayerOptionController
{
    public function pre()
    {

        if (!is_object($this->db)) {
            $this->db = \App::load('DB');
        }

        $getData =  Request::post()->toArray();

        //영상관리에 등록되고 노출중인지 체크
        $db = \App::load('DB');
        $strSQL = " SELECT * FROM es_autorenderAdmin WHERE useYn='1' AND goodsCd = '{$getData['goodsNo']}' ";
        $result = $db->fetch($strSQL);

        if (!empty($result['no'])) {
            $this->setData('freeSian', 'Y');
        } else {
            $this->setData('freeSian', 'N');
        }

    }
}