<?php

namespace Controller\Mobile\Mypage;

use Request;
use Session;

class MypageCreateServiceController extends \Controller\Front\Controller
{
    public function index()
    {
        $Autorender = \App::load('\\Component\\Autorender\\AutorenderUser');


        try {

            if(!Session::has('member')) {
                throw new AlertRedirectException(__('로그인하셔야 해당 서비스를 이용하실 수 있습니다.'), 302, null, '../member/login.php', 'top');
            }

            $getData = $Autorender->getListOrder(\Request::get()->toArray());
            $page = \App::load('\\Component\\Page\\Page'); // 페이지 재설정

            $this->setData('page', $page);
            $this->setData('data', $getData['data']);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
