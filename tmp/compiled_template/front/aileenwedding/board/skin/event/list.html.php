<?php /* Template_ 2.2.7 2020/03/15 19:17:27 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/skin/event/list.html 000001764 */ ?>
<div class="section_layer">
    <ul class="webzine_layer">
<?php if($TPL_VAR["bdList"]["list"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
		<li>
			<div class="thumbnail">
				<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
					<img src="<?php echo gd_isset($TPL_V1["viewListImage"],'/data/skin/front/aileenwedding/board/skin/event/img/etc/noimg.png')?>" width="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgWidth"]?>" height="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgHeight"]?>" class="js_image_load" />
				</a>
			</div>
			<div class="info">
				<div class="title"><?php echo $TPL_V1["subject"]?></div>
				<div class="desc"><?php echo $TPL_V1["subSubject"]?></div>
				<div class="period"><?php echo $TPL_V1["eventStart"]?> ~ <?php echo $TPL_V1["eventEnd"]?></div>
				<div class="button_area">
					<ul class="one">
						<li>
							<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')" class="button orange border">자세히보기</a>
						</li>
					</ul>
				</div>
			</div>
		</li>
<?php }}?>
<?php }else{?>
		<li class="no_cont"><?php echo __('게시글이 존재하지 않습니다.')?></li>
<?php }?>
	</ul><!-- .gallery_layer -->
	<div class="pagination">
		<?php echo $TPL_VAR["bdList"]["pagination"]?>

	</div>
</div><!-- .section_layer -->