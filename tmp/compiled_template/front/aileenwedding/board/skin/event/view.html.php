<?php /* Template_ 2.2.7 2020/04/13 03:49:40 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/skin/event/view.html 000018698 */ ?>
<div class="section_layer">
    <div class="board_zone_view">
        <div class="board_view_tit">
            <h3>
<?php if($TPL_VAR["bdView"]["cfg"]["bdCategoryFl"]=='y'&&$TPL_VAR["bdView"]["data"]["category"]){?>
                <span class="text_orange">[<?php echo $TPL_VAR["bdView"]["data"]["category"]?>]</span>
<?php }?>
                <?php echo $TPL_VAR["bdView"]["data"]["subject"]?>

            </h3>
        </div>
        <div class="board_view_info">
            <span class="view_info_idip">
                <strong><?php echo $TPL_VAR["bdView"]["data"]["writer"]?></strong>
<?php if($TPL_VAR["bdView"]["cfg"]["bdIpFl"]=='y'){?><em>(IP: <?php echo $TPL_VAR["bdView"]["data"]["writerIp"]?>)</em><?php }?>
            </span>
            <span class="separate">|</span>
            <span class="view_info_day">
                <em><?php echo $TPL_VAR["bdView"]["data"]["regDate"]?></em>
            </span>
            <span class="separate">|</span>
            <span class="view_info_hits">
                <strong><?php echo __('조회수')?></strong> <?php echo $TPL_VAR["bdView"]["data"]["hit"]?>

            </span>
        </div>
        <!-- //board_view_info -->
    </div><!-- .board_zone_view -->
<?php if($TPL_VAR["bdView"]["data"]["isFile"]=='y'){?>
    <div class="board_view_attach">
        <strong>
            <img src="<?php echo $TPL_VAR["bdView"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>" style="margin-right:5px;"/>
            <?php echo __('첨부파일')?>:
        </strong>
        <span class="attach_list">
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["uploadedFile"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <a href="./download.php?bdId=<?php echo $TPL_VAR["bdView"]["cfg"]["bdId"]?>&sno=<?php echo $TPL_VAR["req"]["sno"]?>&fid=<?php echo $TPL_V1["fid"]?>"><?php echo $TPL_V1["name"]?></a>
<?php }}?>
        </span>
    </div>
    <!-- //board_view_attach -->
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdLinkFl"]=='y'&&$TPL_VAR["bdView"]["data"]["urlLink"]){?>
    <div class="board_view_attach">
        <strong>링크: </strong>
        <span><a href="<?php echo gd_isset($TPL_VAR["bdView"]["data"]["urlLink"])?>" target="_blank"><?php echo $TPL_VAR["bdView"]["data"]["urlLink"]?></a></span>
    </div>
    <!-- //board_view_link -->
<?php }?>
    <div class="board_view_content">
        <div class="view_goods_select">
<?php if($TPL_VAR["bdView"]["data"]["isViewGoodsInfo"]=='y'){?>
            <div class="view_goods_select_item">
                <span class="view_select_item_img">
                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdView"]["data"]["goodsNo"]?>" target="_blank">
                        <img src="<?php echo $TPL_VAR["bdView"]["data"]["goodsData"]["goodsImageSrc"]?>" >
                    </a>
                </span>
                <span class="view_select_item_info">
                    <em><?php echo $TPL_VAR["bdView"]["data"]["goodsData"]["goodsNm"]?></em>
                    <strong>
                        <span><?php echo __('판매금액')?>:</span>
<?php if($TPL_VAR["bdView"]["data"]["goodsData"]["goodsPriceString"]){?>
                        <?php echo $TPL_VAR["bdView"]["data"]["goodsData"]["goodsPriceString"]?>

<?php }else{?>
                        <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["bdView"]["data"]["goodsData"]["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php }?>
                    </strong>
                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdView"]["data"]["goodsNo"]?>" target="_blank" class="link"><i class="xi-angle-right"></i></a>
                </span>
            </div>
            <!-- //view_goods_select_item -->
<?php }?>
<?php if($TPL_VAR["bdView"]["data"]["isViewOrderInfo"]=='y'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["extraData"]["arrOrderGoodsData"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <div class="view_goods_select_item">
                <span class="view_select_item_img">
                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>" target="_blank">
                        <img src="<?php echo $TPL_V1["goodsImageSrc"]?>" >
                    </a>
                </span>
                <span class="view_select_item_info">
                    <em><?php echo $TPL_V1["goodsNm"]?></em>
                    <strong>
                        <span><?php echo __('판매금액')?>:</span>
                        <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["totalGoodsPrice"])?><?php echo gd_global_currency_string()?>

                        <span class="text_type_cont">(<?php echo $TPL_V1["optionName"]?>)</span>
                    </strong>
                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>" target="_blank" class="link"><i class="xi-angle-right"></i></a>
                </span>
            </div>
            <!-- //view_goods_select_item -->
<?php }}?>
<?php }?>
        </div>
        <!-- //view_goods_select -->
		<div class="board_view_qa">
			<div class="view_question_box">
				<div class="seem_cont">
					<?php echo $TPL_VAR["bdView"]["data"]["workedContents"]?>

				</div>
				<!-- //seem_cont -->
			</div>
<?php if($TPL_VAR["bdView"]["cfg"]["bdReplyStatusFl"]=='y'&&$TPL_VAR["bdView"]["data"]["replyStatus"]> 1&&$TPL_VAR["bdView"]["data"]["workedAnswerContents"]){?>
			<div class="view_answer_box">
				<div class="seem_cont">
                    <div class="view_answer_info">
                        <strong><?php echo $TPL_VAR["bdView"]["data"]["answerSubject"]?></strong>
                        <span class="view_info_idip">
                            <strong><?php echo $TPL_VAR["bdView"]["data"]["answerManagerNm"]?></strong>
                        </span>
                        <span class="view_info_day">
                            <em><?php echo $TPL_VAR["bdView"]["data"]["answerModDt"]?></em>
                        </span>
                    </div>
					<div><?php echo $TPL_VAR["bdView"]["data"]["workedAnswerContents"]?></div>
				</div>
				<!-- //seem_cont -->
			</div>
			<!-- //view_answer_box -->
<?php }?>
		</div>
		<!-- //board_view_qa -->
    </div>
    <!-- //board_view_content -->
    <div class="board_button_area button_area">
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["delete"]){case 'y':case 'c':?>
        <button type="button" class="btn_board_del button gray" onclick="gd_btn_delete('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>,'<?php echo $TPL_VAR["bdView"]["data"]["auth"]["modify"]?>')"><strong><?php echo __('삭제')?></strong></button>
<?php }?>
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["modify"]){case 'y':case 'c':?>
        <button type="button" class="btn_board_edit button gray" onclick="gd_btn_modify_write('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>,'<?php echo $TPL_VAR["bdView"]["data"]["auth"]["modify"]?>')"><strong><?php echo __('수정')?></strong></button>
<?php }?>
<?php if($TPL_VAR["bdView"]["data"]["auth"]["reply"]=='y'){?>
        <button type="button" class="btn_board_reply button orange" onclick="gd_btn_reply_write('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>)"><strong><?php echo __('답글')?></strong></button>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["auth"]["list"]=='y'){?>
        <button type="button" class="btn_board_list button black" onclick="gd_btn_list('<?php echo $TPL_VAR["req"]["bdId"]?>')"><strong><?php echo __('목록')?></strong></button>
<?php }?>
    </div>
    <div class="board_view_comment">
        <div class="view_comment js_comment_area" data-bdId="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_VAR["req"]["sno"]?>">
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'||$TPL_VAR["bdView"]["cfg"]["bdRecommendFl"]=='y'){?>
            <div class="view_comment_top">
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
                <span class="comment_num"><strong class="text_orange"><?php echo number_format($TPL_VAR["bdView"]["data"]["memoCnt"])?></strong> 개의 댓글이 있습니다.</span>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdGoodsPtFl"]=='y'){?>
                <span class="rating_star_box">
                    <em><?php echo __('별점')?></em>
                    <span class="rating_star">
                        <span style="width:<?php echo ($TPL_VAR["bdView"]["data"]["goodsPt"])* 20?>%;">&nbsp;</span>
                    </span>
                </span>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdRecommendFl"]=='y'){?>
                <span id="recommendCount" class="comment_best_num">
                    <?php echo __('추천')?>: <strong class="text_orange"><?php echo $TPL_VAR["bdView"]["data"]["recommend"]?></strong>
                    <a href="javascript:gd_recommend('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>)" class="btn_comment_best"><em><?php echo __('추천하기')?></em></a>
                </span>
<?php }?>
            </div>
            <!-- //view_comment_top -->
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
<?php if($TPL_VAR["bdView"]["cfg"]["auth"]["memo"]=='y'){?>
            <div class="board_comment_box js_form_write">
                <div class="board_comment_write">
<?php if(gd_is_login()){?>
                    <div class="form_element">
                    <?php echo $TPL_VAR["secretReplyCheck"]["replyWrite"]?>

                    </div>
                    <div class="comment_textarea">
                        <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>"></textarea>
                        <span class="btn_comment_box button_area"><button type="button" class="btn_comment_ok js_comment_btn_write button orange"><em><?php echo __('확인')?></em></button></span>
                    </div>
<?php }else{?>
                    <div class="form_element">
                    <?php echo $TPL_VAR["secretReplyCheck"]["replyWrite"]?>

                    </div>
                    <div class="comment_textarea">
                        <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>"></textarea>
                        <span class="btn_comment_box button_area"><button type="button" class="btn_comment_ok js_comment_btn_write button orange"><em><?php echo __('확인')?></em></button></span>
                    </div>
                    <div class="comment_input">
                        <input type="text" name="writerNm" placeholder="<?php echo __('이름')?>" autocomplete="off" class="text">
                        <input type="password" name="password" placeholder="<?php echo __('비밀번호')?>" autocomplete="off" class="text">
                        <input type="checkbox" id="infoCollectionAgreeWrite" name="checkCollectAgree" class="checkbox">
                        <label for="infoCollectionAgreeWrite">(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></label>
                        <a href="javascript:void(0)" onclick="gd_redirect_collection_agree()" class="agree_total_btn text_orange"><?php echo __('전체보기')?></a>
                    </div>
<?php }?>
                </div>
                <!-- //board_comment_write -->
            </div>
            <!-- //board_comment_box -->
<?php }else{?>
            <div class="board_comment_box js_form_write">
                <div class="board_comment_write">
<?php if(gd_is_login()){?>
                    <?php echo __('댓글 권한이 없습니다.')?>

<?php }else{?>
                    <?php echo __('로그인을 하셔야 댓글을 등록하실 수 있습니다.')?>

<?php }?>
                </div>
            </div>
<?php }?>
            <div class="view_comment_list">
                <ul>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["memoList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li data-memosno="<?php echo $TPL_V1["sno"]?>" data-memoauth="<?php echo $TPL_V1["auth"]?>" class="js_data_comment_row <?php if($TPL_V1["groupThread"]){?>comment_reply<?php }?>">
                        <div class="comment_name_area">
                            <div class="comment_name"><?php echo $TPL_V1["writer"]?></div>
                            <div class="comment_cont">
<?php if($TPL_VAR["bdView"]["data"]["allReplyShow"]=='y'){?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }else{?>
<?php if($TPL_V1["isSecretReply"]=='y'){?>
<?php if($TPL_V1["isShowSecretReply"]=='y'){?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }else{?>
                                <input type="hidden" name="secretReply" value="y">
                                <em class="js_comment_btn_secret"><img src="<?php echo $TPL_VAR["bdListCfg"]["iconImage"]["secret"]["url"]?>" align=absmiddle><?php echo $TPL_VAR["secretReplyCheck"]["secretReplyTitle"]?></em>
<?php }?>
<?php }else{?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }?>
<?php }?>
                                <div class="board_day_time"><?php echo $TPL_V1["regDt"]?></div>
                            </div>
                        </div>
                        <div class="comment_btn_area">
<?php if(!$TPL_V1["groupThread"]){?>
                            <span class="btn_gray_list"><button type="button" class="btn_gray_small js_comment_btn_reply"><span><?php echo __('답글')?></span></button></span>
<?php }?>
<?php switch($TPL_V1["auth"]){case 'y':case 'c':?>
                            <span class="btn_gray_list"><button type="button" class="btn_gray_small js_comment_btn_modify"><span><?php echo __('수정')?></span></button></span>
                            <span class="btn_gray_list"><button type="button" class="btn_gray_small js_comment_btn_delete"><span><?php echo __('삭제')?></span></button></span>
<?php }?>
                        </div>
                        <div class="board_comment_box js_action_form" style="display:none">
                            <div class="board_comment_write inner">
<?php if(gd_is_login()){?>
                                <div class="form_element">
                                <?php echo $TPL_VAR["secretReplyCheck"]["replyModify"]?>

                                </div>
                                <div class="comment_textarea">
                                    <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>"></textarea>
                                    <span class="btn_comment_box button_area"><button type="button" class="btn_comment_ok js_comment_btn_action button orange"><em><?php echo __('확인')?></em></button></span>
                                </div>
<?php }else{?>
                                <div class="form_element">
                                <?php echo $TPL_VAR["secretReplyCheck"]["replyModify"]?>

                                </div>
                                <div class="comment_textarea">
                                    <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>"></textarea>
                                    <span class="btn_comment_box button_area"><button type="button" class="btn_comment_ok js_comment_btn_action button orange"><em><?php echo __('확인')?></em></button></span>
                                </div>
                                <div class="comment_input">
                                    <input type="text" name="writerNm" placeholder="<?php echo __('이름')?>" autocomplete="off" class="text">
                                    <input type="password" name="password" placeholder="<?php echo __('비밀번호')?>" autocomplete="off" class="text">
                                    <input type="checkbox" id="infoCollectionAgreeAction" name="checkCollectAgree" class="checkbox">
                                    <label for="infoCollectionAgreeAction">(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></label>
                                    <a href="javascript:void(0)" onclick="gd_redirect_collection_agree()" class="agree_total_btn text_orange"><?php echo __('전체보기')?></a>
                                </div>
<?php }?>
                            </div>
                            <!-- //board_comment_write -->
                        </div>
                        <!-- //board_comment_box -->
                    </li>
<?php }}?>
                </ul>
            </div>
            <!-- //view_comment_list -->
<?php }?>
        </div><!-- .view_comment -->
    </div>
<!-- //board_view_comment -->
</div><!-- .section_layer -->

<!-- 레이어 호출시 딤처리 -->
<div id="layerDim" class="dn">&nbsp;</div>
<!-- //레이어 호출시 딤처리 -->
<!-- 비밀글 클릭시 인증 레이어 -->
<div id="lyPassword" class="dn layer_wrap password_layer" style="height: 226px">
    <div class="layer_wrap_cont">
        <div class="ly_tit">
            <h4><?php echo __('비밀번호 인증')?></h4>
        </div>
        <div class="ly_cont">
            <div class="scroll_box">
                <p><?php echo __('글 작성시 설정한 비밀번호를 입력해 주세요.')?></p>
                <input type="password" name="writerPw" class="text">
            </div>
            <!-- // -->
            <div class="btn_center_box button_area">
                <button type="button" class="btn_ly_password js_submit button black"><strong><?php echo __('확인')?></strong></button>
            </div>
        </div>
        <!-- //ly_cont -->
        <a href="#close" class="ly_close layer_close"><i class="xi-close"></i></a>
    </div>
    <!-- //layer_wrap_cont -->
</div>
<!-- //layer_wrap -->
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_view.js" charset="utf-8"></script>