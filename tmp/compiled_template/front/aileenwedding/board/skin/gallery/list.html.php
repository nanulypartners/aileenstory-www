<?php /* Template_ 2.2.7 2020/03/15 19:17:27 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/skin/gallery/list.html 000008382 */ ?>
<div class="section_layer">
<?php if(!$TPL_VAR["inList"]&&$TPL_VAR["bdList"]["cfg"]["auth"]["write"]=='y'){?>
	<div class="title_area">
		<button type="button" class="btn_write button" onclick="javascript:gd_btn_write('<?php echo $TPL_VAR["req"]["bdId"]?>')"><strong><?php echo __('글쓰기')?></strong></button>
	</div><!-- .title_area -->
<?php }?>
	<table class="board_list_table" <?php echo $TPL_VAR["bdList"]["cfg"]["bdWidthStyle"]?>>
		<colgroup>
			<col style="width:60px">
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
			<col style="width:80px">
<?php }?>
			<col>
			<col style="width:100px">
			<col style="width:80px">
			<col style="width:60px">
		</colgroup>
		<thead>
		<tr>
			<th><?php echo __('번호')?></th>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
			<th><?php echo __('말머리')?></th>
<?php }?>
			<th><?php echo __('제목')?></th>
			<th><?php echo __('날짜')?></th>
			<th><?php echo __('작성자')?></th>
			<th><?php echo __('조회')?></th>
		</tr>
		</thead>
		<tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["noticeList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
		<tr class="notice_point">
			<td> <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["notice"]["url"]?>" alt="<?php echo __('공지')?>"/> </td>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
			<td>
<?php if($TPL_V1["category"]){?>[<?php echo $TPL_V1["category"]?>]<?php }?>
			</td>
<?php }?>
			<td class="board_tit">
				<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
					<strong><?php echo $TPL_V1["subject"]?></strong>
				</a>
			</td>
			<td> <?php echo $TPL_V1["regDate"]?> </td>
			<td> <?php echo $TPL_V1["writer"]?> </td>
			<td> <?php echo $TPL_V1["hit"]?> </td>
		</tr>
<?php }}?>
		</tbody>
	</table>
	<div class="board_zone_list">
<?php if($TPL_VAR["bdList"]["list"]){?>
		<div class="board_list_gallery">
			<ul>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
				<li style="width:<?php echo  100/$TPL_VAR["bdList"]["cfg"]["bdListColsCount"]?>%">
					<div class="gallery_cont">
						<div class="board_img">
							<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
								<img src="<?php echo $TPL_V1["viewListImage"]?>" width="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgWidth"]?>" height="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgHeight"]?>" class="js_image_load"/>
							</a>
						</div>
						<div class="gallery_info_cont">
<?php if($TPL_VAR["bdList"]["cfg"]["bdGoodsPtFl"]=='y'){?>
							<div class="rating_star_box">
								<span class="rating_star">
									<span style="width:<?php echo $TPL_V1["goodsPtPer"]?>%;"><?php echo __('별 다섯개중 다섯개')?></span>
								</span>
							</div>
<?php }?>

							<div class="board_tit">
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?><em>[<?php echo $TPL_V1["category"]?>]</em><?php }?>
								<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
<?php if($TPL_V1["isSecret"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["secret"]["url"]?>" align=absmiddle>
<?php }?>
									<strong><?php echo $TPL_V1["subject"]?>

<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
											(<?php echo $TPL_V1["memoCnt"]?>)
<?php }?>
									</strong>
								</a>
<?php if($TPL_V1["isFile"]=='y'){?>
								<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
								<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
								<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>"/>
<?php }?>
<?php if($TPL_V1["isHot"]=='y'){?>
								<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["hot"]["url"]?>" alt="<?php echo __('인기글')?>"/>
<?php }?>
							</div>

							<div class="board_name_day">
								<span class="board_name"><?php echo $TPL_V1["writer"]?></span>
								<span class="board_day"><span><?php echo $TPL_V1["regDate"]?></span></span>
							</div>
							<div class="board_likeit_hits">
<?php if($TPL_VAR["bdList"]["cfg"]["bdRecommendFl"]=='y'){?>
								<span class="board_likeit"><?php echo __('추천')?>  <?php echo $TPL_V1["recommend"]?></span>
<?php }?>
								<span class="board_hits"><?php echo __('조회')?> <?php echo $TPL_V1["hit"]?></span>
							</div>
						</div>
						<!-- //gallery_info_cont -->
					</div>
					<!-- //gallery_cont -->
				</li>
<?php }}?>
			</ul>
		</div>
<?php }else{?>
		<div class="board_list_gallery"><?php echo __('게시글이 존재하지 않습니다.')?></div>
<?php }?>
		<!-- //board_list_gallery -->

		<?php echo $TPL_VAR["bdList"]["pagination"]?>

		<!-- //pagination -->

		<div class="board_search_box">
			<form name="frmList" id="frmList" action="list.php" method="get">
				<input type="hidden" name="bdId" value="<?php echo $TPL_VAR["bdList"]["cfg"]["bdId"]?>">
				<input type="hidden" name="memNo" value="<?php echo $TPL_VAR["req"]["memNo"]?>"/>
				<input type="hidden" name="noheader" value="<?php echo $TPL_VAR["req"]["noheader"]?>"/>

<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
				<?php echo gd_select_box('category','category',$TPL_VAR["bdList"]["cfg"]["arrCategory"],null,gd_isset($TPL_VAR["req"]["category"]),__('선택'),'style="width: 80px;"','chosen-select')?>

<?php }?>
				<select class="chosen-select" name="searchField">
					<option value="subject"
<?php if($TPL_VAR["req"]["searchField"]=='subject'){?>selected<?php }?> ><?php echo __('제목')?></option>
					<option value="contents"
<?php if($TPL_VAR["req"]["searchField"]=='contents'){?>selected<?php }?> ><?php echo __('내용')?></option>
					<option value="writerNm"
<?php if($TPL_VAR["req"]["searchField"]=='writerNm'){?>selected<?php }?> ><?php echo __('작성자')?></option>
				</select>

				<input type="text" class="text" name="searchWord" value="<?php echo $TPL_VAR["req"]["searchWord"]?>">
				<button class="btn_board_search"><em><?php echo __('검색')?></em></button>
			</form>
		</div>
		<!-- //board_search_box -->

	</div>
	<!-- //board_zone_list -->
</div>
<!-- //board_zone_sec -->

<form id="frmWritePassword">
	<div id="lyPassword" class="dn layer_wrap password_layer" style="height: 226px">
		<div class="layer_wrap_cont">
			<div class="ly_tit">
				<h4><?php echo __('비밀번호 인증')?></h4>
			</div>
			<div class="ly_cont">
				<div class="scroll_box">
					<p><?php echo __('비밀번호를 입력해 주세요.')?></p>
					<input type="password" name="writerPw" class="text">
				</div>
				<!-- // -->
				<div class="btn_center_box">
					<button type="button" class="btn_ly_password js_submit button small w100 pink"><strong><?php echo __('확인')?></strong></button>
				</div>
			</div>
			<!-- //ly_cont -->
			<a href="#close" class="ly_close layer_close"><i class="xi-close"></i></a>
		</div>
		<!-- //layer_wrap_cont -->
	</div>
	<!-- //layer_wrap -->
</form>

<div id="layerDim" class="dn">&nbsp;</div>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_list.js" charset="utf-8"></script>
<script>
	$(document).ready(function () {
		$('img.js_image_load').error(function () {
					$(this).css('background', 'url("/data/skin/front/aileenwedding/board/skin/gallery/img/etc/noimg.png") no-repeat center center');
					$(this).attr('src', '/data/skin/front/aileenwedding/img/etc/blank.gif');
				})
				.each(function () {
					$(this).attr("src", $(this).attr("src"));
				})
	});
</script>