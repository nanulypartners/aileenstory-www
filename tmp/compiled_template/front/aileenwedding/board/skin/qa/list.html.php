<?php /* Template_ 2.2.7 2020/05/24 15:40:28 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/skin/qa/list.html 000012198 */ ?>
<div class="section_layer">
<?php if(!$TPL_VAR["inList"]){?>
		<div class="title_area">
<?php if(!$TPL_VAR["inList"]&&$TPL_VAR["bdList"]["cfg"]["auth"]["write"]=='y'){?>
			<button type="button" class="btn_write button" onclick="gd_btn_write('<?php echo $TPL_VAR["req"]["bdId"]?>')"><?php echo __('문의하기')?></button>
<?php }?>
<?php if($TPL_VAR["req"]["noheader"]!='y'){?>
			<div class="cs_info"><img src="/data/skin/front/aileenwedding/img/common/cs_img.png" alt=""></div>
<?php }?>
		</div><!-- .title_area -->
		<form name="frmSearch" method="get" action="list.php" style="display:none;">
			<input type="hidden" name="bdId" value="<?php echo $TPL_VAR["req"]["bdId"]?>"/>
			<input type="hidden" name="memNo" value="<?php echo $TPL_VAR["req"]["memNo"]?>"/>
			<input type="hidden" name="noheader" value="<?php echo $TPL_VAR["req"]["noheader"]?>"/>
			<div class="search_area">
				<div class="field">
					<div class="key">조회기간</div>
					<div class="value">
						<div class="date_check_list" data-target-name="rangDate[]">
							<button type="button" data-value="0"><?php echo __('오늘')?></button>
							<button type="button" data-value="7">7<?php echo __('일')?></button>
							<button type="button" data-value="15">15<?php echo __('일')?></button>
							<button type="button" data-value="30">1<?php echo __('개월')?></button>
							<button type="button" data-value="90">3<?php echo __('개월')?></button>
							<button type="button" data-value="365">1<?php echo __('년')?></button>
						</div>
						<div class="date_check_calendar">
							<input type="text" id="picker2" name="rangDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["req"]["rangDate"][ 0]?>"> ~ <input type="text" name="rangDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["req"]["rangDate"][ 1]?>">
						</div>
						<div class="button_area">
							<button type="submit" class="btn_date_check button orange"><em><?php echo __('조회')?></em></button>
						</div>
					</div><!-- .value -->
				</div><!-- .field -->
			</div><!-- .search_area -->
		</form>
<?php }?>
	<table class="board_list_table table c_table" <?php echo $TPL_VAR["bdList"]["cfg"]["bdWidthStyle"]?>>
		<colgroup>
			<col style="width:10%"> <!-- 문의날짜 -->
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?>
				<col style="width:10%">
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
				<col style="width:15%"> <!-- 카테고리 -->
<?php }?>
			<col><!-- 제목 -->
<?php if(!$TPL_VAR["isMemNo"]){?>
				<col style="width:10%">  <!-- 작성자 -->
<?php }?>
			<col style="width:10%"> <!-- 문의상태 -->
		</colgroup>
		<thead>
			<tr>
				<th><?php echo __('문의날짜')?></th>
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?>
					<th><?php echo __('이미지')?></th>
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
					<th><?php echo __('카테고리')?></th>
<?php }?>
				<th><?php echo __('제목')?></th>
<?php if(!$TPL_VAR["isMemNo"]){?>
					<th><?php echo __('작성자')?></th>
<?php }?>
				<th><?php echo __('문의상태')?></th>
			</tr>
		</thead>
		<tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["noticeList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
			<tr <?php echo $TPL_VAR["bdList"]["cfg"]["bdHeightStyle"]?>>
				<td><img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["notice"]["url"]?>" alt="<?php echo __('공지')?>"/></td>
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?>
					<td>
<?php if($TPL_VAR["bdList"]["cfg"]["bdListNoticeImageDisplayPc"]=='y'){?>
							<div class="board_img">
								<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
									<img src="<?php echo gd_isset($TPL_V1["viewListImage"],'/data/skin/front/aileenwedding/board/skin/qa/img/etc/noimg.png')?>" width="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgWidth"]?>" class="js_image_load"/>
								</a>
							</div>
<?php }?>
					</td>
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
					<td>
<?php if($TPL_V1["category"]){?>[<?php echo $TPL_V1["category"]?>]<?php }?>
					</td>
<?php }?>
				<td class="board_tit title">
					<?php echo $TPL_V1["gapReply"]?>

					<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> ,'<?php echo $TPL_V1["auth"]["view"]?>')" >
						<strong><?php echo $TPL_V1["subject"]?></strong>
					</a>
<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
						<span>(<?php echo $TPL_V1["memoCnt"]?>)</span>
<?php }?>
<?php if($TPL_V1["isFile"]=='y'){?>
						<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
						<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
						<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isHot"]=='y'){?>
						<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["hot"]["url"]?>" alt="<?php echo __('인기글')?>" style="margin-left:5px;"/>
<?php }?>
				</td>
<?php if(!$TPL_VAR["isMemNo"]){?>
					<td> <?php echo $TPL_V1["writer"]?> </td>
<?php }?>
				<td>-</td>
			</tr>
<?php }}?>
<?php if($TPL_VAR["bdList"]["list"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
					<tr <?php echo $TPL_VAR["bdList"]["cfg"]["bdHeightStyle"]?>>
						<td> <?php echo $TPL_V1["regDate"]?> </td>
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?>
							<td>
								<div class="board_img">
									<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> , '<?php echo $TPL_V1["auth"]["view"]?>')">
										<img src="<?php echo gd_isset($TPL_V1["viewListImage"],'/data/skin/front/aileenwedding/board/skin/qa/img/etc/noimg.png')?>" width="<?php echo $TPL_VAR["bdList"]["cfg"]["bdListImgWidth"]?>" class="js_image_load"/>
									</a>
								</div>
							</td>
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'){?>
							<td>
<?php if($TPL_V1["category"]){?>[<?php echo $TPL_V1["category"]?>]<?php }?>
							</td>
<?php }?>
						<td class="board_tit title">
							<?php echo $TPL_V1["gapReply"]?>

<?php if($TPL_V1["groupThread"]){?><img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["re"]["url"]?>"alt="<?php echo __('답변')?>" style="padding:0 5px" /><?php }?>
							<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> ,'<?php echo $TPL_V1["auth"]["view"]?>')" >
<?php if($TPL_V1["isSecret"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["secret"]["url"]?>" align=absmiddle style="margin-right:5px;">
<?php }?>
								<strong><?php echo $TPL_V1["subject"]?></strong>
<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
									<span>(<?php echo $TPL_V1["memoCnt"]?>)</span>
<?php }?>
<?php if($TPL_V1["isFile"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>" style="margin-left:5px;"/>
<?php }?>
<?php if($TPL_V1["isHot"]=='y'){?>
									<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["hot"]["url"]?>" alt="<?php echo __('인기글')?>" style="margin-left:5px;"/>
<?php }?>
							</a>
						</td>
<?php if(!$TPL_VAR["isMemNo"]){?>
							<td> <?php echo $TPL_V1["writer"]?> </td>
<?php }?>
						<td>
<?php if($TPL_V1["replyStatusText"]=='접수'){?>
								<span class="text_gray">
<?php }else{?>
								<span class="text_orange">
<?php }?>
								<?php echo $TPL_V1["replyStatusText"]?>

							</span>
						</td>
					</tr>
<?php }}?>
<?php }else{?>
				<tr>
					<td colspan="5"><?php echo __('게시글이 존재하지않습니다.')?></td>
				</tr>
<?php }?>
		</tbody>
	</table>
	<div class="pagination">
		<?php echo $TPL_VAR["bdList"]["pagination"]?>

	</div>
</div><!-- .section_layer -->
<form id="frmWritePassword">
	<div id="lyPassword" class="dn layer_wrap password_layer" style="height: 226px">
		<div class="layer_wrap_cont">
			<div class="ly_tit">
				<h4><?php echo __('비밀번호 인증')?></h4>
			</div>
			<div class="ly_cont">
				<div class="scroll_box">
					<p><?php echo __('비밀번호를 입력해 주세요.')?></p>
					<input type="password" name="writerPw" class="text">
				</div>
				<!-- // -->
				<div class="button_area">
					<button type="button" class="btn_ly_password js_submit button small w100 pink"><?php echo __('확인')?></button>
				</div><!-- .button_area -->
			</div>
			<!-- //ly_cont -->
			<a href="#close" class="ly_close layer_close"><i class="xi-close"></i></a>
		</div>
		<!-- //layer_wrap_cont -->
	</div>
	<!-- //layer_wrap -->
</form>
<div id="layerDim" class="dn">&nbsp;</div>
<script>
	$(document).ready(function () {
		$('img.js_image_load').error(function () {
			$(this).css('background', 'url("/data/skin/front/aileenwedding/board/skin/qa/img/etc/noimg.png") no-repeat center center');
			$(this).attr('src', '/data/skin/front/aileenwedding/img/etc/blank.gif');
		})
		.each(function () {
			$(this).attr("src", $(this).attr("src"));
		})
	});
</script>
<script type="text/javascript">
	$(function(){
		// 인풋박스 선택 이벤트
		if ($('.js_datepicker').length) {
			$('.js_datepicker').datetimepicker({
				locale: '<?php echo $TPL_VAR["gGlobal"]["locale"]?>',
				format: 'YYYY-MM-DD',
				dayViewHeaderFormat: 'YYYY MM',
				viewMode: 'days',
				ignoreReadonly: true,
				debug: false,
				keepOpen: false
			});
			//$('.check-cal img').click(function(e){
			//	$(this).prev('.js-datepicker').data('DateTimePicker').show();
			//});
		}

		// 기간버튼 선택 이벤트
		if ($('.date_check_list').length) {
			$('.date_check_list button').click(function (e) {
				$startDate = $endDate = '';
				$period = $(this).data('value');
				$elements = $('input[name="' + $(this).closest('.date_check_list').data('target-name') + '"]');
				$format = $($elements[0]).data('DateTimePicker').format();
				if ($period >= 0) {
					$startDate = moment().hours(0).minutes(0).seconds(0).subtract($period, 'days').format($format);
					$endDate = moment().hours(0).minutes(0).seconds(0).format($format);
				}
				$($elements[0]).val($startDate);
				$($elements[1]).val($endDate);
				$('.date_check_list button').removeClass('on');
				$(this).addClass('on');
			});

			// 선택된 버튼에 따른 값 초기화
			$elements = $('input[name*=\'' + $('.date_check_list').data('target-name') + '\']');
			if ($elements.length && $elements.val() != '') {
				$interval = moment($($elements[1]).val()).diff(moment($($elements[0]).val()), 'days');
				$('.date_check_list').find('button[data-value="' + $interval + '"]').trigger('click');
			} else {
				$('.date_check_list').find('button[data-value="-1"]').trigger('click');
			}
		}
	});
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_list.js" charset="utf-8"></script>