<?php /* Template_ 2.2.7 2020/03/15 19:17:28 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/skin/qa/write.html 000016523 */ ?>
<div class="section_layer">
	<div class="board_zone_write">
		<div class="title_area">
			글쓰기
		</div>
		<form name="frmWrite" id="frmWrite" action="./board_ps.php" method="post" enctype="multipart/form-data" class="frmWrite">
			<input type="hidden" name="bdId" value="<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdId"]?>">
			<input type="hidden" name="sno" value="<?php echo gd_isset($TPL_VAR["req"]["sno"])?>">
			<input type="hidden" name="mode" value="<?php echo gd_isset($TPL_VAR["req"]["mode"])?>">
			<!--<input type="hidden" name="chkSpamKey" id="chkSpamKey">-->
			<input type="hidden" name="returnUrl" value="<?php echo $TPL_VAR["queryString"]?>">
<?php if($TPL_VAR["oldPassword"]){?><input type="hidden" name="oldPassword" value="<?php echo $TPL_VAR["oldPassword"]?>"><?php }?>
			<table class="table">
				<colgroup>
					<col style="width:15%" />
					<col style="width:85%" />
				</colgroup>
				<tbody>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdCategoryFl"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('말머리')?></th>
					<td>
						<div class="category_select">
							<?php echo gd_select_box('category','category',$TPL_VAR["bdWrite"]["cfg"]["arrCategory"],null,gd_isset($TPL_VAR["bdWrite"]["data"]["category"]),null,'style="width: 127px;"','chosen-select')?>

						</div>
					</td>
				</tr>
<?php }?>
				<tr>
					<th scope="row"><?php echo __('작성자')?></th>
					<td>
<?php if($TPL_VAR["req"]["mode"]=='modify'||gd_is_login()){?>
						<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>

<?php }else{?>
						<input type="text" class="text" name="writerNm" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>"/>
<?php }?>
					</td>
				</tr>
<?php if(!gd_isset($TPL_VAR["bdWrite"]["data"]["memNo"])&&!$TPL_VAR["oldPassword"]){?>
				<tr>
					<th scope="row"><?php echo __('비밀번호')?></th>
					<td><input type="password" class="text" name="writerPw" style="width:300px;"></td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["data"]["canWriteGoodsSelect"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('상품 선택')?></th>
					<td>
						<div class="board_goods_select">
							<a href="#addGoodsLayer" title="<?php echo __('찾아보기')?>" class="btn_goods_select btn_open_layer btn"> <?php echo __('상품 선택')?> </a>
<?php if(!$TPL_VAR["bdWrite"]["data"]["goodsNo"]){?>
							<div class="js_selected_info_text"><?php echo __('선택된 상품이 없습니다.')?></div>
<?php }?>
							<div id="selectGoodsBox">
<?php if($TPL_VAR["bdWrite"]["data"]["goodsNo"]){?>
								<div class="goods_select_item js_select_item">
									<div class="select_item_img">
										<input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["bdWrite"]["data"]["goodsNo"]?>">
										<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdWrite"]["data"]["goodsNo"]?>" target="_blank">
											<img src="<?php echo $TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsImageSrc"]?>" height="80" width="80" alt="">
										</a>
									</div>
									<div class="select_item_info">
										<em><?php echo $TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsNm"]?></em>
										<strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsPrice"])?><?php echo gd_global_currency_string()?></strong>
										<div class="select_item_button">
											<button type="button" class="btn_goods_item_del js_select_remove"><img src="/data/skin/front/aileenwedding/img/common/btn/btn_goods_del.png" alt="<?php echo __('선택 상품 삭제')?>"></button>
										</div>
									</div>
								</div>
<?php }?>
							</div>
							<!-- //goods_select_item -->
						</div>
						<!-- //board_goods_select -->
					</td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["data"]["canWriteOrderSelect"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('주문 내역')?></th>
					<td>
						<div class="board_goods_select">
<?php if(!$TPL_VAR["bdWrite"]["data"]["extraData"]["arrOrderGoodsData"]){?>
							<span class="js_selected_info_text"><?php echo __('선택된 주문이 없습니다.')?></span>
<?php }?>
							<a href="#addOrderLayer" title="<?php echo __('찾아보기')?>" class="btn_goods_select btn_open_layer"> <?php echo __('주문 내역')?> </a>
							<div id="selectOrderBox">
<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["extraData"]["arrOrderGoodsData"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
								<div class="goods_select_item js_select_item">
									<span class="select_item_img">
										<input type="hidden" name="orderGoodsNo[]" value="<?php echo $TPL_V1["sno"]?>">
										<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>" target="_blank">
											<img src="<?php echo $TPL_V1["goodsImageSrc"]?>" alt="">
										</a>
									</span>
									<span class="select_item_info">
										<em><?php echo $TPL_V1["goodsNm"]?></em>
										<div><?php echo $TPL_V1["optionName"]?></div>
										<strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["totalGoodsPrice"])?><?php echo gd_global_currency_string()?></strong>
										<button type="button" class="btn_goods_item_del js_select_remove"><img src="/data/skin/front/aileenwedding/img/common/btn/btn_goods_del.png" alt="<?php echo __('선택 상품 삭제')?>"></button>
									</span>
								</div>
<?php }}?>
							</div>
							<!-- //goods_select_item -->
						</div>
						<!-- //board_goods_select -->
					</td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdGoodsPtFl"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('평가')?></th>
					<td>
						<div class="form_element">
							<ul class="rating_star_list inline">
<?php if((is_array($TPL_R1=range( 5, 1))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
								<li>
									<label for="rating<?php echo $TPL_V1?>" class="choice_s <?php if($TPL_VAR["bdWrite"]["data"]["goodsPt"]==$TPL_V1){?>on<?php }?>">
										<input type="radio" name="goodsPt" value="<?php echo $TPL_V1?>" id="rating<?php echo $TPL_V1?>" name="rating" <?php if($TPL_VAR["bdWrite"]["data"]["goodsPt"]==$TPL_V1){?>checked<?php }?>/>
										<span class="rating_star">
											<span style="width:<?php echo $TPL_V1* 20?>%;"><?php echo __('별')?><?php echo $TPL_V1?></span>
										</span>
									</label>
								</li>
<?php }}?>
							</ul>
						</div>
					</td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdMobileFl"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('휴대폰')?></th>
					<td><input type="text" id="time" class="write_title" name="writerMobile" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerMobile"])?>" placeholder="- <?php echo __('-없이 입력하세요')?>"
							   autocomplete="off"></td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdEmailFl"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('이메일')?></th>
					<td><input type="text" name="writerEmail" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerEmail"])?>"></td>
				</tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdLinkFl"]=='y'){?>
				<tr>
					<th scope="row"><?php echo __('링크')?></th>
					<td><input type="text" name="urlLink" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["urlLink"])?>"></td>
				</tr>
<?php }?>
				<tr>
					<th scope="row"><?php echo __('제목')?></td>
					<td><input type="text" name="subject" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["subject"])?>" class="text" style="width: 700px;"></td>
				</tr>
				<tr>
					<th scope="row"><?php echo __('본문')?></th>
					<td class="write_editor">
						<div class="form_element">
<?php switch($TPL_VAR["bdWrite"]["cfg"]["bdSecretFl"]){case '1':?>
							<input type="checkbox" id="secret1" name="isSecret" value="y" checked/>
							<label class="check_s on" for="secret1"><?php echo __('비밀글')?></label>
<?php break;case '2':?>
<?php break;case '3':?>
							<em><?php echo __('해당글은 비밀글로만 작성이 됩니다.')?></em>
<?php break;default:?>
<?php if($TPL_VAR["req"]["mode"]=='reply'&&gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>    <!--// 비밀글 설정 - 부모글이 비밀글인 답글 작성시 무조건 비밀글-->
							<input type="hidden"  name="isSecret" value="y"/> <?php echo __('해당글은 비밀글로만 작성이 됩니다.')?>

<?php }else{?>
							<input type="checkbox" id="secret2" name="isSecret" value="y" <?php if(gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>checked
<?php }?> />
							<label for="secret2" class="check_s <?php if(gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>on<?php }?>"><?php echo __('비밀글')?></label>
<?php }?>
<?php }?>
						</div>
						<textarea id="editor" name="contents" cols="30" rows="10" style="width: 100%;"><?php echo $TPL_VAR["bdWrite"]["data"]["contents"]?></textarea>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php echo __('첨부파일')?></th>
					<td id="uploadBox">

<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["uploadFileNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
						<div class="file_upload_sec">
							<div class="form_element">
								<a href="../board/download.php?bdId=<?php echo $TPL_VAR["req"]["bdId"]?>&sno=<?php echo $TPL_VAR["req"]["sno"]?>&fid=<?php echo $TPL_I1?>" class="link_file_down"><?php echo $TPL_V1?></a>
								<input type="checkbox" id="delFile<?php echo $TPL_I1+ 1?>" name="delFile[<?php echo $TPL_I1?>]" value="y"/>
								<label for="delFile<?php echo $TPL_I1+ 1?>" class="check_s"><?php echo __('삭제')?></label>
							</div>
							<label for="attach<?php echo $TPL_I1+ 1?>">
								<input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly">
							</label>
							<div class="btn_upload_box">
								<button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
								<input type="file" id="attach<?php echo $TPL_I1+ 1?>" name="upfiles[]" class="file" title="<?php echo __('찾아보기')?>"/>
							</div>
						</div>
<?php }}?>

						<div class="file_upload_sec">
							<label for="attach">
								<input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly">
							</label>
							<div class="btn_upload_box">
								<button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
								<input type="file" id="attach" name="upfiles[]" class="file" title="<?php echo __('찾아보기')?>"/>
								<span class="btn_gray_list"><button type="button" id="addUploadBtn" class="btn_gray_big"><span>+ <?php echo __('추가')?></span></button></span>
							</div>
						</div>

					</td>
				</tr>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdSpamBoardFl"]& 2){?>
				<tr>
					<th scope="row"><?php echo __('자동등록방지')?></th>
					<td>
						<div class="capcha">
							<div class="capcha_img">
								<img src="./captcha.php" align="absmiddle" id="captchaImg"/>
							</div>
							<div class="capcha_txt">
								<p><?php echo __('보이는 순서대로 %s숫자 및 문자를 모두 입력해 주세요.','<br/>')?></p>
								<input type="text" class="text captcha" name="captchaKey" maxlength="5"
									   onKeyUp="javascript:this.value=this.value.toUpperCase();" onfocus="this.select()" label="<?php echo __('자동등록방지문자')?>">
								<span class="btn_gray_list">
									<button type="button" class="btn_gray_small" onclick="gd_reload_captcha()">
										<span><img src="/data/skin/front/aileenwedding/board/skin/qa/img/icon/icon_reset.png" alt=""> <?php echo __('이미지 새로고침')?></span>
									</button>
								</span>
							</div>
						</div>
					</td>
				</tr>
<?php }?>
				</tbody>
			</table>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdPrivateYN"]=="y"){?>
			<div class="board_write_agree">
				<div class="board_commen_agree">
					<div class="form_element">
						<div class="agree_choice_box">
							<input type="checkbox" name="private" value="y" id="acceptTerms">
							<label for="acceptTerms" class="check_s"><?php echo __('비회원 글작성에 대한 개인정보 수집 및 이용동의')?></label>
							<a href="../service/private.php" class="link_agree_go" target="_blank"><?php echo __('전체보기')?><i class="xi-angle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- //board_commen_agree -->
			</div>
			<!-- //board_write_agree -->
<?php }?>
			<div class="button_area inline">
				<button type="button" class="btn_before button w100 gray" onclick="javascript:history.back()"><strong><?php echo __('이전')?></strong></button>
				<span class="separate"></span>
				<button type="submit" class="btn_write_ok button w100 orange"><strong><?php echo __('저장')?></strong></button>
			</div>
		</form>
	</div><!-- .board_zone_write -->
</div><!-- .section_layer -->

<script type="text/javascript">
	var cfgUploadFl = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdUploadFl"]?>';
	var cfgEditorFl = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdEditorFl"]?>';
	var bdId = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdId"]?>';
	var bdSno = '<?php echo gd_isset($TPL_VAR["req"]["sno"])?>';
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_write.js" charset="utf-8"></script>
<script type="text/template" class="template">
	<div class="file_upload_sec">
		<label for="attach<%=idx%>">
			<input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly">
		</label>
		<div class="btn_upload_box">
			<button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
			<input type="file" id="attach<%=idx%>" name="upfiles[]" class="file" title="<?php echo __('찾아보기')?>"/>
			<span class="btn_gray_list"><button type="button" class="btn_gray_big" onclick="gd_remove_upload(this)"><span>- <?php echo __('삭제')?></span></button></span>
		</div>
	</div>
</script>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdEditorFl"]=='y'){?>
<script type="text/javascript">
	var editorPath = '<?php echo PATH_SKIN?>js/smart';
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/smart/js/HuskyEZCreator.js"></script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/smart/js/editorLoad.js"></script>
<?php }?>
<div id="addGoodsLayer" class="dn"></div>
<div id="addOrderLayer" class="dn"></div>
<script id="selectGoodsTblTr" type="text/html">
	<div class="goods_select_item js_select_item">
    <div class="select_item_img">
        <input type="hidden" name="goodsNo[]" value="<%=goodsNo%>">
        <a href="../goods/goods_view.php?goodsNo=<%=goodsNo%>" target="_blank">
			<img src="<%=goodsImgageSrc%>" height="80" width="80" alt="">
		</a>
    </div>
    <div class="select_item_info">
        <em><%=goodsName%></em>
		<strong><%=goodsPrice%></strong>
		<div class="select_item_button">
			<button type="button" class="btn_goods_item_del js_select_remove btn white"><?php echo __('선택 상품 삭제')?></button>
		</div>
    </div>
	</div>
</script>
<script id="selectOrderTblTr" type="text/html">
	<div class="goods_select_item js_select_item">
        <div class="select_item_img">
            <input type="hidden" name="orderGoodsNo[]" value="<%=orderGoodsNo%>">
            <a href="../goods/goods_view.php?goodsNo=<%=orderGoodsNo%>" target="_blank">
				<img src="<%=goodsImgageSrc%>" alt="">
			</a>
        </div>
        <div class="select_item_info">
            <em><%=goodsName%></em>
            <div><%=optionName%></div>
			<strong><%=goodsPrice%></strong>
			<div class="select_item_button">
				<button type="button" class="btn_goods_item_del js_select_remove btn white"><?php echo __('선택 상품 삭제')?></button>
			</div>
        </div>
	</div>
</script>