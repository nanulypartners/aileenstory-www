<?php /* Template_ 2.2.7 2020/03/15 19:16:40 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/board/view.html 000001370 */  $this->include_("includeFile");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/board_view.css" rel="stylesheet">
<section class="section_container" <?php if($TPL_VAR["req"]["noheader"]=='y'){?>style="padding:0;"<?php }?>>
    <div class="center_layer">
<?php if($TPL_VAR["req"]["noheader"]!='y'){?>
<?php if($TPL_VAR["req"]["bdId"]=='notice'||$TPL_VAR["req"]["bdId"]=='goodsqa'){?>
            <!-- 고객센터 메뉴 -->
            <?php echo includeFile('proc/_cs_menu.html')?>

            <!--// 고객센터 메뉴 -->
<?php }?>
<?php if($TPL_VAR["req"]["bdId"]=='event'){?>
            <!-- 이벤트 메뉴 -->
            <?php echo includeFile('proc/_event_menu.html')?>

            <!--// 이벤트 메뉴 -->
<?php }?>
<?php if($TPL_VAR["req"]["bdId"]=='goodsreview'){?>
            <!-- 이용후기 메뉴 -->
            <?php echo includeFile('proc/_review_menu.html')?>

            <!--// 이용후기 메뉴 -->
<?php }?>
<?php }?>
<?php $this->print_("view",$TPL_SCP,1);?>

    </div><!-- .center_layer -->
</section><!-- .section_container -->
<?php $this->print_("footer",$TPL_SCP,1);?>