<?php /* Template_ 2.2.7 2020/05/24 15:38:52 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/goods_board_review_view.html 000009801 */ ?>
<td></td>
<td colspan="3" class="reviews_new_box">
    <div class="board_cont">
        <div class="board_view">
            <div><?php echo $TPL_VAR["bdView"]["data"]["workedContents"]?></div>
<?php if($TPL_VAR["bdView"]["data"]["isFile"]=='y'){?>
            <div class="board_view_attach">
                <strong><?php echo __('첨부파일')?></strong>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["uploadedFile"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                <span class="attach_list">
                    <a href="../board/download.php?bdId=<?php echo $TPL_VAR["bdView"]["cfg"]["bdId"]?>&sno=<?php echo $TPL_VAR["req"]["sno"]?>&fid=<?php echo $TPL_V1["fid"]?>"><?php echo $TPL_V1["name"]?></a>
                </span>
<?php }}?>
            </div>
            <!-- //board_view_attach -->
<?php }?>
        </div>
        <!-- //board_view -->

        <div class="btn_view_comment_box">
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["modify"]){case 'y':case 'c':?>
            <span class="btn_gray_list"><button type="button" class="btn_gray_mid js_btn_modify" data-auth="<?php echo $TPL_VAR["bdView"]["data"]["auth"]["modify"]?>"><span><?php echo __('수정')?></span></button></span>
<?php }?>
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["delete"]){case 'y':case 'c':?>
            <span class="btn_gray_list"><button type="button" class="btn_gray_mid js_btn_delete" data-auth="<?php echo $TPL_VAR["bdView"]["data"]["auth"]["delete"]?>"><span><?php echo __('삭제')?></span></button></span>
<?php }?>
        </div>
        <!-- //btn_view_comment_box -->

        <div class="view_comment">
            <div class="view_comment_top">
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
                <span class="comment_num"><strong><?php echo $TPL_VAR["bdView"]["data"]["memoCnt"]?></strong> <?php echo __('개의 댓글이 있습니다')?></span>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdRecommendFl"]=='y'){?>
                <span id="recommendCount" class="comment_best_num"><?php echo __('추천')?> : <strong><?php echo $TPL_VAR["bdView"]["data"]["recommend"]?></strong></span>
                <a href="javascript:void(0)" class="btn_comment_best js_btn_recommend"><em><?php echo __('추천하기')?></em></a>
<?php }?>
            </div>
            <!-- //view_comment_top -->
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
            <div class="view_comment_list">
                <ul>
<?php if($TPL_VAR["bdView"]["data"]["memoList"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["memoList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li class="js_data_comment_row <?php if($TPL_V1["groupThread"]){?>comment_reply<?php }?>" data-memosno="<?php echo $TPL_V1["sno"]?>" data-memoauth="<?php echo $TPL_V1["auth"]?>">
                        <div class="comment-item">
                            <strong class="comment_name"><?php echo $TPL_V1["writer"]?></strong>
                            <div class="comment_cont">
<?php if($TPL_VAR["bdView"]["data"]["allReplyShow"]=='y'){?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }else{?>
<?php if($TPL_V1["isSecretReply"]=='y'){?>
<?php if($TPL_V1["isShowSecretReply"]=='y'){?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }else{?>
                                <input type="hidden" name="secretReply" value="y">
                                <em class="js_comment_btn_secret"><img src="<?php echo $TPL_VAR["bdListCfg"]["iconImage"]["secret"]["url"]?>" align=absmiddle><?php echo $TPL_VAR["secretReplyCheck"]["secretReplyTitle"]?></em>
<?php }?>
<?php }else{?>
                                <em><?php echo $TPL_V1["workedMemo"]?></em>
<?php }?>
<?php }?>
                                <span class="board_day_time"><?php echo $TPL_V1["regDt"]?></span>
                            </div>
                            <div class="btn">
<?php if(!$TPL_V1["groupThread"]){?>
                                <span class="btn_gray_list" style="display:none;"><button type="button" class="btn_gray_small js_comment_btn_reply"><span><?php echo __('답글')?></span></button></span>
<?php }?>
<?php switch($TPL_V1["auth"]){case 'y':case 'c':?>
                                <span class="btn_gray_list"><button type="button" class="btn_gray_small js_comment_btn_modify"><span><?php echo __('수정')?></span></button></span>
                                <span class="btn_gray_list"><button type="button" class="btn_gray_small js_comment_btn_delete"><span><?php echo __('삭제')?></span></button></span>
<?php }?>
                            </div>
                        </div>
                        <div class="board_comment_box js_action_form" style="display: none">
                            <div class="board_comment_write">
<?php if(!gd_is_login()){?>
                                <input type="text" name="writerNm" placeholder="<?php echo __('이름')?>">
                                <input type="password" name="password" autocomplete="off" placeholder="<?php echo __('비밀번호')?>" autocomplete="off">
<?php }?>
                                <div class="form_element">
                                    <?php echo $TPL_VAR["secretReplyCheck"]["replyModify"]?>

                                </div>
                                <div class="comment_textarea">
                                    <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>" autocomplete="off"></textarea>
                                    <span class="btn_comment_box"><button type="button" class="button btn_comment_ok js_comment_btn_action"><em><?php echo __('확인')?></em></button></span>
                                </div>
                            </div>
                            <!-- //board_comment_write -->
<?php if(!gd_is_login()){?>
                            <div class="board_commen_agree">
                                <div class="form_element">
                                    <input type="checkbox" name="checkCollectAgree" class="checkbox" id="info_collection_agree_action" />
                                    <label for="info_collection_agree_action">(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></label>
                                    <a href="javascript:void(0)" onclick="gd_redirect_collection_agree()"><?php echo __('전체보기')?></a>
                                    <textarea cols="30" rows="5"><?php echo $TPL_VAR["bdView"]["private"]?></textarea>
                                </div>
                            </div>
                            <!-- //board_commen_agree -->
<?php }?>
                        </div>
                    </li>
<?php }}?>
<?php }else{?>
                    <li>
                        <div class="no_data"><?php echo __('등록된 댓글이 없습니다.')?></div>
                    </li>
<?php }?>
                </ul>
            </div>
            <!-- //view_comment_list -->
<?php if($TPL_VAR["bdView"]["cfg"]["auth"]["memo"]=='y'){?>
            <div class="board_comment_box js_form_write">
                <div class="board_comment_write">
<?php if(!gd_is_login()){?>
                    <input type="text" name="writerNm" placeholder="<?php echo __('이름')?>">
                    <input type="password" name="password" autocomplete="off" placeholder="<?php echo __('비밀번호')?>" autocomplete="off">
<?php }?>
                    <div class="form_element">
                        <?php echo $TPL_VAR["secretReplyCheck"]["replyWrite"]?>

                    </div>
                    <div class="comment_textarea">
                        <textarea class="text" name="memo" placeholder="<?php echo __('댓글 내용을 입력하세요')?>" autocomplete="off"></textarea>
                        <span class="btn_comment_box"><button type="button" class="button btn_comment_ok js_comment_btn_write"><em><?php echo __('확인')?></em></button></span>
                    </div>
                </div>
                <!-- //board_comment_write -->
<?php if(!gd_is_login()){?>
                <div class="board_commen_agree">
                    <div class="form_element">
                        <input type="checkbox" name="checkCollectAgree" class="checkbox" id="info_collection_agree_write" />
                        <label for="info_collection_agree_write">(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></label>
                        <a href="javascript:void(0)" onclick="gd_redirect_collection_agree()"><?php echo __('전체보기')?></a>
                        <textarea cols="30" rows="5"><?php echo $TPL_VAR["bdView"]["private"]?></textarea>
                    </div>
                </div>
                <!-- //board_commen_agree -->
<?php }?>
            </div>
            <!-- //board_comment_box -->
<?php }else{?>
            <div style="display:none;" class="comment-write js-form-write">
<?php if(gd_is_login()){?>
                <?php echo __('댓글 권한이 없습니다.')?>

<?php }else{?>
                <?php echo __('로그인을 하셔야 댓글을 등록하실 수 있습니다.')?>

<?php }?>
            </div>
<?php }?>
<?php }?>
        </div>
        <!-- //view_comment -->
    </div>
    <!-- //board_cont -->
</td>