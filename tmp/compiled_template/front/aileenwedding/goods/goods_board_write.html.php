<?php /* Template_ 2.2.7 2020/04/14 04:55:15 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/goods_board_write.html 000017288 */ ?>
<link href="/data/skin/front/aileenwedding/css/popup.css" rel="stylesheet">
<div class="layer_wrap_cont board_write_popup">
    <div class="ly_tit"><h4><?php echo __($TPL_VAR["bdWrite"]["cfg"]["bdNm"])?> <?php echo __('쓰기')?></h4></div>
    <div class="ly_cont">
        <form name="frmWrite" target="ifrmProcess" id="frmWrite" action="../board/board_ps.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="gboard" value="y">
            <input type="hidden" name="bdId" value="<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdId"]?>">
            <input type="hidden" name="sno" value="<?php echo gd_isset($TPL_VAR["req"]["sno"])?>">
            <input type="hidden" name="mode" value="<?php echo gd_isset($TPL_VAR["req"]["mode"])?>">
            <input type="hidden" name="goodsNo" value="<?php echo gd_isset($TPL_VAR["req"]["goodsNo"])?>">
            <input type="hidden" name="returnUrl" value="<?php echo $TPL_VAR["querySdling"]?>">
<?php if($TPL_VAR["oldPassword"]){?><input type="hidden" name="oldPassword" value="<?php echo $TPL_VAR["oldPassword"]?>"><?php }?>
<?php if($TPL_VAR["req"]["orderGoodsNo"]){?>
            <input type="hidden" name="orderGoodsNo" value="<?php echo $TPL_VAR["req"]["orderGoodsNo"]?>">
<?php }?>

            <div class="scroll_box">
                <div class="top_item_photo_info">
                    <div class="item_photo_box">
                        <?php echo $TPL_VAR["goodsView"]['image']['detail']['img'][ 0]?>

                    </div>
                    <!-- //item_photo_view -->
                    <div class="item_info_box">
                        <h5><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></h5>
<?php if(gd_isset($TPL_VAR["goodsView"]['shortDescription'])){?>
                        <em><?php echo $TPL_VAR["goodsView"]['shortDescription']?></em>
<?php }?>
                    </div>
                </div>
                <!-- //top_item_photo_info -->
                <div class="board_write_box">
                    <table class="board_write_table">
                        <colgroup>
                            <col style="width:15%" />
                            <col style="width:85%" />
                        </colgroup>
                        <tbody>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdCategoryFl"]=='y'){?>
                            <tr>
                                <th scope="row"><?php echo __('말머리')?></th>
                                <td>
                                    <div class="category_select">
                                        <?php echo gd_select_box('category','category',$TPL_VAR["bdWrite"]["cfg"]["arrCategory"],null,gd_isset($TPL_VAR["bdWrite"]["data"]["category"]),null,'style="width:127px;"','chosen-select')?>

                                    </div>
                                </td>
                            </tr>
<?php }?>
                            <tr>
                                <th scope="row"><?php echo __('작성자')?></th>
                                <td>
<?php if($TPL_VAR["bdWrite"]["data"]["writerNm"]){?>
                                    <input type="text" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>" disabled="disabled" class="text" />
                                    <input type="hidden" name="writerNm" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>" />
<?php }else{?>
                                    <input type="text" name="writerNm" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>" title="<?php echo __('작성자 입력')?>" class="text" />
<?php }?>
                                </td>
                            </tr>
<?php if(!gd_isset($TPL_VAR["bdWrite"]["data"]["memNo"])&&!$TPL_VAR["oldPassword"]){?>
                            <tr>
                                <th scope="row"><?php echo __('비밀번호')?></th>
                                <td>
                                    <input type="password" name="writerPw" title="<?php echo __('비밀번호 입력')?>" class="text" />
                                </td>
                            </tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdGoodsPtFl"]=='y'){?>
                            <tr>
                                <th scope="row"><?php echo __('평가')?></th>
                                <td>
                                    <div class="form_element">
                                        <ul class="rating_star_list">
<?php if((is_array($TPL_R1=range( 5, 1))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                                            <li>
                                                <input type="radio" id="rating<?php echo $TPL_V1?>" name="goodsPt" value="<?php echo $TPL_V1?>" <?php if($TPL_VAR["bdWrite"]["data"]["goodsPt"]==$TPL_V1){?>checked<?php }?>/>
                                                <label for="rating<?php echo $TPL_V1?>" class="choice_s <?php if($TPL_VAR["bdWrite"]["data"]["goodsPt"]==$TPL_V1){?>on<?php }?>">
                                                    <span class="rating_star"><span style="width:<?php echo $TPL_V1* 20?>%;"><?php echo __('별')?><?php echo $TPL_V1?></span></span>
                                                </label>
                                            </li>
<?php }}?>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdMobileFl"]=='y'){?>
                            <tr>
                                <th scope="row"><?php echo __('휴대폰')?></th>
                                <td>
                                    <input type="text" name="writerMobile" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerMobile"])?>" placeholder="<?php echo __('-없이 입력하세요')?>" class="text" />
                                </td>
                            </tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdEmailFl"]=='y'){?>
                            <tr>
                                <th scope="row"><?php echo __('이메일')?></th>
                                <td>
                                    <input type="text" name="writerEmail" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerEmail"])?>" placeholder="<?php echo __('이메일 입력')?>" class="text" />
                                </td>
                            </tr>
<?php }?>
                            <tr>
                                <th scope="row"><?php echo __('제목')?></th>
                                <td>
                                    <input type="text" name="subject" placeholder="<?php echo __('제목 입력')?>" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["subject"])?>" class="write_title text" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><?php echo __('내용')?></th>
                                <td class="wirte_editor">
                                    <div class="form_element">
<?php switch($TPL_VAR["bdWrite"]["cfg"]["bdSecretFl"]){case '1':?>
                                        <input type="checkbox" name="isSecret" value="y" id="secret" class="checkbox" checked="checked" />
                                        <label for="secret" class="check_s on"><?php echo __('비밀글')?></label>
<?php break;case '2':?>
<?php break;case '3':?>
                                        <?php echo __('해당글은 비밀글로만 작성이 됩니다.')?>

<?php break;default:?>
<?php if($TPL_VAR["req"]["mode"]=='reply'&&gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>    <!--// 비밀글 설정 - 부모글이 비밀글인 답글 작성시 무조건 비밀글-->
                                        <input type="hidden" name="isSecret" value="y" /> <?php echo __('해당글은 비밀글로만 작성이 됩니다.')?>

                                        <label for="secret" class="check_s on"><?php echo __('비밀글')?></label>
<?php }else{?>
                                        <input type="checkbox" name="isSecret" value="y" id="secret" class="checkbox" <?php if(gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>checked="checked"<?php }?> />
                                        <label for="secret" class="check_s  <?php if(gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>on<?php }?>"><?php echo __('비밀글')?></label>
<?php }?>
<?php }?>
                                    </div>
                                    <textarea title="<?php echo __('내용 입력')?>" id="editor" style="width:100%; min-width:510px;" name="contents" cols="50" rows="3"><?php echo $TPL_VAR["bdWrite"]["data"]["contents"]?></textarea>
                                </td>
                            </tr>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdUploadFl"]=='y'){?>
                            <tr>
                                <th scope="row"><?php echo __('파일')?></th>
                                <td id="uploadBox">
<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["uploadFileNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                                    <div class="file_upload_sec">
                                        <div class="form_element">
                                            <a href="../board/download.php?bdId=<?php echo $TPL_VAR["req"]["bdId"]?>&sno=<?php echo $TPL_VAR["req"]["sno"]?>&fid=<?php echo $TPL_I1?>" class="link_file_down"><?php echo $TPL_V1?></a>
                                            <input type="checkbox" id="delFile[<?php echo $TPL_I1?>]" name="delFile[<?php echo $TPL_I1?>]" value="y" />
                                            <label for="delFile[<?php echo $TPL_I1?>]" class="check_s"><?php echo __('삭제')?></label>
                                        </div>
                                        <label for="attach<?php echo $TPL_I1+ 1?>"><input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly"/></label>
                                        <div class="btn_upload_box">
                                            <button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
                                            <input type="file" name="upfiles[]" id="attach<?php echo $TPL_I1+ 1?>" class="file" title="<?php echo __('찾아보기')?>"/>
                                        </div>
                                    </div>
<?php }}?>
                                    <div class="file_upload_sec">
                                        <label for="attach"><input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly" /></label>
                                        <div class="btn_upload_box">
                                            <button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
                                            <input type="file" id="attach" name="upfiles[]" class="file" title="<?php echo __('찾아보기')?>" />
                                            <span class="btn_gray_list"><button type="button" id="addUploadBtn" class="btn_gray_big"><span>+ <?php echo __('추가')?></span></button></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdSpamBoardFl"]& 2){?>
                            <tr>
                                <th scope="row"><?php echo __('자동등록방지')?></th>
                                <td>
                                    <div class="capcha">
                                        <div class="capcha_img">
                                            <img src="../board/captcha.php?ch=<?php echo time()?>" align="absmiddle" id="captchaImg"/>
                                        </div>
                                        <div class="capcha_txt">
                                            <p><?php echo __('보이는 순서대로 %s숫자 및 문자를 모두 입력해 주세요.','<br/>')?></p>
                                            <input type="text" name="captchaKey" maxlength="5" onKeyUp="javascript:this.value=this.value.toUpperCase();" onfocus="this.select()" label="<?php echo __('자동등록방지문자')?>">
                                            <span class="btn_gray_list">
                                                <button type="button" class="btn_gray_small" onclick="gd_reload_captcha()">
                                                    <span><img src="/data/skin/front/aileenwedding/img/etc/icon_reset.png" alt="" class="va-m"> <?php echo __('이미지 새로고침')?></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
<?php }?>
                        </tbody>
                    </table>
                </div>
                <!-- //board_wirte_box -->
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdPrivateYN"]=='y'){?>
                <div class="board_wirte_agree">
                    <div class="board_commen_agree">
                        <div class="form_element">
                            <h5><?php echo __('비회원 개인정보 수집동의')?></h5>
                            <div class="textarea_box">
                                <div class="textarea_txt"><?php echo $TPL_VAR["bdWrite"]["private"]?></div>
                            </div>
                            <!-- //textarea_box -->
                            <div class="agree_choice_box">
                                <input type="checkbox" id="qnaPrivacyAgree" name="private" value="y" />
                                <label for="qnaPrivacyAgree"><?php echo __('위 내용에 동의합니다.')?></label>
                                <a href="../service/private.php" target="_blank" class="link_agree_go"><?php echo __('전체보기')?>&gt;</a>
                            </div>
                        </div>
                    </div>
                    <!-- //board_commen_agree -->
                </div>
                <!-- //board_wirte_agree -->
<?php }?>
            </div>
            <!-- //scroll_box -->
        </form>
        <div class="btn_center_box button_area inline">
            <button class="btn_ly_cancel button gray" onclick="writerLayer.close()"><strong><?php echo __('취소')?></strong></button>
            <button class="btn_ly_write_ok button orange" onclick="save()"><strong><?php echo __('등록')?></strong></button>
        </div>
    </div>
    <!-- //ly_cont -->
    <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
</div>
<!-- //layer_wrap_cont -->

<script type="text/template" class="template">
    <div class="file_upload_sec">
        <label for="attach<%=idx%>"><input type="text" class="file_text" title="<?php echo __('파일 첨부하기')?>" readonly="readonly" /></label>
        <div class="btn_upload_box">
            <button type="button" class="btn_upload" title="<?php echo __('찾아보기')?>"><em><?php echo __('찾아보기')?></em></button>
            <input type="file" id="attach<%=idx%>" name="upfiles[]" class="file" title="<?php echo __('찾아보기')?>"/>
            <span class="btn_gray_list"><button type="button" class="btn_gray_big" onclick="gd_remove_upload(this)"><span>- <?php echo __('삭제')?></span></button></span>
        </div>
    </div>
</script>
<script>
    var mode = '<?php echo $TPL_VAR["req"]["mode"]?>';
    var cfgUploadFl = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdUploadFl"]?>';
    var cfgEditorFl = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdEditorFl"]?>';

    function save() {
        <?php echo $TPL_VAR["customScript"]?>

        $('#frmWrite').submit();
    }
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_write.js" charset="utf-8"></script>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdEditorFl"]=='y'){?>
<script type="text/javascript">
    var editorPath = '<?php echo PATH_SKIN?>js/smart';
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/smart/js/HuskyEZCreator.js"></script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/smart/js/editorLoad.js"></script>
<?php }?>