<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/goods_list.html 000004378 */  $this->include_("dataSubCategory");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <div class="top_layer">
            <ul class="tab_area inline">
                <li>
                    <a href="?<?php echo $TPL_VAR["cateType"]?>Cd=<?php echo gd_html_cut($TPL_VAR["cateCd"], 3,'')?>">
                        <img src="/data/skin/front/aileenwedding/img/sub/menu_all<?php if(gd_html_cut($TPL_VAR["cateCd"], 3,'')==$TPL_VAR["cateCd"]){?>_on<?php }else{?>_off<?php }?>.png" alt="">
                    </a>
                </li><?php if(dataSubCategory($TPL_VAR["cateCd"],$TPL_VAR["cateType"])){?><?php if((is_array($TPL_R1=dataSubCategory($TPL_VAR["cateCd"],$TPL_VAR["cateType"]))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?><li class="separate"></li><li>
                    <a href="?<?php echo $TPL_VAR["cateType"]?>Cd=<?php echo $TPL_V1["cateCd"]?>">
                        <img src="/data/skin/front/aileenwedding/img/sub/menu_goods_<?php echo $TPL_V1["cateCd"]?><?php if($TPL_VAR["cateCd"]==$TPL_V1["cateCd"]){?>_on<?php }else{?>_off<?php }?>.png" alt="">
                    </a>
                </li><?php }}?><?php }?>
            </ul><!-- .tab_area -->
            <div class="select_area">
                <form name="frmList" action="">
                    <input type="hidden" name="<?php echo $TPL_VAR["cateType"]?>Cd" value="<?php echo $TPL_VAR["cateCd"]?>">
                    <input type="hidden" name="sort" value="">
                </form>
                <div class="selected">
                    <span>
<?php if($TPL_VAR["sort"]==''){?>
                            <?php echo __('추천순')?>

<?php }else{?>
<?php if($TPL_VAR["sort"]=='orderCnt desc,g.regDt desc'){?><?php echo __('인기상품순')?><?php }?>
<?php if($TPL_VAR["sort"]=='goodsPrice asc,g.regDt desc'){?><?php echo __('낮은가격순')?><?php }?>
<?php if($TPL_VAR["sort"]=='goodsPrice desc,g.regDt desc'){?><?php echo __('높은가격순')?><?php }?>
<?php if($TPL_VAR["sort"]=='reviewCnt desc,g.regDt desc'){?><?php echo __('상품평순')?><?php }?>
<?php if($TPL_VAR["sort"]=='g.regDt desc'){?><?php echo __('신상품순')?><?php }?>
<?php }?>
                    </span>
                    <i class="xi-caret-down-min"></i>
                </div>
                <ul>
                    <li data-value=""><?php echo __('추천순')?></li>
                    <li data-value="orderCnt desc,g.regDt desc"><?php echo __('인기상품순')?></li>
                    <li data-value="g.regDt desc"><?php echo __('신상품순')?></li>
                    <li data-value="goodsPrice desc,g.regDt desc"><?php echo __('높은가격순')?></li>
                    <li data-value="goodsPrice asc,g.regDt desc"><?php echo __('낮은가격순')?></li>
                    <li data-value="reviewCnt desc,g.regDt desc"><?php echo __('상품평순')?></li>
                </ul>
            </div><!-- .select_area -->
        </div><!-- .top_layer -->
        <!-- 상품 리스트 -->
<?php $this->print_("goodsTemplate",$TPL_SCP,1);?>

        <!-- //상품 리스트 -->
        <div style="display:none;" class="pagination">
            <?php echo $TPL_VAR["page"]->getPage()?>

        </div>
    </div><!-- .center_layer -->
</section>

<script>
jQuery(function($) {
    $(document).ready(function() {

    });
    $(document).on('click', '.select_area .selected', function() {
        var $select_ul = $(this).parent().find('ul');
        if($select_ul.css('display') == 'none') {
            $select_ul.slideDown('fast');
        }
        else {
            $select_ul.slideUp('fast');
        }
    });
    $(document).on('click', '.select_area ul > li', function() {
        var $select_form = $('form[name=frmList]');
        var $select_sort = $select_form.find('input[name=sort]');
        var $select_value = $(this).data('value');
        $select_sort.val($select_value);
        $select_form.submit();
    }); 
});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>