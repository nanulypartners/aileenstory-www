<?php /* Template_ 2.2.7 2020/06/14 02:01:45 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/goods_pic_write.html 000030107 */ 
if (is_array($TPL_VAR["defaultData"])) $TPL_defaultData_1=count($TPL_VAR["defaultData"]); else if (is_object($TPL_VAR["defaultData"]) && in_array("Countable", class_implements($TPL_VAR["defaultData"]))) $TPL_defaultData_1=$TPL_VAR["defaultData"]->count();else $TPL_defaultData_1=0;
if (is_array($TPL_VAR["extraData"])) $TPL_extraData_1=count($TPL_VAR["extraData"]); else if (is_object($TPL_VAR["extraData"]) && in_array("Countable", class_implements($TPL_VAR["extraData"]))) $TPL_extraData_1=$TPL_VAR["extraData"]->count();else $TPL_extraData_1=0;?>
<link href="/data/skin/front/aileenwedding/css/reset.css" rel="stylesheet" />
<link href="/data/skin/front/aileenwedding/css/webfont.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/fontawesome-all.min.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/xeicon.min.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/simplebar.min.css" rel="stylesheet" />
<link href="/data/skin/front/aileenwedding/css/layout.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/goods_pic.css" rel="stylesheet">
<script src="/data/skin/front/aileenwedding/js/jquery/jquery.min.js"></script>
<script src="/data/skin/front/aileenwedding/js/simplebar.min.js"></script>

<div class="goods_pic_wrapper" id="GoodsPicWrapper">
    <div style="display:none;">
        <?php echo gd_debug($TPL_VAR["extraData"])?>

    </div>
    <div class="goods_pic_header">
        <div class="video_area">
            <div class="video">
                <iframe src="<?php echo $TPL_VAR["goodsData"]['externalVideoUrl']?>?title=0&amp;byline=0&amp;portrait=0" allow="autoplay; fullscreen" allowfullscreen="" width="640" height="360" frameborder="0"></iframe>
            </div>
        </div>
<?php if($TPL_VAR["orderData"]['orderName']){?>
        <div class="header_area left">
            <div class="title">
                <?php echo $TPL_VAR["orderData"]['orderName']?>님의<br>파일을 업로드해 주세요.
            </div>
            <div class="desc">
                주문번호: <?php echo $TPL_VAR["orderData"]['orderNo']?>

            </div>
        </div><!-- .header_area -->
<?php }else{?>
        <div class="header_area left">
            <div class="title">
                파일을 업로드해 주세요.
            </div>
            <div class="desc">
                주문자 정보를 정확하게 입력해 주시기 바랍니다.
            </div>
        </div><!-- .header_area -->
<?php }?>
        <div class="header_area right">
            <a href="#" class="btn">
                <img src="/data/skin/front/aileenwedding/img/common/upload_icon_info.png" alt="" />
                <div>업로드 안내</div>
            </a>
        </div><!-- .header_area -->
    </div><!-- .goods_pic_header -->
    <div class="goods_pic_total">
        전체 <?php echo $TPL_VAR["uploadCount"]?>개 중 / <span class="active"><span class="count" id="UploadedCount">0</span>개 업로드</span>
        <ul class="icon_list">
            <li><a href="#" onclick="return false;" id="UploadListButton"><img src="/data/skin/front/aileenwedding/img/common/upload_icon_list_off.png" alt="" /></a></li>
            <li><a href="#" onclick="return false;" id="UploadDetailButton"><img src="/data/skin/front/aileenwedding/img/common/upload_icon_detail_off.png" alt="" /></a></li>
        </ul>
    </div><!-- .goods_pic_total -->
    <ul class="goods_pic_menu" id="TabMenu">
        <li class="on" data-menu="all"><a href="#" onclick="return false;">전체<span class="underline"></span></a></li>
        <li data-menu="picture"><a href="#" onclick="return false;">사진<span class="underline"></span></a></li>
        <li data-menu="video"><a href="#" onclick="return false;">동영상<span class="underline"></span></a></li>
        <li data-menu="text"><a href="#" onclick="return false;">자막<span class="underline"></span></a></li>
        <li data-menu="etc"><a href="#" onclick="return false;">기타<span class="underline"></span></a></li>
        <li class="not_menu" data-menu="not"><a href="#" onclick="return false;">미업로드<span class="underline"></span></a></li>
    </ul><!-- .goods_pic_menu -->
    <form action="./" method="post" enctype="multipart/form-data" id="GoodsPicForm">
        <input type="hidden" name="orderNo" value="<?php echo $TPL_VAR["orderData"]['orderNo']?>" />
        <input type="hidden" name="goodsNo" value="<?php echo $TPL_VAR["goodsData"]['goodsNo']?>" />
        <input type="hidden" name="goodsPicNo" value="<?php echo $TPL_VAR["goodsPicNo"]?>" />
        <input type="hidden" name="status" value="1" />
        <input type="hidden" name="upload_count" value="<?php echo $TPL_VAR["uploadCount"]?>" />
        <input type="hidden" name="uploaded_count" value="" />
        <div class="goods_pic_body">
            <div class="default_area section_area" data-type="default">
                <input type="hidden" name="uploaded_default" value="false" />
                <div class="title default">
                    기본정보 입력
                    <a href="#" onclick="return false;" class="check">
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_ex.png" alt="" />
                    </a>
                </div>
                <div class="section">
                    <ul class="form">
<?php if($TPL_defaultData_1){foreach($TPL_VAR["defaultData"] as $TPL_V1){?>
                        <li data-type="<?php echo $TPL_V1["type"]?>">
<?php if($TPL_V1["type"]=='single'){?>
                            <input type="text" name="default_input[<?php echo $TPL_V1["order"]?>]" value="<?php echo $TPL_V1["cont"]?>" placeholder="<?php echo $TPL_V1["input"]?>" class="check_text text" />
<?php }elseif($TPL_V1["type"]=='multi'){?>
                            <textarea name="default_input[<?php echo $TPL_V1["order"]?>]" placeholder="<?php echo $TPL_V1["input"]?>" class="check_text text"><?php echo $TPL_V1["cont"]?></textarea>
<?php }?>
<?php if($TPL_V1["order"]<= 2){?>
                            <span class="must">(필수)</span>
<?php }?>
                        </li>
<?php }}?>
                    </ul><!-- .form -->
                </div><!-- .section -->
            </div><!-- .section_area -->
<?php if($TPL_extraData_1){foreach($TPL_VAR["extraData"] as $TPL_V1){?>
            <div class="extra_area section_area" data-type="<?php echo $TPL_V1["type"]?>">
                <input type="hidden" name="uploaded_extra[]" value="false" />
                <div class="title">
                    <span class="icon">
<?php if($TPL_V1["type"]=='single'||$TPL_V1["type"]=='multi'){?>
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_single.png" alt="" />
<?php }elseif($TPL_V1["type"]=='picture1'||$TPL_V1["type"]=='picture2'||$TPL_V1["type"]=='picture3'){?>
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_picture.png" alt="" />
<?php }elseif($TPL_V1["type"]=='video'){?>
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_video.png" alt="" />
<?php }elseif($TPL_V1["type"]=='file'){?>
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_audio.png" alt="" />
<?php }?>
                    </span>
                    [<?php echo $TPL_V1["order"]?>] <?php echo $TPL_V1["title"]?>

                    <a href="#" onclick="return false;" class="check">
                        <img src="/data/skin/front/aileenwedding/img/common/upload_icon_ex.png" alt="" />
                    </a>
                </div>
                <div class="section">
                    <ul class="upload single">
<?php if($TPL_V1["image"]){?>
                        <li class="image" style="background-image:url(<?php echo $TPL_V1["image"]?>);">
                        </li>
<?php }?>
<?php if($TPL_V1["type"]=='picture1'||$TPL_V1["type"]=='picture2'||$TPL_V1["type"]=='picture3'||$TPL_V1["type"]=='video'||$TPL_V1["type"]=='file'){?>
                        <li class="file">
<?php if($TPL_V1["type"]=='picture1'){?>
                            <input type="file" name="extra_image[<?php echo $TPL_V1["order"]?>]" data-order="<?php echo $TPL_V1["order"]?>" accept="*" />
<?php }elseif($TPL_V1["type"]=='picture2'||$TPL_V1["type"]=='picture3'){?>
                            <input type="file" name="extra_image[<?php echo $TPL_V1["order"]?>]" data-order="<?php echo $TPL_V1["order"]?>" accept="*" multiple />
<?php }elseif($TPL_V1["type"]=='video'){?>
                            <input type="file" name="extra_image[<?php echo $TPL_V1["order"]?>]" data-order="<?php echo $TPL_V1["order"]?>" accept="*" multiple />
<?php }elseif($TPL_V1["type"]=='file'){?>
                            <input type="file" name="extra_image[<?php echo $TPL_V1["order"]?>]" data-order="<?php echo $TPL_V1["order"]?>" accept="*" multiple />
<?php }?>
                            <div class="explain">
                                <i class="xi-cloud-upload-o"></i><br>
                                클릭 또는 드래그하여<br>
                                업로드 해주세요
                            </div>
                        </li>
<?php }?>
                    </ul><!-- .upload -->
<?php if($TPL_V1["type"]=='picture1'||$TPL_V1["type"]=='picture2'||$TPL_V1["type"]=='picture3'||$TPL_V1["type"]=='video'||$TPL_V1["type"]=='file'){?>
                    <div class="upload_file_count">업로드 수: <span class="current_count"><?php echo $TPL_V1["upload_file_count"]?></span> / <span class="max_count"><?php echo $TPL_V1["upload_count"]?></span></div>
<?php }?>
<?php if(!empty($TPL_V1["image_text"])){?>
                    <div class="desc"><?php echo $TPL_V1["image_text"]?></div>
<?php }?>
                    <div class="uploaded_files" id="UploadedFiles<?php echo $TPL_V1["order"]?>">
<?php if((is_array($TPL_R2=$TPL_V1["files"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <div class="list" style="background-image:url(<?php echo $TPL_V2["path"]?>);">
<?php if($TPL_V2["type"]=='video'){?>
                            <div class="file_type">
                                <i class="xi-file-video-o"></i><br>
                                비디오 파일
                            </div>
<?php }elseif($TPL_V2["type"]=='etc'){?>
                            <div class="file_type">
                                <i class="xi-file-zip-o"></i><br>
                                기타 파일
                            </div>
<?php }?>
                            <a href="#" onclick="return false;" class="delete_file" data-file_no="<?php echo $TPL_V2["goodsPicFileNo"]?>"><i class="xi-close"></i></a>
                        </div>
<?php }}?>
                    </div>
                    <div class="input">
<?php if($TPL_V1["type"]=='single'||$TPL_V1["type"]=='picture1'){?>
<?php if(!empty($TPL_V1["cont"])){?>
                            <input type="text" name="extra_text[<?php echo $TPL_V1["order"]?>]" value="<?php echo $TPL_V1["cont"]?>" class="check_text cont text" data-text_count="<?php echo $TPL_V1["text_count"]?>" />
<?php }else{?>
                            <input type="text" name="extra_text[<?php echo $TPL_V1["order"]?>]" value="<?php echo $TPL_V1["text"]?>" class="check_text cont text" data-text_count="<?php echo $TPL_V1["text_count"]?>" />
<?php }?>
<?php }else{?>
<?php if(!empty($TPL_V1["cont"])){?>
                            <textarea name="extra_text[<?php echo $TPL_V1["order"]?>]" class="check_text text" data-text_count="<?php echo $TPL_V1["text_count"]?>"><?php echo $TPL_V1["cont"]?></textarea>
<?php }else{?>
                            <textarea name="extra_text[<?php echo $TPL_V1["order"]?>]" class="check_text text" data-text_count="<?php echo $TPL_V1["text_count"]?>"><?php echo $TPL_V1["text"]?></textarea>
<?php }?>
<?php }?>
<?php if(!empty($TPL_V1["text_count"])){?>
                        <div class="check_text_count">글자수 제한: <span>0</span> / <?php echo $TPL_V1["text_count"]?></div>
<?php }?>
                    </div>
                </div><!-- .section -->
            </div><!-- .section_area -->
<?php }}?>
        </div><!-- .goods_pic_body -->
    </form>
    <div class="goods_pic_footer">
        <ul>
           <li>
               <a href="#" onclick="return false;" class="button gray" id="TempButton">임시저장</a>
           </li><li>
                <a href="#" onclick="return false;" class="button orange" id="SaveButton">업로드완료</a>
            </li> 
        </ul>
    </div><!-- .goods_pic_footer -->
</div><!-- .goods_pic_wrapper -->

<script>
jQuery(function($) {
    $(document).ready(function() {
        new SimpleBar(document.getElementById('GoodsPicWrapper'));
        $('.check_text').trigger('keyup');
        toggleUploadedFiles();
        checkUploaded();
    });

    // #리스트 보기
    $(document).on('click', '#UploadListButton', function() {
        $('#UploadDetailButton img').attr('src', $('#UploadDetailButton img').attr('src').replace('_on', '_off'));
        $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_off', '_on'));
        $('.section').slideUp('fast');
    });

    // #자세히 보기
    $(document).on('click', '#UploadDetailButton', function() {
        $('#UploadListButton img').attr('src', $('#UploadListButton img').attr('src').replace('_on', '_off'));
        $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_off', '_on'));
        $('.section').slideDown('fast');
    });

    // #타이틀 클릭 시
    $(document).on('click', '.section_area .title', function() {
        $(this).closest('.section_area').find('.section').slideToggle('fast');
    });

    // #탭 메뉴
    $(document).on('click', '#TabMenu > li', function() {
        var menu = $(this).data('menu');
        if(menu == 'all') {
            $('.section_area').each(function() {
                $(this).show();
            });
        }
        else if(menu == 'picture') {
            $('.section_area').each(function() {
                if($(this).data('type') == 'picture1' || $(this).data('type') == 'picture2' || $(this).data('type') == 'picture3') {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
        else if(menu == 'video') {
            $('.section_area').each(function() {
                if($(this).data('type') == 'video') {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
        else if(menu == 'text') {
            $('.section_area').each(function() {
                if($(this).data('type') == 'single' || $(this).data('type') == 'multi') {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
        else if(menu == 'etc') {
            $('.section_area').each(function() {
                if($(this).data('type') == 'default' || $(this).data('type') == 'file') {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
        else if(menu == 'not') {
            $('.section_area').each(function() {
                if($(this).find('input[name="uploaded_extra[]"]').val() == 'false') {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
        $('#TabMenu > li').each(function() {
            $(this).removeClass('on');
        });
        $(this).addClass('on');
    });

    // #글자수 확인
    $(document).on('keyup', '.check_text', function() {
        var text_count = $(this).data('text_count');
        if(text_count) {
            var chk_bytes = fnChkByte($(this).val(), text_count);
            if(chk_bytes[0] === true) {
                $(this).closest('.input').find('.check_text_count > span').text(chk_bytes[1]);
            }
            else {
                $(this).val(chk_bytes[1]);
            }
        }
        checkUploaded();
    });

    // #임시 저장
    $(document).on('click', '#TempButton', function() {
        upsertCont('1');
    });

    // #업로드 완료
    $(document).on('click', '#SaveButton', function() {
        var upload_count = $('input[name=upload_count]').val();
        var uploaded_count = $('input[name=uploaded_count]').val();
        if(upload_count !== uploaded_count) {
            alert('업로드가 완료되지 않았습니다.\n미업로드를 확인해 주시기 바랍니다.');
            $('#TabMenu > li.not_menu').trigger('click');
            return false;
        }
        if(confirm('업로드를 완료 하시면 수정이 불가능 합니다.\n이대로 완료 하시겠습니까?')) {
            upsertCont('2');
            return;
        }
    });

    // #이벤트 파일 업로드
    $(document).on('click', '.goods_pic_body .file', function(e) {
        e.stopPropagation();
        if(!$(e.target).is('input[type=file]')) {
            $(this).find('input[type=file]').trigger('click');
        }
    });

    // #이벤트 드래그 기능
    $(document).on("dragover drop", 'body', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on('dragenter dragover', '.goods_pic_body .file', function(e) {
        $(this).addClass('over');
    });
    $(document).on('dragleave', '.goods_pic_body .file', function(e) {
        $(this).removeClass('over');
    });
    $(document).on('drop', '.goods_pic_body .file', function(e) {
        $(this).removeClass('over');

        var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
        var files = e.target.files || (dt && dt.files);
        if(files) {
            if(files.length < 1) {
                alert('폴더는 업로드 할 수 없습니다.');
            }
            else {
                addFiles(e);
            }
        }
        else {
            alert('업로드 파일이 없습니다.');
        }
        
        return;
    });

    // #이벤트 파일 등록,삭제
    $(document).on('change', '.goods_pic_body .file input[type=file]', addFiles);
    $(document).on('click', '.goods_pic_body .uploaded_files .delete_file', deleteFile);

    // #업로드 확인
    function checkUploaded() {
        var chk_default = 0;
        var chk_extra = 0;
        var uploaded_count = 0;
        // *기본 정보 확인 : 첫번째, 두번째 영역만 입력하면 ok
        var chk_default_count = 0;
        $('.default_area .form > li').each(function() {
            if(($(this).index() == 0 || $(this).index() == 1)) {
                if($(this).find('input[type=text]').val().length > 0) {
                    chk_default_count++;
                }
            }
        });
        // *추가 정보 확인
        $('.extra_area').each(function() {
            var type = $(this).data('type');
            if(type == 'single') {
                if($(this).find('input[type=text]').val().length > 0) {
                    $(this).find("input[name='uploaded_extra[]']").val(true);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('ex', 'check'));
                    chk_extra++;
                }
                else {
                    $(this).find("input[name='uploaded_extra[]']").val(false);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('check', 'ex'));
                }
            }
            else if(type == 'multi') {
                if($(this).find('textarea').val().length > 0) {
                    $(this).find("input[name='uploaded_extra[]']").val(true);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('ex', 'check'));
                    chk_extra++;
                }
                else {
                    $(this).find("input[name='uploaded_extra[]']").val(false);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('check', 'ex'));
                }
            }
            else if(type == 'picture1') {
                var max_count = $(this).find('.max_count').text();
                var upload_file_count = $(this).find('.uploaded_files .list').length;
                if((max_count == 0 && upload_file_count > 0) || (max_count > 0 && max_count == upload_file_count)) {
                    if($(this).find('input[type=text]').val().length > 0) {
                        $(this).find("input[name='uploaded_extra[]']").val(true);
                        $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('ex', 'check'));
                        chk_extra++;
                    }
                }
                else {
                    $(this).find("input[name='uploaded_extra[]']").val(false);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('check', 'ex'));
                }
            }
            else if(type == 'picture2' || type == 'picture3' || type == 'video' || type == 'file') {
                var max_count = $(this).find('.max_count').text();
                var upload_file_count = $(this).find('.uploaded_files .list').length;
                if((max_count == 0 && upload_file_count > 0) || (max_count > 0 && max_count == upload_file_count)) {
                    if($(this).find('textarea').val().length > 0) {
                        $(this).find("input[name='uploaded_extra[]']").val(true);
                        $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('ex', 'check'));
                        chk_extra++;
                    }
                }
                else {
                    $(this).find("input[name='uploaded_extra[]']").val(false);
                    $(this).find('.title .check img').attr('src', $(this).find('.title .check img').attr('src').replace('check', 'ex'));
                }
            }
        });
        // *기본 정보 갯수에 따른 완성 여부
        if(chk_default_count == 2) {
            $('.default_area').find('input[name=uploaded_default]').val(true);
            $('.default_area').find('.title .check img').attr('src', $('.default_area').find('.title .check img').attr('src').replace('ex', 'check'));
            chk_default = 1;
        }
        else {
            $('.default_area').find('input[name=uploaded_default]').val(false);
            $('.default_area').find('.title .check img').attr('src', $('.default_area').find('.title .check img').attr('src').replace('check', 'ex'));
            chk_default = 0;
        }
        // *업로드 갯수
        $('#UploadedCount').text(chk_extra + chk_default);
        $('input[name=uploaded_count').val(chk_extra + chk_default);
    }

    // #내용 저장
    function upsertCont(_status) {
        $('#GoodsPicForm').find('input[name=status]').val(_status);
        var form_data = new FormData($('#GoodsPicForm')[0]);
        $.ajax({
            type : "POST",
            url: "../goods/goods_pic_cont_upsert.php",
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
                if(data.success == true) {
                    if(_status == 1) {
                        location.reload();
                    }
                    else if(_status == 2) {
                        opener.document.location.reload();
                        self.close();
                    }
                }  
                else alert(data.msg);
            },
            err: function(err) {
                alert(err.status);
            }
        });
    }

    // #파일 추가
    function addFiles(e) {
        var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
        var files = e.target.files || (dt && dt.files);
        var $section = $(e.target).closest('.section');
        var orderNo = $('input[name=orderNo]').val();
        var goodsNo = $('input[name=goodsNo]').val();
        var order = $section.find('input[type=file]').data('order');
        var files_arr = Array.prototype.slice.call(files);
        var $uploaded_files = $section.find('.uploaded_files');
        var $current_count = $section.find('.current_count');
        $section.find('input[type=file]').val('');
        
        var form_data = new FormData();
        form_data.append('orderNo', orderNo);
        form_data.append('goodsNo', goodsNo);
        form_data.append('order', order);
        for(var i = 0; i < files_arr.length; i++) {
            form_data.append('files[]', files_arr[i]);
        }
        
        $.ajax({
            type : "POST",
            url: "../goods/goods_pic_file_upload.php",
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if(data.success == true) {
                    var count = 0;
                    var file_list = data.files;
                    var uploaded_html = '';
                    $.each(file_list, function(key, value) {
                        if(value['type'] == 'image') {
                            uploaded_html = '<div class="list" style="background-image:url(' + value['path'] + ');">';
                        }
                        else if(value['type'] == 'video') {
                            uploaded_html = '<div class="list">';
                            uploaded_html += '<div class="file_type">';
                            uploaded_html += '<i class="xi-file-video-o"></i><br>';
                            uploaded_html += '비디오 파일';
                            uploaded_html += '</div>';
                        }
                        else if(value['type'] == 'etc') {
                            uploaded_html = '<div class="list">';
                            uploaded_html += '<div class="file_type">';
                            uploaded_html += '<i class="xi-file-zip-o"></i><br>';
                            uploaded_html += '기타 파일';
                            uploaded_html += '</div>';
                        }
                        uploaded_html += '<a href="#" onclick="return false;" class="delete_file" data-file_no="' + key + '"><i class="xi-close"></i></a>';
                        uploaded_html += '</div>';
                        $uploaded_files.append(uploaded_html);
                        count++;
                    });
                    $current_count.text(parseInt($current_count.text()) + count);
                    toggleUploadedFiles();
                }
                else alert(data.msg);
            },
            err: function(err) {
                alert(err.status);
            }
        });
    }

    // #파일 삭제
    function deleteFile(e) {
        var goodsPicFileNo = $(this).data('file_no');
        var $uploaded_file = $(this).parent();
        var $current_count = $(this).closest('.section_area').find('.current_count');
        
        var form_data = new FormData();
        form_data.append('goodsPicFileNo', goodsPicFileNo);
        
        $.ajax({
            type : "POST",
            url: "../goods/goods_pic_file_delete.php",
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
                if(data.success == true) {
                    $uploaded_file.remove();
                    $current_count.text(parseInt($current_count.text()) - 1);
                    toggleUploadedFiles();
                }
                else alert(data.msg);
            },
            err: function(err) {
                alert(err.status);
            }
        });
    }

    // #업로드 파일 목록 토글
    function toggleUploadedFiles() {
        $('.goods_pic_body .uploaded_files:has(.list)').show();
        $('.goods_pic_body .uploaded_files:not(:has(.list))').hide();
        checkUploaded();
    }

    // #글자수 확인
    function fnChkByte(_str, _max) {
        var maxByte = _max; //최대 입력 바이트 수
        var str = _str;
        var str_len = str.length;
    
        var rbyte = 0;
        var rlen = 0;
        var one_char = "";
        var str2 = "";
    
        for (var i = 0; i < str_len; i++) {
            one_char = str.charAt(i);
    
            if (escape(one_char).length > 4) {
                rbyte += 2; //한글2Byte
            } else {
                rbyte++; //영문 등 나머지 1Byte
            }
    
            if (rbyte <= maxByte) {
                rlen = i + 1; //return할 문자열 갯수
            }
        }
    
        if (rbyte > maxByte) {
            //alert("한글 " + (maxByte / 2) + "자 / 영문 " + maxByte + "자를 초과 입력할 수 없습니다.");
            str2 = str.substr(0, rlen); //문자열 자르기
            //fnChkByte(obj, maxByte);
            return [false, str2];
        } else {
            //document.getElementById('byteInfo').innerText = rbyte;
            return [true, rbyte];
        }
    }
});
</script>