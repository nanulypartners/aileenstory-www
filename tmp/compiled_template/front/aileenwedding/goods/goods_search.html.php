<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/goods_search.html 000009210 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="content">
    <div class="goods_search_cont">
        <strong class="search_text_result">
            <span>&quot;<?php echo gd_htmlspecialchars_slashes($TPL_VAR["goodsData"]["listSearch"]['keyword'])?>&quot;</span> <?php echo __('검색결과')?> <?php echo number_format(gd_isset($TPL_VAR["page"]->recode['total']))?><?php echo __('개')?>

<?php if($TPL_VAR["paycosearchUse"]){?>
            <span class="paycosearch-banner">
            <img src="/data/skin/front/aileenwedding/img/etc/payco_search.png" alt="Powered by PAYCO Search" /></span>
<?php }?>
        </strong>
        <div class="goods_search_box">
            <form name="frmSearch" id="frmSearch" method="get">
                <input type=hidden name=reSearchKeyword[] value="<?php echo $TPL_VAR["goodsData"]["listSearch"]['keyword']?>">
                <input type=hidden name=reSearchKey[] value="<?php echo $TPL_VAR["goodsData"]["listSearch"]['key']?>">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsData"]["listSearch"]['reSearchKeyword'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                <input type=hidden name=reSearchKeyword[] value="<?php echo $TPL_V1?>">
                <input type=hidden name=reSearchKey[] value="<?php echo $TPL_VAR["goodsData"]["listSearch"]['reSearchKey'][$TPL_K1]?>">
<?php }}?>
                <input type=hidden name=sort value="<?php echo $TPL_VAR["sort"]?>">
                <input type=hidden name=pageNum value="<?php echo $TPL_VAR["pageNum"]?>">

<?php if(in_array('keyword',$TPL_VAR["goodsConfig"]["searchType"])){?>
                <div class="search_again_box">
                    <div class="form_element">
                        <input type="checkbox" id="rescan" class="checkbox" name="reSearch" value="y" <?php if($TPL_VAR["goodsData"]["listSearch"]['reSearch']=='y'){?> checked='checked' <?php }?>>
                        <label for="rescan" class="check_s <?php if($TPL_VAR["goodsData"]["listSearch"]['reSearch']=='y'){?>on<?php }?>"><?php echo __('결과 내 재검색')?></label>
                    </div>
<?php if($TPL_VAR["paycosearchUse"]==false){?>
                    <select name="key" class="chosen-select">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsData"]["listSearch"]['combineSearch'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                        <option value="<?php echo $TPL_K1?>"<?php if($TPL_K1==$TPL_VAR["goodsData"]["listSearch"]['key']){?>selected="selected"
<?php }?>><?php echo $TPL_V1?>

                        </option>
<?php }}?>
                    </select>
<?php }?>
                    <div class="keyword-div">
                        <input type="text" name="keyword" class="keyword_input" autocomplete="off">
                        <?php echo includeWidget('proc/_autocomplete_search.html','mode','detail')?>

                    </div>
                    <button type="submit" class="btn_goods_search"><em><?php echo __('검색')?></em></button>
                </div>
<?php }else{?>
                <input type="hidden" name="key" value="all" />
                <input type="hidden" name="keyword" value="<?php echo $TPL_VAR["goodsData"]["listSearch"]['keyword']?>" />
<?php }?>
                <!-- //search_again_box -->

<?php if($TPL_VAR["hitKeywordConfig"]["keyword"]){?>
                <div class="search_hot_list">
                    <strong class="search_hot_tit"><?php echo __('인기검색어')?></strong>
                    <ul>
<?php if((is_array($TPL_R1=$TPL_VAR["hitKeywordConfig"]["keyword"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                        <li><a href="./goods_search.php?keyword=<?php echo urlencode($TPL_V1)?>"><span><?php echo $TPL_V1?></span></a></li>
<?php }}?>
                    </ul>
                </div>
<?php }?>
                <!-- //search_hot_list -->
                <div id="detailSearch" class="dn"></div>
            </form>
        </div>
        <!-- //goods_search_box -->

        <div class="goods_pick_list">
            <div class="pick_list_box">
                <ul class="pick_list">
                    <li>
                        <input type="radio" id="sort1" class="radio" name="sort" value="">
                        <label for="sort1"><?php echo __('추천순')?></label>
                    </li>
                    <li>
                        <input type="radio" id="sort2" class="radio" name="sort" value="orderCnt desc,g.regDt desc">
                        <label for="sort2"><?php echo __('판매인기순')?></label>
                    </li>
                    <li>
                        <input type="radio" id="sort3" class="radio" name="sort" value="goodsPrice asc,g.regDt desc">
                        <label for="sort3"><?php echo __('낮은가격순')?></label>
                    </li>
                    <li>
                        <input type="radio" id="sort4" class="radio" name="sort" value="goodsPrice desc,g.regDt desc">
                        <label for="sort4"><?php echo __('높은가격순')?></label>
                    </li>
                    <li>
                        <input type="radio" id="sort5" class="radio" name="sort" value="reviewCnt desc,g.regDt desc">
                        <label for="sort5"><?php echo __('상품평순')?></label>
                    </li>
                    <li>
                        <input type="radio" id="sort6" class="radio" name="sort" value="g.regDt desc">
                        <label for="sort6"><?php echo __('등록일순')?></label>
                    </li>
                </ul>
                <div class="choice_num_view">
                    <select class="chosen-select" name="pageNum">
                        !--<?php if((is_array($TPL_R1=$TPL_VAR["goodsData"]["multiple"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                        <option value="<?php echo $TPL_V1?>"  <?php if($TPL_VAR["pageNum"]==$TPL_V1){?>selected='selected'<?php }?>><?php echo $TPL_V1?><?php echo __('개씩보기')?></option>
<?php }}?>
                    </select>
                </div>
                <!-- //choice_num_view -->
            </div>
            <!-- //pick_list_box -->
        </div>
        <!-- //goods_pick_list -->

        <div class="goods_list">
            <!-- //goods_list_tit -->
            <div class="goods_list_cont">
<?php $this->print_("goodsTemplate",$TPL_SCP,1);?>

            </div>
            <!-- //goods_list_cont -->
        </div>
        <!-- //goods_list -->

        <div class="pagination">
            <?php echo $TPL_VAR["page"]->getPage()?>

        </div>
        <!-- //pagination -->
    </div>
    <!-- //goods_search_cont -->
</div>
<!-- //content -->


<script type="text/javascript">
    $(document).ready(function () {

        $('select[name=pageNum]').change(function() {
            $('form[name=frmSearch] input[name="pageNum"]').val($(this).val());
            $('form[name=frmSearch]').submit();
        });

        $('input[name=sort]').click(function() {
            $('form[name=frmSearch] input[name="sort"]').val($(this).val());
            $('form[name=frmSearch]').submit();
        });

        $('input[name="sort"][value="<?php echo $TPL_VAR["sort"]?>"]').prop("checked","checked")
        $('input[name="sort"][value="<?php echo $TPL_VAR["sort"]?>"]').next().addClass('on');



        $('input[name=reSearch]').click(function() {
            if($(this).is(":checked") == true)
            {
                $('form[name=frmSearch] input[name="keyword"]').val('');
            }

        });

        $('#btnSearchSubmit').click(function() {
            $("#frmSearch").submit();
        });


        $("#frmSearch").validate({
            submitHandler: function (form) {

                var $orginal = $('#frmSearchDetail');

                if($orginal.length)
                {
                    var $cloned = $orginal.clone();
                    var $originalSelects = $orginal.find('select');
                    $cloned.find('select').each(function(index, item) {
                        $(item).val( $originalSelects.eq(index).val() );

                    });

                    var $originalTextareas = $orginal.find('textarea');
                    $cloned.find('textarea').each(function(index, item) {
                        $(item).val($originalTextareas.eq(index).val());
                    });


                    $cloned.appendTo('#detailSearch');
                }

                form.submit();
            }
        });

    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>