<?php /* Template_ 2.2.7 2020/04/14 03:28:00 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/layer_coupon_apply.html 000013402 */ 
if (is_array($TPL_VAR["memberCouponArrData"])) $TPL_memberCouponArrData_1=count($TPL_VAR["memberCouponArrData"]); else if (is_object($TPL_VAR["memberCouponArrData"]) && in_array("Countable", class_implements($TPL_VAR["memberCouponArrData"]))) $TPL_memberCouponArrData_1=$TPL_VAR["memberCouponArrData"]->count();else $TPL_memberCouponArrData_1=0;?>
<div class="layer_wrap_cont">
    <div class="ly_tit">
        <h4>
            <?php echo __('쿠폰 적용하기')?>

<?php if(gd_is_login()){?>
            <span><?php echo __('선택한 상품에 적용가능한 총')?> <strong><?php echo $TPL_memberCouponArrData_1?><?php echo __('개%s의 보유쿠폰이 있습니다.','</strong>')?></span>
<?php }?>
        </h4>
    </div>
    <div class="ly_cont">
        <div class="scroll_box">
<?php if(gd_is_login()){?>
            <div class="top_table_type">
                <form name="frmCouponApply" method="post">
                    <table>
                        <colgroup>
                            <col style="width:50px;" />
                            <col style="width:200px" />
                            <col style="width:200px" />
                            <col />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th><?php echo __('쿠폰')?></th>
                                <th><?php echo __('사용조건')?></th>
                                <th><?php echo __('사용기한')?></th>
                            </tr>
                        </thead>
                        <tbody>
<?php if($TPL_memberCouponArrData_1){$TPL_I1=-1;foreach($TPL_VAR["memberCouponArrData"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
                            <tr>
                                <td>
                                    <span class="form_element">
<?php if($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAlertMsg"][$TPL_V1["memberCouponNo"]]=='LIMIT_MIN_PRICE'){?>
                                        <input type="checkbox" id="check<?php echo $TPL_I1?>" class="checkbox" disabled="disabled">
                                        <label class="check_dis_s single" for="check<?php echo $TPL_I1?>"><?php echo __('선택')?></label>
<?php }else{?>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                        <input type="checkbox" id="check<?php echo $TPL_I1?>" name="memberCouponNo[]" class="checkbox" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                        <input type="checkbox" id="check<?php echo $TPL_I1?>" name="memberCouponNo[]" class="checkbox" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                        <input type="checkbox" id="check<?php echo $TPL_I1?>" name="memberCouponNo[]" class="checkbox" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }?>
                                        <label class="check_s single" for="check<?php echo $TPL_I1?>"><?php echo __('선택')?></label>
<?php }?>
                                    </span>
                                </td>
                                <td class="td_left">
                                    <label for="check<?php echo $TPL_I1?>">
                                        <strong>
                                            <?php echo gd_currency_symbol()?>

<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                            <b><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]])?></b>
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                            <b><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]])?></b>
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                            <b><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]])?></b>
<?php }?>
                                            <?php echo gd_currency_string()?>

                                            <span><?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponKindType"]?></span>
                                        </strong>
                                        <span class="text_info"><?php echo $TPL_V1["couponNm"]?></span>
                                    </label>
                                </td>
                                <td class="td_left">
                                    <span class="text_info">
<?php if($TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMaxBenefit"]){?>
                                        - <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMaxBenefit"]?><br />
<?php }?>
<?php if($TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMinOrderPrice"]){?>
                                        - <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMinOrderPrice"]?><br />
<?php }?>
                                        - <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponApplyDuplicateType"]?>

                                    </span>
                                </td>
                                <td>
                                    <span><?php echo $TPL_V1["memberCouponEndDate"]?></span>
                                </td>
                            </tr>
<?php }}?>
                        </tbody>
                    </table>
                </form>
            </div>
<?php }else{?>
            <div class="coupon_down_txt"><p><?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?></p></div>
<?php }?>
        </div>
        <!-- //scroll_box -->
        <div class="coupon_total_box">
            <dl>
                <dt><?php echo __('총 할인금액')?></dt>
                <dd><?php echo gd_currency_symbol()?><strong id="couponSalePrice">0</strong><?php echo gd_currency_string()?></dd>
            </dl>
            <dl>
                <dt><?php echo __('총 적립금액')?></dt>
                <dd><?php echo gd_currency_symbol()?><strong id="couponAddPrice">0</strong><?php echo gd_currency_string()?></dd>
            </dl>
        </div>
        <!-- //coupon_total_box -->
        <div class="btn_center_box button_area">
<?php if(gd_is_login()){?>
            <button class="btn_ly_cancel layer_close button inline gray"><strong><?php echo __('취소')?></strong></button>
            <button id="btnCouponApply" class="btn_ly_coupon_apply button inline orange"><strong><?php echo __('쿠폰 적용')?></strong></button>
<?php }else{?>
            <a href="../member/join_method.php" class="btn_ly_join button inline black"><strong>회원가입</strong></a>
            <a href="../member/login.php" class="btn_ly_login button inline orange"><strong>로그인</strong></a>
<?php }?>
        </div>
    </div>
    <!-- //ly_cont -->
    <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
</div>
<!-- //layer_wrap_cont -->
<?php if(gd_is_login()!==false){?>
<script type="text/javascript">
    <!--
    $(document).ready(function () {
        // @todo 적용한 쿠폰이 중복불가 시 쿠폰 변경시 disable 처리 부분 필요
        $('input:checkbox[name="memberCouponNo[]"]').click(function (e) {
            if (($(this).prop('checked') == true) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).attr("checked", false);
                    $(this).next('label').removeClass('on');
                    $(this).attr('disabled', 'disabled');
                });
            } else if (($(this).prop('checked') == false) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).removeAttr('disabled', 'disabled');
                });
            }
            var objCouponPrice = gd_coupon_price_sum();
            $('#couponSalePrice').text(numeral(objCouponPrice.sale).format());
            $('#couponAddPrice').text(numeral(objCouponPrice.add).format());
        });

        $('#btnCouponApply').click(function (e) {
            var couponPaymentTypeCheck = 'n';
            var couponApplyNoArr = [];
            $('input:checkbox[name="memberCouponNo[]"]:checked').each(function (index) {
                couponApplyNoArr[index] = $(this).val();
                // 결제방식제한쿠폰체크
                if ($(this).attr('data-paytype') == 'bank') {
                    couponPaymentTypeCheck = 'y';
                }
            });
            if (couponApplyNoArr.length > 0){
                var couponApplyNoString = couponApplyNoArr.join('<?php echo INT_DIVISION?>');
                $('#option_display_item_<?php echo $TPL_VAR["optionKey"]?> input:hidden[name="couponApplyNo[]"]').val(couponApplyNoString);
                var objCouponPrice = gd_coupon_price_sum();
                $('#option_display_item_<?php echo $TPL_VAR["optionKey"]?> input:hidden[name="couponSalePriceSum[]"]').val(objCouponPrice.sale);
                $('#option_display_item_<?php echo $TPL_VAR["optionKey"]?> input:hidden[name="couponAddPriceSum[]"]').val(objCouponPrice.add);
                var couponApplyHtml = "<img class=\"btn_coupon_cancel\" src=\"/data/skin/front/aileenwedding/img/common/btn/btn_coupon_cancel.png\" data-key=\"<?php echo $TPL_VAR["optionKey"]?>\" alt=\"<?php echo __('쿠폰취소')?>\" /> <a href=\"#lyCouponApply\" class=\"btn_open_layer\" data-key=\"<?php echo $TPL_VAR["optionKey"]?>\"><img src=\"/data/skin/front/aileenwedding/img/common/btn/btn_coupon_change.png\" alt=\"<?php echo __('쿠폰변경')?>\" />";
                $('#coupon_apply_<?php echo $TPL_VAR["optionKey"]?>').html(couponApplyHtml);
                if($('#cart_tab_coupon_apply_<?php echo $TPL_VAR["optionKey"]?>').length) $('#cart_tab_coupon_apply_<?php echo $TPL_VAR["optionKey"]?>').html(couponApplyHtml);
                gd_benefit_calculation();
            } else {
                gd_coupon_cancel('<?php echo $TPL_VAR["optionKey"]?>');
            }
            if (couponPaymentTypeCheck == 'y') {
                $('.payco_pay').addClass('dn');
                $('.naver_pay').addClass('dn');
            } else {
                $('.payco_pay').removeClass('dn');
                $('.naver_pay').removeClass('dn');
            }

            gd_open_layer();
            gd_bind_coupon_cancel();
            gd_close_layer();
        });
        gd_coupon_apply_setting();
    });

    // 쿠폰 적용 내용 초기화 (설정)
    function gd_coupon_apply_setting() {
        var couponApplyNoString = '<?php echo $TPL_VAR["couponApplyNo"]?>';
        var couponApplyNoArr = new Array();
        if (couponApplyNoString) {
            var couponApplyNoArr = couponApplyNoString.split('<?php echo INT_DIVISION?>');
        }
        $.each(couponApplyNoArr, function (index) {
            $('input:checkbox[name="memberCouponNo[]"][value="' + couponApplyNoArr[index] + '"]').trigger('click');
        });
        var objCouponPrice = gd_coupon_price_sum();
        $('#couponSalePrice').text(numeral(objCouponPrice.sale).format());
        $('#couponAddPrice').text(numeral(objCouponPrice.add).format());
    }

    // 선택 쿠폰 금액 계산
    function gd_coupon_price_sum() {
        var salePrice = 0;
        var addPrice = 0;
        $('input:checkbox[name="memberCouponNo[]"]:checked').each(function (index) {
            if ($(this).data('type') == 'sale') {
                salePrice += parseFloat($(this).data('price'));
            } else if ($(this).data('type') == 'add') {
                addPrice += parseFloat($(this).data('price'));
            }
        });
        var couponPrice = {
            'sale': salePrice,
            'add': addPrice
        };
        return couponPrice;
    }
    //-->
</script>
<?php }?>