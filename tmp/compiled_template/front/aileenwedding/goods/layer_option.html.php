<?php /* Template_ 2.2.7 2020/04/17 03:56:18 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/layer_option.html 000038788 */ ?>
<style>
    .chosen-container .chosen-results {
        max-height:100px;
    }
</style>
<script type="text/javascript" src="/data/skin/front/aileenwedding/js/gd_goods_view.js"></script>
<script type="text/javascript">
    var goodsViewLayerController = new gd_goods_view();
    $(document).on('keydown focusout', 'input[name^=goodsCnt]', function(e){
        $(this).val($(this).val().replace(/[^0-9\-]/g,""));
    });
</script>

<div class="option_layer_cont">
    <form name="frmViewLayer" id="frmViewLayer" method="post">
        <input type="hidden" name="brandCd" value="<?php echo $TPL_VAR["goodsView"]['brandCd']?>" />
        <input type="hidden" name="cateCd" value="<?php echo $TPL_VAR["goodsView"]['cateCd']?>" />
        <input type="hidden" name="cartMode" value="" />
        <input type="hidden" name="goodsDiscount" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscount']?>" />
        <input type="hidden" name="goodsDiscountFl" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountFl']?>" />
        <input type="hidden" name="goodsDiscountUnit" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountUnit']?>" />
        <input type="hidden" id="goodsOptionCnt" value="1" />
        <input type="hidden" name="mileageFl" value="<?php echo $TPL_VAR["goodsView"]['mileageFl']?>" />
        <input type="hidden" name="mileageGoods" value="<?php echo $TPL_VAR["goodsView"]['mileageGoods']?>" />
        <input type="hidden" name="mileageGoodsUnit" value="<?php echo $TPL_VAR["goodsView"]['mileageGoodsUnit']?>" />
        <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
        <input type="hidden" name="selectGoodsFl" value="<?php echo $TPL_VAR["selectGoodsFl"]?>" />
        <input type="hidden" id="set_add_goods_price" name="set_add_goods_price" value="0" />
        <input type="hidden" name="set_coupon_dc_price" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>" />
        <input type="hidden" id="set_dc_price" value="0" />
        <input type="hidden" id="set_goods_price" name="set_goods_price" value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />
        <input type="hidden" id="set_goods_fixedPrice" name="set_goods_fixedPrice" value="<?php echo gd_isset($TPL_VAR["goodsView"]['fixedPrice'], 0)?>" />
        <input type="hidden" name="set_goods_mileage" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsMileageBasic'], 0)?>" />
        <input type="hidden" name="set_goods_stock" value="<?php echo gd_isset($TPL_VAR["goodsView"]['stockCnt'], 0)?>" />
        <input type="hidden" id="set_goods_total_price" name="set_goods_total_price" value="0" />
        <input type="hidden" id="set_option_price" name="set_option_price" value="0" />
        <input type="hidden" id="set_option_text_price" name="set_option_text_price" value="0" />
        <input type="hidden" id="set_total_price" name="set_total_price" value="0" />
        <input type="hidden" name="taxFreeFl" value="<?php echo $TPL_VAR["goodsView"]['taxFreeFl']?>" />
        <input type="hidden" name="taxPercent" value="<?php echo $TPL_VAR["goodsView"]['taxPercent']?>" />
        <input type="hidden" name="orderPossible" value="<?php echo $TPL_VAR["goodsView"]['orderPossible']?>" />
        <input type="hidden" name="useBundleGoods" value="1" />
        <input type="hidden" name="mainSno" value="<?php echo $TPL_VAR["mainSno"]?>" />

        <h4><?php echo __('옵션선택')?></h4>
        <div class="option_layer_scroll">
            <div class="option_tit_box">
                <dl>
                    <dt><?php echo $TPL_VAR["goodsView"]['image']['magnify']['thumb'][ 0]?></dt>
                    <dd>
                        <strong><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></strong>
<?php if(gd_isset($TPL_VAR["goodsView"]['shortDescription'])){?>
                        <span><?php echo $TPL_VAR["goodsView"]['shortDescription']?></span>
<?php }?>
                    </dd>
                </dl>
            </div>
            <!-- //option_tit_box -->
            <div class="option_select_box">
                <div class="option_chosen">
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
                    <dl>
                        <dt><?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='one'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?><?php echo $TPL_VAR["goodsView"]['optionName']?><?php }else{?><?php echo __('옵션 선택')?><?php }?></dt>
                        <dd>
                            <select name="optionSnoInput" class="chosen-select" onchange="goodsViewLayerController.option_price_display(this);"<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>>
                                <option value="">
                                    =
<?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='many'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?><?php echo $TPL_VAR["goodsView"]['optionName']?><?php }else{?><?php echo __('옵션')?><?php }?>
                                    : <?php echo __('가격')?>

<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>: <?php echo __('재고')?><?php }?>
                                    =
                                </option>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['option'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["optionViewFl"]=='y'){?>
                                <option <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_V1["optionImage"]){?>data-img-src="<?php echo $TPL_V1["optionImage"]?>"<?php }else{?>data-img-src="blank"<?php }?><?php }?> value="<?php echo $TPL_V1["sno"]?><?php echo INT_DIVISION?><?php echo gd_global_money_format($TPL_V1["optionPrice"],false)?><?php echo INT_DIVISION?><?php echo $TPL_V1["mileage"]?><?php echo INT_DIVISION?><?php echo $TPL_V1["stockCnt"]?><?php echo STR_DIVISION?><?php echo $TPL_V1["optionValue"]?><?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["optionSellFl"]=='t')){?><?php echo INT_DIVISION?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?>]<?php }?><?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]!=''){?>[<?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?>]<?php }?>"<?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]<$TPL_VAR["goodsView"]['minOrderCnt'])||$TPL_V1["optionSellFl"]=='n'||$TPL_V1["optionSellFl"]=='t'){?> disabled="disabled"<?php }?><?php if(gd_isset($TPL_VAR["optionInfo"]['optionSno'])&&$TPL_VAR["optionInfo"]['optionSno']==$TPL_V1["sno"]){?> selected='selected'<?php }?>>
                                    <?php echo $TPL_V1["optionValue"]?>

<?php if(gd_isset($TPL_V1["optionPrice"])!='0'){?> : <?php echo gd_global_currency_symbol()?><?php if(gd_isset($TPL_V1["optionPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V1["optionPrice"])?><?php echo gd_global_currency_string()?><?php }?>
<?php if($TPL_V1["optionSellFl"]=='t'){?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?>]
<?php }elseif(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]<$TPL_VAR["goodsView"]['minOrderCnt'])||$TPL_V1["optionSellFl"]=='n'){?>[<?php echo $TPL_VAR["optionSoldOutCode"]['n']?>]
<?php }else{?>
<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])&&$TPL_VAR["goodsView"]['stockFl']=='y'){?> : <?php echo number_format($TPL_V1["stockCnt"])?><?php echo __('개')?>

<?php }?>
<?php }?>
<?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]!=''){?>[<?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?>]
<?php }?>
                                </option>
<?php }?>
<?php }}?>
                            </select>
                        </dd>
                    </dl>
                    <!-- //option_chosen (s) -->
<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionName'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?>
                    <input type="hidden" name="optionSnoInput" value="<?php if($TPL_VAR["optionInfo"]['optionSnoText']){?><?php echo $TPL_VAR["optionInfo"]['optionSnoText']?><?php }?>" />
                    <input type="hidden" name="optionCntInput" value="<?php echo $TPL_S1?>" />
<?php }?>
                    <dl>
                        <dt><?php echo $TPL_V1?></dt>
                        <dd>
                            <select name="optionNo_<?php echo $TPL_I1?>" class="chosen-select" onchange="goodsViewLayerController.option_select(this,'<?php echo $TPL_I1?>', '<?php echo gd_isset($TPL_VAR["goodsView"]['optionName'][($TPL_I1+ 1)])?>','<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>y<?php }else{?>n<?php }?>');"<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'||$TPL_I1> 0){?> disabled="disabled"<?php }?>>
                            <option value="">
                                =
<?php if($TPL_I1== 0){?><?php echo $TPL_V1?> <?php echo __('선택')?>

<?php }else{?><?php echo __('%s을 먼저 선택해 주세요',$TPL_VAR["goodsView"]['optionName'][($TPL_I1- 1)])?>

<?php }?>
                                =
                            </option>
<?php if($TPL_I1== 0){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['optionDivision'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                            <option <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]){?> data-img-src="<?php echo $TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]?>"<?php }else{?>data-img-src="blank"<?php }?><?php }?> value="<?php echo $TPL_V2?>">
                                <?php echo $TPL_V2?>

                            </option>
<?php }}?>
<?php }?>
                            </select>
                        </dd>
                    </dl>
                    <div id="iconImage_<?php echo $TPL_I1?>" class="option_icon"></div>
<?php }}?>
<?php }?>
<?php }?>
                    <!-- //option_chosen (d) -->
<?php if($TPL_VAR["goodsView"]['optionTextFl']=='y'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionText'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                    <dl>
<?php if($TPL_I1== 0){?>
                        <input type="hidden" id="optionTextCnt" value="<?php echo $TPL_S1?>" />
<?php }?>
                        <dt>
                            <input type="hidden" name="optionTextMust_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["mustFl"]?>" />
                            <input type="hidden" name="optionTextLimit_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["inputLimit"]?>" />
                            <span class="optionTextNm_<?php echo $TPL_I1?>"><?php echo $TPL_V1["optionName"]?><?php if($TPL_V1["mustFl"]=='y'){?><em>(<?php echo __('필수')?>)</em><?php }?></span>
                        </dt>
                        <dd class="optionTextDisplay<?php echo $TPL_V1["sno"]?>">
                            <input type="hidden" name="optionTextSno_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["sno"]?>" />
                            <input type="text" name="optionTextInput_<?php echo $TPL_I1?>" class="text" data-sno="<?php echo $TPL_V1["sno"]?>" onchange="goodsViewLayerController.option_text_select(this)" placeholder="<?php echo $TPL_V1["inputLimit"]?><?php echo __('글자를 입력하세요.')?>" maxlength="<?php echo $TPL_V1["inputLimit"]?>" size="25" value=""<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>/>
                            <input type="hidden" value="<?php echo $TPL_V1["addPrice"]?>" />
<?php if($TPL_V1["addPrice"]!= 0){?>
                            <span class="option_msg">※ <?php echo __('작성시')?> <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["addPrice"])?><?php echo gd_global_currency_string()?> <?php echo __('추가')?></span>
<?php }?>
                        </dd>
                    </dl>
<?php }}?>
<?php }?>
                    <!-- //option_chosen (optionText) -->
<?php if($TPL_VAR["goodsView"]['addGoods']){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['addGoods'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                    <dl <?php if($TPL_K1=='0'){?>class="add"<?php }?>>
                        <dt>
                            <?php echo $TPL_V1["title"]?>

<?php if($TPL_V1["mustFl"]=='y'){?>
                            <em>(<?php echo __('필수')?>)</em>
                            <input type="hidden" name="addGoodsInputMustFl[]" value="<?php echo $TPL_K1?>" />
<?php }?>
                        </dt>
                        <dd>
                            <select name="addGoodsInput<?php echo $TPL_K1?>" class="chosen-select" data-key="<?php echo $TPL_K1?>" onchange="goodsViewLayerController.add_goods_select(this)"<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>>
                                <option value=""><?php echo __('추가상품')?></option>
<?php if((is_array($TPL_R2=$TPL_V1["addGoodsList"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                <option <?php if($TPL_V1["addGoodsImageFl"]=='y'){?><?php if($TPL_V2["imageSrc"]){?> data-img-src="<?php echo $TPL_V2["imageSrc"]?>"<?php }else{?> data-img-src="blank"<?php }?><?php }?> value="<?php echo $TPL_V2["addGoodsNo"]?><?php echo INT_DIVISION?><?php echo $TPL_V2["goodsPrice"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["goodsNm"]?>(<?php echo $TPL_V2["optionNm"]?>)<?php echo STR_DIVISION?><?php echo rawurlencode(gd_html_add_goods_image($TPL_V2["addGoodsNo"],$TPL_V2["imageNm"],$TPL_V2["imagePath"],$TPL_V2["imageStorage"], 30,$TPL_V2["goodsNm"],'_blank'))?><?php echo STR_DIVISION?><?php echo $TPL_K1?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockUseFl"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockCnt"]?>"<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> disabled="disabled"<?php }?>>
                                    <?php echo $TPL_V2["goodsNm"]?> (<?php echo $TPL_V2["optionNm"]?><?php if(gd_isset($TPL_V2["goodsPrice"])!='0'){?> / <?php echo gd_global_currency_symbol()?><?php if(gd_isset($TPL_V2["goodsPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?><?php echo gd_global_currency_string()?><?php }?><?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> / <?php echo __('품절')?><?php }?>)
                                </option>
<?php }}?>
                            </select>
                        </dd>
                    </dl>
<?php }}?>
<?php }?>
                </div>
                <!-- //option_chosen (addGoods) -->
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
                <div class="option_total_display_area item_choice_list option_list">
                    <table class="option_display_area" border="0" cellpadding="0" cellspacing="0">
                        <colgroup>
                            <col width="380px" />
                            <col>
                            <col width="80px" />
                            <col width="40px" />
                        </colgroup>
                    </table>
                </div>
<?php }else{?>
                <div class="item_choice_list option_list">
                    <table class="option_display_area" border="0" cellspacing="0" cellpadding="0">
                        <colgroup>
                            <col width="380px" />
                            <col>
                            <col width="80px" />
                            <col width="40px" />
                        </colgroup>
                        <tbody id="option_display_item_0" class="option_display_item_0" >
                            <tr class="check optionKey_0">
                                <td class="cart_prdt_name">
                                    <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>" />
                                    <input type="hidden" name="optionSno[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['sno'])?>" />
                                    <input type="hidden" name="goodsPriceSum[]" value="0" />
                                    <input type="hidden" name="addGoodsPriceSum[]" value="0" />
                                    <div class="cart_tit_box"><strong class="cart_tit"><span><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></span></strong></div>
                                    <span id="option_text_display_0"></span>
                                </td>
                                <td>
                                    <span class="count">
                                        <span class="goods_qty">
                                            <input type="text" name="goodsCnt[]" class="text goodsCnt_0" title="<?php echo __('수량')?>" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsCnt'],$TPL_VAR["goodsView"]['minOrderCnt'])?>" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsCnt'],$TPL_VAR["goodsView"]['minOrderCnt'])?>" data-stock="<?php echo $TPL_VAR["goodsView"]['totalStock']?>" data-key="0" onchange="goodsViewLayerController.input_count_change(this, '1');return false;">
                                            <span>
                                                <button type="button" class="up goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?>0"><i class="xi-caret-up-min"></i></button>
                                                <button type="button" class="down goods_cnt" title="<?php echo __('감소')?>" value="dn<?php echo STR_DIVISION?>0"><i class="xi-caret-down-min"></i></button>
                                            </span>
                                        </span>
                                    </span>
                                </td>
                                <td class="item_choice_price">
                                    <input type="hidden" name="optionPriceSum[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0)?>" />
                                    <input type="hidden" name="option_price_0" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'])?>" />
                                    <?php echo gd_global_currency_symbol()?><strong class="option_price_display_0"><?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0),false)?></strong><?php echo gd_global_currency_string()?>

                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
<?php }?>
                <!-- //option_list -->
            </div>
            <!-- //option_select_box -->
        </div>
        <!-- //option_layer_scroll -->
        <div class="btn_box button_area">
<?php if($TPL_VAR["goodsView"]['orderPossible']=='y'){?>
            <button type="button" class="btn_cancel button inline gray"><span><?php echo __('취소')?></span></button>
            <button type="button" class="btn_confirm js_btn_cart button inline orange"><span><?php echo __('확인')?></span></button>
<?php }else{?>
            <button class="btn_add_soldout button inline black" disabled="disabled"><?php echo __('구매 불가')?></button>
<?php }?>
        </div>
        <!-- //btn_box -->
        <span class="layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="닫기"/></span>
    </form>
    <!-- //frmViewLayer -->
</div>
<!-- //option_layer_cont -->

<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/chosen-imageselect/src/ImageSelect.jquery.js"></script>
<link type="text/css" rel="stylesheet" href="/data/skin/front/aileenwedding/js/jquery/chosen-imageselect/src/ImageSelect.css" />
<script type="text/javascript">
    <!--
    var goodsTotalCnt;
    var goodsOptionCnt = [];
    $(document).ready(function() {
        var parameters = {
            'setTemplate' : 'Layer',
            'setControllerName' : goodsViewLayerController,
            'setOptionFl' : '<?php echo $TPL_VAR["goodsView"]["optionFl"]?>',
            'setOptionTextFl' : '<?php echo $TPL_VAR["goodsView"]["optionTextFl"]?>',
            'setOptionDisplayFl' : '<?php echo $TPL_VAR["goodsView"]["optionDisplayFl"]?>',
            'setAddGoodsFl'	: '<?php if(is_array($TPL_VAR["goodsView"]["addGoods"])){?>y<?php }else{?>n<?php }?>',
            'setIntDivision' : '<?php echo INT_DIVISION?>',
            'setStrDivision' : '<?php echo STR_DIVISION?>',
            'setMileageUseFl' : '<?php echo $TPL_VAR["mileageData"]["useFl"]?>',
            'setCouponUseFl' : '<?php echo $TPL_VAR["couponUse"]?>',
            'setMinOrderCnt' : '<?php echo $TPL_VAR["goodsView"]["minOrderCnt"]?>',
            'setMaxOrderCnt' : '<?php echo $TPL_VAR["goodsView"]["maxOrderCnt"]?>',
            'setStockFl' : '<?php echo gd_isset($TPL_VAR["goodsView"]["stockFl"])?>',
            'setSalesUnit' : '<?php echo gd_isset($TPL_VAR["goodsView"]["salesUnit"], 1)?>',
            'setDecimal' : '<?php echo $TPL_VAR["currency"]["decimal"]?>',
            'setGoodsPrice' : '<?php echo gd_isset($TPL_VAR["goodsView"]["goodsPrice"], 0)?>',
            'setGoodsNo' : '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>',
            'setMileageFl' : ' <?php echo $TPL_VAR["goodsView"]["mileageFl"]?>',
            'setGoodsNm': "<?php echo $TPL_VAR["goodsView"]['goodsNm']?>",
            'setImage': "<?php echo $TPL_VAR["goodsView"]['social']?>",
            'setFixedSales' : "<?php echo $TPL_VAR["goodsView"]['fixedSales']?>",
            'setFixedOrderCnt' : "<?php echo $TPL_VAR["goodsView"]['fixedOrderCnt']?>",
            'setOptionPriceFl' : "<?php echo $TPL_VAR["optionPriceFl"]?>"
        };

        goodsViewLayerController.init(parameters);

<?php if($TPL_VAR["goodsView"]['optionFl']=='n'){?>
        goodsViewLayerController.goods_calculate('#frmViewLayer', 1, 0, "<?php echo gd_isset($TPL_VAR["goodsView"]['goodsCnt'],$TPL_VAR["goodsView"]['minOrderCnt'])?>");
<?php }?>

        $('#frmViewLayer button.goods_cnt').on('click', function() {
            goodsViewLayerController.count_change(this, 1);
        });

        $('button.add_goods_cnt').on('click', function() {
            goodsViewLayerController.count_change(this);
        });

        var salesUnit = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['salesUnit'], 1)?>");
        var minOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['minOrderCnt'], 1)?>");
        var maxOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['maxOrderCnt'], 0)?>");
        $('.js_btn_cart').on('click', function() {
            <?php echo $TPL_VAR["customScript"]?>

<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
            var goodsInfo = $('#frmViewLayer input[name*="optionSno[]"]').length;
<?php }else{?>
            var goodsInfo = $('#frmViewLayer input[name="optionSnoInput"]').val();
<?php }?>

            if (goodsInfo == '') {
                alert("<?php echo __('가격 정보가 없거나 옵션이 선택되지 않았습니다!')?>");
                return false;
            }

<?php if(gd_isset($TPL_VAR["goodsView"]['optionTextFl'])=='y'){?>
            if (!goodsViewLayerController.option_text_valid("#frmViewLayer")) {
                alert("<?php echo __('입력 옵션을 확인해주세요.')?>");
                return false;
            }
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
            if (!goodsViewLayerController.add_goods_valid("#frmViewLayer")) {
                alert("<?php echo __('필수 추가 상품을 확인해주세요.')?>");
                return false;
            }
<?php }?>

            var submitFl = true;
            if (isNaN(goodsTotalCnt)) goodsTotalCnt = 0;
<?php if($TPL_VAR["goodsView"]['fixedSales']=='goods'){?>
            var perSalesCnt = goodsTotalCnt % salesUnit;

            if (perSalesCnt !== 0) {
                alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                submitFl = false;
            }
<?php }else{?>
            for (i in goodsOptionCnt) {
                if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                if (perSalesCnt !== 0) {
                    alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                    submitFl = false;
                    break;
                }
            }
<?php }?>

            if (submitFl == true) {
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
                if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                    alert(__('최소 %s개 이상 구매가능합니다.', minOrderCnt));
                    submitFl = false;
                } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                    alert(__('최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                    submitFl = false;
                }
<?php }else{?>
                for (i in goodsOptionCnt) {
                    if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                    var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                    if (minOrderCnt > 1 && goodsOptionCnt[i] < minOrderCnt) {
                        alert(__('최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                        break;
                    } else if (maxOrderCnt > 0 && goodsOptionCnt[i] > maxOrderCnt) {
                        alert(__('최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                        break;
                    }
                }
<?php }?>
            }

            $('#optionViewLayer').find("input[name='goodsNo[]']").val("<?php echo $TPL_VAR["goodsView"]['goodsNo']?>");

<?php if($TPL_VAR["page"]=='goods'){?>
            gd_goods_option_view_result($("#frmViewLayer").serialize(), "<?php echo $TPL_VAR["optionInfo"]['sno']?>");
<?php }elseif($TPL_VAR["page"]=='cart_tab_wish'||$TPL_VAR["page"]=='cart_tab_cart'){?>
            gd_carttab_option_view_result($("#frmViewLayer").serialize(), "<?php echo $TPL_VAR["optionInfo"]['sno']?>");
<?php }elseif($TPL_VAR["type"]=='wish'){?>
            var params = $('#frmViewLayer').serializeArray();
            params.push({name: 'page', value: '<?php echo $TPL_VAR["page"]?>'});
            gd_option_view_result(params, "<?php echo $TPL_VAR["optionInfo"]['sno']?>");
<?php }else{?>
            gd_option_view_result($("#frmViewLayer").serialize(), "<?php echo $TPL_VAR["optionInfo"]['sno']?>");
<?php }?>

<?php if($TPL_VAR["type"]!='goods'){?>
            gd_close_layer();
            $("#optionViewLayer").html('');
<?php }?>
        });

<?php if($TPL_VAR["optionInfo"]['optionSno']){?>
<?php if($TPL_VAR["goodsView"]['optionFl']=='n'){?>
        var optionKey = $('#optionViewLayer').find("tbody[id*='option_display_item_0']");
<?php }else{?>
        goodsViewLayerController.option_price_display("#frmViewLayer");
        var optionKey = $('#optionViewLayer').find("tbody[id*='option_display_item_<?php echo $TPL_VAR["optionInfo"]['optionSno']?>']");
<?php }?>

        if ($(optionKey).attr('id')) {
            optionKey = $(optionKey).attr('id').replace("option_display_item_", "");
            $("#frmViewLayer .goodsCnt_" + optionKey).val("<?php echo $TPL_VAR["optionInfo"]['goodsCnt']?>");

<?php if($TPL_VAR["optionInfo"]['optionTextSno']){?>
<?php if((is_array($TPL_R1=$TPL_VAR["optionInfo"]['optionTextStr'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            var optionText = $("#frmViewLayer .optionTextDisplay<?php echo $TPL_K1?>").find("input[name*='optionTextInput_']");
            $(optionText).val("<?php echo $TPL_V1?>");
<?php }}?>

            goodsViewLayerController.option_text_select($("#frmViewLayer input[name*='optionTextInput_']"));

            $("#frmViewLayer input[name*='optionTextInput_']").val('');
<?php }?>

<?php if((is_array($TPL_R1=$TPL_VAR["optionInfo"]['addGoodsNo'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            $("#frmViewLayer select[name*='addGoodsInput']").each(function (key) {
                if($(this).find("option[value*='<?php echo $TPL_V1?>']").length) {
                    $(this).find("option[value*='<?php echo $TPL_V1?>']").attr("selected", "selected");
                    goodsViewLayerController.add_goods_select($("#frmViewLayer select[name='" + $(this).attr('name') + "']"));
                    $("#frmViewLayer .addGoodsCnt_" + optionKey + "_<?php echo $TPL_V1?>").val("<?php echo $TPL_VAR["optionInfo"]['addGoodsCnt'][$TPL_K1]?>");
                    goodsViewLayerController.goods_calculate("#frmViewLayer", '', optionKey + '<?php echo INT_DIVISION?><?php echo $TPL_V1?>', "<?php echo $TPL_VAR["optionInfo"]['addGoodsCnt'][$TPL_K1]?>");
                }
            });
<?php }}?>

<?php if(!$TPL_VAR["optionInfo"]['optionTextSno']&&!$TPL_VAR["optionInfo"]['addGoodsNo']){?>
            goodsViewLayerController.goods_calculate("#frmViewLayer", 1, optionKey, "<?php echo $TPL_VAR["optionInfo"]['goodsCnt']?>");
<?php }?>
        }
<?php }?>
    });

    /**
     * 총 합산
     */
    function gd_total_calculate() {
        var goodsPrice = parseFloat($('#frmViewLayer input[name="set_goods_price"]').val());

        //총합계 계산
        goodsTotalCnt = 0;
        goodsOptionCnt = [];
        $('#frmViewLayer input[name*="goodsCnt[]"]').each(function (index) {
            goodsTotalCnt += parseFloat($(this).val());
            goodsOptionCnt[index] = parseFloat($(this).val());
        });
        var goodsTotalPrice = goodsPrice * goodsTotalCnt;
        var setOptionPrice = 0;

        $('#frmViewLayer input[name*="optionPriceSum[]"]').each(function () {
            setOptionPrice += parseFloat($(this).val());
        });

        var setOptionTextPrice = 0;
        $('#frmViewLayer input[name*="optionTextPriceSum[]"]').each(function () {
            setOptionTextPrice += parseFloat($(this).val());
        });

        var setAddGoodsPrice = 0;
        $('#frmViewLayer input[name*="add_goods_total_price["]').each(function () {
            setAddGoodsPrice += parseFloat($(this).val());
        });

        $('#set_option_price').val(setOptionPrice);
        $('#set_option_text_price').val(setOptionTextPrice);
        $('#set_add_goods_price').val(setAddGoodsPrice);

        var totalGoodsPrice = (goodsTotalPrice + setOptionPrice + setOptionTextPrice + setAddGoodsPrice).toFixed(<?php echo $TPL_VAR["currency"]["decimal"]?>);
        $('#frmViewLayer input[name="set_total_price"]').val(totalGoodsPrice);
    }
    //-->
</script>
<script type="text/html" id="optionTemplateLayer">
    <tbody id="option_display_item_<%=displayOptionkey%>">
        <tr class="check optionKey_<%=optionSno%>">
            <td class="cart_prdt_name">
                <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>" />
                <input type="hidden" name="optionSno[]" value="<%=optionSno%>" />
                <input type="hidden" name="goodsPriceSum[]" value="0" />
                <input type="hidden" name="addGoodsPriceSum[]" value="0" />
                <input type="hidden" name="displayOptionkey[]" value="<%=displayOptionkey%>" />
<?php if($TPL_VAR["couponUse"]=='y'){?>
                <input type="hidden" name="couponApplyNo[]" value="" />
                <input type="hidden" name="couponSalePriceSum[]" value="" />
                <input type="hidden" name="couponAddPriceSum[]" value="" />
<?php }?>
                <div class="cart_tit_box">
                    <strong class="cart_tit">
                        <span><%=optionName%><%=optionSellCodeValue%><%=optionDeliveryCodeValue%></span>
<?php if($TPL_VAR["couponUse"]=='y'){?>
                        <span class="cart_btn_box">
<?php if(gd_is_login()===false){?>
                            <button type="button" class="btn_alert_login"><img src="/data/skin/front/aileenwedding/img/icon/goods_icon/icon_coupon.png" alt="<?php echo __('쿠폰')?>" title="<?php echo __('쿠폰')?>" /></button>
<?php }else{?>
                            <span id="coupon_apply_<%=displayOptionkey%>">
                                <a href="#couponApplyLayer" class="icon_item_coupon btn_open_layer" data-key="<%=displayOptionkey%>"><img src="/data/skin/front/aileenwedding/img/icon/goods_icon/icon_coupon.png" alt="<?php echo __('쿠폰적용')?>" title="<?php echo __('쿠폰적용')?>" /></a>
                            </span>
<?php }?>
                        </span>
<?php }?>
                        <span id="option_text_display_<%=displayOptionkey%>"></span>
                    </strong>
                </div>
            </td>
            <td>
                <span class="count">
                    <span class="goods_qty">
                        <input type="text" name="goodsCnt[]" class="text goodsCnt_<%=displayOptionkey%>" title="<?php echo __('수량')?>" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsCnt'],$TPL_VAR["goodsView"]['minOrderCnt'])?>" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsCnt'],$TPL_VAR["goodsView"]['minOrderCnt'])?>"  data-stock="<%=optionStock%>" data-key="<%=displayOptionkey%>" onchange="goodsViewLayerController.input_count_change(this, '1');return false;" />
                        <span>
                            <button type="button" class="up goods_cnt" title="<?php echo __('증가')?>" value="up<?php echo STR_DIVISION?><%=displayOptionkey%>"><i class="xi-caret-up-min"></i></button>
                            <button type="button" class="down goods_cnt" title="<?php echo __('감소')?>" value="dn<?php echo STR_DIVISION?><%=displayOptionkey%>"><i class="xi-caret-down-min"></i></button>
                        </span>
                    </span>
                </span>
            </td>
            <td class="item_choice_price">
                <input type="hidden" name="option_price_<%=displayOptionkey%>" value="<%=optionPrice%>" />
                <input type="hidden" name="optionPriceSum[]" value="0" />
                <?php echo gd_global_currency_symbol()?><strong class="option_price_display_<%=displayOptionkey%>"><%=optionPrice%></strong><?php echo gd_global_currency_string()?>

            </td>
            <td>
                <button class="delete_goods" data-key="option_display_item_<%=displayOptionkey%>"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_opt_del.png" alt="<?php echo __('삭제')?>"/></button>
            </td>
        </tr>
    </tbody>
</script>
<script type="text/html" id="addGoodsTemplateLayer">
    <tr id="add_goods_display_item_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" class="check item_choice_divide">
        <td class="cart_prdt_name">
            <div class="cart_tit_box">
                <input type="hidden" name="addGoodsNo[<%=optionIndex%>][]" value="<%=optionSno%>" data-group="<%=addGoodsGroup%>" />
                <strong class="item_choice_tit">
                    <em class="item_choice_photo"><%=addGoodsimge%></em><span><%=addGoodsName%></span>
                </strong>
            </div>
        </td>
        <td>
            <span class="count">
                <span class="goods_qty">
                    <input type="text" name="addGoodsCnt[<%=optionIndex%>][]" class="text addGoodsCnt_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" title="<?php echo __('수량')?>" value="1" data-key="<%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>" data-stock-fl="<%=addGoodsStockFl%>" data-stock="<%=addGoodsStock%>" onchange="goodsViewLayerController.input_count_change(this);return false;" />
                    <span>
                        <button type="button" class="up add_goods_cnt" title="<?php echo __('증가')?>" value="up<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><i class="xi-caret-up-min"></i></button>
                        <button type="button" class="down add_goods_cnt" title="<?php echo __('감소')?>" value="dn<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><i class="xi-caret-down-min"></i></button>
                    </span>
                </span>
            </span>
        </td>
        <td class="item_choice_price">
            <input type="hidden" name="add_goods_price_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" value="<%=addGoodsPrice%>" />
            <input type="hidden" name="add_goods_total_price[<%=optionIndex%>][]" value="" />
            <?php echo gd_global_currency_symbol()?><strong class="add_goods_price_display_<%=displayOptionkey%>_<%=displayAddGoodsKey%>"></strong><?php echo gd_global_currency_string()?>

        </td>
        <td>
            <button class="delete_add_goods" data-key="<%=displayOptionkey%>-<%=displayAddGoodsKey%>"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_opt_del.png" alt="<?php echo __('삭제')?>"/></button>
        </td>
    </tr>
</script>