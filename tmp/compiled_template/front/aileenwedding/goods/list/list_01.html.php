<?php /* Template_ 2.2.7 2020/05/24 15:23:28 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/goods/list/list_01.html 000002389 */ 
if (is_array($TPL_VAR["goodsList"])) $TPL_goodsList_1=count($TPL_VAR["goodsList"]); else if (is_object($TPL_VAR["goodsList"]) && in_array("Countable", class_implements($TPL_VAR["goodsList"]))) $TPL_goodsList_1=$TPL_VAR["goodsList"]->count();else $TPL_goodsList_1=0;?>
<ul class="gallery_layer">
<?php if($TPL_VAR["goodsList"]){?>
<?php if($TPL_goodsList_1){foreach($TPL_VAR["goodsList"] as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?><li>
                <a href="<?php echo gd_goods_url($TPL_V2["goodsUrl"],$TPL_V2["goodsNo"])?>" <?php if($TPL_VAR["themeInfo"]["relationLinkFl"]=='blank'){?> target="_blank"<?php }?>>
                    <div class="thumb"><img src="<?php echo $TPL_V2["goodsImageSrc"]?>" alt="" onmouseover="this.src='<?php echo $TPL_V2["goodsSubImage"]?>'" onmouseleave="this.src='<?php echo $TPL_V2["goodsImageSrc"]?>'" /></div>
                    <div class="title"><?php echo $TPL_V2["goodsNm"]?></div>
                    <div class="desc"><?php echo $TPL_V2["shortDescription"]?></div>
                    <div class="seo"><?php echo $TPL_V2["goodsSearchWord"]?></div>
                    <div class="price">
<?php if($TPL_V2["fixedPrice"]!= 0&&($TPL_V2["fixedPrice"]!=$TPL_V2["goodsPrice"])){?>
                            <span class="og">
                                <?php echo gd_money_format($TPL_V2["fixedPrice"])?>won
                            </span>
                            <span class="arrow"><i class="xi-long-arrow-right" style="display:none;"></i></span>
                            <span class="dc">
                                <?php echo gd_money_format($TPL_V2["goodsPrice"])?>won
                            </span>
<?php }else{?>
                            <span class="dc">
                                <?php echo gd_money_format($TPL_V2["goodsPrice"])?>won
                            </span>
<?php }?>
                    </div>
                </a>
            </li><?php }}?>
<?php }}?>
<?php }else{?>
        <li class="no_gallery"><strong><?php echo __('상품이 존재하지 않습니다.')?></strong></li>
<?php }?>
</ul>