<?php /* Template_ 2.2.7 2020/03/15 23:05:32 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/main/index.html 000009341 */  $this->include_("dataBanner");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/main.css" rel="stylesheet">
<section class="section_container">
    <div class="shortcut_wrapper">
        <ul class="shortcut_layer center_layer">
<?php if((is_array($TPL_R1=dataBanner('782176668',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <li>
                <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
            </li>
            <li class="separate"></li>
<?php }}?>
        </ul>
    </div><!-- .shortcut_wrapper -->
    <div class="best_wrapper newest_wrapper">
        <div class="center_layer">
            <div class="big_title"><img src="/data/skin/front/aileenwedding/img/main/title_best.png" alt=""></div>
            <ul class="tab_layer">
                <li>
                    <a href="#" onclick="return false;" data-box="one"><img src="/data/skin/front/aileenwedding/img/main/tab_best_1_on.png" alt=""></a>
                </li>
                <li>
                    <a href="#" onclick="return false;" data-box="two"><img src="/data/skin/front/aileenwedding/img/main/tab_best_2_off.png" alt=""></a>
                </li>
                <li>
                    <a href="#" onclick="return false;" data-box="three"><img src="/data/skin/front/aileenwedding/img/main/tab_best_3_off.png" alt=""></a>
                </li>
                <li>
                    <a href="#" onclick="return false;" data-box="four"><img src="/data/skin/front/aileenwedding/img/main/tab_best_4_off.png" alt=""></a>
                </li>
                <li>
                    <a href="#" onclick="return false;" data-box="five"><img src="/data/skin/front/aileenwedding/img/main/tab_best_5_off.png" alt=""></a>
                </li>
            </ul><!-- .tab_layer -->
            <div class="box_layer">
                <ul class="one">
<?php if((is_array($TPL_R1=dataBanner('2640822563',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li>
                        <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                    </li>
<?php }}?>
                </ul>
                <ul class="two">
<?php if((is_array($TPL_R1=dataBanner('2724025122',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li>
                        <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                    </li>
<?php }}?>
                </ul>
                <ul class="three">
<?php if((is_array($TPL_R1=dataBanner('2046408785',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li>
                        <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                    </li>
<?php }}?>
                </ul>
                <ul class="four">
<?php if((is_array($TPL_R1=dataBanner('3560641317',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li>
                        <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                    </li>
<?php }}?>
                </ul>
                <ul class="five">
<?php if((is_array($TPL_R1=dataBanner('3356508179',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <li>
                        <a href="<?php echo $TPL_V1["bannerLink"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                    </li>
<?php }}?>
                </ul>
            </div><!-- .box_layer -->
        </div><!-- .center_layer -->
    </div><!-- .best_wrapper -->
    <div class="hot_wrapper newest_wrapper">
        <div class="center_layer">
            <div class="big_title"><img src="/data/skin/front/aileenwedding/img/main/title_notice.png" alt=""></div>
            <ul class="gallery_layer">
<?php if((is_array($TPL_R1=dataBanner('1408043158',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                <li>
                    <a href="<?php echo $TPL_V1["bannerLink"]?>" target="<?php echo $TPL_V1["bannerTarget"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>"></a>
                </li>
<?php }}?>
            </ul>
            <div class="box_layer swiper-container" id="MainHotSlide" style="display:none;">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="thumb"></div>
                        <div class="info">
                            <div class="left">
                                <div class="title">Every sweet day [레이저]</div>
                                <div class="price">107,000 won</div>
                            </div>
                            <div class="right">
                                <div class="desc">
                                신랑 신부의 모습을 귀여운 일러스트로 풀어낸 러블리한 감성의 레이저 청첩장입니다. 청첩장을 열면 숨겨진 비밀이 짠!
                                </div>
                            </div>
                        </div><!-- .info -->
                    </div><!-- .swiper-slide -->
                    <div class="swiper-slide">
                        <div class="thumb"></div>
                        <div class="info">
                            <div class="left">
                                <div class="title">Every sweet day [레이저]</div>
                                <div class="price">107,000 won</div>
                            </div>
                            <div class="right">
                                <div class="desc">
                                신랑 신부의 모습을 귀여운 일러스트로 풀어낸 러블리한 감성의 레이저 청첩장입니다. 청첩장을 열면 숨겨진 비밀이 짠!
                                </div>
                            </div>
                        </div><!-- .info -->
                    </div><!-- .swiper-slide -->
                    <div class="swiper-slide">
                        <div class="thumb"></div>
                        <div class="info">
                            <div class="left">
                                <div class="title">Every sweet day [레이저]</div>
                                <div class="price">107,000 won</div>
                            </div>
                            <div class="right">
                                <div class="desc">
                                신랑 신부의 모습을 귀여운 일러스트로 풀어낸 러블리한 감성의 레이저 청첩장입니다. 청첩장을 열면 숨겨진 비밀이 짠!
                                </div>
                            </div>
                        </div><!-- .info -->
                    </div><!-- .swiper-slide -->
                </div><!-- .swiper-wrapper -->
                <div class="hot_next_button swiper-button-next"><i class="xi-angle-right-thin"></i></div>
                <div class="hot_prev_button swiper-button-prev"><i class="xi-angle-left-thin"></i></div>
            </div><!-- .box_layer -->
        </div><!-- .center_layer -->
    </div><!-- .hot_wrapper -->
</section>
<?php $this->print_("footer",$TPL_SCP,1);?>


<script>
var main_hot_slide = new Swiper('#MainHotSlide', {
    slidesPerView: 2,
    spaceBetween: 10,
    navigation: {
        nextEl: '.hot_next_button',
        prevEl: '.hot_prev_button',
    },
});

jQuery(function($) {
    $(document).on('click', '.best_wrapper .tab_layer a', function() {
        $('.best_wrapper .tab_layer a').each(function() {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_on', '_off'));
        });
        $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_off', '_on'));

        $('.best_wrapper .box_layer ul').each(function() {
            $(this).hide();
        });
        $('.best_wrapper .box_layer ul.' + $(this).data('box')).show();
    });
});
</script>