<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/find_password.html 000002218 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="login_section">
	<div class="login_layer">
		<form name="formFind" id="formFind" action="../member/user_certification.php" method="post">
			<div class="title_area">비밀번호 찾기</div>
			<div class="desc_area">비밀번호를 찾고자 하는 아이디를 입력해 주세요.</div>
			<ul class="input_member">
				<li>
					<div><input type="text" id="memberId" name="memberId" placeholder="아이디" required="true"></div>
				</li>
			</ul><!-- .input_member -->
			<ul class="link">
				<li class="left">
					아이디를 모르시나요?
				</li>
				<li class="right">
					<a href="#" id="btnFindId">아이디 찾기</a>
				</li>
			</ul>
			<div class="button_area">
				<button type="submit" class="button">다음</button>
			</div><!-- .button_area -->
		</form>
	</div><!-- .login_layer -->
</section><!-- .login_section -->
<!-- //content_box -->
<script type="text/javascript">
	$(document).ready(function () {
		$('#btnFindId').click(function (e) {
			location.href = '../member/find_id.php';
			e.preventDefault();
		});

		$('#formFind button[type="submit"]').click(function () {
			if ($('#formFind').valid() == false) return false;
		});

		$('#formFind').validate({
			dialog: false,
			rules: {
				memberId: {
					required: true
				}
			},
			messages: {
				memberId: {
					required: "<?php echo __('아이디를 입력해주세요')?>"
				}
			}, submitHandler: function (form) {
				var data = $(form).serializeArray();
				data.push({name: "mode", value: "find_member"});

				$.post('../member/find_ps.php', data).done(function (data, textStatus, jqXHR) {
					//console.log('gd_member ajax', data, textStatus, jqXHR);
					var code = data.code;
					var message = data.message;
					if (_.isUndefined(code) && _.isUndefined(message)) {
						form.submit();
					} else {
						alert(message);
						return;
					}
				});
			}
		});
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>