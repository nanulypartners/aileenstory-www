<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/join.html 000004192 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="join_section login_section">
	<div class="join_layer login_layer">
		<form id="formJoin" name="formJoin" action="<?php echo $TPL_VAR["joinActionUrl"]?>" method="post">
			<input type="hidden" name="rncheck" value="<?php echo $TPL_VAR["data"]["rncheck"]?>">
			<input type="hidden" name="dupeinfo" value="<?php echo $TPL_VAR["data"]["dupeinfo"]?>">
			<input type="hidden" name="pakey" value="<?php echo $TPL_VAR["data"]["pakey"]?>">
			<input type="hidden" name="foreigner" value="<?php echo $TPL_VAR["data"]["foreigner"]?>">
			<input type="hidden" name="adultFl" value="<?php echo $TPL_VAR["data"]["adultFl"]?>">
			<input type="hidden" name="mode" value="join">
			<!-- 회원가입/정보 기본정보 --><?php echo includeWidget('member/_join_view.html')?><!-- 회원가입/정보 기본정보 -->
			<!-- 회원가입/정보 사업자정보 --><?php echo includeWidget('member/_join_view_business.html')?><!-- 회원가입/정보 사업자정보 -->
			<!-- 회원가입/정보 부가정보 --><?php echo includeWidget('member/_join_view_other.html')?><!-- 회원가입/정보 부가정보 -->
			<div class="button_area">
				<ul>
					<li>
						<button type="button" id="btnCancel" class="button white">취소</button>
					</li>
					<li>
						<button type="button" class="button orange js_btn_join" value="회원가입">회원가입</button>
					</li>
				</ul>
			</div><!-- .button_area -->
		</form>
	</div><!-- .join_layer -->
</section><!-- .join_section -->
<script type="text/javascript">
	var paycoProfile = <?php echo $TPL_VAR["paycoProfile"]?>;
    var naverProfile = <?php echo $TPL_VAR["naverProfile"]?>;
	var thirdPartyProfile = <?php echo $TPL_VAR["thirdPartyProfile"]?>;
	var kakaoProfile = <?php echo $TPL_VAR["kakaoProfile"]?>;

	$(document).ready(function () {
		var $formJoin = $('#formJoin');

		$(':text:first', $formJoin).focus();

		$('#btnCancel', $formJoin).click(function (e) {
			e.preventDefault();
			top.location.href = '/';
		});

		if ($('.js_datepicker').length) {
			$('.js_datepicker').datetimepicker({
				locale: '<?php echo $TPL_VAR["gGlobal"]["locale"]?>',
				format: 'YYYY-MM-DD',
				dayViewHeaderFormat: 'YYYY MM',
				viewMode: 'days',
				ignoreReadonly: true,
				debug: false,
				keepOpen: false
			});
		}

		$('#btnPostcode').click(function (e) {
			e.preventDefault();
			$('#address-error, #addressSub-error').remove();
			$(':text[name=address], :text[name=addressSub]').removeClass('text_warning');
			gd_postcode_search('zonecode', 'address', 'zipcode');
		});

		$('#btnCompanyPostcode').click(function (e) {
			e.preventDefault();
			$('#comAddress-error, #comAddressSub-error').remove();
			$(':text[name=comAddress], :text[name=comAddressSub]').removeClass('text_warning');
			gd_postcode_search('comZonecode', 'comAddress', 'comZipcode');
		});

<?php if($TPL_VAR["joinField"]["businessinfo"]["use"]=='y'){?>
		$(':radio[name="memberFl"]').change(function () {
			var $businessinfo = $('.business_info_box');
			if (this.value == 'business') {
				$businessinfo.removeClass('dn');
				$businessinfo.find('input, select').removeClass('ignore');
			} else {
				$businessinfo.addClass('dn');
				$businessinfo.find('input, select').addClass('ignore');
			}
		});
		$(':radio[name="memberFl"]:checked').trigger('change');
<?php }?>

<?php if($TPL_VAR["joinField"]["marriFl"]["use"]=='y'&&$TPL_VAR["joinField"]["marriDate"]["use"]=='y'){?>
		$(':radio[name="marriFl"]').change(function () {
			var $marridateinfo = $('.js_marridateinfo');
			if (this.value == 'y') {
				$marridateinfo.removeClass('dn');
			} else {
				$marridateinfo.addClass('dn');
				$marridateinfo.find('input#marriDate').val('');
			}
		});
<?php }?>

		gd_select_email_domain('email');

		gd_member2.init($formJoin);

		$('.js_btn_join').click({form: $formJoin}, gd_member2.save);
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>