<?php /* Template_ 2.2.7 2020/05/24 15:02:02 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/join_agreement.html 000006130 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="join_section login_section">
	<div class="join_layer login_layer">
		<form id="formTerms" name="formTerms" method="post" action="../member/join.php">
			<input type="hidden" name="token" value="<?php echo $TPL_VAR["token"]?>">
			<input type="hidden" name=mode value="chkRealName">
			<input type="hidden" name=rncheck value="none">
			<input type="hidden" name=nice_nm value="">
			<input type="hidden" name=pakey value="">
			<input type="hidden" name=birthday value="">
			<input type="hidden" name=mobile value="">
			<input type="hidden" name=mobileService value="">
			<input type="hidden" name=sex value="">
			<input type="hidden" name=dupeinfo value="">
			<input type="hidden" name=foreigner value="">
			<input type="hidden" name=adultFl value="">
			<input type="hidden" name=phone value="">
			<input type="hidden" name=type>
			<div class="title_area">약관동의</div>
			<ul class="input_member">
				<li class="block">
					<div>
						<input type="checkbox" id="allAgree">
						<label class="check" for="allAgree"><?php echo __('%s의 모든 약관을 확인하고 전체 동의합니다.',$TPL_VAR["serviceInfo"]['mallNm'])?></label>
					</div>
					<div class="msg">전체동의, 선택항목도 포함됩니다.</div>
				</li>
				<li class="block">
					<div class="js_terms_view">
						<input type="checkbox" id="termsAgree1" name="agreementInfoFl" class="require">
						<label class="check_s" for="termsAgree1"><strong class="text_pink">(필수)</strong> 이용약관</label>
						<a href="../service/agreement.php?code=<?php echo $TPL_VAR["agreementInfo"]['informCd']?>" target="_blank" class="btn total">전체보기</a>
					</div>
					<div class="agreement_box">
						<?php echo nl2br($TPL_VAR["agreementInfo"]['content'])?>

					</div>
				</li>
				<li class="block">
					<div class="js_terms_view">
						<input type="checkbox" id="termsAgree2" name="privateApprovalFl" class="require">
						<label class="check_s" for="termsAgree2"><strong class="text_pink">(필수)</strong> 개인정보 수집 및 이용</label>
						<a href="../service/agreement.php?code=<?php echo $TPL_VAR["privateApproval"]['informCd']?>" target="_blank" class="btn total">전체보기</a>
					</div>
					<div class="agreement_box">
						<?php echo nl2br($TPL_VAR["privateApproval"]['content'])?>

					</div>
				</li>
			</ul><!-- .input_member -->
			<div class="button_area">
				<ul>
<?php if($TPL_VAR["useThirdParty"]){?>
						<li>
							<button type="button" id="btnPrevStep" class="button">이전단계</button>
						</li>
<?php }?>
					<li>
						<button type="button" id="btnNextStep" class="button">다음단계</button>
					</li>
				</ul>
			</div><!-- .button_area -->
		</form>
	</div><!-- .join_layer -->
</section><!-- .login_section -->
<script type="text/javascript">
	$(document).ready(function () {
		var body = $('body'), $formTerms = $('#formTerms');

		$('#btnNextStep').click(function () {
			var pass = true;
			/*
			 * 필수 동의 항목 검증
			 */
			$(':checkbox.require').each(function (idx, item) {
				var $item = $(item);
				if (!$item.prop('checked')) {
					pass = false;
					alert($item.next().text() + "<?php echo __('을 체크해주세요.')?>");
					return false;
				}
			});

			if (pass) {
				/*
				 * 실명인증 검증
				 */
				if ($('input[name="RnCheckType"]').length > 0) {
					switch ($('input[name="RnCheckType"]:checked').val()) {
						case 'ipin' :
							var popupWindow = window.open("", "popupCertKey", "top=100, left=200, status=0, width=417, height=490");
							ifrmRnCheck.location.href = "/member/ipin/ipin_main.php?callType=joinmember";
							break;
						case  'authCellPhone' :
							var protocol = location.protocol;
							var callbackUrl = "<?php echo $TPL_VAR["domainUrl"]?>/member/authcellphone/dreamsecurity_result.php";
							ifrmHpauth.location.href = protocol + "//hpauthdream.godo.co.kr/module/NEW_hpauthDream_Main.php?callType=joinmember&shopUrl=" + callbackUrl + "&cpid=<?php echo $TPL_VAR["authDataCpCode"]?>";
							break;
						default :
							alert("<?php echo __('본인인증이 필요합니다.')?>");
							$('input[name="RnCheckType"]:first').focus();
							break;
					}
					return false;
				} else {
					$formTerms.submit();
				}
			}
		});

		/*
		 * 전체 동의 체크박스 이벤트
		 */
		$('#allAgree', $formTerms).change(function (e) {
			var $target = $(e.target), $checkbox = $(':checkbox'), $label = $checkbox.siblings('label');
			if ($target.prop('checked') === true) {
				$checkbox.prop('checked', true).val('y');
				$label.addClass('on');
			} else {
				$checkbox.prop('checked', false).val('n');
				$label.removeClass('on');
			}
		});

		/*
		 * 이전단계 버튼 이벤트
		 */
		$('#btnPrevStep', $formTerms).click(function (e) {
			e.preventDefault();
			history.back();
		});

		/*
		 * 약관 체크박스 이벤트
		 */
		$('.js_terms_view :checkbox', $formTerms).change(function (e) {
			var $target = $(e.target), $label = $target.siblings('label'), $termsView = $target.closest('.js_terms_view');
			var isTermsAgreeSelect = (e.target.id === 'termsAgree3') || (e.target.id === 'termsAgree4') || (e.target.id === 'termsAgree5');
			var isTargetChecked = $target.prop('checked') === true;
			if (isTargetChecked) {
				if (isTermsAgreeSelect) {
					$termsView.find('.agreement_choice_box label').addClass('on');
					$termsView.find('.agreement_choice_box :checkbox').val('y');
				} else {
					$target.val('y');
					$label.addClass('on');
				}
			} else {
				if (isTermsAgreeSelect) {
					$termsView.find('.agreement_choice_box label').removeClass('on');
					$termsView.find('.agreement_choice_box :checkbox').val('n');
				} else {
					$target.val('n');
					$label.removeClass('on');
				}
			}
		});
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>