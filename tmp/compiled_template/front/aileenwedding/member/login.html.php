<?php /* Template_ 2.2.7 2020/05/24 15:02:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/login.html 000006718 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="login_section">
	<div class="login_layer">
		<form id="formLogin" method="post" action="<?php echo $TPL_VAR["loginActionUrl"]?>">
			<input type="hidden" id="mode" name="mode" value="<?php echo $TPL_VAR["modeLogin"]?>"/>
			<input type="hidden" id="returnUrl" name="returnUrl" value="<?php echo $TPL_VAR["returnUrl"]?>"/>
			<div class="title_area">회원 로그인</div>
			<ul class="input_member">
				<li class="left">
					<div><input type="text" id="loginId" name="loginId" value="<?php echo $TPL_VAR["data"]["loginId"]?>" placeholder="아이디" required="true"></div>
					<div class="input_text"><input type="password" id="loginPwd" name="loginPwd" value="" placeholder="비밀번호" required="true"></div>
				</li>
				<li class="right">
					<button type="submit" class="button">로그인</button>
				</li>
			</ul><!-- .input_member -->
			<ul class="link">
				<li class="left">
					<input type="checkbox" id="saveId" name="saveId" value="y" checked="<?php if(!empty($TPL_VAR["data"]["loginId"])){?>true<?php }?>">
					<label for="saveId">아이디 저장</label>
				</li>
				<li class="right">
					<a href="#" onclick="return false;" id="btnFindPwd">비밀번호 찾기</a>
				</li>
				<li class="separate"></li>
				<li class="right">
					<a href="#" onclick="return false;" id="btnFindId">아이디 찾기</a>
				</li>
				<li class="separate"></li>
				<li class="right">
					<a href="#" onclick="return false;" id="btnJoinMember">회원가입</a>
				</li>
			</ul><!-- .link -->
			<div class="sns_button button_area">
<?php if($TPL_VAR["usePaycoLogin"]){?>
				<a class="btn_payco_login js_btn_payco_login" href="#">
					페이코 로그인
				</a>
<?php }?>
<?php if($TPL_VAR["useFacebookLogin"]){?>
				<a href="#" class="btn_facebook_login js_btn_facebook_login" href="#" data-facebook-url="<?php echo $TPL_VAR["facebookUrl"]?>">
					<i class="xi-facebook-official"></i>페이스북 로그인
				</a>
<?php }?>
<?php if($TPL_VAR["useNaverLogin"]){?>
				<a href="#" class="button naver btn_naver_login js_btn_naver_login" href="#" data-naver-url="<?php echo $TPL_VAR["naverUrl"]?>">
					<i class="xi-naver-square"></i>네이버 로그인
				</a>
<?php }?>
<?php if($TPL_VAR["useKakaoLogin"]){?>
				<a href="#" class="button kakao btn_kakao_login js_btn_kakao_login" data-kakao-type="login" data-return-url="<?php echo $TPL_VAR["kakaoReturnUrl"]?>">
					<i class="xi-kakaotalk"></i>카카오 로그인
				</a>
<?php }?>
<?php if($TPL_VAR["useWonderLogin"]){?>
				<a href="#" class="btn_wonder_login js_btn_wonder_login" data-wonder-type="login" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>">
					원더 로그인
				</a>
<?php }?>
			</div><!-- .button_area -->
		</form>
<?php if($TPL_VAR["hasGuestOrder"]==false){?>
			<form id="formOrderLogin" action="../member/member_ps.php" method="post" style="display:none;">
				<input type="hidden" name="mode" value="guestOrder">
				<input type="hidden" name="returnUrl" value="../mypage/order_view.php">
				<div class="guest_area">
					<div class="title">비회원 주문조회</div>
					<ul class="input">
						<li class="left">
							<input type="text" name="orderNm" placeholder="주문자명">
						</li>
						<li class="right">
							<input type="text" name="orderNo" placeholder="주문번호" data-max-length="<?php echo $TPL_VAR["orderNoMaxLength"]?>">
						</li>
					</ul><!-- .input -->
					<div class="button_area">
						<button type="submit" class="button">확인</button>
					</div><!-- .button_area -->
					<div class="help">
						주문번호와 비밀번호를 잊으신 경우, 고객센터로 문의히여 주시기 바랍니다.
					</div>
				</div><!-- .guest_area -->
			</form>
<?php }?>
	</div><!-- .login_layer -->
</section><!-- .login_section -->
<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/jquery.serialize.object.js"></script>
<script type="text/javascript">
	var $formLogin;
	$(document).ready(function () {
	    var order_no_max_length = $('input[name=orderNo]').data('max-length');
		$('#btnJoinMember').click(function (e) {
			e.preventDefault();
			location.href = '../member/join_agreement.php';
		});
		$('#btnFindId').click(function (e) {
			e.preventDefault();
			location.href = '../member/find_id.php';
		});
		$('#btnFindPwd').click(function (e) {
			e.preventDefault();
			location.href = '../member/find_password.php';
		});

		$('#loginId, #loginPwd').focusin(function () {
			$('.js_caution_msg1', '#formLogin').addClass('dn');
		});

		$formLogin = $('#formLogin');
		$formLogin.validate({
			dialog: false,
			rules: {
				loginId: {
					required: true
				},
				loginPwd: {
					required: true
				}
			},
			messages: {
				loginId: {
					required: "<?php echo __('아이디를 입력해주세요')?>"
				},
				loginPwd: {
					required: "<?php echo __('패스워드를 입력해주세요')?>"
				}
			}, submitHandler: function (form) {
			    if (window.location.search) {
                    var _tempUrl = window.location.search.substring(1);
                    var _tempVal = _tempUrl.split('=');

                    if (_tempVal[1] == 'lnCouponDown') {
                        $('#returnUrl').val(document.referrer);
                    }
                }
				form.target = 'ifrmProcess';
				form.submit();
			}
		});

		// 비회원 주문조회 폼 체크
		$('#formOrderLogin').validate({
			rules: {
				orderNm: 'required',
				orderNo: {
					required: true,
					number: true,
					maxlength: order_no_max_length
				}
			},
			messages: {
				orderNm: {
					required: "<?php echo __('주문자명을 입력해주세요.')?>"
				},
				orderNo: {
					required: "<?php echo __('주문번호를 입력해주세요.')?>",
					number: "<?php echo __('숫자로만 입력해주세요.')?>",
					maxlength: "<?php echo __('주문번호는 " + order_no_max_length + "자리입니다.')?>"
				}
			},
			submitHandler: function (form) {
				$.post(form.action, $(form).serializeObject()).done(function (data, textStatus, jqXhr) {
					//console.log(data);
					if (data.result == 0) {
						location.replace('../mypage/order_view.php?orderNo=' + data.orderNo);
					} else {
						alert('주문자명과 주문번호가 일치하는 주문이 존재하지 않습니다. 다시 입력해 주세요.');
						return;
					}
				});
				return false;
			}
		});
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>