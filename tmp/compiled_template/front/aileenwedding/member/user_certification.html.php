<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/user_certification.html 000004182 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="login_section">
	<div class="login_layer">
		<form name="formAuth" id="formAuth" method="post" action="">
			<input type="hidden" name="act" value="Y"/>
			<input type="hidden" name="rncheck" value="none"/>
			<input type="hidden" name="dupeinfo" value=""/>
			<input type="hidden" name="token" value="<?php echo $TPL_VAR["token"]?>"/>
			<div class="title_area">비밀번호 찾기</div>
			<div class="desc_area">본인인증 방법을 선택해 주세요.</div>
			<ul class="input_member">
				<!-- <?php if($TPL_VAR["emailFl"]===true){?> -->
					<li class="block">
						<div>
							<input type="radio" id="authEmail" name="authType" value="authEmail">
							<label for="authEmail">이메일 인증</label>
							<strong>( <?php echo $TPL_VAR["email"]?> )</strong>
						</div>
					</li>
				<!-- <?php }?> -->
				<!-- <?php if($TPL_VAR["smsFl"]===true){?>  -->
					<li class="block">
						<input type="radio" id="authSms" name="authType" value="authSms">
						<label for="authSms">SMS 인증</label>
						<strong>( <?php echo $TPL_VAR["cellphone"]?> )</strong>
					</li>
				<!-- <?php }?> -->
				<!-- <?php if($TPL_VAR["ipinFl"]===true){?>  -->
					<li class="block">
						<input type="radio" id="authIpin" name="authType" value="authIpin">
						<label for="authIpin">아이핀 본인인증</label>
						<iframe id="ifrmRnCheck" name="ifrmRnCheck" style="width:500px;height:500px;display:none;"></iframe>
					</li>
				<!-- <?php }?> -->
				<!-- <?php if($TPL_VAR["authCellphoneFl"]===true){?>  -->
					<li class="block">
						<input type="radio" id="authCellphone" name="authType" value="authCellphone">
						<label for="authCellphone">휴대폰 본인인증</label>
						<iframe id="ifrmHpauth" name="ifrmHpauth" style="width:500px;height:500px;display:none;"></iframe>
					</li>
				<!-- <?php }?> -->
			</ul><!-- .input_member -->
			<div class="button_area">
				<button type="submit" class="button">다음</button>
			</div><!-- .button_area -->
		</form>
	</div><!-- .login_layer -->
</section><!-- .login_section -->
<!-- //content_box -->
<script type="text/javascript">
	$(document).ready(function () {
		$('#formAuth').validate({
			dialog: false,
			rules: {
				authType: {
					required: true
				}
			},
			messages: {
				authType: {
					required: "<?php echo __('인증수단을 선택해 주세요.')?>"
				}
			}, submitHandler: function (form) {
				//$('#errorMessage').addClass('dn');
				//                console.log('submitHandler');
				var params = $(form).serializeArray();
				params.push({name: "mode", value: "requestAuth"});

				switch ($('input[name=authType]:checked').val()) {
					case 'authIpin' :
						var popupWindow = window.open("", "popupCertKey", "top=100, left=200, status=0, width=417, height=490");
						ifrmRnCheck.location.href = "../member/ipin/ipin_main.php?callType=findpwd";
						return;
						break;
					case 'authCellphone' :
						var protocol = location.protocol;
						var callbackUrl = "<?php echo $TPL_VAR["domaiUrl"]?>/member/authcellphone/dreamsecurity_result.php";
						ifrmHpauth.location.href = protocol + "//hpauthdream.godo.co.kr/module/NEW_hpauthDream_Main.php?callType=findid&shopUrl=" + callbackUrl + "&cpid=<?php echo $TPL_VAR["authDataCpCode"]?>";
						break;
					case 'authEmail':
					case 'authSms':
						$.post('../member/user_certification_ps.php', params).done(function (data, textStatus, jqXHR) {
							console.log('gd_member ajax', data, textStatus, jqXHR);
							var code = data.code;
							var message = data.message;
							if (_.isUndefined(code) && _.isUndefined(message)) {
								alert(data);
								form.action = '../member/user_certification_confirm.php';
								form.submit();
							} else {
								alert(message);
								return;
							}
						});
						break;
					default :
				}

			}
		});
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>