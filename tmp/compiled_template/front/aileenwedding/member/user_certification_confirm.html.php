<?php /* Template_ 2.2.7 2020/03/15 19:16:56 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/member/user_certification_confirm.html 000004374 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<section class="login_section">
	<div class="login_layer">
		<form name="formAuthConfirm" id="formAuthConfirm" method="post">
			<input type="hidden" name="authType" value="<?php echo $TPL_VAR["authType"]?>"/>
			<input type="hidden" name="certificationType" value="<?php echo $TPL_VAR["certificationType"]?>"/>
			<input type="hidden" name="token" value="<?php echo $TPL_VAR["token"]?>"/>
			<div class="title_area">비밀번호 찾기</div>
			<!-- <?php if($TPL_VAR["authType"]=='authEmail'){?> -->
				<div class="desc_area">수신된 이메일의 인증번호를 입력해 주세요.</div>
			<!-- <?php }elseif($TPL_VAR["authType"]=='authSms'){?> -->
				<div class="desc_area">수신된 SMS의 인증번호를 입력해 주세요.</div>
			<!-- <?php }?> -->
			<ul class="input_member">
				<li class="block">
					<div>
						<input type="text" id="inputCertify" name="inputCertify" placeholder="인증번호 입력">
					</div>
				</li>
			</ul><!-- .input_member -->
			<ul class="link warning">
				<li class="left">
					<!-- <?php if($TPL_VAR["authType"]=='authEmail'){?> --> 
						인증메일이 도착하지 않았나요? 
					<!-- <?php }elseif($TPL_VAR["authType"]=='authSms'){?> -->
						인증번호가 도착하지 않았나요?
					<!-- <?php }?> -->
				</li>
				<li class="right">
					<a href="#" name="btnAgain">인증번호 다시받기</a>
				</li>
			</ul>
			<div class="button_area">
				<button type="submit" class="button">확인</button>
				<button type="button" class="button white" id="btnCancel">이전</button>
			</div><!-- .button_area -->
		</form>
	</div><!-- .login_layer -->
</section><!-- .login_section -->
<script type="text/javascript">
	$(document).ready(function () {
		var $formAuthConfirm = $('#formAuthConfirm');

		$('#btnCancel', $formAuthConfirm).click(function (e) {
			e.preventDefault();
			location.href = '../member/user_certification.php';
		});

		$('[name=btnAgain]', $formAuthConfirm).click(function (e) {
			e.preventDefault();

			var params = $formAuthConfirm.serializeArray();
			params.push({name: "mode", value: "requestAuth"});

			$.post('../member/user_certification_ps.php', params).done(function (data, textStatus, jqXHR) {
				//console.log('gd_member ajax', data, textStatus, jqXHR);
				var code = data.code;
				var message = data.message;
				if (_.isUndefined(code) && _.isUndefined(message)) {
					alert(data);
					return;
				} else {
					$('#guideMsg', $formAuthConfirm).addClass('dn');
					$('#errorMessage', $formAuthConfirm).removeClass('dn').html('<p class="info_again">' + message + '<a href="#" name="btnAgain"><?php echo __("인증번호 다시받기")?></a></p>');
					if (code == 403) {
						location.href = '../member/find_password.php';
					}
				}
			});
		});

		$formAuthConfirm.validate({
			dialog: false,
			rules: {
				inputCertify: {
					required: true
				}
			},
			messages: {
				inputCertify: {
					required: "<?php echo __('인증번호를 입력해주세요.')?>"
				}
			}, submitHandler: function (form) {
				var params = $(form).serializeArray();
				var certificationType = $('input[name=certificationType]', form).val();
				switch (certificationType) {
					case 'find_password':
						params.push({name: "mode", value: "certificationFindPassword"});
						break;
				}

				$.post('../member/user_certification_ps.php', params).done(function (data, textStatus, jqXHR) {
					//console.log('gd_member ajax', data, textStatus, jqXHR);
					var code = data.code;
					var message = data.message;
					var url = data.url;
					if (_.isUndefined(code) && _.isUndefined(message)) {
						alert(data);
						switch (certificationType) {
							case 'find_password':
								form.action = '../member/find_password_reset.php';
								break;
						}
						form.submit();
					} else {
						alert(message);
						$('#guideMsg', form).addClass('dn');
						$('#errorMessage', form).removeClass('dn');
						if (_.isUndefined(url) === false) {
							location.href = url;
						}
					}
				});
			}
		});
	});
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>