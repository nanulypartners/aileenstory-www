<?php /* Template_ 2.2.7 2020/06/19 06:51:13 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/_order_goods_list.html 000032088 */ 
if (is_array($TPL_VAR["orderData"])) $TPL_orderData_1=count($TPL_VAR["orderData"]); else if (is_object($TPL_VAR["orderData"]) && in_array("Countable", class_implements($TPL_VAR["orderData"]))) $TPL_orderData_1=$TPL_VAR["orderData"]->count();else $TPL_orderData_1=0;?>
<div class="order_table_type">
    <table class="table c_table">
        <colgroup>
            <col style="width:15%"> <!-- 날짜/주문번호 -->
<?php if($TPL_VAR["mode"]==='backRegist'||$TPL_VAR["mode"]==='refundRegist'||$TPL_VAR["mode"]==='exchangeRegist'){?>
            <col style="width:5%">  <!-- 체크박스 -->
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
            <col style="width:100px"/>
<?php }?>
            <col>					<!-- 상품명/옵션 -->
            <col style="width:10%"> <!-- 상품금액/수량 -->
            <col style="width:10%"> <!-- 주문상태 -->
            <col style="width:10%"> <!-- 업로드상태 -->
            <col style="width:10%"> <!-- 확인/리뷰 -->
        </colgroup>
        <thead>
            <tr>
                <th><?php echo __('날짜/주문번호')?></th>
<?php if($TPL_VAR["mode"]==='backRegist'||$TPL_VAR["mode"]==='refundRegist'||$TPL_VAR["mode"]==='exchangeRegist'){?>
                <th>
                    <div class="form_element">
                        <input type="checkbox" id="allCheck" class="gd_checkbox_all" data-target-id="goodsno_" data-target-form="#frmClaimRegist" />
                        <label for="allCheck" class="check_s"></label>
                    </div>
                </th>
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
                <th><?php echo __('배송지')?></th>
<?php }?>
                <th><?php echo __('상품명/옵션')?></th>
                <th><?php echo __('상품금액/수량')?></th>
                <th><?php echo __('주문상태')?></th>
                <th><?php echo __('업로드상태')?></th>
                <th>
<?php if($TPL_VAR["mode"]=='backRegist'){?>
                    <?php echo __('반품수량')?>

<?php }elseif($TPL_VAR["mode"]=='refundRegist'){?>
                    <?php echo __('환불수량')?>

<?php }elseif($TPL_VAR["mode"]=='exchangeRegist'){?>
                    <?php echo __('교환수량')?>

<?php }else{?>
                    <?php echo __('확인/리뷰')?>

<?php }?>
                </th>
            </tr>
        </thead>
        <tbody>
<?php if($TPL_VAR["orderData"]){?>
<?php if($TPL_orderData_1){foreach($TPL_VAR["orderData"] as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1["goods"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_V2){$TPL_I2++;?>
            <tr class="<?php if($TPL_V1["orderGoodsCnt"]> 0&&($TPL_I2+ 1!=$TPL_V1["orderGoodsCnt"]||empty($TPL_V2["addGoods"])===false)){?>row_line<?php }?>" data-order-no="<?php echo $TPL_V1["orderNo"]?>" data-order-goodsno="<?php echo $TPL_V2["sno"]?>" data-order-status="<?php echo $TPL_V2["orderStatus"]?>" data-order-userhandlesno="<?php echo $TPL_V2["userHandleSno"]?>" >
<?php if($TPL_I2== 0){?>
                <td rowspan="<?php echo $TPL_V1["orderGoodsCnt"]+$TPL_V1["orderAddGoodsCnt"]?>" class="order_day_num">
                    <em><?php echo gd_date_format('Y/m/d',$TPL_V1["regDt"])?></em>
                    <a href="../mypage/order_view.php?orderNo=<?php echo $TPL_V1["orderNo"]?>" <?php if($TPL_VAR["mode"]!=='list'){?>target="_blank"<?php }?> class="order_num_link"><span><?php echo $TPL_V1["orderNo"]?></span></a>
                    <div class="btn_claim">
<?php if($TPL_V1["orderChannelFl"]!='naverpay'){?>
<?php if($TPL_VAR["mode"]!=='backRegist'&&$TPL_VAR["mode"]!=='refundRegist'&&$TPL_VAR["mode"]!=='exchangeRegist'&&$TPL_V1["orderSettleButton"]===true){?>
                        <span class="btn_gray_list"><a href="#orderSettleLayer" class="btn_gray_small btn_open_layer" data-order-no="<?php echo $TPL_V1["orderNo"]?>"><span><?php echo __('구매확정')?></span></a></span>
<?php }?>
<?php if(substr($TPL_V2["orderStatus"], 0, 1)=='o'){?>
                        <span class="btn_gray_list"><a href="#" class="btn_gray_small js_btn_order_cancel"><span><?php echo __('주문취소')?></span></a></span>
<?php }else{?>
<?php if($TPL_V1["canRefund"]==true&&gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true){?>
                        <span class="btn_gray_list"><a href="../mypage/layer_order_refund_regist.php?mode=refundRegist&orderNo=<?php echo $TPL_V1["orderNo"]?>" class="btn_gray_small"><span><?php echo __('환불신청')?></span></a></span>
<?php }?>
<?php if($TPL_V1["canBack"]==true&&gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true){?>
                        <span class="btn_gray_list"><a href="../mypage/layer_order_back_regist.php?mode=backRegist&orderNo=<?php echo $TPL_V1["orderNo"]?>" class="btn_gray_small"><span><?php echo __('반품신청')?></span></a></span>
<?php }?>
<?php if($TPL_V1["canExchange"]==true&&gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true){?>
                        <span class="btn_gray_list"><a href="../mypage/layer_order_exchange_regist.php?mode=exchangeRegist&orderNo=<?php echo $TPL_V1["orderNo"]?>" class="btn_gray_small"><span><?php echo __('교환신청')?></span></a></span>
<?php }?>
<?php }?>
<?php }?>
                    </div>
                </td>
<?php }?>
<?php if(($TPL_VAR["mode"]==='backRegist'&&$TPL_V2["canBack"]==true)||($TPL_VAR["mode"]==='refundRegist'&&$TPL_V2["canRefund"]==true)||($TPL_VAR["mode"]==='exchangeRegist'&&$TPL_V2["canExchange"]==true)){?>
<?php if($TPL_V2["userHandleSno"]> 0||$TPL_V2["handleSno"]> 0){?>
                <td></td>
<?php }elseif($TPL_V2["canBack"]==true||$TPL_V2["canRefund"]==true||$TPL_V2["canExchange"]==true){?>
                <td class="td_chk">
                    <div class="form_element">
                        <input type="checkbox" name="orderGoodsNo[]" value="<?php echo $TPL_V2["sno"]?>" id="goodsno_<?php echo $TPL_V2["sno"]?>" data-order-status="<?php echo $TPL_V2["orderStatus"]?>"/>
                        <label for="goodsno_<?php echo $TPL_V2["sno"]?>" class="check_s"></label>
                    </div>
                </td>
<?php }?>
<?php }elseif($TPL_VAR["mode"]==='backRegist'||$TPL_VAR["mode"]==='refundRegist'||$TPL_VAR["mode"]==='exchangeRegist'){?>
                <td></td>
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
<?php if($TPL_V2["orderInfoRow"]){?>
                <td rowspan="<?php echo $TPL_V2["orderInfoRow"]?>">
<?php if($TPL_V2["orderInfoCd"]== 1){?><?php echo __('메인')?><?php }else{?><?php echo __('추가')?><?php echo $TPL_V2["orderInfoCd"]- 1?><?php }?>
                </td>
<?php }else{?>
<?php }?>
<?php }?>
                <td class="td_left title">
                    <div class="pick_add_cont">
<?php if($TPL_V2["goodsType"]==='addGoods'){?>
                        <span class="pick_add_plus"><?php echo __('추가')?></span>
                        <span class="pick_add_img">
                            <?php echo $TPL_V2["goodsImage"]?>

                        </span>
<?php }else{?>
                        <span class="pick_add_img">
                            <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["goodsNo"]?>"><?php echo $TPL_V2["goodsImage"]?></a>
                        </span>
<?php }?>
                        <div class="pick_add_info">
<?php if($TPL_V2["handleMode"]==='z'){?>
                            <div><span class="exchange_add_info">교환추가</span></div>
<?php }?>

<?php if($TPL_V2["goodsType"]==='addGoods'){?>
                            <div style="margin-top:3px;">
                                <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["parentGoodsNo"]?>"><em><?php echo $TPL_V2["goodsNm"]?></em></a>
                            </div>
<?php }else{?>
                            <div style="margin-bottom:3px;">
                                <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["goodsNo"]?>"><em><?php echo $TPL_V2["goodsNm"]?></em></a>
                            </div>
<?php }?>

<?php if((is_array($TPL_R3=$TPL_V2["optionInfo"])&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_S3=count($TPL_R3);$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
                            <span class="text_type_cont">
                                <em><?php echo $TPL_V3["optionName"]?> :</em> <?php echo $TPL_V3["optionValue"]?>

<?php if($TPL_V3["optionRealPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
                                (<?php if($TPL_V3["optionRealPrice"]> 0){?>+<?php }?><?php echo gd_global_order_currency_display($TPL_V3["optionRealPrice"])?>)
<?php }?>
<?php if($TPL_I3==$TPL_S3- 1){?>
<?php if(empty($TPL_V3["deliveryInfoStr"])===false){?> [<?php echo $TPL_V3["deliveryInfoStr"]?>]<?php }?>
<?php }?>
                            </span>
<?php }}?>
<?php if($TPL_V2["optionTextInfo"]){?>
<?php if((is_array($TPL_R3=$TPL_V2["optionTextInfo"])&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_V3){?>
                            <span class="text_type_cont">
                                <em><?php echo $TPL_V3["optionName"]?> :</em> <?php echo $TPL_V3["optionValue"]?>

<?php if($TPL_V3["optionTextPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
                                (<?php if($TPL_V3["optionTextPrice"]> 0){?>+<?php }?><?php echo gd_global_order_currency_display($TPL_V3["optionTextPrice"])?>)
<?php }?>
                            </span>
<?php }}?>
<?php }?>
                        </div>
                    </div>
                    <!-- //pick_add_info -->
                </td>
                <td><strong><?php echo gd_global_order_currency_display((($TPL_V2["goodsPrice"]+$TPL_V2["optionPrice"]+$TPL_V2["optionTextPrice"])*$TPL_V2["goodsCnt"]),$TPL_V1["exchangeRate"],$TPL_V1["currencyPolicy"])?></strong> / <?php echo $TPL_V2["goodsCnt"]?><?php echo __('개')?></td>
                <td>
<?php if($TPL_V2["userHandleSno"]> 0&&$TPL_V2["handleSno"]== 0){?>
                    <em>
<?php if($TPL_V2["userHandleFl"]=='n'){?>
<?php if($TPL_V2["userHandleMode"]=='b'){?>
                        <?php echo __('반품거절')?>

<?php }elseif($TPL_V2["userHandleMode"]=='r'){?>
                        <?php echo __('환불거절')?>

<?php }elseif($TPL_V2["userHandleMode"]=='e'){?>
                        <?php echo __('교환거절')?>

<?php }?>
<?php }elseif($TPL_V2["userHandleFl"]=='y'){?>
<?php if($TPL_V2["userHandleMode"]=='b'){?>
                        <?php echo __('반품승인')?>

<?php }elseif($TPL_V2["userHandleMode"]=='r'){?>
                        <?php echo __('환불승인')?>

<?php }elseif($TPL_V2["userHandleMode"]=='e'){?>
                        <?php echo __('교환승인')?>

<?php }?>
<?php }else{?>
<?php if($TPL_V2["userHandleMode"]=='b'){?>
                        <?php echo __('반품신청')?>

<?php }elseif($TPL_V2["userHandleMode"]=='r'){?>
                        <?php echo __('환불신청')?>

<?php }elseif($TPL_V2["userHandleMode"]=='e'){?>
                        <?php echo __('교환신청')?>

<?php }?>
<?php }?>
                    </em>
<?php }else{?>
                    <em>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===false&&(substr($TPL_V2["orderStatus"], 0, 1)=='c'||$TPL_V2["orderStatus"]=='b4'||$TPL_V2["orderStatus"]=='e5'||$TPL_V2["orderStatus"]=='r3')&&$TPL_V2["handleDetailReasonShowFl"]=='y'){?>
                        <a href="#lyReason" class="order_num_link btn_open_layer">
<?php }?>

<?php if($TPL_V2["orderStatus"]=='d2'){?>
                            <?php echo $TPL_VAR["eachOrderStatus"]["d2"]["name"]?>

<?php }elseif($TPL_V2["orderStatus"]=='b4'){?>
                            <?php echo $TPL_VAR["eachOrderStatus"]["b4"]["name"]?>

<?php }elseif($TPL_V2["orderStatus"]=='e5'){?>
                            <?php echo $TPL_VAR["eachOrderStatus"]["e5"]["name"]?>

<?php }elseif($TPL_V2["orderStatus"]=='r3'){?>
                            <?php echo $TPL_VAR["eachOrderStatus"]["r3"]["name"]?>

<?php }else{?>
                            <?php echo $TPL_V2["orderStatusStr"]?>

<?php if($TPL_V2["naverpayStatus"]["code"]=='DelayProductOrder'){?>
                            (<?php echo $TPL_V2["naverpayStatus"]["notice"]?>)
<?php }?>
<?php }?>

<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===false&&(substr($TPL_V2["orderStatus"], 0, 1)=='c'||$TPL_V2["orderStatus"]=='b4'||$TPL_V2["orderStatus"]=='e5'||$TPL_V2["orderStatus"]=='r3')&&$TPL_V2["handleDetailReasonShowFl"]=='y'){?>
                        </a>
<?php }?>
                    </em>
<?php }?>
                    <div class="btn_gray_list">
<?php if((substr($TPL_V2["orderStatus"], 0, 1)=='d'||substr($TPL_V2["orderStatus"], 0, 1)=='s'||substr($TPL_V2["orderStatus"], 0, 1)=='z')&&$TPL_V2["invoiceNo"]){?>
<?php if($TPL_V2["deliveryMethodFl"]=='delivery'||!$TPL_V2["deliveryMethodFl"]){?>
                        <a href="#" class="btn_gray_small js_btn_delivery_trace" data-invoice-company-sno="<?php echo $TPL_V2["invoiceCompanySno"]?>" data-invoice-no="<?php echo $TPL_V2["invoiceNo"]?>"><span><?php echo __('배송추적')?></span></a>
<?php }else{?>
                        <div style="margin-bottom: 7px;">
                            (<a href="#deliveryMethodLayer" class="style-none btn_open_layer"><?php echo $TPL_V2["deliveryMethodFlText"]?></a>)
                        </div>
<?php }?>
<?php }?>
<?php if($TPL_V2["orderStatus"]=='d1'){?>
                        <a href="#" class="btn_gray_small js_btn_order_delivery"><span><?php echo __('수취확인')?></span></a>
<?php }?>
                    </div>
                </td>
                <td>
<?php if($TPL_V2["goodsType"]!=='addGoods'||($TPL_V2["goodsType"]==='addGoods'&&!empty($TPL_V2["goodsModelNo"]))){?>
                    <div class="btn_gray_list">
<?php if($TPL_V2["orderStatusStr"]==='o1'){?>
                        <div style="margin:5px 0;">
                            <a href="#" onclick="alert('결제 후 사진 등록이 가능합니다.'); return false;" class="button small gray"><span><?php echo __('사진올리기')?></span></a>
                        </div>
<?php }elseif($TPL_V2["orderStatus"]==='p1'){?>
                        <div style="margin:5px 0;">
<?php if($TPL_V2["goodsType"]==='addGoods'){?>
                            <a href="#" onclick="window.open('http://leveltest1.cafe24.com/files/aileen/index.php?picType=aileen&orderNo=<?php echo $TPL_V1["orderNo"]?>&goodsNo=<?php echo $TPL_V2["goodsModelNo"]?>', '사진올리기', 'width=600, height=700, toolbar=no, menubar=no, scrollbars=yes, resizable=yes'); return false;" class="button small orange"><span><?php echo __('사진올리기')?></span></a>
<?php }else{?>
                            <a href="#" onclick="window.open('http://leveltest1.cafe24.com/files/aileen/index.php?picType=aileen&orderNo=<?php echo $TPL_V1["orderNo"]?>&goodsNo=<?php echo $TPL_V2["goodsNo"]?>', '사진올리기', 'width=600, height=700, toolbar=no, menubar=no, scrollbars=yes, resizable=yes'); return false;" class="button small orange"><span><?php echo __('사진올리기')?></span></a>
<?php }?>
                        </div>
<?php }elseif($TPL_V2["orderStatus"]==='g7'){?>
                        <div style="margin:5px 0;">
                            <a href="#" onclick="window.open('http://leveltest1.cafe24.com/files/aileen/modify.php?picType=aileen&orderNo=<?php echo $TPL_V1["orderNo"]?>&goodsNo=<?php echo $TPL_V2["goodsNo"]?>', '수정요청', 'width=600, height=700, toolbar=no, menubar=no, scrollbars=yes, resizable=yes'); return false;" class="button small orange"><span><?php echo __('수정요청')?></span></a>
                        </div>
<?php }elseif($TPL_V2["orderStatus"]==='g8'){?>
                        <div style="margin:5px 0;">
                            <a href="#" onclick="window.open('http://leveltest1.cafe24.com/files/aileen/modify.php?picType=aileen&orderNo=<?php echo $TPL_V1["orderNo"]?>&goodsNo=<?php echo $TPL_V2["goodsNo"]?>', '영상확인', 'width=600, height=700, toolbar=no, menubar=no, scrollbars=yes, resizable=yes'); return false;" class="button small orange"><span><?php echo __('영상확인')?></span></a>
                        </div>
<?php }elseif($TPL_V2["orderStatus"]==='g9'){?>
                        <div style="margin:5px 0;">
                            <a href="#" onclick="window.open('http://leveltest1.cafe24.com/files/aileen/complete.php?picType=aileen&orderNo=<?php echo $TPL_V1["orderNo"]?>&goodsNo=<?php echo $TPL_V2["goodsNo"]?>', '영상다운로드', 'width=600, height=450, toolbar=no, menubar=no, scrollbars=yes, resizable=yes'); return false;" class="button small orange"><span><?php echo __('영상다운로드')?></span></a>
                        </div>
<?php }?>
                    </div>
<?php }?>
                </td>
                <td>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&($TPL_V2["userHandleSno"]> 0||($TPL_V2["handleSno"]> 0&&$TPL_V2["handleDetailReasonShowFl"]=='y'))){?>
                    <span class="btn_gray_list">
                        <a href="#lyReason" class="btn_gray_small btn_open_layer">
                            <span>
<?php if($TPL_V2["userHandleMode"]=='b'||$TPL_V2["handleMode"]=='b'){?>
                                    <?php echo __('반품사유')?>

<?php }elseif($TPL_V2["userHandleMode"]=='r'||$TPL_V2["handleMode"]=='r'){?>
                                    <?php echo __('환불사유')?>

<?php }elseif($TPL_V2["userHandleMode"]=='e'||$TPL_V2["handleMode"]=='e'){?>
                                    <?php echo __('교환사유')?>

<?php }elseif($TPL_V2["userHandleMode"]=='c'||$TPL_V2["handleMode"]=='c'){?>
                                    <?php echo __('취소사유')?>

<?php }?>
                            </span>
                        </a>
                    </span>
<?php }else{?>
<?php if($TPL_VAR["mode"]!=='backRegist'&&$TPL_VAR["mode"]!=='refundRegist'&&$TPL_VAR["mode"]!=='exchangeRegist'&&substr($TPL_V2["orderStatus"], 0, 1)=='d'){?>
                    <a href="#;" class="btn_buy_ok js_btn_order_settle" data-goods-no="<?php echo $TPL_V2["sno"]?>" style="display:none;"><span><?php echo __('구매확정')?></span></a>
<?php }elseif(($TPL_VAR["mode"]==='backRegist'&&$TPL_V2["canBack"]==true)||($TPL_VAR["mode"]==='refundRegist'&&$TPL_V2["canRefund"]==true)||($TPL_VAR["mode"]==='exchangeRegist'&&$TPL_V2["canExchange"]==true)){?>
                    <span class="count">
                        <span class="goods_qty">
                            <input type="number" name="claimGoodsCnt[<?php echo $TPL_V2["sno"]?>]" class="text" value="<?php echo $TPL_V2["goodsCnt"]?>" min="1" max="<?php echo $TPL_V2["goodsCnt"]?>" data-mode="<?php echo $TPL_VAR["mode"]?>">
                        </span>
                    </span>
<?php }?>
<?php }?>

<?php if($TPL_V2["viewWriteGoodsReview"]){?>
<?php if($TPL_V2["goodsType"]!=='addGoods'){?>
                    <div><a class="skinbtn point2  btn_review_write ogl_reviewrite btn_open_layer button small black" data-id="<?php echo $TPL_VAR["goodsReviewId"]?>" data-goods-no="<?php echo $TPL_V2["goodsNo"]?>" data-sno="<?php echo $TPL_V2["sno"]?>"  href="#writeReviewLayer"><em><?php echo __('리뷰쓰기')?></em></a></div>
<?php }?>
<?php }?>

<?php if($TPL_V2["viewWritePlusReview"]){?>
                    <div><a class="skinbtn point2 btn_review_write button small green" href="javascript:gd_popup_plus_review_write(<?php echo $TPL_V2["sno"]?>)"><em><?php echo __('리뷰등록')?></em></a></div>
<?php }?>

<?php if($TPL_V2["naverpayStatus"]["code"]=='DelayProductOrder'){?>
                    <a href="#orderDetailReasonLayer" class="btn_buy_ok btn_open_layer button small black" data-goods-no="<?php echo $TPL_V2["sno"]?>"><span><?php echo __('지연사유')?></span></a>
<?php }?>
                </td>
            </tr>
<?php if(empty($TPL_V2["addGoods"])===false){?>
<?php if((is_array($TPL_R3=$TPL_V2["addGoods"])&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
            <tr class="<?php if($TPL_V1["orderAddGoodsCnt"]> 0&&($TPL_I2+$TPL_I3)+ 2!=($TPL_V1["orderGoodsCnt"]+$TPL_V1["orderAddGoodsCnt"])){?>row_line<?php }?>">
                <td class="td_left">
                    <div class="pick_add_cont">
                        <span class="pick_add_plus"><?php echo __('추가')?></span>
                        <span class="pick_add_img">
                            <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["goodsNo"]?>"><?php echo $TPL_V3["goodsImage"]?></a>
                        </span>
                        <div class="pick_add_info">
                            <div style="margin-top:3px;">
                                <em><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["goodsNo"]?>"><?php echo $TPL_V3["goodsNm"]?></a></em>
                            </div>
                            <span class="text_type_cont"><em><?php echo __('옵션')?> : </em></span>
                            <span class="text_type_cont"><?php echo $TPL_V3["optionNm"]?></span>
                        </div>
                    </div>
                    <!-- //pick_add_info -->
                </td>
                <td><strong><?php echo gd_global_order_currency_display($TPL_V3["goodsPrice"],$TPL_V1["exchangeRate"],$TPL_V1["currencyPolicy"])?></strong>원 / <?php echo $TPL_V3["goodsCnt"]?><?php echo __('개')?></td>
                <td></td>
                <td></td>
            </tr>
<?php }}?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }else{?>
            <tr>
                <td colspan="8"><p class="no_data"><?php echo __('조회내역이 없습니다.')?></p></td>
            </tr>
<?php }?>
        </tbody>
    </table>
</div>


<!-- 사유 레이어 -->
<div id="lyReason" class="layer_wrap reason_layer dn" data-remote="../mypage/layer_order_refund_reason.php"></div>
<!--//사유 레이어 -->
<div id="lyWritePop" class="layer_wrap board_write_layer dn"></div>

<!-- 네이버페이 상세정보 레이어 -->
<div id="orderDetailReasonLayer" class="layer_wrap reason_layer dn" data-remote="../mypage/layer_order_naverpay_reason.php"></div>
<!--//네이버페이 상세정보 레이어 -->

<!-- 배송방식 상태 확인 레이어(택배제외) -->
<div id="deliveryMethodLayer" class="layer_wrap dn" data-remote="../mypage/layer_order_delivery_method.php"></div>
<!-- 배송방식 상태 확인 레이어(택배제외) -->

<!-- 구매확정 레이어 -->
<div id="orderSettleLayer" class="layer_wrap settle_layer dn" data-remote="../mypage/layer_order_settle.php"></div>
<!-- 구매확정 레이어 -->

<script type="text/javascript">
    $(function () {
        // 구매취소
        $('.js_btn_order_cancel').click(function (e) {
            if (confirm("<?php echo __('주문취소 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'cancelRegist',
                    orderNo: $(this).closest('tr').data('order-no'),
                    orderGoodsNo: $(this).closest('tr').data('order-goodsno'),
                    orderStatus: $(this).closest('tr').data('order-status')
                };

                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        // 구매확정
        $('.js_btn_order_settle').click(function (e) {
            if (confirm("<?php echo __('구매확정 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'settleRegist',
                    orderNo: $(this).closest('tr').data('order-no'),
                    orderGoodsNo: $(this).closest('tr').data('order-goodsno')
                };

                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        //구매확정레이어


        // 수취확인
        $('.js_btn_order_delivery').click(function (e) {
            if (confirm("<?php echo __('수취확인 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'deliveryCompleteRegist',
                    orderNo: $(this).closest('tr').data('order-no'),
                    orderGoodsNo: $(this).closest('tr').data('order-goodsno')
                };

                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        // 배송추적
        $('.js_btn_delivery_trace').click(function (e) {
            win = gd_popup({
                url: '../share/delivery_trace.php?invoiceCompanySno=' + $(this).data('invoice-company-sno') + '&invoiceNo=' + $(this).data('invoice-no'),
                target: 'trace',
                width: 650,
                height: 660,
                resizable: 'yes',
                scrollbars: 'yes'
            });
            win.focus();
            return win;
        });

        // 레이어 오픈 바인딩
        $('.btn_open_layer').bind('click', function (e) {
            e.preventDefault();
            // 일반 레이어 호출
            if ($(this).attr('href') == '#lyReason') {
                gd_call_layer_handler($(this));

                // 리뷰쓰기 레이어 호출
            } else if ($(this).attr('href') == '#writeReviewLayer') {
                var bdId = $(this).data('id');
                var goodsNo = $(this).data('goods-no');
                var sno = $(this).data('sno');
                gd_open_write_layer(bdId, goodsNo,sno);

                //구매확정 레이어 호출
            } else if ($(this).attr('href') == '#orderSettleLayer') {
                var target = $(this).attr('href');
                var url = $(target).data('remote');
                var params = {
                    orderNo : $(this).data('order-no')
                };

                $(target).empty();
                $.post(url, params, function (data) {
                    $(target).empty().html(data);
                    $(target).find('>div').center();
                });


                // 레이어 confirm 호출
            } else if ($(this).attr('href') == '#orderDetailReasonLayer') {
                var orderGoodsNo = $(this).data('goods-no');
                var target = $(this).attr('href');
                var url = $(target).data('remote');
                var params = {
                    orderNo: $(this).closest('tr').data('order-no'),
                    orderGoodsNo : orderGoodsNo
                };

                $(target).empty();
                $.post(url, params, function (data) {
                    if (!_.isUndefined(data.code) && data.code == 0) {
                        alert(data.message);
                        gd_close_layer();
                        return false;
                    }
                    $(target).empty().html(data);
                    $(target).find('>div').center();
                });

            } else if($(this).attr('href') == "#deliveryMethodLayer"){
                gd_call_layer_handler($(this));
            
            } else if($(this).attr('href') == '#goodsPicLayer') {
                var target = $(this).attr('href');
                var url = $(target).data('remote');
                var params = {
                    orderNo : $(this).data('orderNo'),
                    goodsNo : $(this).data('goodsNo')
                };
                
                $(target).empty();
                $.post(url, params, function (data) {
                    $(target).empty().html(data);
                    $(target).find('>div').center();
                });

            } else {
                if (confirm($(this).text() + "<?php echo __('처리를 하시겠습니까?')?>")) {
                    gd_call_layer_handler($(this));
                } else {
                    return false;
                }
            }
        });

        function gd_call_layer_handler(obj) {
            var target = obj.attr('href');
            var url = $(target).data('remote');
            var params = {
                orderNo: obj.closest('tr').data('order-no'),
                orderGoodsNo: obj.closest('tr').data('order-goodsno'),
                orderStatus: obj.closest('tr').data('order-status'),
                userHandleSno: obj.closest('tr').data('order-userhandlesno')
            };

            $(target).empty();
            $.post(url, params, function (data) {
                if (!_.isUndefined(data.code) && data.code == 0) {
                    alert(data.message);
                    gd_close_layer();
                    return false;
                }
                $(target).empty().html(data);
                $(target).find('>div').center();
            });
        }

        // 클레임 리스트일 경우 전체선택
        if ($('.gd_checkbox_all').length) {
            $('.gd_checkbox_all').trigger('click');
        }

        // 클레임신청 수량 초과 체크
        $('input[name*="claimGoodsCnt"]').on('keyup', function(){
            var mode = $(this).data('mode');
            var orginCnt = parseInt($(this).attr('max'));
            var claimCnt = parseInt($(this).val());
            switch (mode) {
                case 'refundRegist' :
                    mode = "<?php echo __('환불')?>";
                    break;
                case 'backRegist' :
                    mode = "<?php echo __('반품')?>";
                    break;
                case 'exchangeRegist' :
                    mode = "<?php echo __('교환')?>";
                    break;
            }
            if (claimCnt > orginCnt || claimCnt <= 0) {
                alert(mode + '수량은 주문수량 ' + orginCnt + ' 보다 큰값 또는 0 값을 입력할 수 없습니다.');
                $(this).val(orginCnt);
            }
        });

        // 체크박스 전체 선택상태에 따른 체크박스 변경처리
        $('input:checkbox[name="orderGoodsNo[]"]').click(function () {
            var checkedCount = 0;
            var eachCheckBox = $(this).closest('table').find('tbody input[name="orderGoodsNo[]"]:checkbox');
            eachCheckBox.each(function () {
                if ($(this).prop('checked') === true) {
                    checkedCount++;
                }
            });
            if (eachCheckBox.length == checkedCount) {
                $(this).closest('table').find('thead > tr > th:nth-child(2) input[id="allCheck"]').prop('checked', true);
                $(this).closest('table').find('thead > tr > th:nth-child(2) label[for="allCheck"]').addClass('on');
            } else {
                $(this).closest('table').find('thead > tr > th:nth-child(2) input[id="allCheck"]').prop('checked', false);
                $(this).closest('table').find('thead > tr > th:nth-child(2) label[for="allCheck"]').removeClass('on');
            }
        });

    });
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_goods.js" charset="utf-8"></script>