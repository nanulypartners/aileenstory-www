<?php /* Template_ 2.2.7 2020/05/24 15:56:35 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/coupon.html 000025565 */  $this->include_("includeWidget");
if (is_array($TPL_VAR["data"])) $TPL_data_1=count($TPL_VAR["data"]); else if (is_object($TPL_VAR["data"]) && in_array("Countable", class_implements($TPL_VAR["data"]))) $TPL_data_1=$TPL_VAR["data"]->count();else $TPL_data_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!--// 마이페이지 회원 요약정보 -->
        <div class="section_layer">
            <div class="title_area">
                <div class="title"><?php echo __('쿠폰')?></div>
                <div class="button_area">
                    <ul>
                        <li>
                            <a href="#couponregLayer" class="btn_coupon_register btn_open_layer link" style="font-size:18px;"><?php echo __('쿠폰등록')?> <i class="xi-angle-right-min"></i></a>
                        </li>
                    </ul>
                </div>
            </div><!-- .title_area -->
            <form name="frmCouponSearch" method="get">
                <input type="hidden" name="memberCouponState" value="">
                <input type="hidden" name="dateOpt" value="">
                <div class="search_area">
                    <div class="field">
                        <div class="key"><?php echo __('조회기간')?></div>
                        <div class="value">
                            <div class="date_check_list" data-target-name="wDate[]">
                                <button type="button" data-value="0"><?php echo __('오늘')?></button>
                                <button type="button" data-value="7">7<?php echo __('일')?></button>
                                <button type="button" data-value="15">15<?php echo __('일')?></button>
                                <button type="button" data-value="30">1<?php echo __('개월')?></button>
                                <button type="button" data-value="90">3<?php echo __('개월')?></button>
                                <button type="button" class="oneYear" data-value="365">1<?php echo __('년')?></button>
                            </div>
                            <div class="date_check_calendar">
                                <input type="text" id="picker2" name="wDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["search"]['wDate'][ 0]?>"> ~ <input type="text" name="wDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["search"]['wDate'][ 1]?>">
                            </div>
                            <div class="button_area">
                                <button type="submit" class="btn_date_check button orange"><em><?php echo __('조회')?></em></button>
                            </div>
                        </div><!-- .value -->
                    </div><!-- .field -->
                </div><!-- .search_area -->
            </form>
            <div class="mypage_lately_info">

                <div class="mypage_lately_info_cont">
    
                    <div class="mypage_breakdown_tab">
                        <ul>
                            <li <?php if($TPL_VAR["selected"]['memberCouponState']["y"]){?>class="on"<?php }?>><a href="#" onclick="gd_coupon_state('y')"><span><?php echo __('사용가능')?></span></a></li>
                            <li <?php if($TPL_VAR["selected"]['memberCouponState']["n"]){?>class="on"<?php }?>><a href="#" onclick="gd_coupon_state('n')"><span><?php echo __('사용불가')?></span></a></li>
                        </ul>
                    </div>
                    <!-- //mypage_breakdown_tab -->
    
                    <div class="mypage_table_type">
                        <span class="pick_list_day"><strong><?php echo $TPL_VAR["search"]['wDate'][ 0]?> ~ <?php echo $TPL_VAR["search"]['wDate'][ 1]?></strong> <?php echo __('까지의 쿠폰내역')?></span>
                        <table class="table c_table">
                            <colgroup>
                                <col style="width:70px;">
                                <col style="">	<!-- 쿠폰명 -->
                                <col style="width:17%"> <!-- 혜택 -->
                                <col style="width:15%"> <!-- 사용조건 -->
                                <col style="width:13%"> <!-- 제한조건 -->
                                <col style="width:20%"> <!-- 유효기간 -->
                                <col style="width:8%"> <!-- 발급일 -->
<?php if($TPL_VAR["barcodeDisplay"]=='y'){?>
                                <col style="width:8%"> <!-- 바코드 보기 -->
<?php }?>
                            </colgroup>
                            <thead>
                            <tr>
                                <th></th>
                                <th><?php echo __('쿠폰명')?></th>
                                <th><?php echo __('혜택')?></th>
                                <th><?php echo __('사용조건')?></th>
                                <th><?php echo __('제한조건')?></th>
                                <th><?php echo __('유효기간')?></th>
                                <th><?php echo __('발급일')?></th>
<?php if($TPL_VAR["barcodeDisplay"]=='y'){?>
                                <th><?php echo __('바코드<br />보기')?></th>
<?php }?>
                            </tr>
                            </thead>
                            <tbody>
<?php if($TPL_data_1){$TPL_I1=-1;foreach($TPL_VAR["data"] as $TPL_V1){$TPL_I1++;?>
                            <tr>
                                <td><img src="<?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponImage"]?>" width="70"></td>
                                <td class="title">
                                    <div class="mypage_coupon_name">
                                        <strong><?php echo $TPL_V1["couponNm"]?></strong>
                                    </div>
                                </td>
                                <td><strong><?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponBenefit"]?> <?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponKindType"]?></strong></td>
                                <td>
                                    <div class="btn_layer">
                                        <span class="btn_gray_list"><a href="#lyUseCase<?php echo $TPL_V1["memberCouponNo"]?>" class="btn_gray_small"><em><?php echo __('자세히보기')?></em></a></span>
    
                                        <!-- N : 사용가능조건 레이어 시작 -->
                                        <div id="lyUseCase<?php echo $TPL_V1["memberCouponNo"]?>" class="layer_area" style="display:none;">
                                            <div class="ly_wrap use_case_layer">
                                                <div class="ly_tit">
                                                    <strong><?php echo __('사용가능조건 보기')?></strong>
                                                </div>
                                                <div class="ly_cont">
                                                    <div class="use_case_list">
                                                        <dl>
                                                            <dt>[<?php echo __('사용범위')?>]</dt>
                                                            <dd><?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponDeviceType"]?></dd>
                                                        </dl>
<?php if($TPL_V1["couponMaxBenefitType"]=='y'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('최대 할인/적립 금액')?>]</dt>
                                                            <dd><?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_V1["couponMaxBenefit"])?><?php echo gd_currency_string()?> <?php echo __('할인/적립')?></dd>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponApplyMemberGroup"]){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 회원등급')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponApplyMemberGroup"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponApplyProductType"]=='provider'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 공급사')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponApplyProvider"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponApplyProductType"]=='category'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 카테고리')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponApplyCategory"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponApplyProductType"]=='brand'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 브랜드')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponApplyBrand"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponApplyProductType"]=='goods'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 상품')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponApplyGoods"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["goodsNm"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponMinOrderPrice"]> 0){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 가능 구매액')?>]</dt>
                                                            <dd><?php echo __('구매금액이 %s 이상인 경우',gd_currency_display($TPL_V1["couponMinOrderPrice"]))?></dd>
                                                        </dl>
<?php }?>
                                                        <dl>
                                                            <dt>[<?php echo __('쿠폰 중복사용 여부')?>]</dt>
                                                            <dd><?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponKindType"]?><?php echo __('별')?> <?php echo $TPL_VAR["convertArrData"][$TPL_I1]["couponApplyDuplicateType"]?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                                <!-- //ly_cont -->
                                                <a href="#lyUseCase<?php echo $TPL_V1["memberCouponNo"]?>" class="ly_close" style="display:none;"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                            </div>
                                            <!-- //ly_wrap -->
                                        </div>
                                        <!-- N : 사용가능조건 레이어 끝 -->
    
                                    </div>
                                </td>
                                <td>
<?php if($TPL_V1["couponExceptProviderType"]!='y'&&$TPL_V1["couponExceptCategoryType"]!='y'&&$TPL_V1["couponExceptBrandType"]!='y'&&$TPL_V1["couponExceptGoodsType"]!='y'){?>
    
<?php }else{?>
                                    <div class="btn_layer">
                                        <span class="btn_gray_list"><a href="#lyUseCaseLimit<?php echo $TPL_V1["memberCouponNo"]?>" class="btn_gray_small"><em><?php echo __('자세히보기')?></em></a></span>
    
                                        <!-- N : 제한조건 레이어 시작 -->
                                        <div id="lyUseCaseLimit<?php echo $TPL_V1["memberCouponNo"]?>" class="layer_area" style="display:none;">
                                            <div class="ly_wrap use_case_layer">
                                                <div class="ly_tit">
                                                    <strong><?php echo __('제한조건 보기')?></strong>
                                                </div>
                                                <div class="ly_cont">
                                                    <div class="use_case_list">
<?php if($TPL_V1["couponExceptProviderType"]=='y'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 불가 공급사')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponExceptProvider"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponExceptCategoryType"]=='y'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 불가 카테고리')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponExceptCategory"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponExceptBrandType"]=='y'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 불가 브랜드')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponExceptBrand"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["name"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
<?php if($TPL_V1["couponExceptGoodsType"]=='y'){?>
                                                        <dl>
                                                            <dt>[<?php echo __('사용 불가 상품')?>]</dt>
<?php if((is_array($TPL_R2=$TPL_V1["couponExceptGoods"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                            <dd><?php echo $TPL_V2["goodsNm"]?></dd>
<?php }}?>
                                                        </dl>
<?php }?>
                                                    </div>
                                                </div>
                                                <!-- //ly_cont -->
                                                <a href="#lyUseCaseLimit<?php echo $TPL_V1["memberCouponNo"]?>" class="ly_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                            </div>
                                            <!-- //ly_wrap -->
                                        </div>
                                        <!-- N : 제한조건 레이어 끝 -->
    
                                    </div>
<?php }?>
                                </td>
                                <td>
                                    <em class="coupon_date_day">
                                        <?php echo gd_date_format('Y-m-d H:i',$TPL_V1["memberCouponStartDate"])?>

                                        ~<?php echo gd_date_format('Y-m-d H:i',$TPL_V1["memberCouponEndDate"])?>

                                    </em>
<?php if($TPL_V1["memberCouponUsable"]=='YES'){?>
                                    <div class="coupon_before_use" style="margin-top:5px;"><?php echo __('사용가능')?></div>
<?php }elseif($TPL_V1["memberCouponUsable"]=='USE_CART'){?>
                                    <div class="coupon_cart_use" style="margin-top:5px;"><?php echo __('장바구니사용')?></div>
<?php }elseif($TPL_V1["memberCouponUsable"]=='USE_ORDER'){?>
                                    <div class="coupon_order_use" style="margin-top:5px;"><?php echo __('주문사용')?></div>
<?php }elseif($TPL_V1["memberCouponUsable"]=='EXPIRATION_START_PERIOD'){?>
                                    <div class="coupon_before_use" style="margin-top:5px;"><?php echo __('사용전')?></div>
<?php }elseif($TPL_V1["memberCouponUsable"]=='EXPIRATION_END_PERIOD'){?>
                                    <div class="coupon_expire_use" style="margin-top:5px;"><?php echo __('사용만료')?></div>
<?php }?>
                                </td>
                                <td><?php echo gd_date_format('Y-m-d',$TPL_V1["regDt"])?></td>
<?php if($TPL_VAR["barcodeDisplay"]=='y'){?>
                                <td class="btn_gray_list">
<?php if(gd_isset($TPL_V1["barcodeAuthKey"])){?>
                                    <a href="#viewBarcodeLayer" data-authkey="<?php echo $TPL_V1["barcodeAuthKey"]?>" class="btn_view_barcode btn_open_layer"><span><?php echo __('보기')?></span></a>
<?php }?>
                                </td>
<?php }?>
                            </tr>
<?php }}?>
    
                            </tbody>
                        </table>
                    </div>
    
                </div>
                <!-- //mypage_lately_info_cont -->
            </div>
            <!-- //mypage_lately_info -->
    
            <div class="pagination">
                <?php echo $TPL_VAR["page"]->getPage()?>

            </div>
            <!-- //pagination -->
        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section>

<!-- 쿠폰등록 레이어 -->
<?php if(gd_use_coupon_offline()){?>
<div id="couponregLayer" class="layer_wrap coupon_register_layer dn">
    <div class="layer_wrap_cont">
        <form name="frmCouponOffline" id="frmCouponOffline" method="post" action="coupon_ps.php">
            <input type="hidden" name="mode" value="couponOfflineRegist">
            <div class="ly_tit">
                <h4><?php echo __('쿠폰등록')?></h4>
            </div>
            <div class="ly_cont">
                <div class="coupon_register_cont">
                    <p><strong><?php echo __('발급 받으신 쿠폰 인증 번호를 아래에 입력해주세요.')?></strong> (8<?php echo __('자리')?> ~ <?php echo __('최대')?> 12<?php echo __('자리')?>)</p>
                    <span><input type="text" name="couponOfflineNumber" maxlength="12" class="text" /></span>
                </div>
                <!-- //coupon_register_cont -->
                <div class="btn_center_box button_area">
                    <button type="button" class="btn_ly_cancel layer_close button inline small gray"><strong><?php echo __('취소')?></strong></button>
                    <button class="btn_ly_save button inline small orange"><strong><?php echo __('등록')?></strong></button>
                </div>
            </div>
            <!-- //ly_cont -->
            <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
        </form>
    </div>
    <!-- //layer_wrap_cont -->
</div>
<!-- //layer_wrap -->
<?php }?>
<!-- //쿠폰등록 레이어 -->

<!-- 바코드 보기 레이어 -->
<div id="viewBarcodeLayer" class="layer_wrap view_barcode_layer dn"></div>
<!--//바코드 보기 레이어 -->

<script type="text/javascript">
    <!--
    $(document).ready(function () {
        $("#frmCouponOffline").validate({
            submitHandler: function (form) {
                form.target = 'ifrmProcess';
                form.submit();
            },
            rules: {
                couponOfflineNumber: {
                    required: true,
                    minlength: 8,
                    maxlength: 12,
                },
            },
            messages: {
                couponOfflineNumber: {
                    required: "<?php echo __('쿠폰 인증 번호를 입력하세요.')?>",
                    minlength: "<?php echo __('쿠폰 인증 번호의 길이는 최소 8자 입니다.')?>",
                    maxlength: "<?php echo __('쿠폰 인증 번호의 길이는 최대 12자 입니다.')?>",
                },
            }
        });

        // 인풋박스 선택 이벤트
        if ($('.js_datepicker').length) {
            $('.js_datepicker').datetimepicker({
                locale: '<?php echo $TPL_VAR["gGlobal"]["locale"]?>',
                format: 'YYYY-MM-DD',
                dayViewHeaderFormat: 'YYYY MM',
                viewMode: 'days',
                ignoreReadonly: true,
                debug: false,
                keepOpen: false
            });
        }

        // 기간버튼 선택 이벤트
        if ($('.date_check_list').length) {
            $('.date_check_list button').click(function (e) {
                $startDate = $endDate = '';
                $period = $(this).data('value');
                $elements = $('input[name="' + $(this).closest('.date_check_list').data('target-name') + '"]');
                $format = $($elements[0]).data('DateTimePicker').format();
                if ($period >= 0) {
                    $startDate = moment().hours(0).minutes(0).seconds(0).subtract($period, 'days').format($format);
                    $endDate = moment().hours(0).minutes(0).seconds(0).format($format);
                }
                $($elements[0]).val($startDate);
                $($elements[1]).val($endDate);
                $('.date_check_list button').removeClass('on');
                $(this).addClass('on');
            });

            // 선택된 버튼에 따른 값 초기화
            $elements = $('input[name*=\'' + $('.date_check_list').data('target-name') + '\']');
            if ($elements.length && $elements.val() != '') {
                $interval = moment($($elements[1]).val()).diff(moment($($elements[0]).val()), 'days');
                $('.date_check_list').find('button[data-value="' + $interval + '"]').trigger('click');
            } else {
                $('.date_check_list').find('button[data-value="-1"]').trigger('click');
            }
        }

        //바코드 보기
        $('.btn_view_barcode').bind('click', function(e) {
            var authkey = $(this).data('authkey');
            // AJAX 호출
            $('#viewBarcodeLayer').empty();
            $.post('/mypage/layer_view_barcode.php', {authkey : authkey}, function(data) {
                $('#viewBarcodeLayer').append(data);
                $('#viewBarcodeLayer').find('>div').currentCenter();
                $('#viewBarcodeLayer > .layer_wrap_cont').css({'width':'400px'});
            });
        });

    });

    function gd_coupon_state(flag){
        $('input:hidden[name="memberCouponState"]').val(flag);
        document.frmCouponSearch.submit();
    }
    //-->
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>