<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/hack_out.html 000005265 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/member.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!--// 마이페이지 회원 요약정보 -->
        <div class="my_page_member join_section login_section">
            <form id="formHackOut" name="formHackOut" action="../mypage/my_page_ps.php" method="post">
                <input type="hidden" name="mode" value="hackOut"/>
                <input type="hidden" name="snsType" value="<?php echo $TPL_VAR["snsType"]?>"/>
                <div class="hack_out join_layer login_layer">
                    <div class="title_area">
                        <?php echo __('회원탈퇴')?>

                    </div>
                    <div class="mypage_unregister">
                        <div class="mypage_zone_tit">
                            <h3>01.<?php echo __('회원탈퇴 안내')?></h3>
                        </div>
    
                        <div class="unregister_info">
                            <?php echo nl2br($TPL_VAR["hackOutGuideContent"])?>

                        </div>
                        <!-- //unregister_info -->
    
                        <div class="mypage_zone_tit">
                            <h3>02.<?php echo __('회원탈퇴 하기')?></h3>
                        </div>
    
                        <div class="mypage_table_type">
                            <table class="table normal">
                                <colgroup>
                                    <col style="width:15%;">
                                    <col style="width:85%;">
                                </colgroup>
                                <tbody>
<?php if($TPL_VAR["snsType"]!='naver'){?>
<?php if($TPL_VAR["snsType"]!='kakao'){?>
                                <tr>
                                    <th scope="row"><?php echo __('비밀번호')?></th>
                                    <td><input type="password" name="memPw" class="text" style="width:300px;"></td>
                                </tr>
<?php }?>
<?php }?>
                                <tr>
                                    <th scope="row"><?php echo __('탈퇴사유')?></th>
                                    <td>
                                        <div class="form_element">
                                            <?php echo $TPL_VAR["reasonCodeRadio"]?>

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><?php echo __('남기실 말씀')?></th>
                                    <td><textarea cols="30" rows="5"  name="reasonDesc" class="text" style="width:100%;"></textarea></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- //mypage_unregister -->
    
                </div>
                <!-- //hack_out -->
    
                <div class="btn_center_box button_area inline" style="margin-top:30px;">
                    <a href="#" class="btn_claim_cancel btn_prev button orange"><em><?php echo __('이전으로')?></em></a>
                    <button type="submit" class="btn_claim_ok button gray"><em><?php echo __('탈퇴')?></em></button>
                </div>
            </form>
        </div>
    </div><!-- .center_layer -->
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formHackOut').validate({
            rules: {
                memPw: "required",
                "reasonCd[]": "required"
            }, messages: {
                memPw: "<?php echo __('비밀번호를 입력해주세요')?>",
                "reasonCd[]": "<?php echo __('탈퇴사유를 1개 이상 체크하여 주시기바랍니다.')?>"
            },
            submitHandler: function (form) {
                var params = $(form).serializeArray();
                var mileage = "<?php echo $TPL_VAR["memberData"]['mileage']?>";
                if (confirm("<?php echo __('현재 보유중인 회원혜택')?> : <?php echo __('쿠폰')?> " + "<?php echo $TPL_VAR["memberCouponCount"]?>" + "<?php echo __('장')?> / <?php echo __('마일리지')?>" + mileage + " \n<?php echo __('탈퇴 시 보유중인  회원혜택은 모두 삭제됩니다.')?> <?php echo __('정말로 탈퇴하시곘습니까?')?>")) {
                    form.action = '../mypage/hack_out_ps.php';
                    form.target = 'ifrmProcess';
                    form.submit();
                }
            }
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>