<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/index.html 000001492 */  $this->include_("includeWidget","includeFile");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!--// 마이페이지 회원 요약정보 -->
        <div class="section_layer">
            <div class="title_area">
                <div class="title">최근 주문 정보</div>
                <div class="button_area">
                    <ul>
                        <li>
                            <a href="../mypage/order_list.php" class="link">더보기 <i class="xi-angle-right-min"></i></a>
                        </li>
                    </ul>
                </div>
            </div><!-- .title_area -->
            <!-- 주문상품 리스트 -->
            <?php echo includeFile('mypage/_order_goods_list.html')?>

            <!--// 주문상품 리스트 -->
        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section><!-- .section_container -->
<?php $this->print_("footer",$TPL_SCP,1);?>