<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/my_page_password.html 000008383 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!--// 마이페이지 회원 요약정보 -->
        <div class="my_page_member my_page_password">
            <div class="title_area">
                <?php echo __('회원정보 변경')?>

                <span class="explain"><i class="xi-check-circle important text_pink"></i><?php echo $TPL_VAR["messages"]['info']?></span>
            </div>
            <form id="formFind">
                <table class="table_left table no_header">
                    <colgroup>
                        <col style="width:15%;">
                        <col style="width:85%;">
                    </colgroup>
                    <tbody>
<?php if($TPL_VAR["usePayco"]){?>
                        <tr>
                            <th><?php echo __('페이코 로그인')?></th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="button" class="btn_pw_certify js_btn_payco_login button small orange" data-payco-type="my_page_password"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }elseif($TPL_VAR["useFacebook"]){?>
                        <tr>
                            <th><?php echo __('페이스북 로그인')?></th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="button" class="btn_pw_certify js_btn_facebook_re_auth button small orange" data-sns-type="my_page_password" data-re-authentication-url="<?php echo $TPL_VAR["facebookReAuthenticationUrl"]?>"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }elseif($TPL_VAR["useNaver"]){?>
                        <tr>
                            <th><?php echo __('네이버 아이디 로그인')?></th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="button" class="btn_pw_certify js_btn_naver_login button small orange" data-naver-type="<?php echo $TPL_VAR["naverType"]?>"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }elseif($TPL_VAR["useKakao"]){?>
                        <tr>
                            <th><?php echo __('카카오 아이디 로그인')?></th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="button" data-return-url="../mypage/my_page.php" class="btn_pw_certify js_btn_kakao_login button small orange" data-kakao-type="<?php echo $TPL_VAR["kakaoType"]?>"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }elseif($TPL_VAR["useWonder"]){?>
                        <tr>
                            <th><?php echo __('원더 로그인')?></th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="button" class="btn_pw_certify js_btn_wonder_login button small orange" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>" data-wonder-type="<?php echo $TPL_VAR["wonderType"]?>"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }else{?>
                        <tr>
                            <th><?php echo __('아이디')?></th>
                            <td><strong><?php echo $TPL_VAR["data"]['memId']?></strong></td>
                        </tr>
                        <tr>
                            <th><?php echo __('비밀번호')?></th>
                            <td>
                                <div class="member_warning">
                                    <input type="password" name="findPassword" id="findPassword" class="text">
                                    <div class="text_warning help">
                                        <?php echo __('비밀번호가 일치하지 않습니다.')?>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>인증</th>
                            <td>
                                <div class="button_area inline left">
                                    <button type="submit" class="btn_pw_certify button small orange"><em><?php echo __('인증하기')?></em></button>
                                    <a href="#" onclick="return false;" class="btn_pw_cancel js_btn_back button small gray"><em><?php echo __('취소')?></em></a>
                                </div>
                            </td>
                        </tr>
<?php }?>
                    </tbody>
                </table>
            </form>
        </div>
        <!-- //my_page_password -->
    </div><!-- .center_layer -->
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.js_btn_back').click(function () {
            window.location.href = '../mypage/index.php';
        });

        $('#formFind').validate({
            dialog: false,
            rules: {
                findPassword: {
                    required: true
                }
            },
            messages: {
                findPassword: {
                    required: "<?php echo __('패스워드를 입력해주세요')?>"
                }
            }, submitHandler: function (form) {
                var $target = $(':password#findPassword', form);
                if (!_.isEmpty($target.val())) {
                    var $ajax = $.ajax('../mypage/my_page_ps.php', {
                        type: "post", data: {
                            memPw: $target.val(),
                            mode: "verifyPassword"
                        }
                    });
                    $ajax.done(function (data, textStatus, jqXHR) {
                        console.log('gd_member ajax', data, textStatus, jqXHR);
                        var code = data.code;
                        var message = data.message;
                        if (_.isUndefined(code) && _.isUndefined(message)) {
                            top.location.href = "../mypage/my_page.php";
                        } else {
                            $(form).find('.member_warning').addClass('prior_wrong');
                            $(form).find('.text_warning').text(message);
                        }
                    });
                }
            }
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>