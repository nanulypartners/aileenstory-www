<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/order_list.html 000005953 */  $this->include_("includeWidget","includeFile");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!--// 마이페이지 회원 요약정보 -->
        <div class="section_layer">
            <div class="title_area">
                <div class="title">주문목록/배송조회</div>
            </div><!-- .title_area -->
            <form method="get" name="frmDateSearch">
                <div class="search_area">
                    <div class="field">
                        <div class="key">조회기간</div>
                        <div class="value">
                            <div class="date_check_list" data-target-name="wDate[]">
                                <button type="button" data-value="0"><?php echo __('오늘')?></button>
                                <button type="button" data-value="7">7<?php echo __('일')?></button>
                                <button type="button" data-value="15">15<?php echo __('일')?></button>
                                <button type="button" data-value="30">1<?php echo __('개월')?></button>
                                <button type="button" data-value="90">3<?php echo __('개월')?></button>
                                <button type="button" class="oneYear" data-value="365">1<?php echo __('년')?></button>
                            </div>
                            <div class="date_check_calendar">
                                <input type="text" id="picker2" name="wDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["wDate"][ 0]?>"> ~ <input type="text" name="wDate[]" class="anniversary js_datepicker" value="<?php echo $TPL_VAR["wDate"][ 1]?>">
                            </div>
                            <div class="button_area">
                                <button type="submit" class="btn_date_check button orange"><em><?php echo __('조회')?></em></button>
                            </div>
                        </div><!-- .value -->
                    </div><!-- .field -->
                </div><!-- .search_area -->
            </form>
            <!-- 주문상품 리스트 -->
            <?php echo includeFile('mypage/_order_goods_list.html')?>

            <!--// 주문상품 리스트 -->
            <div class="pagination">
                <?php echo $TPL_VAR["page"]->getPage()?>

            </div>
            <!-- //pagination -->
        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section>
<script type="text/javascript">
    $(function(){
        // 인풋박스 선택 이벤트
        if ($('.js_datepicker').length) {
            var today = new Date();
            var minDate = new Date();
            minDate.setDate(today.getDate() - 365);
            minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());

            $('.js_datepicker').datetimepicker({
                locale: '<?php echo $TPL_VAR["gGlobal"]["locale"]?>',
                format: 'YYYY-MM-DD',
                dayViewHeaderFormat: 'YYYY MM',
                viewMode: 'days',
                ignoreReadonly: true,
                debug: false,
                keepOpen: false,
                maxDate: today,
                //minDate: minDate
            });

            //1년 이상 데이터 조회시 1년기간버튼 선택 이벤트
            $inputDate = $('input[name="wDate[]"]');
            var startDate = ($($inputDate[0]).val()).split('-');
            startDate = new Date(startDate[0], startDate[1], startDate[2]);

            var endDate = ($($inputDate[1]).val()).split('-');
            endDate = new Date(endDate[0], endDate[1], endDate[2]);
            var period = (endDate-startDate)/(24*3600*1000);

            if(period > 365){
                $('.date_check_list button').removeClass('on');
                $('.oneYear').addClass('on');
            }
        }

        // 기간버튼 선택 이벤트
        if ($('.date_check_list').length) {
            $('.date_check_list button').click(function (e) {
                $startDate = $endDate = '';
                $period = $(this).data('value');
                $elements = $('input[name="' + $(this).closest('.date_check_list').data('target-name') + '"]');
                $format = $($elements[0]).data('DateTimePicker').format();
                if ($period >= 0) {
                    $startDate = moment().hours(0).minutes(0).seconds(0).subtract($period, 'days').format($format);
                    $endDate = moment().hours(0).minutes(0).seconds(0).format($format);
                }
                $($elements[0]).val($startDate);
                $($elements[1]).val($endDate);
                $('.date_check_list button').removeClass('on');
                $(this).addClass('on');
            });

            // 선택된 버튼에 따른 값 초기화
            $elements = $('input[name*=\'' + $('.date_check_list').data('target-name') + '\']');
            if ($elements.length && $elements.val() != '') {
                $interval = moment($($elements[1]).val()).diff(moment($($elements[0]).val()), 'days');
                $('.date_check_list').find('button[data-value="' + $interval + '"]').trigger('click');
            } else {
                $('.date_check_list').find('button[data-value="-1"]').trigger('click');
            }
        }
    });
</script>

<?php $this->print_("footer",$TPL_SCP,1);?>