<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/mypage/order_view.html 000080154 */  $this->include_("includeWidget","includeFile");
if (is_array($TPL_VAR["emailDomain"])) $TPL_emailDomain_1=count($TPL_VAR["emailDomain"]); else if (is_object($TPL_VAR["emailDomain"]) && in_array("Countable", class_implements($TPL_VAR["emailDomain"]))) $TPL_emailDomain_1=$TPL_VAR["emailDomain"]->count();else $TPL_emailDomain_1=0;
if (is_array($TPL_VAR["totalRefundData"])) $TPL_totalRefundData_1=count($TPL_VAR["totalRefundData"]); else if (is_object($TPL_VAR["totalRefundData"]) && in_array("Countable", class_implements($TPL_VAR["totalRefundData"]))) $TPL_totalRefundData_1=$TPL_VAR["totalRefundData"]->count();else $TPL_totalRefundData_1=0;
if (is_array($TPL_VAR["exchangeHandleData"])) $TPL_exchangeHandleData_1=count($TPL_VAR["exchangeHandleData"]); else if (is_object($TPL_VAR["exchangeHandleData"]) && in_array("Countable", class_implements($TPL_VAR["exchangeHandleData"]))) $TPL_exchangeHandleData_1=$TPL_VAR["exchangeHandleData"]->count();else $TPL_exchangeHandleData_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/mypage.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 마이페이지 회원 요약정보 -->
        <?php echo includeWidget('mypage/_member_summary.html')?>

        <!-- 마이페이지 회원 요약정보 -->
        <div class="section_layer">
            <div class="content order_wrap">
                <div class="mypage_main">
                    <div class="mypage_lately_info">
                        <div class="order_zone_tit">
                            <?php echo __('주문/배송상세')?>

                            <strong class="order_num_view">(<?php echo __('주문번호')?> : <?php echo $TPL_VAR["orderInfo"]["orderNo"]?> )</strong>
                        </div>
                        <div class="mypage_lately_info_cont">
                            <!-- 주문상품 리스트 -->
                            <?php echo includeFile('mypage/_order_goods_list.html')?>

                            <!--// 주문상품 리스트 -->
                        </div>
                        <!-- //mypage_lately_info_cont -->
                    </div>
                    <!-- //mypage_lately_info -->

                    <div class="order_view_info">

<?php if($TPL_VAR["orderInfo"]["gift"]){?>
                        <div class="orderer_info order_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('사은품 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["orderInfo"]["gift"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                                    <tr>
<?php if($TPL_I1== 0){?>
                                        <th scope="row" rowspan="<?php echo $TPL_VAR["totalGiftCnt"]?>"><?php echo __('사은품 정보')?></th>
<?php }?>
                                        <td><?php echo $TPL_V1["imageUrl"]?> <?php echo $TPL_V1["giftNm"]?> (<?php echo $TPL_V1["giveCnt"]?>개) | <?php echo $TPL_V1["presentTitle"]?></td>
                                    </tr>
<?php }}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php }?>


                        <div class="orderer_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('주문자 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><?php echo __('주문자 정보')?></th>
                                        <td><?php echo $TPL_VAR["orderInfo"]["orderName"]?></td>
                                    </tr>
<?php if(gd_is_login()==true&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                                    <tr>
                                        <th scope="row"><?php echo __('주소')?></th>
                                        <td><?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            (<?php echo gd_isset($TPL_VAR["orderInfo"]["orderZonecode"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["orderAddressSub"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderAddress"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderState"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderCity"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderCountry"])?>

<?php }else{?>
                                            (<?php echo $TPL_VAR["orderInfo"]["orderZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["orderAddress"]?> <?php echo $TPL_VAR["orderInfo"]["orderAddressSub"]?>

<?php }?>
                                        </td>
                                    </tr>
<?php }?>
                                    <tr>
                                        <th scope="row"><?php echo __('전화번호')?></th>
                                        <td><?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            (+<?php echo gd_isset($TPL_VAR["orderInfo"]["orderPhonePrefix"])?>)
<?php }?>
                                            <?php echo $TPL_VAR["orderInfo"]["orderPhone"]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('휴대폰 번호')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            (+<?php echo gd_isset($TPL_VAR["orderInfo"]["orderCellPhonePrefix"])?>)
<?php }?>
                                            <?php echo $TPL_VAR["orderInfo"]["orderCellPhone"]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('이메일')?></th>
                                        <td><?php echo $TPL_VAR["orderInfo"]["orderEmail"]?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- //orderer_info -->

                        <div class="delivery_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('배송지 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'&&$TPL_VAR["multiOrderInfo"]){?>
                                    <tr>
                                        <th colspan="2" scope="row"><?php echo __('메인 배송지')?></th>
                                    </tr>
<?php }?>
                                    <tr>
                                        <th scope="row"><?php echo __('배송자 정보')?></th>
                                        <td> <?php echo $TPL_VAR["orderInfo"]["receiverName"]?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('주소')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            (<?php echo $TPL_VAR["orderInfo"]["receiverZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["receiverAddressSub"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverAddress"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverState"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverCity"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverCountry"]?>

<?php }else{?>
                                            (<?php echo $TPL_VAR["orderInfo"]["receiverZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["receiverAddress"]?> <?php echo $TPL_VAR["orderInfo"]["receiverAddressSub"]?>

<?php }?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('전화번호')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["orderInfo"]["receiverPhonePrefix"]?>)<?php }?>
                                            <?php echo $TPL_VAR["orderInfo"]["receiverPhone"]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('휴대폰 번호')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["orderInfo"]["receiverCellPhonePrefix"]?>)<?php }?>
                                            <?php echo $TPL_VAR["orderInfo"]["receiverCellPhone"]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('남기실 말씀')?></th>
                                        <td><?php echo $TPL_VAR["orderInfo"]["orderMemo"]?></td>
                                    </tr>
                                    </tbody>
                                </table>

<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'&&$TPL_VAR["multiOrderInfo"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["multiOrderInfo"]["receiverNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                                <table class="table_left table no_header" style="margin-top:10px;">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th colspan="2" scope="row"><?php echo __('추가 배송지')?> <?php echo $TPL_I1+ 1?></th>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('배송자 정보')?></th>
                                        <td>
                                            <?php echo $TPL_VAR["multiOrderInfo"]["receiverNm"][$TPL_I1]?>

                                            <!--<button type="button" class="normal-btn small1" onclick="pg_receipt_view('cash', '<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>');"><em>배송지 변경</em></button>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('주소')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            (<?php echo $TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1]?>) <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverState"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverCity"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverCountry"][$TPL_I1]?>

<?php }else{?>
                                            (<?php echo $TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1]?>) <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1]?> <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1]?>

<?php }?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('전화번호')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["multiOrderInfo"]["receiverPhonePrefix"][$TPL_I1]?>)<?php }?>
                                            <?php echo $TPL_VAR["multiOrderInfo"]["receiverPhone"][$TPL_I1]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('휴대폰 번호')?></th>
                                        <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["multiOrderInfo"]["receiverCellPhonePrefix"][$TPL_I1]?>)<?php }?>
                                            <?php echo $TPL_VAR["multiOrderInfo"]["receiverCellPhone"][$TPL_I1]?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('남기실 말씀')?></th>
                                        <td><?php echo $TPL_VAR["multiOrderInfo"]["orderMemo"][$TPL_I1]?></td>
                                    </tr>
                                    </tbody>
                                </table>
<?php }}?>
<?php }?>
                            </div>
                        </div>
                        <!-- //delivery_info -->

<?php if(empty($TPL_VAR["orderInfo"]["addField"])===false){?>
                        <div class="addition_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('추가 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["orderInfo"]["addField"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["process"]=='goods'){?>
<?php if((is_array($TPL_R2=$TPL_V1["data"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                                    <tr>
                                        <th><?php echo $TPL_V1["name"]?> : <?php echo $TPL_V1["goodsNm"][$TPL_K2]?></th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $TPL_V2?></td>
                                    </tr>
<?php }}?>
<?php }else{?>
                                    <tr>
                                        <th><?php echo $TPL_V1["name"]?></th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $TPL_V1["data"]?></td>
                                    </tr>
<?php }?>
<?php }}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php }?>
                        <!-- //addition_info -->

                        <div class="payment_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('결제정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><?php echo __('상품 합계 금액')?></th>
                                        <td><?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalGoodsPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></td>
                                    </tr>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                    <tr>
                                        <th scope="row"><?php echo __('총 무게')?></th>
                                        <td>
                                            <?php echo number_format($TPL_VAR["orderInfo"]["totalDeliveryWeight"], 2)?>kg
                                            <?php echo __('상품무게 %s kg + 박스무게 %s kg',number_format($TPL_VAR["orderInfo"]["deliveryWeightInfo"]["goods"], 2),number_format($TPL_VAR["orderInfo"]["deliveryWeightInfo"]["box"], 2))?>

                                        </td>
                                    </tr>
<?php }?>
                                    <tr>
                                        <th scope="row"><?php echo __('배송비')?></th>
                                        <td>
                                            <?php echo gd_global_order_currency_display($TPL_VAR["orderDeliveryInfo"]["deliveryPolicyCharge"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?>

<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'){?>
<?php if($TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"]){?>
                                            (<?php if((is_array($TPL_R1=$TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?><?php echo __('메인 배송지')?><?php }else{?><?php echo __('추가 배송지')?><?php echo $TPL_I1?><?php }?> : <?php echo gd_global_currency_display(gd_isset($TPL_V1))?>

<?php }}?>)
<?php }?>
<?php }?>
                                        </td>
                                    </tr>
<?php if($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]> 0){?>
                                    <tr>
                                        <th scope="row"><?php echo __('지역별 배송비')?></th>
                                        <td><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]))?></td>
                                    </tr>
<?php }?>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                    <tr>
                                        <th scope="row"><?php echo __('해외배송 보험료')?></th>
                                        <td><strong><?php echo gd_global_order_currency_display(gd_isset($TPL_VAR["orderInfo"]["totalDeliveryInsuranceFee"]),$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong></td>
                                    </tr>
<?php }?>
                                    <tr>
                                        <th scope="row"><?php echo __('할인혜택')?></th>
                                        <td>
                                            <div class="discount_benefit">
                                                <dl>
                                                    <dt><?php echo __('상품')?></dt>
                                                    <dd>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalGoodsDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
                                                </dl>
<?php if($TPL_VAR["orderInfo"]["totalMyappDcPrice"]> 0){?>
                                                <dl>
                                                    <dt><?php echo __('모바일앱')?></dt>
                                                    <dd>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalMyappDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
                                                </dl>
<?php }?>
<?php if(gd_is_login()==true){?>
<?php if($TPL_VAR["orderInfo"]["useMileage"]> 0){?>
                                                <dl>
                                                    <dt><?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
                                                    <dd>(-) <?php echo gd_global_order_money_format($TPL_VAR["orderInfo"]["useMileage"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"]["globalCurrencyDecimal"])?> <?php echo $TPL_VAR["mileageUse"]["unit"]?></dd>
                                                </dl>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["useDeposit"]> 0){?>
                                                <dl>
                                                    <dt><?php echo $TPL_VAR["depositUse"]["name"]?></dt>
                                                    <dd>(-) <?php echo gd_global_order_money_format($TPL_VAR["orderInfo"]["useDeposit"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"]["globalCurrencyDecimal"])?> <?php echo $TPL_VAR["depositUse"]["unit"]?></dd>
                                                </dl>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])> 0){?>
                                                <dl>
                                                    <dt><?php echo __('회원')?></dt>
                                                    <dd>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
                                                </dl>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"])> 0){?>
                                                <dl>
                                                    <dt><?php echo __('쿠폰')?></dt>
                                                    <dd>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
                                                </dl>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["totalEnuriDcPrice"]!= 0){?>
                                                <dl>
                                                    <dt><?php echo __('운영자 추가 할인')?></dt>
                                                    <dd>
<?php if($TPL_VAR["orderInfo"]["totalEnuriDcPrice"]< 0){?>
                                                        (+)
<?php }else{?>
                                                        (-)
<?php }?>
                                                        <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["absTotalEnuriDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?>

                                                    </dd>
                                                </dl>
<?php }?>
<?php }?>
                                                <dl>
                                                    <dt><?php echo __('총 할인')?></dt>
                                                    <dd><?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["useMileage"]+$TPL_VAR["orderInfo"]["useDeposit"]+$TPL_VAR["orderInfo"]["totalGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"]+$TPL_VAR["orderInfo"]["totalEnuriDcPrice"]+$TPL_VAR["orderInfo"]["totalMyappDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
                                                </dl>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('총 결제 금액')?></th>
                                        <td><strong class="total_pay_money"><?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["settlePrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong>
<?php if($TPL_VAR["orderInfo"]["totalMemberBankDcPrice"]> 0){?>
                                            <span>(<?php echo __('무통장결제 추가 할인')?> <em style="font-weight:bold; color:#e91818;">-<?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalMemberBankDcPrice"])?> </em>)</span>
<?php }?>
                                        </td>
                                    </tr>
<?php if(substr($TPL_VAR["orderInfo"]["settleKind"], 0, 1)=='o'){?>
                                    <tr>
                                        <th scope="row"><?php echo __('승인금액')?></th>
                                        <td><strong><?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["settlePrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong></td>
                                    </tr>
<?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["orderStatus"])=='f'&&empty($TPL_VAR["orderInfo"]["pgFailReason"])===false){?>
                                    <tr>
                                        <th scope="row"><?php echo __('결제 실패 사유')?></th>
                                        <td><strong><?php echo $TPL_VAR["orderInfo"]["pgFailReason"]?></strong></td>
                                    </tr>
<?php }?>
                                    <tr>
                                        <th scope="row"><?php echo __('결제수단')?></th>
                                        <td>
                                            <div class="pay_with_list">
                                                <strong><?php echo $TPL_VAR["orderInfo"]["settleName"]?></strong>
                                                <ul>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleKind"])=='gb'){?>
                                                    <li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 0])?></li>
                                                    <li><?php echo __('입금계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 1])?></li>
                                                    <li><?php echo __('예금주명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 2])?></li>
                                                    <li><?php echo __('입금금액')?> : <strong class="deposit_money"><?php echo gd_global_order_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]),$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong></li>
                                                    <li><?php echo __('입금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankSender"])?></li>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='c'){?>
                                                    <li><?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='b'){?>
                                                    <li><?php echo __('이체은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='v'){?>
                                                    <li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
                                                    <li><?php echo __('가상계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 1])?></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])!=''){?>
                                                    <li><?php echo __('예금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])?></li>
<?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
                                                    <li><?php echo __('송금일자')?> : <?php echo gd_date_format('Y-m-d',gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0]))?> 까지</li>
<?php }?>
                                                    <li><?php echo __('입금금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='h'){?>
                                                    <li><?php echo __('통신사')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
                                                    <li><?php echo __('결제 휴대폰 번호')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])?></li>
<?php }?>
<?php }?>
<?php }?>
                                                </ul>

<?php if(gd_isset($TPL_VAR["escrowConfirmFl"])===true){?>
                                                <!-- 에스크로 구매확인 레이어 -->
                                                <?php echo $TPL_VAR["escrowScript"]?>

                                                <div id="pgEscrowConfirmLayer" class="layer_wrap escrow_layer dn"></div>
                                                <!--//에스크로 구매확인 레이어 -->
                                                <form id="frmEscrowConfirm" name="frmEscrowConfirm" action="./order_ps.php" method="post" target="ifrmProcess">
                                                    <input type="hidden" name="mode" value="escrowConfirm"/>
                                                    <input type="hidden" name="orderNo" value="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>"/>
                                                    <input type="hidden" name="pgName" value="<?php echo $TPL_VAR["orderInfo"]["pgName"]?>"/>
                                                    <span class="btn_gray_list">
                                                        <button class="btn_gray_mid"><span><?php echo __('에스크로 구매확인')?></span></button>
                                                    </span>
                                                </form>
<?php }?>
                                            </div>
                                        </td>
                                    </tr>
<?php if(gd_is_login()==true&&$TPL_VAR["orderInfo"]["totalMileage"]> 0&&gd_use_mileage()){?>
                                    <tr>
                                        <th scope="row"><?php echo __('적립')?>  <?php echo $TPL_VAR["mileageUse"]["name"]?></th>
                                        <td>
                                            <div class="saving_mileage">
                                                <strong><?php echo __('적립 예정')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong>
<?php if(($TPL_VAR["orderInfo"]["totalGoodsMileage"]+$TPL_VAR["orderInfo"]["totalMemberMileage"])> 0){?>
                                                <dl>
                                                    <dt><?php echo __('구매')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
                                                    <dd><?php echo gd_money_format($TPL_VAR["orderInfo"]["totalGoodsMileage"]+$TPL_VAR["orderInfo"]["totalMemberMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></dd>
                                                </dl>
<?php }?>
<?php if( 0> 0){?>
                                                <dl>
                                                    <dt><?php echo __('후기작성')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
                                                    <dd><?php echo gd_money_format( 0)?><?php echo $TPL_VAR["mileageUse"]["unit"]?></dd>
                                                </dl>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]+$TPL_VAR["orderInfo"]["totalCouponOrderMileage"])> 0){?>
                                                <dl>
                                                    <dt><?php echo __('추가')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
                                                    <dd><?php echo gd_money_format($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]+$TPL_VAR["orderInfo"]["totalCouponOrderMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></dd>
                                                </dl>
<?php }?>
                                                <dl>
                                                    <dt><?php echo __('총')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
                                                    <dd><?php echo gd_money_format($TPL_VAR["orderInfo"]["totalMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></dd>
                                                </dl>
                                            </div>
                                        </td>
                                    </tr>
<?php }?>

<?php if(gd_isset($TPL_VAR["orderInfo"]["settleReceipt"])){?>
                                    <tr>
                                        <th scope="row"><?php echo __('결제영수증')?></th>
                                        <td><span class="btn_gray_list">
                                                <button type="button" class="btn_gray_mid" onclick="gd_pg_receipt_view('<?php echo $TPL_VAR["orderInfo"]["settleReceipt"]?>', '<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>');"><span><?php echo __('결제영수증 조회')?></span></button>
                                            </span>
                                        </td>
                                    </tr>
<?php }?>
<?php if((empty($TPL_VAR["orderInfo"]["cash"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='r')||($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
                                    <tr>
                                        <th scope="row"><?php echo __('현금영수증')?></th>
                                        <td>
<?php if((empty($TPL_VAR["orderInfo"]["cash"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='r')){?>
                                            <div class="pay_with_list">
                                                <ul>
                                                    <li><?php echo __('발행 용도')?> :
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["useFl"])=='d'){?><?php echo __('소득공제용')?><?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["useFl"])=='e'){?><?php echo __('지출증빙용')?><?php }?>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <?php echo __('발행 현황')?> :
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='r'){?>
                                                        <?php echo __('발급요청')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='y'){?>
                                                        <?php echo __('발행완료')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='c'){?>
                                                        <?php echo __('발행취소')?>

<?php }else{?>
                                                        <?php echo __('발행오류')?>

<?php }?>
                                                    </li>
                                                </ul>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='y'||gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='c'){?>
                                                <ul>
                                                    <li>
                                                        <span class="btn_gray_list">
                                                            <button type="button" class="btn_gray_mid" onclick="gd_pg_receipt_view('cash', '<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>');"><span><?php echo __('현금영수증 출력')?></span></button>
                                                        </span>
                                                    </li>
                                                </ul>
<?php }?>
                                            </div>
<?php }?>

<?php if(($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
<?php if($TPL_VAR["receipt"]['periodFl']=='y'){?>
                                            <div class="btn_layer">
                                                <span class="btn_gray_list"><a href="#lyCashReceipt" class="btn_gray_small" onclick="gd_cash_receipt_toggle();"><em><?php echo __('현금영수증 발행')?></em></a></span>

                                                <!-- N : 현금영수증 발행 레이어 시작 -->
                                                <div id="lyCashReceipt" class="layer_area" style="display:none;">
                                                    <div class="ly_wrap cash_receipt_layer">
                                                        <form id="frmCashReceipt" name="frmCashReceipt" action="./order_ps.php" method="post" target="ifrmProcess">
                                                            <input type="hidden" name="mode" value="cashReceiptRequest"/>
                                                            <input type="hidden" name="orderNo" value="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>"/>
                                                            <div class="ly_tit">
                                                                <strong><?php echo __('현금영수증 발행')?></strong>
                                                            </div>
                                                            <div class="ly_cont">
                                                                <div class="cash_receipt_cont">
                                                                    <dl>
                                                                        <dt><?php echo __('발행용도')?></dt>
                                                                        <dd>
                                                                            <div class="form_element">
                                                                                <ul class="cash_receipt_select">
                                                                                    <li>
                                                                                        <input type="radio" id="cashCertD" name="cashUseFl" value="d" onclick="gd_cash_receipt_toggle();" checked="checked"/>
                                                                                        <label class="choice_s on" for="cashCertD"><?php echo __('소득공제용')?></label>
                                                                                    </li>
                                                                                    <li>
                                                                                        <input type="radio" id="cashCertE" name="cashUseFl" value="e" onclick="gd_cash_receipt_toggle();"/>
                                                                                        <label class="choice_s" for="cashCertE"><?php echo __('지출증빙용')?></label>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </dd>
                                                                    </dl>
                                                                    <dl>
                                                                        <dt class="js_cert_no_title"><?php echo __('휴대폰번호')?></dt>
                                                                        <dd id="certNoHp">
                                                                            <input type="text" class="number_only" name="cashCertNo[c]" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['cash']['cellPhone'])?>" placeholder="<?php echo __('- 없이 입력하세요')?>" maxlength="11"/>
                                                                        </dd>
                                                                        <dd id="certNoBno">
                                                                            <input type="text" class="number_only" name="cashCertNo[b]" value="<?php echo str_replace('-','',gd_isset($TPL_VAR["memberInvoiceInfo"]['cash']['cashBusiNo']))?>" placeholder="<?php echo __('- 없이 입력하세요')?>" maxlength="10" />
                                                                        </dd>
                                                                    </dl>
                                                                </div>
                                                                <div class="btn_center_box">
                                                                    <a href="#" class="bnt_cash_receipt"><em><?php echo __('현금영수증 발행')?></em></a>
                                                                </div>
                                                            </div>
                                                            <!-- //ly_cont -->
                                                            <a href="#lyCashReceipt" class="ly_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                                        </form>
                                                    </div>
                                                    <!-- //ly_wrap -->
                                                </div>
                                                <!-- //layer_area -->
                                                <!-- //N : 현금영수증 발행 레이어 끝 -->
                                            </div>

<?php }else{?>
                                            발급불가 (※ 발급요청은 결제완료 후 3일 이내에만 가능합니다.)
<?php }?>
<?php }?>

                                        </td>
                                    </tr>
<?php }?>

<?php if((empty($TPL_VAR["orderInfo"]["tax"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='t')||($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
                                    <tr>
                                        <th scope="row"><?php echo __('세금계산서')?></th>
                                        <td>

<?php if((empty($TPL_VAR["orderInfo"]["tax"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='t')){?>
                                            <div class="pay_with_list">
                                                <ul>
                                                    <li><?php echo __('발행 현황')?> :
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='r'){?>
                                                        <?php echo __('발급요청')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'){?>
                                                        <?php echo __('발행완료')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='c'){?>
                                                        <?php echo __('발행취소')?>

<?php }else{?>
                                                        <?php echo __('발행오류')?>

<?php }?>
                                                    </li>
                                                </ul>
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'){?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["issueFl"])=='g'){?>
                                                <ul>
                                                    <li>
                                                        <span class="btn_gray_list">
                                                            <button type="button" class="btn_gray_mid js_btn_tax_invoice"><span><?php echo __('세금계산서 출력')?></span></button>
                                                        </span>
                                                    </li>
                                                </ul>
<?php }else{?>
                                                <ul>
                                                    <li>
                                                        <div class="admin_text">
                                                            * <?php echo __('전자세금계산서는 주문자정보의 이메일주소로 발송됩니다. %s 발행완료된 후 전자세금계산서 메일이 확인되지 않으면 고객센터로 문의주시기 바랍니다.','<br/>&nbsp;&nbsp;')?>

                                                        </div>
                                                    </li>
                                                </ul>
<?php }?>
<?php }?>
                                            </div>
<?php }?>

<?php if(($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
<?php if(($TPL_VAR["receipt"]["limitDateFl"]=='y')){?>
                                            <div class="btn_layer">
                                                <span class="btn_gray_list"><a href="#lyTaxInvoice" class="btn_gray_small"><em><?php echo __('세금계산서 발행')?></em></a></span>
<?php if($TPL_VAR["taxinvoiceInfo"]){?>
                                                <div class="admin_text"><?php echo $TPL_VAR["taxinvoiceInfo"]?></div>
<?php }?>

                                                <!-- N : 세금계산서 발행 레이어 시작 -->
                                                <div id="lyTaxInvoice" class="layer_area" style="display:none;">
                                                    <div class="ly_wrap tax_invoice_layer">
                                                        <div class="ly_tit">
                                                            <strong><?php echo __('세금계산서 발행')?></strong>
<?php if($TPL_VAR["memberInvoiceInfo"]['tax']['memberTaxInfoFl']=='y'){?>
                                                            <button class="normal-btn small1 tax_info_init"><em><?php echo __('초기화')?></em></button>
<?php }?>
                                                        </div>
                                                        <div class="ly_cont">
                                                            <form id="frmTaxInvoiceRequest" name="frmTaxInvoiceRequest" action="./order_ps.php" method="post" target="ifrmProcess">
                                                                <input type="hidden" name="mode" value="taxInvoiceRequest"/>
                                                                <input type="hidden" name="orderNo" value="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>"/>
                                                                <div class="tax_invoice_cont">
                                                                    <div class="left_table_type">
                                                                        <table id="tax_info" summary="<?php echo __('세금계산서 입력폼입니다.')?>">
                                                                            <colgroup>
                                                                                <col style="width:20%;">
                                                                                <col style="width:30%;">
                                                                                <col style="width:20%;">
                                                                                <col style="width:30%;">
                                                                            </colgroup>
                                                                            <tbody>
                                                                            <tr>
                                                                                <th scope="row"><?php echo __('사업자번호')?></th>
                                                                                <td colspan="3"><input type="text" name="taxBusiNo" placeholder="<?php echo __('- 없이 입력하세요')?>" value="<?php echo str_replace('-','',gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['taxBusiNo']))?>" maxlength="10"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?php echo __('회사명')?></th>
                                                                                <td><input type="text" name="taxCompany" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['company'])?>" maxlength="50" data-pattern="gdMemberNmGlobal"/></td>
                                                                                <th scope="row"><?php echo __('대표자명')?></th>
                                                                                <td><input type="text" name="taxCeoNm" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['ceo'])?>"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?php echo __('업태')?></th>
                                                                                <td><input type="text" name="taxService" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['service'])?>"/></td>
                                                                                <th scope="row"><?php echo __('종목')?></th>
                                                                                <td><input type="text" name="taxItem" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['item'])?>"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?php echo __('사업장주소')?></th>
                                                                                <td colspan="3" class="member_address">
                                                                                    <div class="address_postcode">
                                                                                        <input type="text" name="taxZonecode" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comZonecode'])?>" readonly="readonly">
                                                                                        <input type="hidden" name="taxZipcode" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comZipcode'])?>">
                                                                                        <span id=taxrZipcodeText" class="old_post_code"></span>
                                                                                        <button type="button" class="btn_post_search" onclick="gd_postcode_search('taxZonecode', 'taxAddress', 'taxZipcode');"><?php echo __('우편번호검색')?></button>
                                                                                    </div>
                                                                                    <div class="address_input">
                                                                                        <input type="text" name="taxAddress" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comAddress'])?>"/>
                                                                                        <input type="text" name="taxAddressSub" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comAddressSub'])?>"/>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
<?php if($TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
                                                                            <tr>
                                                                                <th scope="row"><?php echo __('발행 이메일')?></th>
                                                                                <td colspan="3" class="cash_receipt_email">
                                                                                    <input type="text" id="idTaxEmail" name="taxEmail" placeholder="<?php echo __('미입력 시 주문자의 이메일로 발행')?>" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['email'])?>"/>
                                                                                    <select id="taxEmailDomain" class="chosen_select_none">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
                                                                                        <option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
<?php }?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div class="btn_center_box">
                                                                    <button type="submit" class="bnt_tax_invoice"><em>세금계산서 발행</em></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- //ly_cont -->
                                                        <a href="#lyTaxInvoice" class="ly_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                                    </div>
                                                    <!-- //ly_wrap -->
                                                </div>
                                                <!-- //layer_area -->
                                                <!-- //N : 세금계산서 발행 레이어 끝 -->
                                            </div>
<?php }else{?>

                                            <?php echo __('발급불가 %s 결제완료 기준 다음 달  %s 일까지 신청 가능 가능합니다.%s','(※ ',$TPL_VAR["receipt"]['taxInvoiceLimitDate'],')')?><br />
<?php if($TPL_VAR["taxinvoiceDeadline"]){?>
                                            <div class="admin_text">
                                                <?php echo $TPL_VAR["taxinvoiceDeadline"]?>

                                            </div>
<?php }?>

<?php }?>
<?php }?>
                                        </td>
                                    </tr>
<?php }?>

<?php if($TPL_VAR["receipt"]['reception']=='y'||$TPL_VAR["receipt"]['particularFl']=='y'){?>
                                    <form id="printform" name="printform" action="./order_print.php" method="post" target="orderPrintPopup">
                                        <input type="hidden" name="orderPrintMode" />
                                        <input type="hidden" name="orderPrintCode" value="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>"/>
                                    </form>
<?php }?>
<?php if($TPL_VAR["receipt"]['particularFl']=='y'){?>
                                    <tr>
                                        <th scope="row"><?php echo __('거래명세서')?></th>
                                        <td>
                                            <span class="btn_gray_list">
                                                <button type="button" class="btn_gray_mid button small black inline" onclick="gd_order_print_popup('particular')"><span><?php echo __('거래명세서 인쇄')?></span></button>
                                            </span>
                                        </td>
                                    </tr>
<?php }?>
<?php if($TPL_VAR["receipt"]['reception']=='y'){?>
                                    <tr>
                                        <th scope="row"><?php echo __('간이영수증')?></th>
                                        <td>
                                            <span class="btn_gray_list">
                                                <button type="button" class="btn_gray_mid button small black inline" onclick="gd_order_print_popup('reception')"><span><?php echo __('간이영수증 인쇄')?></span></button>
                                            </span>
                                        </td>
                                    </tr>
<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- //payment_info -->

<?php if($TPL_VAR["isHandle"]&&$TPL_VAR["totalRefundPrice"]> 0){?>
                        <div class="orderer_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('환불 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><?php echo __('환불 금액')?></th>
                                        <td><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundPrice"])?></strong><?php echo gd_global_currency_string()?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('환불 내역')?></th>
                                        <td>
                                            <div class="pay_with_list">
                                                <ul>
<?php if($TPL_VAR["totalCompleteCashPrice"]> 0){?>
                                                    <li><?php echo __('현금')?> :
                                                        <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteCashPrice"])?></strong><?php echo gd_global_currency_string()?>

                                                    </li>
<?php }?>
<?php if($TPL_VAR["totalCompletePgPrice"]> 0){?>
                                                    <li>PG :
                                                        <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompletePgPrice"])?></strong><?php echo gd_global_currency_string()?>

                                                    </li>
<?php }?>
<?php if($TPL_VAR["totalCompleteDepositPrice"]> 0){?>
                                                    <li><?php echo __('예치금')?> :
                                                        <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteDepositPrice"])?></strong><?php echo gd_global_currency_string()?> (<?php echo __('사용 예치금 환불')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundUseDeposit"])?></strong><?php echo gd_global_currency_string()?>)
                                                    </li>
<?php }?>
<?php if($TPL_VAR["totalRefundUseMileage"]> 0){?>
                                                    <li><?php echo __('마일리지')?> :
                                                        <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundUseMileage"])?></strong><?php echo gd_global_currency_string()?>

                                                    </li>
<?php }?>
<?php if($TPL_VAR["totalCompleteMileagePrice"]> 0){?>
                                                    <li><?php echo __('기타')?> :
                                                        <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteMileagePrice"])?></strong><?php echo gd_global_currency_string()?>

                                                    </li>
<?php }?>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
<?php if($TPL_VAR["totalCompleteCashPrice"]> 0){?>
                                    <tr>
                                        <th scope="row"><?php echo __('환불 계좌정보')?></th>
                                        <td>
<?php if($TPL_VAR["totalRefundData"]){?>
<?php if($TPL_totalRefundData_1){foreach($TPL_VAR["totalRefundData"] as $TPL_V1){?>
<?php if($TPL_V1["account"]){?>
                                            <?php echo __('입금은행')?> : <?php echo nl2br($TPL_V1["bank"])?> | <?php echo __('입금계좌')?> : <?php echo nl2br($TPL_V1["account"])?> | <?php echo __('입금자명')?> : <?php echo nl2br($TPL_V1["depositor"])?> <br />
<?php }?>
<?php }}?>
<?php }?>
                                        </td>
                                    </tr>
<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php }?>

<?php if($TPL_VAR["isExchangeHandle"]){?>
<?php if($TPL_exchangeHandleData_1){foreach($TPL_VAR["exchangeHandleData"] as $TPL_V1){?>
<?php if($TPL_V1["ehDifferencePrice"]< 0){?>
                        <div class="orderer_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('교환 추가결제 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><?php echo __('추가결제 금액')?></th>
                                        <td><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_V1["ehAbsDifferencePrice"])?></strong><?php echo gd_global_currency_string()?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('입금자명')?></th>
                                        <td><?php echo $TPL_V1["ehSettleName"]?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('결제계좌정보')?></th>
                                        <td><?php echo $TPL_V1["ehSettleBankAccountInfo"]?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php }else{?>
                        <div class="orderer_info">
                            <div class="mypage_zone_tit">
                                <h4><?php echo __('교환환불 정보')?></h4>
                            </div>

                            <div class="mypage_table_type">
                                <table class="table_left table no_header">
                                    <colgroup>
                                        <col style="width:15%;">
                                        <col style="width:85%;">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><?php echo __('환불금액')?></th>
                                        <td><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_V1["ehAbsDifferencePrice"])?></strong><?php echo gd_global_currency_string()?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?php echo __('환불수단')?></th>
                                        <td><?php echo $TPL_V1["ehRefundNameStr"]?></td>
                                    </tr>
<?php if($TPL_V1["ehRefundMethod"]=='bank'){?>
                                    <tr>
                                        <th scope="row"><?php echo __('환불계좌정보')?></th>
                                        <td><?php echo $TPL_V1["ehRefundBankName"]?> / <?php echo $TPL_V1["ehRefundBankAccountNumber"]?> / <?php echo $TPL_V1["ehRefundName"]?></td>
                                    </tr>
<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php }?>
<?php }}?>
<?php }?>
                    </div>
                    <!-- //order_view_info -->

                </div>
                <!-- //mypage_main -->

            </div>
            <!-- //content -->
        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section>
<?php $this->print_("footer",$TPL_SCP,1);?>


<script type="text/javascript">
    $(document).ready(function () {

        $('input.number_only').on('keyup', function () {
            var value = $(this).val().replace(/[^0-9]/g, '');
            $(this).val(value);
        });

        // 현금영수증 신청
        $('.bnt_cash_receipt').click(function (e) {

            var certCode = $('input[name=\'cashUseFl\']:checked').val();
            var pass = true;
            if (certCode == 'd') {
                if ($('input[name=\'cashCertNo[c]\']').val() == '') {
                    pass = false;
                    alert("<?php echo __('휴대폰 번호를 입력해주세요.')?>");
                }
            } else if (certCode == 'e') {
                if ($('input[name=\'cashCertNo[b]\']').val() == '') {
                    pass = false;
                    alert("<?php echo __('사업자 번호를 입력해주세요.')?>");
                }
            } else {
                pass = false;
                alert("<?php echo __('발행용도를 선택해주세요.')?>");
            }

            if (pass == true) {
                $('#frmCashReceipt').target = 'ifrmProcess'
                $('#frmCashReceipt').submit();
            }
        });

<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'&&gd_isset($TPL_VAR["orderInfo"]["tax"]["issueFl"])=='g'){?>
        // 세금계산서 보기
        $('.js_btn_tax_invoice').click(function (e) {
            var win = gd_popup({
                url: '../share/show_tax_invoice.php?orderNo=<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>'
                , target: 'tax_invoice'
                , width: 750
                , height: 600
                , resizable: 'yes'
                , scrollbars: 'yes'
            });
            win.focus();
            return win;

        });
<?php }?>

<?php if(($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
<?php if($TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
        $.validator.addMethod(
            'taxEmailChk', function (value, element) {
                if ($('input[name="taxEmail"]').val() !==  '' && !$.validator.methods.email.call(this, $.trim(value), element)) {
                    $.validator.messages.taxEmailChk =  __('[세금계산서] 발행 이메일을 정확하게 입력하여 주세요.');
                    return false;
                }
                return true;
            },'');
<?php }?>

        // 세금계산서 신청
        $('#frmTaxInvoiceRequest').validate({
            onkeyup: onkeyup,
            submitHandler: function (form) {
<?php if($TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
                if($('#idTaxEmail').val().length==0){
                    $('#idTaxEmail').val('<?php echo $TPL_VAR["orderInfo"]["orderEmail"]?>');
                }
<?php }?>
                form.target = 'ifrmProcess';
                form.submit();
            },
            rules: {
                taxBusiNo: {
                    required: true,
                    businessnoKR: true
                },
                taxCompany: {
                    required: true
                },
                taxCeoNm: {
                    required: true
                },
                taxService: {
                    required: true
                },
                taxItem: {
                    required: true
                },
<?php if($TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
                taxEmail: {
                    taxEmailChk: true
                },
<?php }?>
                taxAddress: {
                    required: true
                }
            },
            messages: {
                taxBusiNo: {
                    required: "<?php echo __('[세금계산서] 사업자번호를 입력하세요.')?>"
                },
                taxCompany: {
                    required: "<?php echo __('[세금계산서] 회사명을 입력하세요.')?>"
                },
                taxCeoNm: {
                    required: "<?php echo __('[세금계산서] 대표자명을 입력하세요.')?>"
                },
                taxService: {
                    required: "<?php echo __('[세금계산서] 업태를 입력하세요.')?>"
                },
                taxItem: {
                    required: "<?php echo __('[세금계산서] 종목을 입력하세요.')?>"
                },
                taxAddress: {
                    required: "<?php echo __('[세금계산서] 사업장주소를 입력하세요.')?>"
                }
            }
        });

        //이메일세팅
        gd_select_email_domain('taxEmail','taxEmailDomain');

<?php if($TPL_VAR["memberInvoiceInfo"]['tax']['memberTaxInfoFl']=='y'){?>
        // 세금계산서 입력 정보 초기화
        $('.tax_info_init').on('click', function (e) {
            e.preventDefault();
            $('#tax_info').find('input').val('');
        });
<?php }?>
<?php }?>
    });


<?php if(($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n'&&$TPL_VAR["receipt"]['periodFl']=='y')){?>
    /**
     * 현금영수증 인증방법 선택
     * (소득공제용 - 휴대폰 번호(c), 지출증빙용 - 사업자번호(b))
     */
    function gd_cash_receipt_toggle() {
        var certCode = $('input[name=\'cashUseFl\']:checked').val();

        if (certCode == 'd') {
            $('input[name=\'cashCertFl\']').val('c');
            $('.js_cert_no_title').text("<?php echo __('휴대폰번호')?>");
            $('#certNoHp').show();
            $('#certNoBno').hide();
        } else {
            $('input[name=\'cashCertFl\']').val('b');
            $('.js_cert_no_title').text("<?php echo __('사업자번호')?>");
            $('#certNoHp').hide();
            $('#certNoBno').show();
        }
    }
<?php }?>


<?php if($TPL_VAR["receipt"]['reception']=='y'||$TPL_VAR["receipt"]['particularFl']=='y'){?>
    // 주문 프린트 팝업창
    function gd_order_print_popup(orderPrintMode){
        var orderPrint = window.open('', 'orderPrintPopup', 'width=750,height=600,menubar=yes,scrollbars=yes,resizable=yes');
        $('input[name=orderPrintMode]').val(orderPrintMode);
        $('#printform').submit();
    }
<?php }?>

    /**
     * validator onkeyup 함수
     * @param element
     * @param event
     * @returns <?php echo $TPL_VAR["boolean"]?>

     */
    function onkeyup(element, event) {
        if (check_key_code2(event)) {
            return true;
        }
        if ($.isFunction(replace_keyup[$(element).data('pattern')])) {
            replace_keyup[$(element).data('pattern')](element);
        }
    }

    var regexp_test = function (string, pattern) {
        if (string === undefined || string.length < 1) {
            return false;
        }
        return pattern.test(string);
    };

    var replace_pattern = function (string, pattern, c) {
        if (string === undefined || string.length < 1) {
            return '';
        }
        return string.replace(pattern, c);
    };

    var replace_keyup = {
        gdEngNum: function (element) {
            element.value = replace_pattern(element.value, /[^\da-zA-Z]/g, '');
        },
        gdEngKor: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣\u119E\u11A2]/g, '');
            }
        },
        gdEngKorNum: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣0-9\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣0-9\u119E\u11A2]/g, '');
            }

        },
        gdNum: function (element) {
            element.value = replace_pattern(element.value, /[^\d]/g, '');
        },
        gdEng: function (element) {
            element.value = replace_pattern(element.value, /[^a-zA-Z]/g, '');
        },
        gdKor: function (element) {
            if (regexp_test(element.value, /[^가-힣ㄱ-ㅎㅏ-ㅣ\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^가-힣ㄱ-ㅎㅏ-ㅣ\u119E\u11A2]/g, '');
            }
        },
        gdMemberId: function (element) {
            element.value = replace_pattern(element.value, /[^\da-zA-Z\.\-_@]/g, '');
        },
        gdMemberNmGlobal: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[\/\'\"\\\|]/)) {
                element.value = replace_pattern(element.value, /[\/\'\"\\\|]/g, '');
            }
        }
    };

    /**
     * jquery validation의 키 체크 함수
     * @param event
     * @returns <?php echo $TPL_VAR["boolean"]?>

     */
    function check_key_code2(event) {
        // Avoid revalidate the field when pressing one of the following keys
        /* Shift       => 16 Ctrl        => 17 Alt         => 18
         Caps lock   => 20 End         => 35 Home        => 36
         Left arrow  => 37 Up arrow    => 38 Right arrow => 39
         Down arrow  => 40 Insert      => 45 Num lock    => 144 AltGr key   => 225*/
        var excludedKeys = [
            16, 17, 18, 20, 35, 36, 37,
            38, 39, 40, 45, 144, 225
        ];

        return event.which === 9 || $.inArray(event.keyCode, excludedKeys) !== -1;
    }
</script>
<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/validation/additional/businessnoKR.js"></script>