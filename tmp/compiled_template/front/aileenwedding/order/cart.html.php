<?php /* Template_ 2.2.7 2020/04/17 03:39:51 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/order/cart.html 000058114 */ 
if (is_array($TPL_VAR["cartInfo"])) $TPL_cartInfo_1=count($TPL_VAR["cartInfo"]); else if (is_object($TPL_VAR["cartInfo"]) && in_array("Countable", class_implements($TPL_VAR["cartInfo"]))) $TPL_cartInfo_1=$TPL_VAR["cartInfo"]->count();else $TPL_cartInfo_1=0;
if (is_array($TPL_VAR["useSettleKindPg"])) $TPL_useSettleKindPg_1=count($TPL_VAR["useSettleKindPg"]); else if (is_object($TPL_VAR["useSettleKindPg"]) && in_array("Countable", class_implements($TPL_VAR["useSettleKindPg"]))) $TPL_useSettleKindPg_1=$TPL_VAR["useSettleKindPg"]->count();else $TPL_useSettleKindPg_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<section class="section_container" <?php if($TPL_VAR["req"]["noheader"]=='y'){?>style="padding:0;"<?php }?>>
    <div class="center_layer">
        <div class="top_layer">
            <ul class="tab_area inline">
                <li class="on">
                    <a href="#" onclick="return false"><?php echo __('장바구니')?></a>
                    <span class="underline"></span>
                </li>
            </ul><!-- .tab_area -->
        </div><!-- .top_layer -->                        
        <div class="section_layer content_box">
            <div class="order_wrap">
                <div class="order_tit">
                    <ol>
                        <li class="page_on">
                            <span>01</span> <?php echo __('장바구니')?>

                        </li><li class="page_on separate"><i class="xi-angle-right"></i></li><li>
                            <span>02</span> <?php echo __('주문서작성/결제')?>

                        </li><li class="separate"><i class="xi-angle-right"></i></li><li>
                            <span>03</span> <?php echo __('주문완료')?>

                        </li>
                    </ol>
                </div><!-- //order_tit -->
                <div class="cart_cont">
                    <form id="frmCart" name="frmCart" method="post" target="ifrmProcess">
                        <input type="hidden" name="mode" value=""/>
                        <input type="hidden" name="cart[cartSno]" value=""/>
                        <input type="hidden" name="cart[goodsNo]" value=""/>
                        <input type="hidden" name="cart[goodsCnt]" value=""/>
                        <input type="hidden" name="cart[addGoodsNo]" value=""/>
                        <input type="hidden" name="cart[addGoodsCnt]" value=""/>
<?php if($TPL_VAR["couponUse"]=='y'){?>
                        <input type="hidden" name="cart[couponApplyNo]" value=""/>
<?php }?>
                        <input type="hidden" name="useBundleGoods" value="1" />
                        <!-- 장바구니 상품리스트 시작 -->

<?php if($TPL_cartInfo_1){foreach($TPL_VAR["cartInfo"] as $TPL_K1=>$TPL_V1){?>
                        <div class="cart_cont_list">
                            <div class="order_table_type">
                                <table class="table c_table">
                                    <colgroup>
                                        <col style="width:50px;">  <!-- 체크박스 -->
                                        <col>					<!-- 상품명/옵션 -->
                                        <col style="width:130px;">  <!-- 수량 -->
                                        <col style="width:100px;"> <!-- 상품금액 -->
                                        <col style="width:100px;"> <!-- 할인/적립 -->
                                        <col style="width:100px;"> <!-- 합계금액 -->
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <col style="width:100px;"> <!-- 배송비 -->
<?php }?>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="form_element">
                                                <input type="checkbox" id="allCheck<?php echo $TPL_K1?>" class="gd_select_all_goods" data-target-id="cartSno<?php echo $TPL_K1?>_" data-target-form="#frmCart" checked="checked">
                                                <label for="allCheck<?php echo $TPL_K1?>" class="check_s on"></label>
                                            </div>
                                        </th>
                                        <th><?php echo __('상품')?>/<?php echo __('옵션 정보')?></th>
                                        <th><?php echo __('수량')?></th>
                                        <th><?php echo __('상품금액')?></th>
                                        <th><?php echo __('할인')?><?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>/<?php echo __('적립')?><?php }?></th>
                                        <th><?php echo __('합계금액')?></th>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <th><?php echo __('배송비')?></th>
<?php }?>
                                    </tr>
                                    </thead>
                                    <tbody>

<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
                                    <tr>
                                        <td class="td_chk">
                                            <div class="form_element">
                                                <input type="checkbox" id="cartSno<?php echo $TPL_K1?>_<?php echo $TPL_V3["sno"]?>" name="cartSno[]" value="<?php echo $TPL_V3["sno"]?>" checked="checked" data-price="<?php echo $TPL_V3["price"]['goodsPriceSubtotal']?>" data-mileage="<?php echo ($TPL_V3["mileage"]["goodsMileage"]+$TPL_V3["mileage"]["memberMileage"])?>" data-goodsdc="<?php echo $TPL_V3["price"]["goodsDcPrice"]?>" data-memberdc="<?php echo ($TPL_V3["price"]["memberDcPrice"]+$TPL_V3["price"]["memberOverlapDcPrice"])?>" data-coupondc="<?php echo $TPL_V3["price"]["couponGoodsDcPrice"]?>" data-possible="<?php echo $TPL_V3["orderPossible"]?>" data-goods-key="<?php echo $TPL_V3["goodsKey"]?>" data-goods-no="<?php echo $TPL_V3["goodsNo"]?>" data-goods-nm="<?php echo gd_remove_only_tag($TPL_V3["goodsNm"])?>" data-option-nm="<?php echo $TPL_V3["optionNm"]?>" data-fixed-sales="<?php echo $TPL_V3["fixedSales"]?>" data-sales-unit="<?php echo $TPL_V3["salesUnit"]?>" data-fixed-order-cnt="<?php echo $TPL_V3["fixedOrderCnt"]?>" data-min-order-cnt="<?php echo $TPL_V3["minOrderCnt"]?>" data-max-order-cnt="<?php echo $TPL_V3["maxOrderCnt"]?>" data-default-goods-cnt="<?php echo $TPL_V3["goodsCnt"]?>">
                                                <label for="cartSno<?php echo $TPL_K1?>_<?php echo $TPL_V3["sno"]?>" class="check_s on"></label>
                                            </div>
                                        </td>
                                        <td class="td_left title">
                                            <div class="pick_add_cont">
                                                <span class="pick_add_img">
                                                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsImage"]?></a>
                                                </span>
                                                <div class="pick_add_info">
<?php if($TPL_V3["orderPossibleMessageList"]){?>
                                                    <strong class="caution_msg1"><?php echo __('구매 이용 조건 안내')?>

                                                        <a class="normal_btn js_impossible_layer">
                                                            <em ><?php echo __('전체보기')?> <i class="xi-angle-right"></i></em>
                                                        </a>
                                                        <div class="nomal_layer dn">
                                                            <div class="wrap">
                                                                <strong><?php echo __('결제 제한 조건 사유')?></strong>
                                                                <div class="list">
                                                                    <table cellspacing="0" border="0">
<?php if((is_array($TPL_R4=$TPL_V3["orderPossibleMessageList"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
                                                                        <tr>
                                                                            <td class="strong"><?php echo $TPL_V4?></td>
                                                                        </tr>
<?php }}?>
                                                                    </table>
                                                                </div>
                                                                <button type="button" class="close js_impossible_layer" title="<?php echo __('닫기')?>"><?php echo __('닫기')?></button>
                                                            </div>
                                                        </div>
                                                    </strong>

<?php }?>
<?php if($TPL_V3["duplicationGoods"]==='y'){?>
                                                    <strong class="chk_none"><?php echo __('중복 상품')?></strong>
<?php }?>

<?php if(($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member')&&$TPL_V3["couponBenefitExcept"]=='n'){?>
                                                    <div id="coupon_apply_<?php echo $TPL_V3["sno"]?>" class="pick_btn_box">
<?php if(gd_is_login()===false){?>
                                                        <a href="#" class="btn_alert_login btn"><?php echo __('쿠폰적용')?></a>
<?php }else{?>
<?php if($TPL_V3["memberCouponNo"]){?>
                                                        <a href="#" class="js_btn_coupon_cancel btn" data-cartsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('쿠폰취소')?></a>
                                                        <a href="#couponApplyLayer" class="btn_open_layer btn" data-cartsno="<?php echo $TPL_V3["sno"]?>" ><?php echo __('쿠폰변경')?></a>
<?php }else{?>
                                                        <a href="#couponApplyLayer" class="btn_open_layer btn" data-cartsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('쿠폰적용')?></a>
<?php }?>
<?php }?>
                                                    </div>
<?php }?>

                                                    <em><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsNm"]?></a></em>

<?php if($TPL_V3["payLimitFl"]=='y'&&is_array($TPL_V3["payLimit"])){?>
                                                    <div class="icon_pick_list">
<?php if((is_array($TPL_R4=$TPL_V3["payLimit"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
<?php if($TPL_V4=='pg'){?>
                                                        <div class="icon_pg_over">
                                                            <img src="/data/skin/front/aileenwedding/img/icon/order/icon_settle_kind_<?php echo $TPL_V4?>.png">
                                                            <div class="icon_pg_cont"></div>
                                                        </div>
<?php }else{?>
                                                        <img src="/data/skin/front/aileenwedding/img/icon/order/icon_settle_kind_<?php echo $TPL_V4?>.png">
<?php }?>
<?php }}?>
                                                    </div>
<?php }?>
                                                    <!-- //icon_pick_list -->

                                                    <div class="pick_option_box">
<?php if((is_array($TPL_R4=$TPL_V3["option"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {$TPL_S4=count($TPL_R4);$TPL_I4=-1;foreach($TPL_R4 as $TPL_V4){$TPL_I4++;?>
<?php if($TPL_V4["optionName"]){?>
                                                        <span class="text_type_cont"><?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?> <?php if((($TPL_I4+ 1)==$TPL_S4)&&$TPL_V4["optionPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?><strong>(<?php if($TPL_V4["optionPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionPrice"])?>)</strong><?php }?><?php if((($TPL_I4+ 1)==$TPL_S4)){?><?php if(empty($TPL_V4["optionSellStr"])===false){?><strong>[<?php echo $TPL_V4["optionSellStr"]?>]</strong><?php }?><?php if(empty($TPL_V4["optionDeliveryStr"])===false){?><strong>[<?php echo $TPL_V4["optionDeliveryStr"]?>]</strong><?php }?><?php }?></span>
<?php }?>
<?php }}?>
                                                    </div>

                                                    <div class="pick_option_box">
<?php if((is_array($TPL_R4=$TPL_V3["optionText"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
<?php if($TPL_V4["optionValue"]){?>
                                                        <span class="text_type_cont"><?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?> <?php if($TPL_V4["optionTextPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?><strong>(<?php if($TPL_V4["optionTextPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionTextPrice"])?>)</strong><?php }?></span>
<?php }?>
<?php }}?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- //pick_add_cont -->

<?php if(empty($TPL_V3["addGoods"])===false){?>
                                            <div class="pick_add_list">
<?php if((is_array($TPL_R4=$TPL_V3["addGoods"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
                                                <div class="pick_add_cont">
                                                    <span class="pick_add_plus"><em><?php echo __('추가')?></em></span>
                                                    <span class="pick_add_img"><?php echo $TPL_V4["addGoodsImage"]?></span>
                                                    <div class="pick_add_info">
                                                        <em>
                                                            <b><?php echo $TPL_V4["addGoodsNm"]?></b> <br> <?php echo __('수량')?> : <?php echo $TPL_V4["addGoodsCnt"]?><?php echo __('개')?>

<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                                            (+<?php echo gd_global_currency_display( 0)?>)
<?php }else{?>
                                                            (+<?php echo gd_global_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?>)
                                                            <p class="add_currency"><?php echo gd_global_add_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?></p>
<?php }?>
                                                        </em>
                                                        <div class="pick_option_box">
<?php if($TPL_V4["optionNm"]){?>
                                                            <span class="text_type_cont"><?php echo __('옵션')?> : <?php echo $TPL_V4["optionNm"]?></span>
<?php }?>
                                                        </div>
                                                    </div>
                                                </div>
<?php }}?>
                                                <!-- //pick_add_cont -->
                                            </div>
<?php }?>
                                            <!-- //pick_add_list -->

                                        </td>
                                        <td class="td_order_amount">
                                            <div class="order_goods_num">
                                                <strong><?php echo $TPL_V3["goodsCnt"]?><?php echo __('개')?></strong>
                                                <div class="btn_gray_list">
                                                    <a href="#optionViewLayer" class="btn_gray_small btn_open_layer" <?php if($TPL_V3["memberCouponNo"]){?> data-coupon='use' <?php }else{?> data-goodsno="<?php echo $TPL_V3["goodsNo"]?>" data-sno="<?php echo $TPL_V3["sno"]?>"  <?php }?>><span><?php echo __('옵션/수량변경')?></span></a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                            <strong class="order_sum_txt"><?php echo $TPL_V3["goodsPriceString"]?></strong>
<?php }else{?>
                                            <strong class="order_sum_txt <?php if($TPL_V3["timeSaleFl"]){?>time_sale_cost<?php }else{?>price<?php }?>"><?php echo gd_global_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?></strong>
                                            <p class="add_currency"><?php echo gd_global_add_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?></p>
<?php }?>
                                        </td>
                                        <td class="td_benefit">
<?php if($TPL_V3["goodsPriceDisplayFl"]=='n'){?>
                                            <div>-</div>
<?php }else{?>
                                                <ul class="benefit_list">
<?php if(gd_is_login()===true){?>
<?php if(($TPL_V3["price"]['goodsDcPrice']+$TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice']+$TPL_V3["price"]['couponGoodsDcPrice'])> 0){?>
                                                    <li class="benefit_sale">
                                                        <em><?php echo __('할인')?></em>
<?php if($TPL_V3["price"]['goodsDcPrice']> 0){?>
                                                        <span><?php echo __('상품')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDcPrice'])?></strong></span>
<?php }?>
<?php if(($TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice'])> 0){?>
                                                        <span><?php echo __('회원')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice'])?></strong></span>
<?php }?>
<?php if($TPL_V3["price"]['couponGoodsDcPrice']> 0){?>
                                                        <span><?php echo __('쿠폰')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['couponGoodsDcPrice'])?></strong></span>
<?php }?>
                                                    </li>
<?php }?>
<?php if($TPL_VAR["mileage"]["useFl"]==='y'&&($TPL_V3["mileage"]["goodsMileage"]+$TPL_V3["mileage"]["memberMileage"]+$TPL_V3["mileage"]["couponGoodsMileage"])> 0){?>
                                                    <li class="benefit_mileage">
                                                        <em><?php echo __('적립')?></em>
<?php if($TPL_V3["mileage"]["goodsMileage"]> 0){?>
                                                        <span><?php echo __('상품')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]["goodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
<?php if($TPL_V3["mileage"]["memberMileage"]> 0){?>
                                                        <span><?php echo __('회원')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]["memberMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
<?php if($TPL_V3["mileage"]["couponGoodsMileage"]> 0){?>
                                                        <span><?php echo __('쿠폰')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]["couponGoodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
                                                    </li>
<?php }?>
<?php }else{?>
<?php if(($TPL_V3["price"]['goodsDcPrice'])> 0){?>
                                                    <li class="benefit_sale">
                                                        <em><?php echo __('할인')?></em>
<?php if($TPL_V3["price"]['goodsDcPrice']> 0){?>
                                                        <span><?php echo __('상품')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDcPrice'])?></strong></span>
<?php }?>
                                                    </li>
<?php }?>
<?php }?>
                                                </ul>
<?php }?>
                                        </td>
                                        <td>
                                            <strong class="order_sum_txt"><?php echo gd_global_currency_display($TPL_V3["price"]['goodsPriceSubtotal'])?></strong>
                                            <p class="add_currency"><?php echo gd_global_add_currency_display($TPL_V3["price"]['goodsPriceSubtotal'])?></p>
                                        </td>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
<?php if($TPL_V3["goodsDeliveryFl"]==='y'){?>
<?php if($TPL_I3=='0'){?>
                                        <td class="td_delivery" rowspan="<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsLineCnt']?>">
                                            <?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethod']?><br/>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['fixFl']==='free'){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreeFl']==='y'){?>
                                            <?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])===false){?>
                                            <!--(<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectFl']==='later'){?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])===false){?>
                                            <?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])?>

                                            <br/>(<?php echo __('상품수령 시 결제')?>)
<?php }?>
<?php }else{?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])===true){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
                                            <?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText'])===false){?>
                                            <br/>
                                            (<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText']?>)
<?php }?>
                                        </td>
<?php }?>
<?php }else{?>
                                        <td class="td_delivery">
                                            <?php echo $TPL_V3["goodsDeliveryMethod"]?><br/>
<?php if($TPL_V3["goodsDeliveryFixFl"]==='free'){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_V3["goodsDeliveryWholeFreeFl"]==='y'){?>
                                            <?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])===false){?>
                                            <!--<br/>(<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_V3["goodsDeliveryCollectFl"]==='later'){?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryCollectPrice'])===false){?>
                                            <?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryCollectPrice'])?>

                                            <br/>(<?php echo __('상품수령 시 결제')?>)
<?php }?>
<?php }else{?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryPrice'])===true){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
                                            <?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_V3["goodsDeliveryMethodFlText"])===false){?>
                                            <br />
                                            (<?php echo $TPL_V3["goodsDeliveryMethodFlText"]?>)
<?php }?>
                                        </td>
<?php }?>
<?php }?>
                                    </tr>
<?php }}?>
<?php }}?>

                                    </tbody>

<?php if($TPL_VAR["cartScmCnt"]> 1&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                                    <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <div class="price_sum">

                                                <strong class="price_shop_neme">[<?php echo $TPL_VAR["cartScmInfo"][$TPL_K1]['companyNm']?> <?php echo __('배송상품')?>]</strong>

                                                <div class="price_sum_cont">
                                                    <div class="price_sum_list">
                                                        <dl>
                                                            <dt><?php echo __('총 %s개의 상품금액','<strong>'.number_format($TPL_VAR["cartScmGoodsCnt"][$TPL_K1]).'</strong>')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
<?php if($TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1]> 0){?>
                                                        <span><img src="/data/skin/front/aileenwedding/img/order/order_price_minus.png" alt="<?php echo __('빼기')?>" /></span>
                                                        <dl>
                                                            <dt><?php echo __('상품할인')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
<?php }?>
<?php if(($TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]+$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1])> 0){?>
                                                        <span><img src="/data/skin/front/aileenwedding/img/order/order_price_minus.png" alt="<?php echo __('빼기')?>" /></span>
                                                        <dl>
                                                            <dt><?php echo __('회원할인')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format(($TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]+$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1]))?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
<?php }?>
<?php if($TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1]> 0){?>
                                                        <span><img src="/data/skin/front/aileenwedding/img/order/order_price_minus.png" alt="<?php echo __('빼기')?>" /></span>
                                                        <dl>
                                                            <dt><?php echo __('쿠폰할인')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                                        <span><img src="/data/skin/front/aileenwedding/img/order/order_price_plus.png" alt="<?php echo __('더하기')?>" /></span>
                                                        <dl>
                                                            <dt><?php echo __('배송비')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsDeliveryCharge"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
<?php }?>
                                                        <span><img src="/data/skin/front/aileenwedding/img/order/order_price_total.png" alt="<?php echo __('합계')?>" /></span>
                                                        <dl class="price_total">
                                                            <dt><?php echo __('합계')?></dt>
                                                            <dd><?php echo gd_global_currency_symbol()?><strong class="total"><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsPrice"][$TPL_K1]-$TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1]-$TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]-$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1]-$TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1]+$TPL_VAR["totalScmGoodsDeliveryCharge"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                                <!-- //price_sum_cont -->
                                            </div>
                                            <!-- //price_sum -->
                                        </td>
                                    </tr>
                                    </tfoot>
<?php }?>

                                </table>
                            </div>

                        </div>
<?php }}?>
                        <!-- //cart_cont_list -->
                        <!-- 장바구니 상품리스트 끝 -->

<?php if(empty($TPL_VAR["cartCnt"])===true){?>
                        <p class="no_data"><?php echo __('장바구니에 담겨있는 상품이 없습니다.')?></p>
<?php }?>

                    </form>

                    <div class="btn_left_box">
                        <a href="<?php echo $TPL_VAR["shoppingUrl"]?>" class="shop_go_link"><i class="xi-angle-left"></i><em><?php echo __('쇼핑 계속하기')?></em></a>
                    </div>

                    <div class="price_sum">
                        <div class="price_sum_cont">
                            <div class="price_sum_list">
                                <dl>
                                    <dt><?php echo __('총 %s 개의 상품금액 %s','<strong id="totalGoodsCnt">'.number_format($TPL_VAR["cartCnt"]).'</strong>','')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalGoodsPrice"><?php echo gd_global_money_format($TPL_VAR["totalGoodsPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                </dl>
<?php if($TPL_VAR["totalGoodsDcPrice"]> 0){?>
                                <span><i class="xi-minus"></i></span>
                                <dl>
                                    <dt><?php echo __('상품할인')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalGoodsDcPrice"><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                </dl>
<?php }?>
<?php if($TPL_VAR["totalSumMemberDcPrice"]> 0){?>
                                <span><i class="xi-minus"></i></span>
                                <dl>
                                    <dt><?php echo __('회원할인')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalMinusMember"><?php echo gd_global_money_format($TPL_VAR["totalSumMemberDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                </dl>
<?php }?>
<?php if($TPL_VAR["totalCouponGoodsDcPrice"]> 0){?>
                                <span><i class="xi-minus"></i></span>
                                <dl>
                                    <dt><?php echo __('쿠폰할인')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalCouponGoodsDcPrice"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                </dl>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                <span><i class="xi-plus"></i></span>
                                <dl>
                                    <dt><?php echo __('배송비')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalDeliveryCharge"><?php echo gd_global_money_format($TPL_VAR["totalDeliveryCharge"])?></strong><?php echo gd_global_currency_string()?></dd>
                                </dl>
<?php }?>
                                <span class="equal"><i class="xi-drag-handle"></i></span>
                                <dl class="price_total">
                                    <dt><?php echo __('합계')?></dt>
                                    <dd><?php echo gd_global_currency_symbol()?><strong id="totalSettlePrice"><?php echo gd_global_money_format($TPL_VAR["totalSettlePrice"])?></strong><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                        <div class="add_currency"><?php echo gd_global_add_currency_symbol()?><em id="totalSettlePriceAdd" ><?php echo gd_global_add_money_format($TPL_VAR["totalSettlePrice"])?></em><?php echo gd_global_add_currency_string()?></div>
<?php }?>
                                    </dd>
                                </dl>
                            </div>
                            <em id="deliveryChargeText" class="tobe_mileage" style="display:none;"></em>
<?php if(gd_is_login()===true&&$TPL_VAR["mileage"]["useFl"]=='y'){?>
                            <em class="tobe_mileage"><?php echo __('적립예정')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <span id="totalGoodsMileage"><?php echo gd_global_money_format($TPL_VAR["totalMileage"])?></span> <?php echo $TPL_VAR["mileage"]["unit"]?></em>
<?php }?>
<?php if($TPL_VAR["totalMemberBankDcPrice"]> 0){?>
                            <span>(<?php echo __('무통장결제 추가 할인')?> : <span id="totalMemberBankDcPrice" style="color:#e91818;">-<?php echo gd_global_currency_display($TPL_VAR["totalMemberBankDcPrice"])?></span> )</span>
<?php }?>
                        </div>
                        <!-- //price_sum_cont -->
                    </div>
                    <!-- //price_sum -->

<?php if($TPL_VAR["cartCnt"]> 0){?>
                    <div class="btn_order_box button_area inline">
                        <span class="btn_left_box">
                            <button type="button" class="btn_order_choice_del button small gray" onclick="gd_cart_process('cartDelete');"><?php echo __('선택 상품 삭제')?></button>
                            <button type="button" class="btn_order_choice_wish button small black" onclick="gd_cart_process('cartToWish');"><?php echo __('선택 상품 찜')?></button>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_CARTESTIMATE)===true&&$TPL_VAR["estimateUseFl"]=='y'){?>
                            <button type="button" class="btn_order_choice_wish button small deepgray" onclick="gd_cart_estimate_print();"><?php echo __('견적서 출력')?></button>
<?php }?>
                        </span>
                        <span class="btn_right_box">
                            <button type="button" class="btn_order_choice_buy button border orange" onclick="gd_cart_process('orderSelect');"><?php echo __('선택 상품 주문')?></button>
                            <button type="button" class="btn_order_whole_buy button orange" onclick="gd_order_all();"><?php echo __('전체 상품 주문')?></button>
                        </span>
                    </div>
                    <!-- //btn_order_box -->

<?php if($TPL_VAR["mileage"]["useFl"]=='y'){?>
                    <em class="chk_none"><i class="xi-info"></i> <?php echo __('주문서 작성단계에서 할인%s 적용을 하실 수 있습니다.','/'.$TPL_VAR["mileage"]["name"])?></em>
<?php }else{?>
                    <em class="chk_none"><i class="xi-info"></i> <?php echo __('주문서 작성단계에서 할인%s 적용을 하실 수 있습니다.','')?></em>
<?php }?>

                    <div class="pay_box">
                        <div class="payco_pay">
                            <?php echo $TPL_VAR["payco"]?>

                        </div>
                        <div class="naver_pay">
                            <?php echo $TPL_VAR["naverPay"]?>

                        </div>
                    </div>
                    <!-- //pay_box -->
<?php }?>
                </div>
                <!-- //cart_cont -->
            </div>
            <!-- //order_wrap -->
        </div>
        <!-- //content_box -->
    </div><!-- .center_layer -->
</section>

<?php if($TPL_VAR["couponUse"]=='y'){?>
<!-- 쿠폰 적용하기 레이어 -->
<div id="couponApplyLayer" class="layer_wrap coupon_apply_layer dn"></div>
<!--//쿠폰 적용하기 레이어 -->
<?php }?>

<!-- 옵션 변경하기 레이어 -->
<div id="optionViewLayer" class="layer_wrap dn"></div>
<!-- 옵션 변경하기 레이어 -->

<script type="text/javascript">
    <!--
    $(document).ready(function () {
        $('.js_impossible_layer').on('click', function(){
            $(".nomal_layer").addClass('dn');
            if ($(".nomal_layer").is(":hidden")) {
                $(this).next(".nomal_layer").removeClass('dn');
            }
        });

<?php if($TPL_VAR["couponUse"]=='y'){?>
        // 쿠폰 적용/변경 레이어
        $('.btn_open_layer').bind('click', function(e){
            if($(this).attr('href') == '#couponApplyLayer') {
                var params = {
                    mode: 'coupon_apply',
                    cartSno: $(this).data('cartsno'),
                };
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../order/layer_coupon_apply.php",
                    data: params,
                    success: function (data) {
                        $('#couponApplyLayer').empty().append(data);
                        $('#couponApplyLayer').find('>div').center();
                    },
                    error: function (data) {
//                        console.log(data);
                        alert(data);
                    }
                });
            }
        });
        // 쿠폰 취소
        $('.js_btn_coupon_cancel').bind('click', function(e){
            var cartSno = $(this).data('cartsno');
            $('[name="cart[cartSno]"]').val(cartSno);
            $('#frmCart input[name=\'mode\']').val('couponDelete');
            $('#frmCart').attr('method', 'post');
            $('#frmCart').attr('target', 'ifrmProcess');
            $('#frmCart').attr('action', '../order/cart_ps.php');
            $('#frmCart').submit();

            return false;
        });
<?php }?>
        // 숫자 체크
        //$('input[name*=\'goodsCnt\']').number_only();


<?php if(empty($TPL_VAR["cartCnt"])===false){?>
        var totalDeliveryCharge = numeral().unformat($('#totalDeliveryCharge').text());
        // 선택한 상품에 따른 금액 계산
        $('#frmCart input:checkbox[name="cartSno[]"], .gd_select_all_goods').click(function () {
            // 체크박스 전체 선택상태에 따른 체크박스 변경처리
            var checkedCount = 0;
            var $eachCheckBox = $(this).closest('table').find('tbody input[name="cartSno[]"]:checkbox');
            // 전체 및 개별 상품 선택 처리
            if ($(this).hasClass('gd_select_all_goods')) {
                var allCheckFl = $(this).prop('checked');
                $eachCheckBox.each(function(){
                    $(this).prop('checked', allCheckFl);
                    if (allCheckFl) {
                        $('label[for=' + $(this).attr('id') + ']').addClass('on');
                    } else {
                        $('label[for=' + $(this).attr('id') + ']').removeClass('on');
                    }
                });
            } else {
                $eachCheckBox.each(function(idx){
                    if ($(this).prop('checked') === true) {
                        checkedCount++;
                    }
                });
                if ($eachCheckBox.length == checkedCount) {
                    $(this).closest('table').find('thead > tr > th:first-child input[id*=allCheck]').prop('checked', true);
                    $(this).closest('table').find('thead > tr > th:first-child label[for*=allCheck]').addClass('on');
                } else {
                    $(this).closest('table').find('thead > tr > th:first-child input[id*=allCheck]').prop('checked', false);
                    $(this).closest('table').find('thead > tr > th:first-child label[for*=allCheck]').removeClass('on');
                }
            }

            window.setTimeout(function(){
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../order/cart_ps.php",
                    data: "mode=cartSelectCalculation&" + $('#frmCart input:checkbox[name="cartSno[]"]:checked').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                        $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', true);
                    }
                }).success(function (data) {
                    $('#totalGoodsCnt').html(numeral(data.cartCnt).format('0,0'));
                    $('#totalGoodsPrice').html(gd_money_format(data.totalGoodsPrice));
                    $('#totalGoodsDcPrice').html(gd_money_format(data.totalGoodsDcPrice));
                    $('#totalMinusMember').html(gd_money_format(data.totalMemberDcPrice));
                    $('#totalCouponGoodsDcPrice').html(gd_money_format(data.totalCouponGoodsDcPrice));
                    $('#totalSettlePrice').html(gd_money_format(data.totalSettlePrice));
                    $('#totalSettlePriceAdd').html(gd_add_money_format(data.totalSettlePrice));
                    $('#totalGoodsMileage').html(numeral(data.totalMileage).format());
                    $('#deliveryChargeText').html('');
                    $('#totalDeliveryCharge').html(gd_money_format(data.totalDeliveryCharge));
                    $('#totalMemberBankDcPrice').html(numeral(data.totalMemberBankDcPrice).format());
                    $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', false);
                }).error(function (e) {
                    alert(e);
                    $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', false);
                });
            }, 200);


        });

<?php }?>


        $('.btn_open_layer').bind('click', function(e){
            if($(this).attr('href') == '#optionViewLayer') {
                if($(this).data('coupon') == 'use') {
                    alert("<?php echo __('쿠폰 적용 취소 후 옵션 변경 가능합니다.')?>");
                    return false;
                } else {
                    var params = {
                        type : 'cart',
                        sno: $(this).data('sno'),
                        goodsNo: $(this).data('goodsno')
                    };

                    $.ajax({
                        method: "POST",
                        cache: false,
                        url: "../goods/layer_option.php",
                        data: params,
                        success: function (data) {
                            $('#optionViewLayer').empty().append(data);
                            $('#optionViewLayer').find('>div').center();
                        },
                        error: function (data) {
                            alert(data.message);

                        }
                    });

                }

            }
        });

<?php if($TPL_VAR["isPayLimit"]> 0){?>
        var adddHtml = '';
        adddHtml +=  "<strong><?php echo __('결제수단')?></strong>";
        adddHtml +=  "<ul>";
<?php if($TPL_useSettleKindPg_1){foreach($TPL_VAR["useSettleKindPg"] as $TPL_V1){?>
        adddHtml +=  "<li><?php echo $TPL_V1?></li>";
<?php }}?>
        adddHtml +=  "</ul>";
        $(adddHtml).appendTo('.icon_pg_cont');
<?php }else{?>
        $('.icon_pg_cont').addClass('dn');
<?php }?>

    });

    /**
     * 선택 상품 처리
     */
    function gd_cart_process(mode) {
<?php if(empty($TPL_VAR["cartCnt"])===true){?>
        alert("<?php echo __('장바구니에 담겨있는 상품이 없습니다.')?>");
<?php }else{?>
        // 선택한 상품 개수
        var checkedCnt = $('#frmCart  input:checkbox[name="cartSno[]"]:checked').length;

        // 모드에 따른 메시지 및 처리
        if (mode == 'cartDelete') {
            msg = "<?php echo __('상품을 장바구니에서 삭제 하시겠습니까?')?>";
        } else if (mode == 'cartToWish') {
            msg = "<?php echo __('상품을 찜리스트에 담으시겠습니까?')?>";
        } else if (mode == 'orderSelect') {
            msg = "<?php echo __('상품만 주문합니다.')?>";

            var alertMsg = gd_cart_cnt_info();
            if (alertMsg) {
                alert(alertMsg);
                return false;
            }

            // 구매 불가 체크
            var orderPossible = 'y';
            $('#frmCart  input:checkbox[name="cartSno[]"]:checked').each(function() {
                if ($(this).data('possible') == 'n') {
                    orderPossible = 'n';
                }
            });
            if (orderPossible == 'n') {
                alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s장바구니 상품을 확인해 주세요!','\n')?>");
                return false;
            }

            if (parseInt(checkedCnt) == parseInt(<?php echo $TPL_VAR["cartCnt"]?>)) {
                location.href='../order/order.php';
                return true;
            }
        } else {
            return false;
        }

        if (checkedCnt == 0) {
            alert("<?php echo __('선택하신 상품이 없습니다.')?>");
            return false;
        } else {
            if (confirm(__('선택하신 %i개', checkedCnt) + <?php if($TPL_VAR["gGlobal"]["locale"]!='ko'){?> ', ' + <?php }?> msg) === true) {
                $('#frmCart input[name=\'mode\']').val(mode);
                $('#frmCart').attr('method', 'post');
                $('#frmCart').attr('target', 'ifrmProcess');
                $('#frmCart').attr('action', '../order/cart_ps.php');
                $('#frmCart').submit();
            }
            return true;
        }
<?php }?>
    }

    /**
     * 전체 상품 주문
     *
     */
    function gd_order_all() {
<?php if(empty($TPL_VAR["cartCnt"])===true){?>
        alert("<?php echo __('장바구니에 담겨있는 상품이 없습니다.')?>");
<?php }else{?>
        var alertMsg = gd_cart_cnt_info('all');
        if (alertMsg) {
            alert(alertMsg);
            return false;
        }
<?php if($TPL_VAR["orderPossible"]===true){?>
        location.href='../order/order.php';
<?php }else{?>
<?php if($TPL_VAR["orderPossibleMessage"]){?>
        alert("<?php echo __($TPL_VAR["orderPossibleMessage"])?>");
<?php }else{?>
        alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s장바구니 상품을 확인해 주세요!','\n')?>");
<?php }?>
<?php }?>
<?php }?>
    }

    /**
     * 장바구니 비우기
     */

    function gd_cart_remove() {
        if (confirm("<?php echo __('장바구니를 비우시겠습니까?')?>") === true) {
            ifrmProcess.location.replace('./cart_ps.php?mode=remove');
        }
    }

    /**
     * 재고 체크
     *
     * @param intger stockLimit 현재 상품의 총 재고
     * @param intger thisCnt 현재 구매 수량
     * @param intger thisIndex 현재 상품의 index
     */
    function gd_stock_check(stockLimit, thisCnt, thisIndex) {
        if (stockLimit < thisCnt) {
            alert('재고가 부족합니다. 현재 ' + stockLimit + '개의 재고가 남아 있습니다.');
            $('input[name=\'goodsCnt[]\']').eq(thisIndex).val(stockLimit);
        }
    }


    /**
     * 옵션변경 처리
     *
     * @param string params 옵션변경정보
     * @param intger sno 장바구니 고유번호
     */
    function gd_option_view_result(params,sno) {

        params += "&mode=cartUpdate&sno="+sno;

        $.ajax({
            method: "POST",
            cache: false,
            url: "../order/cart_ps.php",
            data: params,
            success: function (data) {
                document.location.reload();
            },
            error: function (data) {
                alert(data.message);
            }
        });

    }

    function gd_cart_cnt_info(mode) {
        var target = 'input[name="cartSno[]"]';
        if (mode != 'all') target += ':checked';

        var goodsCntData = [];
        $.each($(target), function(){
            var $goodsCnt = $(this);
            var goodsKey = $goodsCnt.data('goods-key');
            if (goodsCntData[goodsKey]) {
                goodsCntData[goodsKey] += $goodsCnt.data('default-goods-cnt');
            } else {
                goodsCntData[goodsKey] = $goodsCnt.data('default-goods-cnt');
            }
        });

        var msgByUnit = [];
        var msgByCnt = [];
        var msg;
        $.each(goodsCntData, function(index, value){
            if (_.isUndefined(value)) return true;

            var $data = $(target + '[data-goods-key="' + index + '"]');

            if ($data.data('fixed-sales') == 'goods') {
                if (value % $data.data('sales-unit') > 0) {
                    msg = $data.data('goods-nm') + ' ' + $data.data('sales-unit') + __('개');
                    msgByUnit['goods'] = msgByUnit['goods'] ? msgByUnit['goods'] + '\n' + msg : msg;
                }
            } else {
                $.each($data, function(){
                    if ($(this).data('default-goods-cnt') % $(this).data('sales-unit') > 0) {
                        msg = $(this).data('goods-nm') + '(' + $(this).data('option-nm') + ')' + ' ' + $(this).data('sales-unit') + __('개');
                        msgByUnit['option'] = msgByUnit['option'] ? msgByUnit['option'] + '\n' + msg : msg;
                    }
                });
            }
            if ($data.data('fixed-order-cnt') == 'goods') {
                if ($data.data('min-order-cnt') > 1 && $data.data('min-order-cnt') > value) {
                    msg = __('%1$s 최소 %2$s개 이상', [$data.data('goods-nm'), $data.data('min-order-cnt')]);
                    msgByCnt['goods'] = msgByCnt['goods'] ?  msgByCnt['goods'] + '\n' + msg : msg;
                }
                if ($data.data('max-order-cnt') > 0 && $data.data('max-order-cnt') < value) {
                    msg = __('%1$s 최대 %2$s개 이하', [$data.data('goods-nm'), $data.data('max-order-cnt')]);
                    msgByCnt['goods'] = msgByCnt['goods'] ?  msgByCnt['goods'] + '\n' + msg : msg;
                }
            } else {
                $.each($data, function(){
                    if ($(this).data('min-order-cnt') > 1 && $(this).data('min-order-cnt') > $(this).data('default-goods-cnt')) {
                        msg = __('%1$s(%2$s) 최소 %3$s개 이상', [$(this).data('goods-nm'), $(this).data('option-nm'), $(this).data('min-order-cnt')]);
                        msgByCnt['option'] = msgByCnt['option'] ?  msgByCnt['option'] + '\n' + msg : msg;
                    }
                    if ($(this).data('max-order-cnt') > 0 && $(this).data('max-order-cnt') < $(this).data('default-goods-cnt')) {
                        msg = __('%1$s(%2$s) 최대 %3$s개 이하', [$(this).data('goods-nm'), $(this).data('option-nm'), $(this).data('max-order-cnt')]);
                        msgByCnt['option'] = msgByCnt['option'] ?  msgByCnt['option'] + '\n' + msg : msg;
                    }
                });
            }
        });

        var alertMsg = [];
        var msg;
        if (msgByUnit['option']) {
            msg = __('옵션기준');
            msg += '\n';
            msg += __('%1$s단위로 묶음 주문 상품입니다.', msgByUnit['goods']);
            alertMsg.push(msg);
        }
        if (msgByUnit['goods']) {
            msg = __('상품기준');
            msg += '\n';
            msg += __('%1$s단위로 묶음 주문 상품입니다.', msgByUnit['goods']);
            alertMsg.push(msg);
        }
        if (alertMsg.length) {
            return alertMsg.join('\n\n');
        }

        if (msgByCnt['option']) {
            alertMsg.push(__('옵션기준\n%1$s구매가능합니다.', msgByCnt['option']));
        }
        if (msgByCnt['goods']) {
            alertMsg.push(__('상품기준\n%1$s구매가능합니다.', msgByCnt['goods']));
        }
        if (alertMsg.length) {
            return alertMsg.join('\n\n');
        }
    }

<?php if(gd_is_plus_shop(PLUSSHOP_CODE_CARTESTIMATE)===true&&$TPL_VAR["estimateUseFl"]=='y'){?>
    /**
     * 선택 상품 견적서 출력
     */
    function gd_cart_estimate_print() {
<?php if(empty($TPL_VAR["cartCnt"])===true){?>
        alert("<?php echo __('장바구니에 담겨있는 상품이 없습니다.')?>");
<?php }else{?>
        // 선택한 상품 개수
        var checkedCnt = $('#frmCart').find('input:checkbox[name="cartSno[]"]:checked').length;

        var msg = "<?php echo __('상품의 견적서를 출력하시겠습니까?')?>";

        if (checkedCnt == 0) {
            alert("<?php echo __('선택하신 상품이 없습니다.')?>");
            return false;
        } else {
            if (confirm(__('선택하신 %i개', checkedCnt) + <?php if($TPL_VAR["gGlobal"]["locale"]!='ko'){?> ', ' + <?php }?> msg) === true) {
                // 구매 불가 체크
                var orderPossible = 'y';
                $('#frmCart').find('input:checkbox[name="cartSno[]"]:checked').each(function() {
                    if ($(this).data('possible') == 'n') {
                        orderPossible = 'n';
                    }
                });
                if (orderPossible == 'n') {
                    alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s장바구니 상품을 확인해 주세요!','\n')?>");
                    return false;
                }

                var estimatePrint = gd_popup({
                    target: 'cartEstimatePrint'
                    , url: ''
                    , width: 750
                    , height: 800
                    , resizable: 'yes'
                    , scrollbars: 'yes'
                    , menubar: 'yes'
                });

                $('#frmCart').find('input[name="mode"]').val('cartEstimate');
                $('#frmCart').attr('method', 'post');
                $('#frmCart').attr('target', 'cartEstimatePrint');
                $('#frmCart').attr('action', '../order/cart_estimate.php');
                $('#frmCart').submit();
                estimatePrint.focus();
            }
        }
<?php }?>
    }
<?php }?>

    //-->
</script>
<?php echo $TPL_VAR["fbCartScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>