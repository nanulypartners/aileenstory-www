<?php /* Template_ 2.2.7 2020/04/14 03:32:27 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/order/layer_coupon_apply.html 000017134 */ 
if (is_array($TPL_VAR["memberCouponArrData"])) $TPL_memberCouponArrData_1=count($TPL_VAR["memberCouponArrData"]); else if (is_object($TPL_VAR["memberCouponArrData"]) && in_array("Countable", class_implements($TPL_VAR["memberCouponArrData"]))) $TPL_memberCouponArrData_1=$TPL_VAR["memberCouponArrData"]->count();else $TPL_memberCouponArrData_1=0;
if (is_array($TPL_VAR["cartCouponArrData"])) $TPL_cartCouponArrData_1=count($TPL_VAR["cartCouponArrData"]); else if (is_object($TPL_VAR["cartCouponArrData"]) && in_array("Countable", class_implements($TPL_VAR["cartCouponArrData"]))) $TPL_cartCouponArrData_1=$TPL_VAR["cartCouponArrData"]->count();else $TPL_cartCouponArrData_1=0;?>
<div class="layer_wrap_cont" style="position: absolute; margin: 0px; top: 616px; left: 557px;">
    <div class="ly_tit">
        <h4><?php echo __('쿠폰 적용하기')?>

<?php if(gd_is_login()===false){?>
<?php }else{?>
            <span><?php echo __('선택한 상품에 적용가능한 총')?> <strong><?php echo $TPL_memberCouponArrData_1?><?php echo __('개')?></strong><?php echo __('의 보유쿠폰이 있습니다.')?></span>
<?php }?>
        </h4>
    </div>
    <div class="ly_cont">
        <div class="scroll_box">
            <div class="top_table_type">
                <table>
                    <colgroup>
                        <col style="width:50px;">
                        <col style="width:200px">
                        <col style="width:200px">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?php echo __('쿠폰')?></th>
                        <th><?php echo __('사용조건')?></th>
                        <th><?php echo __('사용기한')?></th>
                    </tr>
                    </thead>
                    <tbody>
<?php if($TPL_cartCouponArrData_1){foreach($TPL_VAR["cartCouponArrData"] as $TPL_K1=>$TPL_V1){?>
                    <tr>
                        <td>
                            <span class="form_element">
<?php if($TPL_VAR["convertCartCouponPriceArrData"]["memberCouponAlertMsg"][$TPL_V1["memberCouponNo"]]=='LIMIT_MIN_PRICE'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" checked="checked" disabled="disabled">
                                <label class="check_dis_s single" for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }else{?>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" checked="checked" data-price="<?php echo $TPL_VAR["convertCartCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" checked="checked" data-price="<?php echo $TPL_VAR["convertCartCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" checked="checked" data-price="<?php echo $TPL_VAR["convertCartCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }?>
                                <label class="check_s single on" for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }?>
                            </span>
                        </td>
                        <td class="td_left">
                            <label for="check<?php echo $TPL_V1["memberCouponNo"]?>">
                                <strong class="coupon_price">
                                    <b>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertCartCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertCartCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertCartCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }?>
                                    </b>
                                    <?php echo $TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponKindType"]?>

                                </strong>
                                <span class="text_info"><?php echo $TPL_V1["couponNm"]?></span>
                            </label>
                        </td>
                        <td class="td_left">
<?php if($TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponMaxBenefit"]){?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponMaxBenefit"]?></span>
<?php }?>
<?php if($TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponMinOrderPrice"]){?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponMinOrderPrice"]?></span>
<?php }?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertCartCouponArrData"][$TPL_K1]["couponApplyDuplicateType"]?></span>
                        </td>
                        <td><span><?php echo $TPL_V1["memberCouponEndDate"]?></span></td>
                    </tr>
<?php }}?>
<?php if($TPL_memberCouponArrData_1){foreach($TPL_VAR["memberCouponArrData"] as $TPL_K1=>$TPL_V1){?>
                    <tr>
                        <td>
                            <span class="form_element">
<?php if($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAlertMsg"][$TPL_V1["memberCouponNo"]]=='LIMIT_MIN_PRICE'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" disabled="disabled">
                                <label class="check_dis_s single" for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }else{?>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                <input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="memberCouponNo[]" class="checkbox" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }?>
                                <label class="check_s single" for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }?>
                            </span>
                        </td>
                        <td class="td_left">
                            <label for="check<?php echo $TPL_V1["memberCouponNo"]?>">
                                <strong class="coupon_price">
                                    <b>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
                                        <?php echo gd_currency_symbol()?><?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]])?><?php echo gd_currency_string()?>

<?php }?>
                                    </b>
                                    <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponKindType"]?>

                                </strong>
                                <span class="text_info"><?php echo $TPL_V1["couponNm"]?></span>
                            </label>
                        </td>
                        <td class="td_left">
<?php if($TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMaxBenefit"]){?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMaxBenefit"]?></span>
<?php }?>
<?php if($TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMinOrderPrice"]){?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponMinOrderPrice"]?></span>
<?php }?>
                            <span class="text_info">- <?php echo $TPL_VAR["convertMemberCouponArrData"][$TPL_K1]["couponApplyDuplicateType"]?></span>
                        </td>
                        <td><span><?php echo $TPL_V1["memberCouponEndDate"]?></span></td>
                    </tr>
<?php }}?>
                    </tbody>
                </table>
            </div>
<?php if(gd_is_login()===false){?>
            <div class="coupon_down_txt">
                <p><?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?></p>
                <div class="btn_center_box">
                    <a href="../member/join_method.php" class="btn_ly_join"><strong><?php echo __('회원가입')?></strong></a>
                    <a href="../member/login.php" class="btn_ly_login"><strong><?php echo __('로그인')?></strong></a>
                </div>
            </div>
            <!-- //coupon_down_txt -->
<?php }?>

        </div>
        <!-- //scroll_box -->
        <div class="coupon_total_box">
            <dl>
                <dt><?php echo __('총 할인금액')?></dt>
                <dd><?php echo gd_currency_symbol()?><strong id="couponSalePrice">0</strong><?php echo gd_currency_string()?></dd>
            </dl>
            <dl>
                <dt><?php echo __('총 적립금액')?></dt>
                <dd><?php echo gd_currency_symbol()?><strong id="couponAddPrice">0</strong><?php echo gd_currency_string()?></dd>
            </dl>
        </div>
        <!-- //coupon_total_box -->
        <div class="btn_center_box button_area">
            <button type="button" class="btn_ly_cancel layer_close button inline gray"><strong><?php echo __('취소')?></strong></button>
            <button type="button" id="btnCouponApply" class="btn_ly_coupon_apply button inline orange"><strong><?php echo __('쿠폰 적용')?></strong></button>
        </div>
    </div>
    <!-- //ly_cont -->
    <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="닫기"></a>
</div>
<!-- //layer_wrap_cont -->
</div>
<!-- //layer_wrap -->



<?php if(gd_is_login()!==false){?>
<script type="text/javascript">
    <!--
    $(document).ready(function () {
        $('input:checkbox[name="memberCouponNo[]"]').click(function (e) {
            if (($(this).prop('checked') == true) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).attr("checked", false);
                    $(this).next('label').removeClass('on');
                    $(this).attr('disabled', 'disabled');
                });
            } else if (($(this).prop('checked') == false) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).removeAttr('disabled', 'disabled');
                });
            }
            gd_coupon_price_sum();
        });
        $('#btnCouponApply').click(function (e) {
            var couponApplyNoArr = [];
            $('input:checkbox[name="memberCouponNo[]"]:checked').each(function (index) {
                couponApplyNoArr[index] = $(this).val();
            });
            var couponApplyNoString = couponApplyNoArr.join('<?php echo INT_DIVISION?>');
            $('[name="cart[cartSno]"]').val('<?php echo $TPL_VAR["cartSno"]?>');
            $('[name="cart[couponApplyNo]"]').val(couponApplyNoString);

<?php if($TPL_VAR["action"]=='cart_tab'){?>
                $('#frmTabCart input[name=\'mode\']').val('couponApply');
                var params = $( "#frmTabCart" ).serialize();

                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../order/cart_ps.php",
                    data: params,
                    success: function (data) {
                        gd_close_layer();
                        gd_cart_tab_action('#cart_tab_cart');
                    },
                    error: function (data) {
                        alert(data.message);

                    }
                });

<?php }else{?>
                $('#frmCart input[name=\'mode\']').val('couponApply');
                $('#frmCart').attr('method', 'post');
                $('#frmCart').attr('target', 'ifrmProcess');
                $('#frmCart').attr('action', '../order/cart_ps.php');
                $('#frmCart').submit();
<?php }?>
            return false;
        });
        gd_coupon_apply_setting();
        gd_coupon_price_sum();
    });

    // 쿠폰 적용 내용 초기화 (설정)
    function gd_coupon_apply_setting() {
        $.each($('input:checkbox[name="memberCouponNo[]"]:checked'), function (index){
            if ($(this).data('duplicate') == 'n') {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).attr("checked", false);
                    $(this).next('label').removeClass('on');
                    $(this).attr('disabled', 'disabled');
                });
            } else if ($(this).data('duplicate') == 'n') {
                $('input:checkbox[name="memberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).removeAttr('disabled', 'disabled');
                });
            }
        });
    }

    // 선택 쿠폰 금액 계산
    function gd_coupon_price_sum() {
        var salePrice = 0;
        var addPrice = 0;
        $('input:checkbox[name="memberCouponNo[]"]:checked').each(function (index) {
            if ($(this).data('type') == 'sale') {
                salePrice += parseFloat($(this).data('price'));
            } else if ($(this).data('type') == 'add') {
                addPrice += parseFloat($(this).data('price'));
            }
        });

        $('#couponSalePrice').text(numeral(salePrice).format());
        $('#couponAddPrice').text(numeral(addPrice).format());
    }
    //-->
</script>
<?php }?>