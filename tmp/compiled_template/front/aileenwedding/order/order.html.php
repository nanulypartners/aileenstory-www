<?php /* Template_ 2.2.7 2020/05/24 15:59:19 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/order/order.html 000210709 */  $this->include_("dataEggBanner");
if (is_array($TPL_VAR["cartInfo"])) $TPL_cartInfo_1=count($TPL_VAR["cartInfo"]); else if (is_object($TPL_VAR["cartInfo"]) && in_array("Countable", class_implements($TPL_VAR["cartInfo"]))) $TPL_cartInfo_1=$TPL_VAR["cartInfo"]->count();else $TPL_cartInfo_1=0;
if (is_array($TPL_VAR["giftInfo"])) $TPL_giftInfo_1=count($TPL_VAR["giftInfo"]); else if (is_object($TPL_VAR["giftInfo"]) && in_array("Countable", class_implements($TPL_VAR["giftInfo"]))) $TPL_giftInfo_1=$TPL_VAR["giftInfo"]->count();else $TPL_giftInfo_1=0;
if (is_array($TPL_VAR["emailDomain"])) $TPL_emailDomain_1=count($TPL_VAR["emailDomain"]); else if (is_object($TPL_VAR["emailDomain"]) && in_array("Countable", class_implements($TPL_VAR["emailDomain"]))) $TPL_emailDomain_1=$TPL_VAR["emailDomain"]->count();else $TPL_emailDomain_1=0;
if (is_array($TPL_VAR["bank"])) $TPL_bank_1=count($TPL_VAR["bank"]); else if (is_object($TPL_VAR["bank"]) && in_array("Countable", class_implements($TPL_VAR["bank"]))) $TPL_bank_1=$TPL_VAR["bank"]->count();else $TPL_bank_1=0;
if (is_array($TPL_VAR["useSettleKindPg"])) $TPL_useSettleKindPg_1=count($TPL_VAR["useSettleKindPg"]); else if (is_object($TPL_VAR["useSettleKindPg"]) && in_array("Countable", class_implements($TPL_VAR["useSettleKindPg"]))) $TPL_useSettleKindPg_1=$TPL_VAR["useSettleKindPg"]->count();else $TPL_useSettleKindPg_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<section class="section_container" <?php if($TPL_VAR["req"]["noheader"]=='y'){?>style="padding:0;"<?php }?>>
    <div class="center_layer">
        <div class="top_layer">
            <ul class="tab_area inline">
                <li class="on">
                    <a href="#" onclick="return false"><?php echo __('주문서작성/결제')?></a>
                    <span class="underline"></span>
                </li>
            </ul><!-- .tab_area -->
        </div><!-- .top_layer -->     
        <div class="section_layer content_box">
            <form id="frmOrder" name="frmOrder" action="./order_ps.php" method="post" target="ifrmProcess">
                <input type="hidden" name="csrfToken" value="<?php echo $TPL_VAR["token"]?>" />
                <input type="hidden" name="orderChannelFl" value="shop" />
                <input type="hidden" name="orderCountryCode" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['countryCode'])?>" />
                <input type="hidden" name="orderZipcode" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['zipcode'])?>" />
                <input type="hidden" name="orderZonecode" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['zonecode'])?>" />
                <input type="hidden" name="orderState" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['state'])?>" />
                <input type="hidden" name="orderCity" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['city'])?>" />
                <input type="hidden" name="orderAddress" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['address'])?>" />
                <input type="hidden" name="orderAddressSub" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['addressSub'])?>" />
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                <input type="hidden" name="orderPhonePrefixCode" value="kr" />
                <input type="hidden" name="orderPhonePrefix" value="82" />
                <input type="hidden" name="orderCellPhonePrefixCode" value="kr" />
                <input type="hidden" name="orderCellPhonePrefix" value="82" />
                <input type="hidden" name="receiverCountryCode" value="kr" />
                <input type="hidden" name="receiverPhonePrefixCode" value="kr" />
                <input type="hidden" name="receiverPhonePrefix" value="82" />
                <input type="hidden" name="receiverCellPhonePrefixCode" value="kr" />
                <input type="hidden" name="receiverCellPhonePrefix" value="82" />
                <input type="hidden" name="receiverState" value="" />
                <input type="hidden" name="receiverCity" value="" />
<?php }?>
                <input type="hidden" name="chooseMileageCoupon" value="<?php echo $TPL_VAR["chooseMileageCoupon"]?>" />
                <input type="hidden" name="chooseCouponMemberUseType" value="<?php echo $TPL_VAR["couponConfig"]['chooseCouponMemberUseType']?>" />
                <input type="hidden" name="totalCouponGoodsDcPrice" value="<?php echo $TPL_VAR["totalCouponGoodsDcPrice"]?>" />
                <input type="hidden" name="totalCouponGoodsMileage" value="<?php echo $TPL_VAR["totalCouponGoodsMileage"]?>" />
                <input type="hidden" name="totalMemberDcPrice" value="<?php echo $TPL_VAR["totalMemberDcPrice"]?>" />
                <input type="hidden" name="totalMemberBankDcPrice" value="<?php echo $TPL_VAR["totalMemberBankDcPrice"]?>" />
                <input type="hidden" name="totalMemberOverlapDcPrice" value="<?php echo $TPL_VAR["totalMemberOverlapDcPrice"]?>" />
                <input type="hidden" name="deliveryFree" value="<?php echo $TPL_VAR["deliveryFree"]?>" />
                <input type="hidden" name="totalDeliveryFreePrice" value="" />
                <input type="hidden" name="cartGoodsCnt" value="<?php echo $TPL_VAR["cartGoodsCnt"]?>" />
                <input type="hidden" name="cartAddGoodsCnt" value="<?php echo $TPL_VAR["cartAddGoodsCnt"]?>" />
                <input type="hidden" name="productCouponChangeLimitType" value="<?php echo $TPL_VAR["productCouponChangeLimitType"]?>" />
                <input type="hidden" name="resetMemberBankDcPrice" value="" />

                <div class="order_wrap">
                    <div class="order_tit">
                        <ol>
                            <li>
                                <span>01</span> <?php echo __('장바구니')?>

                            </li><li class="separate"><i class="xi-angle-right"></i></li><li class="page_on">
                                <span>02</span> <?php echo __('주문서작성/결제')?>

                            </li><li class="page_on separate"><i class="xi-angle-right"></i></li><li>
                                <span>03</span> <?php echo __('주문완료')?>

                            </li>
                        </ol>
                    </div><!-- //order_tit -->

                    <div class="order_cont">

                        <div class="cart_cont_list">
                            <div class="order_table_type">
                                <!-- 장바구니 상품리스트 시작 -->
                                <table class="table c_table">
                                    <colgroup>
                                        <col>					<!-- 상품명/옵션 -->
                                        <col style="width:130px;">  <!-- 수량 -->
                                        <col style="width:100px;"> <!-- 상품금액 -->
                                        <col style="width:100px;"> <!-- 할인/적립 -->
                                        <col style="width:100px;"> <!-- 합계금액 -->
                                        <col style="width:100px;"> <!-- 배송비 -->
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th><?php echo __('상품/옵션 정보')?></th>
                                        <th><?php echo __('수량')?></th>
                                        <th><?php echo __('상품금액')?></th>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <th><?php echo __('할인/적립')?></th>
<?php }?>
                                        <th><?php echo __('합계금액')?></th>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <th><?php echo __('배송비')?></th>
<?php }?>
                                    </tr>
                                    </thead>
                                    <tbody>


<?php if($TPL_cartInfo_1){foreach($TPL_VAR["cartInfo"] as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
                                    <tr>
                                        <td class="td_left title">
                                            <input type="hidden" name="cartSno[]" value="<?php echo $TPL_V3["sno"]?>" />
                                            <input type="hidden" name="memberDcInfo[<?php echo $TPL_V3["goodsNo"]?>][<?php echo $TPL_V3["optionSno"]?>]" value='<?php echo $TPL_V3["memberDcInfo"]?>' />
                                            <input type="hidden" name="priceInfo[<?php echo $TPL_V3["goodsNo"]?>][<?php echo $TPL_V3["optionSno"]?>]" value='<?php echo $TPL_V3["priceInfo"]?>' />
                                            <input type="hidden" name="mileageInfo[<?php echo $TPL_V3["goodsNo"]?>][<?php echo $TPL_V3["optionSno"]?>]" value='<?php echo $TPL_V3["mileageInfo"]?>' />
                                            <div class="pick_add_cont">
                                                <span class="pick_add_img">
                                                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsImage"]?></a>
                                                </span>
                                                <div class="pick_add_info">

<?php if($TPL_V3["orderPossible"]==='n'){?>
                                                    <strong class="chk_none"><?php echo __('구매 불가')?>: <?php echo $TPL_V3["orderPossibleMessage"]?></strong>
<?php }?>
<?php if($TPL_V3["duplicationGoods"]==='y'){?>
                                                    <strong class="chk_none"><?php echo __('중복 상품')?></strong>
<?php }?>

                                                    <em><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsNm"]?></a></em>

<?php if($TPL_V3["payLimitFl"]=='y'&&is_array($TPL_V3["payLimit"])){?>
                                                    <div class="icon_pick_list">
<?php if((is_array($TPL_R4=$TPL_V3["payLimit"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
<?php if($TPL_V4=='pg'){?>
                                                        <div class="icon_pg_over">
                                                            <img src="/data/skin/front/aileenwedding/img/icon/order/icon_settle_kind_<?php echo $TPL_V4?>.png">
                                                            <div class="icon_pg_cont"></div>
                                                        </div>
<?php }else{?>
                                                        <img src="/data/skin/front/aileenwedding/img/icon/order/icon_settle_kind_<?php echo $TPL_V4?>.png">
<?php }?>
<?php }}?>
                                                    </div>
<?php }?>
                                                    <!-- //icon_pick_list -->

                                                    <div class="pick_option_box">
<?php if((is_array($TPL_R4=$TPL_V3["option"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {$TPL_S4=count($TPL_R4);$TPL_I4=-1;foreach($TPL_R4 as $TPL_V4){$TPL_I4++;?>
<?php if($TPL_V4["optionName"]){?>
                                                        <span class="text_type_cont"><?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?> <?php if((($TPL_I4+ 1)==$TPL_S4)&&$TPL_V4["optionPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?><strong>(<?php if($TPL_V4["optionPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionPrice"])?>)</strong><?php }?><?php if(empty($TPL_V4["optionSellStr"])===false){?>[<?php echo $TPL_V4["optionSellStr"]?>]<?php }?><?php if((($TPL_I4+ 1)==$TPL_S4)){?><?php if(empty($TPL_V4["optionDeliveryStr"])===false){?>[<?php echo $TPL_V4["optionDeliveryStr"]?>]<?php }?><?php }?></span>
<?php }?>
<?php }}?>
                                                    </div>

                                                    <div class="pick_option_box">
<?php if((is_array($TPL_R4=$TPL_V3["optionText"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
<?php if($TPL_V4["optionValue"]){?>
                                                        <span class="text_type_cont"><?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?> <?php if($TPL_V4["optionTextPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?><strong>(<?php if($TPL_V4["optionTextPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionTextPrice"])?>)</strong><?php }?></span>
<?php }?>
<?php }}?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- //pick_add_cont -->

<?php if(empty($TPL_V3["addGoods"])===false){?>
                                            <div class="pick_add_list">
<?php if((is_array($TPL_R4=$TPL_V3["addGoods"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
                                                <div class="pick_add_cont">
                                                    <span class="pick_add_plus"><em><?php echo __('추가')?></em></span>
                                                    <span class="pick_add_img"><?php echo $TPL_V4["addGoodsImage"]?></span>
                                                    <div class="pick_add_info">
                                                        <em>
                                                            <b><?php echo $TPL_V4["addGoodsNm"]?></b> <br> <?php echo __('수량')?> : <?php echo $TPL_V4["addGoodsCnt"]?><?php echo __('개')?> (+<?php echo gd_global_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?>)
                                                            <!--<a href="#;"><img src="/data/skin/front/aileenwedding/img/icon/etc/icon_order_cart_del.png" alt="삭제"></a>-->
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                            <p class="add_currency"><?php echo gd_global_add_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?></p>
<?php }?>
                                                        </em>
                                                        <div class="pick_option_box">
<?php if($TPL_V4["optionNm"]){?>
                                                            <span class="text_type_cont"><?php echo __('옵션')?> : <?php echo $TPL_V4["optionNm"]?></span>
<?php }?>
                                                        </div>
                                                    </div>
                                                </div>
<?php }}?>
                                                <!-- //pick_add_cont -->
                                            </div>
<?php }?>
                                            <!-- //pick_add_list -->

                                        </td>
                                        <td class="td_order_amount">
                                            <div class="order_goods_num">
                                                <strong><?php echo $TPL_V3["goodsCnt"]?><?php echo __('개')?></strong>
                                            </div>
                                        </td>
                                        <td>
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                            <strong class="order_sum_txt"><?php echo $TPL_V3["goodsPriceString"]?></strong>
<?php }else{?>
                                            <strong class="order_sum_txt <?php if($TPL_V3["timeSaleFl"]){?>time_sale_cost<?php }else{?>price<?php }?>"><?php echo gd_global_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?></strong>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            <p class="add_currency"><?php echo gd_global_add_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?></p>
<?php }?>
<?php }?>
                                        </td>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <td class="td_benefit">
                                            <ul class="benefit_list">
<?php if(($TPL_V3["price"]['goodsDcPrice']+$TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice']+$TPL_V3["price"]['couponGoodsDcPrice'])> 0){?>
                                                <li class="benefit_sale">
                                                    <em><?php echo __('할인')?></em>
<?php if($TPL_V3["price"]['goodsDcPrice']> 0){?>
                                                    <span><?php echo __('상품')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDcPrice'])?></strong></span>
<?php }?>
<?php if(($TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice'])> 0){?>
                                                    <span><?php echo __('회원')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['memberDcPrice']+$TPL_V3["price"]['memberOverlapDcPrice'])?></strong></span>
<?php }?>
<?php if($TPL_V3["price"]['couponGoodsDcPrice']> 0){?>
                                                    <span><?php echo __('쿠폰')?> <strong>-<?php echo gd_global_currency_display($TPL_V3["price"]['couponGoodsDcPrice'])?></strong></span>
<?php }?>
                                                </li>
<?php }?>
<?php if($TPL_VAR["mileage"]['useFl']==='y'&&($TPL_V3["mileage"]['goodsMileage']+$TPL_V3["mileage"]['memberMileage']+$TPL_V3["mileage"]["couponGoodsMileage"])> 0){?>
                                                <li class="benefit_mileage js_mileage">
                                                    <em><?php echo __('적립')?></em>
<?php if($TPL_V3["mileage"]['goodsMileage']> 0){?>
                                                    <span><?php echo __('상품')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]['goodsMileage'])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
<?php if($TPL_V3["mileage"]['memberMileage']> 0){?>
                                                    <span><?php echo __('회원')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]['memberMileage'])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
<?php if($TPL_V3["mileage"]["couponGoodsMileage"]> 0){?>
                                                    <span><?php echo __('쿠폰')?> <strong>+<?php echo gd_global_money_format($TPL_V3["mileage"]["couponGoodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></span>
<?php }?>
                                                </li>
<?php }?>
                                            </ul>
                                        </td>
<?php }?>
                                        <td>
                                            <strong class="order_sum_txt"><?php echo gd_global_currency_display($TPL_V3["price"]['goodsPriceSubtotal'])?></strong>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            <p class="add_currency"><?php echo gd_global_add_currency_display($TPL_V3["price"]['goodsPriceSubtotal'])?></p>
<?php }?>
                                        </td>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
<?php if($TPL_V3["goodsDeliveryFl"]==='y'){?>
<?php if($TPL_I3=='0'){?>
                                        <td class="td_delivery" rowspan="<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsLineCnt']?>">
                                            <?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethod']?><br/>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['fixFl']==='free'){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreeFl']==='y'){?>
                                            <?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])===false){?>
                                            <!--(<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectFl']==='later'){?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])===false){?>
                                            <?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])?>

                                            <br/>(<?php echo __('상품수령 시 결제')?>)
<?php }?>
<?php }else{?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])===true){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
                                            <?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText'])===false){?>
                                            <br />
                                            (<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText']?>)
<?php }?>
                                        </td>
<?php }?>
<?php }else{?>
                                        <td class="td_delivery">
                                            <?php echo $TPL_V3["goodsDeliveryMethod"]?><br/>
<?php if($TPL_V3["goodsDeliveryFixFl"]==='free'){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_V3["goodsDeliveryWholeFreeFl"]==='y'){?>
                                            <?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])===false){?>
                                            <!--<br/>(<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_V3["goodsDeliveryCollectFl"]==='later'){?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryCollectPrice'])===false){?>
                                            <?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryCollectPrice'])?>

                                            <br/>(<?php echo __('상품수령 시 결제')?>)
<?php }?>
<?php }else{?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryPrice'])===true){?>
                                            <?php echo __('무료배송')?>

<?php }else{?>
                                            <?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_V3["goodsDeliveryMethodFlText"])===false){?>
                                            <br />
                                            (<?php echo $TPL_V3["goodsDeliveryMethodFlText"]?>)
<?php }?>
                                        </td>
<?php }?>
<?php }?>
                                    </tr>
<?php }}?>
<?php }}?>
<?php }}?>

<?php if(empty($TPL_VAR["cartCnt"])===true){?>
                                    <tr>
                                        <td colspan="6">
                                            <p class="no_data"><?php echo __('장바구니에 담겨있는 상품이 없습니다.')?></p>
                                        </td>
                                    </tr>
<?php }?>

                                    </tbody>
                                </table>
                                <!-- 장바구니 상품리스트 끝 -->
                            </div>

                        </div>
                        <!-- //cart_cont_list -->

                        <div class="btn_left_box">
                            <a href="./cart.php" class="shop_go_link"><i class="xi-angle-left"></i><em><?php echo __('장바구니 가기')?></em></a>
                        </div>

                        <div class="price_sum">
                            <div class="price_sum_cont">
                                <div class="price_sum_list">
                                    <dl>
                                        <dt><?php echo __('총 %s 개의 상품금액 %s','<strong>'.$TPL_VAR["cartCnt"].'</strong>','')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalGoodsPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                    </dl>
<?php if($TPL_VAR["totalGoodsDcPrice"]> 0){?>
                                    <span><i class="xi-minus"></i></span>
                                    <dl>
                                        <dt><?php echo __('상품할인')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                    </dl>
<?php }?>
<?php if($TPL_VAR["totalSumMemberDcPrice"]> 0){?>
                                    <span><i class="xi-minus"></i></span>
                                    <dl>
                                        <dt><?php echo __('회원할인')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalSumMemberDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                    </dl>
<?php }?>
<?php if($TPL_VAR["totalCouponGoodsDcPrice"]> 0){?>
                                    <span><i class="xi-minus"></i></span>
                                    <dl>
                                        <dt><?php echo __('쿠폰할인')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsDcPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
                                    </dl>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                    <span><i class="xi-plus"></i></span>
                                    <dl>
                                        <dt><?php echo __('배송비')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalDeliveryCharge"])?></strong><?php echo gd_global_currency_string()?></dd>
                                    </dl>
<?php }?>
                                    <span class="equal"><i class="xi-drag-handle"></i></span>
                                    <dl class="price_total">
                                        <dt><?php echo __('합계')?></dt>
                                        <dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalSettlePrice"])?></strong><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            <div class="add_currency"><?php echo gd_global_add_currency_display($TPL_VAR["totalSettlePrice"])?></div>
<?php }?>
                                        </dd>
                                    </dl>
                                </div>
<?php if(gd_is_login()===true&&$TPL_VAR["mileage"]['useFl']=='y'){?>
                                <em class="tobe_mileage js_mileage"><?php echo __('적립예정')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <span><?php echo gd_global_money_format($TPL_VAR["totalMileage"])?></span> <?php echo $TPL_VAR["mileage"]["unit"]?></em>
<?php }?>
<?php if($TPL_VAR["totalMemberBankDcPrice"]> 0){?>
                                <span>(<?php echo __('무통장결제 추가 할인')?> : <span id="totalMemberBankDcPrice" style="color:#e91818;">-<?php echo gd_global_currency_display($TPL_VAR["totalMemberBankDcPrice"])?></span> )</span>
<?php }?>
                            </div>
                            <!-- //price_sum_cont -->
                        </div>
                        <!-- //price_sum -->

<?php if($TPL_VAR["giftConf"]["giftFl"]=='y'&&count($TPL_VAR["giftInfo"])){?>
                        <div class="order_freebie">
                            <div class="order_zone_tit">
                                <h4><?php echo __('사은품 선택')?><span><?php echo __('함께 받으실 사은품을 선택해주세요.')?></span></h4>
                            </div>
                            <div class="order_freebie_list">
<?php if($TPL_giftInfo_1){foreach($TPL_VAR["giftInfo"] as $TPL_K1=>$TPL_V1){?>
                                <dl>
<?php if((is_array($TPL_R2=$TPL_V1["gift"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if($TPL_V2["total"]> 0){?>
                                    <dt><?php echo $TPL_V1["title"]?><span>(<strong><?php if($TPL_V2["selectCnt"]== 0){?><?php echo $TPL_V1["total"]?><?php }else{?>0<?php }?></strong>/<?php if($TPL_V2["selectCnt"]== 0){?><?php echo $TPL_V1["total"]?><?php }else{?><?php echo $TPL_V2["selectCnt"]?><?php }?>)</span></dt>
                                    <dd>
                                        <div class="form_element">
                                            <ul>
<?php if((is_array($TPL_R3=$TPL_V2["multiGiftNo"])&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
                                                <li>
                                                    <input type="hidden" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][goodsNo]" value="<?php echo $TPL_V1["goodsNo"]?>" />
                                                    <input type="hidden" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][scmNo]" value="<?php echo $TPL_V3["scmNo"]?>" />
                                                    <input type="hidden" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][selectCnt]" value="<?php echo $TPL_V2["selectCnt"]?>" class="gift_select_cnt" />
                                                    <input type="hidden" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][giveCnt]" value="<?php echo $TPL_V2["giveCnt"]?>" />
                                                    <input type="hidden" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][minusStockFl]" value="<?php echo $TPL_V3["stockFl"]?>" />
                                                    <input type="checkbox" id="line<?php echo $TPL_K1?>_gift<?php echo $TPL_I3?>" name="gift[<?php echo $TPL_K1?>][<?php echo $TPL_I3?>][giftNo]" value="<?php echo $TPL_V3["giftNo"]?>" <?php if($TPL_V2["selectCnt"]== 0){?> checked="checked" readonly="readonly"<?php }?> />
                                                    <label for="line<?php echo $TPL_K1?>_gift<?php echo $TPL_I3?>" class="check_s <?php if($TPL_V2["selectCnt"]== 0){?> on<?php }?>">
                                                        <b><?php echo $TPL_V3["imageUrl"]?></b>
                                                        <em><?php echo $TPL_V3["giftNm"]?> <span>(<?php echo __('%s개',$TPL_V2["giveCnt"])?>)</span></em>
                                                        <span class="icon_freebie_check"><!-- 선택한경우 활성 --></span>
                                                    </label>
                                                </li>
<?php }}?>
                                            </ul>
                                        </div>
                                    </dd>
<?php }?>
<?php }}?>
                                </dl>
<?php }}?>
                            </div>
                            <!-- //order_freebie_list -->
                        </div>
                        <!-- //order_freebie -->
<?php }?>

                        <div class="order_view_info">
<?php if(gd_is_login()===false){?>
<?php if($TPL_VAR["hasScmGoods"]===false){?>
                            <div class="join_agreement_box" style="margin-top:50px;">
                                <div class="form_element">
                                    <input type="checkbox" id="allAgree">
                                    <label class="check" for="allAgree"> <h3 style="margin-left:10px;"><?php echo __('%s 이용약관 및 비회원 주문에 대한 개인정보 수집 이용 동의를 확인하고 전체 동의합니다.',$TPL_VAR["serviceInfo"]['mallNm'])?></h3></label>
                                </div>
                            </div>
<?php }else{?>
                            <div class="join_agreement_box" style="margin-top:50px;">
                                <div class="form_element">
                                    <input type="checkbox" id="allAgreeScm">
                                    <label class="check" for="allAgreeScm"> <h3 style="margin-left:10px;"><?php echo __('%s 이용약관 및 비회원 주문에 대한 개인정보 수집 이용 동의, 상품 공급사 개인정보 제공 동의를 확인하고 전체 동의합니다.',$TPL_VAR["serviceInfo"]['mallNm'])?></h3></label>
                                </div>
                            </div>
<?php }?>

                            <div class="order_agree" style="margin-top:10px;">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('비회원 주문에 대한 개인정보 수집 이용 동의')?></h4>
                                </div>
                                <div class="order_agree_cont">
                                    <div class="join_agreement_box">
                                        <div class="agreement_box">
                                            <?php echo nl2br($TPL_VAR["privateGuestOffer"]['content'])?>

                                        </div>
                                        <!-- //agreement_box -->
                                        <div class="form_element">
                                            <input type="checkbox" id="termAgree_guest" name="termAgreeGuest" class="require">
                                            <label for="termAgree_guest" class="check_s"><strong>(<?php echo __('필수')?>)</strong> <?php echo __('비회원 개인정보 수집 이용에 대한 내용을 확인 하였으며 이에 동의 합니다.')?></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- //order_agree_cont -->
                            </div>
                            <!-- //order_agree -->
                            <div class="order_agree">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('이용약관 동의')?></h4>
                                </div>
                                <div class="order_agree_cont">
                                    <div class="join_agreement_box">
                                        <div class="agreement_box">
                                            <?php echo nl2br($TPL_VAR["agreementInfo"]['content'])?>

                                        </div>
                                        <!-- //agreement_box -->
                                        <div class="form_element">
                                            <input type="checkbox" id="termAgree_agreement" name="termAgreeAgreement" class="require">
                                            <label for="termAgree_agreement" class="check_s"><strong>(<?php echo __('필수')?>)</strong> <?php echo __('이용약관에 대한 내용을 확인 하였으며 이에 동의 합니다.')?></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- //order_agree_cont -->
                            </div>
                            <!-- //order_agree -->
<?php }?>

<?php if($TPL_VAR["hasScmGoods"]){?>
                            <div class="order_agree">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('상품 공급사 개인정보 제공 동의')?></h4>
                                </div>
                                <div class="order_agree_cont">
                                    <div class="join_agreement_box">
                                        <div class="agreement_box">
                                            <?php echo nl2br($TPL_VAR["privateProvider"]['content'])?>

                                        </div>
                                        <!-- //agreement_box -->
                                        <div class="form_element">
                                            <input type="checkbox" id="termAgree_privateProvider" name="termAgreePrivateProvider" class="require">
                                            <label for="termAgree_privateProvider" class="check_s"><strong>(<?php echo __('필수')?>)</strong> <?php echo __('상품 공급사 개인정보 제공 동의에 대한 내용을 확인 하였으며 이에 동의 합니다.')?></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- //order_agree_cont -->
                            </div>
                            <!-- //order_agree -->
<?php }?>

                            <div class="order_info">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('주문자 정보')?></h4>
                                </div>

                                <div class="order_table_type">
                                    <table class="table_left table no_header">
                                        <colgroup>
                                            <col style="width:15%;">
                                            <col style="width:85%;">
                                        </colgroup>
                                        <tbody>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('주문하시는 분')?></span></th>
                                            <td><input type="text" name="orderName" value="<?php echo $TPL_VAR["orderName"]?>" data-pattern="<?php if($TPL_VAR["gGlobal"]['isFront']){?>gdMemberNmGlobal<?php }else{?>gdEngKor<?php }?>" maxlength="20" class="text"/></td>
                                        </tr>
<?php if(isset($TPL_VAR["gMemberInfo"]['address'])===true&&empty($TPL_VAR["gMemberInfo"]['address'])===false){?>
                                        <tr>
                                            <th scope="row"><?php echo __('주소')?></th>
                                            <td><?php if(empty($TPL_VAR["gMemberInfo"]['zonecode'])===false){?>
                                                [<?php echo gd_isset($TPL_VAR["gMemberInfo"]['zonecode'])?>]
<?php }else{?>
<?php if(empty($TPL_VAR["gMemberInfo"]['zipcode'])===false){?>
                                                [<?php echo gd_isset($TPL_VAR["gMemberInfo"]['zipcode'])?>]
<?php }?>
<?php }?>
                                                <?php echo gd_isset($TPL_VAR["gMemberInfo"]['address'])?> <?php echo gd_isset($TPL_VAR["gMemberInfo"]['addressSub'])?></td>
                                        </tr>
<?php }?>
                                        <tr style="display:none;">
                                            <th scope="row"><?php echo __('전화번호')?></th>
                                            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo gd_select_box('orderPhonePrefixCode','orderPhonePrefixCode',$TPL_VAR["orderCountryPhone"],null,$TPL_VAR["gMemberInfo"]["phoneCountryCode"],__('국가코드'),null,'chosen-select')?>

<?php }?>
                                                <input type="text" id="phoneNum" name="orderPhone" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['cellPhone'])?>" maxlength="20" class="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('휴대폰 번호')?></span></th>
                                            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo gd_select_box('orderCellPhonePrefixCode','orderCellPhonePrefixCode',$TPL_VAR["orderCountryPhone"],null,$TPL_VAR["gMemberInfo"]['cellPhoneCountryCode'],__('국가코드'),null,'tune select-small')?>

<?php }?>
                                                <input type="text" id="mobileNum" name="orderCellPhone" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['cellPhone'])?>" maxlength="20" class="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('이메일')?></span></th>
                                            <td class="member_email">
                                                <input type="text" name="orderEmail" value="<?php echo gd_isset($TPL_VAR["gMemberInfo"]['email'])?>" class="text" />

                                                <select id="emailDomain" class="chosen-select" style="width:150px;">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
                                                    <option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- //order_info -->

                            <div class="delivery_info">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('배송정보')?></h4>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
                                    <span class="form_element" style="float:right; margin-bottom:5px;">
                                        <input type="checkbox" id="multiShippingFl" name="multiShippingFl" value="y">
                                        <label class="check-s" for="multiShippingFl"><?php echo __('복수 배송지 이용')?></label>
                                    </span>
<?php }?>
                                </div>

                                <div class="order_table_type shipping_info">
                                    <table class="table_left shipping_info_table table no_header">
                                        <colgroup>
                                            <col style="width:15%;">
                                            <col style="width:85%;">
                                        </colgroup>
                                        <tbody>
                                        <tr>
                                            <th scope="row"><?php echo __('배송지 확인')?></th>
                                            <td>
                                                <div class="form_element">
                                                    <ul class="inline">
<?php if(gd_is_login()===true){?>
                                                        <li>
                                                            <input type="radio" name="shipping" id="shippingBasic">
                                                            <label for="shippingBasic" class="choice_s"><?php echo __('기본 배송지')?></label>
                                                        </li>
                                                        <li class="separate"></li>
                                                        <li>
                                                            <input type="radio" name="shipping" id="shippingRecently">
                                                            <label for="shippingRecently" class="choice_s"><?php echo __('최근 배송지')?></label>
                                                        </li>
                                                        <li class="separate"></li>
<?php }?>
                                                        <li>
                                                            <input type="radio" name="shipping" id="shippingNew">
                                                            <label for="shippingNew" class="choice_s"><?php echo __('직접 입력')?></label>
                                                        </li>
                                                        <li class="separate"></li>
                                                        <li>
                                                            <input type="radio" name="shipping" id="shippingSameCheck">
                                                            <label for="shippingSameCheck" class="choice_s"><?php echo __('주문자정보와 동일')?></label>
                                                        </li>
                                                    </ul>
<?php if(gd_is_login()===true){?>
                                                    <span class="btn_gray_list" style="display:none;"><a href="#myShippingListLayer" class="btn_gray_small btn_open_layer js_shipping"><span><?php echo __('배송지 관리')?></span></a></span>
<?php }?>
                                                </div>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
                                                <span class="btn_gray_list"><a class="btn_gray_small shipping_add_btn dn" style="float:right; cursor:pointer;"><span>+ <?php echo __('추가')?></span></a></span>
<?php }?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('받으실분')?></span></th>
                                            <td><input type="text" name="receiverName" data-pattern="<?php if($TPL_VAR["gGlobal"]['isFront']){?>gdMemberNmGlobal<?php }else{?>gdEngKor<?php }?>" maxlength="20" class="text"/></td>
                                        </tr>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('국가')?></span></th>
                                            <td><?php echo gd_select_box('receiverCountryCode','receiverCountryCode',$TPL_VAR["countryAddress"],null,$TPL_VAR["recommendCountryCode"],__('=선택해주세요='),null,'chosen-select')?></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('도시')?></span></th>
                                            <td><input type="text" name="receiverCity" maxlength="20" class="text"/></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('주/지방/지역')?></span></th>
                                            <td><input type="text" name="receiverState" maxlength="20" class="text"/></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('주소1')?></span></th>
                                            <td><input type="text" name="receiverAddress" class="text"/></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('주소2')?></span></th>
                                            <td><input type="text" name="receiverAddressSub" class="text"/></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('우편번호')?></span></th>
                                            <td>
                                                <input type="text" name="receiverZonecode" class="text"/>
                                                <input type="hidden" name="receiverZipcode"/>
                                            </td>
                                        </tr>
<?php }else{?>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('받으실 곳')?></span></th>
                                            <td class="member_address">
                                                <div class="address_postcode">
                                                    <input type="text" name="receiverZonecode" readonly="readonly" class="text" />
                                                    <input type="hidden" name="receiverZipcode"/>
                                                    <span id="receiverZipcodeText" class="old_post_code"></span>
                                                    <button type="button" class="btn_post_search" onclick="gd_postcode_search('receiverZonecode', 'receiverAddress', 'receiverZipcode');"><?php echo __('우편번호검색')?></button>
                                                </div>
                                                <div class="address_input">
                                                    <input type="text" name="receiverAddress" readonly="readonly" class="text" style="width:500px;"/>
                                                    <input type="text" name="receiverAddressSub" class="text" style="width:300px;" />
                                                </div>
                                            </td>
                                        </tr>
<?php }?>
                                        <tr style="display:none;">
                                            <th scope="row"><?php echo __('전화번호')?></th>
                                            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo gd_select_box('receiverPhonePrefixCode','receiverPhonePrefixCode',$TPL_VAR["countryPhone"],null,null,__('국가코드'),null,'chosen-select')?>

<?php }?>
                                                <input type="text" id="receiverPhone" name="receiverPhone" class="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><span class="important"><?php echo __('휴대폰 번호')?></span></th>
                                            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo gd_select_box('receiverCellPhonePrefixCode','receiverCellPhonePrefixCode',$TPL_VAR["countryPhone"],null,null,__('국가코드'),null,'chosen-select')?>

<?php }?>
                                                <input type="text" id="receiverCellPhone" name="receiverCellPhone" class="text"/>
<?php if($TPL_VAR["gGlobal"]["isFront"]==false&&$TPL_VAR["useSafeNumberFl"]=='y'){?>
                                                <span class="form_element td_chk">
                                                    <input type="checkbox" id="receiverUseSafeNumberFl" name="receiverUseSafeNumberFl" value="w" checked="checked">
                                                    <label for="receiverUseSafeNumberFl" class="check_s on"><em><?php echo __('안심번호 사용')?></em></label>
                                                </span>
                                                <div><?php echo __('안심번호 사용 체크 시 1회 성으로 가상의 번호인 안심번호(050*-0000-0000)를 부여합니다. 안심번호는 일정 기간이 지나면 자동으로 해지됩니다.')?></div>
<?php }?>
                                            </td>
                                        </tr>
                                        <tr style="display:none;">
                                            <th scope="row"><?php echo __('남기실 말씀')?></th>
                                            <td class="td_last_say"><input type="text" name="orderMemo" class="text" style="width:100%;"/></td>
                                        </tr>
<?php if(gd_is_login()===true&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                                        <tr id="memberinfoApplyTr" style="display:none;">
                                            <th scope="row"><?php echo __('회원정보 반영')?></th>
                                            <td>
                                                <div class="form_element">
                                                    <div id="memberinfoApplyTr1" class="member_info_delivery">
                                                        <input type="checkbox" id="reflectApplyDelivery" name="reflectApplyDelivery" value="y" >
                                                        <label for="reflectApplyDelivery" class="check_s"><em><?php echo __('나의 배송지에 추가합니다.')?></em></label>
                                                    </div>
                                                    <div id="memberinfoApplyTr2" class="member_info_apply">
                                                        <input type="checkbox" id="reflectApplyMember" name="reflectApplyMember" value="y">
                                                        <label for="reflectApplyMember" class="check_s"><?php echo __('위 내용을 회원정보에 반영합니다.')?> <span>(<?php echo __('주소/전화번호/휴대폰번호')?>)</span></label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
                                        <tr class="select_goods_tr dn">
                                            <th scope="row"><?php echo __('배송 상품')?></th>
                                            <td>
                                                <input type="hidden" name="selectGoods[0]" value="">
                                                <input type="hidden" name="multiDelivery[0]" value="0">
                                                <span class="btn_gray_list"><a href="#cartSelectGoods" class="btn_gray_small shipping_goods_select btn_open_layer" style="margin-bottom:5px;"><span>+ <?php echo __('상품 선택')?></span></a></span>
                                                <div class="table1 type1 select_goods order_goods dn">
                                                    <table class="check"></table>
                                                </div>
                                            </td>
                                        </tr>
<?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- //delivery_info -->

<?php if($TPL_VAR["addFieldInfo"]["addFieldConf"]=='y'){?>
                            <input type="hidden" name="addFieldConf" value="<?php echo $TPL_VAR["addFieldInfo"]["addFieldConf"]?>" />
                            <div class="addition_info">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('추가 정보')?></h4>
                                </div>

                                <div class="order_table_type">
                                    <table class="table_left table no_header">
                                        <colgroup>
                                            <col style="width:15%;">
                                            <col style="width:85%;">
                                        </colgroup>
                                        <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["addFieldInfo"]["data"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                                        <tr>
                                            <th><?php echo gd_isset($TPL_V1["orderAddFieldName"])?></th>
                                        </tr>
                                        <tr>
                                            <td><?php echo gd_isset($TPL_V1["orderAddFieldHtml"])?></td>
                                        </tr>
<?php }}?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
<?php }?>
                            <!-- //addition_info -->

                            <div class="payment_info">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('결제정보')?></h4>
                                </div>

                                <div class="order_table_type">
                                    <table class="table_left table no_header">
                                        <colgroup>
                                            <col style="width:15%;">
                                            <col style="width:85%;">
                                        </colgroup>
                                        <tbody>
                                        <tr>
                                            <th scope="row"><?php echo __('상품 합계 금액')?></th>
                                            <td>
                                                <strong id="totalGoodsPrice" class="order_payment_sum"><?php echo gd_global_currency_display($TPL_VAR["totalGoodsPrice"])?></strong>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <span class="add_currency"><?php echo gd_global_add_currency_display($TPL_VAR["totalGoodsPrice"])?></span>
<?php }?>
                                            </td>
                                        </tr>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                        <tr>
                                            <th scope="row"><?php echo __('총 무게')?></th>
                                            <td>
                                                <p>
                                                    <strong><?php echo number_format($TPL_VAR["totalDeliveryWeight"]["total"], 2)?>kg</strong>
                                                    <span>( <?php echo __('상품무게 %s kg + 박스무게 %s kg',number_format($TPL_VAR["totalDeliveryWeight"]["goods"], 2),number_format($TPL_VAR["totalDeliveryWeight"]["box"], 2))?> )</span>
                                                </p>
                                            </td>
                                        </tr>
<?php }?>
                                        <tr>
                                            <th scope="row"><?php echo __('배송비')?></th>
                                            <td>
                                                <?php echo gd_global_currency_symbol()?><span id="totalDeliveryCharge"><?php echo gd_global_money_format($TPL_VAR["totalDeliveryCharge"])?></span><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                                <?php echo gd_global_add_currency_symbol()?><span id="totalDeliveryChargeAdd" class="add_currency"><?php echo gd_global_add_money_format($TPL_VAR["totalDeliveryCharge"])?></span><?php echo gd_global_add_currency_string()?>

<?php }?>
                                                <span class="multi_shipping_text"></span>
                                            </td>
                                        </tr>
                                        <tr id="rowDeliveryInsuranceFee" class="dn">
                                            <th scope="row"><?php echo __('해외배송 보험료')?></th>
                                            <td>
                                                <?php echo gd_global_currency_symbol()?><span id="deliveryInsuranceFee">0</span><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                                <?php echo gd_global_add_currency_symbol()?><span id="deliveryInsuranceFeeAdd" class="add_currency">0</span><?php echo gd_global_add_currency_string()?>

<?php }?>
                                                <input type="hidden" name="deliveryInsuranceFee" value="0">
                                            </td>
                                        </tr>
                                        <tr id="rowDeliverAreaCharge" class="dn">
                                            <th scope="row"><?php echo __('지역별 배송비')?></th>
                                            <td>
                                                <?php echo gd_global_currency_symbol()?><span id="deliveryAreaCharge">0</span><?php echo gd_global_currency_string()?>

                                                <input type="hidden" name="totalDeliveryCharge" value="<?php echo $TPL_VAR["totalDeliveryCharge"]?>">
                                                <input type="hidden" name="deliveryAreaCharge" value="0">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php echo __('할인 및 적립')?></th>
                                            <td>
                                                <ul class="order_benefit_list">
                                                    <li class="order_benefit_sale">
                                                        <em id="saleDefault">
                                                            <?php echo __('할인')?> : <strong>(-) <?php echo gd_global_currency_symbol()?><b class="total-member-dc-price"><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"]+$TPL_VAR["totalSumMemberDcPrice"]+$TPL_VAR["totalCouponGoodsDcPrice"])?></b><?php echo gd_global_currency_string()?></strong>
                                                            <span>(
                                                            <?php echo __('상품')?> <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"])?><?php echo gd_global_currency_string()?>

<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                                            , <?php echo __('회원')?> <?php echo gd_global_currency_symbol()?><span class="member-dc-price"><?php echo gd_global_money_format($TPL_VAR["totalSumMemberDcPrice"])?><?php echo gd_global_currency_string()?></span>
                                                            , <?php echo __('쿠폰')?> <?php echo gd_global_currency_symbol()?><span class="goods-coupon-dc-price"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsDcPrice"])?></span><?php echo gd_global_currency_string()?>

<?php }?>
                                                            )</span>
<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
<?php if(($TPL_VAR["totalGoodsDcPrice"]+$TPL_VAR["totalSumMemberDcPrice"]+$TPL_VAR["totalCouponGoodsDcPrice"])!= 0){?><?php echo gd_global_add_currency_symbol()?><?php }?><span class="total-member-dc-price-add" style="color: #717171;"><?php echo gd_global_add_money_format($TPL_VAR["totalGoodsDcPrice"]+$TPL_VAR["totalSumMemberDcPrice"]+$TPL_VAR["totalCouponGoodsDcPrice"])?></span><?php echo gd_global_add_currency_string()?>

<?php }?>
                                                        </em>
                                                        <em id="saleWithoutMember" class="dn">
                                                            <?php echo __('할인')?> : <strong>(-) <?php echo gd_global_currency_symbol()?><b class="total-member-dc-price-without-member"><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"]+$TPL_VAR["totalCouponGoodsDcPrice"])?></b><?php echo gd_global_currency_string()?></strong>
                                                            <span>(
                                                            <?php echo __('상품')?> <?php echo gd_global_currency_display($TPL_VAR["totalGoodsDcPrice"])?>

<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                                            , <?php echo __('회원')?> <?php echo gd_global_currency_display( 0)?>

                                                            , <?php echo __('쿠폰')?> <span class="goods-coupon-dc-price-without-member"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsDcPrice"])?></span><?php echo gd_global_currency_string()?></span>
<?php }?>
                                                            )</span>
                                                        </em>
                                                    </li>
<?php if(gd_is_login()===true){?>
<?php if($TPL_VAR["deliveryFree"]=='y'&&$TPL_VAR["totalDeliveryCharge"]> 0){?>
                                                    <li class="sale">
                                                        <?php echo __('배송비 할인')?> : <strong>(-) <?php echo gd_global_currency_symbol()?><b class="delivery-free">0</b><?php echo gd_global_currency_string()?></strong> <span>(<?php echo __('회원등급 배송할인')?>)</span>
                                                    </li>
<?php }?>
<?php if($TPL_VAR["mileage"]["useFl"]==='y'){?>
                                                    <li class="order_benefit_mileage js_mileage">
                                                        <em id="mileageDefault">
                                                            <?php echo __('적립')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <strong>(+) <b class="total-member-mileage"><?php echo gd_global_money_format($TPL_VAR["totalMileage"])?></b><?php echo $TPL_VAR["mileage"]["unit"]?></strong>
                                                            <span>
                                                                (
                                                                <?php echo __('상품')?> <span class="goods-mileage"><?php echo gd_global_money_format($TPL_VAR["totalGoodsMileage"])?></span><?php echo $TPL_VAR["mileage"]["unit"]?>,
                                                                <?php echo __('회원')?> <span class="member-mileage"><?php echo gd_global_money_format($TPL_VAR["totalMemberMileage"])?></span><?php echo $TPL_VAR["mileage"]["unit"]?>,
                                                                <?php echo __('쿠폰')?> <span class="goods-coupon-add-mileage"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsMileage"])?></span><?php echo $TPL_VAR["mileage"]["unit"]?>

                                                                )
                                                            </span>
                                                        </em>
                                                        <em id="mileageWithoutMember" class="js_mileage dn">
                                                            <?php echo __('적립')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <strong>(+) <b class="total-member-mileage-without-member"><?php echo gd_global_money_format($TPL_VAR["totalGoodsMileage"]+$TPL_VAR["totalCouponGoodsMileage"])?></b><?php echo $TPL_VAR["mileage"]["unit"]?></strong>
                                                            <span>
                                                                (
                                                                <?php echo __('상품')?> <?php echo gd_global_money_format($TPL_VAR["totalGoodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?>,
                                                                <?php echo __('회원')?> <?php echo gd_global_money_format( 0)?><?php echo $TPL_VAR["mileage"]["unit"]?>,
                                                                <?php echo __('쿠폰')?> <span class="goods-coupon-add-mileage-without-member"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsMileage"])?></span><?php echo $TPL_VAR["mileage"]["unit"]?>

                                                                )
                                                            </span>
                                                        </em>
                                                    </li>
<?php }?>
                                                </ul>
                                            </td>
                                        </tr>
<?php }else{?>
<?php if($TPL_VAR["totalGoodsDcPrice"]> 0){?>
                                        <tr>
                                            <th scope="row"><?php echo __('상품 할인')?></th>
                                            <td>
                                                <?php echo gd_global_currency_display($TPL_VAR["totalGoodsDcPrice"])?>

                                            </td>
                                        </tr>
<?php }?>
<?php }?>
<?php if(gd_is_login()===true){?>
<?php if($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                                        <tr>
                                            <th scope="row"><?php echo __('쿠폰 사용')?></th>
                                            <td>
                                                <input type="hidden" name="couponApplyOrderNo" value="" />
                                                <input type="hidden" name="totalCouponOrderDcPrice" value="" />
                                                <input type="hidden" name="totalCouponOrderPrice" value="" />
                                                <input type="hidden" name="totalCouponOrderMileage" value="" />
                                                <input type="hidden" name="totalCouponDeliveryDcPrice" value="" />
                                                <input type="hidden" name="totalCouponDeliveryPrice" value="" />
                                                <ul class="order_benefit_list order_coupon_benefits  dn">
                                                    <li class="order_benefit_sale">
                                                        <em>
                                                            <?php echo __('주문할인')?> : <strong>(-) <?php echo gd_global_currency_symbol()?><b id="useDisplayCouponDcPrice"><?php echo gd_global_money_format( 0)?></b><?php echo gd_global_currency_string()?></strong>
                                                        </em>
                                                    </li>
                                                    <li class="order_benefit_sale">
                                                        <em>
                                                            <?php echo __('배송비할인')?> : <strong>(-) <?php echo gd_global_currency_symbol()?><b id="useDisplayCouponDelivery"><?php echo gd_global_money_format( 0)?></b><?php echo gd_global_currency_string()?></strong>
                                                        </em>
                                                    </li>
                                                    <li class="order_benefit_mileage js_mileage">
                                                        <em>
                                                            <?php echo __('적립')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <strong>(+) <b id="useDisplayCouponMileage"><?php echo gd_global_money_format( 0)?></b><?php echo $TPL_VAR["mileage"]["unit"]?></strong>
                                                        </em>
                                                    </li>
                                                </ul>
                                                <span class="btn_gray_list">
                                                    <button type="button" href="#couponOrderApplyLayer" class="btn_gray_mid btn_open_layer btn" ><span><?php echo __('쿠폰 조회 및 적용')?></span></button>
                                                </span>
                                            </td>
                                        </tr>
<?php }?>
<?php if($TPL_VAR["mileageUse"]["payUsableFl"]==='y'){?>
                                        <tr>
                                            <th scope="row"><?php echo $TPL_VAR["mileage"]["name"]?> <?php echo __('사용')?></th>
                                            <td>
                                                <div class="order_money_use">
                                                    <b><input type="text" name="useMileage" onblur="gd_mileage_use_check('y', 'y', 'y', 'y');" class="text" /> <?php echo $TPL_VAR["mileage"]["unit"]?></b>
                                                    <div class="form_element">
                                                        <input type="checkbox" id="useMileageAll" onclick="gd_mileage_use_all();">
                                                        <label for="useMileageAll" class="check_s"><?php echo __('전액 사용하기')?></label>
                                                        <span class="money_use_sum">(<?php echo __('보유')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <?php echo gd_global_money_format($TPL_VAR["gMemberInfo"]["mileage"])?> <?php echo $TPL_VAR["mileage"]["unit"]?>)</span>
                                                    </div>
                                                    <em class="money_use_txt js-mileageInfoArea"></em>
                                                </div>
                                            </td>
                                        </tr>
<?php }?>
<?php if($TPL_VAR["depositUse"]["payUsableFl"]==='y'){?>
                                        <tr>
                                            <th scope="row"><?php echo $TPL_VAR["depositUse"]["name"]?> <?php echo __('사용')?></th>
                                            <td>
                                                <div class="order_money_use">
                                                    <b><input type="text" name="useDeposit" onblur="gd_deposit_use_check();" class="text"/> <?php echo $TPL_VAR["depositUse"]["unit"]?></b>
                                                    <div class="form_element">
                                                        <input type="checkbox" id="useDepositAll" onclick="deposit_use_all();">
                                                        <label for="useDepositAll" class="check_s"><?php echo __('전액 사용하기')?></label>
                                                        <span class="money_use_sum">(<?php echo __('보유')?> <?php echo $TPL_VAR["depositUse"]["name"]?> : <?php echo gd_global_money_format($TPL_VAR["gMemberInfo"]["deposit"])?> <?php echo $TPL_VAR["depositUse"]["unit"]?>)</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
<?php }?>
<?php }?>
                                        <tr class="total_price">
                                            <th scope="row"><?php echo __('최종 결제 금액')?></th>
                                            <td class="text_orange">
                                                <input type="hidden" name="settlePrice" value="<?php echo gd_money_format($TPL_VAR["totalSettlePrice"],false)?>">
                                                <input type="hidden" name="overseasSettlePrice" value="0" />
                                                <input type="hidden" name="overseasSettleCurrency" value="KRW" />
                                                <?php echo gd_global_currency_symbol()?><strong id="totalSettlePrice" class="order_payment_sum"><?php echo gd_global_money_format($TPL_VAR["totalSettlePrice"])?></strong><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                                <span class="add_currency"><?php echo gd_global_add_currency_symbol()?><strong id="totalAddSettlePrice"><?php echo gd_global_add_money_format($TPL_VAR["totalSettlePrice"])?></strong><?php echo gd_global_add_currency_string()?></span>
<?php }?>

<?php if($TPL_VAR["totalMemberBankDcPrice"]> 0){?>
                                                <br /><span style="font-size:12px;" id="totalMemberBankDcPriceViewTop">(<?php echo __('일반결제의 무통장 입금 추가할인이 적용된 금액이며, 다른 결제수단 이용시')?> <strong style="font-size:12px;"><?php echo gd_global_currency_display($TPL_VAR["totalMemberBankDcPrice"])?> <?php echo __('이 추가된 금액으로 최종 결제')?></strong> <?php echo __('됩니다.')?>)</span>
<?php }?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- //payment_info -->

                            <div class="payment_progress">
                                <div class="order_zone_tit">
                                    <h4><?php echo __('결제수단 선택 / 결제')?></h4>
<?php if(isset($TPL_VAR["settle"]["escrow"])){?>
                                    <p class="js_pay_content"><?php echo dataEggBanner('order')?></p>
<?php }?>
                                </div>

                                <div class="payment_progress_list">
                                    <div class="js_pay_content">
                                        <!-- N : 페이코결제 시작 -->
<?php if(isset($TPL_VAR["payco"])){?>
                                        <div id="settlekind_payco" class="payco_payment">
                                            <dl>
                                                <dt>
                                                    <?php echo $TPL_VAR["payco"]?>

                                                </dt>
                                                <dd>
                                                    <div class="form_element">
                                                        <ul>
<?php if((is_array($TPL_R1=$TPL_VAR["settle"]["payco"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["useFl"]=='y'){?>
                                                            <li>
                                                                <input type="radio" id="settleKind_payco_<?php echo $TPL_K1?>" name="settleKind" value="<?php echo $TPL_K1?>" onclick="gd_payco_toggle('<?php echo $TPL_K1?>');"/>
                                                                <label for="settleKind_payco_<?php echo $TPL_K1?>" class="choice_payco">
                                                                    <span><img src="/data/skin/front/aileenwedding/img/order/payco_<?php echo $TPL_K1?>_icon.png" alt="<?php echo $TPL_V1["name"]?>"></span>
                                                                </label>
                                                            </li>
<?php }?>
<?php }}?>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>

                                        </div>
                                        <!-- //payco_payment -->
<?php }?>
                                        <!-- N : 페이코결제 끝 -->

<?php if($TPL_VAR["settle"]["general"]){?>
                                        <!-- N : 일반결제 시작 -->
                                        <div id="settlekind_general" class="general_payment">
                                            <dl>
                                                <dt><?php echo __('일반결제')?></dt>
                                                <dd>
                                                    <div class="form_element">
                                                        <ul class="payment_progress_select inline">
<?php if((is_array($TPL_R1=$TPL_VAR["settle"]["general"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["useFl"]=='y'){?>
                                                            <li id="settlekindType_<?php echo $TPL_K1?>">
                                                                <input type="radio" id="settleKind_<?php echo $TPL_K1?>" name="settleKind" value="<?php echo $TPL_K1?>"/>
                                                                <label for="settleKind_<?php echo $TPL_K1?>" class="choice_s"><?php echo $TPL_V1["name"]?></label>
                                                            </li>
<?php }?>
<?php }}?>
                                                        </ul>
                                                    </div>

<?php if(isset($TPL_VAR["settle"]["general"]["gb"])){?>
                                                    <!-- N : 무통장입금 -->
                                                    <div id="settlekind_general_gb" class="pay_bankbook_box">
<?php if(isset($TPL_VAR["settle"]["general"]["gb"])){?>
                                                        <em class="pay_bankbook_txt">( <?php echo __('%s 의 경우 입금확인 후부터 배송단계가 진행됩니다.',$TPL_VAR["settle"]["general"]["gb"]["name"])?> )</em>
<?php }?>
                                                        <ul>
                                                            <li>
                                                                <strong><?php echo __('입금자명')?></strong>
                                                                <input type="text" name="bankSender" class="text">
                                                            </li>
                                                            <li>
                                                                <strong><?php echo __('입금은행')?></strong>
                                                                <select name="bankAccount" class="chosen-select" style="width:300px;">
                                                                    <option value=""><?php echo __('선택하세요')?></option>
                                                                    <option value="1234">은행</option>
<?php if($TPL_bank_1){foreach($TPL_VAR["bank"] as $TPL_V1){?>
                                                                    <option value="<?php echo $TPL_V1["sno"]?>"><?php echo $TPL_V1["bankName"]?> <?php echo $TPL_V1["accountNumber"]?> <?php echo $TPL_V1["depositor"]?></option>
<?php }}?>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- //pay_bankbook_box -->
<?php }?>

<?php if(isset($TPL_VAR["settle"]["general"]["pc"])){?>
                                                    <!-- 신용카드 컨텐츠 -->
                                                    <div class="card" id="settlekind_general_pc"></div>
                                                    <!-- //신용카드 컨텐츠 -->
<?php }?>

<?php if(isset($TPL_VAR["settle"]["general"]["pb"])){?>
                                                    <!-- 계좌이체 컨텐츠 -->
                                                    <div class="account-bank" id="settlekind_general_pb"></div>
                                                    <!-- //계좌이체 컨텐츠 -->
<?php }?>

<?php if(isset($TPL_VAR["settle"]["general"]["pv"])){?>
                                                    <!-- 가상계좌 컨텐츠 -->
                                                    <div class="virtual-bank" id="settlekind_general_pv"></div>
                                                    <!-- //가상계좌 컨텐츠 -->
<?php }?>

<?php if(isset($TPL_VAR["settle"]["general"]["ph"])){?>
                                                    <!-- 휴대폰 컨텐츠 -->
                                                    <div class="cellphone" id="settlekind_general_ph"></div>
                                                    <!-- //휴대폰 컨텐츠 -->
<?php }?>

<?php if(isset($TPL_VAR["settle"]["general"]["pe"])){?>
                                                    <!-- 페이코 포인트 -->
                                                    <div class="payco-point" id="settlekind_general_pe"></div>
                                                    <!-- //페이코 포인트 -->
<?php }?>

                                                </dd>
                                            </dl>
                                        </div>
                                        <!-- //general_payment -->
                                        <!-- N : 일반결제 끝 -->
<?php }?>

<?php if($TPL_VAR["settle"]["escrow"]){?>
                                        <!-- N : 에스크로결제 시작 -->
                                        <div id="settlekind_escrow" class="escrow_payment">
                                            <dl>
                                                <dt><?php echo __('에스크로결제')?></dt>
                                                <dd>
                                                    <div class="form_element">
                                                        <ul class="payment_progress_select inline">
<?php if((is_array($TPL_R1=$TPL_VAR["settle"]["escrow"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["useFl"]=='y'){?>
                                                            <li>
                                                                <input type="radio" name="settleKind" id="settleKind_<?php echo $TPL_K1?>" value="<?php echo $TPL_K1?>">
                                                                <label for="settleKind_<?php echo $TPL_K1?>" class="choice_s"><?php echo $TPL_V1["name"]?></label>
                                                            </li>
<?php }?>
<?php }}?>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <!-- //escrow_payment -->
                                        <!-- N : 에스크로결제 끝 -->
<?php }?>

<?php if($TPL_VAR["settle"]["overseas"]){?>
                                        <!-- N : 해외PG 시작 -->
                                        <div id="settlekind_overseas" class="pg_payment">
                                            <dl>
                                                <dt><?php echo __('해외PG')?></dt>
                                                <dd>
                                                    <div class="form_element">
                                                        <ul class="payment_progress_select">
<?php if((is_array($TPL_R1=$TPL_VAR["settle"]["overseas"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["useFl"]=='y'){?>
                                                            <li>
                                                                <input type="radio" name="settleKind" id="settleKind_<?php echo $TPL_K1?>" value="<?php echo $TPL_K1?>">
                                                                <label for="settleKind_<?php echo $TPL_K1?>" class="choice_s">
                                                                    <img src="/data/commonimg/overseas_<?php echo $TPL_K1?>_icon.png" alt="<?php echo $TPL_V1["name"]?>" title="<?php echo $TPL_V1["name"]?>">
                                                                </label>
                                                            </li>
<?php }?>
<?php }}?>
                                                        </ul>
                                                    </div>
                                                    <!-- N : 해외PG 승인금액 -->
                                                    <div class="pay_pg_box">
<?php if((is_array($TPL_R1=$TPL_VAR["settle"]["overseas"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["useFl"]=='y'){?>
                                                        <ul id="settlekind_overseas_<?php echo $TPL_K1?>" data-settle-currency="<?php echo $TPL_V1["pgCurrency"]?>" data-settle-symbol="<?php echo $TPL_V1["pgSymbol"]?>" data-settle-decimal="<?php echo $TPL_V1["pgDecimal"]?>" data-settle-format="<?php echo $TPL_V1["pgDecimalFormat"]?>">
                                                            <li>
                                                                <strong><?php echo $TPL_V1["name"]?> <?php echo __('승인금액')?></strong>
                                                                <em id="overseasSettelprice_<?php echo $TPL_K1?>">0</em>
                                                            </li>
                                                        </ul>
<?php }?>
<?php }}?>
                                                    </div>
                                                    <!-- //pay_pg_box -->
                                                </dd>
                                            </dl>
                                        </div>
                                        <!-- //pg_payment -->
                                        <!-- N : 해외PG 끝 -->
<?php }?>
                                    </div>
<?php if($TPL_VAR["receipt"]['cashFl']=='y'||$TPL_VAR["receipt"]['taxFl']=='y'){?>
                                    <!-- N : 현금영수증/계산서 발행 시작 -->
                                    <div id="receiptSelect" class="cash_tax_get">
                                        <dl>
                                            <dt><?php echo __('현금영수증/계산서 발행')?></dt>
                                            <dd>
                                                <div class="form_element">
                                                    <ul class="payment_progress_select inline">
                                                        <li id="nonReceiptView">
                                                            <input type="radio" id="receiptNon" name="receiptFl" value="n" onclick="gd_receipt_display();" />
                                                            <label for="receiptNon" class="choice_s on">
                                                                <span class="cash_receipt_non"><?php echo __('신청안함')?></span>
                                                                <span class="cash_receipt_pg"><?php echo __('현금영수증')?> (※ <?php echo __('결제창에서 신청')?>)</span>
                                                            </label>
                                                        </li>
<?php if($TPL_VAR["receipt"]['cashFl']=='y'){?>
                                                        <li id="cashReceiptView">
                                                            <input type="radio" id="receiptCash" name="receiptFl" value="r" onclick="gd_receipt_display();" />
                                                            <label for="receiptCash" class="choice_s"><?php echo __('현금영수증')?></label>
                                                        </li>
<?php }?>
<?php if($TPL_VAR["receipt"]['taxFl']=='y'){?>
                                                        <li id="taxReceiptView">
                                                            <input type="radio" id="receiptTax" name="receiptFl" value="t" onclick="gd_receipt_display();" />
                                                            <label for="receiptTax" class="choice_s"><?php echo __('세금계산서')?></label>
<?php if($TPL_VAR["memberInvoiceInfo"]['tax']['memberTaxInfoFl']=='y'){?>
                                                            <button class="tax_info_init dn btn_reset"><em>초기화</em></button>
<?php }?>
                                                        </li>
<?php }?>
                                                    </ul>
                                                </div>
                                            </dd>
                                        </dl>

<?php if($TPL_VAR["receipt"]['cashFl']=='y'){?>
                                        <!-- N : 현금영수증 시작 -->
                                        <div id="cash_receipt_info" class="cash_receipt_box js_receipt dn">
                                            <div class="form_element">
                                                <ul class="payment_progress_select">
                                                    <input type="hidden" name="cashCertFl" value="c" />
                                                    <li>
                                                        <input type="radio" id="cashCert_d" name="cashUseFl" value="d" onclick="gd_cash_receipt_toggle();" checked="checked" />
                                                        <label class="choice_s" for="cashCert_d"><?php echo __('소득공제용')?></label>
                                                    </li>
                                                    <li>
                                                        <input type="radio" id="cashCert_e" name="cashUseFl" value="e" onclick="gd_cash_receipt_toggle();" />
                                                        <label class="choice_s" for="cashCert_e"><?php echo __('지출증빙용')?></label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cash_receipt_list">
                                                <dl id="certNoHp">
                                                    <dt><?php echo __('휴대폰번호')?></dt>
                                                    <dd><input type="text" name="cashCertNo[c]" class="number_only" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['cash']['cellPhone'])?>" maxlength="11" class="text" /></dd>
                                                </dl>
                                                <dl id="certNoBno">
                                                    <dt><?php echo __('사업자번호')?></dt>
                                                    <dd><input type="text" name="cashCertNo[b]" class="number_only" value="<?php echo str_replace('-','',gd_isset($TPL_VAR["memberInvoiceInfo"]['cash']['cashBusiNo']))?>" maxlength="10" class="text" /></dd>
                                                </dl>
                                            </div>
                                        </div>
                                        <!-- //cash_receipt_box -->
<?php }?>

<?php if($TPL_VAR["receipt"]['taxFl']=='y'){?>
                                        <!-- N : 세금 계산서 -->
                                        <div id="tax_info" class="tax_invoice_box js_receipt dn">
                                            <div class="order_table_type">
                                                <table summary="<?php echo __('세금계산서 입력폼입니다.')?>" class="table_left table left">
                                                    <colgroup>
                                                        <col style="width:15%;">
                                                        <col style="width:35%;">
                                                        <col style="width:15%;">
                                                        <col style="width:35%;">
                                                    </colgroup>
                                                    <tbody>
                                                    <tr>
                                                        <th scope="row"><?php echo __('사업자번호')?></th>
                                                        <td colspan="3"><input type="text" name="taxBusiNo" placeholder="- <?php echo __('없이 입력하세요')?>" value="<?php echo str_replace('-','',gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['taxBusiNo']))?>" maxlength="10" class="text"/></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row"><?php echo __('회사명')?></th>
                                                        <td><input type="text" name="taxCompany" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['company'])?>" maxlength="50" data-pattern="gdMemberNmGlobal" class="text"/></td>
                                                        <th scope="row"><?php echo __('대표자명')?></th>
                                                        <td><input type="text" name="taxCeoNm" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['ceo'])?>" class="text"/></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row"><?php echo __('업태')?></th>
                                                        <td><input type="text" name="taxService" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['service'])?>" class="text"/></td>
                                                        <th scope="row"><?php echo __('종목')?></th>
                                                        <td><input type="text" name="taxItem" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['item'])?>" class="text"/></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row"><?php echo __('사업장주소')?></th>
                                                        <td colspan="3" class="member_address">
                                                            <div class="address_postcode">
                                                                <input type="text" name="taxZonecode" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comZonecode'])?>" readonly="readonly" class="text" >
                                                                <input type="hidden" name="taxZipcode" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comZipcode'])?>">
                                                                <span id="taxrZipcodeText" class="old_post_code"></span>
                                                                <button type="button" onclick="gd_postcode_search('taxZonecode', 'taxAddress', 'taxZipcode');" class="btn_post_search"><?php echo __('우편번호검색')?></button>
                                                            </div>
                                                            <div class="address_input">
                                                                <input type="text" name="taxAddress" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comAddress'])?>" class="text"/>
                                                                <input type="text" name="taxAddressSub" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['comAddressSub'])?>" class="text"/>
                                                            </div>
                                                        </td>
                                                    </tr>
<?php if($TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
                                                    <tr>
                                                        <th scope="row"><?php echo __('발행 이메일')?></th>
                                                        <td colspan="3" class="cash_receipt_email">
                                                            <input type="text" name="taxEmail" placeholder="<?php echo __('미입력 시 주문자의 이메일로 발행')?>" value="<?php echo gd_isset($TPL_VAR["memberInvoiceInfo"]['tax']['email'])?>" class="text" />
                                                            <select id="taxEmailDomain" class="chosen-select">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
                                                                <option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
                                                            </select>
                                                        </td>
                                                    </tr>
<?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- //tax_invoice_box -->
<?php }?>

                                    </div>
                                    <!-- //cash_tax_get -->
                                    <!-- N : 현금영수증/계산서 발행 끝-->
<?php }?>

                                </div>
                                <!-- //payment_progress_list -->
                                <div class="payment_final">
                                    <div class="payment_final_total">
                                        <dl>
                                            <dt><?php echo __('최종 결제 금액')?></dt>
                                            <dd><span><?php echo gd_global_currency_symbol()?><strong id="totalSettlePriceView"><?php echo gd_global_money_format($TPL_VAR["totalSettlePrice"])?></strong><?php echo gd_global_currency_string()?></span></dd>
                                        </dl>
<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                        <p class="add_currency"><?php echo gd_global_add_currency_symbol()?><em id="totalAddSettlePriceView"><?php echo gd_global_add_money_format($TPL_VAR["totalSettlePrice"])?></em><?php echo gd_global_add_currency_string()?></p>
<?php }?>

<?php if($TPL_VAR["totalMemberBankDcPrice"]> 0){?>
                                        <br /><br /><span style="font-size:12px;" id="totalMemberBankDcPriceViewBottom">(<?php echo __('일반결제의 무통장 입금 추가할인이 적용된 금액이며, 다른 결제수단 이용시')?> <label style="font-size:12px; font-weight:bold; color:#393838;"><?php echo gd_global_currency_display($TPL_VAR["totalMemberBankDcPrice"])?> <?php echo __('이 추가된 금액으로 최종 결제')?></label><?php echo __(' 됩니다.')?>)</span>
<?php }?>
                                    </div>
<?php if($TPL_VAR["orderPolicy"]["reagreeConfirmFl"]=='y'){?>
                                    <div class="payment_final_check">
                                        <div class="form_element">
                                            <input type="checkbox" id="termAgree_orderCheck" class="require">
                                            <label for="termAgree_orderCheck" class="check_s"><em><b>(<?php echo __('필수')?>)</b> <?php echo __('구매하실 상품의 결제정보를 확인하였으며, 구매진행에 동의합니다.')?></em></label>
                                        </div>
                                    </div>
<?php }?>
<?php if($TPL_VAR["settleOrderPossible"]){?>
                                    <div class="btn_center_box button_area inline">
                                        <button class="btn_order_buy order-buy button orange"><em><?php echo __('결제하기')?></em></button>
                                    </div>
<?php }else{?>
                                    <strong class="chk_none"><?php echo __('주문하시는 상품의 결제 수단이 상이 하여 결제가 불가능합니다.')?></strong>
                                    <div class="btn_center_box button_area inline">
                                        <button class="btn_order_buy order-buy button orange"><em><?php echo __('결제하기')?></em></button>
                                    </div>
<?php }?>
                                </div>
                                <!-- //payment_final -->

                            </div>
                            <!-- //payment_progress -->

                        </div>
                        <!-- //order_view_info -->
                    </div>
                    <!-- //order_cont -->
                </div>
                <!-- //order_wrap -->
            </form>
        </div>
        <!-- //content_box -->
    </div><!-- .center_layer -->
</section>

<!-- 나의 배송지 관리 레이어 -->
<div id="myShippingListLayer" class="layer_wrap delivery_add_list_layer dn"></div>
<!-- //나의 배송지 관리 레이어 -->
<?php if($TPL_VAR["couponUse"]=='y'){?>
<!-- 상품 쿠폰 적용하기 레이어 -->
<div id="couponApplyLayer" class="layer_wrap coupon_apply_layer dn"></div>
<!--//상품 쿠폰 적용하기 레이어 -->
<!-- 주문 쿠폰 적용하기 레이어 -->
<div id="couponOrderApplyLayer" class="layer_wrap coupon_apply_layer dn"></div>
<!--//주문 쿠폰 적용하기 레이어 -->
<?php }?>
<!-- PG 결제 적용하기 레이어 -->
<div id="pgSettlementApplyLayer" class="layer_wrap pg_layer dn"></div>
<!--//PG 결제 적용하기 레이어 -->
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
<!-- 복수배송지 상품 선택 레이어 -->
<div id="cartSelectGoods" class="layer_wrap dn"></div>
<!--//복수배송지 상품 선택 레이어 -->
<script type="text/html" class="template" id="multiShippingRow">
    <table class="table_left shipping_info_table" style="margin-top:20px;">
        <colgroup>
            <col style="width:15%;">
            <col style="width:85%;">
        </colgroup>
        <tbody>
        <tr>
            <th scope="row"><?php echo __('추가 배송지')?><span class="no"><%=no%></span></th>
            <td>
                <div class="form_element">
                    <ul>
                        <li>
                            <input type="radio" name="shippingAdd[<%=no%>]" id="shippingNewAdd<%=no%>">
                            <label class="choice_s on" for="shippingNewAdd<%=no%>"><?php echo __('직접 입력')?></label>
                        </li>
                    </ul>
<?php if(gd_is_login()===true){?>
                    <span class="btn_gray_list"><a href="#myShippingListLayer" class="btn_gray_small btn_open_layer js_shipping" data-no="<%=no%>"><span><?php echo __('배송지 관리')?></span></a></span>
<?php }?>
                </div>

                <span class="btn_gray_list"><a class="btn_gray_small shipping_remove_btn" style="float:right; cursor:pointer;"><span>- <?php echo __('삭제')?></span></a></span>
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('받으실분')?></span></th>
            <td>
                <input type="text" name="receiverNameAdd[<%=no%>]" value="" data-pattern="<?php if($TPL_VAR["gGlobal"]['isFront']){?>gdMemberNmGlobal<?php }else{?>gdEngKor<?php }?>" maxlength="20" class="text" />
            </td>
        </tr>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
        <tr>
            <th scope="row"><span class="important"><?php echo __('국가')?></span></th>
            <td>
                <?php echo gd_select_box('receiverCountryCodeAdd[<%=no%>]','receiverCountryCodeAdd[<%=no%>]',$TPL_VAR["countryAddress"],null,$TPL_VAR["recommendCountryCode"],__('=선택해주세요='),null,'tune select-small')?>

            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('도시')?></span></th>
            <td>
                <input type="text" name="receiverCityAdd[<%=no%>]" value="" maxlength="20" class="text" />
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('주/지방/지역')?></span></th>
            <td>
                <input type="text" name="receiverStateAdd[<%=no%>]" value="" maxlength="20" class="text" />
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('주소1')?></span></th>
            <td>
                <input type="text" name="receiverAddressAdd[<%=no%>]" value="" class="text" />
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('주소2')?></span></th>
            <td>
                <input type="text" name="receiverAddressSubAdd[<%=no%>]" value="" class="text" />
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('우편번호')?></span></th>
            <td>
                <input type="text" name="receiverZonecodeAdd[<%=no%>]" value=""/>
                <input type="hidden" name="receiverZipcodeAdd[<%=no%>]" value=""/>
            </td>
        </tr>
<?php }else{?>
        <tr>
            <th scope="row"><span class="important"><?php echo __('받으실 곳')?></span></th>
            <td class="member_address">
                <div class="address_postcode">
                    <input type="text" name="receiverZonecodeAdd[<%=no%>]" readonly="readonly" />
                    <input type="hidden" name="receiverZipcodeAdd[<%=no%>]"/>
                    <span id="receiverZipcodeTextAdd[<%=no%>]" class="old_post_code"></span>
                    <button type="button" data-no="<%=no%>" class="btn_post_search postcode_search post_search"><?php echo __('우편번호검색')?></button>
                </div>
                <div class="address_input">
                    <input type="text" name="receiverAddressAdd[<%=no%>]" value="" readonly="readonly" class="text" />
                    <input type="text" name="receiverAddressSubAdd[<%=no%>]" value="" class="text" />
                </div>
            </td>
        </tr>
<?php }?>
        <tr>
            <th scope="row"><?php echo __('전화번호')?></th>
            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                <?php echo gd_select_box('receiverPhonePrefixCodeAdd[<%=no%>]','receiverPhonePrefixCodeAdd[<%=no%>]',$TPL_VAR["countryPhone"],null,null,__('국가코드'),null,'tune select-small')?>

<?php }?>
                <input type="text" id="receiverPhoneAdd[<%=no%>]" name="receiverPhoneAdd[<%=no%>]" value="" class="text" />
            </td>
        </tr>
        <tr>
            <th scope="row"><span class="important"><?php echo __('휴대폰 번호')?></span></th>
            <td>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                <?php echo gd_select_box('receiverCellPhonePrefixCodeAdd[<%=no%>]','receiverCellPhonePrefixCodeAdd[<%=no%>]',$TPL_VAR["countryPhone"],null,null,__('국가코드'),null,'tune select-small')?>

<?php }?>
                <input type="text" id="receiverCellPhoneAdd[<%=no%>]" name="receiverCellPhoneAdd[<%=no%>]" value="" class="text" />
<?php if($TPL_VAR["gGlobal"]["isFront"]==false&&$TPL_VAR["useSafeNumberFl"]=='y'){?>
                <span class="form_element td_chk">
                    <input type="checkbox" id="receiverUseSafeNumberFlAdd<%=no%>" name="receiverUseSafeNumberFlAdd[<%=no%>]" value="w" checked="checked">
                    <label for="receiverUseSafeNumberFlAdd<%=no%>" class="check_s on"><em><?php echo __('안심번호 사용')?></em></label>
                </span>
                <div><?php echo __('안심번호 사용 체크 시 1회 성으로 가상의 번호인 안심번호(050*-0000-0000)를 부여합니다. 안심번호는 일정 기간이 지나면 자동으로 해지됩니다.')?></div>
<?php }?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('남기실 말씀')?></th>
            <td>
                <input type="text" name="orderMemoAdd[<%=no%>]" value="" class="text" />
            </td>
        </tr>
<?php if(gd_is_login()===true&&!$TPL_VAR["gGlobal"]["isFront"]){?>
        <tr>
            <th scope="row"><?php echo __('회원정보 반영')?></th>
            <td>
                <div class="form_element">
                    <div class="member_info_delivery">
                        <input type="checkbox" name="reflectApplyDeliveryAdd[<%=no%>]" value="y" id="reflectApplyDeliveryAdd<%=no%>">
                        <label for="reflectApplyDeliveryAdd<%=no%>" class="check_s"><?php echo __('나의 배송지에 추가합니다.')?></label><br />
                    </div>
                </div>
            </td>
        </tr>
<?php }?>
        <tr class="select_goods_tr">
            <th scope="row"><?php echo __('배송 상품')?></th>
            <td>
                <input type="hidden" name="selectGoods[<%=no%>]" value="">
                <input type="hidden" name="multiDelivery[<%=no%>]" value="0">
                <span class="btn_gray_list"><a href="#cartSelectGoods" class="btn_gray_small shipping_goods_select btn_open_layer" style="margin-bottom:5px;"><span>+ <?php echo __('상품 선택')?></span></a></span>
                <div class="table1 type1 select_goods order_goods dn">
                    <table class="check"></table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</script>
<?php }?>

<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/jquery.textchange.js"></script>
<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/jquery.serialize.object.js"></script>
<script type="text/javascript" src="/data/skin/front/aileenwedding/js/jquery/validation/additional/businessnoKR.js"></script>
<script type="text/javascript">
    <!--
    // 배송지 데이터 글로벌 선언
    var defaultShippingAddress = <?php echo $TPL_VAR["defaultShippingAddress"]?>;
    var recentShippingAddress = <?php echo $TPL_VAR["recentShippingAddress"]?>;
    // 마일리지 사용 정보
    var mileageUse = {
        'usableFl' : '<?php echo $TPL_VAR["mileageUse"]["usableFl"]?>',
        'minimumHold' : parseInt('<?php echo $TPL_VAR["mileageUse"]["minimumHold"]?>'),
        'minimumLimit' : parseInt('<?php echo $TPL_VAR["mileageUse"]["minimumLimit"]?>'),
        'orderAbleLimit' : parseInt('<?php echo $TPL_VAR["mileageUse"]["orderAbleLimit"]?>'),
        'orderAbleStandardPrice' : parseInt('<?php echo $TPL_VAR["mileageUse"]["orderAbleStandardPrice"]?>'), // '최소 상품구매금액 제한' 을 비교하기 위한 계산된 구매금액
        'useDeliveryFl' : '<?php echo $TPL_VAR["mileageUse"]["useDeliveryFl"]?>',
        'maximumLimit' : '<?php echo $TPL_VAR["mileageUse"]["maximumLimit"]?>',
        'oriMaximumLimit' : parseInt('<?php echo $TPL_VAR["mileageUse"]["oriMaximumLimit"]?>'), // 변형되지 않은 설정 그대로의 값 - %는 원으로 계산되어 나옴
    };

    $(document).ready(function(){

        $('input.number_only').on('keyup', function () {
            var value = $(this).val().replace(/[^0-9]/g, '');
            $(this).val(value);
        });

        $(document).on('click', '#pgSettlementApplyLayer .close', function(e){
            $(this).closest('.layer_wrap').addClass('dn');
            $('#layerDim').addClass('dn');
            //$('html').removeClass('oh-space');
            //$('#scroll-left, #scroll-right').removeClass('dim');
            $('.inipay_modal-backdrop').remove();
        });

<?php if($TPL_VAR["couponUse"]=='y'){?>
        // 쿠폰 적용/변경 레이어
        $('.btn_open_layer').bind('click', function(e){
            if($(this).attr('href') == '#couponOrderApplyLayer') {
                // 마일리지 쿠폰 중복사용 체크
                var checkMileageCoupon = gd_choose_mileage_coupon('coupon');
                if (!checkMileageCoupon) {
                    return false;
                }

                var cartIdx = [];
                $('input[name="cartSno[]"]').each(function(idx){
                    cartIdx.push($(this).val());
                });
                var params = {
                    mode: 'coupon_apply_order',
                    cartSno: cartIdx,
                    couponApplyOrderNo: $('input:hidden[name="couponApplyOrderNo"]').val(),
                };
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../order/layer_coupon_apply_order.php",
                    data: params,
                    success: function (data) {
                        $('#couponOrderApplyLayer').empty().append(data);
                        $('#couponOrderApplyLayer').find('>div').center();
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
<?php }?>

        // 사은품 체크 및 체크된 수량 출력
        $('.order_freebie_list input[type=checkbox]').click(function(e){
            if($(this).prop('readonly') == false) {
                var selectCnt = $(this).closest('dl').find('.gift_select_cnt').val();
                var checkedCnt = $(this).closest('dl').find('input[type=checkbox]:checked').length;
                if (checkedCnt > selectCnt) {
                    alert("{사은품은 최대 " + selectCnt + "개만 선택하실 수 있습니다.");
                    $(this).prop('checked', false).next('label').removeClass('on');

                    return false;
                }
                $(this).closest('dd').prev('dt').find('strong').text(checkedCnt);
            }
        });

        // 배송지관리 이벤트
        $(document).on('click', '.btn_open_layer.js_shipping', function(e) {
            var shippingNo = '';
            $('#myShippingListLayer').empty();
            if (typeof $(this).data('no') != 'undefined') {
                shippingNo = $('.btn_open_layer.js_shipping').index(this);
            }

            $.get('../order/layer_shipping_address.php?shippingNo=' + shippingNo, function(data){
                $('#myShippingListLayer').append(data);
                $('#myShippingListLayer').find('>div').center();
            });
        });

        // 세금계산서 관련 체크용
        var checkTax = function() {
            if ($('#settleKind_gb').is(':checked') && $('#receiptTax').is(':checked')) {
                return true;
            }
            return false;
        }

        // 무통장입금 체크
        var checkBank = function() {
            if ($('#settleKind_gb').is(':checked') && parseInt($('input[name=settlePrice]').val()) > 0) {
                return true;
            }
            return false;
        }

        // 우편번호 체크를 위한 알파벳+숫자+띄어쓰기 체크
        $.validator.addMethod( "alphanumeric", function( value, element ) {
            return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
        }, __("알파벳과 숫자로만 구성되어야 합니다.") );

<?php if($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
        $.validator.addMethod(
            'taxEmailChk', function (value, element) {
                if($('input[name=receiptFl]:checked').val() =='t' && $('input[name=taxEmail]').val() !=  '' && !$.validator.methods.email.call(this, $.trim(value), element)){
                    $.validator.messages.taxEmailChk =  __('[세금계산서] 발행 이메일을 정확하게 입력하여 주세요.');
                    return false;
                }
                return true;
            },'');
<?php }?>

        /*
         * 비회원 주문 전체 동의 체크박스 이벤트
         */
<?php if(gd_is_login()===false){?>
<?php if($TPL_VAR["hasScmGoods"]===false){?>
        $('#allAgree', $('#frmOrder')).change(function (e) {
            e.preventDefault();
            var $target = $(e.target), $checkboxAgreement = $('#termAgree_agreement'),  $checkboxGuest = $('#termAgree_guest'), $labelAgree = $checkboxAgreement.siblings('label'), $labelGuest = $checkboxGuest.siblings('label');
            if ($target.prop('checked') === true) {
                $checkboxAgreement.prop('checked', true).val('y');
                $labelAgree.addClass('on');
                $checkboxGuest.prop('checked', true).val('y');
                $labelGuest.addClass('on');
            } else {
                $checkboxAgreement.prop('checked', false).val('n');
                $labelAgree.removeClass('on');
                $checkboxGuest.prop('checked', false).val('n');
                $labelGuest.removeClass('on');
            }
        });
<?php }else{?>
        $('#allAgreeScm', $('#frmOrder')).change(function (e) {
            e.preventDefault();
            var $target = $(e.target),$checkboxAgreement = $('#termAgree_agreement'),  $checkboxGuest = $('#termAgree_guest'), $labelAgree = $checkboxAgreement.siblings('label'), $labelGuest = $checkboxGuest.siblings('label'), $checkboxScm = $('#termAgree_privateProvider'), $labelScm = $checkboxScm.siblings('label');
            if ($target.prop('checked') === true) {
                $checkboxAgreement.prop('checked', true).val('y');
                $labelAgree.addClass('on');
                $checkboxGuest.prop('checked', true).val('y');
                $labelGuest.addClass('on');
                $checkboxScm.prop('checked', true).val('y');
                $labelScm.addClass('on');
            } else {
                $checkboxAgreement.prop('checked', false).val('n');
                $labelAgree.removeClass('on');
                $checkboxGuest.prop('checked', false).val('n');
                $labelGuest.removeClass('on');
                $checkboxScm.prop('checked', false).val('n');
                $labelScm.removeClass('on');
            }
        });
<?php }?>
<?php }?>
        // 폼 체크
        $('#frmOrder').validate({
            onkeyup: onkeyup,
            invalidHandler: function(event, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    alert(validator.errorList[0].message);
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
<?php if($TPL_VAR["giftConf"]["giftFl"]=='y'){?>
<?php if($TPL_giftInfo_1){foreach($TPL_VAR["giftInfo"] as $TPL_K1=>$TPL_V1){
if (is_array($TPL_V1["gift"])) $TPL_gift_2=count($TPL_V1["gift"]); else if (is_object($TPL_V1["gift"]) && in_array("Countable", class_implements($TPL_V1["gift"]))) $TPL_gift_2=$TPL_V1["gift"]->count();else $TPL_gift_2=0;?>
<?php if($TPL_gift_2){foreach($TPL_V1["gift"] as $TPL_V2){?>
<?php if($TPL_V2["total"]> 0){?>
                var selectCnt = $('input[type=checkbox][name*="gift[<?php echo $TPL_K1?>]"]').closest('dl').find('.gift_select_cnt').val();
                if ($('input[type=checkbox][name*="gift[<?php echo $TPL_K1?>]"]:checked').length < selectCnt) {
                    pass = false;
                    alert("사은품은 최소 " + selectCnt + "개 이상 선택하셔야 합니다.");
                    $('input[type=checkbox][name*="gift[<?php echo $TPL_K1?>]"]').eq(0).focus();
                    return false;
                }
<?php }?>
<?php }}?>
<?php }}?>
<?php }?>

<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
                if ($('input[name="multiShippingFl"]').prop('checked') === true) {
                    var msg = '';
                    var cartGoodsCnt = 0;
                    var cartAddGoodsCnt = 0;

                    $('input[name^="selectGoods"]').each(function(index){
                        if (index > 0) {
                            if (!$('input[name="receiverNameAdd[' + index + ']"]').val()) {
                                $('input[name="receiverNameAdd[' + index + ']"]').focus();
                                msg = __('받으실 분 정보를 입력해 주세요.');
                                return false;
                            }
                            if (!$('input[name="receiverZonecodeAdd[' + index + ']"]').val()) {
                                $('input[name="receiverZonecodeAdd[' + index + ']"]').focus();
                                msg = __('받으실 곳 우편번호 정보를 입력해 주세요.');
                                return false;
                            }
                            if (!$('input[name="receiverAddressAdd[' + index + ']"]').val()) {
                                $('input[name="receiverAddressAdd[' + index + ']"]').focus();
                                msg = __('받으실 곳 주소 정보를 입력해 주세요.');
                                return false;
                            }
                            if (!$('input[name="receiverAddressSubAdd[' + index + ']"]').val()) {
                                $('input[name="receiverAddressSubAdd[' + index + ']"]').focus();
                                msg = __('받으실 곳 주소 정보를 입력해 주세요.');
                                return false;
                            }
                            if (!$('input[name="receiverCellPhoneAdd[' + index + ']"]').val()) {
                                $('input[name="receiverCellPhoneAdd[' + index + ']"]').focus();
                                msg = __('받으실 분 휴대폰 번호 정보를 입력해 주세요.');
                                return false;
                            }

                            for (var i = 0; i < index ; i++) {
                                var prevReceiverZoneCode = $('input[name="receiverZonecodeAdd[' + i + ']"]').val();
                                var prevReceiverAddressSub = $('input[name="receiverAddressSubAdd[' + i + ']"]').val();

                                if (i == 0) {
                                    prevReceiverZoneCode = $('input[name="receiverZonecode"]').val();
                                    prevReceiverAddressSub = $('input[name="receiverAddressSub"]').val();
                                }

                                if (prevReceiverZoneCode == $('input[name="receiverZonecodeAdd[' + index + ']"]').val() && prevReceiverAddressSub == $('input[name="receiverAddressSubAdd[' + index + ']"]').val()) {
                                    msg = __('복수 배송지를 이용 시에는 동일 배송지를 선택할 수 없습니다.');
                                    return false;
                                }
                            }
                        }
                        if ($(this).val()) {
                            $.parseJSON($(this).val()).forEach(function(ele){
                                if (ele.goodsCnt > 0) {
                                    cartGoodsCnt += parseInt(ele.goodsCnt);
                                }
                                if (ele.addGoodsTotalCnt > 0) {
                                    cartAddGoodsCnt += parseInt(ele.addGoodsTotalCnt);
                                }
                            });
                        } else {
                            msg = __('배송지마다 최소 1개 이상의 주문상품이 선택되어야 합니다.');
                            return false;
                        }
                    });

                    if (msg) {
                        alert(msg);
                        return false;
                    }

                    if (cartGoodsCnt != $('input[name="cartGoodsCnt"]').val() || cartAddGoodsCnt != $('input[name="cartAddGoodsCnt"]').val()) {
                        alert(__('배송지가 설정되지 않은 주문상품이 존재합니다.'));
                        return false;
                    }
                }
<?php }?>

                var pass = true;
                $('input:checkbox[id*="termAgree_"].require').each(function (idx, item) {
                    var $item = $(item);
                    if (!$item.prop('checked')) {
                        pass = false;
                        alert("<?php echo __('청약의사 재확인을 동의해 주셔야 주문을 진행하실 수 있습니다.')?>");
                        return false;
                    }
                });

                if (pass && $('input[name=settleKind]').length == 0 && $('#totalSettlePriceView').html() != '0') {
                    alert(__('선택된 결제 수단이 없습니다.'));
                    return false;
                }

                if (pass) {
                    //무통장 주문결제시에 버튼 비활성화
                    if($('input[name=settleKind]:checked').val() ==  'gb'){
                        $('.order_buy').attr("disabled","disabled");
                        $('.order_buy em').html("<?php echo __('결제처리중')?>");
                    }
                    form.target = 'ifrmProcess'
                    form.submit();
                }
            },
            rules: {
<?php if(gd_is_login()==false){?>
                termAgreeGuest: 'required',
                termAgreeAgreement: 'required',
                termAgreePrivate: 'required',
<?php }?>
<?php if($TPL_VAR["hasScmGoods"]){?>
                termAgreePrivateProvider: 'required',
<?php }?>
                orderName: 'required',
                orderCellPhone: 'required',
                orderEmail: {
                    required: true,
                    email: true
                },
                receiverName: 'required',
                receiverZonecode: {
                    required: true,
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                    number: true,
<?php }else{?>
                    alphanumeric: true,
<?php }?>
                },
                receiverAddress: 'required',
                receiverAddressSub: 'required',
                receiverCellPhone: 'required',
                bankSender: {
                    required: checkBank
                },
                bankAccount: {
                    required: checkBank
                },
                taxBusiNo: {
                    required: checkTax,
                    businessnoKR: checkTax
                },
                taxCompany: {
                    required: checkTax
                },
                taxCeoNm: {
                    required: checkTax
                },
                taxService: {
                    required: checkTax
                },
                taxItem: {
                    required: checkTax
                },
<?php if($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["taxInfo"]["eTaxInvoiceFl"]=='y'){?>
                taxEmail: {
                    taxEmailChk: true
                },
<?php }?>
                taxAddress: {
                    required: checkTax
                }
            },
            messages: {
<?php if(gd_is_login()==false){?>
                termAgreeGuest: {
                    required: "<?php echo __('비회원 주문에 대한 개인정보 수집 이용에 동의해 주세요.')?>"
                },
                termAgreeAgreement: {
                    required: "<?php echo __('이용약관에 동의해 주세요.')?>"
                },
                termAgreePrivate: {
                    required: "<?php echo __('개인정보 수집 이용 약관에 동의해 주세요.')?>"
                },
<?php }?>
<?php if($TPL_VAR["hasScmGoods"]){?>
                termAgreePrivateProvider: {
                    required: "<?php echo __('상품 공급사 개인정보 제공에 동의해 주세요.')?>"
                },
<?php }?>
                orderName: {
                    required: "<?php echo __('주문하시는 분 정보를 입력해 주세요.')?>"
                },
                orderCellPhone: {
                    required: "<?php echo __('주문하시는 분 휴대폰 번호 정보를 입력해 주세요.')?>"
                },
                orderEmail: {
                    required: "<?php echo __('주문하시는 분 이메일 정보를 입력해 주세요.')?>",
                    email: "<?php echo __('이메일을 정확하게 입력해주세요.')?>"
                },
                receiverName: {
                    required: "<?php echo __('받으실 분 정보를 입력해 주세요.')?>"
                },
                receiverZonecode: {
                    required: "<?php echo __('받으실 곳 우편번호 정보를 입력해 주세요.')?>",
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                    number: "<?php echo __('숫자만 입력하실 수 있습니다.')?>",
<?php }else{?>
                    alphanumeric: "<?php echo __('알파벳과 숫자로만 구성되어야 합니다.')?>",
<?php }?>
                },
                receiverAddress: {
                    required: "<?php echo __('받으실 곳 주소 정보를 입력해 주세요.')?>"
                },
                receiverAddressSub: {
                    required: "<?php echo __('받으실 곳 주소 정보를 입력해 주세요.')?>"
                },
                receiverCellPhone: {
                    required: "<?php echo __('받으실 분 휴대폰 번호 정보를 입력해 주세요.')?>"
                },
                bankSender: {
                    required: "<?php echo __('입금자명을 입력해주세요.')?>"
                },
                bankAccount: {
                    required: "<?php echo __('입금은행을 선택해주세요.')?>"
                },
                taxBusiNo: {
                    required: "<?php echo __('[세금계산서] 사업자번호를 입력하세요.')?>"
                },
                taxCompany: {
                    required: "<?php echo __('[세금계산서] 회사명을 입력하세요.')?>"
                },
                taxCeoNm: {
                    required: "<?php echo __('[세금계산서] 대표자명을 입력하세요.')?>"
                },
                taxService: {
                    required: "<?php echo __('[세금계산서] 업태를 입력하세요.')?>"
                },
                taxItem: {
                    required: "<?php echo __('[세금계산서] 종목을 입력하세요.')?>"
                },
                taxAddress: {
                    required: "<?php echo __('[세금계산서] 사업장주소를 입력하세요.')?>"
                }
            },
            focusInvalid: {
                receiverName: true,
                receiverZonecode: true,
                receiverAddress: true,
                receiverAddressSub: true,
                receiverCellPhone: true,
            }
        });

        if ($('input[name=settleKind]').length > 0) {
            // 일반결제 > 결제수단 선택 클릭 이벤트
            $('input[name=settleKind]').click(function(e){
                //페이코 결제가 아닐때 처리
                if ($(this).val().substring(0, 1) != 'f') {
                    gd_payment_reset();
                    gd_settlekind_selector($(this).val());
                }
            });
        }

        // 마일리지 체크 이벤트
<?php if(gd_is_login()===true&&$TPL_VAR["mileageUse"]["payUsableFl"]!='n'){?>
        gd_mileage_use_check('n', 'n', 'n', 'n');
<?php }?>
        $('input[name=useMileage]').blur(function(e){
            if (!_.isUndefined(e.isTrigger)) {
                gd_mileage_use_check('y', 'y', 'y', 'y');
            }
        });

        // 마일리지 쿠폰 중복사용 체크
        $('input[name=useMileage]').change(function (e) {
            // 마일리지 쿠폰 중복사용 체크
            e.preventDefault();
            gd_choose_mileage_coupon('mileage');
        });

        // 예치금 체크 이벤트
        $('input[name=useDeposit]').blur(function(e){
            if (!_.isUndefined(e.isTrigger)) {
                gd_deposit_use_check();
            }
        });

        // 배송지 선택
        $('input[name=shipping]:radio').click(function(e){
            switch ($(this).prop('id')) {
                // 기본배송지
                case 'shippingBasic':
                    if (!_.isEmpty(defaultShippingAddress)) {
                        gd_set_delivery_shipping_address(defaultShippingAddress);
                    } else {
                        alert("<?php echo __('배송지관리 목록이 없습니다.')?>");
                        return false;
                    }
                    break;

                // 최근 배송지
                case 'shippingRecently':
                    if (!_.isEmpty(recentShippingAddress)) {
                        gd_set_delivery_shipping_address(recentShippingAddress);
                    } else {
                        alert("<?php echo __('최근 배송지가 없습니다.')?>");
                        return false;
                    }
                    break;

                // 신규 배송지
                // 주문자정보와 동일
                case 'shippingNew':
                case 'shippingSameCheck':
                    gd_order_info_same();
                    break;
            }
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
            gd_get_delivery_country_charge();
<?php }?>
            gd_reflect_apply_delivery($(this).prop('id'));
        });

        // 지역별 배송비 체크
        $(document).on('blur', 'input[name^=receiverAddressSub]', function(e){
            gd_get_delivery_area_charge();
        });

        // 해외 배송비 체크
        $('select[name=receiverCountryCode]').change(function(e){
            gd_get_delivery_country_charge();
        });

        // 페이지 로딩시 우선국가 지정으로 인해 배송비 체크
        if ($('select[name=receiverCountryCode]').val()) {
            $('select[name=receiverCountryCode]').trigger('change');
        }

        // 이메일 도메인 대입
        gd_select_email_domain('orderEmail');
        gd_select_email_domain('taxEmail','taxEmailDomain');
        $("#taxEmailDomain_chosen").width("120px");

        // 결제 방법 선택
        if ($('input[name=settleKind]').length > 0) {
            gd_settlekind_toggle();
        }

<?php if($TPL_VAR["isPayLimit"]> 0){?>
        var adddHtml = '';
        adddHtml +=  "<strong><?php echo __('결제수단')?></strong>";
        adddHtml +=  "<ul>";
<?php if($TPL_useSettleKindPg_1){foreach($TPL_VAR["useSettleKindPg"] as $TPL_V1){?>
        adddHtml +=  "<li><?php echo $TPL_V1?></li>";
<?php }}?>
        adddHtml +=  "</ul>";
        $(adddHtml).appendTo('.icon_pg_cont');
<?php }else{?>
        $('.icon_pg_cont').addClass('dn');
<?php }?>

        // 기본배송비 설정에 따른 트리거 처리
        if (!_.isEmpty(defaultShippingAddress)) {
            $('#shippingBasic').prop('checked', true).trigger('click');
            gd_get_delivery_area_charge();
        } else {
            $('#shippingNew').prop('checked', true);
            $('label[for="shippingNew"]').addClass('on');
        }

<?php if($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["memberInvoiceInfo"]['tax']['memberTaxInfoFl']=='y'){?>
        // 세금계산서 입력 정보 초기화
        $('.tax_info_init').on('click', function (e) {
            e.preventDefault();
            $('#tax_info').find('input').val('');
        });
<?php }?>

        gd_set_real_settle_price();
    });

    /**
     * 기본배송지 가져오기
     */
    function getDefaultShippingAddress()
    {
        return defaultShippingAddress;
    }

    /**
     * 기본배송지 설정하기
     */
    function gd_set_default_shipping_address(data)
    {
        defaultShippingAddress = data;
    }

    /**
     * 지역별 배송비 체크 (우편번호 팝업에서 콜백받는 함수)
     */
    function postcode_callback() {
        gd_get_delivery_area_charge();
    }

    /**
     * 주소에 따른 지역별 배송비 가져오기
     */
    function gd_get_delivery_area_charge() {
        var cartIdx = [];
        $('input[name="cartSno[]"]').each(function(idx){
            cartIdx.push($(this).val());
        });
        var params = {
            mode: 'check_area_delivery',
            cartSno: cartIdx,
            receiverAddress: $('input[name=receiverAddress]').val(),
            receiverAddressSub: $('input[name=receiverAddressSub]').val(),
            totalCouponOrderDcPrice: $('input:hidden[name="totalCouponOrderDcPrice"]').val(),
        };
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
        if ($('input[name="multiShippingFl"]').prop('checked') === true) {
            params = {
                mode: 'check_multi_area_delivery',
                data: $('#frmOrder').serialize(),
                cartAllSno: cartIdx,
            };
        }
<?php }?>
        $.post('cart_ps.php', params, function(data){
            $('input[name=deliveryAreaCharge]').val(data.areaDelivery);
            gd_set_real_settle_price();

<?php if(gd_is_login()===true&&$TPL_VAR["mileageUse"]["payUsableFl"]!='n'){?>
            mileageUse = data.mileageUse;

            gd_mileage_use_check('y', 'n', 'n', 'n');
<?php }?>
        });
    }

    /**
     * 국가에 따른 해외 배송비 가져오기
     */
    function gd_get_delivery_country_charge()
    {
        // 국제 전화번호 셀렉트
        $('select[name=receiverPhonePrefixCode]').val($('select[name=receiverCountryCode]').val()).trigger('chosen:updated');
        $('select[name=receiverCellPhonePrefixCode]').val($('select[name=receiverCountryCode]').val()).trigger('chosen:updated');

        // 배송비 가져오기
        var cartIdx = [];
        $('input[name="cartSno[]"]').each(function(idx){
            cartIdx.push($(this).val());
        });
        var params = {
            mode: 'check_country_delivery',
            countryCode: $('select[name=receiverCountryCode]').val(),
            cartSno: cartIdx,
        };
        $.post('cart_ps.php', params, function(data){
            // 배송비 계산
            if (data.error === 1) {
                data.overseasDelivery = 0;
                data.overseasInsuranceFee = 0;
                alert(data.message);
            }

            $('#totalDeliveryCharge').text(gd_money_format(data.overseasDelivery));
            $('#totalDeliveryChargeAdd').text(gd_add_money_format(data.overseasDelivery));
            $('input[name=totalDeliveryCharge]').val(data.overseasDelivery);
            $('input[name=deliveryInsuranceFee]').val(data.overseasInsuranceFee);
            gd_set_real_settle_price();
        });
    }

    /**
     * 주문고객 정보와 배송지 정보 동일 처리
     */
    function gd_order_info_same()
    {
        var orderKey = new Array('orderName', 'orderCountryCode', 'orderZonecode', 'orderZipcode', 'orderState', 'orderCity', 'orderAddress', 'orderAddressSub', 'orderPhonePrefixCode', 'orderPhone', 'orderCellPhonePrefixCode', 'orderCellPhone');
        var receiverKey = new Array('receiverName', 'receiverCountryCode', 'receiverZonecode', 'receiverZipcode', 'receiverState', 'receiverCity', 'receiverAddress', 'receiverAddressSub', 'receiverPhonePrefixCode', 'receiverPhone', 'receiverCellPhonePrefixCode', 'receiverCellPhone');
        var sameCheck = $('#shippingSameCheck:checked').val();

        if (sameCheck == 'on') {
            var standardKey = orderKey;
        } else {
            var standardKey = receiverKey;
        }

        for (var i = 0; i < standardKey.length; i++) {
            var orderObj = $('select[name=\''+orderKey[i]+'\']:eq(0), input[name=\''+orderKey[i]+'\']:eq(0)');
            var receiverObj = $('select[name=\''+receiverKey[i]+'\']:eq(0), input[name=\''+receiverKey[i]+'\']:eq(0)');
            if (sameCheck == 'on') {
                if (_.isUndefined(orderObj.val())) {
                    continue;
                }

                // 값 입력
                receiverObj.val(orderObj.val());

                // 셀렉트박스 동적 업데이트 처리
                if (receiverObj[0].tagName == 'SELECT') {
                    receiverObj.trigger("chosen:updated");
                }

                if (receiverKey[i] == 'receiverZipcode' && orderObj.val() !='') {
                    $('input[name=receiverZipcode]').val(orderObj.val());
                    //$('#receiverZipcodeText').html('(' + orderObj.val() + ')');
                    $('#receiverZipcodeText').show();
                } else if (receiverKey[i] == 'receiverZipcode') {
                    //$('#receiverZipcodeText').html('');
                    $('#receiverZipcodeText').hide();
                }
            } else {
                if (_.isUndefined(receiverObj.val())) {
                    continue;
                }

                // 값 삭제
                receiverObj.val('');

                // 셀렉트박스 동적 업데이트 처리
                if (receiverObj[0].tagName == 'SELECT') {
                    receiverObj.trigger("chosen:updated");
                }

                if (receiverKey[i] == 'receiverZipcode') {
                    $('#receiverZipcodeText').html('');
                    $('#receiverZipcodeText').hide();
                }
            }
        }

<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
        // 지역별 배송비 실시간 추가
        $('input[name=receiverAddress], input[name=receiverAddressSub]').trigger('blur');
<?php }?>
    }

    /**
     * 배송지관리 주소 가져와 입력하기
     *
     * @param data
     */
    function gd_set_delivery_shipping_address(data, shippingNo)
    {
        if (!_.isUndefined(data.shippingName)) {
            var nameTails = '';
            if (shippingNo > 0) {
                nameTails = 'Add[' + shippingNo + ']';
            }

            $('input[name="receiverName' + nameTails + '"]').val(data.shippingName);
            $('input[name="receiverZonecode' + nameTails + '"]').val(data.shippingZonecode);
            $('select[name="receiverCountryCode' + nameTails + '"]').val(data.shippingCountryCode).trigger('chosen:updated');
            $('input[name="receiverCity' + nameTails + '"]').val(data.shippingCity);
            $('input[name="receiverState' + nameTails + '"]').val(data.shippingState);
            $('input[name="receiverAddress' + nameTails + '"]').val(data.shippingAddress);
            $('input[name="receiverAddressSub' + nameTails + '"]').val(data.shippingAddressSub);
            $('input[name="receiverPhonePrefixCode' + nameTails + '"]').val(data.shippingPhonePrefixCode).trigger('chosen:updated');
            $('input[name="receiverPhone' + nameTails + '"]').val(data.shippingPhone);
            $('input[name="receiverCellPhonePrefixCode' + nameTails + '"]').val(data.shippingCellPhonePrefixCode).trigger('chosen:updated');
            $('input[name="receiverCellPhone' + nameTails + '"]').val(data.shippingCellPhone);

            if (data.shippingZipcode !='') {
                $('input[name="receiverZipcode' + nameTails + '"]').val(data.shippingZipcode);
                //$('#receiverZipcodeText' + nameTails).html('(' + data.shippingZipcode + ')');
                $('#receiverZipcodeText' + nameTails).show();
            }

<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
            // 지역별 배송비 실시간 추가
            $('input[name="receiverAddress' + nameTails + '"], input[name="receiverAddressSub' + nameTails + '"]').trigger('blur');
<?php }else{?>
            // 해외배송비 체크
            $('select[name="receiverCountryCode' + nameTails + ']').trigger('change');
<?php }?>
        }
    }

    /**
     * 현재 결제 금액 체크
     * 우선순위 : 지역배송비 > 주문쿠폰 > 배송비쿠폰 > 마일리지 > 예치금
     *
     * @param exceptMode 제외할 모드
     */
    function gd_set_real_settle_price(exceptMode, isTax)
    {
        // 상품 금액
        var goodsPrice = parseFloat('<?php echo $TPL_VAR["totalGoodsPrice"]?>');
        // 배송비
<?php if(gd_is_login()===true&&$TPL_VAR["productCouponChangeLimitType"]=='n'){?>
        var deliveryPrice = parseFloat($('input[name=totalDeliveryCharge]').val());
<?php }else{?>
        var deliveryPrice = parseFloat('<?php echo $TPL_VAR["totalDeliveryCharge"]?>');
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
        deliveryPrice = parseFloat(multiShippingPrice(deliveryPrice));
<?php }?>
        // 전체 금액
        var settlePrice = goodsPrice + deliveryPrice;
        // 상품 할인 차감
        var goodsDcPrice = parseFloat('<?php echo $TPL_VAR["totalGoodsDcPrice"]?>');
        if (goodsDcPrice > 0) {
            settlePrice = settlePrice - goodsDcPrice;
        }
        // 상품 쿠폰 차감
<?php if(gd_is_login()===true&&$TPL_VAR["productCouponChangeLimitType"]=='n'){?>
            var goodsCouponDcPrice = parseFloat($('input[name=totalCouponGoodsDcPrice]').val());
<?php }else{?>
            var goodsCouponDcPrice = parseFloat('<?php echo $TPL_VAR["totalCouponGoodsDcPrice"]?>');
<?php }?>
        if (goodsCouponDcPrice > 0) {
            // 상품 쿠폰사용시 상품할인 금액이 0 일 경우 무통장 할인율 처리
            if ($('input[name=totalMemberBankDcPrice]').val() > 0) {
                var totalGoodsPrice = parseFloat('<?php echo $TPL_VAR["totalGoodsPrice"]?>');
                var totalMemberBankDcPrice = parseFloat($('input[name=totalMemberBankDcPrice]').val());
                var totalMemberDcPrice = parseFloat($('input[name=totalMemberDcPrice]').val());
                var totalDcPrice = goodsCouponDcPrice + goodsDcPrice + totalMemberDcPrice - totalMemberBankDcPrice;
                if (totalGoodsPrice == totalDcPrice) {
                    $('.total-member-dc-price').html(totalDcPrice);
                    $('.member-dc-price').html(totalMemberDcPrice - totalMemberBankDcPrice);
                    $('input[name="totalMemberDcPrice"]').val(totalMemberDcPrice - totalMemberBankDcPrice);
                    $('input[name="resetMemberBankDcPrice"]').val('y');
                    $('#totalMemberBankDcPriceViewTop').hide();
                    $('#totalMemberBankDcPriceViewBottom').hide();
                }
            }
            settlePrice = settlePrice - goodsCouponDcPrice;
        }

        // 회원 할인 차감
        var totalMemberDcPrice = parseFloat($('input[name=totalMemberDcPrice]').val());
        var totalMemberOverlapDcPrice = parseFloat($('input[name=totalMemberOverlapDcPrice]').val());
        var totalSettlePrice = parseFloat(settlePrice) - totalMemberDcPrice - totalMemberOverlapDcPrice;

        // 실제 결제금액
        var realSettlePrice = totalSettlePrice;

        // 지역별 배송비 합산
        if ($('input[name=deliveryAreaCharge]').val() > 0) {
            var deliveryAreaCharge = parseInt($('input[name=deliveryAreaCharge]').val());
            realSettlePrice += deliveryAreaCharge;
            $('#deliveryAreaCharge').text(numeral(deliveryAreaCharge).format());
            $('#rowDeliverAreaCharge').removeClass('dn');
        } else {
            $('#deliveryAreaCharge').text(numeral(0).format());
            $('#rowDeliverAreaCharge').addClass('dn');
        }

        // 배송비 무료 차감
<?php if($TPL_VAR["deliveryFree"]=='y'){?>
        var deliveryFree = parseInt(deliveryPrice);
        $('.delivery-free').text(numeral(deliveryFree).format());
        $('input[name="totalDeliveryFreePrice"]').val(deliveryFree);
        realSettlePrice -= deliveryFree;
<?php }?>

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
        // 해외 배송비 합산 (위에서 기본적으로 배송비가 포함되기 때문에 해외에서만 적용해야 함)
        if ($('input[name=totalDeliveryCharge]').val() > 0) {
            var deliveryOverseasCharge = parseInt($('input[name=totalDeliveryCharge]').val());
            realSettlePrice += deliveryOverseasCharge;
        }
<?php }?>

        // 해외배송 보험료 합산
        if ($('input[name=deliveryInsuranceFee]').val() > 0) {
            var deliveryInsuranceFee = parseInt($('input[name=deliveryInsuranceFee]').val());
            realSettlePrice += deliveryInsuranceFee;
            $('#deliveryInsuranceFee').text(gd_money_format(deliveryInsuranceFee));
            $('#deliveryInsuranceFeeAdd').text(gd_add_money_format(deliveryInsuranceFee));
            $('#rowDeliveryInsuranceFee').removeClass('dn');
        } else {
            $('#deliveryInsuranceFee').text(numeral(0).format());
            $('#rowDeliveryInsuranceFee').addClass('dn');
        }

<?php if($TPL_VAR["couponUse"]=='y'){?>
        if (exceptMode != 'coupon') {
            // 쿠폰기본설정에서 쿠폰만 사용일때 처리
            if ($('input[name="chooseCouponMemberUseType"]').val() == 'coupon' && $('input[name="couponApplyOrderNo"]').val() != '') {
                var memberDcPrice = totalMemberDcPrice + totalMemberOverlapDcPrice;
                if (memberDcPrice > 0) {
                    realSettlePrice += memberDcPrice;
                }
            }

            // 주문쿠폰 적용 금액
            if ($('input[name="totalCouponOrderDcPrice"]').val() > 0) {
                var originOrderPrice = <?php echo $TPL_VAR["totalGoodsPrice"]-$TPL_VAR["totalGoodsDcPrice"]-$TPL_VAR["totalCouponGoodsDcPrice"]?> - totalMemberDcPrice - totalMemberOverlapDcPrice;
                var originOrderPriceWithoutMember = <?php echo $TPL_VAR["totalGoodsPrice"]-$TPL_VAR["totalGoodsDcPrice"]-$TPL_VAR["totalCouponGoodsDcPrice"]?>;
                // 쿠폰기본설정에서 쿠폰만 사용일때 처리
                if ($('input[name="chooseCouponMemberUseType"]').val() == 'coupon' && $('input[name="couponApplyOrderNo"]').val() != '') {
                    originOrderPrice = originOrderPriceWithoutMember;
                }

                if (!_.isUndefined(originOrderPrice) && $('input[name="totalCouponOrderPrice"]').val() > originOrderPrice) {
                    var useTotalCouponOrderDcPrice = parseFloat(originOrderPrice);
                } else {
                    var useTotalCouponOrderDcPrice = parseFloat($('input[name="totalCouponOrderPrice"]').val());
                }

                // 주문 쿠폰사용시 상품할인 금액이 0 일 경우 무통장 할인율 처리
                if ($('input[name=totalMemberBankDcPrice]').val() > 0) {
                    var totalGoodsPrice = parseFloat('<?php echo $TPL_VAR["totalGoodsPrice"]?>');
                    var totalMemberBankDcPrice = parseInt($('input[name=totalMemberBankDcPrice]').val());
                    var totalCouponOrderPrice = parseInt($('input[name=totalCouponOrderPrice]').val());
                    var totalDcPrice = goodsDcPrice + totalMemberDcPrice - totalMemberBankDcPrice;
                    if (totalGoodsPrice - totalCouponOrderPrice == 0) {
                        $('.total-member-dc-price').html(totalDcPrice);
                        $('.member-dc-price').html(totalMemberDcPrice - totalMemberBankDcPrice);
                        $('input[name="resetMemberBankDcPrice"]').val('y');
                        $('#totalMemberBankDcPriceViewTop').hide();
                        $('#totalMemberBankDcPriceViewBottom').hide();
                        useTotalCouponOrderDcPrice = parseInt(useTotalCouponOrderDcPrice) + totalMemberBankDcPrice;
                        realSettlePrice += totalMemberBankDcPrice;
                    } else {
                        $('#totalMemberBankDcPriceViewTop').show();
                        $('#totalMemberBankDcPriceViewBottom').show();
                    }
                }

                $('input[name="totalCouponOrderDcPrice"]').val(useTotalCouponOrderDcPrice);
                $('#useDisplayCouponDcPrice').text(numeral(useTotalCouponOrderDcPrice).format());
            } else {
                var useTotalCouponOrderDcPrice = 0;
            }

            // 배송비쿠폰 적용 금액
            if ($('input[name="totalCouponDeliveryDcPrice"]').val() > 0) {
                var originDeliveryCharge = numeral().unformat($('#totalDeliveryCharge').text()) + numeral().unformat($('#deliveryAreaCharge').text());
                if (!_.isUndefined($('input[name="deliveryFree"]')) && $('input[name="deliveryFree"]').val() == 'y') {
                    originDeliveryCharge -= numeral().unformat($('#totalDeliveryCharge').text());
                }
                if (!_.isUndefined(originDeliveryCharge) && $('input[name="totalCouponDeliveryPrice"]').val() > originDeliveryCharge) {
                    var useTotalCouponDeliveryDcPrice = parseFloat(originDeliveryCharge);
                } else {
                    var useTotalCouponDeliveryDcPrice = parseFloat($('input[name="totalCouponDeliveryPrice"]').val());
                }
                $('input[name="totalCouponDeliveryDcPrice"]').val(useTotalCouponDeliveryDcPrice);
                $('#useDisplayCouponDelivery').text(numeral(useTotalCouponDeliveryDcPrice).format());
            } else {
                var useTotalCouponDeliveryDcPrice = 0;
            }

            // 실 결제금액
            realSettlePrice -= (useTotalCouponOrderDcPrice + useTotalCouponDeliveryDcPrice);
        }
<?php }?>

<?php if(gd_is_login()===true&&$TPL_VAR["mileageUse"]["payUsableFl"]!='n'){?>
        if (exceptMode != 'mileage') {
            // 구매자가 작성한 마일리지 금액
            if ($('input[name=\'useMileage\']').val() > 0) {
                var useMileage = parseInt($('input[name=\'useMileage\']').val());
            } else {
                var useMileage = 0;
            }

<?php if($TPL_VAR["mileageGiveExclude"]=='n'){?>
            // 적립마일리지 지급 정책 안보이게 처리
            if (useMileage > 0) {
                $('.js_mileage').hide();
            } else {
                $('.js_mileage').show();
            }
<?php }?>

            realSettlePrice -= useMileage;
        }
<?php }?>

<?php if(gd_is_login()===true&&$TPL_VAR["depositUse"]["payUsableFl"]!='n'){?>
        if (exceptMode != 'deposit') {
            // 구매자가 작성한 예치금 금액
            if ($('input[name=\'useDeposit\']').val() > 0) {
                var useDeposit = parseInt($('input[name=\'useDeposit\']').val());
            } else {
                var useDeposit = 0;
            }
            realSettlePrice -= useDeposit;
        }
<?php }?>

        // 금액 화면 출력
        if (_.isUndefined(exceptMode)) {
            $('#totalSettlePrice').html(gd_money_format(realSettlePrice));
            $('#totalAddSettlePrice').html(gd_add_money_format(realSettlePrice));
            $('#totalSettlePriceView').html(gd_money_format(realSettlePrice));
            $('#totalAddSettlePriceView').html(gd_add_money_format(realSettlePrice));
            $('input[name=settlePrice]').val(realSettlePrice);

            // 해외PG 최종승인 금액 및 통화 설정
            var settleKind = $('input[name=settleKind]:checked').val();
            if (!_.isUndefined(settleKind)) {
                if (settleKind.substring(0, 1) == 'o') {
                    var selectedOverseasSettleKind = $('[id=settlekind_overseas_' + settleKind + ']');
                    var overseasSettlePrice = fx.convert($('input[name=settlePrice]').val(), {to: selectedOverseasSettleKind.data('settle-currency')});
                    var overseasDecimal = selectedOverseasSettleKind.data('settle-decimal');
                    var overseasDecimalFormat = selectedOverseasSettleKind.data('settle-format');
                    $('#overseasSettelprice_' + settleKind).html(selectedOverseasSettleKind.data('settle-symbol') + ' ' + numeral(overseasSettlePrice.toRealFixed(overseasDecimal, overseasDecimalFormat)).format('0,' + overseasDecimalFormat));
                    $('input[name=overseasSettlePrice]').val(overseasSettlePrice.toRealFixed(overseasDecimal, overseasDecimalFormat));
                    $('input[name=overseasSettleCurrency]').val(selectedOverseasSettleKind.data('settle-currency'));
                }
            }

            // 금액이 0원이 되는 경우에 대한 처리
<?php if($TPL_VAR["receipt"]['taxZeroFl']=='y'){?>
            if (realSettlePrice == 0) {
                $('.payment_progress .js_pay_content').hide();
                gd_receipt_selector('zero');
            } else {
                $('.payment_progress .js_pay_content').show();
                gd_receipt_selector();
            }
<?php }else{?>
            if (realSettlePrice == 0) {
                $('.payment_progress .payment_progress_list').hide();
            } else {
                $('.payment_progress .payment_progress_list').show();
            }
<?php }?>
        }

        // 세금계산서 가능여부 노출 (세금정보 조건에 따라 실결제금액이 0원인 경우 세금계산서 발행 불가 처리)
        var taxRealSettlePrice = realSettlePrice;
        var taxMileageFl = '<?php echo $TPL_VAR["taxInfo"]["TaxMileageFl"]?>',
            taxDepositFl = '<?php echo $TPL_VAR["taxInfo"]["taxDepositFl"]?>',
            taxDeliveryFl = '<?php echo $TPL_VAR["taxInfo"]["taxDeliveryFl"]?>';

        if (taxMileageFl == 'y') {
            taxRealSettlePrice += numeral().unformat($('input[name="useMileage"]').val());
        }

        if (taxDepositFl == 'y') {
            taxRealSettlePrice += numeral().unformat($('input[name="useDeposit"]').val());
        }

        if (taxDeliveryFl == 'n') {
            taxRealSettlePrice -= numeral().unformat($('#totalDeliveryCharge').text());
        }

        if (taxRealSettlePrice <= 0) {
            $('#taxReceiptView').hide();
        } else {
            $('#taxReceiptView').show();
        }

        return realSettlePrice;
    }

<?php if(gd_is_login()===true&&$TPL_VAR["mileageUse"]["payUsableFl"]!='n'){?>
    function gd_mileage_disable_check(disableValue)
    {
        if(disableValue === 'y'){
            //disable 처리
            $('input[name=\'useMileage\'], #useMileageAll').closest('span').addClass('disabled');
            $('input[name=\'useMileage\'], #useMileageAll').attr('disabled', 'disabled');
        }
        else {
            //disable 해제
            $('input[name=\'useMileage\'], #useMileageAll').closest('span').removeClass('disabled');
            $('input[name=\'useMileage\'], #useMileageAll').attr('disabled', false);
        }
    }

    /**
     * 마일리지 안내문구 출력
     */
    function gd_mileage_info_write(message)
    {
        var prefixMessage = "※ ";
        $(".js-mileageInfoArea").html(prefixMessage + message);
    }

    /**
     * 마일리지 사용 제한 체크
     */
    function gd_mileage_use_check(setUseOption, setUseCheck, setUseCalculationFl, mileageInfoAreaFl)
    {
        mileageUse.minimumHold = parseInt(mileageUse.minimumHold);
        mileageUse.minimumLimit = parseInt(mileageUse.minimumLimit);
        mileageUse.orderAbleLimit = parseInt(mileageUse.orderAbleLimit);
        mileageUse.orderAbleStandardPrice = parseInt(mileageUse.orderAbleStandardPrice);
        mileageUse.maximumLimit = parseInt(mileageUse.maximumLimit);
        mileageUse.oriMaximumLimit = parseInt(mileageUse.oriMaximumLimit);

        // 회원 보유 마일리지
        var memMileage = parseInt('<?php echo $TPL_VAR["gMemberInfo"]["mileage"]?>');

        // 현재 결제 금액
        var realSettlePrice = gd_set_real_settle_price('mileage');
        // 배송비가 제외된 금액 (할인등은 포함되어 있는 상태)
        var goodsPrice = gd_get_goodsSalesPrice(realSettlePrice);

        // 배송비 포함 여부를 통해 비교 결제금액을 정의
        if(mileageUse.useDeliveryFl === 'n'){
            var realSettleDeliveryPrice = goodsPrice;
        }
        else {
            var realSettleDeliveryPrice = realSettlePrice;
        }

        // 실제 사용할 수 있는 최소 마일리지
        var realMinMileage = parseInt(Math.min(mileageUse.minimumLimit, realSettleDeliveryPrice));

        // 실제 사용 할 수 있는 최대 마일리지 ( ex: 배송비를 제외한 상품 함계급액이 2000원, 회원 마일리지 5000원일시 2000원 까지 사용 가능)
        var realMaxMileage = parseInt(Math.min(mileageUse.maximumLimit, realSettleDeliveryPrice, memMileage));

        // 마일리지 사용 불가능한 상태의 input 을 입력방지
        if(mileageUse.usableFl === 'n'){
            gd_mileage_disable_check('y');
        }
        else {
            gd_mileage_disable_check('n');
        }

        // 마일리지 전액 결제시 회원등급 추가 무통장 할인 처리
        if ($('input[name=totalMemberBankDcPrice]').val() > 0) {
            var allCheck = $('#useMileageAll:checked').val();
            var totalMemberBankDcPrice = parseInt($('input[name=totalMemberBankDcPrice]').val());
            var totalCouponGoodsDcPrice = $('input[name=totalCouponGoodsDcPrice]').val();
            var totalCouponOrderDcPrice = $('input[name=totalCouponOrderDcPrice]').val();
            if (allCheck == 'on') {
                if (totalCouponGoodsDcPrice > 0 || totalCouponOrderDcPrice > 0) {}
                else {
                    realMaxMileage += totalMemberBankDcPrice;
                    mileageUse.maximumLimit += totalMemberBankDcPrice;
                    $('input[name=\'useMileage\']').val(realMaxMileage);
                }
            } else {
                if (totalCouponGoodsDcPrice > 0) {}
                else if (totalCouponOrderDcPrice > 0) {
                    realMaxMileage += parseFloat('<?php echo $TPL_VAR["totalGoodsDcPrice"]?>') + parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]-$TPL_VAR["totalMemberBankDcPrice"]?>');
                    mileageUse.maximumLimit += parseFloat('<?php echo $TPL_VAR["totalGoodsDcPrice"]?>') + parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]-$TPL_VAR["totalMemberBankDcPrice"]?>');
                    $('input[name=\'useMileage\']').val(realMaxMileage);
                } else {
                    mileageUse.maximumLimit -= totalMemberBankDcPrice;
                    realMaxMileage = mileageUse.maximumLimit;
                }
                mileageInfoAreaFl = 'y';
            }
        }

        // *** 1. 보유 마일리지에 대한 제한조건 체크

        // 회원 보유 마일리지 체크
        if(memMileage < 1){
            gd_mileage_disable_check('y');
            return;
        }

        // 마일리지 사용설정 - 최소 보유마일리지 제한
        if(mileageUse.minimumHold > 0){
            // '회원 보유마일리지'가 '최소 보유마일리지 제한' 보다 작을 경우
            if(memMileage < mileageUse.minimumHold){
                if(mileageUse.minimumLimit <= mileageUse.minimumHold){
                    gd_mileage_info_write("<?php echo __('%s%s이상 보유해야 사용이 가능합니다.',gd_global_money_format($TPL_VAR["mileageUse"]["minimumHold"]),$TPL_VAR["mileage"]["unit"])?>");
                }
                else {
                    // '최소 사용마일리지 제한' > '최소 보유마일리지 제한' > 회원 보유 마일리지
                    gd_mileage_info_write("<?php echo __('최소 %s%s이상 사용해야 합니다.',gd_global_money_format($TPL_VAR["mileageUse"]["minimumLimit"]),$TPL_VAR["mileage"]["unit"])?>");
                }

                gd_mileage_disable_check('y');
                return;
            }
        }

        // 마일리지 사용설정 - 최소 사용마일리지 제한
        if(mileageUse.minimumLimit > 0){
            // '회원 마일리지' 보다 '최소 사용마일리지 제한' 보다 작을 경우
            if(memMileage < mileageUse.minimumLimit){
                if(mileageUse.minimumHold <= mileageUse.minimumLimit){
                    gd_mileage_info_write("<?php echo __('최소 %s%s이상 사용해야 합니다.',gd_global_money_format($TPL_VAR["mileageUse"]["minimumLimit"]),$TPL_VAR["mileage"]["unit"])?>");
                }
                else {
                    // '최소 보유마일리지 제한' > '최소 사용마일리지 제한' > 회원 보유 마일리지
                    gd_mileage_info_write("<?php echo __('%s%s이상 보유해야 사용이 가능합니다.',gd_global_money_format($TPL_VAR["mileageUse"]["minimumHold"]),$TPL_VAR["mileage"]["unit"])?>");
                }

                gd_mileage_disable_check('y');
                return;
            }
        }

        // *** 2. 상품 구매 금액에 대한 제한조건 체크

        // 마일리지 사용설정 - 최소 상품구매금액 제한 (구매금액 합계가 ? 이상인 경우 결제시 사용 가능)
        if(mileageUse.orderAbleLimit > 0){
            // orderAbleStandardPrice : 기본설정의 구매금액 기준 + 사용설정의 할인금액 미포함, 포함 가격이 적용된 기준
            if(mileageUse.orderAbleStandardPrice < mileageUse.orderAbleLimit){
                gd_mileage_info_write("<?php echo __('상품 합계 금액이 %s 이상인 경우에만 사용 가능합니다.',gd_global_currency_display($TPL_VAR["mileageUse"]["orderAbleLimit"]))?>");

                gd_mileage_disable_check('y');
                return;
            }
        }

        // 마일리지 사용설정 - 최소 사용마일리지 제한
        if(mileageUse.minimumLimit > 0){
            // 결제금액이 '최소 사용마일리지 제한' 보다 작을 경우
            if(realSettleDeliveryPrice < mileageUse.minimumLimit){
                var messageMaxMileage = memMileage;
                if(mileageUse.oriMaximumLimit > 0){
                    if(mileageUse.oriMaximumLimit > realSettleDeliveryPrice){
                        messageMaxMileage = Math.min(mileageUse.oriMaximumLimit, memMileage);
                    }
                }
                gd_mileage_info_write(__('%1$s%2$s부터 %3$s%4$s까지 사용 가능합니다.', [numeral(mileageUse.minimumLimit).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>', numeral(messageMaxMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']));

                gd_mileage_disable_check('y');
                return;
            }
        }

        // *** 3. 사용가능 마일리지 범위 정보 노출
        if (mileageInfoAreaFl == 'y') {
            if(realMinMileage > realMaxMileage){
                //최소 사용가능 마일리지가 최대 사용가능 마일리지보다 클때
                gd_mileage_info_write("<?php echo __('마일리지 사용조건이 충족되지 않아 사용이 불가합니다.')?>");
                gd_mileage_disable_check('y');
                return;
            }
            else if(realMinMileage === realMaxMileage){
                //최소 사용가능 마일리지가 최대 사용가능 마일리지와 같을때
                gd_mileage_info_write(__('%1$s%2$s만 사용 가능합니다.', [numeral(realMaxMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']), realMaxMileage);
                gd_mileage_disable_check('n');
            }
            else {
                //최소 사용가능 마일리지가 최대 사용가능 마일리지보다 작을때
                if(realMinMileage < 1){
                    gd_mileage_info_write(__('%1$s%2$s까지 사용 가능합니다.', [numeral(realMaxMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']), realMaxMileage);
                }
                else {
                    gd_mileage_info_write(__('%1$s%2$s부터 %3$s%4$s까지 사용 가능합니다.', [numeral(realMinMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>', numeral(realMaxMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']), realMaxMileage);
                }
                gd_mileage_disable_check('n');
            }
        }

        // *** 4. 사용가능 마일리지 범위 체크 및 결제금액 계산

        if(setUseOption === 'y'){
            var useMileage = parseInt($('input[name=\'useMileage\']').val());

            if(setUseCheck === 'y'){
                if(useMileage < realMinMileage){
                    gd_mileage_abort(__('%1$s 사용은 최소 %2$s%3$s입니다.', ['<?php echo $TPL_VAR["mileage"]["name"]?>', numeral(realMinMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']), realMinMileage);
                }
                if(useMileage > realMaxMileage){
                    gd_mileage_abort(__('%1$s 사용은 최대 %2$s%3$s입니다.', ['<?php echo $TPL_VAR["mileage"]["name"]?>', numeral(realMaxMileage).format(), '<?php echo $TPL_VAR["mileage"]["unit"]?>']), realMaxMileage);
                }
            }
            else {
                if(useMileage < realMinMileage){
                    $('input[name=\'useMileage\']').val(realMinMileage);
                }
                else if(useMileage > realMaxMileage){
                    $('input[name=\'useMileage\']').val(realMaxMileage);
                }
                else { }
            }

            if(setUseCalculationFl === 'y'){
                // 결제 금액 계산
                gd_set_recalculation();
                gd_set_real_settle_price();
            }
        }
    }

    /**
     * 마일리지를 잘못 입력한 경우 처리
     */
    function gd_mileage_abort(message, useMileage)
    {
        // 경고출력
        if (!_.isUndefined(message) && message !== null) {
            alert(message);
        }

        // 값 대입
        if (_.isUndefined(useMileage)) {
            $('input[name=\'useMileage\']').val('');
        } else {
            $('input[name=\'useMileage\']').val(useMileage);
        }
    }

    /**
     * 마일리지 전액 사용하기
     */
    function gd_mileage_use_all()
    {
        // 마일리지 쿠폰 중복사용 체크
        var checkMileageCoupon = gd_choose_mileage_coupon('mileage');
        if (!checkMileageCoupon) {
            return false;
        }
        var allCheck = $('#useMileageAll:checked').val();

        // 현재 결제 금액
        var realSettlePrice = gd_set_real_settle_price('mileage');
        if(mileageUse.useDeliveryFl === 'n'){
            // 마일리지 사용의 배송비 제외 설정에 따른 배송비 체크
            realSettlePrice = gd_get_goodsSalesPrice(realSettlePrice);
        }

        var memberMileage = parseInt("<?php echo $TPL_VAR["gMemberInfo"]["mileage"]?>");
        var checkMileage = Math.min(mileageUse.maximumLimit, realSettlePrice, memberMileage);

        if (allCheck == 'on') {
            $('input[name=\'useMileage\']').val(checkMileage);

            gd_mileage_use_check('y', 'y', 'y', 'y');
        }
        else {
            $('input[name=\'useMileage\']').val('');

            gd_set_recalculation();
            gd_set_real_settle_price();
        }
    }
<?php }?>

<?php if(gd_is_login()===true&&$TPL_VAR["depositUse"]['payUsableFl']!='n'){?>
    /**
     * 예치금 사용 제한 체크
     */
    function gd_deposit_use_check()
    {
        // 예치금 작성한 금액이 있는지 체크
        if ($('input[name=\'useDeposit\']').val() < 0) {
            return;
        }

        // 현재 결제 금액
        var realSettlePrice = gd_set_real_settle_price('deposit');
        var memberDeposit = parseInt(<?php echo $TPL_VAR["gMemberInfo"]["deposit"]?>);
        var ownDeposit = parseInt(<?php echo $TPL_VAR["gMemberInfo"]["deposit"]?>);
        var checkDeposit = memberDeposit;

        if (realSettlePrice < memberDeposit) {
            checkDeposit = realSettlePrice;
        }
        if (realSettlePrice > ownDeposit) {
            checkDeposit = ownDeposit;
        }

        // 구매자가 작성한 예치금 금액
        var useDeposit = parseInt($('input[name=\'useDeposit\']').val());

        // 예치금 사용 제한 체크
        if (useDeposit > checkDeposit) {
            $('input[name=\'useDeposit\']').val(checkDeposit);
        }

        // 결제 금액 계산
        gd_set_real_settle_price();
    }

    /**
     * 예치금 전액 사용하기
     */
    function deposit_use_all()
    {
        // 현재 결제 금액
        var realSettlePrice = gd_set_real_settle_price('deposit');
        var allCheck = $('#useDepositAll:checked').val();
        var memberDeposit = parseInt(<?php echo $TPL_VAR["gMemberInfo"]["deposit"]?>);
        var checkDeposit = memberDeposit;

        // 예치금 전액 결제시 회원등급 추가 무통장 할인 처리
        if ($('input[name=totalMemberBankDcPrice]').val() > 0) {
            // 배송비
<?php if(gd_is_login()===true&&$TPL_VAR["productCouponChangeLimitType"]=='n'){?>
            var deliveryPrice = parseFloat($('input[name=totalDeliveryCharge]').val());
<?php }else{?>
            var deliveryPrice = parseFloat('<?php echo $TPL_VAR["totalDeliveryCharge"]?>');
<?php }?>
<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
            deliveryPrice = parseFloat(multiShippingPrice(deliveryPrice));
<?php }?>

            // 상품 쿠폰 차감
<?php if(gd_is_login()===true&&$TPL_VAR["productCouponChangeLimitType"]=='n'){?>
            var goodsCouponDcPrice = parseFloat($('input[name=totalCouponGoodsDcPrice]').val());
<?php }else{?>
            var goodsCouponDcPrice = parseFloat('<?php echo $TPL_VAR["totalCouponGoodsDcPrice"]?>');
<?php }?>

            var settlePrice = parseFloat('<?php echo $TPL_VAR["totalGoodsPrice"]?>') + deliveryPrice;
            var totalDcPrice = parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]?>') + parseFloat('<?php echo $TPL_VAR["totalGoodsDcPrice"]?>') + goodsCouponDcPrice;
            var totalMemberBankDcPrice = parseFloat($('input[name=totalMemberBankDcPrice]').val());

            if (allCheck == 'on') {
                if (settlePrice - totalDcPrice == realSettlePrice) {
                    $('input[name="totalMemberDcPrice"]').val(parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]?>') - totalMemberBankDcPrice);
                    $('.total-member-dc-price').html(totalDcPrice - totalMemberBankDcPrice);
                    $('.member-dc-price').html(parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]?>') - totalMemberBankDcPrice);
                    $('#totalMemberBankDcPriceViewTop').hide();
                    $('#totalMemberBankDcPriceViewBottom').hide();
                    realSettlePrice += totalMemberBankDcPrice;
                }
            } else {
                $('input[name="totalMemberDcPrice"]').val(parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]?>'));
                $('.total-member-dc-price').html(totalDcPrice + totalMemberBankDcPrice);
                $('.member-dc-price').html(parseFloat('<?php echo $TPL_VAR["totalMemberDcPrice"]?>'));
                $('#totalMemberBankDcPriceViewTop').show();
                $('#totalMemberBankDcPriceViewBottom').show();
                realSettlePrice -= totalMemberBankDcPrice;
            }
        }

        if (realSettlePrice < memberDeposit) {
            checkDeposit = realSettlePrice;
        }

        if (allCheck == 'on') {
            $('input[name=\'useDeposit\']').val(checkDeposit);
        } else {
            $('input[name=\'useDeposit\']').val('');
        }

        // 결제 금액 계산
        gd_set_real_settle_price();
    }
<?php }?>

    /**
     * 결제 방법에 따른 결제 수단
     */
    function gd_settlekind_toggle()
    {
        // 초기 결제수단 처리
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
        var settleKind = $('#settlekind_overseas').find('input').first().val();
        $('#settlekind_overseas').find('input').first().prop('checked', true);
        $('label[for=settleKind_' + settleKind + ']').addClass('on')
<?php }else{?>
<?php if(isset($TPL_VAR["settle"]["payco"])){?>
        var settleKind = $('#settlekind_payco').find('input').first().val();
        gd_payco_toggle(settleKind);
<?php }else{?>
        var settleKind = $('#settlekind_general').find('input').first().val();
        $('#settlekind_general').find('input').first().prop('checked', true);
        $('label[for=settleKind_' + settleKind + ']').addClass('on')
<?php }?>
<?php }?>

    // 결제수단 선택
    gd_settlekind_selector(settleKind);
    }

    /**
     * 결제 수단을 초기화
     */
    function gd_payment_reset()
    {
        // 주문 채널을 기본 shop 으로 처리
        $('[name="orderChannelFl"]').val('shop');

    }

    /**
     * PAYCO 결제 클릭시
     */
    function gd_payco_toggle(settleKind)
    {
        // 초기화
        gd_payment_reset()

        // 주문 채널
        $('[name="orderChannelFl"]').val('payco');

        // 해당 주문 체크
        $('#settleKind_payco_' + settleKind).prop('checked', true).next('label').addClass('on');

        // 다른 주문 체크 해제
        $('label[for*=settleKind_][class="choice_s on"]').each(function(i, val) {
            $(this).removeClass('on');
        });


        // 결제방법 체크
        gd_settlekind_selector(settleKind);
    }

    /**
     * 결제수단 선택
     *
     * @param settleKind
     * @returns <?php echo $TPL_VAR["boolean"]?>

     */
    function gd_settlekind_selector(settleKind)
    {
        // 결제수단 값이 없는 경우 반드시 해당 settleKind를 radio 버튼에 checked 표기 해줘야 한다.
        // 만약 이부분 누락되면 결제수단 카드로 열리는 경우가 발생
        if (_.isUndefined(settleKind)) {
            settleKind = $('label[for*=settleKind_][class="choice_s on"]').prev('input[type=radio]').val();
            $('label[for*=settleKind_][class="choice_s on"]').prev('input[type=radio]').prop('checked', true);
        }

        // 결제수단 선택에 따른 입력 폼 전환
        $('[id*="settlekind_general_"]').hide();
        $('[id*="settlekind_overseas_"]').hide();

        if ($('[name="orderChannelFl"]').val() == 'shop') {
            // 일반 PG 안내 / 설정
            $('[id=settlekind_general_' + settleKind + ']').show();

            // 해외PG 최종승인 금액 및 통화 설정 (국가 변경시 배송비 실시간 적용 처리)
            if (settleKind.substring(0, 1) == 'o') {
                // 해외 PG 안내 / 설정
                $('[id=settlekind_overseas_' + settleKind + ']').show();
                gd_set_real_settle_price();
            }
        }

        // 영수증 선택 리셋
        gd_receipt_reset();

<?php if($TPL_VAR["receipt"]['cashFl']=='y'||$TPL_VAR["receipt"]['taxFl']=='y'){?>
        // 영수증 선택
        gd_receipt_selector();
<?php }?>
    }

    /**
     * 영수증 선택 리셋
     * - 우선 처리 모드에 따라서 영수증 종류 기본 선택 처리
     */
    function gd_receipt_reset()
    {
        // 선택된 결제 방법
        var strSettleKind = $('input[name=settleKind]:checked').val();

        // 현금영수증 (소득공제/지출증빙) 보이지 않도록 강제 처리
        $('input[name="receiptFl"]').prop('checked', false).next('label').removeClass('on');

        // 영수증 신청 안함 보이게 처리
        $('#nonReceiptView').show();

        // 사용 안함을 기본 체크 처리
        if (strSettleKind.substring(0, 1) != 'g' || '<?php echo gd_isset($TPL_VAR["receipt"]['aboveFl'],'n')?>' == 'n'){
        $('#receiptNon').eq(0).prop('checked', true).next('label').addClass('on');
    }

<?php if($TPL_VAR["receipt"]['aboveFl']=='r'||$TPL_VAR["receipt"]['aboveFl']=='t'){?>
        // 결제 방법의 코드가 일반 인 경우
        if (strSettleKind.substring(0, 1) == 'g') {
            // 영수증 신청 안함 안보이게 처리
            $('#nonReceiptView').hide();

<?php if($TPL_VAR["receipt"]['aboveFl']=='r'){?>
            // 현금영수증을 기본 체크 처리
            $('#receiptCash').eq(0).prop('checked', true).next('label').addClass('on');
<?php }?>
        }

<?php if($TPL_VAR["receipt"]['aboveFl']=='t'){?>
        // 세금계산서를 기본 체크 처리
        var useReceiptCode = <?php echo $TPL_VAR["receipt"]['useReceiptCode']?>;
        $.each(useReceiptCode, function(i, val) {
            if (strSettleKind == val) {
                $('#receiptNon').eq(0).prop('checked', false).next('label').removeClass('on');
                $('#receiptTax').eq(0).prop('checked', true).next('label').addClass('on');
                return false;
            }
        });
<?php }?>
<?php }?>
    }

    /**
     * 영수증 선택
     *
     * @param string mode 모드에 따른 처리 (null : 일반 처리, zero : 전액결제 처리)
     */
    function gd_receipt_selector(mode)
    {
<?php if($TPL_VAR["receipt"]['cashFl']=='y'||$TPL_VAR["receipt"]['taxFl']=='y'){?>
        var useReceiptCode = <?php echo $TPL_VAR["receipt"]['useReceiptCode']?>;
        var useCashReceiptCode = 'gb';
        var strSettleKind = $('input[name=settleKind]:checked').val();

        $('#receiptSelect').hide();
        if (typeof strSettleKind != 'undefined') {
            $.each(useReceiptCode, function(i, val) {
                if (strSettleKind == val) {
                    $('#receiptSelect').show();
                    return false;
                }
            });
        }

        // 전액 결제인 경우 영수증 항목 보이기
        if (mode == 'zero') {
            $('#receiptSelect').show();
        }

        // 영수증 관련 선택
        gd_receipt_display();

        // 현금 영수증 설정 (무통장입금인 경우에만 출력, 계좌이체와 가상계좌는 PG사 창에서 처리)
        if (strSettleKind == useCashReceiptCode) {
            $('#cashReceiptView').show();
            $('.cash_receipt_non').show();
            $('.cash_receipt_pg').hide();
        } else {
            $('#cashReceiptView').hide();
            $('.cash_receipt_non').hide();
            $('.cash_receipt_pg').show();
        }

        // 전액 결제인 경우 영수증 항목 보이기
        if(mode == 'zero') {
            $('#cashReceiptView').hide();
            $('.cash_receipt_non').show();
            $('.cash_receipt_pg').hide();
        }

<?php }?>
    }

<?php if($TPL_VAR["receipt"]['cashFl']=='y'||$TPL_VAR["receipt"]['taxFl']=='y'){?>
    /**
     * 영수증 관련 선택
     *
     * @param string clearChecker 해제 관련
     */
    function gd_receipt_display()
    {
        var useCode = {
            t: 'tax_info',
            r: 'cash_receipt_info'
        };
        var checkedBox = $('input[name=receiptFl]:checked');
        var target = eval('useCode.' + checkedBox.val());

        $('.js_receipt').addClass('dn');
        $('#' + target).removeClass('dn');

        if (checkedBox.val() == 'r') {
            gd_cash_receipt_toggle();
        }

<?php if($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["memberInvoiceInfo"]['tax']['memberTaxInfoFl']=='y'){?>
        $('.tax_info_init').addClass('dn');
        if (checkedBox.val() == 't') {
            $('.tax_info_init').removeClass('dn');
        }
<?php }?>
    }
<?php }?>

    /**
     * 현금영수증 인증방법 선택
     * (소득공제용 - 휴대폰 번호(c), 지출증빙용 - 사업자번호(b))
     */
    function gd_cash_receipt_toggle()
    {
<?php if($TPL_VAR["receipt"]['cashFl']=='y'){?>
        var certCode = $('input[name=\'cashUseFl\']:checked').val();
        $('label[for=cashCert_' + certCode + ']').addClass('on');

        if (certCode == 'd') {
            $('input[name=\'cashCertFl\']').val('c');
            $('#certNoHp').show();
            $('#certNoBno').hide();
        } else {
            $('input[name=\'cashCertFl\']').val('b');
            $('#certNoHp').hide();
            $('#certNoBno').show();
        }
<?php }?>
    }

    /**
     * 주문시 Exception 발생하는 경우 결제버튼 복원
     */
    function callback_not_ordable()
    {
        $('.order-buy').prop("disabled", false);
        $('.order-buy em').html("<?php echo __('결제하기')?>");
    }

    /**
     * validator onkeyup 함수
     * @param element
     * @param event
     * @returns <?php echo $TPL_VAR["boolean"]?>

     */
    function onkeyup(element, event) {
        if (check_key_code2(event)) {
            return true;
        }
        if ($.isFunction(replace_keyup[$(element).data('pattern')])) {
            replace_keyup[$(element).data('pattern')](element);
        }
    }

    var regexp_test = function (string, pattern) {
        if (string === undefined || string.length < 1) {
            return false;
        }
        return pattern.test(string);
    };

    var replace_pattern = function (string, pattern, c) {
        if (string === undefined || string.length < 1) {
            return '';
        }
        return string.replace(pattern, c);
    };

    var replace_keyup = {
        gdEngNum: function (element) {
            element.value = replace_pattern(element.value, /[^\da-zA-Z]/g, '');
        },
        gdEngKor: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣\u119E\u11A2]/g, '');
            }
        },
        gdEngKorNum: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣0-9\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣0-9\u119E\u11A2]/g, '');
            }

        },
        gdNum: function (element) {
            element.value = replace_pattern(element.value, /[^\d]/g, '');
        },
        gdEng: function (element) {
            element.value = replace_pattern(element.value, /[^a-zA-Z]/g, '');
        },
        gdKor: function (element) {
            if (regexp_test(element.value, /[^가-힣ㄱ-ㅎㅏ-ㅣ\u119E\u11A2]/)) {
                element.value = replace_pattern(element.value, /[^가-힣ㄱ-ㅎㅏ-ㅣ\u119E\u11A2]/g, '');
            }
        },
        gdMemberId: function (element) {
            element.value = replace_pattern(element.value, /[^\da-zA-Z\.\-_@]/g, '');
        },
        gdMemberNmGlobal: function (element) {
            // IE11에서 초중종성 분리되는 현상때문에 test 후 replace 처리로 변경
            if (regexp_test(element.value, /[\/\'\"\\\|]/)) {
                element.value = replace_pattern(element.value, /[\/\'\"\\\|]/g, '');
            }
        }
    };

    /**
     * jquery validation의 키 체크 함수
     * @param event
     * @returns <?php echo $TPL_VAR["boolean"]?>

     */
    function check_key_code2(event) {
        // Avoid revalidate the field when pressing one of the following keys
        /* Shift       => 16 Ctrl        => 17 Alt         => 18
         Caps lock   => 20 End         => 35 Home        => 36
         Left arrow  => 37 Up arrow    => 38 Right arrow => 39
         Down arrow  => 40 Insert      => 45 Num lock    => 144 AltGr key   => 225*/
        var excludedKeys = [
            16, 17, 18, 20, 35, 36, 37,
            38, 39, 40, 45, 144, 225
        ];

        return event.which === 9 || $.inArray(event.keyCode, excludedKeys) !== -1;
    }

    function gd_reflect_apply_delivery(mode) {
        switch (mode) {
            case 'shippingBasic':
                $('input[name="reflectApplyDelivery"]').prop('checked', false).closest('div').hide();
                break;
            default:
                $('input[name="reflectApplyDelivery"]').closest('div').show();
                break;
        }

        gd_display_memberinfo_apply();
    }

    /**
     * 마일리지 쿠폰 중복사용 체크
     */
    function gd_choose_mileage_coupon(type) {
        if (type == undefined) {
            return false;
        }

        // 마일리지 쿠폰 중복사용 체크
        if ($('input[name=chooseMileageCoupon]').length > 0) {
            if ($('input[name=chooseMileageCoupon]').val() == 'y') {
                if (type == 'mileage') {
                    // 마일리지 입력시 체크
                    if ($('input[name=totalCouponGoodsDcPrice]').length > 0 && $('input[name=totalCouponGoodsMileage]').length > 0) {
                        var totalCouponGoodsDcPrice = $('input[name=totalCouponGoodsDcPrice]').val();
                        var totalCouponGoodsMileage = $('input[name=totalCouponGoodsMileage]').val();

                        if (totalCouponGoodsDcPrice > 0 || totalCouponGoodsMileage > 0 || ($('input[name=couponApplyOrderNo]').val() != '' && $('input[name=couponApplyOrderNo]').length > 0)) {
                            alert('마일리지와 쿠폰은 동시에 사용하실 수 없습니다.');
                            $('input[name=useMileage]').val('');
                            $("#useMileageAll").attr('checked', false);
                            $('label[for=useMileageAll]').removeClass('on');
                            return false;
                        }
                    }
                } else {
                    // 쿠폰사용 클릭시 체크
                    if ($('input[name=useMileage]').val() != '' && $('input[name=useMileage]').val() != 0) {
                        alert('마일리지와 쿠폰은 동시에 사용하실 수 없습니다.');
                        return false;
                    }
                }
            }
        }

        return true;
    }

    // 주문서 변경에 따른 상품 금액 정보 변경 처리
    function gd_set_recalculation()
    {
        // 마일리지 사용시 / 주문쿠폰 적용시 재계산
        var cartIdx = [];
        $('input[name="cartSno[]"]').each(function(idx){
            cartIdx.push($(this).val());
        });
        $.ajax({
            method: "POST",
            data: {
                'mode': 'set_recalculation',
                'cartIdx': cartIdx,
                'totalCouponOrderDcPrice': $('input:hidden[name="totalCouponOrderDcPrice"]').val(),
                'deliveryFree': $('input:hidden[name="deliveryFree"]').val(),
                'useMileage': $('input[name="useMileage"]').val(),
                'totalDeliveryCharge' : $('input[name=totalDeliveryCharge]').val(),
                'deliveryAreaCharge' : $('input[name=deliveryAreaCharge]').val(),
                'resetMemberBankDcPrice' : (<?php echo $TPL_VAR["totalMemberBankDcPrice"]?> > 0 && $('#useMileageAll:checked').val() == 'on') ? 'y' : 'n'
            },
            cache: false,
            async: false,
            url: "../order/order_ps.php",
            success: function (data) {
                if (data) {
                    if (data.totalMemberBankDcPrice > 0) {
                        // 마일리지 전액 결제시 회원등급 추가 무통장 할인 처리
                        var allCheck = $('#useMileageAll:checked').val();
                        var totalCouponGoodsDcPrice = $('input[name=totalCouponGoodsDcPrice]').val();
                        var totalCouponOrderDcPrice = $('input[name=totalCouponOrderDcPrice]').val();
                        if (allCheck == 'on') {
                            if (totalCouponGoodsDcPrice > 0 || totalCouponOrderDcPrice > 0) {}
                            else {
                                data.totalMemberDcPrice -= data.totalMemberBankDcPrice;
                                $('#totalMemberBankDcPriceViewTop').hide();
                                $('#totalMemberBankDcPriceViewBottom').hide();
                                $('input[name=resetMemberBankDcPrice]').val('y');
                                gd_mileage_use_check('n', 'n', 'n', 'y');
                            }
                        } else {
                            if (totalCouponGoodsDcPrice > 0 || totalCouponOrderDcPrice > 0) {}
                            else {
                                $('#totalMemberBankDcPriceViewTop').show();
                                $('#totalMemberBankDcPriceViewBottom').show();
                                $('.member-dc-price').html(data.totalMemberDcPrice + data.totalMemberBankDcPrice);
                                $('input[name=resetMemberBankDcPrice]').val('n');
                                gd_mileage_use_check('n', 'n', 'n', 'y');
                            }
                        }
                        $('#totalMemberBankDcPriceViewTop').show();
                        $('#totalMemberBankDcPriceViewBottom').show();

                        // 주문쿠폰 사용시 상품할인 금액이 0 일 경우  마일리지 사용 disable 처리
                        if ($('input[name="resetMemberBankDcPrice"]').val() == 'y') {
                            mileage_disable_check('y');
                        }
                    } else {
                        $('#totalMemberBankDcPriceViewTop').hide();
                        $('#totalMemberBankDcPriceViewBottom').hide();

                        // 상품 쿠폰사용시 상품할인 금액이 0 일 경우 마일리지 사용 disable 처리
                        if (data.totalCouponGoodsDcPrice + data.totalMemberDcPrice + data.totalGoodsDcPrice == data.totalGoodsPrice) {
                            mileage_disable_check('y');
                        }
                    }

                    var totalGoodsDcPrice = data.totalGoodsDcPrice;
                    var totalSumMemberDcPrice = data.totalMemberDcPrice + data.totalMemberOverlapDcPrice;
                    var totalCouponGoodsDcPrice = data.totalCouponGoodsDcPrice;
                    var totalMemberMileage = data.totalMemberMileage;
                    var totalGoodsMileage = data.totalGoodsMileage;
                    var totalMileage = data.totalMileage;

                    var totalMemberDcPrice = totalGoodsDcPrice + totalSumMemberDcPrice + totalCouponGoodsDcPrice;

                    $('input[name=totalMemberDcPrice]').val(data.totalMemberDcPrice);
                    $('input[name=totalMemberBankDcPrice]').val(data.totalMemberBankDcPrice);
                    $('input[name=totalMemberOverlapDcPrice]').val(data.totalMemberOverlapDcPrice);
                    $('input[name=totalMemberMileage]').val(data.totalMemberMileage);
                    $('input[name=totalCouponGoodsDcPrice]').val(totalCouponGoodsDcPrice);

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                    totalSumMemberDcPrice = gd_money_format(totalSumMemberDcPrice);
                    totalMemberDcPrice = gd_money_format(totalMemberDcPrice);

                    var totalMemberDcPriceAdd = data.totalMemberDcPriceAdd;

                    totalMileage = gd_money_format(totalMileage);
                    totalGoodsMileage = gd_money_format(totalGoodsMileage);
                    totalMemberMileage = gd_money_format(totalMemberMileage);

                    $('.total-member-dc-price').html(totalMemberDcPrice);
                    $('.total-member-dc-price-add').html(totalMemberDcPriceAdd);
                    $('.member-dc-price').html(totalSumMemberDcPrice);
                    $('.total-member-mileage').html(totalMileage);
                    $('.goods-mileage').html(totalGoodsMileage);
                    $('.member-mileage').html(totalMemberMileage);
<?php }else{?>

                    $('.total-member-dc-price').html(numeral(totalMemberDcPrice).format());
                    $('.member-dc-price').html(numeral(totalSumMemberDcPrice).format());
                    $('.total-member-mileage').html(numeral(totalMileage).format());
                    $('.goods-mileage').html(numeral(totalGoodsMileage).format());
                    $('.member-mileage').html(numeral(totalMemberMileage).format());
<?php }?>

<?php if(gd_is_login()===true&&$TPL_VAR["productCouponChangeLimitType"]=='n'){?>
                    var totalDeliveryCharge = data.totalDeliveryCharge;
                    $('.goods-coupon-dc-price').html(numeral(data.totalCouponGoodsDcPrice).format());
                    $('.goods-coupon-add-mileage').html(numeral(data.totalCouponGoodsMileage).format());

<?php if($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                    $('.goods-coupon-dc-price-without-member').html(numeral(data.totalCouponGoodsDcPrice).format());
                    $('.goods-coupon-add-mileage-without-member').html(numeral(data.totalCouponGoodsMileage).format());
                    $('.total-member-dc-price-without-member').html(numeral(data.totalGoodsDcPrice + data.totalCouponGoodsDcPrice).format());
                    $('.total-member-mileage-without-member').html(numeral(totalMileage).format());
<?php }?>

<?php if($TPL_VAR["deliveryFree"]!='y'){?>
                    $('input[name=totalDeliveryCharge]').val(totalDeliveryCharge);
                    $('#totalDeliveryCharge').html(numeral(parseInt(totalDeliveryCharge)).format());
<?php }?>
<?php }?>

<?php if(gd_is_login()===true&&$TPL_VAR["mileageUse"]["payUsableFl"]!='n'){?>
                    mileageUse = data.mileageUse;
<?php }?>
                }
            }
        });
    }

    /**
     * 결제금액에서 상품금액만 구하기 (배송비 제외)
     * @param realSettlePrice
     * @returns {number|*}
     */
    function gd_get_goodsSalesPrice(realSettlePrice)
    {
        var deliveryFreePrice = parseInt($('input[name="totalDeliveryFreePrice"]').val());
        var deliveryPrice = 0;
        if (deliveryFreePrice > 0) {
            var deliveryAreaPrice = parseInt($('input[name="deliveryAreaCharge"]').val());
            var deliveryDcPrice = parseInt($('input[name="totalCouponDeliveryDcPrice"]').val());
            if (deliveryAreaPrice > 0) {
                deliveryPrice = deliveryPrice + deliveryAreaPrice;
            }
            if (deliveryDcPrice > 0) {
                deliveryPrice = deliveryPrice - deliveryDcPrice;
            }
        } else {
            var deliveryBasicPrice = parseInt($('input[name="totalDeliveryCharge"]').val());
            var deliveryAreaPrice = parseInt($('input[name="deliveryAreaCharge"]').val());
            var deliveryDcPrice = parseInt($('input[name="totalCouponDeliveryDcPrice"]').val());
            if (deliveryBasicPrice > 0) {
                deliveryPrice = deliveryPrice + deliveryBasicPrice;
            }
            if (deliveryAreaPrice > 0) {
                deliveryPrice = deliveryPrice + deliveryAreaPrice;
            }
            if (deliveryDcPrice > 0) {
                deliveryPrice = deliveryPrice - deliveryDcPrice;
            }
        }

        realSettlePrice = realSettlePrice - deliveryPrice;

        return realSettlePrice;
    }

    /**
     * 결제 페이지 호출 확인 후 결제
     */
    function pgSettleStartAfterCheck() {
        if (typeof pgSettleStart === 'function') {
            pgSettleStart();
        } else {
            console.log('pgSettle fail');
            return false;
        }
    }

<?php if(($TPL_VAR["authData"]['useFl']=='y'&&$TPL_VAR["authData"]['useDataModifyFl']=='y')||($TPL_VAR["authData"]['useFlKcp']=='y'&&$TPL_VAR["authData"]['useDataModifyFlKcp']=='y')){?>
    $('input[name="reflectApplyMember"]').prop('checked', false).closest('div').hide();
    gd_display_memberinfo_apply();
<?php }?>

    function gd_display_memberinfo_apply() {
        if ($('#memberinfoApplyTr1').css('display') == 'none' && $('#memberinfoApplyTr2').css('display') == 'none') {
            $('#memberinfoApplyTr').css('display', 'none');
        } else {
            //$('#memberinfoApplyTr').css('display', '');
        }
    }

<?php if($TPL_VAR["isUseMultiShipping"]===true){?>
    var multiShippingPrice = function(deliveryPrice){
        if ($('input[name="multiShippingFl"]').prop('checked') === true) {
            var multiDelivery = 0;
            var multiShippingText = [];
            $('input[name^="multiDelivery"]').each(function(index){
                var value = $(this).val() ? $(this).val() : 0;
                var html = __('추가 배송지') + index + ' : ';
                if (index <= 0) {
                    html = __('메인 배송지') + ' : ';
                }
                multiShippingText.push(html + '<?php echo gd_global_currency_symbol()?>' + numeral(parseInt(value)).format() + '<?php echo gd_global_currency_string()?>');
                multiDelivery += parseInt(value);
            });
            $('.multi_shipping_text').html('(' + multiShippingText.join(', ') + ')');
        } else {
            $('.multi_shipping_text').empty();
            multiDelivery = deliveryPrice;
        }
        $('#totalDeliveryCharge').html(numeral(parseInt(multiDelivery)).format());
        $('input[name="totalDeliveryCharge"]').val(multiDelivery);

        return multiDelivery;
    }
<?php }?>

    var resetMileage = function() {
        var cartIdx = [];
        $('input[name="cartSno[]"]').each(function(idx){
            cartIdx.push($(this).val());
        });
        var param = {
            mode: 'set_mileage',
            cartSno: cartIdx,
            totalDeliveryCharge : $('input[name=totalDeliveryCharge]').val(),
            deliveryAreaCharge : $('input[name=deliveryAreaCharge]').val(),
            totalCouponOrderDcPrice: $('input:hidden[name="totalCouponOrderDcPrice"]').val(),
        };
        $.post('cart_ps.php', param, function(data){
            mileageUse = data.mileageUse;

            gd_mileage_use_check('n', 'n', 'n', 'n');
        });
    }
    //-->
</script>

<?php $this->print_("footer",$TPL_SCP,1);?>