<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/order/order_end.html 000031812 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<link href="/data/skin/front/aileenwedding/css/order.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <div class="top_layer">
            <ul class="tab_area inline">
                <li class="on">
                    <a href="#" onclick="return false"><?php echo __('주문완료')?></a>
                    <span class="underline"></span>
                </li>
            </ul><!-- .tab_area -->
        </div><!-- .top_layer -->   
        <div class="section_layer content_box">
            <div class="order_wrap">
                <div class="order_tit">
                    <ol>
                        <li>
                            <span>01</span> <?php echo __('장바구니')?>

                        </li><li class="separate"><i class="xi-angle-right"></i></li><li>
                            <span>02</span> <?php echo __('주문서작성/결제')?>

                        </li><li class="page_on separate"><i class="xi-angle-right"></i></li><li class="page_on">
                            <span>03</span> <?php echo __('주문완료')?>

                        </li>
                    </ol>
                </div><!-- //order_tit -->
                <!-- //member_tit -->
                <div class="order_cont">
                    <div class="order_end">
        
<?php if(gd_isset($TPL_VAR["orderInfo"]["orderStatus"])=='o'){?>
                        <!-- 주문 접수 완료 -->
                        <div class="order_end_completion">
                            <span><i class="xi-check-circle"></i></span>
                            <p><strong><?php echo __('주문이 정상적으로 접수 되었습니다.')?></strong><br /><em><?php echo __('감사합니다.')?></em></p>
                        </div>
<?php }elseif($TPL_VAR["orderInfo"]["orderStatus"]=='p'){?>
                        <!-- 결제 완료 -->
                        <div class="order_end_completion">
                            <span><i class="xi-check-circle"></i></span>
                            <p><strong><?php echo __('결제가 완료 되었습니다.')?></strong><br /><em><?php echo __('감사합니다.')?></em></p>
                        </div>
<?php }elseif($TPL_VAR["orderInfo"]["orderStatus"]=='f'){?>
                        <!-- 결제 실패 -->
                        <div class="order_end_completion">
                            <span><i class="xi-check-circle"></i></span>
                            <p>
<?php if(gd_pg_result_recheck($TPL_VAR["orderInfo"]["pgName"])===true){?>
                                <strong><?php echo __('결제가 정상적으로 이루어지지 않았습니다. [결제확인] 버튼을 클릭하여 정상여부를 다시 확인해주시기 바랍니다.')?></strong><br /><em><?php echo __('지속적으로 문제가 발생될 경우 관리자에게 문의 하시기 바랍니다.')?></em>
<?php }else{?>
                                <strong><?php echo __('결제가 정상적으로 이루어지지 않았습니다. 다시 결제 진행을 해주시기 바랍니다.')?></strong><br /><em><?php echo __('지속적으로 문제가 발생될 경우 관리자에게 문의 하시기 바랍니다.')?></em>
<?php }?>
                            </p>
                        </div>
<?php }elseif(empty($TPL_VAR["orderInfo"]["orderStatus"])===false){?>
                        <!-- 재접속 등에 의한 기주문 완료 건으로 접속시 -->
                        <div class="order_end_completion">
                            <span><i class="xi-check-circle"></i></span>
                            <p><strong><?php echo __('이미 결제가 완료된 주문입니다.')?></strong><br /><em><?php echo __('감사합니다.')?></em></p>
                        </div>
<?php }else{?>
                        <!-- 재접속 등에 의한 기주문 완료 건으로 접속시 -->
                        <div class="order_end_completion">
                            <span><i class="xi-check-circle"></i></span>
                            <p><strong><?php echo __('주문 정보가 없습니다. 다시 확인 바랍니다.')?></strong><br /><em><?php echo __('지속적으로 문제가 발생될 경우 관리자에게 문의 하시기 바랍니다.')?></em></p>
                        </div>
<?php }?>
        
<?php if(empty($TPL_VAR["orderInfo"]["orderStatus"])===false){?>
                        <div class="order_zone_tit">
                            <h4><?php echo __('주문요약정보')?></h4>
                        </div>
        
                        <div class="order_table_type">
                            <table class="table_left table no_header">
                                <colgroup>
                                    <col style="width:15%;">
                                    <col style="width:85%;">
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th><?php echo __('결제수단')?></th>
                                    <td><div class="pay_with_list">
                                        <strong><?php if(gd_isset($TPL_VAR["orderInfo"]["settleGateway"])=='e'){?><?php echo __('에스크로')?><?php }?><?php echo gd_isset($TPL_VAR["orderInfo"]["settleName"])?></strong>
                                        <ul>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleGateway"])=='f'){?>
                                            <!--작업자는 간편결제 내용을 작성해주세요.-->
<?php }else{?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleKind"])=='gb'){?>
                                            <li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 0])?></li>
                                            <li><?php echo __('입금계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 1])?></li>
                                            <li><?php echo __('예금주명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 2])?></li>
                                            <li><?php echo __('입금금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
                                            <li><?php echo __('입금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankSender"])?></li>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='c'){?>
                                            <li><?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
                                            <li><?php echo __('결제금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='b'){?>
                                            <li><?php echo __('이체은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
                                            <li><?php echo __('결제금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='v'){?>
                                            <li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
                                            <li><?php echo __('가상계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 1])?></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])!=''){?>
                                            <li><?php echo __('예금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])?></li>
<?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
                                            <li><?php echo __('송금일자')?> : <?php echo gd_date_format('Y-m-d',gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0]))?> <?php echo __('까지')?></li>
<?php }?>
                                            <li><?php echo __('입금금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='h'){?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])!=''){?>
                                            <li><?php echo __('통신사')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
                                            <li><?php echo __('결제 휴대폰 번호')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])?></li>
<?php }?>
                                            <li><?php echo __('결제금액')?> : <strong class="deposit_money"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong></li>
<?php }?>
<?php }?>
<?php }?>
                                        </ul>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo __('주문번호')?></th>
                                    <td><?php echo $TPL_VAR["orderInfo"]["orderNo"]?></td>
                                </tr>
<?php if($TPL_VAR["orderInfo"]["orderStatus"]=='f'){?>
                                <tr>
                                    <th><?php echo __('결제실패사유')?></th>
                                    <td><?php echo gd_isset($TPL_VAR["orderInfo"]["pgFailReason"])?></td>
                                </tr>
<?php if(gd_pg_result_recheck($TPL_VAR["orderInfo"]["pgName"])===true){?>
                                <tr>
                                    <th><?php echo __('결제확인')?></th>
                                    <td><a href="../payment/<?php echo $TPL_VAR["orderInfo"]["pgName"]?>/pg_result_recheck.php?orderNo=<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>" class="normal-btn small2"><em><?php echo __('결제확인')?></em></a></td>
                                </tr>
<?php }?>
<?php }else{?>
                                <tr>
                                    <th><?php echo __('주문일자')?></th>
                                    <td><?php echo $TPL_VAR["orderInfo"]["regDt"]?></td>
                                </tr>
                                <tr>
                                    <th><?php echo __('주문상품')?></th>
                                    <td><?php echo $TPL_VAR["orderInfo"]["orderGoodsNm"]?></td>
                                </tr>
                                <tr>
                                    <th><?php echo __('주문자명')?></th>
                                    <td><?php echo $TPL_VAR["orderInfo"]["orderName"]?></td>
                                </tr>
                                <tr>
                                    <th><?php echo __('배송정보')?></th>
                                    <td>
<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'){?>
                                        <div class="order_table_type">
                                            <table class="table_left">
                                                <colgroup>
                                                    <col style="width:15%;">
                                                    <col style="width:85%;">
                                                </colgroup>
                                                <tr>
                                                    <th><?php echo __('메인 배송지')?></th>
                                                    <td>
                                                        <p>
                                                            <strong><?php echo gd_isset($TPL_VAR["orderInfo"]["receiverName"])?></strong> <br />
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                            [<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverZonecode"])?>] <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddressSub"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddress"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverState"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCity"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCountry"])?><br />
                                                            (+<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhonePrefix"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhone"])?>  / (+<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhonePrefix"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhone"])?><br />
                                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["orderMemo"])?>

<?php }else{?>
                                                            [<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverZonecode"])?>] <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddress"])?> <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddressSub"])?><br />
                                                            <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhone"])?>  /  <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhone"])?><?php if($TPL_VAR["orderInfo"]["receiverUseSafeNumberFl"]=='w'||$TPL_VAR["orderInfo"]["receiverUseSafeNumberFl"]=='y'){?>&nbsp;(<?php echo __('안심번호 사용')?>)<?php }?><br />
                                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["orderMemo"])?>

<?php }?>
                                                        </p>
                                                    </td>
                                                </tr>
<?php if($TPL_VAR["multiOrderInfo"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["multiOrderInfo"]["receiverNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                                                <tr>
                                                    <th><?php echo __('추가 배송지')?><?php echo $TPL_I1+ 1?></th>
                                                    <td>
                                                        <p>
                                                            <strong><?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverNm"][$TPL_I1])?></strong> <br />
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                            [<?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1])?>] <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1])?>, <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1])?>, <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverState"][$TPL_I1])?>, <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverCity"][$TPL_I1])?>, <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverCountry"][$TPL_I1])?><br />
                                                            (+<?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverPhonePrefix"][$TPL_I1])?>) <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverPhone"][$TPL_I1])?>  / (+<?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverCellPhonePrefix"][$TPL_I1])?>) <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverCellPhone"][$TPL_I1])?><br />
                                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["orderMemo"][$TPL_I1])?>

<?php }else{?>
                                                            [<?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1])?>] <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1])?> <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1])?><br />
                                                            <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverPhone"][$TPL_I1])?>  /  <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["receiverCellPhone"][$TPL_I1])?><?php if($TPL_VAR["multiOrderInfo"]["receiverUseSafeNumberFl"][$TPL_I1]=='w'||$TPL_VAR["multiOrderInfo"]["receiverUseSafeNumberFl"][$TPL_I1]=='y'){?>&nbsp;(<?php echo __('안심번호 사용')?>)<?php }?><br />
                                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["multiOrderInfo"]["orderMemo"][$TPL_I1])?>

<?php }?>
                                                        </p>
                                                    </td>
                                                </tr>
<?php }}?>
<?php }?>
                                            </table>
                                        </div>
<?php }else{?>
                                        <p>
                                            <strong><?php echo gd_isset($TPL_VAR["orderInfo"]["receiverName"])?></strong> <br />
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                            [<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverZonecode"])?>] <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddressSub"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddress"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverState"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCity"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCountry"])?><br />
                                            (+<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhonePrefix"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhone"])?>  / (+<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhonePrefix"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhone"])?><br />
                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["orderMemo"])?>

<?php }else{?>
                                            [<?php echo gd_isset($TPL_VAR["orderInfo"]["receiverZonecode"])?>] <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddress"])?> <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverAddressSub"])?><br />
                                            <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhone"])?>  /  <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverPhone"])?>  /  <?php echo gd_isset($TPL_VAR["orderInfo"]["receiverCellPhone"])?><?php if($TPL_VAR["orderInfo"]["receiverUseSafeNumberFl"]=='w'||$TPL_VAR["orderInfo"]["receiverUseSafeNumberFl"]=='y'){?>&nbsp;(<?php echo __('안심번호 사용')?>)<?php }?><br />
                                            <?php echo __('남기실 말씀')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["orderMemo"])?>

<?php }?>
                                        </p>
<?php }?>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo __('상품 금액')?></th>
                                    <td><strong class="order_payment_sum"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["totalGoodsPrice"]))?></strong>
                                        <span class="add_currency"><?php echo gd_global_add_currency_display(gd_isset($TPL_VAR["orderInfo"]["totalGoodsPrice"]))?></span>
                                    </td>
                                </tr>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                <tr>
                                    <th><?php echo __('총 무게')?></th>
                                    <td><strong><?php echo number_format($TPL_VAR["orderInfo"]["totalDeliveryWeight"], 2)?>kg</strong></td>
                                </tr>
<?php }?>
                                <tr>
                                    <th><?php echo __('배송비')?></th>
                                    <td><?php echo __('기본배송')?> <?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderDeliveryInfo"]["deliveryPolicyCharge"]))?>

                                        <span class="add_currency"><?php echo gd_global_add_currency_display(gd_isset($TPL_VAR["orderDeliveryInfo"]["deliveryPolicyCharge"]))?></span>
<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'){?>
<?php if($TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"]){?>
                                        (<?php if((is_array($TPL_R1=$TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?><?php echo __('메인 배송지')?><?php }else{?><?php echo __('추가 배송지')?><?php echo $TPL_I1?><?php }?> : <?php echo gd_global_currency_display(gd_isset($TPL_V1))?>

<?php }}?>)
<?php }?>
<?php }?>
                                    </td>
                                </tr>
<?php if($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]> 0){?>
                                <tr>
                                    <th><?php echo __('지역별 배송비')?></th>
                                    <td><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]))?></td>
                                </tr>
<?php }?>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                <tr>
                                    <th><?php echo __('해외배송 보험료')?></th>
                                    <td>
                                        <strong><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["totalDeliveryInsuranceFee"]))?></strong>
<?php if($TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
                                        <?php echo gd_global_add_currency_symbol()?><span class="add_currency"><?php echo gd_global_add_money_format(gd_isset($TPL_VAR["orderInfo"]["totalDeliveryInsuranceFee"]))?></span><?php echo gd_global_add_currency_string()?>

<?php }?>
                                    </td>
        
                                </tr>
<?php }?>
<?php if($TPL_VAR["depositUse"]["payUsableFl"]==='y'&&$TPL_VAR["orderInfo"]["useDeposit"]> 0){?>
                                <tr>
                                    <th><?php echo __('사용')?> <?php echo $TPL_VAR["depositUse"]["name"]?></th>
                                    <td><strong><?php echo gd_money_format($TPL_VAR["orderInfo"]["useDeposit"])?><?php echo $TPL_VAR["depositUse"]["unit"]?></strong></td>
                                </tr>
<?php }?>
<?php if($TPL_VAR["mileage"]["useFl"]==='y'&&$TPL_VAR["orderInfo"]["useMileage"]> 0){?>
                                <tr>
                                    <th><?php echo __('사용')?> <?php echo $TPL_VAR["mileage"]["name"]?></th>
                                    <td><strong><?php echo gd_money_format($TPL_VAR["orderInfo"]["useMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong></td>
                                </tr>
<?php }?>
                                <tr>
                                    <th><?php echo __('할인 및 적립')?></th>
                                    <td>
                                        <ul class="order_benefit_list">
                                            <li class="order_benefit_sale">
                                                <em><?php echo __('할인')?> : <strong>(-) <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalGoodsDcPrice"]+($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberDeliveryDcPrice"])?></strong>
                                                    <span>(
                                                    <?php echo __('상품')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalGoodsDcPrice"])?>

<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                                                    , <?php echo __('회원')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])?>

                                                    , <?php echo __('배송비')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalMemberDeliveryDcPrice"])?>

                                                    , <?php echo __('상품쿠폰')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"])?>

                                                    , <?php echo __('주문쿠폰')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"])?>

                                                    , <?php echo __('배송비쿠폰')?> <?php echo gd_global_currency_display($TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"])?>

<?php }?>
                                                    )</span>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["gGlobal"]["useAddCurrency"]){?>
<?php if(($TPL_VAR["orderInfo"]["totalGoodsDcPrice"]+($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberDeliveryDcPrice"])!= 0){?><?php echo gd_global_add_currency_symbol()?><?php }?><span class="add_currency" style="color: #717171;"><?php echo gd_global_add_money_format($TPL_VAR["orderInfo"]["totalGoodsDcPrice"]+($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberDeliveryDcPrice"])?></span>
<?php }?>
                                                </em>
                                            </li>
<?php if($TPL_VAR["mileage"]["useFl"]==='y'){?>
                                            <li class="order_benefit_mileage">
<?php if($TPL_VAR["orderInfo"]["mileageGiveExclude"]==='n'&&$TPL_VAR["orderInfo"]["useMileage"]> 0){?>
                                                <em><?php echo __('적립')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <?php echo __('마일리지 지급정책에 의거하여 마일리지를 사용한 주문 건에 대해 적립마일리지를 지급하지 않습니다.')?></em>
<?php }else{?>
                                                <em> <?php echo __('적립')?> <?php echo $TPL_VAR["mileage"]["name"]?> : <strong>(+) <?php echo gd_money_format($TPL_VAR["orderInfo"]["totalGoodsMileage"]+$TPL_VAR["orderInfo"]["totalMemberMileage"]+$TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]+$TPL_VAR["orderInfo"]["totalCouponOrderMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?></strong>
                                                    <span>(
<?php if($TPL_VAR["orderInfo"]["totalGoodsMileage"]> 0){?><?php echo __('상품')?> <?php echo gd_money_format($TPL_VAR["orderInfo"]["totalGoodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?><?php }?>
<?php if($TPL_VAR["orderInfo"]["totalMemberMileage"]> 0){?>, <?php echo __('회원')?> <?php echo gd_money_format($TPL_VAR["orderInfo"]["totalMemberMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?><?php }?>
<?php if($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]> 0){?>, <?php echo __('상품쿠폰')?> <?php echo gd_money_format($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?><?php }?>
<?php if($TPL_VAR["orderInfo"]["totalCouponOrderMileage"]> 0){?>, <?php echo __('주문쿠폰')?> <?php echo gd_money_format($TPL_VAR["orderInfo"]["totalCouponOrderMileage"])?><?php echo $TPL_VAR["mileage"]["unit"]?><?php }?>
                                                    )</span>
                                                </em>
<?php }?>
                                            </li>
<?php }?>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo __('총 결제금액')?></th>
                                    <td><strong class="order_payment_sum"><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></strong>
                                        <span class="add_currency"><?php echo gd_global_add_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]))?></span>
                                    </td>
                                </tr>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleGateway"])=='o'){?>
                                <tr>
                                    <th><?php echo __('승인금액')?></th>
                                    <td><strong class="deposit_money"><?php echo $TPL_VAR["orderInfo"]["overseasSettleCurrency"]?> <?php echo gd_isset($TPL_VAR["orderInfo"]["overseasSettlePrice"])?></strong></td>
                                </tr>
<?php }?>
                                <tr>
                                    <th><?php echo __('현금영수증')?></th>
                                    <td>
<?php if($TPL_VAR["orderInfo"]["receiptFl"]=='r'){?>
                                        <?php echo __('소득공제용')?> or <?php echo __('지출증빙용 신청완료')?>

<?php }else{?>
                                        <?php echo __('미발급')?>

<?php }?>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo __('세금계산서')?></th>
                                    <td>
<?php if($TPL_VAR["orderInfo"]["receiptFl"]=='t'){?>
                                        <?php echo __('신청완료')?>

<?php }else{?>
                                        <?php echo __('미발급')?>

<?php }?>
                                    </td>
                                </tr>
<?php }?>
                            </table>
                        </div><!-- .order_table_type -->
<?php }?>
                        <div class="btn_center_box button_area inline">
                            <a href="/" class="btn_order_end_ok button orange"><em><?php echo __('확인')?></em></a>
                        </div>
                    </div>
                    <!-- //order_end -->
                </div>
                <!-- //order_cont -->
            </div>
            <!-- //order_wrap -->
        </div>
    </div><!-- .center_layer -->
</section>

<?php echo $TPL_VAR["naverCommonInflowScript"]?>

<?php echo $TPL_VAR["fbOrderEndScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>