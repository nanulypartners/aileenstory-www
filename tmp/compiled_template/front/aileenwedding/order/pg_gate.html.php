<?php /* Template_ 2.2.7 2020/03/15 19:16:57 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/order/pg_gate.html 000004883 */ ?>
<?php $this->print_("share_header",$TPL_SCP,1);?>

<!-- 해당 페이지는 되도록이면 수정하지 마시기 바랍니다. 수정시 PG사 결제창이 뜨지 않을수 있습니다. -->
<!-- 해당 페이지 수정에 대한 모든 책임은 수정자에게 있습니다. -->

<h1 class="dn"><?php echo __('결제 페이지 호출')?></h1>
<script type="text/javascript">
    <!--
    /**
     * 부모창에 결제 레이어창 띄우기
     */
    function set_pg_settlement_layer() {
        var params = {
            orderNo: '<?php echo $TPL_VAR["orderNo"]?>',
        };

        var ajaxUrl = "../payment/<?php echo $TPL_VAR["pgName"]?>/pg_start.php";

        parent.$.ajax({
            method: "HEAD",
            cache: false,
            url: ajaxUrl,
            success: function () {
                settleAjax(ajaxUrl);
            },
            error: function () {
                ajaxUrl = "../../payment/<?php echo $TPL_VAR["pgName"]?>/pg_start.php";
                settleAjax(ajaxUrl);
            }
        });

        function settleAjax(url) {
            parent.$('#pgSettlementApplyLayer').empty();
            parent.$.ajax({
                method: "POST",
                cache: false,
                url: url,
                data: params,
                async: false,
                success: function (data) {
                    var progressFl = true;
                    if (data == 'NO_ORDER_DATA') {
                        alert("<?php echo __('결제 정보가 없습니다. 다시 확인 바랍니다.')?>");
                        progressFl = false;
                    }

                    if (data == 'INVALID_ORDER_DATA') {
                        alert("<?php echo __('결제를 진행할 수 없습니다.')?>");
                        progressFl = false;
                    }

                    if (progressFl == true) {
                        // 화면 출력 템플릿 구성
                        var complied = _.template($('#settleBoxLayer').html());

                        // 결제 진행
                        parent.$('#pgSettlementApplyLayer').append(complied({content: data}));
                        gd_set_layer_center();
                        if (typeof parent.pgSettleStart === 'function') {
                            parent.pgSettleStart();
                        }
                    } else {
                        // 장바구니로 이동
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert(data);
                }
            });
        }
    }

    /**
     * 결제창 레이어 출력 및 중앙 정렬
     */
    function gd_set_layer_center() {
        parent.$('#pgSettlementApplyLayer').removeClass('dn');
        parent.$('#layerDim').removeClass('dn');
        if (!parent.$('#pgSettlementApplyLayer').hasClass('dn')) {
            parent.$('#pgSettlementApplyLayer').find('> div').center(true);
        }
    }

    /**
     * 결제창 닫기 부분은 order.php에 구현
     */
    $(document).ready(function () {
        set_pg_settlement_layer();
    });
    //-->
</script>

<script id="settleBoxLayer" type="text/template">
    <input type="hidden" id="orderNo" value="<?php echo $TPL_VAR["orderNo"]?>" />
    <div class="layer_wrap_cont" style="position: absolute; margin: 0px; top: 178.5px; left: 663px; z-index: 10405;">

        <div class="ly_tit">
            <h4><?php echo __('결제 진행중')?></h4>
        </div>
        <div class="ly_cont">
            <div class="escrow_cont">
                <p>
                    <?php echo __('잠시후 결제창이 자동으로 열리게 됩니다.')?><br>
                    <?php echo __('결제창이 열리지 않으면 아래 결제창 열기를 눌러주세요.')?>

                </p>
                <div class="btn_center_box">
                    <button type="button" class="btn_pg_go" onclick="pgSettleStartAfterCheck()"><em><?php echo __('결제창 열기')?></em></button>
                </div>
            </div>
            <div class="btn_pg_box">
                <a href="../order/order_end.php?orderNo=<?php echo $TPL_VAR["orderNo"]?>&amp;mode=pgUserStop" class="btn_pg_cancel"><em><?php echo __('결제 취소')?></em></a>
                <a href="../order/cart.php" class="btn_pg_cartmove"><em><?php echo __('장바구니 이동')?></em></a>
            </div>
            <%=content%>
        </div>
        <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/aileenwedding/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
    </div>
</script>

<?php $this->print_("share_footer",$TPL_SCP,1);?>