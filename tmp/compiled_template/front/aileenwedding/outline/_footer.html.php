<?php /* Template_ 2.2.7 2020/04/10 01:38:53 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/outline/_footer.html 000002107 */  $this->include_("includeWidget","plusShop");?>
<?php if($TPL_VAR["tpls"]["footer_inc"]){?>
<?php $this->print_("footer_inc",$TPL_SCP,1);?>

<?php }?>
<!-- //footer_wrap -->

<div class="scroll_wrap">
	<!-- 우측 스크롤 배너 -->
<?php $this->print_("scroll_banner_right",$TPL_SCP,1);?>

	<!-- //우측 스크롤 배너 -->
</div>
<!-- //scroll_wrap -->

<!-- 퀵 검색 폼 -->
<?php if($TPL_VAR["tpls"]["quick_search"]){?>
<?php echo includeWidget('proc/_quick_search.html')?>

<?php }?>
<!-- 퀵 검색 폼 -->

<!-- 쇼핑 카트 탭 -->
<?php echo plusShop('proc/_cart_tab.html')?>

<!-- //쇼핑 카트 탭 -->

<!-- 절대! 지우지마세요 : Start -->
<div id="layerDim" class="dn">&nbsp;</div>
<iframe name="ifrmProcess" src='/blank.php' style="display:none<?php if(!$TPL_VAR["isProduction"]){?>block<?php }?>" width="100%" height="<?php if(!$TPL_VAR["isProduction"]){?>50<?php }?>0" bgcolor="#000"></iframe>
<!-- 절대! 지우지마세요 : End -->

<!-- 외부 스크립트 -->
<?php echo $TPL_VAR["customFooter"]?>


<!-- 팝업 -->
<div class="popup_bg" id="popupBg"></div>
<div class="popup_modal" id="popupModal">
	<a href="#" onclick="return false;" class="btn_popup_hide" id="btnPopupHide"><i class="xi-close"></i></a>
	<div class="popup_cont"></div>
</div>
<script type="text/javascript">
	jQuery('#popupBg').on('click', function() {
		hidePopupModal();
	});

	jQuery('#btnPopupHide').on('click', function() {
		hidePopupModal();
	});

	function showPopupModal(w, h) {
		var m_l = w / 2 * -1;
		var m_t = h / 2 * -1;
		jQuery('#popupModal').css({'margin-top': m_t + 'px','margin-left': m_l + 'px', 'width': w + 'px', 'height': h + 'px'}).fadeIn('fast');
		jQuery('#popupBg').show();
	}

	function hidePopupModal() {
		jQuery('#popupModal .popup_cont').empty();
		jQuery('#popupModal').css({'margin-top': '-350px', 'margin-left': '-250px', 'width': '500px', 'height': '700px'}).hide();
		jQuery('#popupBg').hide();
	}
</script>

</body>
</html>