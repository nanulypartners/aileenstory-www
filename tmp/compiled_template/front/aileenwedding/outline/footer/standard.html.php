<?php /* Template_ 2.2.7 2020/04/09 00:46:17 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/outline/footer/standard.html 000003914 */ ?>
<footer class="footer_container">
    <div class="line_wrapper"></div>
    <div class="top_wrapper">
        <div class="center_layer">
            <div class="left_layer">
                <ul class="inline">
                    <li>
                        <a href="/"><img src="/data/skin/front/aileenwedding/img/common/footer_menu_1.png" alt=""></a>
                    </li><li class="separate">|</li><li>
                        <a href="/service/agreement.php"><img src="/data/skin/front/aileenwedding/img/common/footer_menu_2.png" alt=""></a>
                    </li><li class="separate">|</li><li>
                        <a href="/service/private.php"><img src="/data/skin/front/aileenwedding/img/common/footer_menu_3.png" alt=""></a>
                    </li><li class="separate">|</li><li>
                        <a href="/board/list.php?bdId=cooperation"><img src="/data/skin/front/aileenwedding/img/common/footer_menu_4.png" alt=""></a>
                    </li>
                </ul>
            </div>
            <div class="right_layer">
                <img src="/data/skin/front/aileenwedding/img/common/footer_deposit.png" alt="">
            </div>
        </div>
    </div><!-- middle_wrapper -->
    <div class="cs_wrapper center_layer">
        <div class="left_layer">
            <div class="title">CS CENTER</div>
            <div class="phone">1800-8101</div>
            <ul class="desc">
                <li>
                    <span class="key1">평일</span>
                    <span class="val">10:00 AM ~ 06:00 PM</span>
                    <span class="key2">점심시간</span>
                    <span class="val">12:30 pm ~ 02:00 pm</span>
                </li>
                <li>주말(토,일) 및 공휴일은 휴무입니다.</li>
            </ul>
        </div><!-- .left_layer -->
        <div class="right_layer">
            <ul class="link inline">
                <li>
                    <a href="/board/list.php?bdId=qa" class="z_btn">
                        질문과 답변
                        <i class="xi-play"></i>
                    </a>
                </li><li>
                    <a href="/service/faq.php" class="z_btn">
                        자주묻는 질문
                        <i class="xi-play"></i>
                    </a>
                </li>
            </ul><!-- .link -->
        </div><!-- .right_layer -->
    </div><!-- .cs_wrapper -->
    <div class="copy_wrapper center_layer">
        <div class="left_layer">
            <div class="title">
                에일린컴퍼니 대표 이창제
            </div><!-- .title -->
            <div class="desc">
                주소: 경기도 남양주시 다산지금로 146번길 67<br>
                통신판매번호: 제2020-다산-0165호&nbsp;&nbsp;&nbsp;
                사업자등록번호: 534-05-00060<br>
                고객센터: 1800-8101&nbsp;&nbsp;&nbsp;
                이메일: aileen1800@naver.com<br>
                개인정보관리책임자: 이창제
            </div><!-- .desc -->
            <div class="copy">
                Copyright (c) 2019 AILEENCOMPANY All rights reserved.
            </div><!-- .copy -->
        </div><!-- .left_layer -->
        <div class="right_layer" style="display:none;">
            <ul class="sns inline">
                <li>
                    <a href="#"><img src="/data/skin/front/aileenwedding/img/common/footer_icon_insta.png"></a>
                </li><li>
                    <a href="#"><img src="/data/skin/front/aileenwedding/img/common/footer_icon_blog.png"></a>
                </li>
            </ul><!-- .sns_layer -->
        </div><!-- .right_layer -->
    </div><!-- .copy_wrapper -->
</footer><!-- .footer_container -->