<?php /* Template_ 2.2.7 2020/05/24 13:59:36 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/outline/header/main.html 000002143 */  $this->include_("includeFile","dataBanner");?>
<header class="header_container">
    <?php echo includeFile("proc/_menu_login.html")?>

    <div class="banner_wrapper swiper-container" id="MainBannerSlide">
        <div class="banner_layer swiper-wrapper">
<?php if((is_array($TPL_R1=dataBanner('2536020156',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <div class="swiper-slide" style="background-image:url(<?php echo $TPL_V1["bannerImageUrl"]?>);" onclick="location.href='<?php echo $TPL_V1["bannerLink"]?>'; return false;"></div>
<?php }}?>
        </div>
        <div class="logo_layer"><a href="/"><img src="/data/skin/front/aileenwedding/img/common/logo.png"></a></div>
        <div class="over_layer">
            <div class="center_layer">
                <div class="pager_area" id="MainBannerPager"></div>
                <ul class="link_area inline">
                    <li>
                        <a href="/board/list.php?bdId=goodsreview"><img src="/data/skin/front/aileenwedding/img/common/menu_review.png" alt=""></a>
                    </li><li class="separate"></li><li>
                        <a href="/board/list.php?bdId=event"><img src="/data/skin/front/aileenwedding/img/common/menu_event.png" alt=""></a>
                    </li>
                </ul><!-- .link_area -->
            </div><!-- .center_layer -->
        </div><!-- .over_layer -->
    </div><!-- .banner_wrapper -->
    <?php echo includeFile("proc/_menu_category.html")?>

</header>

<script>
    var main_banner_slide = new Swiper('#MainBannerSlide', {
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        centeredSlides: true,
        autoplay: {
            delay: 3300,
            disableOnInteraction: false,
        },
        pagination: {
            el: '#MainBannerPager',
            clickable: true,
        },
    });
</script>