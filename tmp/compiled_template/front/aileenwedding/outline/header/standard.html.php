<?php /* Template_ 2.2.7 2020/05/24 13:59:43 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/outline/header/standard.html 000001803 */  $this->include_("includeFile");?>
<header class="header_container">
    <?php echo includeFile("proc/_menu_login.html")?>

    <div class="logo_wrapper">
        <div class="center_layer">
            <div class="logo_layer">
                <a href="/"><img src="/data/skin/front/aileenwedding/img/common/logo.png"></a>
            </div><!-- .logo_layer -->
            <ul class="search_layer inline">
                <li class="menu">
                    <a href="/board/list.php?bdId=goodsreview"><img src="/data/skin/front/aileenwedding/img/common/menu_review1.png" alt=""></a>
                </li><li class="separate"></li><li class="menu">
                    <a href="/board/list.php?bdId=event"><img src="/data/skin/front/aileenwedding/img/common/menu_event1.png" alt=""></a>
                </li><li class="separate"></li><li class="search">
                    <input type="text" name="keyword" value="" id="search-form" autocomplete="off">
                    <a href="#" onclick="return false;" class="searchbtn"><i class="xi-search"></i></a>
                </li>
            </ul><!-- .search_layer -->
        </div><!-- .center_layer -->
    </div><!-- .logo_wrapper -->
    <?php echo includeFile("proc/_menu_category.html")?>

</header>

<script>
    var main_banner_slide = new Swiper('#MainBannerSlide', {
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        centeredSlides: true,
        autoplay: {
            delay: 3300,
            disableOnInteraction: false,
        },
        pagination: {
            el: '#MainBannerPager',
            clickable: true,
        },
    });
</script>