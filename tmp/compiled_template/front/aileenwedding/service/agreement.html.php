<?php /* Template_ 2.2.7 2020/03/15 19:16:58 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/service/agreement.html 000000668 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<section class="section_container">
    <div class="center_layer">
        <div class="section_layer">
            <div class="title_area">
                <div class="title"><?php echo $TPL_VAR["codeData"]["informNm"]?></div>
            </div><!-- .title_area -->
            <?php echo nl2br($TPL_VAR["terms"]['content'])?>

        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section><!-- .section_container -->
<?php $this->print_("footer",$TPL_SCP,1);?>