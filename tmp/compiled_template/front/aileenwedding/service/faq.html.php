<?php /* Template_ 2.2.7 2020/03/15 19:16:58 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/service/faq.html 000005585 */  $this->include_("includeFile");
if (is_array($TPL_VAR["faqCode"])) $TPL_faqCode_1=count($TPL_VAR["faqCode"]); else if (is_object($TPL_VAR["faqCode"]) && in_array("Countable", class_implements($TPL_VAR["faqCode"]))) $TPL_faqCode_1=$TPL_VAR["faqCode"]->count();else $TPL_faqCode_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<link href="/data/skin/front/aileenwedding/css/contents.css" rel="stylesheet">
<section class="section_container">
    <div class="center_layer">
        <!-- 고객센터 메뉴 -->
        <?php echo includeFile('proc/_cs_menu.html')?>

        <!--// 고객센터 메뉴 -->
        <div class="section_layer">
            <form name="frmList" id="frmList" action="faq.php" method="get">
                <input type="hidden" name="noheader" value="<?php echo $TPL_VAR["req"]["noheader"]?>">
                <input type="hidden" name="searchField" value="all"/>
                <div class="search_area">
                    <div class="field">
                        <div class="key">질문 검색</div>
                        <div class="value">
                            <div class="text_input">
                                <input type="text" id="time" name="searchWord" class="text" placeholder="<?php echo __('검색어를 입력하세요')?>"/>
                            </div>
                            <div class="button_area">
                                <button type="submit" class="btn_date_check button orange"><em><?php echo __('검색')?></em></button>
                            </div>
                        </div><!-- .value -->
                    </div><!-- .field -->
                </div><!-- .search_area -->
            </form>
            <ul class="tab_menu">
                <li <?php if(!$TPL_VAR["req"]["category"]){?>class="on"<?php }?>><a href="faq.php?noheader=<?php echo $TPL_VAR["req"]["noheader"]?>&isBest=<?php echo $TPL_VAR["req"]["isBest"]?>&searchField=<?php echo $TPL_VAR["req"]["searchField"]?>&searchWord=<?php echo $TPL_VAR["req"]["searchWord"]?>"><span><?php echo __('전체')?></span></a></li>
<?php if($TPL_faqCode_1){foreach($TPL_VAR["faqCode"] as $TPL_K1=>$TPL_V1){?>
                <li <?php if($TPL_VAR["req"]["category"]==$TPL_K1){?>class="on"<?php }?>><a href="faq.php?category=<?php echo $TPL_K1?>&noheader=<?php echo $TPL_VAR["req"]["noheader"]?>&isBest=<?php echo $TPL_VAR["req"]["isBest"]?>&searchField=<?php echo $TPL_VAR["req"]["searchField"]?>&searchWord=<?php echo $TPL_VAR["req"]["searchWord"]?>"><span><?php echo $TPL_V1?></span></a></li>
<?php }}?>
            </ul>
            <table class="table c_table no_header" id="FaqList">
                <colgroup>
                    <col style="width:67px">
                    <col style="width:144px">
                    <col>
                </colgroup>
                <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["faqList"]["data"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <tr data-sno="<?php echo $TPL_V1["sno"]?>">
                        <td class="no"><?php echo $TPL_V1["no"]?></td>
                        <td><?php echo $TPL_V1["categoryNm"]?></td>
                        <td class="title">
                            <strong class="faq_type text_gray">Q.</strong>
                            <a href="#" onclick="return false;" class="btn_view">
                                <strong><?php echo $TPL_V1["subject"]?></strong>
                            </a>
                        </td>
                    </tr>
<?php }}?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $TPL_VAR["faqList"]["pagination"]?>

            </div>
        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section><!-- .section_container -->
<script>
    $(function () {
        $('.btn_view').bind('click', function () {
            var $target_tr = $(this).closest('tr');
            var sno = $target_tr.attr('data-sno');

            // 초기화
            $('#FaqList tr.toggle_faq').remove();

            if($target_tr.hasClass('on')) {
                $target_tr.removeClass('on');
            }
            else {
                // 초기화
                $('#FaqList tr').each(function() {
                    if($(this).hasClass('on')) $(this).removeClass('on');
                });
                $target_tr.addClass('on');

                // 답변 확인
                $.ajax({
                    method: "POST",
                    url: "./faq.php",
                    data: {mode: 'getAnswer', sno: sno},
                    dataType: 'json'
                }).success(function (data) {
                    var answer_tr = '<tr class="toggle_faq">';
                    answer_tr += '<td></td><td></td>';
                    answer_tr += '<td class="title">';
                    answer_tr += '<strong class="faq_type text_orange">A.</strong>';
                    answer_tr += '<div class="view">' + data['answerContents'] + '</div>';
                    answer_tr += '</td>';
                    answer_tr += '</tr>';
                    $target_tr.after(answer_tr);
                }).error(function (e) {
                    alert(e.responseText);
                });
            }
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>