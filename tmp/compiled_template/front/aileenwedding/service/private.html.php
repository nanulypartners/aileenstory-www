<?php /* Template_ 2.2.7 2020/03/15 19:16:58 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/service/private.html 000000655 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<section class="section_container">
    <div class="center_layer">
        <div class="section_layer">
            <div class="title_area">
                <div class="title"><?php echo __('개인정보처리방침')?></div>
            </div><!-- .title_area -->
            <?php echo nl2br($TPL_VAR["private"])?>

        </div><!-- .section_layer -->
    </div><!-- .center_layer -->
</section><!-- .section_container -->
<?php $this->print_("footer",$TPL_SCP,1);?>