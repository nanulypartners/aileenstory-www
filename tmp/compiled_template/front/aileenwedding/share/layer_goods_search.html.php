<?php /* Template_ 2.2.7 2020/03/15 19:16:58 /www/aileen8919_godomall_com/data/skin/front/aileenwedding/share/layer_goods_search.html 000002708 */ 
if (is_array($TPL_VAR["list"])) $TPL_list_1=count($TPL_VAR["list"]); else if (is_object($TPL_VAR["list"]) && in_array("Countable", class_implements($TPL_VAR["list"]))) $TPL_list_1=$TPL_VAR["list"]->count();else $TPL_list_1=0;?>
<div class="top_table_type">
    <form id="frmSelect">
        <span class="pick_list_num"><?php echo __('상품')?> <strong><?php echo $TPL_VAR["total"]?></strong> <?php echo __('건')?></span>
        <table>
            <colgroup>
                <col style="width:80px">
                <col>
                <col style="width:137px">
            </colgroup>
            <thead>
            <tr>
                <th><?php echo __('선택')?></th>
                <th><?php echo __('상품명')?></th>
                <th><?php echo __('상품금액')?></th>
            </tr>
            </thead>
            <tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
            <tr id="tbl_add_<?php echo $TPL_V1["goodsNo"]?>">
                <td><input type="radio" id="<?php echo $TPL_V1["goodsNo"]?>" name="goodsNo[]" class="radio" value="<?php echo $TPL_V1["goodsNo"]?>"></td>
                <td class="td_left">
                    <label class="choice_s" for="<?php echo $TPL_V1["goodsNo"]?>">
                        <div class="pick_add_cont">
                            <span class="pick_add_img">
                                <img src="<?php echo $TPL_V1["goodsImageSrc"]?>" alt="">
                            </span>
                            <div class="pick_add_info">
                                <em><?php echo $TPL_V1["goodsNm"]?></em>
                            </div>
                        </div>
                        <!-- //pick_add_info -->
                    </label>
                </td>
                <td class="js_item_price">
<?php if($TPL_V1["soldOut"]=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='text'){?>
                    <strong><?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_text"]?></strong>
<?php }else{?>
<?php if($TPL_V1["goodsPriceString"]!=''){?>
                        <strong><?php echo $TPL_V1["goodsPriceString"]?></strong>
<?php }else{?>
                        <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["oriGoodsPrice"])?></strong>
                        <?php echo gd_global_currency_string()?>

<?php }?>
<?php }?>
                </td>
            </tr>
<?php }}?>
            </tbody>
        </table>
    </form>
</div>

<?php echo $TPL_VAR["pagination"]?>

<!-- //pagination -->