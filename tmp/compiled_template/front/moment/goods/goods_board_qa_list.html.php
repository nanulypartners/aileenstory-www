<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/front/moment/goods/goods_board_qa_list.html 000003145 */ ?>
<div class="qna_table">
    <table class="qna_table_type">
        <colgroup>
            <col width="5%">
            <col>
            <col width="13%">
            <col width="13%">
            <col width="13%">
        </colgroup>
        <thead>
            <tr>
                <th><?php echo __('번호')?></th>
                <th><?php echo __('제목')?></th>
                <th><?php echo __('작성자')?></th>
                <th><?php echo __('작성일')?></th>
                <th><?php echo __('진행상황')?></th>
            </tr>
        </thead>
        <tbody>
<?php if($TPL_VAR["bdList"]["list"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <tr class="js_data_row" data-bdid="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_V1["sno"]?>" data-auth="<?php echo $TPL_V1["auth"]["view"]?>">
                <td><?php echo $TPL_V1["articleListNo"]?></td>
                <td class="board_tit">
                    <a href="javascript:void(0)" class="js_btn_view <?php if($TPL_V1["groupThread"]){?>reply<?php }?>">
                        <?php echo $TPL_V1["gapReply"]?>

<?php if($TPL_V1["isSecret"]=='y'){?>
                        <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["secret"]["url"]?>">
<?php }?>
                        <strong><?php echo $TPL_V1["subject"]?></strong>
<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
                        <span>(<?php echo $TPL_V1["memoCnt"]?>)</span>
<?php }?>
<?php if($TPL_V1["isFile"]=='y'){?>
                        <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
                        <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
                        <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>"/>
<?php }?>
                    </a>
                </td>
                <td><?php echo $TPL_V1["writer"]?></td>
                <td><?php echo $TPL_V1["regDate"]?></td>
                <td><?php echo $TPL_V1["replyStatusText"]?></td>
            </tr>
            <tr class="js_detail" data-bdid="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_V1["sno"]?>" data-auth="<?php echo $TPL_V1["auth"]["view"]?>"></tr>
<?php }}?>
<?php }else{?>
            <tr>
                <td class="no_data" colspan="5"><?php echo __('등록된 상품문의가 없습니다.')?></td>
            </tr>
<?php }?>
        </tbody>
</table>
</div>
<?php echo $TPL_VAR["pagienation"]?>