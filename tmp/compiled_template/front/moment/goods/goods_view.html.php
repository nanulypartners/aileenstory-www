<?php /* Template_ 2.2.7 2020/01/31 14:56:13 /www/aileen8919_godomall_com/data/skin/front/moment/goods/goods_view.html 000149180 */  $this->include_("includeWidget","includeFile");
if (is_array($TPL_VAR["goodsCategoryList"])) $TPL_goodsCategoryList_1=count($TPL_VAR["goodsCategoryList"]); else if (is_object($TPL_VAR["goodsCategoryList"]) && in_array("Countable", class_implements($TPL_VAR["goodsCategoryList"]))) $TPL_goodsCategoryList_1=$TPL_VAR["goodsCategoryList"]->count();else $TPL_goodsCategoryList_1=0;
if (is_array($TPL_VAR["snsShareButton"])) $TPL_snsShareButton_1=count($TPL_VAR["snsShareButton"]); else if (is_object($TPL_VAR["snsShareButton"]) && in_array("Countable", class_implements($TPL_VAR["snsShareButton"]))) $TPL_snsShareButton_1=$TPL_VAR["snsShareButton"]->count();else $TPL_snsShareButton_1=0;
if (is_array($TPL_VAR["displayField"])) $TPL_displayField_1=count($TPL_VAR["displayField"]); else if (is_object($TPL_VAR["displayField"]) && in_array("Countable", class_implements($TPL_VAR["displayField"]))) $TPL_displayField_1=$TPL_VAR["displayField"]->count();else $TPL_displayField_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<script type="text/javascript">
    <!--
    var bdGoodsQaId = '<?php echo $TPL_VAR["bdGoodsQaId"]?>';
    var bdGoodsReviewId = '<?php echo $TPL_VAR["bdGoodsReviewId"]?>';
    var goodsNo = '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>';
    var goodsViewController = new gd_goods_view();
    var goodsTotalCnt;
    var goodsOptionCnt = [];

    $(document).ready(function(){
        var parameters = {
            'setControllerName' : goodsViewController,
            'setOptionFl' : '<?php echo $TPL_VAR["goodsView"]["optionFl"]?>',
            'setOptionTextFl'	: '<?php echo $TPL_VAR["goodsView"]["optionTextFl"]?>',
            'setOptionDisplayFl'	: '<?php echo $TPL_VAR["goodsView"]["optionDisplayFl"]?>',
            'setAddGoodsFl'	: '<?php if(is_array($TPL_VAR["goodsView"]["addGoods"])){?>y<?php }else{?>n<?php }?>',
            'setIntDivision'	: '<?php echo INT_DIVISION?>',
            'setStrDivision'	: '<?php echo STR_DIVISION?>',
            'setMileageUseFl'	: '<?php echo $TPL_VAR["mileageData"]["useFl"]?>',
            'setCouponUseFl'	: '<?php echo $TPL_VAR["couponUse"]?>',
            'setMinOrderCnt'	: '<?php echo $TPL_VAR["goodsView"]["minOrderCnt"]?>',
            'setMaxOrderCnt'	: '<?php echo $TPL_VAR["goodsView"]["maxOrderCnt"]?>',
            'setStockFl'	: '<?php echo gd_isset($TPL_VAR["goodsView"]["stockFl"])?>',
            'setSalesUnit' : '<?php echo gd_isset($TPL_VAR["goodsView"]["salesUnit"], 1)?>',
            'setDecimal' : '<?php echo $TPL_VAR["currency"]["decimal"]?>',
            'setGoodsPrice' : '<?php echo gd_isset($TPL_VAR["goodsView"]["goodsPrice"], 0)?>',
            'setGoodsNo' : '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>',
            'setMileageFl' : ' <?php echo $TPL_VAR["goodsView"]["mileageFl"]?>',
            'setFixedSales' : '<?php echo $TPL_VAR["goodsView"]["fixedSales"]?>',
            'setFixedOrderCnt' : '<?php echo $TPL_VAR["goodsView"]["fixedOrderCnt"]?>',
            'setOptionPriceFl' : '<?php echo $TPL_VAR["optionPriceFl"]?>',
            'setStockCnt' : '<?php echo $TPL_VAR["goodsView"]["stockCnt"]?>'
    };

        goodsViewController.init(parameters);

<?php if($TPL_VAR["goodsView"]['qrCodeFl']=='y'&&$TPL_VAR["goodsView"]['qrStyle']=='btn'){?>
        $('#qrCodeDownloadButton').on('click', function() {
            location.href = './goods_qr_code.php?goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>&goodsName=<?php echo gd_htmlspecialchars_addslashes($TPL_VAR["goodsView"]["goodsNmDetail"])?>';
        });
<?php }?>

<?php if($TPL_VAR["goodsView"]['optionFl']=='n'&&$TPL_VAR["goodsView"]['orderPossible']=='y'){?>
        goodsViewController.goods_calculate('#frmView', 1, 0, "<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>");
<?php }?>

        /* 상품 이미지 슬라이드 */
        $('.item_photo_info_sec .slider_goods_nav').slick({
            dots: false,
            centerMode: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: $('.item_photo_info_sec .slick_goods_prev'),
            nextArrow: $('.item_photo_info_sec .slick_goods_next')
        });

        /* 줌레이어 상품 이미지 슬라이드 */
        $('.ly_slider_goods_nav').slick({
            dots: false,
            centerMode: false,
            vertical: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: $('.zoom_layer .slick_goods_prev'),
            nextArrow: $('.zoom_layer .slick_goods_next')
        });


        $('button.goods_cnt').on('click', function(e) {
            goodsViewController.count_change(this, 1);
        });

        $('button.add_goods_cnt').on('click', function(e) {
            goodsViewController.count_change(this);
        });


<?php if($TPL_VAR["goodsView"]['benefitPossible']=='y'){?>
        gd_benefit_calculation();
<?php }?>

<?php if($TPL_VAR["couponUse"]=='y'){?>
        gd_open_layer();
<?php }?>

<?php if($TPL_VAR["goodsView"]['imgDetailViewFl']=='y'){?>
        $("#mainImage img").data("image-zoom", $("#mainImage img").attr('src'));
//        $("#mainImage img").data("image-zoom", $("#testZoom img").attr('src'));
        $("#mainImage img").elevateZoom();
<?php }?>

        $('.btn_add_order').on('click', function(e){
            gd_goods_order('d');
            return false;
        });

        $('.btn_add_wish').on('click', function(e){
            gd_goods_order('w');
            return false;
        });

        $('.btn_add_cart').on('click', function(e){
            gd_goods_order();
            return false;
        });

        //상품 재입고 알림 팝업 오픈
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
        $('.restockSelector').on('click', function(e) {
            window.open("./popup_goods_restock.php?goodsNo="+goodsNo, "popupRestock", "top=100, left=200, status=0, width=682px, height=600px");
            return false;
        });
<?php }?>

<?php if($TPL_VAR["goodsReviewAuthList"]=='y'){?>
        gd_load_goodsBoardList(bdGoodsReviewId, goodsNo);
<?php }?>

<?php if($TPL_VAR["goodsQaAuthList"]=='y'){?>
        gd_load_goodsBoardList(bdGoodsQaId, goodsNo);
<?php }?>

        // SNS 공유하기
        $('.target_sns_share').on('click', function() {
            if ($("#lySns").css("display") == 'block') {
                // 단축주소 가져오기
                $.ajax({
                    type: 'post',
                    url: './goods_ps.php',
                    async: true,
                    cache: true,
                    data: {
                        mode: 'get_short_url',
                        url: '<?php echo $TPL_VAR["snsShareUrl"]?>'
                    },
                    success: function (data) {
                        var json = $.parseJSON(data);
                        $('.sns_copy_url > input').val(json.url);
                        $('.sns_copy_url > button').attr('data-clipboard-text', json.url);
                    }
                });
            }
        });

<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
        $("#displayTimeSale").hide();
        gd_dailyMissionTimer("<?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['timeSaleDuration']?>");
<?php }?>

        var canGoodsReview = '<?php echo $TPL_VAR["canGoodsReview"]?>';
        var canPlusReview = '<?php echo $TPL_VAR["plusReviewConfig"]["isShowGoodsPage"]?>';
        var canGoodsQa = '<?php echo $TPL_VAR["canGoodsQa"]?>';
        var tabCount = 5;
        if (!canGoodsReview && canPlusReview != 'y') {
            $('.tab a[href=#reviews]').remove();
            $('#reviews').hide();
            tabCount-- ;
        }

        if (!canGoodsQa) {
            $('.tab a[href=#qna]').remove();
            $('#qna').hide();
            tabCount--;
        }
        if (tabCount < 5) {
            $('.multiple-topics .tab a').css('width', 100 / tabCount + '%');
        }

        $('.layer-cartaddconfirm').click(function(){
            location.href = '../order/cart.php';
        });

        // 배송비 항목을 노출 안함 설정하면 배송비 타입을 생성
        var deliveryCollectFl = "<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['collectFl']?>";
        if ($('#frmView [name="deliveryCollectFl"]').length > 0) {
            // 이미 존재 패스
        } else if (deliveryCollectFl == 'both') {
            // 선택은 패스
        } else {
            $('#frmView').append('<input type="hidden" name="deliveryCollectFl" value="' + deliveryCollectFl + '">');
        }

        $('.btn_move_cart').click(function() {
            location.href = '../order/cart.php';
        });

        $('.btn_move_wish').click(function() {
            location.href = '../mypage/wish_list.php';
        });

        //배송 방식에 따른 방문 수령지 노출 여부
        $(".js-deliveryMethodFl").change(function(){
            if($(this).val() === 'visit'){
                $(".js-deliveryMethodVisitArea").removeClass('dn');
            }
            else {
                $(".js-deliveryMethodVisitArea").addClass('dn');
            }
        });
    });

    $(document).on('keydown focusout', 'input[name^=goodsCnt]', function(e){
        $(this).val($(this).val().replace(/[^0-9\-]/g,""));
    });

    /**
     * KC마크 인증정보창
     * @param string url KC인증번호검색 url
     * @return
     */
    function popupKcInfo(url) {
        var win = gd_popup({
            url: url
            , target: 'searchPop'
            , width: 750
            , height: 700
            , resizable: 'no'
            , scrollbars: 'yes'
        });
        win.focus();
        return win;
    }

<?php if($TPL_VAR["couponUse"]=='y'){?>
    // 쿠폰 오픈 레이어에 따른 분기
    function gd_open_layer() {
        $('.btn_open_layer').bind('click', function(e) {
            if ($(this).attr('href') == '#lyCouponDown') {
                gd_coupon_down();
            } else if ($(this).attr('href') == '#lyCouponApply') {
                gd_coupon_apply($(this).data('key'));
            }
        });
    }

    function gd_bind_coupon_cancel() {
        $('.btn_coupon_cancel').bind('click', function(e){
            $('.payco_pay').removeClass('dn');
            $('.naver_pay').removeClass('dn');
            gd_coupon_cancel($(this).data('key'), '');
        });
    }

    function gd_coupon_cancel(optionKey, typeCode) {
        $('#option_display_item_' + optionKey + ' input:hidden[name="couponApplyNo[]"]').val('');
        $('#option_display_item_' + optionKey + ' input:hidden[name="couponSalePriceSum[]"]').val('');
        $('#option_display_item_' + optionKey + ' input:hidden[name="couponAddPriceSum[]"]').val('');
        var couponApplyHtml = "<a href=\"#lyCouponApply\" class=\"btn_open_layer\" data-key=\""+optionKey+"\"><img src=\"/data/skin/front/moment/img/common/btn/btn_coupon_apply.png\" alt=\"<?php echo __('쿠폰적용')?>\"/></a>";
        $('#coupon_apply_' + optionKey).html(couponApplyHtml);
        if ($('#cart_tab_coupon_apply_'+optionKey).length) $('#cart_tab_coupon_apply_' + optionKey).html(couponApplyHtml);
        gd_open_layer();
        if (typeCode == 'noCalculation') {
            // 재계산 안함
        } else {
            gd_benefit_calculation();
        }
    }

    function gd_coupon_down() {
        $.ajax({
                method: "POST",
                cache: false,
                url: "../goods/layer_coupon_down.php",
                data: "goodsNo=" + <?php echo $TPL_VAR["goodsView"]["goodsNo"]?>,
            success: function (data) {
            $('#lyCouponDown').empty().append(data);
            $('#lyCouponDown').find('>div').center();
        },
        error: function (data) {
            alert(data.message);
            gd_close_layer();
        }
    });
    }
    function gd_coupon_apply(optionKey) {
        var params = {
            mode: 'coupon_apply',
            goodsNo: <?php echo $TPL_VAR["goodsView"]['goodsNo']?>,
            optionKey: optionKey,
            couponApplyNotNo: $('input:hidden[name="couponApplyNo[]"]').serializeArray(),
            couponApplyNo: $('#option_display_item_'+optionKey+' input:hidden[name="couponApplyNo[]"]').val(),
            goodsCnt: $('#option_display_item_'+optionKey+' input:text[name="goodsCnt[]"]').val(),
            goodsPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="goodsPriceSum[]"]').val(),
            optionPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="optionPriceSum[]"]').val(),
            optionTextPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="optionTextPriceSum[]"]').val(),
            addGoodsPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="addGoodsPriceSum[]"]').val(),
        };

        $.ajax({
            method: "POST",
            cache: false,
            url: "../goods/layer_coupon_apply.php",
            data: params,
            success: function (data) {
                $('#lyCouponApply').empty().append(data);
                $('#lyCouponApply').find('>div').center();
            },
            error: function (data) {
                alert(data.message);
                gd_close_layer();
            }
        });
    }
<?php }?>

    /**
     * 메인 이미지 변경
     *
     * @param string keyNo 상품 배열 키값
     */
    function gd_change_image(keyNo, type) {
        if (typeof keyNo == 'string') {
            var detailKeyID = new Array();
<?php if((is_array($TPL_R1=gd_isset($TPL_VAR["goodsView"]['image']['detail']['img']))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            detailKeyID[<?php echo $TPL_K1?>] = "<?php echo gd_htmlspecialchars_slashes($TPL_V1,'add')?>";
<?php }}?>

            var magnifyKeyID = new Array();
<?php if((is_array($TPL_R1=gd_isset($TPL_VAR["goodsView"]['image']['magnify']['img']))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            magnifyKeyID[<?php echo $TPL_K1?>] = "<?php echo gd_htmlspecialchars_slashes($TPL_V1,'add')?>";
<?php }}?>

            if (type == 'detail') {
                $('#mainImage').html(detailKeyID[keyNo]);
            } else {
                $('#magnifyImage').html(magnifyKeyID[keyNo]);
            }

<?php if($TPL_VAR["goodsView"]['imgDetailViewFl']=='y'){?>
            $("#mainImage img").data("image-zoom", $("#mainImage img").attr('src'));
//            $("#mainImage img").data("image-zoom", $("#testZoom img").attr('src'));
            $("#mainImage img").elevateZoom();
<?php }?>
        }
    }

    /**
     * 총 합산
     */
    function gd_total_calculate() {
        var goodsPrice = parseFloat($('#frmView input[name="set_goods_price"]').val());

        //총합계 계산
        goodsTotalCnt =  0;
        $('#frmView input[name*="goodsCnt[]"]').each(function (index) {
            goodsTotalCnt += parseFloat($(this).val());
            goodsOptionCnt[index] = parseFloat($(this).val());
        });
        var goodsTotalPrice = goodsPrice * goodsTotalCnt;
        var setOptionPrice =  0;

        $('#frmView input[name*="optionPriceSum[]"]').each(function () {
            setOptionPrice += parseFloat($(this).val());
        });

        var setOptionTextPrice =  0;
        $('#frmView input[name*="optionTextPriceSum[]"]').each(function () {
            setOptionTextPrice += parseFloat($(this).val());
        });

        var setAddGoodsPrice =  0;
        $('#frmView input[name*="add_goods_total_price["]').each(function () {
            setAddGoodsPrice += parseFloat($(this).val());
        });

        $('#frmView input[name="set_option_price"]').val(setOptionPrice);
        $('#frmView input[name="set_option_text_price"]').val(setOptionTextPrice);
        $('#frmView input[name="set_add_goods_price"]').val(setAddGoodsPrice);

        var totalGoodsPrice = (goodsTotalPrice + setOptionPrice + setOptionTextPrice + setAddGoodsPrice).toFixed(<?php echo $TPL_VAR["currency"]["decimal"]?>);
        $('#frmView input[name="set_total_price"]').val(totalGoodsPrice);
        $(".goods_total_price").html("<?php echo gd_global_currency_symbol()?>" + gd_money_format(totalGoodsPrice) + "<b><?php echo gd_global_currency_string()?></b>");

<?php if($TPL_VAR["addGlobalCurrency"]){?>
        $(".goods_total_price").append("<p class='add_currency'><?php echo gd_global_add_currency_symbol()?> " + gd_add_money_format(totalGoodsPrice) + "<?php echo gd_global_add_currency_string()?></p>");
<?php }?>

        gd_benefit_calculation();
    }

    /*
     * 혜택
     */
    function gd_benefit_calculation() {
<?php if($TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='n'){?>
        $('button.goods_cnt').attr('disabled', false);
        $('button.add_goods_cnt').attr('disabled', false);
        return false;
<?php }?>

        $('input[name="mode"]').val('get_benefit');
        var parameters = $("#frmView").serialize();

        if ($("#frmView input[name*='goodsNo']").length == 0) {
            parameters += "&goodsNo%5B%5D=<?php echo $TPL_VAR["goodsView"]['goodsNo']?>&goodsCnt%5B%5D=1";
        }

        $.post('./goods_ps.php', parameters, function (data) {
            var getData = $.parseJSON(data);

            if(getData.totalDcPrice > 0 || getData.totalMileage > 0) {
                $(".item_discount_mileage").removeClass('dn');

                if(getData.totalDcPrice > 0 ) {
                    $(".item_discount_mileage span.item_discount").removeClass('dn');
                    $(".end_price dl.total_discount").removeClass('dn');
                    var tmp = new Array();
                    if (getData.goodsDcPrice) tmp.push("<?php echo __('상품')?> : " + " <?php echo gd_global_currency_symbol()?>" + gd_money_format(getData.goodsDcPrice) + "<?php echo gd_global_currency_string()?>");
                    if (getData.memberDcPrice) tmp.push("<?php echo __('회원')?> : " + " <?php echo gd_global_currency_symbol()?>" + gd_money_format(getData.memberDcPrice) + "<?php echo gd_global_currency_string()?>");
                    if (getData.couponDcPrice) tmp.push("<?php echo __('쿠폰')?> : " + " <?php echo gd_global_currency_symbol()?>" + gd_money_format(getData.couponDcPrice) + "<?php echo gd_global_currency_string()?>");

                    $(".benefit_price").html("(" + tmp.join() + ")");

                    $(".total_benefit_price").html("-<?php echo gd_global_currency_symbol()?>" + gd_money_format(getData.totalDcPrice) + "<b><?php echo gd_global_currency_string()?></b>");

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                    $(".end_price .total_benefit_price").append("<p class='add_currency'>-<?php echo gd_global_add_currency_symbol()?> " + gd_add_money_format(getData.totalDcPrice) + "<?php echo gd_global_add_currency_string()?></p>");
<?php }?>

                    $("#set_dc_price").val(getData.totalDcPrice);

                } else {
                    $("#set_dc_price").val('0');
                    $(".item_discount_mileage span.item_discount").addClass('dn');
                    $(".end_price dl.total_discount").addClass('dn');
                }

                if(getData.totalMileage > 0 ) {
                    $(".item_discount_mileage span.item_mileage").removeClass('dn');
                    var tmp =new Array();
                    if (getData.goodsMileage) tmp.push("<?php echo __('상품')?> : " + gd_money_format(getData.goodsMileage) + "<?php echo $TPL_VAR["mileageData"]['unit']?>");
                    if (getData.memberMileage) tmp.push("<?php echo __('회원')?> : " + gd_money_format(getData.memberMileage) + "<?php echo $TPL_VAR["mileageData"]['unit']?>");
                    if (getData.couponMileage) tmp.push("<?php echo __('쿠폰')?> : " + gd_money_format(getData.couponMileage) + "<?php echo $TPL_VAR["mileageData"]['unit']?>");
                    $(".benefit_mileage").html("("+tmp.join()+")");

                    $(".total_benefit_mileage").html("+" + gd_money_format(getData.totalMileage) + "<?php echo $TPL_VAR["mileageData"]['unit']?>");
                } else {
                    $(".item_discount_mileage span.item_mileage").addClass('dn');
                }
            } else {
                $("#set_dc_price").val('0');
                $(".item_discount_mileage").addClass('dn');
                $(".end_price dl.total_discount").addClass('dn');
            }

            if ($('#frmView input[name="set_total_price"]').val().trim() == '0') {
                $(".total_price").html("<?php echo gd_global_currency_symbol()?>0<b><?php echo gd_global_currency_string()?></b>");
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                $(".total_price").append("<p class='add_currency'><?php echo gd_global_add_currency_symbol()?> " + 0 + "<?php echo gd_global_add_currency_string()?></p>");
<?php }?>
                if ($("#cart_tab_option").length) $("#cart_tab_option .total_benefit_price").html("<?php echo gd_global_currency_symbol()?>0<b><?php echo gd_global_currency_string()?></b>");

            } else {
                var totalPrice = parseFloat($('#frmView input[name="set_total_price"]').val()) - parseFloat(getData.totalDcPrice);
                $(".total_price").html(" <?php echo gd_global_currency_symbol()?> " + gd_money_format(totalPrice) + "<b><?php echo gd_global_currency_string()?></b>");

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                $(".total_price").append("<p class='add_currency'><?php echo gd_global_add_currency_symbol()?> " + gd_add_money_format(totalPrice) + "<?php echo gd_global_add_currency_string()?></p>");
<?php }?>
            }

            $('button.goods_cnt').attr('disabled', false);
            $('button.add_goods_cnt').attr('disabled', false);
            // 쿠폰 구매금액 제한에 따른 처리
            if (typeof getData.couponAlertKey == 'undefined') {
                // 구매 금액 제한에 걸리지 않음
            } else {
                gd_coupon_cancel(getData.couponAlertKey, 'noCalculation');
                alert("<?php echo __('적용 쿠폰이 구매 금액 제한에 걸려 적용 쿠폰이 취소 되었습니다.')?>");
            }
        });
    }

    /**
     * 바로구매, 장바구니, 상품 보관함
     *
     * @param string modeStr 처리 모드
     */
    var salesUnit = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['salesUnit'], 1)?>");
    var minOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['minOrderCnt'], 1)?>");
    var maxOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['maxOrderCnt'], 0)?>");
    function gd_goods_order(modeStr)
    {
        <?php echo $TPL_VAR["customScript"]?>

        $('#frmView input[name=\'cartMode\']').val(modeStr);

        if (modeStr == 'w') {
<?php if(gd_is_login()===false){?>
            alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
            document.location.href = "../member/login.php";
            return false;
<?php }else{?>

            var goodsNoCnt = $('#frmView input[name*="goodsNo[]"]').length;
            if(goodsNoCnt == 0) {
                $('#frmView input[name="cartMode"]').val('<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>');
            }

            $('#frmView input[name="mode"]').val('wishIn');
            $('#frmView').attr('action','../mypage/wish_list_ps.php');
<?php }?>
        } else {
            $('#frmView input[name="mode"]').val('cartIn');
            $('#frmView').attr('action','../order/cart_ps.php');

<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
            var goodsInfo = $('#frmView input[name*=\'optionSno[]\']').length;
<?php }else{?>
            var goodsInfo = $('#frmView input[name="optionSnoInput"]').val();
<?php }?>

            if (goodsInfo == '') {
                alert("<?php echo __('가격 정보가 없거나 옵션이 선택되지 않았습니다!')?>");
                return false;
            }

<?php if(gd_isset($TPL_VAR["goodsView"]['optionTextFl'])=='y'){?>
            if(!goodsViewController.option_text_valid("#frmView")) {
                alert("<?php echo __('입력 옵션을 확인해주세요.')?>");
                return false;
            }
<?php if(gd_isset($TPL_VAR["goodsView"]['stockFl'])=='y'){?>
                var checkOptionCnt = goodsViewController.option_text_cnt_valid("#frmView");
                if(checkOptionCnt) {
                    alert(__('재고가 부족합니다. 현재 %s개의 재고가 남아 있습니다.', checkOptionCnt));
                    return false;
                }
<?php }?>
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
            //추가상품
            if(!goodsViewController.add_goods_valid("#frmView")) {
                alert("<?php echo __('필수 추가 상품을 확인해주세요.')?>");
                return false;
            }
<?php }?>

        }

        var submitFl = true;
        if (isNaN(goodsTotalCnt)) goodsTotalCnt = 1;
        if (_.isEmpty(goodsOptionCnt)) goodsOptionCnt[0] = 1;
<?php if($TPL_VAR["goodsView"]['fixedSales']=='goods'){?>
        var perSalesCnt = goodsTotalCnt % salesUnit;

        if (perSalesCnt !== 0) {
            alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
            submitFl = false;
        }
<?php }else{?>
        for (i in goodsOptionCnt) {
            if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
            var perSalesCnt = goodsOptionCnt[i] % salesUnit;

            if (perSalesCnt !== 0) {
                alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                submitFl = false;
                break;
            }
        }
<?php }?>

        if (submitFl == true) {
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
            var fixedAlertString = '상품당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='option'){?>
            var fixedAlertString = '옵션당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
            var fixedAlertString = 'ID당';
<?php }?>

<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'||$TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
            if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                submitFl = false;
            } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                submitFl = false;
            }
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
            //ajax로 id구매카운트 체크
            var params = {
                mode: 'check_memberOrderGoodsCount',
                goodsNo: <?php echo $TPL_VAR["goodsView"]['goodsNo']?>,
            };
            $.ajax({
                method: "POST",
                async: false,
                cache: false,
                url: '../order/order_ps.php',
                data: params,
                success: function (data) {
                    // error 메시지 예외 처리용
                    if (!_.isUndefined(data.error) && data.error == 1) {
                        alert(data.message);
                        return false;
                    }

                    if (minOrderCnt > 1 && (goodsTotalCnt + data.count) < minOrderCnt) {
                        alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                    } else if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                        alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                    } else if (maxOrderCnt > 0 && (goodsTotalCnt + data.count) > maxOrderCnt) {
                        alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                    } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                        alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                    }
                },
                error: function (data) {
                    alert(data.message);
                    submitFl = false;
                }
            });
<?php }?>
<?php }else{?>
            for (i in goodsOptionCnt) {
                if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                if (minOrderCnt > 1 && goodsOptionCnt[i] < minOrderCnt) {
                    alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                    submitFl = false;
                    break;
                } else if (maxOrderCnt > 0 && goodsOptionCnt[i] > maxOrderCnt) {
                    alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                    submitFl = false;
                    break;
                }
            }
<?php }?>
        }

        if ((modeStr == 'd' || modeStr == 'pa') && submitFl === false) {
            return false;
        }

        if(modeStr == 'pa') {
            return true;
        }

        $('#frmView').attr('target','');

        // 쿠폰 사용기간 체크
        if ($('input:hidden[name="couponApplyNo[]"]').val()) {
            var checkCouponType = true;
            var couponApplyNo;
            $.ajax({
                method: "POST",
                cache: false,
                async: false,
                url: "../goods/goods_ps.php",
                data: {mode: 'goodsCheckCouponTypeArr', couponNo : $('input:hidden[name="couponApplyNo[]"]').val() },
                success: function (data) {
                    checkCouponType = data.isSuccess;
                    couponApplyNo = data.setCouponApplyNo.join('<?php echo INT_DIVISION?>');
                },
                error: function (e) {
                }
            });

            if(!checkCouponType) {
                $('input:hidden[name="couponApplyNo[]"]').val(couponApplyNo);
                alert('사용기간이 만료된 쿠폰이 포함되어 있어 제외 후 진행합니다.');
            }
        }

        if (modeStr == 'w' || typeof modeStr == 'undefined') {
            var params = $("#frmView").serialize();

            if (modeStr == 'w') {
                var ajaxUrl = '../mypage/wish_list_ps.php';
                var target = $("#addWishLayer");
            } else {
                var ajaxUrl = '../order/cart_ps.php';
                var target = $("#addCartLayer");
            }

            $.ajax({
                method: "POST",
                cache: false,
                url: ajaxUrl,
                data: params,
                success: function (data) {
                    // error 메시지 예외 처리용
                    if (!_.isUndefined(data.error) && data.error == 1) {
                        alert(data.message);
                        return false;
                    }

<?php if($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='y'||($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='n'&&$TPL_VAR["cartInfo"]["moveWishPageDeviceFl"]=='mobile')){?>
                    if (modeStr == 'w') {
                        location.href = "../mypage/wish_list.php";
                        return false;
                    }
<?php }?>
<?php if($TPL_VAR["cartInfo"]["moveCartPageFl"]=='y'||($TPL_VAR["cartInfo"]["moveCartPageFl"]=='n'&&$TPL_VAR["cartInfo"]["moveCartPageDeviceFl"]=='mobile')){?>
                    if (typeof modeStr == 'undefined') {
                        location.href = "../order/cart.php";
                        return false;
                    }
<?php }?>
                    target.removeClass('dn');
                    $('#layerDim').removeClass('dn');
                    target.find('> div').center();
                },
                error: function (data) {
                    alert(data.message);
                }
            });
        } else {
            $('#frmView').submit();
        }
    }

<?php if($TPL_VAR["goodsView"]['optionImagePreviewFl']=='y'){?>

    /**
     * 옵션 선택시 상세 이미지 변경
     *
     * @param integer optionNo 상품 배열 키값 (기본 0)
     */
    function gd_option_image_apply() {
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
        var optionImgSrc = $('select[name="optionSnoInput"] option:selected').data('img-src');
<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
        var optionImgSrc = $('select[name="optionNo_0"] option:selected').data('img-src');
<?php }?>

        if(optionImgSrc && optionImgSrc !='blank') $("#mainImage img").attr("src",optionImgSrc);
    }
<?php }?>

<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
    /**
     * 시간간격 카운트
     * @returns <?php echo $TPL_VAR["String"]?>

     */
    function gd_dailyMissionTimer(duration) {

        var timer = duration;
        var days, hours, minutes, seconds;

        var interval = setInterval(function(){
            days	= parseInt(timer / 86400, 10);
            hours	= parseInt(((timer % 86400 ) / 3600), 10);
            minutes = parseInt(((timer % 3600 ) / 60), 10);
            seconds = parseInt(timer % 60, 10);

            if(days == 0) {
                $('.time_day_view').hide();
            } else {
                days = days < 10 ? "0" + days : days;
                $('#displayTimeSaleDay').text(days);
            }

            hours 	= hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $('#displayTimeSaleTime').text(hours + " : " + minutes + " : " + seconds);

            $("#displayTimeSale").show();

            if (--timer < 0) {
                timer = 0;
                clearInterval(interval);
            }
        }, 1000);
    }
<?php }?>

    //-->
</script>
<script type="text/html" id="optionTemplate">
    <tbody id="option_display_item_<%=displayOptionkey%>">
    <tr class="check optionKey_<%=optionSno%>">
        <td class="cart_prdt_name">
            <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>" />
            <input type="hidden" name="optionSno[]" value="<%=optionSno%>" />
            <input type="hidden" name="goodsPriceSum[]" value="0" />
            <input type="hidden" name="addGoodsPriceSum[]" value="0" />
            <input type="hidden" name="displayOptionkey[]" value="<%=displayOptionkey%>" />
<?php if($TPL_VAR["couponUse"]=='y'){?>
            <input type="hidden" name="couponApplyNo[]" value="" />
            <input type="hidden" name="couponSalePriceSum[]" value="" />
            <input type="hidden" name="couponAddPriceSum[]" value="" />
<?php }?>
            <div class="cart_tit_box">
                <strong class="cart_tit">
                    <span><%=optionName%></span>
                    <span><%=optionSellCodeValue%><%=optionDeliveryCodeValue%></span>
<?php if($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                    <span class="cart_btn_box">
<?php if(gd_is_login()===false){?>
                            <button type="button" class="btn_alert_login"><img src="/data/skin/front/moment/img/common/btn/btn_coupon_apply.png" alt="<?php echo __('쿠폰적용')?>"/></button>
<?php }else{?>
                            <a href="#lyCouponApply" id="coupon_apply_<%=displayOptionkey%>" class="btn_open_layer" data-key="<%=displayOptionkey%>">
                                <img src="/data/skin/front/moment/img/common/btn/btn_coupon_apply.png" alt="<?php echo __('쿠폰적용')?>"/>
                            </a>
<?php }?>
                        </span>
<?php }?>
                    <span id="option_text_display_<%=displayOptionkey%>"></span>
                </strong>
            </div>
        </td>
        <td>
                <span class="count">
                    <span class="goods_qty">
                        <input type="text" class="text goodsCnt_<%=displayOptionkey%>" title="<?php echo __('수량')?>" name="goodsCnt[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-stock="<%=optionStock%>" data-key="<%=displayOptionkey%>" onchange="goodsViewController.input_count_change(this, '1');return false;" />
                        <span>
                            <button type="button" class="up goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?><%=displayOptionkey%>"><?php echo __('증가')?></button>
                            <button type="button" class="down goods_cnt" title="<?php echo __('감소')?>"  value="dn<?php echo STR_DIVISION?><%=displayOptionkey%>"><?php echo __('감소')?></button>
                        </span>
                    </span>
                </span>
        </td>
        <td class="item_choice_price">
            <input type="hidden" name="option_price_<%=displayOptionkey%>" value="<%=optionPrice%>" />
            <input type="hidden" name="optionPriceSum[]" value="0" />
            <?php echo gd_global_currency_symbol()?><strong class="option_price_display_<%=displayOptionkey%>"><%=optionPrice%></strong><?php echo gd_global_currency_string()?>

        </td>
        <td>
            <button type="button" class="delete_goods" data-key="option_display_item_<%=displayOptionkey%>"><img src="/data/skin/front/moment/img/icon/shop_cart/ico_cart_del.png" alt="<?php echo __('삭제')?>"/></button>
        </td>
    </tr>
    </tbody>
</script>
<script type="text/html" id="addGoodsTemplate">
    <tr id="add_goods_display_item_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" class="check item_choice_divide">
        <td class="cart_prdt_name">
            <div class="cart_tit_box">
                <input type="hidden" name="addGoodsNo[<%=optionIndex%>][]" value="<%=optionSno%>" data-group="<%=addGoodsGroup%>" />
                <strong class="item_choice_tit">
                    <em class="item_choice_photo"><%=addGoodsimge%></em><span><%=addGoodsName%></span>
                </strong>
            </div>
        </td>
        <td>
            <span class="count">
                <span class="goods_qty">
                    <input type="text" name="addGoodsCnt[<%=optionIndex%>][]" class="text addGoodsCnt_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" title="<?php echo __('수량')?>" value="1"  data-key="<%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"  data-value="1" data-stock-fl="<%=addGoodsStockFl%>"  data-stock="<%=addGoodsStock%>" onchange="goodsViewController.input_count_change(this);return false;">
                    <span>
                        <button type="button" class="up add_goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><?php echo __('증가')?></button>
                        <button type="button" class="down add_goods_cnt" title="<?php echo __('감소')?>" value="dn<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><?php echo __('감소')?></button>
                    </span>
                </span>
            </span>
        </td>
        <td class="item_choice_price">
            <input type="hidden" name="add_goods_price_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" value="<%=addGoodsPrice%>" />
            <input type="hidden" name="add_goods_total_price[<%=optionIndex%>][]" value="" />
            <?php echo gd_global_currency_symbol()?><strong class="add_goods_price_display_<%=displayOptionkey%>_<%=displayAddGoodsKey%>"></strong><?php echo gd_global_currency_string()?>

        </td>
        <td>
            <button type="button" class="delete_add_goods" data-key="<%=displayOptionkey%>-<%=displayAddGoodsKey%>"><img src="/data/skin/front/moment/img/icon/shop_cart/ico_cart_del.png" alt="<?php echo __('삭제')?>"/></button>
        </td>
    </tr>
</script>

<div class="content_box">
    <div class="location_wrap">
        <div class="location_cont">
            <em><a href="#" class="local_home">HOME</a></em>
<?php if($TPL_goodsCategoryList_1){foreach($TPL_VAR["goodsCategoryList"] as $TPL_V1){?>
<?php if($TPL_V1["cateNm"]){?>
            <span>&nbsp;&gt;&nbsp;</span>
            <div class="location_select">
                <div class="location_tit"><a href="#"><span><?php echo $TPL_V1["cateNm"]?></span></a></div>
                <ul style="display:none;">
<?php if((is_array($TPL_R2=$TPL_V1["data"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                    <li><a href="./goods_list.php?cateCd=<?php echo $TPL_K2?>"><span><?php echo $TPL_V2?></span></a></li>
<?php }}?>
                </ul>
            </div>
<?php }?>
<?php }}?>
        </div>
    </div>
    <!-- //location_wrap -->
    <!-- 상품 상단 -->
    <div class="item_photo_info_sec">
        <div class="item_photo_view_box">
            <div class="item_photo_view">
                <div class="item_photo_big">
                    <span class="img_photo_big"><a href="#lyZoom" id="mainImage" <?php if(gd_isset($TPL_VAR["goodsView"]['magnifyImage'])=='y'){?>class="zoom_layer_open btn_open_layer"<?php }?>><?php echo $TPL_VAR["goodsView"]['image']['detail']['img'][ 0]?></a></span>
                    <a href="#lyZoom" class="btn_zoom zoom_layer_open btn_open_layer"><img src="/data/skin/front/moment/img/icon/goods_icon/icon_zoom.png" alt=""></a>
                </div>
                <div id="testZoom" style="display:none">
                    <?php echo $TPL_VAR["goodsView"]['image']['magnify']['img'][ 0]?>

                </div>
                <!-- //item_photo_big -->
<?php if(in_array('goodsColor',$TPL_VAR["displayAddField"])){?>
                <?php echo $TPL_VAR["goodsView"]['goodsColor']?>

<?php }?>
                <div class="item_photo_slide">
                    <button type="button" class="slick_goods_prev"><img src="/data/skin/front/moment/img/icon/shop_cart/btn_slide_prev.png" alt="<?php echo __('이전 상품 이미지')?>"/></button>
                    <ul class="slider_wrap slider_goods_nav">
<?php if((is_array($TPL_R1=gd_isset($TPL_VAR["goodsView"]['image']['detail']['thumb']))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                        <li><a href="javascript:gd_change_image('<?php echo $TPL_K1?>', 'detail');"><?php echo $TPL_V1?></a></li>
<?php }}?>
                    </ul>
                    <button type="button" class="slick_goods_next"><img src="/data/skin/front/moment/img/icon/shop_cart/btn_slide_next.png" alt="<?php echo __('다음 상품 이미지')?>"/></button>
                </div>
                <!-- //item_photo_slide -->
            </div>
            <!-- //item_photo_view -->
        </div>
        <!-- //item_photo_view_box -->

        <form name="frmView" id="frmView" method="post">
            <input type="hidden" name="mode" value="cartIn" />
            <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
            <input type="hidden" name="cartMode" value="" />
            <input type="hidden" name="set_goods_price" value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />
            <input type="hidden" id="set_goods_fixedPrice" name="set_goods_fixedPrice" value="<?php echo gd_isset($TPL_VAR["goodsView"]['fixedPrice'], 0)?>" />
            <input type="hidden" name="set_goods_mileage" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsMileageBasic'], 0)?>" />
            <input type="hidden" name="set_goods_stock" value="<?php echo gd_isset($TPL_VAR["goodsView"]['stockCnt'], 0)?>" />
            <input type="hidden" name="set_coupon_dc_price" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>" />
            <input type="hidden" id="set_goods_total_price" name="set_goods_total_price" value="0" />
            <input type="hidden" id="set_option_price" name="set_option_price" value="0" />
            <input type="hidden" id="set_option_text_price" name="set_option_text_price" value="0" />
            <input type="hidden" id="set_add_goods_price" name="set_add_goods_price" value="0" />
            <input type="hidden" name="set_total_price" value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />
            <input type="hidden" name="mileageFl" value="<?php echo $TPL_VAR["goodsView"]['mileageFl']?>" />
            <input type="hidden" name="mileageGoods" value="<?php echo $TPL_VAR["goodsView"]['mileageGoods']?>" />
            <input type="hidden" name="mileageGoodsUnit" value="<?php echo $TPL_VAR["goodsView"]['mileageGoodsUnit']?>" />
            <input type="hidden" name="goodsDiscountFl" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountFl']?>" />
            <input type="hidden" name="goodsDiscount" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscount']?>" />
            <input type="hidden" name="goodsDiscountUnit" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountUnit']?>" />
            <input type="hidden" name="taxFreeFl" value="<?php echo $TPL_VAR["goodsView"]['taxFreeFl']?>" />
            <input type="hidden" name="taxPercent" value="<?php echo $TPL_VAR["goodsView"]['taxPercent']?>" />
            <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
            <input type="hidden" name="brandCd" value="<?php echo $TPL_VAR["goodsView"]['brandCd']?>" />
            <input type="hidden" name="cateCd" value="<?php echo $TPL_VAR["goodsView"]['cateCd']?>" />
            <input type="hidden" id="set_dc_price" value="0" />
            <input type="hidden" id="goodsOptionCnt" value="1" />
            <input type="hidden" name="optionFl" value="<?php echo $TPL_VAR["goodsView"]['optionFl']?>" />
<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['mileageFl']=='n'){?>
            <input type="hidden" name="goodsMileageExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['memberDcFl']=='n'){?>
            <input type="hidden" name="memberBenefitExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['couponFl']=='n'){?>
            <input type="hidden" name="couponBenefitExcept" value="y" />
<?php }?>
<?php }?>
            <input type="hidden" name="goodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['goodsDeliveryFl']?>" />
            <input type="hidden" name="sameGoodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['sameGoodsDeliveryFl']?>" />
            <input type="hidden" name="useBundleGoods" value="1" />
            <div class="item_info_box">
<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
                <div id="displayTimeSale" class="time_sale" style="display:none">
                    <strong class="time_sale_num"><?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['benefit']?><span>%</span> <span class="skip"><?php echo __('타임세일')?></span></strong>
                    <strong class="time_day">
                        <span id="displayTimeSaleDay" class="time_day_view"></span><b><?php echo __('일')?></b>
                        <span id="displayTimeSaleTime"></span>
                    </strong>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['orderCntDisplayFl']=='y'){?>
                    <strong class="time_now_order"><?php echo __('현재')?> <span><?php echo number_format($TPL_VAR["goodsView"]['timeSaleInfo']['orderCnt'])?></span><?php echo __('개 구매')?></strong>
<?php }?>
                </div>
<?php }?>
                <!-- //time_sale -->
                <div class="item_tit_detail_cont">
                    <div class="item_detail_tit">
<?php if(in_array('goodsIcon',$TPL_VAR["displayAddField"])&&$TPL_VAR["goodsView"]['goodsIcon']){?>
                        <div class="prd_icon"><?php echo $TPL_VAR["goodsView"]['goodsIcon']?></div>
<?php }?>
                        <h3><?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsNmDescription']){?>[<?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['goodsNmDescription']?>]<?php }?><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></h3>
                        <div class="btn_layer btn_qa_share_box">
<?php if($TPL_VAR["goodsView"]['qrCodeFl']=='y'){?>
                            <span class="btn_gray_list"><a href="#lyQrcode" class="btn_gray_mid"><em><?php echo __('QR 코드')?></em></a></span>
<?php }?>
<?php if($TPL_VAR["snsShareUseFl"]){?>
                            <span class="btn_gray_list target_sns_share"><a href="#lySns" class="btn_gray_mid"><em><?php echo __('공유')?></em></a></span>
<?php }?>
<?php if($TPL_VAR["goodsView"]['qrCodeFl']=='y'){?>
                            <div id="lyQrcode" class="layer_area" style="display:none;">
                                <div class="ly_wrap qrcode_layer">
                                    <div class="ly_tit">
                                        <strong><?php echo __('QR 코드')?></strong>
                                    </div>
                                    <div class="ly_cont">
                                        <div class="qrcode_list">
                                            <div class="qrcode_img_box">
                                                <strong><img src="<?php echo $TPL_VAR["goodsView"]['qrCodeImage']?>" id="qrCodeImage" alt="" /></strong>
<?php if($TPL_VAR["goodsView"]['qrStyle']=='btn'){?>
                                                <span class="btn_gray_list"><a href="#download" id="qrCodeDownloadButton" class="btn_gray_mid"><em class="icon_download"><?php echo __('QR코드 저장하기')?></em></a></span>
<?php }?>
                                            </div>
                                            <dl>
                                                <dt><?php echo __('QR코드 인식방법')?></dt>
                                                <dd>
                                                    01 <?php echo __('QR코드 앱 다운로드')?><br />
                                                    02 <?php echo __('스마트폰으로 QR코드 인식')?><br />
                                                    03 <?php echo __('쇼핑몰 상품페이지로 이동')?>

                                                </dd>
                                            </dl>
                                            <dl>
                                                <dt><?php echo __('QR코드란?')?></dt>
                                                <dd><?php echo __('QR코드(QR code)는 흑백격자무늬 패턴으로 %s정보를 나타내는 매트릭스 형식의 바코드입니다.','<br />')?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <!-- //ly_cont -->
                                    <a href="#close" class="ly_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                </div>
                                <!-- //ly_wrap -->
                            </div>
                            <!-- //layer_area -->
<?php }?>

<?php if($TPL_VAR["snsShareUseFl"]){?>
                            <div id="lySns" class="layer_area" style="display:none;">
                                <div class="ly_wrap sns_layer">
                                    <div class="ly_tit">
                                        <strong>SNS <?php echo __('공유하기')?></strong>
                                    </div>
                                    <div class="ly_cont">
                                        <div class="sns_list">
                                            <ul>
<?php if($TPL_snsShareButton_1){foreach($TPL_VAR["snsShareButton"] as $TPL_V1){?>
                                                <?php echo $TPL_V1?>

<?php }}?>
                                            </ul>
<?php if($TPL_VAR["snsShareUrl"]){?>
                                            <div class="sns_copy_url">
                                                <input type="text" value="<?php echo $TPL_VAR["snsShareUrl"]?>">
                                                <button type="button" class="gd_clipboard" title="<?php echo __('상품주소')?>" data-clipboard-text="<?php echo $TPL_VAR["snsShareUrl"]?>"><em><?php echo __('URL복사')?></em></button>
                                            </div>
<?php }?>
                                        </div>
                                    </div>
                                    <!-- //ly_cont -->
                                    <a href="#close" class="ly_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                </div>
                                <!-- //ly_wrap -->
                            </div>
                            <!-- //layer_area -->
<?php }?>
                        </div>
                        <!-- //btn_qa_share_box -->
                    </div>
                    <div class="item_detail_list">
<?php if($TPL_displayField_1){foreach($TPL_VAR["displayField"] as $TPL_V1){?>
<?php switch($TPL_V1){case 'shortDescription':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['shortDescription'])){?>
                        <dl>
                            <dt><?php echo __('짧은설명')?></dt>
                            <dd><?php echo $TPL_VAR["goodsView"]['shortDescription']?></dd>
                        </dl>
<?php }?>
<?php break;case 'fixedPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['fixedPrice'])> 0&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dl>
                            <dt><?php echo __('정가')?></dt>
                            <dd>
                                <?php echo $TPL_VAR["fixedPriceTag"]?><span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['fixedPrice'])?></span><?php echo gd_global_currency_string()?> <?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['fixedPrice'])?><?php echo $TPL_VAR["fixedPriceTag2"]?>

                            </dd>
                        </dl>
<?php }?>
<?php break;case 'couponPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['couponPrice'])> 0&&$TPL_VAR["couponUse"]=='y'&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dl class="item_price">
                            <dt><?php echo __('쿠폰적용가')?></dt>
                            <dd>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['couponPrice'])?></strong><strong><?php echo gd_global_currency_string()?></strong>
                                <strong class="item_apply">( -<?php echo gd_global_money_format($TPL_VAR["goodsView"]['couponSalePrice'])?><?php echo gd_global_currency_string()?><?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['couponDcRate'])){?>, <?php echo $TPL_VAR["goodsView"]['couponDcRate']?>%<?php }?>)</strong>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'myCouponPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['myCouponSalePrice'])> 0&&$TPL_VAR["couponUse"]=='y'&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dl class="item_price">
                            <dt><?php echo __('내 쿠폰적용가')?></dt>
                            <dd>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['myCouponSalePrice'])?></strong><strong><?php echo gd_global_currency_string()?></strong>
                                <strong class="item_apply">( -<?php echo gd_global_money_format($TPL_VAR["goodsView"]['myCouponPrice'])?><?php echo gd_global_currency_string()?><?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['myCouponDcRate'])){?>, <?php echo $TPL_VAR["goodsView"]['myCouponDcRate']?>%<?php }?>)</strong>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'goodsPrice':?>
                        <dl class="<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>time_sale_price<?php }else{?>item_price<?php }?>">
                            <dt><?php if($TPL_VAR["goodsView"]['timeSaleFl']){?><?php echo __('타임세일가')?><?php }else{?><?php echo __('판매가')?><?php }?></dt>
                            <dd>
<?php if($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='text'){?>
                                <strong><?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_text"]?></strong>
<?php }elseif($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='custom'){?>
                                <img src="<?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_img"]?>">
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['goodsPriceString']!=''){?>
                                <strong><?php echo $TPL_VAR["goodsView"]['goodsPriceString']?></strong>
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsPriceViewFl']=='y'&&$TPL_VAR["goodsView"]['oriGoodsPrice']> 0){?>
                                <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['oriGoodsPrice'])?><?php echo gd_global_currency_string()?></del>
<?php }?>
                                <strong><?php if($TPL_VAR["goodsView"]['timeSaleFl']){?><img src="/data/skin/front/moment/img/icon/goods_icon/icon_time.png" alt="<?php echo __('타임세일가')?>"><?php }?><?php echo $TPL_VAR["goodsPriceTag"]?><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?><?php echo $TPL_VAR["goodsPriceTag2"]?></strong><?php echo gd_global_currency_string()?>

                                <!-- 글로벌 참조 화폐 임시 적용 -->
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <em class="add_currency"><?php echo $TPL_VAR["goodsPriceTag"]?><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['goodsPrice'])?><?php echo $TPL_VAR["goodsPriceTag2"]?></em>
<?php }?>
<?php }?>
<?php }?>
                            </dd>
                        </dl>
<?php break;case 'goodsDiscount':?>
<?php if($TPL_VAR["goodsView"]['dcPrice']> 0){?>
                        <dl class="item_price">
                            <dt><?php echo __('할인적용가')?></dt>
                            <dd>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice']-$TPL_VAR["goodsView"]['dcPrice'])?></strong><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <em class="add_currency"><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['goodsPrice']-$TPL_VAR["goodsView"]['dcPrice'])?></em>
<?php }?>
<?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['goodsDcRate'])){?> <strong class="item_apply">(<?php echo $TPL_VAR["goodsView"]['goodsDcRate']?>%)</strong><?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'maxOrderCnt':?>
<?php if(!($TPL_VAR["goodsView"]['minOrderCnt']==='1'&&$TPL_VAR["goodsView"]['maxOrderCnt']==='0')){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='option'){?>옵션당
<?php }elseif($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>상품당
<?php }elseif($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>ID당
<?php }?>
<?php if($TPL_VAR["goodsView"]['minOrderCnt']){?><?php echo __('최소 %s개',$TPL_VAR["goodsView"]['minOrderCnt'])?><?php }?>
<?php if($TPL_VAR["goodsView"]['maxOrderCnt']){?> / <?php echo __('최대 %s개',$TPL_VAR["goodsView"]['maxOrderCnt'])?><?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'benefit':?>
                        <dl class="item_discount_mileage dn">
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd>
                                <span class="item_discount"><?php echo __('할인')?> : <strong class="total_benefit_price"></strong> <strong class="benefit_price item_apply"></strong></span>
                                <span class="item_mileage"><?php echo __('적립')?> <?php echo gd_display_mileage_name()?> : <strong class="total_benefit_mileage"></strong> <strong class="benefit_mileage item_apply"></strong></span>
                            </dd>
                        </dl>
<?php break;case 'couponDownload':?>
<?php if(empty($TPL_VAR["couponArrData"])===false){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
<?php if(gd_is_login()===false){?>
                            <dd><span class="btn_gray_list"><a href="#;" id="lnCouponDown" class="btn_gray_small btn_alert_login"><em class="icon_download"><?php echo __('쿠폰 다운받기')?></em></a></span></dd>
<?php }else{?>
                            <dd><span class="btn_gray_list"><a href="#lyCouponDown" class="btn_gray_small btn_open_layer"><em class="icon_download"><?php echo __('쿠폰 다운받기')?></em></a></span></dd>
<?php }?>
                        </dl>
<?php }?>
<?php break;case 'delivery':?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                        <dl class="item_delivery">
                            <dt><?php echo __('배송비')?></dt>
                            <dd>
<?php if($TPL_VAR["goodsView"]['multipleDeliveryFl']===true){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['firstCharge']['0']['price'])?><?php echo gd_global_currency_string()?></strong>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <strong><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['firstCharge']['0']['price'])?></strong>
<?php }?>
<?php }else{?>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['firstCharge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?><?php echo gd_global_currency_string()?></strong>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <strong><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['firstCharge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?></strong>
<?php }?>
<?php }?>
                                <div class="btn_layer">
                                    <span class="btn_gray_list"><a href="#lyDelivery" class="btn_gray_small"><em><?php echo __('조건별배송')?></em></a></span>
                                    <div id="lyDelivery" class="layer_area" style="display:none;">
                                        <div class="ly_wrap ly_dev_wrap delivery_layer">
                                            <div class="ly_tit"><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['fixFlText']?></div>
                                            <div class="ly_cont">
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='fixed'){?>
                                                <div class="ly_btn <?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2){?>two<?php }else{?>three<?php }?>">
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_K2=>$TPL_V2){$TPL_I2++;?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2&&$TPL_I2% 2== 0){?><div class="row"><?php }else{?><?php if($TPL_I2% 3== 0){?><div class="row"><?php }?><?php }?>
                                                    <span class="delivery-method" onclick="selectDeliveryMethod(this, '<?php echo $TPL_K2?>')"><?php echo $TPL_V2?></span>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2&&$TPL_I2% 2== 1){?></div><?php }else{?><?php if($TPL_I2% 3== 2){?></div><?php }else{?><?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])-$TPL_I2< 3&&((count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])% 3== 1&&$TPL_I2% 3== 0)||(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])% 3== 2&&$TPL_I2% 3== 1))){?></div><?php }?><?php }?><?php }?>
<?php }}?>
												</div>
                                                <script type="text/javascript">
                                                    function selectDeliveryMethod(e, method) {
                                                        $(e).parents(".ly_btn").find("span").removeClass("on");
                                                        $(e).addClass("on");
                                                        $('.delivery-method-li').hide();
                                                        $('.delivery-method-li').filter('[data-method="' + method + '"]').show();
                                                    }
                                                    $(function(){
                                                        $('.delivery-method').eq(0).trigger('click');
                                                    })
                                                </script>
<?php }?>
                                                <div class="delivery_list">
                                                    <ul>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['multiCharge'][ 0])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if(gd_isset($TPL_V2["sno"])){?>
                                                        <li>
                                                            <?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'][$TPL_K2]?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }}?>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['rangeRepeat'])!='y'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_K3=>$TPL_V3){?>
<?php if(gd_isset($TPL_V3["sno"])){?>
<?php if($TPL_V3["unitEnd"]> 0){?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo gd_global_money_format($TPL_V3["unitStart"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?> ~ <?php echo gd_global_money_format($TPL_V3["unitEnd"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo gd_global_money_format($TPL_V3["unitStart"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_K3=>$TPL_V3){?>
<?php if(gd_isset($TPL_V3["sno"])){?>
<?php if($TPL_V3["unitEnd"]> 0){?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo number_format($TPL_V3["unitStart"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V3["unitEnd"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo number_format($TPL_V3["unitStart"])?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }else{?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_K3=>$TPL_V3){?>
<?php if(gd_isset($TPL_V3["sno"])){?>
<?php if($TPL_V3["unitEnd"]> 0){?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo number_format($TPL_V3["unitStart"], 2)?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V3["unitEnd"], 2)?><?php echo $TPL_V3["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
                                                            <?php echo number_format($TPL_V3["unitStart"], 2)?><?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }?>
<?php }else{?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_V2){$TPL_I2++;?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_K3=>$TPL_V3){?>
<?php if(gd_isset($TPL_V3["sno"])){?>
<?php if($TPL_I2== 0){?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                            <?php echo gd_global_money_format($TPL_V3["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                            <?php echo number_format($TPL_V3["unitStart"])?>

<?php }else{?>
                                                            <?php echo number_format($TPL_V3["unitStart"], 2)?>

<?php }?>
                                                            <?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?> ~
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                            <?php echo gd_global_money_format($TPL_V3["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                            <?php echo number_format($TPL_V3["unitEnd"])?>

<?php }else{?>
                                                            <?php echo number_format($TPL_V3["unitEnd"], 2)?>

<?php }?>
                                                            <?php echo $TPL_V3["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li class="delivery-method-li" data-method="<?php echo $TPL_K3?>">
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                            <?php echo gd_global_money_format($TPL_V3["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                            <?php echo number_format($TPL_V3["unitStart"])?>

<?php }else{?>
                                                            <?php echo number_format($TPL_V3["unitStart"], 2)?>

<?php }?>
                                                            <?php echo $TPL_V3["unitText"]?> <?php echo __('이상')?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                            <?php echo gd_global_money_format($TPL_V3["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                            <?php echo number_format($TPL_V3["unitEnd"])?>

<?php }else{?>
                                                            <?php echo number_format($TPL_V3["unitEnd"], 2)?>

<?php }?>
                                                            <?php echo $TPL_V3["unitText"]?> <?php echo __('마다 추가')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V3["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V3["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }?>
<?php }?>
                                                    </ul>
                                                </div>
                                            </div>
<?php if($TPL_VAR["goodsView"]["delivery"]["basic"]["fixFl"]=='price'){?>
                                            <p class="chk_none">
                                                <?php echo __('배송비 계산 기준 : 판매가')?>

<?php if(in_array('option',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                + <?php echo __('옵션가')?>

<?php }?>
<?php if(in_array('add',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                + <?php echo __('추가상품가')?>

<?php }?>
<?php if(in_array('text',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                + <?php echo __('텍스트옵션가')?>

<?php }?>
<?php if(in_array('goods',$TPL_VAR["goodsView"]["delivery"]["basic"]["priceMinusStandard"])){?>
                                                - <?php echo __('상품할인가')?>

<?php }?>
<?php if(in_array('coupon',$TPL_VAR["goodsView"]["delivery"]["basic"]["priceMinusStandard"])){?>
                                                - <?php echo __('상품쿠폰할인가')?>

<?php }?>
                                            </p>
<?php }?>
                                            <!-- //ly_cont -->
                                            <a href="#close" class="ly_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                        </div>
                                    </div>
                                </div>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='free'){?>
                                <strong><?php echo __('무료')?></strong>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?><?php echo gd_global_currency_string()?></strong>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <strong><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?></strong>
<?php }?>
<?php }else{?>
                                <strong><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?><?php echo gd_global_currency_string()?></strong>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <strong><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?></strong>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='pre'){?>
                                <input type="hidden" name="deliveryCollectFl" value="pre" />
                                <strong> / <?php echo __('주문시결제')?>(<?php echo __('선결제')?>)</strong>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='later'){?>
                                <input type="hidden" name="deliveryCollectFl" value="later" />
                                <strong> / <?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)</strong>
<?php }?>
                                <span class="btn_layer">
                                    <span class="btn_gray_list"><a href="#lyDelivery" class="btn_gray_small"><em><?php echo __('조건별배송')?></em></a></span>
                                    <div id="lyDelivery" class="layer_area" style="display:none;">
                                        <div class="ly_wrap delivery_layer">
                                            <div class="ly_tit"><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['fixFlText']?></div>
                                            <div class="ly_cont">
                                                <div class="delivery_list">
                                                    <ul>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['rangeRepeat'])!='y'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                                                        <li>
                                                            <?php echo gd_global_money_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo gd_global_money_format($TPL_V2["unitEnd"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li>
                                                            <?php echo gd_global_money_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }}?>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                                                        <li>
                                                            <?php echo number_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V2["unitEnd"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
						                                        <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li>
                                                            <?php echo number_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
						                                        <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }}?>
<?php }else{?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                                                        <li>
                                                            <?php echo number_format($TPL_V2["unitStart"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V2["unitEnd"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
						                                        <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }else{?>
                                                        <li>
                                                            <?php echo number_format($TPL_V2["unitStart"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?>

                                                            <span>
                                                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
						                                        <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                            </span>
                                                        </li>
<?php }?>
<?php }}?>
<?php }?>
<?php }else{?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_V2){$TPL_I2++;?>
<?php if($TPL_I2== 0){?>
                                                                     <li>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                                            <?php echo gd_global_money_format($TPL_V2["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                                            <?php echo number_format($TPL_V2["unitStart"])?>

<?php }else{?>
                                                                            <?php echo number_format($TPL_V2["unitStart"], 2)?>

<?php }?>
                                                                        <?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                                            <?php echo gd_global_money_format($TPL_V2["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                                            <?php echo number_format($TPL_V2["unitEnd"])?>

<?php }else{?>
                                                                            <?php echo number_format($TPL_V2["unitEnd"], 2)?>

<?php }?>
                                                                        <?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?>


                                                                         <span>
                                                                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                            <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                                         </span>
                                                                     </li>
<?php }else{?>
                                                                    <li>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                                            <?php echo gd_global_money_format($TPL_V2["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                                            <?php echo number_format($TPL_V2["unitStart"])?>

<?php }else{?>
                                                                            <?php echo number_format($TPL_V2["unitStart"], 2)?>

<?php }?>
                                                                        <?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                                                            <?php echo gd_global_money_format($TPL_V2["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                                                            <?php echo number_format($TPL_V2["unitEnd"])?>

<?php }else{?>
                                                                            <?php echo number_format($TPL_V2["unitEnd"], 2)?>

<?php }?>
                                                                        <?php echo $TPL_V2["unitText"]?> <?php echo __('마다 추가')?>

                                                                        <span>
                                                                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                                            <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                                                                        </span>
                                                                    </li>
<?php }?>
<?php }}?>
<?php }?>
                                                    </ul>
                                                </div>
<?php if($TPL_VAR["goodsView"]["delivery"]["basic"]["fixFl"]=='price'){?>
                                                <p class="chk_none">
                                                    <?php echo __('배송비 계산 기준 : 판매가')?>

<?php if(in_array('option',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                    + <?php echo __('옵션가')?>

<?php }?>
<?php if(in_array('add',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                    + <?php echo __('추가상품가')?>

<?php }?>
<?php if(in_array('text',$TPL_VAR["goodsView"]["delivery"]["basic"]["pricePlusStandard"])){?>
                                                    + <?php echo __('텍스트옵션가')?>

<?php }?>
<?php if(in_array('goods',$TPL_VAR["goodsView"]["delivery"]["basic"]["priceMinusStandard"])){?>
                                                    - <?php echo __('상품할인가')?>

<?php }?>
<?php if(in_array('coupon',$TPL_VAR["goodsView"]["delivery"]["basic"]["priceMinusStandard"])){?>
                                                    - <?php echo __('상품쿠폰할인가')?>

<?php }?>
                                                </p>
<?php }?>
                                            </div>
                                            <!-- //ly_cont -->
                                            <a href="#close" class="ly_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                        </div>
                                    </div>
                                </span>
<?php }?>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['areaFl'])=='y'){?>
                                <span class="btn_layer">
                                    <span class="btn_gray_list"><a href="#lyDeliveryZone" class="btn_gray_small"><em><?php echo __('지역별추가배송비')?></em></a></span>
                                    <div id="lyDeliveryZone" class="layer_area" style="display:none;">
                                        <div class="ly_wrap delivery_zone_layer">
                                            <div class="ly_tit"><strong><?php echo __('지역별배송비')?></strong></div>
                                            <div class="ly_cont">
                                                <div class="delivery_list">
                                                    <ul>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['areaDetail'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                                        <li>
                                                            <?php echo $TPL_V2["addArea"]?>

                                                            <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["addPrice"])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                                            <span><?php echo gd_global_add_currency_display($TPL_V2["addPrice"])?></span>
<?php }?>
                                                        </li>
<?php }}?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- //ly_cont -->
                                            <a href="#close" class="ly_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
                                        </div>
                                        <!-- //ly_wrap -->
                                    </div>
                                    <!-- //layer_area -->
                                </span>
<?php }?>
                                <div class="delivery-detail">
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])> 0){?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 1){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                                    <input type="hidden" name="deliveryMethodFl" value="<?php echo $TPL_K2?>" />
                                    <div class="delivery-division"><?php echo $TPL_V2?></div>
<?php }}?>
<?php }else{?>
                                    <select class="js-deliveryMethodFl chosen-select" name="deliveryMethodFl" style="min-width: 100px; width: 100px; max-width: 480px;">
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                                        <option value="<?php echo $TPL_K2?>"><?php echo $TPL_V2?></option>
<?php }}?>
                                    </select>
<?php }?>
<?php }?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='free'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])!='pre'&&gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])!='later'){?>
                                    <div class="delivery-division">&nbsp;/&nbsp;</div>
                                    <select name="deliveryCollectFl" class="chosen-select">
                                        <option value="pre"><?php echo __('주문시결제')?>(<?php echo __('선결제')?>)</option>
                                        <option value="later"><?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)</option>
                                    </select>
<?php }?>
<?php }?>

<?php if($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodVisitArea']&&$TPL_VAR["goodsView"]['delivery']['basic']['dmVisitTypeDisplayFl']!='y'){?>
                                    <div class="js-deliveryMethodVisitArea <?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])!= 1||!$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData']['visit']){?>dn<?php }?>">
                                        <?php echo __('방문 수령지')?> : <?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodVisitArea']?>

                                    </div>
<?php }?>
                                </div>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'goodsWeight':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsWeight'])> 0&&gd_isset($TPL_VAR["goodsView"]['goodsVolume'])> 0){?>
                        <dl>
                            <dt style="width: 85px;"><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo $TPL_VAR["goodsView"]['goodsWeight']?><?php echo $TPL_VAR["weight"]["unit"]?> / <?php echo $TPL_VAR["goodsView"]['goodsVolume']?><?php echo gd_isset($TPL_VAR["volume"]["unit"],'㎖')?></dd>
                        </dl>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsWeight'])> 0){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo $TPL_VAR["goodsView"]['goodsWeight']?><?php echo $TPL_VAR["weight"]["unit"]?></dd>
                        </dl>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsVolume'])> 0){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo $TPL_VAR["goodsView"]['goodsVolume']?><?php echo gd_isset($TPL_VAR["volume"]["unit"],'㎖')?></dd>
                        </dl>
<?php }?>
<?php }?>
<?php break;case 'addInfo':?>
<?php if(empty($TPL_VAR["goodsView"]['addInfo'])===false){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['addInfo'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <dl>
                            <dt><?php echo $TPL_V2["infoTitle"]?></dt>
                            <dd><?php echo $TPL_V2["infoValue"]?></dd>
                        </dl>
<?php }}?>
<?php }?>
<?php break;case 'effectiveStartYmd':?>
<?php if(($TPL_VAR["goodsView"]['effectiveStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveStartYmd'])||($TPL_VAR["goodsView"]['effectiveEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveEndYmd'])){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd>
<?php if($TPL_VAR["goodsView"]['effectiveStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveStartYmd']){?>
                                <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['effectiveStartYmd'])?>

<?php }?>
<?php if($TPL_VAR["goodsView"]['effectiveEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveEndYmd']){?>
                                ~ <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['effectiveEndYmd'])?>

<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'salesStartYmd':?>
<?php if(($TPL_VAR["goodsView"]['salesStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesStartYmd'])||($TPL_VAR["goodsView"]['salesEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesEndYmd'])){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd>
<?php if($TPL_VAR["goodsView"]['salesStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesStartYmd']){?>
                                <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['salesStartYmd'])?>

<?php }?>
<?php if($TPL_VAR["goodsView"]['salesEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesEndYmd']){?>
                                ~ <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['salesEndYmd'])?>

<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php break;case 'salesUnit':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['salesUnit'])> 1){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo __('%s개',number_format($TPL_VAR["goodsView"]['salesUnit']))?></dd>
                        </dl>
<?php }?>
<?php break;case 'totalStock':?>
<?php if($TPL_VAR["goodsView"]['stockFl']=='y'){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo __('%s개',number_format($TPL_VAR["goodsView"]['totalStock']))?></dd>
                        </dl>
<?php }?>
<?php break;default:?>
<?php if(gd_isset($TPL_VAR["goodsView"][$TPL_V1])){?>
                        <dl>
                            <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                            <dd><?php echo $TPL_VAR["goodsView"][$TPL_V1]?></dd>
                        </dl>
<?php }?>
<?php }?>
<?php }}?>
<?php if($TPL_VAR["naverPay"]){?>
                        <dl id="naver-mileage-accum" style="display: none;">
                            <dt><?php echo __('네이버')?>&nbsp;&nbsp;<br/><?php echo __('마일리지')?></dt>
                            <dd><span id="naver-mileage-accum-rate"></span> <?php echo __('적립')?></dd>
                        </dl>
<?php }?>
                        <?php echo $TPL_VAR["myappGoodsBenefitMessage"]?>

<?php if($TPL_VAR["goodsView"]['optionFl']=='y'||$TPL_VAR["goodsView"]['optionTextFl']=='y'||$TPL_VAR["goodsView"]['addGoods']){?>
                        <div class="item_add_option_box">
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
                            <dl>
                                <dt><?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='one'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?><?php echo $TPL_VAR["goodsView"]['optionName']?><?php }else{?><?php echo __('옵션 선택')?><?php }?></dt>
                                <dd>
                                    <select name="optionSnoInput" class="chosen-select" <?php if($TPL_VAR["goodsView"]['orderPossible']=='y'){?>onchange="<?php if($TPL_VAR["goodsView"]['optionImagePreviewFl']=='y'){?>gd_option_image_apply();<?php }?>goodsViewController.option_price_display(this);"<?php }else{?>disabled="disabled"<?php }?>>
                                    <option value="">
                                        =
<?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='many'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?><?php echo $TPL_VAR["goodsView"]['optionName']?>

<?php }else{?><?php echo __('옵션')?>

<?php }?> : <?php echo __('가격')?>

<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>: <?php echo __('재고')?><?php }?>
                                        =
                                    </option>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['option'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["optionViewFl"]=='y'){?>
                                    <option  <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_V1["optionImage"]){?>data-img-src="<?php echo $TPL_V1["optionImage"]?>"<?php }else{?>data-img-src="blank"<?php }?><?php }?> value="<?php echo $TPL_V1["sno"]?><?php echo INT_DIVISION?><?php echo gd_global_money_format($TPL_V1["optionPrice"],false)?><?php echo INT_DIVISION?><?php echo $TPL_V1["mileage"]?><?php echo INT_DIVISION?><?php echo $TPL_V1["stockCnt"]?><?php echo STR_DIVISION?><?php echo $TPL_V1["optionValue"]?><?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["optionSellFl"]=='t')){?><?php echo STR_DIVISION?><?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?><?php }?><?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]!=''){?><?php echo STR_DIVISION?><?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?><?php }?>"<?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['fixedOrderCnt']=='option'&&$TPL_V1["stockCnt"]<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]=='0')||$TPL_V1["optionSellFl"]=='n'||$TPL_V1["optionSellFl"]=='t'){?> disabled="disabled"<?php }?>>
                                    <?php echo $TPL_V1["optionValue"]?>

<?php if(gd_isset($TPL_V1["optionPrice"])!='0'){?> : <?php echo gd_global_currency_symbol()?><?php if(gd_isset($TPL_V1["optionPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V1["optionPrice"])?><?php echo gd_global_currency_string()?><?php }?>
<?php if(($TPL_V1["optionSellFl"]=='t')){?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?>]
<?php }elseif(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]=='0')||$TPL_V1["optionSellFl"]=='n'){?>[<?php echo $TPL_VAR["optionSoldOutCode"]['n']?>]
<?php }else{?>
<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])&&$TPL_VAR["goodsView"]['stockFl']=='y'){?> : <?php echo number_format($TPL_V1["stockCnt"])?><?php echo __('개')?>

<?php }?>
<?php }?>
<?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]!=''){?>[<?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?>]
<?php }?>
                                    </option>
<?php }?>
<?php }}?>
                                    </select>
                                </dd>
                            </dl>
<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionName'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?>
                            <input type="hidden" name="optionSnoInput" value="" />
                            <input type="hidden" name="optionCntInput" value="<?php echo $TPL_S1?>" />
<?php }?>
                            <dl>
                                <dt><?php echo $TPL_V1?></dt>
                                <dd>
                                    <select name="optionNo_<?php echo $TPL_I1?>" class="chosen-select" <?php if($TPL_VAR["goodsView"]['orderPossible']=='y'){?>onchange="<?php if($TPL_I1== 0&&$TPL_VAR["goodsView"]['optionImagePreviewFl']=='y'){?>gd_option_image_apply();<?php }?>goodsViewController.option_select(this, '<?php echo $TPL_I1?>', '<?php echo gd_isset($TPL_VAR["goodsView"]['optionName'][($TPL_I1+ 1)])?>', '<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>y<?php }else{?>n<?php }?>');"<?php }?><?php if($TPL_I1> 0||$TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>>
                                    <option value="">
                                        =
<?php if($TPL_I1== 0){?><?php echo $TPL_V1?> <?php echo __('선택')?>

<?php }else{?><?php echo __('%s을 먼저 선택해 주세요',$TPL_VAR["goodsView"]['optionName'][($TPL_I1- 1)])?>

<?php }?>
                                        =
                                    </option>
<?php if($TPL_I1== 0){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['optionDivision'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                                    <option <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]){?>data-img-src="<?php echo $TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]?>"<?php }else{?>data-img-src="blank"<?php }?><?php }?> value="<?php echo $TPL_V2?>" <?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['fixedOrderCnt']=='option'&&isset($TPL_VAR["goodsView"]['optionDivisionStock'])&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']=='0')||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='n'||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='t'){?> disabled="disabled"<?php }?>>
                                    <?php echo $TPL_V2?>

<?php if(($TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='t')){?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellCode']]?>]
<?php }elseif(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']=='0')||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='n'){?>[<?php echo $TPL_VAR["optionSoldOutCode"]['n']?>]
<?php }?>
                                    </option>
<?php }}?>
<?php }?>
                                    </select>
                                </dd>
                            </dl>
                            <div id="iconImage_<?php echo $TPL_I1?>" class="option_icon"></div>
<?php }}?>
<?php }?>
<?php }?>
<?php if($TPL_VAR["goodsView"]['optionTextFl']=='y'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionText'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                            <dl>
<?php if($TPL_I1== 0){?>
                                <input type="hidden" id="optionTextCnt" value="<?php echo $TPL_S1?>" />
<?php }?>
                                <dt>
                                    <input type="hidden" name="optionTextMust_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["mustFl"]?>" />
                                    <input type="hidden" name="optionTextLimit_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["inputLimit"]?>" />
                                    <span class="optionTextNm_<?php echo $TPL_I1?>"><?php echo $TPL_V1["optionName"]?><?php if($TPL_V1["mustFl"]=='y'){?><strong> (<?php echo __('필수')?>)</strong><?php }?></span>
                                </dt>
                                <dd>
                                    <input type="hidden" name="optionTextSno_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["sno"]?>"/>
                                    <input type="text" name="optionTextInput_<?php echo $TPL_I1?>" value="" size="30" class="text" onkeydown="goodsViewController.enterKey(this)" onkeyup="goodsViewController.max_length_alert(this)" onchange="goodsViewController.option_text_select(this)" placeholder="<?php echo $TPL_V1["inputLimit"]?><?php echo __('글자를 입력하세요.')?>" maxlength="<?php echo $TPL_V1["inputLimit"]+ 1?>"<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>/>
                                    <input type="hidden" value="<?php echo $TPL_V1["addPrice"]?>"/>
<?php if($TPL_V1["addPrice"]!= 0){?>
                                    <strong>※ <?php echo __('작성시 %s%s%s 추가',gd_global_currency_symbol(),gd_global_money_format($TPL_V1["addPrice"]),gd_global_currency_string())?></strong>
<?php }?>
                                </dd>
                            </dl>
<?php }}?>
<?php }?>
<?php if($TPL_VAR["goodsView"]['addGoods']){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['addGoods'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                            <dl>
                                <dt>
                                    <?php echo $TPL_V1["title"]?><?php if($TPL_V1["mustFl"]=='y'){?><strong> (<?php echo __('필수')?>)</strong><input type="hidden" name="addGoodsInputMustFl[]" value="<?php echo $TPL_K1?>"><?php }?>
                                </dt>
                                <dd>
                                    <select name="addGoodsInput<?php echo $TPL_K1?>" class="chosen-select" onchange="goodsViewController.add_goods_select(this)" data-key="<?php echo $TPL_K1?>"<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>>
                                    <option value=""><?php echo __('추가상품')?></option>
<?php if((is_array($TPL_R2=$TPL_V1["addGoodsList"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                    <option
<?php if($TPL_V1["addGoodsImageFl"]=='y'){?>
<?php if($TPL_V2["imageSrc"]){?>
                                    data-img-src="<?php echo $TPL_V2["imageSrc"]?>"
<?php }else{?>
                                    data-img-src="blank"
<?php }?>
<?php }?>value="<?php echo $TPL_V2["addGoodsNo"]?><?php echo INT_DIVISION?><?php echo $TPL_V2["goodsPrice"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["goodsNm"]?>

<?php if($TPL_V2["optionNm"]){?>(<?php echo $TPL_V2["optionNm"]?>)<?php }?>
                                    <?php echo STR_DIVISION?><?php echo rawurlencode(gd_html_add_goods_image($TPL_V2["addGoodsNo"],$TPL_V2["imageNm"],$TPL_V2["imagePath"],$TPL_V2["imageStorage"], 30,$TPL_V2["goodsNm"],'_blank'))?><?php echo STR_DIVISION?><?php echo $TPL_K1?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockUseFl"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockCnt"]?>"
<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?>
                                    disabled="disabled"
<?php }?>><?php echo $TPL_V2["goodsNm"]?>

<?php if($TPL_V2["optionNm"]||gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
                                    (
<?php if($TPL_V2["optionNm"]){?>
                                    <?php echo $TPL_V2["optionNm"]?>

<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
                                    /
<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'){?>
                                    <?php echo gd_global_currency_symbol()?>

<?php if(gd_isset($TPL_V2["goodsPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> / <?php echo __('품절')?><?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
<?php }?>
<?php }elseif(gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'){?>
                                    <?php echo gd_global_currency_symbol()?>

<?php if(gd_isset($TPL_V2["goodsPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> / <?php echo __('품절')?><?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
                                    )
<?php }?></option>

<?php }}?>
                                    </select>
                                </dd>
                            </dl>
<?php }}?>
<?php }?>
                        </div>
                        <!-- //item_add_option_box -->
<?php }?>
                    </div>
                    <!-- //item_detail_list -->
<?php if($TPL_VAR["goodsView"]['orderPossible']=='n'){?>
                    <div class="order_goods"></div>
                    <div class="btn_choice_box btn_restock_box">
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                        <button type="button" class="btn_restock_notice_v2 restockSelector"><?php echo __('재입고알림')?></button>
<?php }?>
                        <button type="button" class="btn_add_soldout" disabled="disabled"><?php echo __('구매 불가')?></button>
                    </div>
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
                    <div class="option_total_display_area item_choice_list">
                        <table class="option_display_area" border="0" cellpadding="0" cellspacing="0">
                            <colgroup>
                                <col width="330px">
                                <col>
                                <col width="80px">
                                <col width="40px">
                            </colgroup>
                        </table>
                        <div class="item_price_cont">
                            <div class="end_price item_tatal_box" style="display:none">
                                <dl class="total_goods">
                                    <dt><?php echo __('총 상품금액')?></dt>
                                    <dd><strong class="goods_total_price"></strong></dd>
                                </dl>
                                <dl class="total_discount">
                                    <dt><?php echo __('총 할인금액')?></dt>
                                    <dd><strong class="total_benefit_price"></strong></dd>
                                </dl>
                                <dl class="total_amount">
                                    <dt><?php echo __('총 합계금액')?></dt>
                                    <dd><strong class="total_price"></strong></dd>
                                </dl>
                            </div>
                            <!-- //item_tatal_box -->
                        </div>
                        <!-- //item_price_cont -->
                    </div>
<?php }else{?>
                    <div class="item_choice_list">
                        <table class="option_display_area" border="0" cellpadding="0" cellspacing="0">
                            <colgroup>
                                <col width="330px" />
                                <col />
                                <col width="80px" />
                                <col width="40px" />
                            </colgroup>
                            <tbody id="option_display_item_0">
                            <tr class="check optionKey_0">
                                <td class="cart_prdt_name">
                                    <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>" />
                                    <input type="hidden" name="optionSno[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['sno'])?>" />
                                    <input type="hidden" name="goodsPriceSum[]" value="0" />
                                    <input type="hidden" name="addGoodsPriceSum[]" value="0" />
<?php if($TPL_VAR["couponUse"]=='y'){?>
                                    <input type="hidden" name="couponApplyNo[]" value="" />
                                    <input type="hidden" name="couponSalePriceSum[]" value="" />
                                    <input type="hidden" name="couponAddPriceSum[]" value="" />
<?php }?>
                                    <div class="cart_tit_box">
                                        <strong class="cart_tit">
                                            <span><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></span>
<?php if($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                                            <span class="cart_btn_box">
<?php if(gd_is_login()===false){?>
                                                    <button type="button" class="btn_alert_login"><img src="/data/skin/front/moment/img/common/btn/btn_coupon_apply.png" alt="<?php echo __('쿠폰적용')?>"/></button>
<?php }else{?>
                                                    <a href="#lyCouponApply" id="coupon_apply_0" class="btn_open_layer" data-key="0">
                                                        <img src="/data/skin/front/moment/img/common/btn/btn_coupon_apply.png" alt="<?php echo __('쿠폰적용')?>"/>
                                                    </a>
<?php }?>
                                                </span>
<?php }?>
                                            <span id="option_text_display_0"></span></span>
                                        </strong>
                                    </div>
                                    <!-- //cart_tit_box -->
                                </td>
                                <!-- //cart_prdt_name -->
                                <td>
                                        <span class="count">
                                            <span class="goods_qty">
                                                <input type="text" name="goodsCnt[]" class="text goodsCnt_0" title="<?php echo __('수량')?>" value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-key="0" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-stock="<?php echo $TPL_VAR["goodsView"]['totalStock']?>" onchange="goodsViewController.input_count_change(this, '1');return false;" />
                                                <span>
                                                    <button type="button" class="up goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?>0"><?php echo __('증가')?></button>
                                                    <button type="button" class="down goods_cnt" title="<?php echo __('감소')?>" value="dn<?php echo STR_DIVISION?>0"><?php echo __('감소')?></button>
                                                </span>
                                            </span>
                                        </span>
                                    <!-- //count -->
                                </td>
                                <td class="item_choice_price">
                                    <input type="hidden" name="optionPriceSum[]"value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0)?>" />
                                    <input type="hidden" name="option_price_0" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0)?>" />
                                    <?php echo gd_global_currency_symbol()?><strong class="option_price_display_0"><?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0),false)?></strong><?php echo gd_global_currency_string()?>

                                </td>
                                <!-- //item_choice_price -->
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="item_price_cont">
                            <div class="end_price item_tatal_box">
                                <dl class="total_goods">
                                    <dt><?php echo __('총 상품금액')?></dt>
                                    <dd><strong class="goods_total_price"></strong></dd>
                                </dl>
                                <dl class="total_discount">
                                    <dt><?php echo __('총 할인금액')?></dt>
                                    <dd><strong class="total_benefit_price"></strong></dd>
                                </dl>
                                <dl class="total_amount">
                                    <dt><?php echo __('총 합계금액')?></dt>
                                    <dd><strong class="total_price"></strong></dd>
                                </dl>
                            </div>
                            <!-- //item_tatal_box -->
                        </div>
                        <!-- //item_price_cont -->
                    </div>
                    <!-- //item_choice_list -->
<?php }?>
                    <div class="btn_choice_box">
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                        <button type="button" class="btn_restock_notice_v1 restockSelector"><?php echo __('재입고알림')?></button>
<?php }?>
                        <div <?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>class="restock"<?php }?>><!-- N:재입고 알림이 있을 때는 restock 클래스를 div에 같이 넣어주세요 -->
                        <button id="cartBtn" type="button" class="btn_add_cart"><?php echo __('장바구니')?></button>
                        <button id="wishBtn" type="button" class="btn_add_wish"><?php echo __('찜하기')?></button>
                        <button type="button" class="btn_add_order"><?php echo __('바로 구매')?></button>
                    </div>
                </div>
                <!-- //btn_choice_box -->
                <div class="pay_box">
                    <div class="payco_pay">
                        <?php echo $TPL_VAR["payco"]?>

                    </div>
                    <div class="naver_pay">
                        <?php echo $TPL_VAR["naverPay"]?>

                    </div>
                </div>
                <!-- //pay_box -->
<?php }?>
            </div>
            <!-- //item_tit_detail_cont -->
    </div>
    <!-- //item_info_box -->
    </form>
</div>
<!-- //item_photo_info_sec -->
<!-- //상품 상단 끝 -->
<!-- 상품상세 -->
<div class="item_goods_sec">
    <div id="detail">
        <div class="item_goods_tab">
            <ul>
                <li class="on"><a href="#detail"><?php echo __('상품상세정보')?></a></li>
                <li><a href="#delivery"><?php echo __('배송안내')?></a></li>
                <li><a href="#exchange"><?php echo __('교환 및 반품안내')?></a></li>
                <li><a href="#reviews"><?php echo __('상품후기')?> <strong>(<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</strong></a></li>
                <li><a href="#qna"><?php echo __('상품문의')?> <strong>(<?php echo $TPL_VAR["goodsQaCount"]?>)</strong></a></li>
            </ul>
        </div>
        <!-- //item_goods_tab -->
        <div class="detail_cont">
            <h3><?php echo __('상품상세정보')?></h3>
            <div class="detail_explain_box">
                <div class="image-manual"><!-- 이미지 --></div>
                <div class="txt-manual">
                    <!-- 상품상세 공통정보 관리를 상세정보 상단에 노출-->
                    <?php echo gd_isset($TPL_VAR["goodsView"]['commonContent'])?>

                    <?php echo gd_isset($TPL_VAR["goodsView"]['goodsDescription'])?>

                </div>
<?php if($TPL_VAR["goodsView"]['externalVideoFl']=='y'&&$TPL_VAR["goodsView"]['externalVideoUrl']){?>
                <div style="padding:10px 0;text-align:center" id="external-video">
                    <?php echo gd_youtube_player($TPL_VAR["goodsView"]['externalVideoUrl'],$TPL_VAR["goodsView"]['externalVideoWidth'],$TPL_VAR["goodsView"]['externalVideoHeight'])?>

                </div>
<?php }?>
            </div>
            <!-- //detail_explain_box -->
<?php if($TPL_VAR["goodsView"]['goodsMustInfo']){?>
            <h3><?php echo __('상품필수 정보')?></h3>
            <div class="detail_info_box">
                <div class="datail_table">
                    <table class="left_table_type">
                        <colgroup>
                            <col />
                            <col />
                        </colgroup>
                        <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['goodsMustInfo'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                        <tr>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                            <th style="width:20%"><?php echo $TPL_V2['infoTitle']?></th>
                            <td <?php if((count($TPL_V1)== 1)){?>colspan="3" style="width:80%"<?php }else{?>style="width:30%"<?php }?>><?php echo $TPL_V2['infoValue']?></td>
<?php }}?>
                        </tr>
<?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- //detail_info_box -->
<?php }?>
<?php if($TPL_VAR["relation"]["relationFl"]!='n'){?>
            <div class="detail_explain_box">
                <h3><?php echo __('관련 상품')?></h3>
                <div class="goods_list">
                    <div class="goods_list_cont">
<?php if($TPL_VAR["widgetGoodsList"]){?>
                        <!-- 추천상품 -->
                        <?php echo includeWidget('goods/_goods_display.html')?>

                        <!-- 추천상품 -->
<?php }?>
                    </div>
                </div>
            </div>
<?php }?>
        </div>
        <!-- //detail_cont -->
    </div>
    <!-- //#detail -->
    <div id="delivery">
        <div class="item_goods_tab">
            <ul>
                <li><a href="#detail"><?php echo __('상품상세정보')?></a></li>
                <li class="on"><a href="#delivery"><?php echo __('배송안내')?></a></li>
                <li><a href="#exchange"><?php echo __('교환 및 반품안내')?></a></li>
                <li><a href="#reviews"><?php echo __('상품후기')?> <strong>(<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</strong></a></li>
                <li><a href="#qna"><?php echo __('상품문의')?> <strong>(<?php echo $TPL_VAR["goodsQaCount"]?>)</strong></a></li>
            </ul>
        </div>
        <!-- //item_goods_tab -->
<?php if($TPL_VAR["infoDelivery"]){?>
        <div class="delivery_cont">
            <h3><?php echo __('배송안내')?></h3>
            <div class="admin_msg"><?php echo $TPL_VAR["infoDelivery"]?></div>
        </div>
        <!-- //delivery_cont -->
<?php }?>
    </div>
    <!-- //#delivery -->
    <div id="exchange">
        <div class="item_goods_tab">
            <ul>
                <li><a href="#detail"><?php echo __('상품상세정보')?></a></li>
                <li><a href="#delivery"><?php echo __('배송안내')?></a></li>
                <li class="on"><a href="#exchange"><?php echo __('교환 및 반품안내')?></a></li>
                <li><a href="#reviews"><?php echo __('상품후기')?> <strong>(<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</strong></a></li>
                <li><a href="#qna"><?php echo __('상품문의')?> <strong>(<?php echo $TPL_VAR["goodsQaCount"]?>)</strong></a></li>
            </ul>
        </div>
        <!-- //item_goods_tab -->
        <div class="exchange_cont">
<?php if($TPL_VAR["infoExchange"]){?>
            <h3><?php echo __('교환 및 반품안내')?></h3>
            <div class="admin_msg">
                <?php echo $TPL_VAR["infoExchange"]?>

            </div>
<?php }?>
<?php if($TPL_VAR["infoRefund"]){?>
            <h3><?php echo __('환불안내')?></h3>
            <div class="admin_msg">
                <?php echo $TPL_VAR["infoRefund"]?>

            </div>
<?php }?>
<?php if($TPL_VAR["infoAS"]){?>
            <h3><?php echo __('AS안내')?></h3>
            <div class="admin_msg">
                <?php echo $TPL_VAR["infoAS"]?>

            </div>
<?php }?>
        </div>
        <!-- //exchange_cont -->
    </div>
    <!-- //#exchange -->
    <div id="reviews">
        <div class="item_goods_tab">
            <ul>
                <li><a href="#detail"><?php echo __('상품상세정보')?></a></li>
                <li><a href="#delivery"><?php echo __('배송안내')?></a></li>
                <li><a href="#exchange"><?php echo __('교환 및 반품안내')?></a></li>
                <li class="on"><a href="#reviews"><?php echo __('상품후기')?> <strong>(<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</strong></a></li>
                <li><a href="#qna"><?php echo __('상품문의')?> <strong>(<?php echo $TPL_VAR["goodsQaCount"]?>)</strong></a></li>
            </ul>
        </div>
        <!-- //item_goods_tab -->

<?php if($TPL_VAR["plusReviewConfig"]["isShowGoodsPage"]=='y'){?>
        <div class="plus_reivew">
            <?php echo includeFile($TPL_VAR["includePlusReviewFile"])?>

        </div>
<?php }?>

<?php if($TPL_VAR["goodsReviewAuthList"]=='y'){?>
        <div class="reviews_cont">
            <h3><?php echo __('상품후기')?></h3>
            <div id="ajax-goods-<?php echo $TPL_VAR["bdGoodsReviewId"]?>-list"></div>
            <div class="btn_reviews_box">
                <a href="../board/list.php?bdId=<?php echo $TPL_VAR["bdGoodsReviewId"]?>" class="btn_reviews_more"><?php echo __('상품후기 전체보기')?></a>
                <a href="javascript:gd_open_write_popup('<?php echo $TPL_VAR["bdGoodsReviewId"]?>', '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>')" class="btn_reviews_write"><?php echo __('상품후기 글쓰기')?></a>
            </div>
            <!-- //btn_reviews_box -->
        </div>
        <!-- //reviews_cont -->
<?php }?>
    </div>
    <!-- //#reviews -->
    <div id="qna">
        <div class="item_goods_tab">
            <ul>
                <li><a href="#detail"><?php echo __('상품상세정보')?></a></li>
                <li><a href="#delivery"><?php echo __('배송안내')?></a></li>
                <li><a href="#exchange"><?php echo __('교환 및 반품안내')?></a></li>
                <li><a href="#reviews"><?php echo __('상품후기')?> <strong>(<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</strong></a></li>
                <li class="on"><a href="#qna"><?php echo __('상품문의')?> <strong>(<?php echo $TPL_VAR["goodsQaCount"]?>)</strong></a></li>
            </ul>
        </div>
        <!-- //item_goods_tab -->
        <div class="qna_cont">
            <h3><?php echo __('상품Q%sA','&amp;')?></h3>
            <div id="ajax-goods-<?php echo $TPL_VAR["bdGoodsQaId"]?>-list"></div>
            <div class="btn_qna_box">
                <a href="../board/list.php?bdId=<?php echo $TPL_VAR["bdGoodsQaId"]?>" class="btn_qna_more"><?php echo __('상품문의 전체보기')?></a>
                <a href="javascript:gd_open_write_popup('<?php echo $TPL_VAR["bdGoodsQaId"]?>', '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>')" class="btn_qna_write"><?php echo __('상품문의 글쓰기')?></a>
            </div>
            <!-- //btn_qna_box -->
        </div>
        <!-- //qna_cont -->
    </div>
    <!-- //qna -->
</div>
<!-- //item_goods_sec -->
<!-- //상품상세 끝 -->
</div>

<div id="lyZoom" class="layer_wrap zoom_layer dn">
    <div class="layer_wrap_cont">
        <div class="ly_tit">
            <h4><?php echo __('이미지 확대보기')?><span><?php echo $TPL_VAR["goodsView"]['goodsNm']?></span></h4>
        </div>
        <div class="ly_cont">
            <div class="item_photo_view_box">
                <div class="item_photo_view">
                    <div class="item_photo_big" id="magnifyImage">
                        <span class="img_photo_big"><?php echo $TPL_VAR["goodsView"]['image']['magnify']['img'][ 0]?></span>
                    </div>
                    <!-- //item_photo_big -->
                    <div class="item_photo_slide">
                        <button type="button" class="slick_goods_prev"><img src="/data/skin/front/moment/img/common/btn/btn_vertical_prev.png" alt="<?php echo __('이전 상품 이미지')?>"/></button>
                        <ul class="slider_wrap ly_slider_goods_nav">
<?php if((is_array($TPL_R1=gd_isset($TPL_VAR["goodsView"]['image']['magnify']['thumb']))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                            <li><a href="javascript:gd_change_image('<?php echo $TPL_K1?>','magnify');"><?php echo $TPL_V1?></a></li>
<?php }}?>
                        </ul>
                        <button type="button" class="slick_goods_next"><img src="/data/skin/front/moment/img/common/btn/btn_vertical_next.png" alt="<?php echo __('다음 상품 이미지')?>"/></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- //ly_cont -->
        <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
    </div>
    <!-- //layer_wrap_cont -->
</div>
<!-- // zoom_layer -->

<div id="lyPassword" class="layer_wrap password_layer js_password_layer dn" style="height: 226px">
    <div class="layer_wrap_cont">
        <div class="ly_tit">
            <h4><?php echo __('비밀번호 인증')?></h4>
        </div>
        <div class="ly_cont">
            <div class="scroll_box">
                <p><?php echo __('글 작성시 설정한 비밀번호를 입력해 주세요.')?></p>
                <input type="password" name="writerPw" class="text" autocomplete="off">
            </div>
            <!-- // -->
            <div class="btn_center_box">
                <button type="button" class="btn_ly_password js_submit"><strong><?php echo __('확인')?></strong></button>
            </div>
        </div>
        <!-- //ly_cont -->
        <a href="#close" class="ly_close layer_close"><img src="/data/skin/front/moment/img/common/layer/btn_layer_close.png" alt="<?php echo __('닫기')?>"></a>
    </div>
    <!-- //layer_wrap_cont -->
</div>
<!-- //password_layer -->

<div id="addCartLayer" class="layer_wrap dn">
    <div class="box add_cart_layer" style="position: absolute; margin: 0px; top: 279.5px; left: 651px;">
        <div class="view">
            <h2><?php echo __('장바구니 담기')?></h2>
            <div class="scroll_box">
                <p class="success"><strong><?php echo __('상품이 장바구니에 담겼습니다.')?></strong><br><?php echo __('바로 확인하시겠습니까?')?></p>
            </div>
            <div class="btn_box">
                <button type="button" class="btn_cancel"><span><?php echo __('취소')?></span></button>
                <button type="button" class="btn_confirm btn_move_cart"><span><?php echo __('확인')?></span></button>
            </div>
            <!-- //btn_box -->
            <button title="<?php echo __('닫기')?>" class="close layer_close" type="button"><?php echo __('닫기')?></button>
        </div>
    </div>
</div>
<!--//addCartLayer -->
<div id="addWishLayer" class="layer_wrap dn">
    <div class="box add_wish_layer" style="position: absolute; margin: 0px; top: 279.5px; left: 651px;">
        <div class="view">
            <h2><?php echo __('찜 리스트 담기')?></h2>
            <div class="scroll_box">
                <p class="success"><strong><?php echo __('상품이 찜 리스트에 담겼습니다.')?></strong><br><?php echo __('바로 확인하시겠습니까?')?></p>
            </div>
            <div class="btn_box">
                <button type="button" class="btn_cancel"><span><?php echo __('취소')?></span></button>
                <button type="button" class="btn_confirm btn_move_wish"><span><?php echo __('확인')?></span></button>
            </div>
            <!-- //btn_box -->
            <button title="<?php echo __('닫기')?>" class="close layer_close" type="button"><?php echo __('닫기')?></button>
        </div>
    </div>
</div>
<!--//addWishLayer -->
<?php if($TPL_VAR["couponUse"]=='y'){?>
<!-- 쿠폰 다운받기 레이어 -->
<div id="lyCouponDown" class="layer_wrap coupon_down_layer dn" style="top:0px; left:0px;"></div>
<!--//쿠폰 다운받기 레이어 -->
<!-- 쿠폰 적용하기 레이어 -->
<div id="lyCouponApply" class="layer_wrap coupon_apply_layer dn" style="top:0px; left:0px;"></div>
<!--//쿠폰 적용하기 레이어 -->
<?php }?>
<!--//일반게시판 레이어 --><div id="lyWritePop" class="layer_wrap board_write_layer dn"></div>
<!--//플러스리뷰 레이어 --><div id="writePop" class="layer_wrap plus_review_edit_layer dn"></div>

<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_goods.js" charset="utf-8"></script>

<?php echo $TPL_VAR["fbGoodsViewScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>