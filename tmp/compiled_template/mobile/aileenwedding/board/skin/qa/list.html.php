<?php /* Template_ 2.2.7 2020/05/08 01:21:28 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/board/skin/qa/list.html 000009531 */ ?>
<div class="list_qa">
	<div id="boardlist" class="boardlist">
		<form name="frmList" id="frmList" action="../board/list.php" method="get">
			<input type="hidden" name="bdId" value="<?php echo $TPL_VAR["bdList"]["cfg"]["bdId"]?>">
			<input type="hidden" name="memNo" value="<?php echo $TPL_VAR["req"]["memNo"]?>"/>
			<input type="hidden" name="noheader" value="<?php echo $TPL_VAR["req"]["noheader"]?>"/>
			<input type="hidden" name="totalPage" value="<?php echo $TPL_VAR["bdList"]["cnt"]["totalPage"]?>"/>
			<div class="board_search">
				<input type="hidden" name="searchField" value="subject_contents"/>
				<div class="search_input"><input type="search" name="searchWord" value="<?php echo $TPL_VAR["req"]["searchWord"]?>" placeholder="<?php echo __('검색 단어를 입력해 주세요.')?>"/></div>
				<div class="search_btn"><button type="submit" class="board_search_btn"><?php echo __('검 색')?></button></div>
			</div>
		</form>
		<div class="goods_area">
			<div>
				<ul class="notice_list">
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["noticeList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
					<li>
						<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> ,'<?php echo $TPL_V1["auth"]["view"]?>')">
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'&&$TPL_VAR["bdList"]["cfg"]["bdListNoticeImageDisplayMobile"]=='y'){?>
							<div class="notice_img">
								<img src="<?php echo gd_isset($TPL_V1["viewListImage"],'/data/skin/mobile/aileenwedding/board/skin/qa/img/etc/noimg.png')?>" width="90" height="60" alt="<?php echo __('상품이미지')?>" title="<?php echo __('상품이미지')?>" class="goodsimg middle">
							</div>
<?php }?>
							<div class="board_qa_noimg <?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?> board_qa_img <?php }?>">
								<div class="notice_title">
									<span class="icon"><img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["notice"]["url"]?>" alt="<?php echo __('공지')?>"/></span>
									<span class="text">
										<?php echo $TPL_V1["gapReply"]?>

<?php if($TPL_V1["groupThread"]){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["re"]["url"]?>" alt="<?php echo __('답변')?>"/>
<?php }?>
<?php if($TPL_V1["isSecret"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["secret"]["url"]?>">
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'&&$TPL_V1["category"]){?>
										[<?php echo $TPL_V1["category"]?>]&nbsp;
<?php }?>
										<?php echo $TPL_V1["subject"]?>

<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
										<span class="c_red">(<?php echo $TPL_V1["memoCnt"]?>)</span>
<?php }?>
<?php if($TPL_V1["isFile"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
<?php if($TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["new"]["userModify"]!= 1){?>
										<span class="icon_new">N</span>
<?php }else{?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>"/>
<?php }?>
<?php }?>
<?php if($TPL_V1["isHot"]=='y'){?>
<?php if($TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["hot"]["userModify"]!= 1){?>
										<span class="icon_hot">HOT</span>
<?php }else{?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["hot"]["url"]?>" alt="<?php echo __('인기글')?>"/>
<?php }?>
<?php }?>
									</span>
								</div>
								<div class="notice_bottom">
									<span class="notice_data">
										<span><?php echo $TPL_V1["writer"]?></span><time><?php echo $TPL_V1["regDt"]?></time>
									</span>
								</div>
							</div>
						</a>
					</li>
<?php }}?>
<?php if($TPL_VAR["bdList"]["list"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
					<li>
						<a href="javascript:gd_btn_view('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_V1["sno"]?> ,'<?php echo $TPL_V1["auth"]["view"]?>')">
<?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?>
							<div class="notice_img">
								<img src="<?php echo gd_isset($TPL_V1["viewListImage"],'/data/skin/mobile/aileenwedding/board/skin/qa/img/etc/noimg.png')?>" width="90" height="60" alt="<?php echo __('상품이미지')?>" title="<?php echo __('상품이미지')?>" class="goodsimg middle">
							</div>
<?php }?>
							<div class="board_qa_noimg <?php if($TPL_VAR["bdList"]["cfg"]["bdListImageFl"]=='y'){?> board_qa_img <?php }?>">
								<div class="notice_title">
									<span class="text">
										<?php echo $TPL_V1["gapReply"]?>

<?php if($TPL_V1["groupThread"]){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["re"]["url"]?>" alt="<?php echo __('답변')?>"/>
<?php }?>
<?php if($TPL_V1["isSecret"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["secret"]["url"]?>">
<?php }?>
<?php if($TPL_VAR["bdList"]["cfg"]["bdCategoryFl"]=='y'&&$TPL_V1["category"]){?>
										[<?php echo $TPL_V1["category"]?>]&nbsp;
<?php }?>
										<?php echo $TPL_V1["subject"]?>

<?php if($TPL_VAR["bdList"]["cfg"]["bdMemoFl"]=='y'&&$TPL_V1["memoCnt"]> 0){?>
										<span class="c_red">(<?php echo $TPL_V1["memoCnt"]?>)</span>
<?php }?>
<?php if($TPL_V1["isFile"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>"/>
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
<?php if($TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["new"]["userModify"]!= 1){?>
										<span class="icon_new">N</span>
<?php }else{?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>"/>
<?php }?>
<?php }?>
<?php if($TPL_V1["isHot"]=='y'){?>
<?php if($TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["hot"]["userModify"]!= 1){?>
										<span class="icon_hot">HOT</span>
<?php }else{?>
										<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImageMobile"]["hot"]["url"]?>" alt="<?php echo __('인기글')?>"/>
<?php }?>
<?php }?>
									</span>
								</div>
								<div class="notice_bottom">
									<span class="notice_data">
										<span><?php echo $TPL_V1["writer"]?></span><time><?php echo $TPL_V1["regDt"]?></time>
									</span>
								</div>
								<div class="notice_info">
									<span <?php if($TPL_V1["replyComplete"]){?> class="reply_complete" <?php }?>><?php echo $TPL_V1["replyStatusText"]?></span>
								</div>
							</div>
						</a>
					</li>
<?php }}?>
				</ul>
				<div class="page_btn_box">
					<button type="button" class="prev_btn" data-page="<?php echo $TPL_VAR["req"]["page"]- 1?>"><?php echo __('이전')?></button>
					<span class="page_number"><strong><?php echo $TPL_VAR["req"]["page"]?></strong> / <?php echo $TPL_VAR["bdList"]["cnt"]["totalPage"]?></span>
					<button type="button" class="next_btn" data-page="<?php echo $TPL_VAR["req"]["page"]+ 1?>"><?php echo __('이후')?></button>
				</div>
<?php }else{?>
				<div class="no_list">
					<?php echo __('게시글이 존재하지 않습니다.')?>

				</div>
<?php }?>
			</div>
		</div>
	</div>
	<form id="frmWritePassword">
		<div class="cite_layer dn js_list_password_layer">
			<div class="wrap">
				<h4><?php echo __('비밀번호 인증')?></h4>
				<div>
					<p><?php echo __('비밀번호를 입력해 주세요.')?></p>
					<input type="password" name="writerPw" class="text"/>
					<div class="btn_box"><a href="javascript:void(0)" class="layer_close_btn js_submit ly_pwok_btn"><?php echo __('확인')?></a></div>
				</div>
				<button type="button" class="close" title="<?php echo __('닫기')?>"><?php echo __('닫기')?></button>
			</div>
		</div>
	</form>
	<div id="layerDim" class="dn">&nbsp;</div>
</div>
<script type="text/javascript" src="<?php echo PATH_MOBILE_SKIN?>js/gd_board_list.js" charset="utf-8"></script>
<script type="text/javascript">
    <!--
    $(document).ready(function(){
        $('.prev_btn').on('click', function() {
            if ($(this).data('page') > 0) {
                url = gd_replace_url_param(location.href, 'page', $('.prev_btn').data('page'));
                location.href = url;
            }
        });

        $('.next_btn').on('click', function() {
            if ($(this).data('page') <= $('input[name="totalPage"]').val()) {
                url = gd_replace_url_param(location.href, 'page', $('.next_btn').data('page'));
                location.href = url;
            }
        });
    });
    //-->
</script>