<?php /* Template_ 2.2.7 2020/05/08 01:21:19 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/goods/brand.html 000011002 */ 
if (is_array($TPL_VAR["korea_alphabet"])) $TPL_korea_alphabet_1=count($TPL_VAR["korea_alphabet"]); else if (is_object($TPL_VAR["korea_alphabet"]) && in_array("Countable", class_implements($TPL_VAR["korea_alphabet"]))) $TPL_korea_alphabet_1=$TPL_VAR["korea_alphabet"]->count();else $TPL_korea_alphabet_1=0;
if (is_array($TPL_VAR["english_alphabet"])) $TPL_english_alphabet_1=count($TPL_VAR["english_alphabet"]); else if (is_object($TPL_VAR["english_alphabet"]) && in_array("Countable", class_implements($TPL_VAR["english_alphabet"]))) $TPL_english_alphabet_1=$TPL_VAR["english_alphabet"]->count();else $TPL_english_alphabet_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="brand_wrap">
    <div class="brand_search_box total_search">
        <div class="search_sec">
            <input type="search" class="input_brand_search"  title="<?php echo __('브랜드명 검색')?>" placeholder="<?php echo __('브랜드명 검색')?>" id="brand_search" autocomplete="off">
            <button type="button" class="txt_cancel bn_wrg"><?php echo __('취소')?></button>
            <button type="button" class="btn_brand_search"><?php echo __('검색')?></button>
        </div>
        <button type="button" class="btn_brand_cancel"><?php echo __('취소')?></button>
    </div>

    <div class="brand_initial_box">
<?php if($TPL_VAR["alphabetFl"]){?>
        <div class="tab_initial_sec">
            <ul class="tab_initial">
                <li class="on">ㄱㄴㄷ</li>
                <li>ABC</li>
            </ul>
            <div class="initial_cate">
                <ul class="t_type0">
<?php if($TPL_korea_alphabet_1){foreach($TPL_VAR["korea_alphabet"] as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_K1== 0){?><li class="on"><?php }else{?><li><?php }?><a href="#<?php echo $TPL_K1?>"><span><?php echo $TPL_V1?></span></a></li>
<?php }}?>
                </ul>
                <ul class="t_type1" style="display:none;">
<?php if($TPL_english_alphabet_1){foreach($TPL_VAR["english_alphabet"] as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_K1== 0){?><li class="on"><?php }else{?><li><?php }?><a href="#<?php echo $TPL_V1?>"><span><?php echo $TPL_V1?></span></a></li>
<?php }}?>
                    <li><a href="#etc"><span>ETC</span></a></li>
                </ul>
            </div>
        </div>
<?php }?>

        <div class="initial_list_sec">
            <ul class="initial_list b_list0">
<?php if($TPL_korea_alphabet_1){foreach($TPL_VAR["korea_alphabet"] as $TPL_K1=>$TPL_V1){?>
                <li>
                    <strong id="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></strong>
                    <ul>
<?php if((is_array($TPL_R2=$TPL_VAR["list"]["korean"][$TPL_V1])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <li><a href="../goods/goods_list.php?brandCd=<?php echo $TPL_V2["cateCd"]?>"><?php echo $TPL_V2["cateNm"]?></a></li>
<?php }}?>
                    </ul>
                </li>
<?php }}?>
            </ul>
            <ul class="initial_list b_list1" style="display:none;">
<?php if($TPL_english_alphabet_1){foreach($TPL_VAR["english_alphabet"] as $TPL_V1){?>
                <li>
                    <strong id="<?php echo $TPL_V1?>"><?php echo $TPL_V1?></strong>
                    <ul>
<?php if((is_array($TPL_R2=$TPL_VAR["list"]["english"][$TPL_V1])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <li><a href="../goods/goods_list.php?brandCd=<?php echo $TPL_V2["cateCd"]?>"><?php echo $TPL_V2["cateNm"]?></a></li>
<?php }}?>
                    </ul>
                </li>
<?php }}?>
                <li>
                    <strong id="etc">ETC</strong>
                    <ul>
<?php if((is_array($TPL_R1=$TPL_VAR["list"]["etc"]["ETC"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                        <li><a href="../goods/goods_list.php?brandCd=<?php echo $TPL_V1["cateCd"]?>"><?php echo $TPL_V1["cateNm"]?></a></li>
<?php }}?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="brand_search_list" >
        <p class="total"><?php echo __('검색결과')?> <span>4</span><?php echo __('개')?></p>
        <ul>
        </ul>
    </div>

    <!-- N:검색결과 없음 안내 -->
    <div class="no_data">
        <p><?php echo __('검색결과가 없습니다.')?></p>
    </div>
    <!-- //N:검색결과 없음 안내 -->
</div>
<script type="text/javascript">

    $(document).ready(function () {

        //브랜드 페이지 검색창
        $(window).on({
            'scroll':function(){
                var sc = $(this).scrollTop();
                var h_offset = $('.top_area').innerHeight();
                var header_h =  $('#header_wrap header .header_box').outerHeight();
                if(sc > h_offset){
                    $('.brand_wrap .brand_search_box').addClass('h_on');
                    $('.brand_wrap .brand_search_box').css('margin-top', header_h);
                }else{
                    $('.brand_wrap .brand_search_box').removeClass('h_on');
                    $('.brand_wrap .brand_search_box').css('margin-top', 0);
                }
            }
        });

        //브랜드 페이지 초성 검색 탭
        var brand_tabmenu_idx = 0;
        $('.tab_initial_sec .tab_initial li').on({
            'click':function(){
                var idx = $(this).index();
                if(brand_tabmenu_idx == idx) return;
                brand_tabmenu_idx = idx;
                $('.tab_initial_sec .tab_initial li').removeClass('on');
                $(this).addClass('on');

                $('.initial_cate li').removeClass('on');
                $('.tab_initial_sec .initial_cate .t_type'+idx).find('li:first-child').addClass('on');

                $('.tab_initial_sec .initial_cate .t_type0, .tab_initial_sec .initial_cate .t_type1').hide();
                $('.tab_initial_sec .initial_cate .t_type'+idx).show();

                $('.initial_list_sec .b_list0, .initial_list_sec .b_list1').hide();
                $('.initial_list_sec .b_list'+idx).show();

            }
        });

<?php if(!$TPL_VAR["alphabetFl"]){?>
        $('.initial_list_sec .b_list1').show();
<?php }?>

        //tag_position
        $('.initial_cate li a[href*=#]').on({
            'click':function(e){
                e.preventDefault;
                _this = $(this);
                _href= _this.attr("href");
                var header_h =  $('#header_wrap header .header_box').outerHeight();
                var s_bar = $('.brand_search_box').outerHeight();
                var sum_h = header_h + s_bar;

                $('.initial_cate li').removeClass('on');
                _this.parents('li').addClass('on');
                e.preventDefault;
                $('html,body').animate({
                    scrollTop: $('' + _href + '').offset().top - sum_h
                }, 500);
            }
        });

        // 검색 버튼 클릭
        $('.btn_brand_search').on({
            'click':function(e){
                show_brand();
            }
        });
        // 취소 버튼 클릭
        $('.btn_brand_cancel').on({
            'click':function(e){
                toggle_brand('list');
            }
        });
        // x 버튼 클릭
        $('.txt_cancel').on({
            'click':function(e){
                $('#brand_search').val('');
            }
        });
        // 검색 인풋 엔터키 입력 시
        $("#brand_search").on({
            'keyup':function(e){
                if(e.keyCode == 13) {
                    show_brand();
                }
            }
        });
    });

    function toggle_brand(type){
        if(type == 'list') {
            $('.brand_initial_box').show();
            $('.brand_search_list').hide();
            $('.brand_search_box').addClass('total_search');
            $('#brand_search').val('');
            $(".no_data").hide();
        } else {
            $('.brand_initial_box').hide();
            $('.brand_search_box').removeClass('total_search');
            $('html,body').scrollTop(0);
            $(".no_data").hide();
            $('.brand_search_list').hide();
        }
    }

    function show_brand() {
        var brand = $('#brand_search').val();
        if(!$.trim(brand)) {
            alert(__('브랜드명을 입력해주세요.'));
            return false;
        }
        $("#brand_search").blur();
        $.ajax({
            method: "POST",
            cache: false,
            url: "./goods_ps.php",
            data: "mode=get_brand&brand="+brand,
            success: function(data) {
                toggle_brand('search');
                var getData = $.parseJSON(data);
                if(data =='false') {
                    $(".no_data").show();
                } else {
                    var addHtml = "";
                    var cnt = 0;
                    if(getData.korean) {
                        $.each(getData.korean, function (brandKey, brandVal) {
                            $.each(brandVal, function (key, val) {
                                addHtml += '<li><a href="../goods/goods_list.php?brandCd=' + val.cateCd + '">' + val.cateNm + '</a></li>';
                                cnt++;
                            });
                        });
                    }
                    if(getData.english) {
                        $.each(getData.english, function (brandKey, brandVal) {
                            $.each(brandVal, function (key, val) {
                                addHtml += '<li><a href="../goods/goods_list.php?brandCd=' + val.cateCd + '">' + val.cateNm + '</a></li>';
                                cnt++;
                            });
                        });
                    }
                    if(getData.etc) {
                        $.each(getData.etc, function (brandKey, brandVal) {
                            $.each(brandVal, function (key, val) {
                                addHtml += '<li><a href="../goods/goods_list.php?brandCd=' + val.cateCd + '">' + val.cateNm + '</a></li>';
                                cnt++;
                            });
                        });
                    }
                    $(".brand_search_list ul").html(addHtml);
                    $('.brand_search_list').show();
                    $('.brand_search_list .total span').text(cnt);
                }
            },
            error: function (data) {
                alert(data.message);
            }
        });
    }
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>