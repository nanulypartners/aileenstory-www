<?php /* Template_ 2.2.7 2020/05/08 01:21:19 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/goods/goods_board_qa_list.html 000004300 */ ?>
<div class="goods_board_qa_list">
    <ul>
<?php if($TPL_VAR["bdList"]["list"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdList"]["list"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
        <li class="<?php if($TPL_V1["replyComplete"]){?>done<?php }?>">
            <div class="item_list_summary">
				<div class="board_left">
					<div class="author_box">
						<span class="ico_ans<?php echo $TPL_V1["replyStatus"]?>"><?php echo $TPL_V1["replyStatusText"]?></span>
						<span class="name"><?php echo $TPL_V1["writer"]?></span> |
						<span class="date"> <?php echo $TPL_V1["regDate"]?></span>
					</div>
					<p class="title">
						<?php echo $TPL_V1["gapReply"]?>

						<span <?php if($TPL_V1["groupThread"]){?>class="reply"  <?php }?>>
<?php if($TPL_V1["isSecret"]=='y'){?>
						<!-- <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["secret"]["url"]?>" align=absmiddle> -->
<?php }?>
						<a class="js_board_qa_detail js_<?php echo $TPL_VAR["req"]["bdId"]?>_detail_<?php echo $TPL_V1["sno"]?>" data-bdid="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_V1["sno"]?>" data-auth="<?php echo $TPL_V1["auth"]["view"]?>" data-goodsno="<?php echo $TPL_V1["goodsNo"]?>"><?php echo $TPL_V1["subject"]?></a>
<?php if($TPL_V1["isFile"]=='y'){?>
						<!-- <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_file"]["url"]?>" alt="<?php echo __('파일첨부 있음')?>"/> -->
<?php }?>
<?php if($TPL_V1["isImage"]=='y'){?>
						<!-- <img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["attach_img"]["url"]?>" alt="<?php echo __('이미지첨부 있음')?>"/> -->
<?php }?>
<?php if($TPL_V1["isNew"]=='y'){?>
						<img src="<?php echo $TPL_VAR["bdList"]["cfg"]["iconImage"]["new"]["url"]?>" alt="<?php echo __('신규 등록글')?>"/>
<?php }?>
					</p>
				</div>
				<div class="board_right">
				  <button type="button"  class="detail_write_more_btn js_board_qa_detail" data-bdid="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_V1["sno"]?>" data-auth="<?php echo $TPL_V1["auth"]["view"]?>" data-goodsno="<?php echo $TPL_V1["goodsNo"]?>"><?php echo __('상품문의 더보기')?></button>
				</div>
			</div>
			<div class="js_goodsqa_<?php echo $TPL_V1["sno"]?>"></div>
		</li>
<?php }}?>
<?php }?>
	</ul>
	<div class="btn_box js_board_goodsqa_more">
		<button type="button" data-next-page="<?php echo $TPL_VAR["pageData"]["next"]?>" class="detail_more_btn"><?php echo __('더보기')?> <?php echo $TPL_VAR["pageData"]["listCount"]?><span>/</span><?php echo $TPL_VAR["pageData"]["total"]?></button>
	</div>
</div>
<?php if($TPL_VAR["pageData"]["now"]=='1'){?>
<script type="text/javascript" src="<?php echo PATH_MOBILE_SKIN?>js/gd_board_view.js" charset="utf-8"></script>
<script type="text/javascript">
	<!--
	$(document).ready(function () {
		$('.js_board_goodsqa_view').off().on('touchstart', '.js_board_qa_detail', function (e) {
			e.stopPropagation();
			e.preventDefault();
			if($(".js_goodsqa_"+$(this).data('sno')).html().trim() !='') {
				$(".js_goodsqa_"+$(this).data('sno')).html('');
				$(this).closest("li").removeClass("selected");
				$(this).closest("li").find('.detail_write_more_btn').removeClass("detail_write_more_btn_on");
			} else {
				gd_view_goods_board($(this).data('bdid'),$(this).data('sno'),$(this).data('auth'),$(this).data('goodsno'),'');
			}
		});
		$('.js_board_goodsqa_view').on('click', '.js_board_goodsqa_more button', function (e) {
			if($(this).data('next-page') > 0 ) {
				gd_load_goods_board_list('<?php echo $TPL_VAR["req"]["bdId"]?>','<?php echo $TPL_VAR["req"]["goodsNo"]?>',$(this).data('next-page'));
			}
		});
		$('.js_board_goodsqa_view').on("DOMSubtreeModified", '.goods_board_qa_list ul > li > div:last-child',function() {
			if ($(this).children('#boardContent').length > 0) {
				if ($(this).data('auth') != 'n'){
					$(this).closest("li").addClass("selected");
					$(this).closest("li").find('.detail_write_more_btn').addClass("detail_write_more_btn_on");
				}
			}
		});
	});
	//-->
</script>
<?php }?>