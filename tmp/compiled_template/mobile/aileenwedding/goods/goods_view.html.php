<?php /* Template_ 2.2.7 2020/05/08 01:21:19 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/goods/goods_view.html 000083562 */  $this->include_("includeFile","includeWidget");
if (is_array($TPL_VAR["snsShareButton"])) $TPL_snsShareButton_1=count($TPL_VAR["snsShareButton"]); else if (is_object($TPL_VAR["snsShareButton"]) && in_array("Countable", class_implements($TPL_VAR["snsShareButton"]))) $TPL_snsShareButton_1=$TPL_VAR["snsShareButton"]->count();else $TPL_snsShareButton_1=0;
if (is_array($TPL_VAR["displayField"])) $TPL_displayField_1=count($TPL_VAR["displayField"]); else if (is_object($TPL_VAR["displayField"]) && in_array("Countable", class_implements($TPL_VAR["displayField"]))) $TPL_displayField_1=$TPL_VAR["displayField"]->count();else $TPL_displayField_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<script type="text/javascript" src="/data/skin/mobile/aileenwedding/js/gd_goods_view.js"></script>
<script src="/data/skin/mobile/aileenwedding/js/slider/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    <!--
    var goodsNo = '<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>';
    var bdGoodsReviewId = '<?php echo $TPL_VAR["bdGoodsReviewId"]?>';
    var bdGoodsQaId = '<?php echo $TPL_VAR["bdGoodsQaId"]?>';
    var goodsViewController = new gd_goods_view();

    function gd_load_goods_board_list(bdId,goodsNo,page){
        $.ajax({
            method: "GET",
            url: "../goods/goods_board_list.php",
            data: {bdId: bdId, goodsNo: goodsNo, gboard : 'y',page : page},
            dataType: 'text',
            cache: false,
            async: false,
        }).success(function (data) {
            $.getScript('/data/skin/mobile/aileenwedding/js/plugins/modalEffects.js');
            if(page > 1) {
                $('.js_board_'+bdId+'_more').remove();
                $('.js_board_' + bdId+'_view').find(".item_list").append(data);
            } else {
                $('.js_board_' + bdId+'_view').find(".item_list").html(data);
            }
        }).error(function (e) {
            alert(e.responseText);
        });
    }

    /**
     * KC마크 인증정보창
     * @param string url KC인증번호검색 url
     * @return
     */
    function popupKcInfo(url) {
        var win = gd_popup({
            url: url
            , target: 'searchPop'
            , width: 750
            , height: 700
            , resizable: 'no'
            , scrollbars: 'yes'
        });
        win.focus();
        return win;
    }

    $(document).ready(function () {

        var parameters        = {
            'setControllerName' : goodsViewController,
            'setOptionFl' : '<?php echo $TPL_VAR["goodsView"]['optionFl']?>',
            'setOptionTextFl'    : '<?php echo $TPL_VAR["goodsView"]['optionTextFl']?>',
            'setOptionDisplayFl'    : '<?php echo $TPL_VAR["goodsView"]['optionDisplayFl']?>',
            'setAddGoodsFl'    : '<?php if(is_array($TPL_VAR["goodsView"]['addGoods'])){?>y<?php }else{?>n<?php }?>',
                'setIntDivision'    : '<?php echo INT_DIVISION?>',
            'setStrDivision'    : '<?php echo STR_DIVISION?>',
            'setMileageUseFl'    : '<?php echo $TPL_VAR["mileageData"]['useFl']?>',
            'setCouponUseFl'    : '<?php echo $TPL_VAR["couponUse"]?>',
            'setMinOrderCnt'    : '<?php echo $TPL_VAR["goodsView"]['minOrderCnt']?>',
            'setMaxOrderCnt'    : '<?php echo $TPL_VAR["goodsView"]['maxOrderCnt']?>',
            'setStockFl'    : '<?php echo gd_isset($TPL_VAR["goodsView"]['stockFl'])?>',
            'setSalesUnit' : '<?php echo gd_isset($TPL_VAR["goodsView"]['salesUnit'], 1)?>',
            'setDecimal' : '<?php echo $TPL_VAR["currency"]["decimal"]?>',
            'setGoodsPrice' : '<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>',
            'setGoodsNo' : '<?php echo $TPL_VAR["goodsView"]['goodsNo']?>',
            'setMileageFl' : ' <?php echo $TPL_VAR["goodsView"]['mileageFl']?>',
            'setFixedSales' : '<?php echo $TPL_VAR["goodsView"]['fixedSales']?>',
            'setFixedOrderCnt' : '<?php echo $TPL_VAR["goodsView"]['fixedOrderCnt']?>',
            'setOptionPriceFl' : '<?php echo $TPL_VAR["optionPriceFl"]?>'
    };

        if(window.location.hash== '#detail-review'){
            setTimeout(function(){
                $('a[data-target=js_board_goodsreview_view]').trigger('click');
            },1);
        }
        else if(window.location.hash== '#detail-qna') {
            setTimeout(function(){
                $('a[data-target=js_board_goodsqa_view]').trigger('click');
            },1);
        }
        $('.bn_srch').bind('click', function(e){
            var $viewport = $('head').children('meta[name="viewport"]');
            $viewport.attr('content', 'user-scalable=yes, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width');
        });


        $('#frmSearchTop .ly_btn_close').bind('click', function(e){
            var $viewport = $('head').children('meta[name="viewport"]');
            $viewport.attr('content', 'user-scalable=yes, width=device-width');
        });


        $(".js_password_layer .bn_cls").on('click',function (e){
            $(".js_password_layer").addClass("hidden");
            return false;
        });

        //$(".js_goods_description img").css("max-width",$(window).width());

        goodsViewController.init(parameters);


        $('.goods_view_image_slider').slick({
            dots: true,
            arrows: false,
            infinite: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true
        });

        $('.goods_view_image_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $(".js_option_add_image").remove();
            $(".slick-slide").show();
        });

        $(document).on('click','.btn_alert_login',function (e){
            var target = $(this).attr('id');
            alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
            target == undefined ? document.location.href = "../member/login.php" : document.location.href = "../member/login.php?id=" + target;
            return false;
        });

        //옵션레이어불러오기
        var params = {
            type : 'goods',
            goodsNo: '<?php echo $TPL_VAR["goodsView"]['goodsNo']?>'
        };

        $.ajax({
            method: "POST",
            cache: false,
            url: "../goods/layer_option.php",
            data: params,
            success: function (data) {
                $(".st_buy_content").empty().append(data);
            }
        });


        //하단 정보
        $('.detail_sub_lst > li').find('>a,>.js_accordian').on('click', function (e) {
            if ($(this).is('a')){
                e.preventDefault();
            }
            if ($(this).hasClass('disabled')) {
                return;
            }
            $('.detail_sub_lst > li').removeClass('selected');
            $(this).parent('li').toggleClass('selected');
            if ($(this).parent('li').hasClass('selected')) {
                $(".js_goods_info").children("div").hide();
                $("."+$(this).data('target')).fadeIn();
            } else {
                $("."+$(this).data('target')).hide();
            }

            if ($(this).data('bdid') == bdGoodsQaId || $(this).data('bdid') == bdGoodsReviewId) {
                $(".js_board_goodsreview_view .item_list").html('');
                $(".js_board_goodsqa_view .item_list").html('');
                gd_load_goods_board_list($(this).data('bdid'),goodsNo,1);
                if($(this).data('bdid') == bdGoodsReviewId){
                    $('#plusReviewList').show();
                }
            }
        });


        //필수정보
        gd_openblock();


        $('.btn_add_order').on('click', function(e){
            gd_goods_order('d');
            return false;
        });

        $('.btn_add_wish').on('click', function(e){
            gd_goods_order('w');
            return false;
        });

        $('.btn_add_cart').on('click', function(e){
            gd_goods_order();
            return false;
        });

        //상품 재입고 알림 팝업 오픈
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
        $('.btn_add_restock').on('click', function(e){
            window.open("./popup_goods_restock.php?goodsNo="+goodsNo, "popupRestock", "top=100, left=200, status=0, width=500px, height=600px");
            return false;
        });
<?php }?>

        $('.js_option_layer').on('click', function(e){
            var params = {
                type : $(this).data('type'),
                sno: $(this).data('sno'),
                goodsNo: $(this).data('goodsno')
            };

            $('#popupOption').modal({
                remote: '../goods/layer_option.php',
                cache: false,
                params: params,
                type : 'POST',
                show: true
            });
        });

        //$('.btn_tab_area').on('click', function(e){
        //	change_tab($(this));
        //	return false;
        //});


        //$('.btn_tab_area').on('click', function(e){
        //	change_tab($(this));
        //	return false;
        //});

<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
        $("#displayTimeSale").hide();
        gd_dailyMissionTimer("<?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['timeSaleDuration']?>");
<?php }?>

<?php if(!$TPL_VAR["canGoodsReview"]){?>
        //$('.tab-goodsreview').remove();
<?php }?>

<?php if(!$TPL_VAR["canGoodsQa"]){?>
        //$('.tab-goodsqa').remove();
<?php }?>

        $('input[name="writerPw"]').bind('keydown', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $('.js_submit').trigger('click');
            }
        });

        // 장바구니 이동 레이어
<?php if($TPL_VAR["cartInfo"]["moveCartPageFl"]=='n'&&$TPL_VAR["cartInfo"]["moveCartPageDeviceFl"]!='pc'){?>
        $('.layer_cartaddconfirm').click(function(){
            location.href = '../order/cart.php';
        });
<?php }?>

        // 찜리스트 이동 레이어
<?php if($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='n'&&$TPL_VAR["cartInfo"]["moveWishPageDeviceFl"]!='pc'){?>
        $('.layer_wishaddconfirm').click(function(){
            $('#addWishLayer').addClass('dn');
            $('#layerDim').addClass('dn');
            location.href = '../mypage/wish_list.php';
        });
<?php }?>
    });


<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
    /**
     * 시간간격 카운트
     * @returns <?php echo $TPL_VAR["String"]?>

     */
    function gd_dailyMissionTimer(duration) {

        var timer = duration;
        var days,hours, minutes, seconds;

        var interval = setInterval(function(){
            days    = parseInt(timer / 86400, 10);
            hours    = parseInt(((timer % 86400 ) / 3600), 10);
            minutes = parseInt(((timer % 3600 ) / 60), 10);
            seconds = parseInt(timer % 60, 10);
            if(days <= 0) {
                $('.time_day_view').hide();
            } else {
                $('#timeViewDay').html("");

                days     = days < 10 ? "0" + days : days;
                for(i = 0; i < days.toString().length; i++) {
                    $('#timeViewDay').append("<strong class='time_day_view'>"+days.toString().substr(i,1)+"</strong>");
                }
                $('.time_day_view_tail').text("<?php echo __('일')?>");
            }

            hours     = hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $('#timeViewTime strong').eq(0).text(hours.toString().substr(0,1));
            $('#timeViewTime strong').eq(1).text(hours.toString().substr(1,1));

            $('#timeViewTime strong').eq(2).text(minutes.toString().substr(0,1));
            $('#timeViewTime strong').eq(3).text(minutes.toString().substr(1,1));

            $('#timeViewTime strong').eq(4).text(seconds.toString().substr(0,1));
            $('#timeViewTime strong').eq(5).text(seconds.toString().substr(1,1));

            $("#displayTimeSale").show();

            if (--timer < 0) {
                timer = 0;
                clearInterval(interval);
            }
        }, 1000);
    }
<?php }?>


    function gd_openblock(){
        $($('.js_openblock')[0]).find('.openblock_content').show(); // 첫번째 페이지 열어놓기
        $($('.js_openblock')[0]).find('.toggle_btn').addClass('open'); // 첫번째 페이지 버튼 open 상태로.
        $('.js_openblock .openblock_header').bind('click',function(){
            var content = $(this).next();
            var btn = $(this).find('.toggle_btn');
            if(content.css('display')=='block'){
                btn.removeClass('open');
                content.slideUp('fast');
            }else{
                btn.addClass('open');
                content.slideDown('fast');
            }
        });
    }

    $(function(){
        /* 탭 메뉴 */
        $('.detail_tab_info').find('button').on({
            'click':function(){
                if($(this).parent().find('.ly_info').css('display') =='none'){
                    $(this).parent().find('.ly_info').slideDown();
                    $(this).parent().find('button').removeClass().addClass('detail_tab_info_btn_on');
                }else{
                    $(this).parent().find('.ly_info').slideUp();
                    $(this).parent().find('button').removeClass().addClass('detail_tab_info_btn');
                }
            }
        });

        $('.detail_tab_info .ly_info li a').on({
            'click':function() {
                $($(this).attr("href")+" a:first").click();
            }
        });

        /* SNS 공유 */
        $('.share_btn, .js_lys_close, .ly_share_bg').on({
            'click':function(e){
                e.preventDefault();
                if($('.ly_share').css('display') == 'none'){
                    // 단축주소 가져오기
                    $.ajax({
                        type: 'post',
                        url: './goods_ps.php',
                        async: true,
                        cache: true,
                        data: {
                            mode: 'get_short_url',
                            url: '<?php echo $TPL_VAR["snsShareUrl"]?>'
                        },
                        success: function (data) {
                            var json = $.parseJSON(data);
                            $('.copy input').val(json.url);
                            $('.copy button').attr('data-clipboard-text', json.url);
                        }
                    });
                    $('.ly_share').fadeIn('fast');
                    gd_lypop_share_postion();
                }else{
                    $('.ly_share').fadeOut('fast');
                }
            }
        });
        /* 레이어 팝업 위치 */
        var gd_lypop_share_postion = function(){
            var ly_h = $('.ly_share .ly_share_content').innerHeight()/2;
            $('.ly_share .ly_share_content').css('margin-top','-'+ly_h+'px');
        }


    });
    //-->
</script>

<!-- 상품 상세 목록 -->
<div class="goods_view">
<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
    <div class="time_sale_box">
        <div class="event">
            <div class="time_count" id="displayTimeSale">
                <div class="sale_percent"><?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['benefit']?>% <?php echo __('할인')?> </div>
                <div class="time_sale_date_box">
                    <span class="time_sale_icon"><img src="/data/skin/mobile/aileenwedding/img/icon/icon_timesale.png" alt="<?php echo __('타임세일아이콘')?>"></span>
                    <div id="timSaleDate" class="timesaledate">
                        <span id="timeViewDay" class="time_day"></span>
                        <span class="time_day_view_tail"></span>
                        <span id="timeViewTime" class="time_box">
							<strong class="time"></strong>
							<strong class="time"></strong>
							<span>:</span>
							<strong class="time"></strong>
							<strong class="time"></strong>
							<span>:</span>
							<strong class="time"></strong>
							<strong class="time"></strong>
						</span>
                    </div>
                </div>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['orderCntDisplayFl']=='y'){?>
                <p class="sale_order_count"><?php echo __('현재 ')?><strong><?php echo number_format($TPL_VAR["goodsView"]['timeSaleInfo']['orderCnt'])?></strong><?php echo __('개 구매')?></p>
<?php }?>
            </div>
        </div>
    </div>
<?php }?>
    <div class="detail_tab_info">
        <button type="button" class="detail_tab_info_btn">INFO</button>
        <ul class="ly_info">
            <li><a href="#goodsDescription" id="btnGodosDescriptiton"><?php echo __('상세정보')?></a></li>
            <li><a href="#goodsInfo" id="btnGodosInfo"><?php echo __('기본정보')?></a></li>
            <li><a href="#detailReview" id="btnDetailReview"><?php echo __('상품후기')?><span> (<?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?>)</span></a></li>
            <li><a href="#detailQna" id="btnDetailQna"><?php echo __('상품문의')?><span> (<?php echo $TPL_VAR["goodsQaCount"]?>)</span></a></li>
        </ul>
    </div>
<?php if($TPL_VAR["snsShareUseFl"]){?>
    <div class="ly_share" style="display:none;">
        <div class="ly_share_bg"></div>
        <div class="ly_share_content">
            <div class="ly_share_box">
                <h3><?php echo __('공유하기')?></h3>
                <ul>
<?php if($TPL_snsShareButton_1){foreach($TPL_VAR["snsShareButton"] as $TPL_V1){?><?php echo $TPL_V1?><?php }}?>
                </ul>
<?php if($TPL_VAR["snsShareUrl"]){?>
                <div class="copy">
                    <div class="input"><input type="text" value="<?php echo $TPL_VAR["snsShareUrl"]?>"></div>
                    <div class="btn"><button type="button" class="sns_clip_board_btn gd_clipboard" data-clipboard-text="<?php echo $TPL_VAR["snsShareUrl"]?>" title="<?php echo __('상품주소')?>"><?php echo __('URL복사')?></button></div>
                </div>
<?php }?>
                <div class="btn_box"><button class="js_lys_close lys_close1_btn"><?php echo __('닫기')?></button></div>
            </div>
        </div>
    </div>
<?php }?>
    <div class="cont_detail">
        <ul class="slider-wrap goods_view_image_slider" >
<?php if((is_array($TPL_R1=gd_isset($TPL_VAR["goodsView"]['image']['detail']['img']))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
            <li><?php echo $TPL_V1?></li>
<?php }}?>
        </ul>
    </div>
<?php if(in_array('goodsColor',$TPL_VAR["displayAddField"])){?>
    <?php echo $TPL_VAR["goodsView"]['goodsColor']?>

<?php }?>
    <div class="detail_info">
        <form  method="post">
            <input type="hidden" name="mode" value="cartIn" />
            <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
            <input type="hidden" name="cartMode" value="" />
            <input type="hidden" name="set_goods_price"   value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />
            <input type="hidden" name="set_goods_fixedPrice"  id="set_goods_fixedPrice" value="<?php echo gd_isset($TPL_VAR["goodsView"]['fixedPrice'], 0)?>" />
            <input type="hidden" name="set_goods_mileage" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsMileageBasic'], 0)?>" />
            <input type="hidden" name="set_goods_stock" value="<?php echo gd_isset($TPL_VAR["goodsView"]['stockCnt'], 0)?>" />
            <input type="hidden" name="set_coupon_dc_price" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>" />

            <input type="hidden" name="set_goods_total_price" id="set_goods_total_price" value="0" />
            <input type="hidden" name="set_option_price"  id="set_option_price" value="0" />
            <input type="hidden" name="set_option_text_price" id="set_option_text_price" value="0" />
            <input type="hidden" name="set_add_goods_price"  id="set_add_goods_price" value="0" />
            <input type="hidden" name="set_total_price" value="0" />


            <input type="hidden" name="mileageFl" value="<?php echo $TPL_VAR["goodsView"]['mileageFl']?>" />
            <input type="hidden" name="mileageGoods" value="<?php echo $TPL_VAR["goodsView"]['mileageGoods']?>" />
            <input type="hidden" name="mileageGoodsUnit" value="<?php echo $TPL_VAR["goodsView"]['mileageGoodsUnit']?>" />
            <input type="hidden" name="goodsDiscountFl" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountFl']?>" />
            <input type="hidden" name="goodsDiscount" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscount']?>" />
            <input type="hidden" name="goodsDiscountUnit" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountUnit']?>" />

            <input type="hidden" name="taxFreeFl" value="<?php echo $TPL_VAR["goodsView"]['taxFreeFl']?>" />
            <input type="hidden" name="taxPercent" value="<?php echo $TPL_VAR["goodsView"]['taxPercent']?>" />

            <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
            <input type="hidden" name="brandCd" value="<?php echo $TPL_VAR["goodsView"]['brandCd']?>" />
            <input type="hidden" name="cateCd" value="<?php echo $TPL_VAR["goodsView"]['cateCd']?>" />
            <input type="hidden" id="set_dc_price" value="0" />

<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['mileageFl']=='n'){?>
            <input type="hidden" name="goodsMileageExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['memberDcFl']=='n'){?>
            <input type="hidden" name="memberBenefitExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['couponFl']=='n'){?>
            <input type="hidden" name="couponBenefitExcept" value="y" />
<?php }?>
<?php }?>
            <input type="hidden" id="goodsOptionCnt" value="1" />
            <input type="hidden" name="useBundleGoods" value="1" />
            <input type="hidden" name="goodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['goodsDeliveryFl']?>" />
            <input type="hidden" name="sameGoodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['sameGoodsDeliveryFl']?>" />
            <div class="detail_info_top">
<?php if(in_array('goodsIcon',$TPL_VAR["displayAddField"])&&$TPL_VAR["goodsView"]['goodsIcon']){?> <div class="prd_icon"><?php echo $TPL_VAR["goodsView"]['goodsIcon']?></div> <?php }?>
<?php if($TPL_VAR["goodsView"]['brandNm']){?> <p class="brand"><?php echo $TPL_VAR["goodsView"]['brandNm']?></p> <?php }?>
                <h3>
<?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsNmDescription']){?>
                    [<?php echo $TPL_VAR["goodsView"]['timeSaleInfo']['goodsNmDescription']?>]
<?php }?>
                    <?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?>

                </h3>
                <div class="price_box">
<?php if($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='text'){?>
                    <strong class="price"><?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_text"]?></strong>
<?php }elseif($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='custom'){?>
                    <img src="<?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_img"]?>">
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['goodsPriceString']!=''){?>
                    <strong class="price"><?php echo $TPL_VAR["goodsView"]['goodsPriceString']?></strong>
<?php }else{?>

                    <!-- //myCouponSalePrice.s -->
<?php if(gd_isset($TPL_VAR["goodsView"]['myCouponSalePrice'])> 0&&((!$TPL_VAR["goodsView"]['timeSaleFl'])||($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["couponUse"]=='y'))){?>
                    <span class="coupon_price"><?php echo __('내 쿠폰적용가')?> </span>
                    <strong class="price"><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['myCouponSalePrice'])?></strong><span class="won2"><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsPriceViewFl']=='y'&&$TPL_VAR["goodsView"]['oriGoodsPrice']> 0){?>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['oriGoodsPrice'])?><?php echo gd_global_currency_string()?></del>
<?php }else{?>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?></del><span class="won1"><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php }?>
                    <!-- //myCouponSalePrice.e -->

<?php if($TPL_VAR["goodsView"]['couponPrice']> 0&&((!$TPL_VAR["goodsView"]['timeSaleFl'])||($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["couponUse"]=='y'))){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['myCouponSalePrice'])> 0){?><br /><?php }?>
                    <span class="coupon_price"><?php echo __('쿠폰적용가')?> </span><strong class="price"><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['couponPrice'])?></strong><span class="won2"><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsPriceViewFl']=='y'&&$TPL_VAR["goodsView"]['oriGoodsPrice']> 0){?>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['oriGoodsPrice'])?><?php echo gd_global_currency_string()?></del><br />
                    <span class="time_sale_price"><?php echo __('타임세일가')?> </span><strong class="price"><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?></strong><span class="won"><?php echo gd_global_currency_string()?></span>
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['displayCouponPrice']!=='y'){?>
                    <strong class="price"><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?></strong><span class="won"><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["goodsView"]['fixedPrice']&&$TPL_VAR["goodsView"]['fixedPrice']> 0){?>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['fixedPrice'])?></del><span class="won1"><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php }elseif($TPL_VAR["goodsView"]['couponPrice']> 0){?>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?></del><span class="won1"><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php }?>

                    <em class="add_currency"><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['goodsPrice'])?></em>

<?php if($TPL_VAR["goodsView"]['dcPrice']> 0){?>
                    <br /><span class="coupon_price"><?php echo __('할인적용가')?></span><strong class="price"><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice']-$TPL_VAR["goodsView"]['dcPrice'])?></strong><span class="won"><?php echo gd_global_currency_string()?></span>
                    <del><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?></del><span class="won1"><?php echo gd_global_currency_string()?></span>
<?php }?>

<?php }?>
<?php }?>
                    <?php echo $TPL_VAR["myappGoodsBenefitMessage"]?>

<?php if(empty($TPL_VAR["couponArrData"])===false){?>
                    <div class="coupon_box">
<?php if(gd_is_login()===false){?>
                        <button id="lnCouponDown" class="detail_coupon_btn btn_alert_login"><?php echo __('쿠폰받기')?></button>
<?php }else{?>
                        <a href="../goods/layer_coupon_down.php?goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>" class="detail_coupon_btn" data-toggle="modal"  data-cache="false" data-target="#popupCoupon" data-type="post" ><?php echo __('쿠폰받기')?></a>
<?php }?>
                    </div>
<?php }?>
                </div>
            </div>
            <div class="detail_sub_info">
<?php if($TPL_VAR["goodsView"]['makerNm']){?>
                <dl>
                    <dt><?php echo __('제조사')?></dt>
                    <dd><?php echo $TPL_VAR["goodsView"]['makerNm']?></dd>
                </dl>
<?php }?>
<?php if($TPL_VAR["goodsView"]['originNm']){?>
                <dl>
                    <dt><?php echo __('원산지')?></dt>
                    <dd><?php echo $TPL_VAR["goodsView"]['originNm']?></dd>
                </dl>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                <dl>
                    <dt><?php echo __('배송정보')?></dt>
                    <dd>
<?php if($TPL_VAR["goodsView"]['multipleDeliveryFl']===true){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
                        <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['firstCharge']['0']['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                        <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['firstCharge']['0']['price'])?></span>
<?php }?>
<?php }else{?>
                        <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['firstCharge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                        <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['firstCharge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?></span>
<?php }?>
<?php }?>
                        <a href="../goods/goods_info.php?target=detailConditionalShipping" data-toggle="modal" data-cache="false" data-target="#popupDetailInfo" class="condition_del">(<?php echo __('조건부배송')?>)</a>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='free'){?>
                        <span><?php echo __('무료')?></span>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
                        <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                        <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?></span>
<?php }?>
<?php }else{?>
                        <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                        <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?></span>
<?php }?>
                        <a href="../goods/goods_info.php?target=detailConditionalShipping" data-toggle="modal" data-cache="false" data-target="#popupDetailInfo" class="condition_del">(<?php echo __('조건부배송')?>)</a>
<?php }?>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['areaFl'])=='y'){?>
                        <a href="../goods/goods_info.php?target=detailRegionalShipping" data-toggle="modal" data-cache="false" data-target="#popupDetailInfo" class="area_del"><?php echo __('지역별 추가배송')?></a>
<?php }?>                        <?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='free'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='pre'){?>
                        <?php echo __('주문시결제')?>(<?php echo __('선결제')?>)
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='later'){?>
                        <?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)
<?php }?>
<?php }?>

<?php if(gd_isset($TPL_VAR["deliveryMethodFlText"])){?>
                        <div style="margin-top: 5px;"><?php echo $TPL_VAR["deliveryMethodFlText"]?></div>
<?php }?>
                    </dd>
                </dl>
<?php }?>
            </div>
            <div class="share_btn_box">
                <ul>
<?php if($TPL_VAR["snsShareUseFl"]){?><li class="d_share"><button class="share_btn"><?php echo __('공유하기')?></button></li><?php }?>
                    <li class="d_wish"><button id="wishBtn" class="btn_add_wish"><?php echo __('찜하기')?></button></li>
                </ul>
            </div>
            <div class="price-result">
                <!--
                <span class="price-result-value">123,456,789</span>
                <span class="unit">
                    원
                </span>-->
            </div>
        </form>
    </div>
    <!-- 구매하기 하단 영역 -->
    <div class="detail_buy">
        <div class="detail_buy_top_btn">
            <div class="detail_open_close"><a class="st_buy_close"><?php echo __('열기/닫기')?></a></div>
            <div class="ly_buy_dn js_goods_view_buy_btn">
                <div class="detail_prd_btn">
                    <ul>
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                        <li class="restock"><a class="detail_restock_btn btn_add_restock"><img src="/data/skin/mobile/aileenwedding/img/icon/icon_restock_red.png" alt="<?php echo __('재입고알림 아이콘')?>" /><?php echo __('재입고알림')?></a></li>
<?php }?>
<?php if($TPL_VAR["goodsView"]['orderPossible']=='n'){?>
                        <li><a class="detail_prd_no_btn"><?php echo __('구매불가')?></a></li>
<?php }else{?>
                        <li><a id="cartBtn" class="detail_cart_btn st_btn_buy basket" data-effect="st-effect-15" data-goods-no="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>"  data-pay="cart" ><?php echo __('장바구니')?></a></li>
                        <li><a class="detail_order_btn st_btn_buy" data-effect="st-effect-15" data-goods-no="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>"  data-pay="cart" ><?php echo __('바로구매')?></a></li>
<?php }?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="st_buy_content" style="display:none" ></div>
        <div id="divAddPay" class="js_goods_add_pay" style="display:none">
<?php if($TPL_VAR["payco"]){?>
            <div class="snsbtn">
                <div class="js_payco_btn"><?php echo $TPL_VAR["payco"]?></div>
            </div>
<?php }?>
<?php if($TPL_VAR["naverPay"]){?>
            <div class="snsbtn">
                <div class="js_naver_btn"><?php echo $TPL_VAR["naverPay"]?></div>
            </div>
<?php }?>
        </div>
    </div>
    <!-- // 구매하기 영역 -->
    <div id="detailGoodsMustInfo" style="display:none">
        <div class="ly_head">
            <h1 class="h_tit"><?php echo __('필수정보')?></h1>
        </div>
        <div class="ly_ct">
            <div class="lst_info">
<?php if($TPL_VAR["goodsView"]['goodsMustInfo']){?>
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table_style2">
                    <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['goodsMustInfo'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                    <tr>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <th>
                            <?php echo $TPL_V2['infoTitle']?>

                        </th>
                        <td <?php if((count($TPL_V1)== 1)){?>colspan="3"<?php }?>><?php echo $TPL_V2['infoValue']?></td>
<?php }}?>
                    </tr>
<?php }}?>
                    </tbody>
                </table>
<?php }?>
            </div>
        </div>
    </div>
<?php if((gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='free'&&gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='fixed')||$TPL_VAR["goodsView"]['multipleDeliveryFl']===true){?>
    <!-- 조건부 배송 레이어 -->
    <div id="detailConditionalShipping" style="display:none">
        <div class="detail_shipping">
            <div class="ly_head">
                <h2 class="h_tit"><?php echo __('조건부 배송')?></h2>
            </div>
            <div class="ly_content">
                <div class="ly_dev_wrap lst_info">
                    <h2 class="tit"><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['fixFlText']?></h2>
<?php if($TPL_VAR["goodsView"]['multipleDeliveryFl']===true){?>
                    <div class="ly_btn_wrap">
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='fixed'){?>
                        <div class="ly_btn <?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2){?>two<?php }else{?>three<?php }?>">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2&&$TPL_I1% 2== 0){?><div class="row"><?php }else{?><?php if($TPL_I1% 3== 0){?><div class="row"><?php }?><?php }?>
                            <a class="delivery-method" onclick="selectDeliveryMethod(this, '<?php echo $TPL_K1?>')"><?php echo $TPL_V1?></a>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 2&&$TPL_I1% 2== 1){?></div><?php }else{?><?php if($TPL_I1% 3== 2){?></div><?php }else{?><?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])-$TPL_I1< 3&&((count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])% 3== 1&&$TPL_I1% 3== 0)||(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])% 3== 2&&$TPL_I1% 3== 1))){?></div><?php }?><?php }?><?php }?>
<?php }}?>
                    </div>
                    <script type="text/javascript">
                        function selectDeliveryMethod(e, method) {
                            $(e).parents(".ly_btn").find("a").removeClass("on");
                            $(e).addClass("on");
                            $('.delivery-method-tr').hide();
                            $('.delivery-method-tr').filter('[data-method="' + method + '"]').show();
                        }
                        $(function(){
                            $('.delivery-method').eq(0).trigger('click');
                        })
                    </script>
<?php }?>
                </div>
<?php }?>
                    <div class="type_cont">
<?php if($TPL_VAR["goodsView"]['multipleDeliveryFl']===true){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['multiCharge'][ 0])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if(gd_isset($TPL_V1["sno"])){?>
                        <dl>
                            <dt><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'][$TPL_K1]?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V1["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }}?>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['rangeRepeat'])!='y'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if(gd_isset($TPL_V2["sno"])){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo gd_global_money_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo gd_global_money_format($TPL_V2["unitEnd"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }else{?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo gd_global_money_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if(gd_isset($TPL_V2["sno"])){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo number_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V2["unitEnd"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }else{?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo number_format($TPL_V2["unitStart"])?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }else{?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if(gd_isset($TPL_V2["sno"])){?>
<?php if($TPL_V2["unitEnd"]> 0){?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo number_format($TPL_V2["unitStart"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V2["unitEnd"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }else{?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt><?php echo number_format($TPL_V2["unitStart"], 2)?><?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?></dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }?>
<?php }else{?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['multiCharge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if(gd_isset($TPL_V2["sno"])){?>
<?php if($TPL_I1== 0){?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V2["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V2["unitStart"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V2["unitStart"], 2)?>

<?php }?>
                                <?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?> ~
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V2["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V2["unitEnd"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V2["unitEnd"], 2)?>

<?php }?>
                                <?php echo $TPL_V2["unitText"]?> <?php echo __('미만')?>

                            </dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }else{?>
                        <dl class="delivery-method-tr" data-method="<?php echo $TPL_K2?>">
                            <dt>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V2["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V2["unitStart"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V2["unitStart"], 2)?>

<?php }?>
                                <?php echo $TPL_V2["unitText"]?> <?php echo __('이상')?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V2["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V2["unitEnd"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V2["unitEnd"], 2)?>

<?php }?>
                                <?php echo $TPL_V2["unitText"]?> <?php echo __('마다 추가')?>

                            </dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V2["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }?>
<?php }}?>
<?php }}?>
<?php }?>
<?php }?>
<?php }else{?>



<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['rangeRepeat'])!='y'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='weight'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["unitEnd"]> 0){?>
                        <dl>
                            <dt><?php echo number_format($TPL_V1["unitStart"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V1["unitEnd"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('미만')?></dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }else{?>
                        <dl>
                            <dt><?php echo number_format($TPL_V1["unitStart"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ </dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }?>
<?php }}?>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["unitEnd"]> 0){?>
                        <dl>
                            <dt><?php echo gd_global_money_format($TPL_V1["unitStart"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ <?php echo gd_global_money_format($TPL_V1["unitEnd"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('미만')?></dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }else{?>
                        <dl>
                            <dt><?php echo gd_global_money_format($TPL_V1["unitStart"], 2)?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ </dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }?>
<?php }}?>
<?php }else{?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["unitEnd"]> 0){?>
                        <dl>
                            <dt><?php echo number_format($TPL_V1["unitStart"])?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ <?php echo number_format($TPL_V1["unitEnd"])?><?php echo $TPL_V1["unitText"]?> <?php echo __('미만')?></dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }else{?>
                        <dl>
                            <dt><?php echo number_format($TPL_V1["unitStart"])?><?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~ </dt>
                            <dd><?php echo gd_global_currency_display($TPL_V1["price"])?></dd>
                        </dl>
<?php }?>
<?php }}?>
<?php }?>
<?php }else{?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['charge'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?>
                        <dl>
                            <dt>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V1["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V1["unitStart"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V1["unitStart"], 2)?>

<?php }?>
                                <?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?> ~
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V1["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V1["unitEnd"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V1["unitEnd"], 2)?>

<?php }?>
                                <?php echo $TPL_V1["unitText"]?> <?php echo __('미만')?>

                            </dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V1["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }else{?>
                        <dl>
                            <dt>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V1["unitStart"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V1["unitStart"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V1["unitStart"], 2)?>

<?php }?>
                                <?php echo $TPL_V1["unitText"]?> <?php echo __('이상')?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='price'){?>
                                <?php echo gd_global_money_format($TPL_V1["unitEnd"])?>

<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='count'){?>
                                <?php echo number_format($TPL_V1["unitEnd"])?>

<?php }else{?>
                                <?php echo number_format($TPL_V1["unitEnd"], 2)?>

<?php }?>
                                <?php echo $TPL_V1["unitText"]?> <?php echo __('마다 추가')?>

                            </dt>
                            <dd>
                                <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["price"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                <span><?php echo gd_global_add_currency_display($TPL_V1["price"])?></span>
<?php }?>
                            </dd>
                        </dl>
<?php }?>
<?php }}?>
<?php }?>
<?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //조건부 배송 레이어 -->
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['areaFl'])=='y'){?>
    <!-- 지역별 배송비 레이어 -->
    <div id="detailRegionalShipping" style="display:none">
        <div class="detail_shipping">
            <div class="ly_head">
                <h2 class="h_tit"><?php echo __('지역별 추가 배송비')?></h2>
            </div>
            <div class="ly_ct">
                <div class="lst_info">
                    <div class="type_cont">
                        <table width="100%" cellspacing="0" cellpadding="0" class="table_style3">
                            <caption><?php echo __('지역별 추가 배송비')?></caption>
                            <colgroup>
                                <col width="50%" />
                                <col width="50%" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th><?php echo __('지역')?></th>
                                <th><?php echo __('추가 배송비')?></th>
                            </tr>
                            </thead>
                            <tbody>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['areaDetail'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
                            <tr>
                                <td class="country"><?php echo $TPL_V1["addArea"]?></td>
                                <td><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["addPrice"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                                    <span><?php echo gd_global_add_currency_display($TPL_V1["addPrice"])?></span>
<?php }?>
                                </td>
                            </tr>
<?php }}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //지역별 배송비 레이어 -->
<?php }?>
    <div class="detail_btm_tab_menu_box">
        <div class="detail_btm_tab_menu">
            <ul class="detail_sub_lst">
                <li id="goodsDescription" class="selected">
                    <a href="#" data-target="js_goods_description"><?php echo __('상세정보')?></a>
                </li>
                <li id="goodsInfo">
                    <a href="#" data-target="js_goods_detail_infotext"><?php echo __('기본정보')?></a>
                </li>
<?php if($TPL_VAR["canGoodsReview"]||$TPL_VAR["plusReviewConfig"]["isShowGoodsPage"]=='y'){?>
                <li id="detailReview" class="js_board_<?php echo $TPL_VAR["bdGoodsReviewId"]?>">
                    <a href="#" data-target="js_board_<?php echo $TPL_VAR["bdGoodsReviewId"]?>_view"  data-bdid="<?php echo $TPL_VAR["bdGoodsReviewId"]?>">
                        <?php echo __('상품후기')?>

                        <span class="itemnum"><?php echo $TPL_VAR["goodsReviewCount"]+$TPL_VAR["plusReview"]["info"]["reviewCount"]?></span>
                    </a>
                </li>
<?php }?>
                <li id="detailQna" class="js_board_<?php echo $TPL_VAR["bdGoodsQaId"]?>">
                    <a data-target="js_board_<?php echo $TPL_VAR["bdGoodsQaId"]?>_view" data-bdid="<?php echo $TPL_VAR["bdGoodsQaId"]?>" class="js_accordian">
                        <?php echo __('상품문의')?>

                        <span class="itemnum"><?php echo $TPL_VAR["goodsQaCount"]?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="detail_info_box js_goods_info">
        <div class="view_box0  js_goods_description">
            <!-- 상품상세 공통정보 관리를 상세정보 상단에 노출-->
            <div><?php echo gd_isset($TPL_VAR["goodsView"]['commonContent'])?></div>
            <div><?php echo gd_isset($TPL_VAR["goodsView"]['goodsDescriptionMobile'])?></div>
<?php if($TPL_VAR["goodsView"]['externalVideoFl']=='y'&&$TPL_VAR["goodsView"]['externalVideoUrl']){?>
            <div style="text-align:center" id="external-video">
                <?php echo gd_youtube_player($TPL_VAR["goodsView"]['externalVideoUrl'],$TPL_VAR["goodsView"]['externalVideoWidth'],$TPL_VAR["goodsView"]['externalVideoHeight'])?>

            </div>
<?php }?>
<?php if($TPL_VAR["goodsView"]['goodsMustInfo']){?>
            <div class="openblock js_openblock">
                <div class="openblock_header">
                    <h3><?php echo __('상품필수 정보')?></h3>
                    <button class="toggle_btn"><span><?php echo __('열기/닫기')?></span></button>
                </div>
                <div class="openblock_content">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['goodsMustInfo'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                    <dl>
                        <dt><?php echo $TPL_V2['infoTitle']?></dt>
                        <dd><?php echo $TPL_V2['infoValue']?></dd>
                    </dl>
<?php }}?>
<?php }}?>
                </div>
            </div>
<?php }?>
        </div>
        <div class="view_box0 js_goods_detail_infotext" style="display:none">
            <div class="openblock">
                <div class="openblock_header">
                    <h3><?php echo __('기본정보')?></h3>
                </div>
                <div class="openblock_content" >
                    <dl>
<?php if($TPL_displayField_1){foreach($TPL_VAR["displayField"] as $TPL_V1){?>
<?php switch($TPL_V1){case 'fixedPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['fixedPrice'])> 0&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dt><?php echo __('정가')?></dt>
                        <dd><?php echo $TPL_VAR["fixedPriceTag"]?><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['fixedPrice'])?><?php echo $TPL_VAR["fixedPriceTag2"]?><?php echo gd_global_currency_string()?></dd>
<?php }?>
<?php break;case 'goodsPrice':?>
                        <dt> <?php if($TPL_VAR["goodsView"]['timeSaleFl']){?><?php echo __('타임세일가')?><?php }else{?><?php echo __('판매가')?><?php }?></dt>
                        <dd>
<?php if($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='text'){?>
                            <?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_text"]?>

<?php }elseif($TPL_VAR["goodsView"]['soldOut']=='y'&&$TPL_VAR["soldoutDisplay"]["soldout_price"]=='custom'){?>
                            <img src="<?php echo $TPL_VAR["soldoutDisplay"]["soldout_price_img"]?>">
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['goodsPriceString']!=''){?>
                            <?php echo $TPL_VAR["goodsView"]['goodsPriceString']?>

<?php }else{?>
<?php if($TPL_VAR["goodsView"]['timeSaleFl']&&$TPL_VAR["goodsView"]['timeSaleInfo']['goodsPriceViewFl']=='y'&&$TPL_VAR["goodsView"]['oriGoodsPrice']> 0){?>
                            <?php echo $TPL_VAR["goodsPriceTag"]?><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['oriGoodsPrice'])?><?php echo $TPL_VAR["goodsPriceTag2"]?><?php echo gd_global_currency_string()?>

                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?><?php echo gd_global_currency_string()?>

<?php }else{?>
                            <?php echo $TPL_VAR["goodsPriceTag"]?><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice'])?><?php echo $TPL_VAR["goodsPriceTag2"]?><?php echo gd_global_currency_string()?>

<?php }?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                            <span><?php echo $TPL_VAR["goodsPriceTag"]?><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['goodsPrice'])?><?php echo $TPL_VAR["goodsPriceTag2"]?></span>
<?php }?>

<?php }?>
<?php }?>
                        </dd>
<?php break;case 'couponPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['couponPrice'])> 0&&$TPL_VAR["couponUse"]=='y'&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dt><?php echo __('쿠폰적용가')?></dt>
                        <dd>
                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['couponPrice'])?><?php echo gd_global_currency_string()?>

                            (-<?php echo gd_global_money_format($TPL_VAR["goodsView"]['couponSalePrice'])?><?php echo gd_global_currency_string()?><?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['couponDcRate'])){?>, <?php echo $TPL_VAR["goodsView"]['couponDcRate']?>%<?php }?>)
                        </dd>
<?php }?>
<?php break;case 'myCouponPrice':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['myCouponSalePrice'])> 0&&$TPL_VAR["couponUse"]=='y'&&$TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='y'){?>
                        <dt><?php echo __('내 쿠폰적용가')?></dt>
                        <dd>
                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['myCouponSalePrice'])?><?php echo gd_global_currency_string()?>

                            (-<?php echo gd_global_money_format($TPL_VAR["goodsView"]['myCouponPrice'])?><?php echo gd_global_currency_string()?><?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['myCouponDcRate'])){?>, <?php echo $TPL_VAR["goodsView"]['myCouponDcRate']?>%<?php }?>)
                        </dd>
<?php }?>
<?php break;case 'goodsDiscount':?>
<?php if($TPL_VAR["goodsView"]['dcPrice']> 0){?>
                        <dt><?php echo __('할인적용가')?></dt>
                        <dd>
                            <?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['goodsPrice']-$TPL_VAR["goodsView"]['dcPrice'])?><?php echo gd_global_currency_string()?>

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                            <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['goodsPrice']-$TPL_VAR["goodsView"]['dcPrice'])?></span>
<?php }?>
<?php if(in_array('dcRate',$TPL_VAR["displayAddField"])&&gd_isset($TPL_VAR["goodsView"]['goodsDcRate'])){?> <span>(<?php echo $TPL_VAR["goodsView"]['goodsDcRate']?>%)</span><?php }?>
                        </dd>
<?php }?>
<?php break;case 'maxOrderCnt':?>
<?php if(!($TPL_VAR["goodsView"]['minOrderCnt']==='1'&&$TPL_VAR["goodsView"]['maxOrderCnt']==='0')){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='option'){?>옵션당
<?php }elseif($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>상품당
<?php }elseif($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>ID당
<?php }?>
<?php if($TPL_VAR["goodsView"]['minOrderCnt']){?><?php echo __('최소 %s개',$TPL_VAR["goodsView"]['minOrderCnt'])?> <?php }?> <?php if($TPL_VAR["goodsView"]['maxOrderCnt']){?>/ <?php echo __('최대 %s개',$TPL_VAR["goodsView"]['maxOrderCnt'])?><?php }?>
                        </dd>
<?php }?>
<?php break;case 'benefit':?>
                        <dt class="benefits dn"><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd class="benefits dn">
                            <div>
                                <p class="benefits_dc_desc"><?php echo __('할인')?> : <strong class="total_benefit_price"></strong> [<span class="benefit_price"></span>]</p>
                                <p class="benefits_mileage_desc"><?php echo __('적립')?> <?php echo gd_display_mileage_name()?> : <strong class="total_benefit_mileage"></strong> [<span class="benefit_mileage"></span>]</p>
                            </div>
                        </dd>
<?php break;case 'couponDownload':?>
<?php if(empty($TPL_VAR["couponArrData"])===false){?>
                        <dt class="coupon"><?php echo __('쿠폰')?></dt>
                        <dd>
<?php if(gd_is_login()===false){?>
                            <button class="detail_coupon_btn1 btn_alert_login"><?php echo __('쿠폰받기')?></button>
<?php }else{?>
                            <a href="../goods/layer_coupon_down.php?goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>" class="detail_coupon_btn1" data-toggle="modal"  data-cache="false" data-target="#popupCoupon" data-type="post" ><?php echo __('쿠폰받기')?></a>
<?php }?>
                        </dd>
<?php }?>
<?php break;case 'delivery':?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
                        <dt class="delivery"><?php echo __('배송비')?></dt>
                        <dd>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='free'){?>
                            <span><?php echo __('무료')?></span>
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])=='fixed'){?>
                            <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                            <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge']['0']['price'])?></span>
<?php }?>
<?php }else{?>
                            <span><?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?><?php echo gd_global_currency_string()?></span>
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                            <span><?php echo gd_global_add_currency_display($TPL_VAR["goodsView"]['delivery']['charge'][$TPL_VAR["goodsView"]['selectedDeliveryPrice']]['price'])?></span>
<?php }?>
                            <a href="../goods/goods_info.php?target=detailConditionalShipping" data-toggle="modal" data-cache="false" data-target="#popupDetailInfo" class="condition_del">(<?php echo __('조건부배송')?>)</a>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['areaFl'])=='y'){?>
                            <a href="../goods/goods_info.php?target=detailRegionalShipping" data-toggle="modal" data-cache="false" data-target="#popupDetailInfo" class="area_del"><?php echo __('지역별 추가배송')?></a>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['fixFl'])!='free'){?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='pre'){?>
                            <?php echo __('주문시결제')?>(<?php echo __('선결제')?>)
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='later'){?>
                            <?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)
<?php }?>
<?php }?>
                        </dd>
<?php }?>
<?php break;case 'goodsWeight':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsWeight'])> 0&&gd_isset($TPL_VAR["goodsView"]['goodsVolume'])> 0){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo $TPL_VAR["goodsView"]['goodsWeight']?><?php echo $TPL_VAR["weight"]["unit"]?> / <?php echo $TPL_VAR["goodsView"]['goodsVolume']?><?php echo gd_isset($TPL_VAR["volume"]["unit"],'㎖')?></dd>
<?php }else{?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsWeight'])> 0){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo $TPL_VAR["goodsView"]['goodsWeight']?><?php echo $TPL_VAR["weight"]["unit"]?></dd>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['goodsVolume'])> 0){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo $TPL_VAR["goodsView"]['goodsVolume']?><?php echo gd_isset($TPL_VAR["volume"]["unit"],'㎖')?></dd>
<?php }?>
<?php }?>
<?php break;case 'addInfo':?>
<?php if(empty($TPL_VAR["goodsView"]['addInfo'])===false){?>
                        <!-- 추가항목 -->
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['addInfo'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                        <dt><?php echo __($TPL_V2["infoTitle"])?></dt>
                        <dd><?php echo $TPL_V2["infoValue"]?></dd>
<?php }}?>
                        <!-- // 추가항목 -->
<?php }?>
<?php break;case 'effectiveStartYmd':?>
<?php if(($TPL_VAR["goodsView"]['effectiveStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveStartYmd'])||($TPL_VAR["goodsView"]['effectiveEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveEndYmd'])){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd>
<?php if($TPL_VAR["goodsView"]['effectiveStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveStartYmd']){?>
                            <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['effectiveStartYmd'])?>

<?php }?>

<?php if($TPL_VAR["goodsView"]['effectiveEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['effectiveEndYmd']){?>
                            ~ <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['effectiveEndYmd'])?>

<?php }?>
                        </dd>
<?php }?>
<?php break;case 'salesStartYmd':?>
<?php if(($TPL_VAR["goodsView"]['salesStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesStartYmd'])||($TPL_VAR["goodsView"]['salesEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesEndYmd'])){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd>
<?php if($TPL_VAR["goodsView"]['salesStartYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesStartYmd']){?>
                            <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['salesStartYmd'])?>

<?php }?>

<?php if($TPL_VAR["goodsView"]['salesEndYmd']!='0000-00-00 00:00:00'&&$TPL_VAR["goodsView"]['salesEndYmd']){?>
                            ~ <?php echo gd_date_format(__('Y년 m월 d일'),$TPL_VAR["goodsView"]['salesEndYmd'])?>

<?php }?>
                        </dd>
<?php }?>
<?php break;case 'salesUnit':?>
<?php if(gd_isset($TPL_VAR["goodsView"]['salesUnit'])> 1){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo number_format($TPL_VAR["goodsView"]['salesUnit'])?><?php echo __('개')?></dd>
<?php }?>
<?php break;case 'totalStock':?>
<?php if($TPL_VAR["goodsView"]['stockFl']=='y'){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo number_format($TPL_VAR["goodsView"]['totalStock'])?><?php echo __('개')?></dd>
<?php }?>
<?php break;default:?>
<?php if(gd_isset($TPL_VAR["goodsView"][$TPL_V1])){?>
                        <dt><?php echo __($TPL_VAR["displayDefaultField"][$TPL_V1])?></dt>
                        <dd><?php echo $TPL_VAR["goodsView"][$TPL_V1]?></dd>
<?php }?>
<?php }?>
<?php }}?>
<?php if($TPL_VAR["naverPay"]){?>
                        <dt id="naver-mileage-accum" class="hidden"><?php echo __('네이버')?>&nbsp;&nbsp;<br/><?php echo __('마일리지')?> : <span id="naver-mileage-accum-rate"></span> <?php echo __('적립')?></dt>
<?php }?>
<?php if($TPL_VAR["infoDelivery"]){?>
                        <dt><?php echo __('배송안내')?></dt>
                        <dd><?php echo $TPL_VAR["infoDelivery"]?></dd>
<?php }?>
<?php if($TPL_VAR["infoExchange"]){?>
                        <dt><?php echo __('교환안내')?></dt>
                        <dd><?php echo $TPL_VAR["infoExchange"]?></dd>
<?php }?>
<?php if($TPL_VAR["infoRefund"]){?>
                        <dt><?php echo __('환불안내')?></dt>
                        <dd><?php echo $TPL_VAR["infoRefund"]?></dd>
<?php }?>
<?php if($TPL_VAR["infoAS"]){?>
                        <dt><?php echo __('AS안내')?></dt>
                        <dd><?php echo $TPL_VAR["infoAS"]?></dd>
<?php }?>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <div class="js_goods_info">
<?php if($TPL_VAR["plusReviewConfig"]["isShowGoodsPage"]=='y'){?>
        <?php echo includeFile($TPL_VAR["includePlusReviewFile"])?>

<?php }?>
<?php if($TPL_VAR["canGoodsReview"]){?>
        <div class="detail_board_box js_board_<?php echo $TPL_VAR["bdGoodsReviewId"]?>_view" data-bdid="<?php echo $TPL_VAR["bdGoodsReviewId"]?>" style="display:none">
            <div class="item_list_head">
                <div class="item_list_write_box">
                    <a href="../board/write.php?bdId=<?php echo $TPL_VAR["bdGoodsReviewId"]?>&goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>&gboard=r&windowType=popup" class="detail_write_btn" data-target="#popupBoard"><?php echo __('후기작성')?></a>
                </div>
            </div>
            <div class="item_list"></div>
        </div>
<?php }?>
<?php if($TPL_VAR["canGoodsQa"]){?>
        <div class="detail_board_box js_board_<?php echo $TPL_VAR["bdGoodsQaId"]?>_view" data-bdid="<?php echo $TPL_VAR["bdGoodsQaId"]?>" style="display:none">
            <div class="item_list_head">
                <div class="item_list_write_box">
                    <a href="../board/write.php?bdId=<?php echo $TPL_VAR["bdGoodsQaId"]?>&goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>&gboard=r&windowType=popup" class="detail_write_btn" data-target="#popupBoard"><?php echo __('문의하기')?></a>
                </div>
            </div>
            <div class="item_list"></div>
        </div>
<?php }?>
    </div>
<?php if($TPL_VAR["relation"]["relationFl"]!='n'){?>
    <div class="related_goods">
        <h3><?php echo __('관련 상품')?></h3>
        <ul class="list">
<?php if($TPL_VAR["widgetGoodsList"]){?><?php echo includeWidget('goods/_goods_display.html')?><?php }?>
        </ul>
    </div>
<?php }?>
</div>
<!-- 비밀글 클릭시 인증 레이어 -->
<form id="frmWritePassword">
    <div class="cite_layer dn js_password_layer zidx320" data-sno="">
        <div class="wrap">
            <h4><?php echo __('비밀번호 인증')?></h4>
            <div>
                <p><?php echo __('비밀번호를 입력해 주세요.')?></p>
                <input type="password" name="writerPw">
                <a href="javascript:void(0)" class="d_pw_confirm_btn layer_close_btn js_submit"><?php echo __('확인')?></a>
            </div>
            <button type="button" class="close" title="<?php echo __('닫기')?>"><?php echo __('닫기')?></button>
        </div>
    </div>
</form>
<!-- 장바구니 담기 레이어 -->
<?php if($TPL_VAR["cartInfo"]["moveCartPageFl"]=='n'&&$TPL_VAR["cartInfo"]["moveCartPageDeviceFl"]!='pc'){?>
<div id="addCartLayer" class="add_cart_layer dn">
    <div class="wrap">
        <div>
            <p><strong><?php echo __('상품이 장바구니에 담겼습니다.')?></strong><br /><?php echo __('바로 확인하시겠습니까?')?></p>
        </div>
        <div class="btn_box">
            <button class="btn layer_cartaddcancel btn_close"><?php echo __('취소')?></button>
            <button class="btn layer_cartaddconfirm btn_cart"><?php echo __('확인')?></button>
        </div>
    </div>
</div>
<?php }?>
<?php if($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='n'&&$TPL_VAR["cartInfo"]["moveWishPageDeviceFl"]!='pc'){?>
<!-- 찜리스트 레이어 -->
<div id="addWishLayer" class="add_wish_layer dn">
    <div class="wrap">
        <div>
            <p><strong><?php echo __('상품이 찜 리스트에 담겼습니다.')?></strong><br /><?php echo __('바로 확인하시겠습니까?')?></p>
        </div>
        <div class="btn_box">
            <button class="btn layer_wishaddcancel btn_close"><?php echo __('취소')?></button>
            <button class="btn layer_wishaddconfirm btn_wish"><?php echo __('확인')?></button>
        </div>
    </div>
</div>
<?php }?>
<div id="layerDim" class="dn zidx310">&nbsp;</div>
<script type="text/javascript" src="/data/skin/mobile/aileenwedding/js/gd_board_goods.js" charset="utf-8"></script>
<?php echo $TPL_VAR["fbGoodsViewScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>