<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/_join_view.html 000009412 */ 
if (is_array($TPL_VAR["emailDomain"])) $TPL_emailDomain_1=count($TPL_VAR["emailDomain"]); else if (is_object($TPL_VAR["emailDomain"]) && in_array("Countable", class_implements($TPL_VAR["emailDomain"]))) $TPL_emailDomain_1=$TPL_VAR["emailDomain"]->count();else $TPL_emailDomain_1=0;?>
<div class="join_view">
	<p class="tit"><?php echo __('기본정보')?></p>
<?php if($TPL_VAR["joinField"]["businessinfo"]["use"]=='y'&&!$TPL_VAR["isMyPage"]){?>
	<div class="input_wrap">
		<div class="input_title"><?php echo __('회원종류')?></div>
		<div class="input_content">
			<span class="inp_rdo">
				<input type="radio" class="ignore" id="memberFlDefault" name="memberFl" value="personal" value="personal" checked="checked"/>
				<label for="memberFlDefault" class="on"><?php echo __('개인회원')?></label>
				<input type="radio" class="ignore" id="memberFlBusiness" name="memberFl" value="business" value="business"/>
				<label for="memberFlBusiness"><?php echo __('사업자회원')?></label>
			</span>
	   </div>
	</div>
<?php }?>
	<div class="input_wrap">
		<div class="input_title"><label for="memId"><?php echo __('아이디')?></label></div>
		<div class="input_content">
<?php if($TPL_VAR["isMyPage"]){?>
			<input type="hidden" name="memId" value="<?php echo $TPL_VAR["data"]["memId"]?>"/>
			<span class="member_id"><?php echo $TPL_VAR["data"]["memId"]?></span>
<?php }else{?>
			<input type="text" name="memId" id="memId" data-pattern="gdMemberId" />
<?php }?>
		</div>
	</div>
	<div class="input_wrap <?php if($TPL_VAR["isPaycoJoin"]||$TPL_VAR["isThirdParty"]||$TPL_VAR["isNaverJoin"]||$TPL_VAR["isKakaoJoin"]||$TPL_VAR["isWonderJoin"]){?>dn<?php }?>">
		<div class="input_title"><label for="newPassword"><?php echo __('비밀번호')?></label></div>
		
		<div class="input_content">
<?php if($TPL_VAR["isMyPage"]){?>
<?php if($TPL_VAR["useSnsLogin"]=='n'){?>
			<button id="btnPassword" class="modify_pw_btn btnPassword"><?php echo __('비밀번호 변경')?></button>
<?php }else{?>
			<button id="btnPassword" class="modify_pw_btn btnPassword"><?php echo __('비밀번호 설정')?></button>
<?php }?>
			<div class="password_reset modify_pw dn">
				<div class="<?php if($TPL_VAR["isEmptyPassword"]){?> dn <?php }?>">
					<label for="currentPassword"><?php echo __('현재 비밀번호')?></label>
					<input type="password" id="currentPassword" name="oldMemPw" />
				</div>
				<div class="input-block">
					<label for="newPassword"><?php echo __('새 비밀번호')?></label>
					<input type="password" id="newPassword" name="memPw" />
				</div>
				<div class="input-block">
					<label for="newPasswordCheck"><?php echo __('새 비밀번호 확인')?></label>
					<input type="password" id="newPasswordCheck" name="memPwRe" />
				</div>
			</div>
<?php }else{?>
			<input type="password" id="newPassword" name="memPw"  autocomplete="off" placeholder="<?php echo __('영문대/소문자, 숫자, 특수문자 중 2가지 이상 조합하세요')?>"/>
<?php }?>
		</div>
	</div>
<?php if(!$TPL_VAR["isMyPage"]){?>
	<div class="input_wrap <?php if($TPL_VAR["isPaycoJoin"]||$TPL_VAR["isThirdParty"]||$TPL_VAR["isNaverJoin"]||$TPL_VAR["isKakaoJoin"]||$TPL_VAR["isWonderJoin"]){?>dn<?php }?>">
		<div class="input_title"><label for="memPwRe"><?php echo __('비밀번호확인')?></label></div>
		<div class="input_content">
			<input type="password" name="memPwRe" id="memPwRe" autocomplete="off">
		</div>
	</div>
<?php }?>
	<div class="input_wrap">
		<div class="input_title"><label for="memNm"><?php echo __('이름')?></label></div>
		<div class="input_content">
			<input type="text"  name="memNm" id="memNm" data-pattern="gdMemberNmGlobal" value="<?php echo $TPL_VAR["data"]["memNm"]?>" maxlength="30" <?php echo $TPL_VAR["data"]["authReadOnly"]?>>
		</div>
	</div>
<?php if($TPL_VAR["joinField"]["pronounceName"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="pronounceName"><?php echo __('이름')?>(<?php echo __('발음')?>)</label></div>
		<div class="input_content">
			<input type="text"  name="pronounceName" id="pronounceName" data-pattern="<?php if($TPL_VAR["gGlobal"]['isFront']){?>gdMemberNmGlobal<?php }else{?>gdEngKor<?php }?>" value="<?php echo $TPL_VAR["data"]["pronounceName"]?>" maxlength="30">
		</div>
	</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["nickNm"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="nickNm"><?php echo __('닉네임')?></label></div>
		<div class="input_content"><input type="text"  name="nickNm" id="nickNm" maxlength="30" value="<?php echo $TPL_VAR["data"]["nickNm"]?>"></div>
	</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["email"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="email"><?php echo __('이메일')?></label></div>
		<div class="input_content">
			<input type="text" class="input_email" name="email" id="email" value="<?php echo $TPL_VAR["data"]["email"]?>">
			<div class="email_select_box">
				<select name="emailDomain" id="emailDomain" class="email_select">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
					<option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
				</select>
			</div>
		</div>
	</div>
	<div class="input_wrap">
		<div class="input_title"></div>
		<div class="input_content">
<?php if($TPL_VAR["joinField"]["maillingFl"]["use"]==='y'){?>
			<div class="description">
				<span class="inp_chk">
					<input type="checkbox" id="maillingFl" class="ignore" name="maillingFl" value="y" <?php if($TPL_VAR["data"]["maillingFl"]=='y'){?>checked="checked"<?php }?>>
					<label for="maillingFl"><?php echo __('정보/이벤트 메일 수신에 동의합니다.')?></label>
				</span>
			</div>
<?php }?>
			<div class="description">※ <?php echo __('아이디 비밀번호 찾기에 활용 되므로 정확하게 입력해 주세요')?></div>
		</div>
	</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["cellPhone"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="cellPhone"><?php echo __('휴대폰번호')?></label></div>
		<div class="input_content">
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
			<div class="select_small_outer">
				<div class="inp_sel">
					<?php echo gd_select_box('cellPhoneCountryCode','cellPhoneCountryCode',$TPL_VAR["countryPhone"],null,$TPL_VAR["data"]["cellPhoneCountryCode"],__('국가코드'),null,'tune select-small')?>

				</div>
			</div>
<?php }?>
			<input type="text" id="cellPhone" name="cellPhone" maxlength="11" placeholder="<?php echo __('- 없이 입력하세요')?>" data-pattern="gdNum" value="<?php echo $TPL_VAR["data"]["cellPhone"]?>" <?php echo $TPL_VAR["data"]["authReadOnly"]?>>
<?php if($TPL_VAR["isMyPage"]){?>
<?php if($TPL_VAR["data"]["authReadOnly"]!=''){?>
			<button class="btn-db-bd post-search" type="button" id="btnAuthPhone">
				<em class="h35 arrow">휴대폰번호 변경</em>
			</button>
<?php }?>
<?php }?>
		</div>
	</div>
<?php if($TPL_VAR["joinField"]["smsFl"]["use"]==='y'){?>
	<div class="input_wrap">
		<div class="input_title"></div>
		<div class="input_content">
			<div class="description">
				<span class="inp_chk">
					<input type="checkbox" class="ignore" id="smsFl" name="smsFl" value="y" <?php if($TPL_VAR["data"]["smsFl"]=='y'){?>checked="checked"<?php }?>>
					<label for="smsFl"><?php echo __('정보/이벤트 SMS 수신에 동의합니다.')?></label>
				</span>
			</div>
			
		</div>
	</div>
<?php }?>
<?php }?>
<?php if($TPL_VAR["joinField"]["phone"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="phone"><?php echo __('전화번호')?></label></div>
		<div class="input_content">
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
			<div class="select_small_outer">
				<div class="inp_sel">
					<?php echo gd_select_box('phoneCountryCode','phoneCountryCode',$TPL_VAR["countryPhone"],null,$TPL_VAR["data"]["phoneCountryCode"],__('국가코드'),null,'tune select-small')?>

				</div>
			</div>
<?php }?>
			<input type="text" id="phone" name="phone" maxlength="12" placeholder="<?php echo __('- 없이 입력하세요')?>" data-pattern="gdNum" value="<?php echo $TPL_VAR["data"]["phone"]?>">
		</div>
	</div>
<?php }?><?php if($TPL_VAR["joinField"]["address"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="btnPostcode"><?php echo __('주소')?></label></div>
		<div class="input_content">
			<div class="zipcode_box">
				<div class="zipcode_top">
					<div class="zipcode_num">
						<input type="text" name="zonecode" readonly="readonly" value="<?php if($TPL_VAR["data"]["zonecode"]==''){?><?php echo $TPL_VAR["data"]["zipcode"]?><?php }else{?><?php echo $TPL_VAR["data"]["zonecode"]?><?php }?>">
						<input type="hidden" name="zipcode" value="<?php echo $TPL_VAR["data"]["zipcode"]?>">
					</div>
					<div class="zipcode_btn_box"><button class="zipcode_btn" type="button" id="btnPostcode"><?php echo __('우편번호')?></button></div>
				</div>
				<input type="text"  name="address" readonly="readonly" value="<?php echo $TPL_VAR["data"]["address"]?>">
				<input type="text"  name="addressSub" value="<?php echo $TPL_VAR["data"]["addressSub"]?>">
			</div>
		</div>
	</div>
<?php }?>
</div>