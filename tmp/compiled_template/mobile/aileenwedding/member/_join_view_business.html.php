<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/_join_view_business.html 000003449 */ ?>
<?php if($TPL_VAR["joinField"]["businessinfo"]["use"]=='y'){?>
<div class="join_view_business <?php if(!$TPL_VAR["isMyPage"]){?>businessinfo dn<?php }?>">
	<p class="tit"><?php echo __('사업자 정보')?></p>
<?php if($TPL_VAR["joinField"]["company"]["use"]=='y'){?>
    <div class="input_wrap">
        <div class="input_title"><?php echo __('상호')?></div>
        <div class="input_content">
            <input type="text"  name="company" value="<?php echo $TPL_VAR["data"]["company"]?>" maxlength="50" data-pattern="gdMemberNmGlobal">
        </div>
    </div>
<?php }?><?php if($TPL_VAR["joinField"]["busiNo"]["use"]=='y'){?>
    <div class="input_wrap">
        <div class="input_title"><?php echo __('사업자번호')?></div>
        <div class="input_content">
            <input type="text" name="busiNo" id="busiNo"  maxlength="10" placeholder="<?php echo __('- 없이 입력하세요')?>" data-pattern="gdNum" value="<?php echo $TPL_VAR["data"]["busiNo"]?>" data-overlap-businofl="<?php echo $TPL_VAR["joinField"]["busiNo"]["overlapBusiNoFl"]?>" data-charlen="<?php echo $TPL_VAR["joinField"]["busiNo"]["charlen"]?>" data-oldbusino="<?php echo $TPL_VAR["data"]["busiNo"]?>">
        </div>
    </div>
<?php }?><?php if($TPL_VAR["joinField"]["ceo"]["use"]=='y'){?>
    <div class="input_wrap">
        <div class="input_title"><?php echo __('대표자명')?></div>
        <div class="input_content">
            <input type="text"  name="ceo" value="<?php echo $TPL_VAR["data"]["ceo"]?>" maxlength="20" data-pattern="gdEngKor">
        </div>
    </div>
<?php }?><?php if($TPL_VAR["joinField"]["service"]["use"]=='y'){?>
    <div class="input_wrap">
        <div class="input_title"><?php echo __('업태')?></div>
        <div class="input_content">
            <input type="text"  name="service" value="<?php echo $TPL_VAR["data"]["service"]?>" maxlength="30" data-pattern="gdEngKor">
        </div>
    </div>
<?php }?><?php if($TPL_VAR["joinField"]["item"]["use"]=='y'){?>
    <div class="input_wrap">
        <div class="input_title"><?php echo __('종목')?></div>
        <div class="input_content">
            <input type="text"  name="item" value="<?php echo $TPL_VAR["data"]["item"]?>" maxlength="30" data-pattern="gdEngKor">
        </div>
    </div>
<?php }?><?php if($TPL_VAR["joinField"]["comAddress"]["use"]=='y'){?>
	<div class="input_wrap">
		<div class="input_title"><label for="btnPostcode"><?php echo __('주소')?></label></div>
		<div class="input_content">
			<div class="zipcode_box">
				<div class="zipcode_top">
					<div class="zipcode_num">
						 <label for="" class="zipcode"><input type="text" class="text" name="comZonecode" readonly="readonly" value="<?php echo $TPL_VAR["data"]["comZipcode"]?>"></label>
						 <input type="hidden" name="comZipcode" value="<?php echo $TPL_VAR["data"]["comZonecode"]?>">
					</div>
					<div class="zipcode_btn_box"><button class="zipcode_btn" type="button" id="btnCompanyPostcode"><?php echo __('우편번호')?></button></div>
				</div>
				<input type="text"  name="comAddress" readonly="readonly" value="<?php echo $TPL_VAR["data"]["comAddress"]?>">
				<input type="text"  name="comAddressSub" value="<?php echo $TPL_VAR["data"]["comAddressSub"]?>">
			</div>
		</div>
	</div>

<?php }?>
</div>
<?php }?>