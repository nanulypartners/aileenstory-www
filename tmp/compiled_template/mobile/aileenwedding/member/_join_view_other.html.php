<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/_join_view_other.html 000025184 */ 
if (is_array($TPL_VAR["privateApprovalOption"])) $TPL_privateApprovalOption_1=count($TPL_VAR["privateApprovalOption"]); else if (is_object($TPL_VAR["privateApprovalOption"]) && in_array("Countable", class_implements($TPL_VAR["privateApprovalOption"]))) $TPL_privateApprovalOption_1=$TPL_VAR["privateApprovalOption"]->count();else $TPL_privateApprovalOption_1=0;
if (is_array($TPL_VAR["privateConsign"])) $TPL_privateConsign_1=count($TPL_VAR["privateConsign"]); else if (is_object($TPL_VAR["privateConsign"]) && in_array("Countable", class_implements($TPL_VAR["privateConsign"]))) $TPL_privateConsign_1=$TPL_VAR["privateConsign"]->count();else $TPL_privateConsign_1=0;
if (is_array($TPL_VAR["privateOffer"])) $TPL_privateOffer_1=count($TPL_VAR["privateOffer"]); else if (is_object($TPL_VAR["privateOffer"]) && in_array("Countable", class_implements($TPL_VAR["privateOffer"]))) $TPL_privateOffer_1=$TPL_VAR["privateOffer"]->count();else $TPL_privateOffer_1=0;?>
<div class="join_view_other">
<?php if($TPL_VAR["joinField"]["options"]["use"]=='y'){?>
	<div class="tit"><?php echo __('부가 정보')?></div>
<?php if($TPL_VAR["joinField"]["fax"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('팩스번호')?></div>
			<div class="input_content">
				<input type="text" id="fax" name="fax" data-pattern="gdNum" maxlength="12" placeholder="<?php echo __('- 없이 입력하세요')?>" value="<?php echo $TPL_VAR["data"]["fax"]?>">
			</div>
		</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["sexFl"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('성별')?></div>
			<div class="input_content">
				<span class="inp_rdo">
					<input type="radio" class="ignore" id="sexFlMan" name="sexFl" value="m" <?php if($TPL_VAR["data"]["sexFl"]=='m'){?>checked="checked"<?php }else{?><?php echo $TPL_VAR["data"]["authDisabled"]?><?php }?>/>
					<label for="sexFlMan" class="<?php if($TPL_VAR["data"]["sexFl"]=='m'){?>on<?php }?>"><?php echo __('남자')?></label>
					<input type="radio" class="ignore" id="sexFlWoman" name="sexFl" value="w" <?php if($TPL_VAR["data"]["sexFl"]=='w'){?>checked="checked"<?php }else{?><?php echo $TPL_VAR["data"]["authDisabled"]?><?php }?>/>
					<label for="sexFlWoman" class="<?php if($TPL_VAR["data"]["sexFl"]=='w'){?>on<?php }?>"><?php echo __('여자')?></label>
				</span>
			</div>
		</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["birthDt"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('생일')?></div>
			<div class="input_content">
<?php if($TPL_VAR["joinField"]["calendarFl"]["use"]=='y'){?>
					<div class="inp_sel">
						<select name="calendarFl" id="calendarFl" class="tune select-small" tabindex="-1">
							<option value=""><?php echo __('선택')?></option>
							<option value="s" <?php if($TPL_VAR["data"]["calendarFl"]=='s'){?>selected="selected"<?php }?>><?php echo __('양력')?></option>
							<option value="l" <?php if($TPL_VAR["data"]["calendarFl"]=='l'){?>selected="selected"<?php }?>><?php echo __('음력')?></option>
						</select>
					</div>
<?php }?>
				<div class="inp_sel">
					<?php echo gd_select_box('birthYear','birthYear',$TPL_VAR["DateYear"],null,$TPL_VAR["data"]["birthYear"],__('년'),'style="width: 40%;"','tune select-small')?>

					<?php echo gd_select_box('birthMonth','birthMonth',$TPL_VAR["DateMonth"],null,$TPL_VAR["data"]["birthMonth"],__('월'),'style="width: 25%;"','tune select-small')?>

					<?php echo gd_select_box('birthDay','birthDay',$TPL_VAR["DateDay"],null,$TPL_VAR["data"]["birthDay"],__('일'),'style="width: 32%;"','tune select-small')?>

				</div>
			</div>
		</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["marriFl"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('결혼여부')?></div>
			<div class="input_content">
				<span class="inp_rdo">
					<input type="radio" class="ignore" id="marriFlNo" name="marriFl" value="n" <?php if($TPL_VAR["data"]["marriFl"]=='n'){?>checked="checked"<?php }?>>
					<label for="marriFlNo" class="choice <?php if($TPL_VAR["data"]["marriFl"]=='n'){?>on<?php }?>"><?php echo __('미혼')?></label>
					<input type="radio" class="ignore" id="marriFlYes" name="marriFl" value="y" <?php if($TPL_VAR["data"]["marriFl"]=='y'){?>checked="checked"<?php }?>>
					<label for="marriFlYes" class="choice <?php if($TPL_VAR["data"]["marriFl"]=='y'){?>on<?php }?>"><?php echo __('기혼')?></label>
				</span>
			</div>
		</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["marriDate"]["use"]=='y'){?>
		<div class="input_wrap marridateinfo <?php if($TPL_VAR["data"]["marriFl"]=='n'){?>dn<?php }?>">
			<div class="input_title"><?php echo __('결혼기념일')?></div>
			<div class="input_content">
				<div class="inp_sel">
					<?php echo gd_select_box('marriYear','marriYear',$TPL_VAR["DateYear"],null,$TPL_VAR["data"]["marriYear"],__('년'),'style="width: 40%;"','tune select-small')?>

					<?php echo gd_select_box('marriMonth','marriMonth',$TPL_VAR["DateMonth"],null,$TPL_VAR["data"]["marriMonth"],__('월'),'style="width: 25%;"','tune select-small')?>

					<?php echo gd_select_box('marriDay','marriDay',$TPL_VAR["DateDay"],null,$TPL_VAR["data"]["marriDay"],__('일'),'style="width: 32%;"','tune select-small')?>

				</div>
			</div>
		</div>
<?php }?>
<?php if($TPL_VAR["joinField"]["job"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('직업')?></div>
			<div class="input_content">
				<div class="select-outer inp_sel">
					<select name="job" >
					<option value=""><?php echo __('직업을 선택해 주세요.')?></option>
<?php if((is_array($TPL_R1=$TPL_VAR["joinField"]["job"]["data"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?><?php if($TPL_K1==$TPL_VAR["data"]['job']){?>
					<option value="<?php echo $TPL_K1?>" selected="selected"><?php echo __($TPL_V1)?></option>
<?php }else{?>
					<option value="<?php echo $TPL_K1?>"><?php echo __($TPL_V1)?></option>
<?php }?><?php }}?>
					</select>
				</div>
			</div>
		</div>
<?php }?><?php if($TPL_VAR["joinField"]["interest"]["use"]=='y'&&count($TPL_VAR["joinField"]["interest"]["data"])> 0){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('관심분야')?></div>
			<div class="input_content">
				<div class="inp_sel">
					<select name="interest">
					<option value=""><?php echo __('관심분야를 선택해 주세요')?></option>
<?php if((is_array($TPL_R1=$TPL_VAR["joinField"]["interest"]["data"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_K1==$TPL_VAR["data"]['interest']){?>
					<option value="<?php echo $TPL_K1?>" selected="selected"><?php echo __($TPL_V1)?></option>
<?php }else{?>
					<option value="<?php echo $TPL_K1?>"><?php echo __($TPL_V1)?></option>
<?php }?>
<?php }}?>
					</select>
				</div>
			</div>
		</div>
<?php }?><?php if($TPL_VAR["joinField"]["recommId"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><span><?php echo __('추천인 아이디')?></span></div>
			<div class="input_content">
<?php if($TPL_VAR["data"]["recommId"]!=''||($TPL_VAR["isMyPage"]&&$TPL_VAR["joinField"]["recommFl"]["use"]=='y')){?>
				<input type="hidden" name="recommId" value="<?php echo $TPL_VAR["data"]["recommId"]?>">
				<span><?php echo $TPL_VAR["data"]["recommId"]?></span>
<?php }else{?>
				<input type="text" id="recommId" name="recommId">
<?php }?>
			</div>
		</div>
<?php }?><?php if($TPL_VAR["joinField"]["expirationFl"]["use"]=='y'&&!$TPL_VAR["getMemberLifeEventCount"]&&$TPL_VAR["data"]["expirationFl"]!='999'){?>
	<div class="input_wrap">
		<div class="input_title"><span><?php echo __('휴면회원 방지기간')?></span></div>
		<div class="input_content">
			<div class="select-small-outer inp_sel">
				<select name="expirationFl" class="tune select-small">
					<option value="1" <?php if($TPL_VAR["data"]["expirationFl"]=='1'){?>selected="selected" <?php }?>><?php echo __('%s년','1')?></option>
					<option value="3" <?php if($TPL_VAR["data"]["expirationFl"]=='3'){?>selected="selected" <?php }?>><?php echo __('%s년','3')?></option>
					<option value="5" <?php if($TPL_VAR["data"]["expirationFl"]=='5'){?>selected="selected" <?php }?>><?php echo __('%s년','5')?></option>
					<option value="999" <?php if($TPL_VAR["data"]["expirationFl"]=='999'){?>selected="selected" <?php }?>><?php echo __('탈퇴 시 - 평생회원')?></option>
				</select>
			</div>
			<!-- 평생회원 이벤트 안내문구 -->
			<div class="member_warning_info <?php if(!$TPL_VAR["activeEvent"]||$TPL_VAR["memberLifeEventCnt"]> 0||$TPL_VAR["memberLifeEventView"]==='hidden'){?>dn<?php }?>">
				<div class="info_title">평생회원 이벤트</div>
				<div class="info_text">
					휴면회원 방지기간을 ‘탈퇴 시’로 변경하시면,<br>
					휴면회원으로 전환되지 않으며 고객님의 정보가 탈퇴 시까지 안전하게 보관됩니다.<br>
<?php if($TPL_VAR["benefitType"]!='manual'&&$TPL_VAR["benefitInfo"]){?>
					<br>
					지금 평생회원으로 전환 시 <span class="text_red"><?php echo $TPL_VAR["benefitInfo"]?></span> 제공!
<?php }?>
				</div>
			</div>
			<!-- 평생회원 이벤트 안내문구 -->
		</div>
	</div>
<?php }?><?php if($TPL_VAR["joinField"]["memo"]["use"]=='y'){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo __('남기는 말씀')?></div>
			<div class="input_content">
				<textarea name="memo" cols="20" rows="5" maxlength="300"><?php echo $TPL_VAR["data"]["memo"]?></textarea>
			</div>
		</div>
<?php }?><?php if((is_array($TPL_R1=$TPL_VAR["joinField"]["ex"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
		<div class="input_wrap">
			<div class="input_title"><?php echo $TPL_V1["title"]?></div>
			<div class="input_content">
<?php if($TPL_V1["type"]=='SELECT'){?>
				<div class="inp_sel">
					<select name="<?php echo $TPL_K1?>">
						<option value=""><?php echo __('선택')?></option>
<?php if((is_array($TPL_R2=$TPL_V1["items"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if(($TPL_K1=='ex1'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex1"])||($TPL_K1=='ex2'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex2"])||($TPL_K1=='ex3'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex3"])||($TPL_K1=='ex4'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex4"])||($TPL_K1=='ex5'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex5"])||($TPL_K1=='ex6'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex6"])){?>
						<option value="<?php echo $TPL_V2?>" selected="selected"><?php echo $TPL_V2?></option>
<?php }else{?>
						<option value="<?php echo $TPL_V2?>"><?php echo $TPL_V2?></option>
<?php }?>
<?php }}?>
					</select>
				</div>
<?php }elseif($TPL_V1["type"]=='RADIO'){?>
<?php if((is_array($TPL_R2=$TPL_V1["items"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_V2){$TPL_I2++;?>
				<span class="inp_rdo">
<?php if(($TPL_K1=='ex1'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex1"])||($TPL_K1=='ex2'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex2"])||($TPL_K1=='ex3'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex3"])||($TPL_K1=='ex4'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex4"])||($TPL_K1=='ex5'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex5"])||($TPL_K1=='ex6'&&gd_trim($TPL_V2)==$TPL_VAR["data"]["ex6"])){?>
				<input id="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>" name="<?php echo $TPL_K1?>" value="<?php echo $TPL_V2?>" type="radio" class="ignore" checked="checked"/>
				<label for="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>"><?php echo $TPL_V2?></label><br/>
<?php }else{?>
				<input id="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>" name="<?php echo $TPL_K1?>" value="<?php echo $TPL_V2?>" class="ignore" type="radio"/>
				<label for="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>"><?php echo $TPL_V2?></label><br/>
<?php }?>
<?php }}?>
				</span>
<?php }elseif($TPL_V1["type"]=='CHECKBOX'){?>
				<div class="choice_check">
<?php if((is_array($TPL_R2=$TPL_V1["items"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {$TPL_I2=-1;foreach($TPL_R2 as $TPL_V2){$TPL_I2++;?>
					<span class="inp_chk">
<?php if(is_array($TPL_VAR["data"]["ex1"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex1"])||is_array($TPL_VAR["data"]["ex2"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex2"])||is_array($TPL_VAR["data"]["ex3"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex3"])||is_array($TPL_VAR["data"]["ex4"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex4"])||is_array($TPL_VAR["data"]["ex5"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex5"])||is_array($TPL_VAR["data"]["ex6"])&&in_array(gd_trim($TPL_V2),$TPL_VAR["data"]["ex6"])){?>
						<input id="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>" name="<?php echo $TPL_K1?>[]" value="<?php echo $TPL_V2?>" class="ignore" type="checkbox" checked="checked"/>
						<label for="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>"><?php echo $TPL_V2?></label>
<?php }else{?>
						<input id="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>" name="<?php echo $TPL_K1?>[]" value="<?php echo $TPL_V2?>" class="ignore" type="checkbox"/>
						<label for="<?php echo $TPL_K1?>_<?php echo $TPL_I2?>"><?php echo $TPL_V2?></label>
<?php }?>
					</span><br/>
<?php }}?>
				</div>
<?php }else{?>
				<input type="text" name="<?php echo $TPL_K1?>" id="<?php echo $TPL_K1?>" value="<?php echo $TPL_VAR["data"][$TPL_K1]?>"/>
<?php }?>
			</div>
		</div>
<?php }}?>
<?php }?>
<?php if($TPL_VAR["isMyPage"]){?>
<?php if($TPL_VAR["usePaycoLogin"]||$TPL_VAR["useFacebookLogin"]||$TPL_VAR["useNaverLogin"]||$TPL_VAR["useKakaoLogin"]||$TPL_VAR["useWonderLogin"]){?>
	<div class="tit"><?php echo __('계정 연결정보')?></div>
	<div class="sns_content">
<?php if($TPL_VAR["connectFacebook"]){?>
		<div class="sns_disconnect">
			<img src="/data/skin/mobile/aileenwedding/img/etc/square_facebook.png" alt="<?php echo __('페이스북')?>"><span><?php echo __('페이스북')?></span>
<?php if($TPL_VAR["data"]['snsJoinFl']=='n'){?>
			<button class="disconnect_sns_btn js_btn_sns_disconnect" data-facebook-url="<?php echo $TPL_VAR["facebookUrl"]?>" data-sns="facebook"><?php echo __('연결해제')?></button>
<?php }?>
		</div>
<?php }elseif($TPL_VAR["connectPayco"]){?>
		<div class="sns_disconnect">
			<img src="/data/skin/mobile/aileenwedding/img/etc/square_payco.png" alt="<?php echo __('페이코')?>"><span><?php echo __('페이코')?></span>
<?php if($TPL_VAR["data"]['snsJoinFl']=='n'){?>
			<button class="disconnect_sns_btn js_btn_sns_disconnect" data-sns="payco"><?php echo __('연결해제')?></button>
<?php }?>
		</div>
<?php }elseif($TPL_VAR["connectNaver"]){?>
		<div class="sns_disconnect">
			<img src="/data/skin/mobile/aileenwedding/img/etc/square_naver.png" alt="<?php echo __('네이버')?>"><span><?php echo __('네이버')?></span>
<?php if($TPL_VAR["data"]['snsJoinFl']=='n'){?>
			<button class="disconnect_sns_btn js_btn_sns_disconnect" data-sns="naver"><?php echo __('연결해제')?></button>
<?php }?>
		</div>
<?php }elseif($TPL_VAR["connectKakao"]){?>
		<div class="sns_disconnect">
			<img src="/data/skin/mobile/aileenwedding/img/etc/square_kakao.png" alt="<?php echo __('카카오')?>"><span><?php echo __('카카오')?></span>
<?php if($TPL_VAR["data"]['snsJoinFl']=='n'){?>
			<button class="disconnect_sns_btn js_btn_sns_disconnect" data-sns="kakao" data-kakao-url="<?php echo $TPL_VAR["kakaoUrl"]?>"><?php echo __('연결해제')?></button>
<?php }?>
		</div>
<?php }elseif($TPL_VAR["connectWonder"]){?>
		<div class="sns_disconnect">
			<img src="/data/skin/mobile/aileenwedding/img/etc/square_wonder.png" alt="<?php echo __('위메프')?>"><span><?php echo __('위메프 로그인')?></span>
<?php if($TPL_VAR["data"]['snsJoinFl']=='n'){?>
			<button class="disconnect_sns_btn js_btn_sns_disconnect" data-sns="wonder" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>"><?php echo __('연결해제')?></button>
<?php }?>
		</div>
<?php }else{?>
		<p class="sns_message"><?php echo __('연결된 계정이 없습니다.')?></p>
		<div class="sns_connect_box">
			<ul>
<?php if($TPL_VAR["usePaycoLogin"]){?>
				<li class="payco"><a href="#button" class="js_btn_sns_connect" data-sns="payco"><img src="/data/skin/mobile/aileenwedding/img/etc/mo_payco_s.png" alt="<?php echo __('PAYCO')?> <?php echo __('아이디 로그인')?>"></a></li>
<?php }?>
<?php if($TPL_VAR["useFacebookLogin"]){?>
				<li class="facebook"><a href="#button" class="js_btn_sns_connect" data-sns="facebook" data-facebook-url="<?php echo $TPL_VAR["facebookUrl"]?>"><img src="/data/skin/mobile/aileenwedding/img/etc/mo_facebook_s.png" alt="<?php echo __('FACEBOOK')?> <?php echo __('아이디 로그인')?>"></a></li>
<?php }?>
<?php if($TPL_VAR["useNaverLogin"]){?>
				<li class="naver"><a href="#button" class="js_btn_sns_connect btn" data-sns="naver"><img src="/data/skin/mobile/aileenwedding/img/etc/mo_naver_s.png" alt="<?php echo __('네이버')?> <?php echo __('아이디 로그인')?>"></a></li>
<?php }?>
<?php if($TPL_VAR["useKakaoLogin"]){?>
				<li class="kakao"><a href="#button" class="js_btn_sns_connect btn" data-sns="kakao"><img src="/data/skin/mobile/aileenwedding/img/etc/mo_kakao_s.png" alt="<?php echo __('카카오')?> <?php echo __('카카오 연결')?>"></a></li>
<?php }?>
<?php if($TPL_VAR["useWonderLogin"]){?>
				<li class="wonder"><a href="#button" class="js_btn_sns_connect btn" data-sns="wonder" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>"><img src="/data/skin/mobile/aileenwedding/img/etc/mo_wonder_s.png" alt="<?php echo __('위메프')?> <?php echo __('위메프 연결')?>"></a></li>
<?php }?>
			</ul>
		</div>
<?php }?>
	</div>
<?php }?>
<?php if($TPL_VAR["privateApprovalOption"][ 0]['modeFl']=='y'){?>
	<div class="privacy_big_box">
		<div class="privacy_box">
			<dl>
				<dt>
					<span class="inp_chk">
						<input type="checkbox" id="termsAgree3" name="">
						<label for="termsAgree3">(<?php echo __('선택')?>) <?php echo __('개인정보 수집 및 이용')?></label>
					</span>
				</dt>
				<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
			</dl>
			<div class="privacy_content">
<?php if($TPL_privateApprovalOption_1){foreach($TPL_VAR["privateApprovalOption"] as $TPL_V1){?>
				<div class="privacy_big_box">
					<div class="privacy_box">
						<dl>
							<dt>
								<span class="inp_chk">
<?php if(isset($TPL_VAR["data"]['privateApprovalOptionFl'])&&is_array($TPL_VAR["data"]['privateOfferFl'])&&key_exists($TPL_V1["sno"],$TPL_VAR["data"]['privateApprovalOptionFl'])){?>
									<input type="checkbox" id="privateApprovalOption_<?php echo $TPL_V1["sno"]?>" name="privateApprovalOptionFl[<?php echo $TPL_V1["sno"]?>]" checked="checked">
									<label for="privateApprovalOption_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }else{?>
									<input type="checkbox" id="privateApprovalOption_<?php echo $TPL_V1["sno"]?>" name="privateApprovalOptionFl[<?php echo $TPL_V1["sno"]?>]">
									<label for="privateApprovalOption_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }?>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
					</div>
				</div>
<?php }}?>
			</div>
		</div>
	</div>
<?php }?>
<?php if($TPL_VAR["privateConsign"][ 0]['modeFl']=='y'){?>
	<div class="privacy_big_box">
		<div class="privacy_box">
			<dl>
				<dt>
					<span class="inp_chk">
						<input type="checkbox" id="termsAgree4" name="">
						<label for="termsAgree4">(<?php echo __('선택')?>) <?php echo __('개인정보 취급위탁')?></label>
					</span>
				</dt>
				<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
			</dl>
			<div class="privacy_content">
<?php if($TPL_privateConsign_1){foreach($TPL_VAR["privateConsign"] as $TPL_V1){?>
				<div class="privacy_big_box">
					<div class="privacy_box">
						<dl>
							<dt>
								<span class="inp_chk">
<?php if(isset($TPL_VAR["data"]['privateConsignFl'])&&is_array($TPL_VAR["data"]['privateOfferFl'])&&key_exists($TPL_V1["sno"],$TPL_VAR["data"]['privateConsignFl'])){?>
									<input type="checkbox"id="privateConsign_<?php echo $TPL_V1["sno"]?>" name="privateConsignFl[<?php echo $TPL_V1["sno"]?>]" checked="checked">
									<label for="privateConsign_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }else{?>
									<input type="checkbox" id="privateConsign_<?php echo $TPL_V1["sno"]?>" name="privateConsignFl[<?php echo $TPL_V1["sno"]?>]">
									<label for="privateConsign_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }?>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
					</div>
				</div>
<?php }}?>
			</div>
		</div>
	</div>
<?php }?>
<?php if($TPL_VAR["privateOffer"][ 0]['modeFl']=='y'){?>
	<div class="privacy_big_box">
		<div class="privacy_box">
			<dl>
				<dt>
					<span class="inp_chk">
						<input type="checkbox" id="termsAgree5" name="">
						<label for="termsAgree5">(<?php echo __('선택')?>) <?php echo __('개인정보 제 3자 제공')?></label>
					</span>
				</dt>
				<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
			</dl>
			<div class="privacy_content">
<?php if($TPL_privateOffer_1){foreach($TPL_VAR["privateOffer"] as $TPL_V1){?>
				<div class="privacy_big_box">
					<div class="privacy_box">
						<dl>
							<dt>
								<span class="inp_chk">
<?php if(isset($TPL_VAR["data"]['privateOfferFl'])&&is_array($TPL_VAR["data"]['privateOfferFl'])&&key_exists($TPL_V1["sno"],$TPL_VAR["data"]['privateOfferFl'])){?>
										<input type="checkbox" id="privateOffer_<?php echo $TPL_V1["sno"]?>" name="privateOfferFl[<?php echo $TPL_V1["sno"]?>]" checked="checked">
										<label for="privateOffer_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }else{?>
										<input type="checkbox" id="privateOffer_<?php echo $TPL_V1["sno"]?>" name="privateOfferFl[<?php echo $TPL_V1["sno"]?>]">
										<label for="privateOffer_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
<?php }?>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
					</div>
				</div>
<?php }}?>
			</div>
		</div>
	</div>
<?php }?>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 * 약관 체크박스 이벤트
			 */
			$('.privacy_box :checkbox').change(function (e) {
				e.preventDefault();
				$('p.msg').addClass('dn');
				var $target = $(e.target), $label = $target.siblings('label'), $termsView = $target.closest('.privacy_box');
				var isTermsAgreeSelect = (e.target.id === 'termsAgree3') || (e.target.id === 'termsAgree4') || (e.target.id === 'termsAgree5');
				var isTargetChecked = $target.prop('checked') === true;

				if (isTargetChecked) {
					if (isTermsAgreeSelect) {
						console.log($termsView.find('.privacy_box label'));
						console.log($termsView.find('.privacy_box :checkbox'));
						$termsView.find('.privacy_box :checkbox').prop('checked', true);
						$termsView.find('.privacy_box :checkbox').val('y');
					} else {
						$target.val('y');
						$label.addClass('on');
					}
				} else {
					if (isTermsAgreeSelect) {
						$termsView.find('.privacy_box :checkbox').prop('checked', false);
						$termsView.find('.privacy_box :checkbox').val('n');
					} else {
						$target.val('n');
						$label.removeClass('on');
					}
				}
			});
			/*
			 * 약관 내용보기 이벤트
			 */
			$('.privacy_box dd button').on({
				'click':function(){
					var privacy_dp = $(this).parent().parent().parent().find('> .privacy_content').css('display');
					if(privacy_dp == 'none'){
						$(this).parent().parent().parent().find('> .privacy_content').slideDown();
						$(this).html("<?php echo __('내용닫기')?> ▲");
					} else {
						$(this).parent().parent().parent().find('> .privacy_content').slideUp();
						$(this).html("<?php echo __('내용보기')?> ▼");
					}
				}
			});
		});
	</script>
<?php }?>
</div>