<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/join.html 000006831 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="join" id="memberjoin">
	<form id="formJoin" name="formJoin" method="post" action="../member/member_ps.php">
		<input type="hidden" name="rncheck" value="<?php echo $TPL_VAR["data"]["rncheck"]?>"/>
		<input type="hidden" name="dupeinfo" value="<?php echo $TPL_VAR["data"]["dupeinfo"]?>"/>
		<input type="hidden" name="pakey" value="<?php echo $TPL_VAR["data"]["pakey"]?>"/>
		<input type="hidden" name="adultFl" value="<?php echo $TPL_VAR["data"]["adultFl"]?>">
		<input type="hidden" name="foreigner" value="<?php echo $TPL_VAR["data"]["foreigner"]?>"/>
		<input type="hidden" name="mode" value="join"/>
		<div class="join_step">
			<ul>
				<li><?php echo __('약관동의')?></li>
				<li class="on"><?php echo __('계정생성')?></li>
				<li><?php echo __('가입완료')?></li>
			</ul>
		</div>
		<div class="join_content_box">
			<div class="join_content" >
				<!-- 회원가입/정보 기본정보 --><?php echo includeWidget('member/_join_view.html')?><!-- 회원가입/정보 기본정보 -->
				<!-- 회원가입/정보 사업자정보 --><?php echo includeWidget('member/_join_view_business.html')?><!-- 회원가입/정보 사업자정보 -->
				<!-- 회원가입/정보 부가정보 --><?php echo includeWidget('member/_join_view_other.html')?><!-- 회원가입/정보 부가정보 -->
			</div>
			<div class="join_btn_box">
				<button class="join_ok_btn" type="button" id="btnConfirm" value="<?php echo __('확인')?>"><?php echo __('확인')?></button>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
    var paycoProfile = <?php echo $TPL_VAR["paycoProfile"]?>;
    var naverProfile = <?php echo $TPL_VAR["naverProfile"]?>;
    var thirdPartyProfile = <?php echo $TPL_VAR["thirdPartyProfile"]?>;
    var kakaoProfile = <?php echo $TPL_VAR["kakaoProfile"]?>;
    $(document).ready(function () {
        var $formJoin = $('#formJoin');


        $(':text:first', $formJoin).focus();

        $('#btnCancel', $formJoin).click(function (e) {
            top.location.href = '/';
            e.preventDefault();
        });

        $('#btnPostcode').click(function (e) {
            e.preventDefault();
            $('#address-error, #addressSub-error').remove();
            $(':text[name=address], :text[name=addressSub]').removeClass('error c_red');
            gd_postcode_search('zonecode', 'address', 'zipcode');
        });

        $('#btnCompanyPostcode').click(function (e) {
            e.preventDefault();
            $('#comAddress-error, #comAddressSub-error').remove();
            $(':text[name=comAddress], :text[name=comAddressSub]').removeClass('error c_red');
            gd_postcode_search('comZonecode', 'comAddress', 'comZipcode');
        });

<?php if($TPL_VAR["joinField"]["businessinfo"]["use"]=='y'){?>
        $(':radio[name="memberFl"]').change(function () {
            var $businessinfo = $('.businessinfo');
            if (this.value == 'business') {
                $businessinfo.removeClass('dn');
                $businessinfo.find('input, select').removeClass('ignore');
            } else {
                $businessinfo.addClass('dn');
                $businessinfo.find('input, select').addClass('ignore');
            }
        });
        $(':radio[name="memberFl"]:checked').trigger('change');
<?php }?>

<?php if(($TPL_VAR["joinField"]["marriFl"]["use"]=='y'&&$TPL_VAR["joinField"]["marriDate"]["use"]=='y')||$TPL_VAR["joinField"]["birthDt"]["use"]=='y'){?>
        function sendDateData(year, month, type) {
            var dayType = '';
            var params = {
                year: year,
                month: month,
            };
            $.ajax('../share/date_select_json.php', {
                data: params,
                method: "POST",
                success: function (data) {
                    if (data !='' && data != null) {
                        if (type == 'marry') {
                            dayType = 'marriDay';
                        }else {
                            dayType = 'birthDay';
                        }
                        $('#'+dayType).empty();
                        $('#'+dayType).append(data);
                    }
                }
            });
        }
<?php }?>

<?php if($TPL_VAR["joinField"]["marriFl"]["use"]=='y'&&$TPL_VAR["joinField"]["marriDate"]["use"]=='y'){?>
        $(':radio[name="marriFl"]').change(function () {
            var $marridateinfo = $('.marridateinfo');
            if (this.value == 'y') {
                $marridateinfo.removeClass('dn');
            } else {
                $marridateinfo.addClass('dn');
                $('#marriYear option:eq(0)').prop('selected', true);
                $('#marriMonth option:eq(0)').prop('selected', true);
                $('#marriDay option:eq(0)').prop('selected', true);
            }
        });

        $('#marriYear').change(function () {
            var marriMonthVal = $('#marriMonth').val();
            if (marriMonthVal != '') {
                sendDateData($('#marriYear').val(), $('#marriMonth').val(), 'marry');
            }
        });
        $('#marriMonth').change(function () {
            var marriYearVal = $('#marriYear').val();
            if (marriYearVal != '') {
                sendDateData($('#marriYear').val(), $('#marriMonth').val(), 'marry');
            }
        });
<?php }?>

<?php if($TPL_VAR["joinField"]["birthDt"]["use"]=='y'){?>
        $('#birthYear').change(function () {
            var birthMonthVal = $('#birthMonth').val();
            if ( birthMonthVal != '') {
                sendDateData($('#birthYear').val(), $('#birthMonth').val(), 'birth');
            }
        });
        $('#birthMonth').change(function () {
            var birthYearVal = $('#birthYear').val();
            if ( birthYearVal != '') {
                sendDateData($('#birthYear').val(), $('#birthMonth').val(), 'birth');
            }
        });
<?php }?>

        gd_select_email_domain('email');

        gd_member2.init($formJoin);

        setTimeout(function(){
            $('#memId').focusout()
            $('#email').focusout();
        },1);

        $('#btnConfirm').click({form: $formJoin}, gd_member2.save);

        $('#birthYears').initSelectDateFormat();
        $('#marriYears').initSelectDateFormat();
    });


    /**
     * 회원가입시 Exception 발생하는 경우 회원가입 버튼 복원
     */
    function callback_not_disabled()
    {
        $('.join_ok_btn').prop("disabled", false);
        $('.join_ok_btn em').html("<?php echo __('회원가입')?>");
    }
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>