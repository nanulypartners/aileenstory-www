<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/join_agreement.html 000018013 */ 
if (is_array($TPL_VAR["privateApprovalOption"])) $TPL_privateApprovalOption_1=count($TPL_VAR["privateApprovalOption"]); else if (is_object($TPL_VAR["privateApprovalOption"]) && in_array("Countable", class_implements($TPL_VAR["privateApprovalOption"]))) $TPL_privateApprovalOption_1=$TPL_VAR["privateApprovalOption"]->count();else $TPL_privateApprovalOption_1=0;
if (is_array($TPL_VAR["privateConsign"])) $TPL_privateConsign_1=count($TPL_VAR["privateConsign"]); else if (is_object($TPL_VAR["privateConsign"]) && in_array("Countable", class_implements($TPL_VAR["privateConsign"]))) $TPL_privateConsign_1=$TPL_VAR["privateConsign"]->count();else $TPL_privateConsign_1=0;
if (is_array($TPL_VAR["privateOffer"])) $TPL_privateOffer_1=count($TPL_VAR["privateOffer"]); else if (is_object($TPL_VAR["privateOffer"]) && in_array("Countable", class_implements($TPL_VAR["privateOffer"]))) $TPL_privateOffer_1=$TPL_VAR["privateOffer"]->count();else $TPL_privateOffer_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="join_agreement">
	<div class="join_step">
		<ul>
			<li class="on"><?php echo __('약관동의')?></li>
			<li><?php echo __('계정생성')?></li>
			<li><?php echo __('가입완료')?></li>
		</ul>
	</div>
	<form id="formTerms" name="formTerms" method="post" action="../member/join.php">
		<input type="hidden" name="token" value="<?php echo $TPL_VAR["token"]?>">
		<input type="hidden" name="mode" value="chkRealName">
		<input type="hidden" name="rncheck" value="none">
		<input type="hidden" name="nice_nm" value="">
		<input type="hidden" name="pakey" value="">
		<input type="hidden" name="birthday" value="">
		<input type="hidden" name="mobile" value="">
		<input type="hidden" name="mobileService" value="">
		<input type="hidden" name="sex" value="">
		<input type="hidden" name="dupeinfo" value="">
		<input type="hidden" name="foreigner" value="">
		<input type="hidden" name="adultFl" value="">
		<input type="hidden" name="phone" value="">
		<input type="hidden" name="type">
<?php if($TPL_VAR["memberJoinConfig"]["under14ConsentFl"]=='y'){?>
		<div class="all_chk" id="termsAgreeDiv">
			<span class="inp_chk">
				<input type="checkbox" id="termsAgree" name="under14ConsentFl">
				<label for="termsAgree"> (필수)  만 14세 이상입니다.</label>
			</span>
		</div>
<?php }?>
		<div class="all_chk">
			<span class="inp_chk">
				<input type="checkbox" id="allAgree">
				<label for="allAgree"> <?php echo __('%s의 모든 약관을 확인하고 전체 동의합니다.',$TPL_VAR["serviceInfo"]['mallNm'])?></label>
			</span>
		</div>
		<div class="privacy_big_box">
			<div class="privacy_box">
				<dl>
					<dt>
						<span class="inp_chk">
							<input type="checkbox" class="require" id="termsAgree1" name="agreementInfoFl">
							<label for="termsAgree1"> (<?php echo __('필수')?>) <?php echo __('이용약관')?></label>
						</span>
					</dt>
					<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
				</dl>
				<div class="privacy_content">
					<div> <?php echo nl2br($TPL_VAR["agreementInfo"]['content'])?></div>
				</div>
				<!-- //privacy_content -->
			</div>
			<!-- //privacy_box -->
			<div class="privacy_box">
				<dl>
					<dt>
						<span class="inp_chk">
							<input type="checkbox" class="require" id="termsAgree2" name="privateApprovalFl">
							<label for="termsAgree2"> (<?php echo __('필수')?>) <?php echo __('개인정보 수집 및 이용')?></label>
						</span>
					</dt>
					<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
				</dl>
				<div class="privacy_content">
					<div> <?php echo nl2br($TPL_VAR["privateApproval"]['content'])?></div>
				</div>
				<!-- //privacy_content -->
			</div>
			<!-- //privacy_box -->
<?php if($TPL_VAR["privateApprovalOption"][ 0]['modeFl']=='y'){?>
			<div class="privacy_box">
				<dl>
					<dt>
						<span class="inp_chk">
							<input type="checkbox" id="termsAgree3">
							<label for="termsAgree3">(<?php echo __('선택')?>) <?php echo __('개인정보 수집 및 이용')?></label>
						</span>
					</dt>
					<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
				</dl>
				<div class="privacy_content">
<?php if($TPL_privateApprovalOption_1){foreach($TPL_VAR["privateApprovalOption"] as $TPL_V1){?>
					<div class="privacy_big_box">
						<dl>
							<dt>
								<span class="inp_chk">
									<input type="checkbox" id="privateApprovalOption_<?php echo $TPL_V1["sno"]?>" name="privateApprovalOptionFl[<?php echo $TPL_V1["sno"]?>]">
									<label for="privateApprovalOption_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
						<!-- //privacy_content -->
					</div>
					<!-- //privacy_big_box -->
<?php }}?>
				</div>
				<!-- //privacy_content -->
			</div>
			<!-- //privacy_box -->
<?php }?>
<?php if($TPL_VAR["privateConsign"][ 0]['modeFl']=='y'){?>
			<div class="privacy_box">
				<dl>
					<dt>
						<span class="inp_chk">
							<input type="checkbox" id="termsAgree4" name="privateConsignFl" value="n">
							<label for="termsAgree4">(<?php echo __('선택')?>) <?php echo __('개인정보 처리위탁')?></label>
						</span>
					</dt>
					<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
				</dl>
				<div class="privacy_content">
<?php if($TPL_privateConsign_1){foreach($TPL_VAR["privateConsign"] as $TPL_V1){?>
					<div class="privacy_big_box">
						<dl>
							<dt>
								<span class="inp_chk">
									<input type="checkbox" id="privateConsign_<?php echo $TPL_V1["sno"]?>" name="privateConsignFl[<?php echo $TPL_V1["sno"]?>]">
									<label for="privateConsign_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
						<!-- //privacy_content -->
					</div>
<?php }}?>
				</div>
				<!-- //privacy_content -->
			</div>
			<!-- //privacy_box -->
<?php }?>
<?php if($TPL_VAR["privateOffer"][ 0]['modeFl']=='y'){?>
			<div class="privacy_box">
				<dl>
					<dt>
						<span class="inp_chk">
							 <input type="checkbox" id="termsAgree5" name="privateOfferFl">
							<label for="termsAgree5">(<?php echo __('선택')?>) <?php echo __('개인정보 제 3자 제공')?></label>
						</span>
					</dt>
					<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
				</dl>
				<div class="privacy_content">
<?php if($TPL_privateOffer_1){foreach($TPL_VAR["privateOffer"] as $TPL_V1){?>
					<div class="privacy_big_box">
						<dl>
							<dt>
								<span class="inp_chk">
									<input type="checkbox" class="sp" id="privateOffer_<?php echo $TPL_V1["sno"]?>" name="privateOfferFl[<?php echo $TPL_V1["sno"]?>]">
									<label for="privateOffer_<?php echo $TPL_V1["sno"]?>"><?php echo nl2br($TPL_V1["informNm"])?></label>
								</span>
							</dt>
							<dd><button type="button"><?php echo __('내용보기')?> ▼</button></dd>
						</dl>
						<div class="privacy_content">
							<div><?php echo nl2br($TPL_V1["content"])?></div>
						</div>
						<!-- //privacy_content -->
					</div>
					<!-- //privacy_big_box -->
<?php }}?>
				</div>
				<!-- //privacy_content -->
			</div>
<?php }?>
			<p class="msg dn">
				<span><?php echo __('이용약관과 개인정보 수집 및 이용에 대한 안내 모두 동의해주세요.')?></span>
			</p>
        </div>
		<!-- //privacy_big_box -->
<?php if($TPL_VAR["ipinConfig"]["useFl"]=='y'||$TPL_VAR["authCellPhoneConfig"]["useFl"]=='y'){?>
		<div class="member_confirm">
			<fieldset>
				<h2><?php echo __('인증수단 선택')?></h2>
				<legend><?php echo __('회원가입 본인인증 폼')?></legend>
				<div class="certify">
					<ul>
<?php if($TPL_VAR["ipinConfig"]["useFl"]=='y'){?>
					<li>
						<span class="inp_rdo">
							<input type="radio" name="RnCheckType" id="authIpin" value="ipin">
							<label class="choice" for="authIpin"><?php echo __('아이핀 본인인증')?></label>
						</span>
					</li>
<?php }?>
<?php if($TPL_VAR["authCellPhoneConfig"]["useFl"]=='y'){?>
					<li>
						<span class="inp_rdo">
							<input type="radio" name="RnCheckType" id="authCellphone" value="authCellPhone">
							<label class="choice" for="authCellphone"><?php echo __('휴대폰 본인인증')?></label>
						</span>
					</li>
<?php }?>
					</ul>
					<p class="dn" id="errorMessage"></p>
				</div>
			</fieldset>
		</div>
<?php }?>
		<div class="member_confirm_btn">
			<ul>
<?php if($TPL_VAR["useThirdParty"]){?>
				<li><button class="join_prev_btn" id="btnPrevStep" type="button"><?php echo __('이전')?></button></li>
<?php }?>
				<li><button class="join_next_btn" id="btnNextStep" type="button"><?php echo __('다음')?></button></li>
			</ul>
		</div>
		<!-- //member_confirm_btn -->
    </form>
</div>
<!-- //join_agreement -->
<script type="text/javascript">
    $(document).ready(function () {
        var body = $('body'), $formTerms = $('#formTerms');

        $('#btnNextStep').click(function () {
            var pass = true;
            /*
             * 필수 동의 항목 검증
             */
            $(':checkbox.require').each(function (idx, item) {
                var $item = $(item);
                if (!$item.prop('checked')) {
                    pass = false;
                    var text = $item.next().text();
                    var index = text.indexOf(')') + 1;
                    var require = '<span>' + text.substring(0, index) + '</span>';
                    var info = text.substring(index);
                    $('p.msg', $formTerms).removeClass('dn').html(__('%s 을 체크해주세요.', require + info));
                    _.delay(function () {
                        $item.focus();
                    }, 1000);
                    return false;
                }
            });

            /*
             * 만 14세 이상 동의 항목 검증
             */
            if ($("#termsAgreeDiv").length > 0) {
                if (!$('#termsAgree').prop('checked')) {
                    pass = false;
                    alert("<?php echo __('만 14세 이상임에 동의해 주세요.')?>");
                    $("#termsAgreeDiv").attr("tabindex", -1).focus();
                    return false;
                }
            }

            if (pass) {
                $('p.msg').addClass('dn');

                /*
                 * 실명인증 검증
                 */
                if ($('input[name="RnCheckType"]').length > 0) {
                    switch ($('input[name="RnCheckType"]:checked').val()) {
                        case 'ipin' :
							window.open("/member/ipin/ipin_main.php?callType=joinmember");
                            break;
                        case  'authCellPhone' :
                            var protocol = location.protocol;
                            var callbackUrl = "<?php echo $TPL_VAR["domainUrl"]?>/member/authcellphone/dreamsecurity_result.php";
                            var userAgent = window.navigator.userAgent;
                            //아이폰+페이스북앱으로 본인인증 진행
                            if(userAgent.indexOf("iPhone") >-1 && (userAgent.indexOf('FBAN') > -1 || userAgent.indexOf('FBAV') > -1)) {
                                checkAgreementFl();
                                location.href = protocol + "//hpauthdream.godo.co.kr/module/Mobile_hpauthDream_Main.php?shopUrl=" + callbackUrl +"&callType=joinmembermobile&cpid=<?php echo $TPL_VAR["authDataCpCode"]?>";
                            }else{
								window.open(protocol + "//hpauthdream.godo.co.kr/module/Mobile_hpauthDream_Main.php?callType=joinmembermobile&shopUrl=" + callbackUrl + "&cpid=<?php echo $TPL_VAR["authDataCpCode"]?>");
                            }
                            break;
                        default :
                            alert("<?php echo __('본인인증이 필요합니다.')?>");
                            $('input[name="RnCheckType"]:first').focus();
                            break;
                    }
                    return false;
                } else {
                    $formTerms.submit();
                }
            }
        });

        /*
         * 전체 동의 체크박스 이벤트
         */
        $('#allAgree', $formTerms).change(function (e) {
            e.preventDefault();
            var $target = $(e.target), $checkbox = $(':checkbox').not('#termsAgree'), $label = $checkbox.siblings('label');
            if ($target.prop('checked') === true) {
                $checkbox.prop('checked', true).val('y');
                $label.addClass('on');
            } else {
                $checkbox.prop('checked', false).val('n');
                $label.removeClass('on');
            }
        });

        /*
         * 이전단계 버튼 이벤트
         */
        $('#btnPrevStep', $formTerms).click(function (e) {
            e.preventDefault();
            window.location.href = '../member/join_method.php'
        });

        /*
         * 약관 체크박스 이벤트
         */
        $('.privacy_box :checkbox').change(function (e) {
            e.preventDefault();
            $('p.msg').addClass('dn');
            var $target = $(e.target), $label = $target.siblings('label'), $termsView = $target.closest('.privacy_box');
            var isTermsAgreeSelect = (e.target.id === 'termsAgree3') || (e.target.id === 'termsAgree4') || (e.target.id === 'termsAgree5');
            var isTargetChecked = $target.prop('checked') === true;

            if (isTargetChecked) {
                if (isTermsAgreeSelect) {
                    $termsView.find('.privacy_content :checkbox').prop('checked', true);
                    $termsView.find('.privacy_content :checkbox').val('y');
                } else {
                    $target.val('y');
                    $label.addClass('on');
                }
            } else {
                if (isTermsAgreeSelect) {
                    $termsView.find('.privacy_content :checkbox').prop('checked', false);
                    $termsView.find('.privacy_content :checkbox').val('n');
                } else {
                    $target.val('n');
                    $label.removeClass('on');
                }
            }
        });

        /*
        * 14세 동의 항목 체크박스 이벤트
        */
        if ($("#termsAgreeDiv").length > 0) {
            $('#termsAgreeDiv :checkbox', $formTerms).change(function (e) {
                var $termsTarget = $(e.target), $termsLabel = $termsTarget.siblings('label');
                var isTermsTargetChecked = $termsTarget.prop('checked') === true;
                if (isTermsTargetChecked) {
                    $termsTarget.val('y');
                    $termsLabel.addClass('on');
                } else {
                    $termsTarget.val('n');
                    $termsLabel.removeClass('on');
                }
            });
        }

		 /*
         * 약관 내용보기 이벤트
         */
		$('.privacy_box dd button').on({
			'click':function(){
				var privacy_dp = $(this).parent().parent().parent().find('> .privacy_content').css('display');
				if(privacy_dp == 'none'){
					$(this).parent().parent().parent().find('> .privacy_content').slideDown();
					$(this).html("<?php echo __('내용닫기')?> ▲");
				} else {
					$(this).parent().parent().parent().find('> .privacy_content').slideUp();
					$(this).html("<?php echo __('내용보기')?> ▼");
				}
			}
		});

        window.addEventListener('message', function (e) {
            if (e.origin === 'http://hpauthdream.godo.co.kr') {
                $('#layerSearch').remove();
            }
        });
    });

    /**
     * 아이폰+페이스북앱에서 location.href로 페이지 이동으로 처리시
     * 약관데이터 전달위해 세션에 저장
     */
    function checkAgreementFl(){
        //필수동의
        var agreementInfoFl = $('#termsAgree1').val();
        var privateApprovalFl = $('#termsAgree2').val();

        //개인정보 수집 및 이용(선택)
        var privateApprovalOption = 'privateApprovalOptionFl';
<?php if($TPL_privateApprovalOption_1){foreach($TPL_VAR["privateApprovalOption"] as $TPL_V1){?>
        privateApprovalOption += "," + <?php echo $TPL_V1["sno"]?>+","+ $('input[name=\'privateApprovalOptionFl[<?php echo $TPL_V1["sno"]?>]\']').val();
<?php }}?>

        //개인정보 처리 위탁(선택)
        var privateConsign = 'privateConsignFl';
<?php if($TPL_privateConsign_1){foreach($TPL_VAR["privateConsign"] as $TPL_V1){?>
        privateConsign += "," + <?php echo $TPL_V1["sno"]?>+","+ $('input[name=\'privateConsignFl[<?php echo $TPL_V1["sno"]?>]\']').val();
<?php }}?>

        //개인정보 3자 제공(선택)
        var privateOffer = 'privateOfferFl';
<?php if($TPL_privateOffer_1){foreach($TPL_VAR["privateOffer"] as $TPL_V1){?>
        privateOffer += "," + <?php echo $TPL_V1["sno"]?>+","+ $('input[name=\'privateOfferFl[<?php echo $TPL_V1["sno"]?>]\']').val();
<?php }}?>

        localStorage.setItem('agreementInfoFl', agreementInfoFl);
        localStorage.setItem('privateApprovalFl', privateApprovalFl);
        localStorage.setItem('privateApprovalOption', privateApprovalOption);
        localStorage.setItem('privateConsign', privateConsign);
        localStorage.setItem('privateOffer', privateOffer);
    }
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>