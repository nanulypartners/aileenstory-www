<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/join_ok.html 000000986 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="join_ok">
	<div class="join_step">
		<ul>
			<li><?php echo __('약관동의')?></li>
			<li><?php echo __('계정생성')?></li>
			<li class="on"><?php echo __('가입완료')?></li>
		</ul>
	</div>
	<div class="join_content">
		<p class="tx"><?php echo __('%s님%s 가입을 축하드립니다!',$TPL_VAR["memNm"],'<br/>')?></p>
	</div>
	<div class="btn"><a href="../member/login.php" class="join_login_btn"><?php echo __('로그인 하기')?></a></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnHome').click(function (e) {
            e.preventDefault();
            top.location.href = '../main/index.php';
        });
    });
</script>
<?php echo $TPL_VAR["fbCompleteRegistrationScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>