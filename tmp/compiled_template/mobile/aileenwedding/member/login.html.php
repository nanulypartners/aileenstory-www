<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/member/login.html 000009072 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="login">
	<div class="login_box">
		<form id="formLogin" name="login_form" action="<?php echo $TPL_VAR["loginActionUrl"]?>" method="post">
			<fieldset>
				<legend><?php echo __('로그인 영역')?></legend>
				<input type="hidden" name="returnUrl" value="<?php echo $TPL_VAR["returnUrl"]?>"/>
				<input type="hidden" name="close" value=""/>
				<input type="hidden" name="saveFlag" value="<?php echo $TPL_VAR["data"]["saveFlag"]?>"/>
				<input type="hidden" id="mode" name="mode" value="<?php echo $TPL_VAR["modeLogin"]?>"/>
				<dl>
					<dt><label for="loginId"><?php echo __('아이디')?></label></dt>
					<dd><input type="text" id="loginId" name="loginId" value="<?php echo $TPL_VAR["data"]["loginId"]?>" placeholder="<?php echo __('아이디')?>"></dd>
				</dl>
				<dl>
					<dt><label for="loginPwd"><?php echo __('비밀번호')?></label></dt>
					<dd><input type="password" placeholder="<?php echo __('비밀번호')?>" id="loginPwd" name="loginPwd" value="<?php echo $TPL_VAR["data"]["loginPwd"]?>"></dd>
				</dl>
				<div class="inp_chk">
					<input type="checkbox" name="saveAutoLogin" value="y" id="saveAutoLogin" <?php if(!empty($TPL_VAR["data"]["loginId"])&&!empty($TPL_VAR["data"]["loginPwd"])){?>checked="checked"<?php }?> />
					<label for="saveAutoLogin"><?php echo __('로그인 상태 유지')?></label>
					<input type="checkbox" id="saveId" name="saveId" value="y" <?php if(!empty($TPL_VAR["data"]["loginId"])){?>checked="checked"<?php }?>/>
					<label for="saveId"><?php echo __('아이디 저장')?></label>
				</div>
				<div class="submit">
					<button type="submit" class="member_login_order_btn"><?php echo __('로그인')?></button>
				</div>
				<!-- //submit -->
			</fieldset>
		</form>
<?php if($TPL_VAR["usePaycoLogin"]||$TPL_VAR["useFacebookLogin"]||$TPL_VAR["useNaverLogin"]||$TPL_VAR["useKakaoLogin"]||$TPL_VAR["useWonderLogin"]){?>
		<ul class="sns_login">
<?php if($TPL_VAR["usePaycoLogin"]){?>
			<li class="payco js_btn_payco_login"><img src="/data/skin/mobile/aileenwedding/img/etc/txt_mo_payco.png" alt="<?php echo __('PAYCO')?> <?php echo __('아이디 로그인')?>"></li>
<?php }?>
<?php if($TPL_VAR["useFacebookLogin"]){?>
			<li class="facebook js_btn_facebook_login" data-facebook-url="<?php echo $TPL_VAR["facebookUrl"]?>"><img src="/data/skin/mobile/aileenwedding/img/etc/txt_mo_facebook.png" alt="<?php echo __('FACEBOOK')?> <?php echo __('아이디 로그인')?>"></li>
<?php }?>
<?php if($TPL_VAR["useNaverLogin"]){?>
			<li class="naver js_btn_naver_login"><img src="/data/skin/mobile/aileenwedding/img/etc/txt_mo_naver.png" alt="<?php echo __('네이버')?> <?php echo __('아이디 로그인')?>" /></li>
<?php }?>
<?php if($TPL_VAR["useKakaoLogin"]){?>
			<li class="kakao js_btn_kakao_login" data-return-url="<?php echo $TPL_VAR["kakaoReturnUrl"]?>"><img src="/data/skin/mobile/aileenwedding/img/etc/txt_mo_kakao.png" alt="<?php echo __('카카오')?>  <?php echo __('아이디 로그인')?>"></li>
<?php }?>
<?php if($TPL_VAR["useWonderLogin"]){?>
			<li class="wonder js_btn_wonder_login" data-wonder-type="login" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>"><img src="/data/skin/mobile/aileenwedding/img/etc/txt_mo_wonder.png" alt="<?php echo __('위메프')?>  <?php echo __('아이디로 로그인')?>"></li>
<?php }?>
		</ul>
		<!-- //sns_login -->
<?php }?>
		<ul class="login_find">
			<li><a href="../member/join_method.php"><?php echo __('회원가입')?></a></li>
			<li><a href="../member/find_id.php"><?php echo __('아이디찾기')?></a></li>
			<li><a href="../member/find_password.php"><?php echo __('비밀번호찾기')?></a></li>
		</ul>
		<!-- //login_find -->
	</div>
	<!-- //login_box -->
	<div class="guest_order_box">
<?php if($TPL_VAR["isMemberOrder"]==false){?>
		<p class="guest_txt">
			<strong><?php echo __('비회원 주문')?></strong>
		</p>
		<!--  //guest_txt -->
		<form action="../member/member_ps.php" method="post">
			<fieldset>
				<legend><?php echo __('비회원 주문하기')?></legend>
				<input type="hidden" name="mode" value="guest">
				<input type="hidden" name="returnUrl" value="<?php echo $TPL_VAR["returnUrl"]?>">
				<button type="submit" class="member_login_order_btn"> <?php echo __('비회원 주문하기')?></button>
			</fieldset>
		</form>
<?php }?>
<?php if($TPL_VAR["hasGuestOrder"]==false){?>
		<p class="guest_txt">
			<strong><?php echo __('비회원 주문조회')?></strong>
			<span><?php echo __('주문자 이름과 주문 번호를 입력해주세요')?></span>
		</p>
		<!-- //guest_txt -->
		<form id="formOrderLogin" name="login_form" action="../member/member_ps.php" method="post">
			<fieldset>
				<legend><?php echo __('주문조회 영역')?></legend>
				<input type="hidden" name="mode" value="guestOrder">
				<input type="hidden" name="returnUrl" value="../mypage/order_view.php">
				<dl>
					<dt><label for="orderNm"><?php echo __('주문자 이름')?></label></dt>
					<dd><input type="text" id="orderNm" name="orderNm" value="" maxlength="12" title="<?php echo __('주문자명')?>" required="required" msgr="<?php echo __('주문자명을 입력하세요.')?>" placeholder="<?php echo __('주문자명')?>"></dd>
				</dl>
				<dl>
					<dt><label for="orderNo"><?php echo __('주문번호')?></label></dt>
					<dd><input type="text" id="orderNo" name="orderNo" maxlength="34" title="<?php echo __('주문번호')?>" required="required" msgr="<?php echo __('주문번호를 입력하세요.')?>" placeholder="<?php echo __('주문번호')?>" data-max-length="<?php echo $TPL_VAR["orderNoMaxLength"]?>"></dd>
				</dl>
				<button type="submit" class="member_login_order_btn"><?php echo __('주문 조회')?></button>
			</fieldset>
		</form>
<?php }?>
	</div>
	<!-- //guest_order_box -->
</div>
<script type="text/javascript" src="/data/skin/mobile/aileenwedding/js/jquery/jquery.serialize.object.js"></script>
<script type="text/javascript">
    var $formLogin;
    $(document).ready(function () {
        var order_no_max_length = $('input[name=orderNo]').data('max-length');
        $formLogin = $('#formLogin');
        $formLogin.validate({
            dialog: false,
            rules: {
                loginId: {
                    required: true
                },
                loginPwd: {
                    required: true
                }
            },
            messages: {
                loginId: {
                    required: "<?php echo __('아이디를 입력해주세요')?>"
                },
                loginPwd: {
                    required: "<?php echo __('패스워드를 입력해주세요')?>"
                }
            }, submitHandler: function (form) {
                if (window.location.search) {
                    var _tempUrl = window.location.search.substring(1);
                    var _tempVal = _tempUrl.split('=');

                    if (_tempVal[1] == 'lnCouponDown') {
                        $('input[name=returnUrl]').val(document.referrer);
                    }
                }
                form.target = 'ifrmProcess';
                form.submit();
            }
        });

        // 비회원 주문조회 폼 체크
        $('#formOrderLogin').validate({
            rules: {
                orderNm: 'required',
                orderNo: {
                    required: true,
                    number: true,
                    maxlength: order_no_max_length
                }
            },
            messages: {
                orderNm: {
                    required: "<?php echo __('주문자명을 입력해주세요.')?>",
                },
                orderNo: {
                    required: "<?php echo __('주문번호를 입력해주세요.')?>",
                    number: "<?php echo __('숫자로만 입력해주세요.')?>",
                    maxlength: "<?php echo __('주문번호는 " + order_no_max_length + "자리입니다.')?>"
                }
            },
            submitHandler: function (form) {
                $.post(form.action, $(form).serializeObject()).done(function (data, textStatus, jqXhr) {
                    console.log(data);
                    if (data.result == 0) {
                        location.replace('../mypage/order_view.php?guest=y&orderNo=' + data.orderNo);
                    } else {
                        alert(data.message);
                        //$('.js-caution').empty().removeClass('caution-msg2').addClass('caution-msg1').html('주문자명과 주문번호가 일치하는 주문이 존재하지 않습니다. 다시 입력해 주세요.<br><span>주문번호와 비밀번호를 잊으신 경우, 고객센터로 문의하여 주시기 바랍니다.</span>');
                    }
                });
                return false;
            }
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>