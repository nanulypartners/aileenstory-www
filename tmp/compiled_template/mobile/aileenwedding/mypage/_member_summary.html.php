<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/mypage/_member_summary.html 000002082 */ ?>
<?php if($TPL_VAR["checkLogin"]!='guest'){?>
<div class="member_summary">
	<!-- 등급 혜택 -->
	<div class="member_name">
		<span><?php echo $TPL_VAR["gMember"]["memNm"]?><?php echo __('님, 즐거운쇼핑 되세요!')?></span>
		<span class="member_modify"><a href="my_page_password.php" class="member_modify_btn"><?php echo __('회원정보 변경')?></a></span>
	</div>
	<div class="member_grade">
		<div class="grade_text <?php if($TPL_VAR["gMemberInfo"]["groupImageGb"]=='none'){?> grade_text1 <?php }?>">
<?php if($TPL_VAR["gMemberInfo"]["groupImageGb"]!='none'){?>
			<span class="grade_img">
				<!-- 이미지 교체로 등급 이미지 변경 width, height 75px로 고정.-->
				<img src="<?php echo gd_get_group_image_http_path($TPL_VAR["gMember"]["groupSno"])?>" alt="회원등급아이콘">
			</span>
<?php }?>
			<span>
				<?php echo __('%s님의 회원등급은 %s입니다.',$TPL_VAR["gMember"]["memNm"],'<br><strong>'.$TPL_VAR["gMemberInfo"]["groupNmWithLabel"].'</strong>')?>

			</span>
		</div>
	</div>
	<!-- 정보 요약 -->
<?php if(gd_use_coupon()||gd_use_deposit()||gd_use_deposit()){?>
	<div class="mypage_number_summary">
	<ul>
<?php if(gd_use_coupon()){?>
		<li><a href="../mypage/coupon.php"><?php echo __('보유쿠폰')?><span><strong><?php echo gd_money_format($TPL_VAR["myCouponCount"])?></strong> 장</span></a></li>
<?php }?>
<?php if(gd_use_deposit()){?>
		<li><a href="../mypage/deposit.php"><?php echo gd_display_deposit('name')?><span><strong><?php echo gd_money_format($TPL_VAR["gMemberInfo"]["deposit"])?></strong> <?php echo gd_display_deposit('unit')?></span></a></li>
<?php }?>
<?php if(gd_use_deposit()){?>
		<li><a href="../mypage/mileage.php"><?php echo gd_display_mileage_name()?></p><span><strong><?php echo gd_money_format($TPL_VAR["gMemberInfo"]["mileage"])?></strong> <?php echo gd_display_mileage_unit()?></span></a></li>
<?php }?>
	</ul>
	</div>
<?php }?>
</div>
<?php }?>