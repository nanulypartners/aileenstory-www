<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/mypage/order_list.html 000002037 */  $this->include_("includeFile");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="order_list">
	<div class="form_box">
		<form method="get" name="frmDateSearch" id="frmDateSearch" autocomplete="off">
		<input type="hidden" name="mode" value="<?php echo $TPL_VAR["mode"]?>">
		<fieldset>
			<legend><?php echo __('주문목록/배송조회 날짜 검색')?></legend>
			<div class="sel_box">
				<div class="inp_sel"><?php echo gd_select_box(null,'searchPeriod',$TPL_VAR["searchDate"],null,$TPL_VAR["selectDate"],null,null,'check_option_inner')?></div>
				<div id="layer" class="layer"></div>
			</div>
		</fieldset>
		</form>
	</div>
	<div id="mypageOrderlist">
		<!-- 주문상품 리스트 -->
		<?php echo includeFile('mypage/_order_goods_list.html')?>

		<!--// 주문상품 리스트 -->
		<div id="orderlist_more"></div>
	</div>
	<div class="btn more_btn_box">
		<button type="button" class="more_btn" data-page="1" data-mode="<?php echo $TPL_VAR["mode"]?>"><?php echo __('더보기')?></button>
	</div>
</div>
<script type="text/javascript">
	<!--
	$(document).ready(function(){
		$('.more_btn_box button').click(function(){
			var page = parseInt($(this).data('page')) + 1;
			var mode = $(this).data("mode");
			var searchPeriod = parseInt($('.check_option_inner').val());
			$.get('./order_list.php', {'listMode' : 'data', 'page' : page, 'mode' : mode, 'searchPeriod' : searchPeriod}, function (data) {
				if ($(data).find("div.my_goods.no").length) {
					alert("<?php echo __('더이상 주문내역이 없습니다.')?>");
				} else {
					$('#orderlist_more').append(data);
					$('.more_btn_box button').data('page', page);
				}
			});
		})
		// 검색기간 선택
		if ($('.check_option_inner').length) {
			$('.check_option_inner').change(function (e) {
				$('#frmDateSearch').submit();
			});
		}
	});
	//-->
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>