<?php /* Template_ 2.2.7 2020/05/08 01:21:18 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/order/_simple_join.html 000014135 */  $this->include_("setBrowserCache");
if (is_array($TPL_VAR["emailDomain"])) $TPL_emailDomain_1=count($TPL_VAR["emailDomain"]); else if (is_object($TPL_VAR["emailDomain"]) && in_array("Countable", class_implements($TPL_VAR["emailDomain"]))) $TPL_emailDomain_1=$TPL_VAR["emailDomain"]->count();else $TPL_emailDomain_1=0;?>
<input type="hidden" name="simpleJoin" value="<?php echo $TPL_VAR["simpleJoin"]?>" />

<?php if(gd_is_plus_shop(PLUSSHOP_CODE_SIMPLEJOIN)===true&&gd_is_login()===false&&$TPL_VAR["joinEventOrder"]["useFl"]=='y'&&$TPL_VAR["joinEventOrder"]["deviceType"]!='pc'){?>
<div class="simple_join">
<?php if($TPL_VAR["joinEventOrder"]["bannerFl"]=='y'){?>
    <div class="sj_banner_box">
<?php if($TPL_VAR["joinEventOrder"]["bannerImageType"]=='basic'){?>
        <img src="/data/skin/mobile/aileenwedding/img/etc/img_sj_default.png" alt="" title="">
<?php }else{?>
        <img src="<?php echo $TPL_VAR["joinEventOrder"]["joinEventOrderImage"]?>" alt="" title="">
<?php }?>
    </div>
<?php }?>
    <!-- //sj_banner_box -->
    <div class="sj_join_box">
        <div class="tit_area">
            <p class="tit">
                <img src="/data/skin/mobile/aileenwedding/img/etc/icon_event.png" alt="event" class="icon_event"><?php echo $TPL_VAR["joinEventOrder"]["title"]?>

            </p>
            <span class="btn_toggle"><img src="/data/skin/mobile/aileenwedding/img/etc/icon_arrow.png" alt=""></span>
        </div>
        <div class="input_box_wrap">
            <div class="input_area">
                <div class="cell_bx">
                    <div class="cell email_input">
                        <input type="text" class="txt-field hs" name="joinEmail" placeholder="<?php echo __('이메일')?>"/>
                    </div>
                    <div class="cell">
                        <div class="inp_sel">
                            <select id="joinEmailDomain">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
                                <option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
                            </select>
                        </div>
                    </div>
                </div>
                <p class="min_txt joinEmail">이메일을 정확하게 입력해주세요.</p>
            </div>
            <div class="input_area">
                <div class="cell_bx">
                    <div class="cell">
                        <div class="inp_sel_r">
                            <input type="password" class="txt-field hs" name="joinPw" placeholder="<?php echo __('비밀번호')?>"/>
                            <p class="min_txt joinPw">최소 10이상 입력해 주세요.</p>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="inp_sel_l">
                            <input type="password" class="txt-field hs" name="joinPwCheck" placeholder="<?php echo __('비밀번호 확인')?>"/>
                            <p class="min_txt imp joinPwCheck">비밀번호가 서로 다릅니다.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inp_chk">
                <input type="checkbox" name="termAgreeGuest" id="chk_sj_agree">
                <label for="chk_sj_agree">
                    <strong>주문자정보를 회원정보 사용에 동의합니다.</strong><br>
                    본인은 만 14세 이상이며, <a href="#" class="btn_layer_agree">[<?php echo $TPL_VAR["serviceInfo"]["mallNm"]?>] 이용약관</a>, <a href="#" class="btn_layer_privacy">개인정보 수집 및 이용</a> 내용을 확인하였으며 동의합니다.
                </label>
            </div>
            <a href="#;" class="btn_sj_submit">간단가입</a>
        </div>
    </div>
    <!-- //sj_join_box -->
</div>
<!-- //simple_join -->

<!-- ly_pop -->
<div class="ly_dim"></div>
<div class="simple_join_layer">
    <div class="ly_simple_join">
        <div class="ly_tit">
            <h4>간단가입 완료</h4>
        </div>
        <div class="ly_cont">
            <div class="scroll_box">
                <strong>간단가입이 완료 되었습니다.</strong>
                <p>아이디: <span class="id"></span></p>
            </div>
            <!-- N:type2일때 표시 -->
            <div class="benefit_box">
                <strong>가입 혜택이 지급되었습니다.</strong>
                <p class="mileage display-none"><?php echo $TPL_VAR["mileage"]["name"]?>: <span></span><?php echo $TPL_VAR["mileage"]["unit"]?></p>
                <p class="coupon display-none"></p>
            </div>
            <!-- //N:type2일때 표시 -->
            <div class="btn_center_box">
                <button type="button" class="btn_ly_ok ly_coupon_apply_btn"><strong>확인</strong></button>
            </div>
        </div>
        <!-- //ly_cont -->
        <a href="javascript:void(0);" class="ly_close">닫기</a>
    </div>
    <!-- //layer_wrap_cont -->
</div>
<!-- //ly_pop -->

<div class="layer_popup ly_ag layer_agree">
    <div class="title"><?php echo __('이용약관')?></div>
    <div class="box_text">
        <?php echo nl2br($TPL_VAR["agreementInfo"]['content'])?>

    </div>
    <div class="btn_wrap">
        <a href="#" class="btn_close">닫기</a>
    </div>
</div>
<div class="layer_popup ly_ag layer_privacy">
    <div class="title"><?php echo __('개인정보 수집 및 이용')?></div>
    <div class="box_text">
        <?php echo nl2br($TPL_VAR["privateApproval"]['content'])?>

    </div>
    <div class="btn_wrap">
        <a href="#" class="btn_close">닫기</a>
    </div>
</div>

<link type="text/css" rel="stylesheet" href="<?php echo setBrowserCache('/data/skin/mobile/aileenwedding/css/gd_simple_join.css')?>">
<script>
    $(function(){
        gd_select_email_domain('joinEmail','joinEmailDomain');
        var simpleJoinFl = true;
        //button toggle
        $('.simple_join .tit_area').click(function(){
            $('.simple_join .input_box_wrap').slideUp();

            if($(this).siblings().css('display') == 'none'){
                $(this).siblings().slideDown(300);
                $(this).find('.btn_toggle').removeClass('on');
            }else{
                $(this).siblings().slideUp(200);
                $(this).find('.btn_toggle').addClass('on');
            }
        });


        //레이어 팝업 함수
        function lyPopup(prm){
            $(prm).css({
                'position': 'fixed',
                'left': '50%',
                'top': '50%'
            });

            $(prm).css({
                'margin-left': -$(prm).outerWidth() / 2 + 'px',
                'margin-top': -$(prm).outerHeight() / 2 + 'px'
            });

            $(prm).show();
            $('.ly_dim').addClass('on');
        }

        //팝업
        $('.simple_join .btn_sj_submit').on({
            'click':function(e){
                if(!simpleJoinFl) return false;
                simpleJoinFl = false;
                e.preventDefault();
                var email = $('input[name=joinEmail]').val();
                var pw = $('input[name=joinPw]').val();
                var pwCheck = $('input[name=joinPwCheck]').val();
                $('#frmSimpleJoin input[name=memId]').val(email);
                $('#frmSimpleJoin input[name=email]').val(email);
                $('#frmSimpleJoin input[name=memPw]').val(pw);
                $('#frmSimpleJoin input[name=memPwCheck]').val(pwCheck);
                if(!email) {
                    simpleJoinFl = true;
                    alert('이메일을 입력해주세요.');
                    return false;
                }
                if($('.sj_join_box  .min_txt').is(':visible')) {
                    simpleJoinFl = true;
                    alert($('.sj_join_box  .min_txt:visible:eq(0)').text());
                    return false;
                }
                if(!pw) {
                    simpleJoinFl = true;
                    alert('비밀번호를 입력해주세요.');
                    return false;
                }
                if(!pwCheck) {
                    simpleJoinFl = true;
                    alert('비밀번호 확인을 입력해주세요.');
                    return false;
                }
                if(pw != pwCheck) {
                    simpleJoinFl = true;
                    alert('비밀번호가 서로 다릅니다.');
                    return false;
                }
                if($('#chk_sj_agree').is(':checked') == false) {
                    simpleJoinFl = true;
                    alert('사용 동의 항목을 체크해주세요.');
                    return false;
                }

                var data = {
                    mode: 'simpleJoin',
                    memId: email,
                    email: email,
                    memPw: pw
                }
                var mileage = 0;
                var coupon;
                var $ajax = $.ajax('../member/member_ps.php', {type: "post", data: data});
                $ajax.done(function (data, textStatus, jqXHR) {
                    simpleJoinFl = true;
                    if(!data.result) {
                        alert(data.message);
                        return false;
                    }
                    $('.simple_join_layer .id').text(email);
                    if(data.mileage) {
                        mileage = parseInt(data.mileage);
                        if(mileage > 0) {
                            $('.simple_join_layer .mileage span').text(mileage);
                            $('.simple_join_layer .mileage').show();
                        }
                    }
                    if(data.coupon != 'false') {
                        coupon = data.coupon;
                        if(coupon) $('.simple_join_layer .coupon').text(coupon).show();
                    }
                    if(mileage > 0 || coupon) {
                        $('.simple_join_layer .ly_simple_join').addClass('type2');
                    }
                    var lySj = '.simple_join_layer';
                    lyPopup(lySj);
                });
            }
        });

        //레이어 팝업 리사이즈시
        $(window).resize(function() {
            $('.simple_join_layer').css({
                'position': 'fixed',
                'left': '50%',
                'top': '50%'
            });

            $('.simple_join_layer').css({
                'margin-left': -$('.simple_join_layer').outerWidth() / 2 + 'px',
                'margin-top': -$('.simple_join_layer').outerHeight() / 2 + 'px'
            });

        });

        //레이어 팝업 닫기
        $('.simple_join_layer .ly_close').on({
            'click':function(e){
                location.reload();
            }
        });

        $('.simple_join_layer .btn_ly_ok').on({
            'click': function(e){
                location.reload();
            }
        });

        $('input[name=joinPwCheck]').focusout(function(){
            if($('input[name=joinPw]').val() != $(this).val()) {
                $('.min_txt.joinPwCheck').show();
            } else {
                $('.min_txt.joinPwCheck').hide();
            }
        });
        $('input[name=joinPw]').focusout(function(){
            simpleJoinFl = false;
            ajax_validate($(this), {
                memPw: $(this).val(), mode: "validateMemberPassword"
            });
        });
        $('input[name=joinEmail]').focusout(function(){
            simpleJoinFl = false;
            ajax_validate($(this), {
                memId: $(this).val(), email: $(this).val(), mode: "overlapMemId"
            });
        });

        function ajax_validate($target, data) {
            var $ajax = $.ajax('../member/member_ps.php', {type: "post", data: data});
            var mode = data.mode;
            var name = $target.attr('name');
            $ajax.done(function (data, textStatus, jqXHR) {
                var code = data.code;
                var message = data.message;
                if (_.isUndefined(code) && _.isUndefined(message)) {
                    $('.min_txt.'+name).hide();
                    if(mode == 'overlapMemId') {
                        ajax_validate($target, {
                            email: $target.val(), mode: "overlapEmail"
                        });
                    } else {
                        simpleJoinFl = true;
                    }
                } else {
                    if(mode == 'overlapMemId') message = message.replace(/아이디는/g, '이메일은').replace(/아이디/g, '이메일');
                    $('.min_txt.'+name).html(message).show();
                    simpleJoinFl = true;
                }
            });
        }

        //이용약관,개인정보 수집 및 이용 레이어 팝업 함수
        function ly_Popup(prm){
            var top = ( $(window).scrollTop() + ( $(window).height() - $(prm).outerHeight()) / 2 );
            $(prm).css({'top':top,});
            $(prm).show();
            $('.ly_dim').addClass('on');
        }

        //이용약관
        $('.btn_layer_agree').on({
            'click':function(e){
                e.preventDefault();
                var lySj = '.layer_agree';
                ly_Popup(lySj);
            }
        });

        //개인정보 수집 및 이용
        $('.btn_layer_privacy').on({
            'click':function(e){
                e.preventDefault();
                var lySj = '.layer_privacy';
                ly_Popup(lySj);
            }
        });

        //레이어 팝업 닫기
        $('.layer_popup .btn_close').on({
            'click':function(e){
                e.preventDefault();
                $(this).parent().parent().hide();
                $('.ly_dim').removeClass('on');
            }
        });

    });
</script>
<?php }?>