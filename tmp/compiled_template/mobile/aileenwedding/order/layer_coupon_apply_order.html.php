<?php /* Template_ 2.2.7 2020/05/08 01:21:17 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/order/layer_coupon_apply_order.html 000064436 */ 
if (is_array($TPL_VAR["cartInfo"])) $TPL_cartInfo_1=count($TPL_VAR["cartInfo"]); else if (is_object($TPL_VAR["cartInfo"]) && in_array("Countable", class_implements($TPL_VAR["cartInfo"]))) $TPL_cartInfo_1=$TPL_VAR["cartInfo"]->count();else $TPL_cartInfo_1=0;?>
<div class="layer_coupon_apply_order">
	<div class="ly_head">
		<h1 class="h_tit"><?php echo __('쿠폰 적용하기')?></h1>
	</div>
	<div class="notice_box">
<?php if(gd_is_login()===false){?>
			<p class="my_notice">※ <?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?></p>
<?php }else{?>
			<p class="my_notice">※ <?php echo __('가능한 쿠폰만 노출됩니다. 실제 보유한 쿠폰과 차이가 날 수 있습니다.')?></p>
<?php }?>
	</div>
	<div class="ly_content ly_ct">
		<div class="ly_coupon_list">
			<form name="frmCouponApply" method="post">
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
                <h2 class="my_tit">상품쿠폰</h2>
                <div class="order">
                    <div class="pay">
                        <ul class="my_goods">
<?php if($TPL_cartInfo_1){foreach($TPL_VAR["cartInfo"] as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {foreach($TPL_R3 as $TPL_V3){?>
                            <li>
                                <div class="info">
                                    <div>
                                        <div class="itemhead">
                                            <div class="thmb_box">
                                                <div class="thmb">
                                                    <a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsImage"]?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="itembody" style="width:100%;">
                                            <p class="name"><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>"><?php echo $TPL_V3["goodsNm"]?></a></p>
                                            <p>
                                                주문수량 :
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                                <?php echo $TPL_V3["goodsCnt"]?>

<?php }else{?>
                                                <?php echo $TPL_V3["goodsCnt"]?>

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo $TPL_V3["goodsCnt"]?>

<?php }?>
<?php }?>
                                            </p>
<?php if((is_array($TPL_R4=$TPL_V3["option"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {$TPL_S4=count($TPL_R4);$TPL_I4=-1;foreach($TPL_R4 as $TPL_V4){$TPL_I4++;?>
<?php if($TPL_V4["optionName"]){?>
                                            <p><?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?> <?php if((($TPL_I4+ 1)==$TPL_S4)&&$TPL_V3["optionPrice"]!= 0){?><strong>(<?php if($TPL_V3["optionPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V3["optionPrice"])?>)<?php }?></p>
<?php }?>
<?php }}?>
                                            <strong class="prc" style="float:right;">
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                                <?php echo $TPL_V3["goodsPriceString"]?>

<?php }else{?>
                                                <?php echo gd_global_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?>

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
                                                <?php echo gd_global_add_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?>

<?php }?>
<?php }?>
                                            </strong>
                                        </div>
                                    </div>
<?php if(empty($TPL_V3["addGoods"])===false){?>
                                    <div class="add_goods_box">
                                        <span class="add_title">추가상품</span>
                                        <ul class="add_goods_list">
<?php if((is_array($TPL_R4=$TPL_V3["addGoods"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
                                            <li>
                                                <div class="add_goods_img"><?php echo $TPL_V4["addGoodsImage"]?></div>
                                                <div class="add_goods_content" style="width:100%;">
                                                    <span class="title"><?php echo $TPL_V4["addGoodsNm"]?></span>
                                                    <div class="add_goods_text">
                                                        <span class="goods_number">주문수량 : <em> <?php echo $TPL_V4["addGoodsCnt"]?></em><?php echo __('개')?></span>
                                                        <span class="goods_price" style="float:right;"><em><?php echo gd_global_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?></em></span>
                                                    </div>
                                                </div>
                                            </li>
<?php }}?>
                                        </ul>
                                    </div>
<?php }?>
                                    <!-- // add_goods_box -->
                                </div>
                                <!-- // info -->
                            </li>
                            <div class="order_box">
                                <fieldset>
                                    <legend>쿠폰 적용 및 쿠폰 혜택 영역</legend>
                                    <div class="table_box">
                                        <table width="100%" cellspacing="0" cellpadding="0" class="table_style4">
                                            <caption>쿠폰 적용 선택 및 쿠폰 혜택 내용 영역</caption>
                                            <colgroup>
                                                <col>
                                                <col>
                                            </colgroup>
                                            <tbody>
                                            <tr>
                                                <th scope="row"><label for="">쿠폰 적용</label></th>
                                                <td>
                                                    <div class="inp_sel">
<?php if($TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]]){?>
<?php if((is_array($TPL_R4=$TPL_VAR["convertGoodsCouponArrData"][$TPL_V3["goodsNo"]])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_K4=>$TPL_V4){?>
                                                        <select name="goodsMemberCouponNo[]" class="chosen-select" style="width:100%;" onchange="gd_goods_coupon_select(this, '<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>','change')" data-goodsNo="<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>" data-goods-option-sno="<?php echo $TPL_V3["optionSno"]?>" data-goods-option-key="<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>_<?php echo $TPL_K4?>" data-goods-max-price="<?php echo $TPL_VAR["convertGoodsCouponPriceArrData"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]]['exceptCouponPrice'][$TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo']]?>">
                                                            <option value="">=<?php echo __('선택해주세요')?>=</option>
<?php if($TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]["couponKindType"]=='sale'){?>
                                                            <option data-cart-sno=<?php echo $TPL_VAR["goodsCouponSnoArr"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]]?> data-paytype="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponUseAblePaymentType']?>" data-price="<?php echo $TPL_VAR["convertGoodsCouponPriceArrData"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]]['memberCouponSalePrice'][$TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo']]?>" data-use-price="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponGoodsDcPrice']?>" data-use-goodsNo="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['goodsNo']?>" data-use-couponNo="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponNo']?>" data-use-member-coupon-state="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponState']?>" data-type="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponKindType']?>" data-duplicate="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponApplyDuplicateType']?>" value="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo']?>" <?php if(in_array($TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo'],$TPL_VAR["cartCouponNoArr"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]])==true){?>selected="selected" <?php }?>> <?php echo $TPL_V4['couponNm']?></option>
<?php }elseif($TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]["couponKindType"]=='add'){?>
                                                            <option data-cart-sno=<?php echo $TPL_VAR["goodsCouponSnoArr"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]]?> data-paytype="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponUseAblePaymentType']?>" data-price="<?php echo $TPL_VAR["convertGoodsCouponPriceArrData"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]]['memberCouponAddMileage'][$TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo']]?>" data-use-price="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponGoodsDcPrice']?>" data-use-goodsNo="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['goodsNo']?>" data-use-couponNo="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponNo']?>" data-use-member-coupon-state="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponState']?>" data-type="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponKindType']?>" data-duplicate="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['couponApplyDuplicateType']?>" value="<?php echo $TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo']?>" <?php if(in_array($TPL_VAR["goodsCouponArrData"][$TPL_V3["goodsNo"]][$TPL_K4]['memberCouponNo'],$TPL_VAR["cartCouponNoArr"][$TPL_V3["goodsNo"]][$TPL_V3["optionSno"]])==true){?>selected="selected" <?php }?>> <?php echo $TPL_V4['couponNm']?></option>
<?php }?>
                                                        </select>
                                                        <br/>
                                                        <script>
                                                            // 선택 박스 너비 지정
                                                            $('select[data-goods-option-key="<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>_<?php echo $TPL_K4?>"]').on('chosen:showing_dropdown', function () {
                                                                gd_resizeOption('<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>_<?php echo $TPL_K4?>');
                                                            });
                                                        </script>
<?php }}?>
<?php }else{?>
                                                        <p>쿠폰 적용 제외 상품</p>
<?php }?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><label for="">쿠폰 혜택</label></th>
                                                <td>
                                                    <div id="goodsCouponBenefit_<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>" data-benefitGoodsNo="<?php echo $TPL_V3["goodsNo"]?>" style="display:none;">
                                                        <p>
                                                            <strong>
                                                                <span class="goods-coupon-dc-priceSum"></span><?php echo gd_currency_string()?> <span class="c_point">할인</span>
                                                                <span class="goods-coupon-add-mileageSum" style="margin-left:60px;"></span><?php echo gd_currency_string()?> <span class="c-blue">적립</span>
                                                            </strong>
                                                        </p>
                                                    </div>
                                                    <div id="goodsCouponBenefitNot_<?php echo $TPL_V3["goodsNo"]?>_<?php echo $TPL_V3["optionSno"]?>" data-benefitGoodsNo-not="<?php echo $TPL_V3["goodsNo"]?>" style="padding-top:5px;">-</div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </div>
<?php }}?>
<?php }}?>
<?php }}?>
                        </ul>
                        <!-- // my_goods -->
                    </div>
                    <!-- // pay -->
                </div>
<?php }?>
				<h2 class="my_tit"><?php echo __('주문쿠폰')?></h2>
<?php if($TPL_VAR["memberCouponArrData"]["order"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["memberCouponArrData"]["order"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
				<dl>
					<dd class="c_chk">
						<div class="couponcheck">
							<span class="inp_chk">
<?php if($TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponAlertMsg"][$TPL_V1["memberCouponNo"]]=='LIMIT_MIN_PRICE'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="orderMemberCouponNo[]" class="disabled" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" disabled="disabled">
								<label class="disabled" for="check<?php echo $TPL_V1["memberCouponNo"]?>"></label>
<?php }else{?>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="orderMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="orderMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="orderMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }?>
								<label for="check<?php echo $TPL_V1["memberCouponNo"]?>"></label>
<?php }?>
							</span>
						</div>
					</dd>
					<dt>
						<div class="c_point">
							<?php echo gd_currency_symbol()?>

<?php if($TPL_V1["couponKindType"]=='sale'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]])?>

<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]])?>

<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['order']["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]])?>

<?php }?>
							<?php echo gd_currency_string()?>

						</div>
						<label for="check<?php echo $TPL_V1["memberCouponNo"]?>">
							<?php echo $TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponKindType"]?>

							<div><?php echo $TPL_V1["couponNm"]?></div>
						</label>
					</dt>
					<dd>
						<?php echo $TPL_V1["memberCouponEndDate"]?><br/>
<?php if($TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponMaxBenefit"]){?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponMaxBenefit"]?><br/>
<?php }?>
<?php if($TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponMinOrderPrice"]){?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponMinOrderPrice"]?><br/>
<?php }?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['order'][$TPL_K1]["couponApplyDuplicateType"]?>

					</dd>
				</dl>
<?php }}?>
<?php }else{?>
				<div class="no_data">
					<p><strong><?php echo __('쿠폰이 없습니다.')?></strong></p>
				</div>

<?php }?>

				<h2 class="my_tit"><?php echo __('배송비쿠폰')?></h2>
<?php if($TPL_VAR["memberCouponArrData"]["delivery"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["memberCouponArrData"]["delivery"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
				<dl>
					<dd class="c_chk">
						<div class="couponcheck">
							<span class="inp_chk">
<?php if($TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponAlertMsg"][$TPL_V1["memberCouponNo"]]=='LIMIT_MIN_PRICE'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="deliveryMemberCouponNo[]" class="disabled" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" disabled="disabled">
								<label class="check_s single disabled" for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }else{?>
<?php if($TPL_V1["couponKindType"]=='sale'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="deliveryMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="deliveryMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
								<input type="checkbox" id="check<?php echo $TPL_V1["memberCouponNo"]?>" name="deliveryMemberCouponNo[]" data-paytype="<?php echo $TPL_V1["couponUseAblePaymentType"]?>" data-price="<?php echo $TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]]?>" data-type="<?php echo $TPL_V1["couponKindType"]?>" data-duplicate="<?php echo $TPL_V1["couponApplyDuplicateType"]?>" value="<?php echo $TPL_V1["memberCouponNo"]?>">
<?php }?>
								<label for="check<?php echo $TPL_V1["memberCouponNo"]?>"><?php echo __('선택')?></label>
<?php }?>
							</span>
						</div>
					</dd>
					<dt>
						<div class="c_point">
							<?php echo gd_currency_symbol()?>

<?php if($TPL_V1["couponKindType"]=='sale'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponSalePrice"][$TPL_V1["memberCouponNo"]])?>

<?php }elseif($TPL_V1["couponKindType"]=='add'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponAddMileage"][$TPL_V1["memberCouponNo"]])?>

<?php }elseif($TPL_V1["couponKindType"]=='delivery'){?>
							<?php echo gd_money_format($TPL_VAR["convertMemberCouponPriceArrData"]['delivery']["memberCouponDeliveryPrice"][$TPL_V1["memberCouponNo"]])?>

<?php }?>
							<?php echo gd_currency_string()?>

						</div>
						<label for="check<?php echo $TPL_V1["memberCouponNo"]?>">
							<?php echo $TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponKindType"]?>

							<div><?php echo $TPL_V1["couponNm"]?></div>
						</label>
					</dt>
					<dd>
						<?php echo $TPL_V1["memberCouponEndDate"]?><br/>
<?php if($TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponMaxBenefit"]){?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponMaxBenefit"]?><br/>
<?php }?>
<?php if($TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponMinOrderPrice"]){?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponMinOrderPrice"]?><br/>
<?php }?>
						<?php echo $TPL_VAR["convertMemberCouponArrData"]['delivery'][$TPL_K1]["couponApplyDuplicateType"]?>

					</dd>
				</dl>
<?php }}?>
<?php }else{?>
				<div class="no_data">
					<p><strong><?php echo __('쿠폰이 없습니다.')?></strong></p>
				</div>
<?php }?>
			</form>
		</div>
		<div class="btn_box">
			<span><button id="btnCouponApply" class="ly_coupon_apply_btn"><?php echo __('쿠폰 사용하기')?></button></span>
		</div>
	</div>
	<div class="close_btn">
		<button type="button" class="lys_close_btn js_coupon_apply_close ly_btn_close"><?php echo __('닫기')?></button>
	</div>
</div>
<?php if(gd_is_login()!==false){?>
<script type="text/javascript">
    <!--
    $(document).ready(function () {

		$('#popupCouponApply .ly_ct').height($(window).height()-73);
        $('body.portrait').css({overflow: 'hidden'});

<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
        // 상품쿠폰 주문에서 적용 여부 기능 추가
        $('select[name="goodsMemberCouponNo[]"]').each(function (index, val) {
            var selectGoodsNo = $(val).attr('data-goodsNo');
            var childSelected = $(val).children('option:last').attr('selected');
            $(val).children('option:last').css('white-space','nowrap').css('overflow-y','hidden').css('overflow-x','visible');
            if(childSelected == 'selected') {
                gd_goods_coupon_select(val, selectGoodsNo, 'load');
            }
        });
<?php }?>
        $('input:checkbox[name="orderMemberCouponNo[]"]').click(function (e) {
            if (($(this).prop('checked') == true) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="orderMemberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).attr("checked", false);
                    $(this).next('label').removeClass('on');
                    $(this).attr('disabled', 'disabled');
                });
            } else if (($(this).prop('checked') == false) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="orderMemberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).removeAttr('disabled', 'disabled');
                });
            }
            var objCouponPrice = gd_coupon_price_sum();
            $('#couponSalePrice').text(numeral(objCouponPrice.saleDc).format());
            $('#couponAddPrice').text(numeral(objCouponPrice.add).format());
        });
        $('input:checkbox[name="deliveryMemberCouponNo[]"]').click(function (e) {
            if (($(this).prop('checked') == true) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="deliveryMemberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).attr("checked", false);
                    $(this).next('label').removeClass('on');
                    $(this).attr('disabled', 'disabled');
                });
            } else if (($(this).prop('checked') == false) && ($(this).data('duplicate') == 'n')) {
                $('input:checkbox[name="deliveryMemberCouponNo[]"]').not($(this)).each(function (index) {
                    $(this).removeAttr('disabled', 'disabled');
                });
            }
            var objCouponPrice = gd_coupon_price_sum();
            $('#couponDeliveryPrice').text(numeral(objCouponPrice.deliveryDc).format());
        });
       $('#btnCouponApply').click(function (e) {
            var couponApplyNoArr = new Array;
            var couponPaymentTypeCheck = 'n';
            var couponNoArr = new Array;

<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
           // 상품쿠폰 주문에서 적용 여부 기능 추가
               var goodsCouponApplyNoArr = new Array(); // 상품쿠폰적용번호(배열)
               var goodsCouponApplyNoAllArr = new Array(); // 상품쿠폰중복가능전체적용번호(배열)
               var goodsCartSnoArr = new Array(); // 카트 sno 비교 (배열)
               var goodsCartNoCouponParam = new Object(); // 전달상품쿠폰적용번호 (오브젝트)
               $('select[name="goodsMemberCouponNo[]"] option:selected').each(function (index, tag) {
                   var goodsApplySno = $(this).val(); // 회원 상품쿠폰 적용번호(단일)
                   var goodsCartSno = $(this).attr('data-cart-sno'); // 카트일련번호
                   var tagSelected = $(tag).attr('selected'); // 선택 여부
                   couponApplyNoArr.push($(this).val()); // 적용 상품 쿠폰 번호 - 주문용(금액계산)

                   // 적용 상품 쿠폰사용 기간 체크
                   if ($(this).attr('data-use-couponno')) {
                       couponNoArr.push($(this).attr('data-use-couponno'));
                   }

                   if(tagSelected == 'selected') {
                       if($.inArray(goodsCartSno, goodsCartSnoArr) == '-1') { // 카트번호배열에 없을 때(배열)
                           goodsCouponApplyNoArr = []; // 적용 상품쿠폰번호 초기화
                           goodsCartSnoArr.push(goodsCartSno);
                           goodsCouponApplyNoArr.push(goodsApplySno); // 적용 상품 쿠폰 번호 - 상품용
                       } else {
                           if (goodsApplySno) { // 상품쿠폰적용번호(단일)
                               if ($.inArray(goodsApplySno, goodsCouponApplyNoArr) == '-1') { // 상품쿠폰적용번호에 없을 때(배열)
                                   goodsCouponApplyNoArr.push(goodsApplySno); // 적용 상품 쿠폰 번호 - 상품용
                               }
                           }
                       }
                       goodsCouponApplyNoAllArr.push(goodsApplySno); // 적용 상품 쿠폰번호 - 전체용
                   }
                   var goodsCouponApplyNoArrString = goodsCouponApplyNoArr.join('<?php echo INT_DIVISION?>'); // 구분자로 적용쿠폰번호 직렬화
                   if(goodsCartSno && goodsCouponApplyNoArrString) {
                       goodsCartNoCouponParam[goodsCartSno] = goodsCouponApplyNoArrString; // 오프젝트 삽입
                   }
                   // 결제방식제한쿠폰체크
                   if ($(this).attr('data-paytype') == 'bank') {
                       couponPaymentTypeCheck = 'y';
                   }
               });

               // 쿠폰 사용기간 체크
               if (couponNoArr.length > 0) {
                   var couponNoString = couponNoArr.join('<?php echo INT_DIVISION?>');
                   var checkCouponType = true;
                   $.ajax({
                       method: "POST",
                       cache: false,
                       async: false,
                       url: "../order/cart_ps.php",
                       data: {mode: 'checkCouponType', couponNo: couponNoString},
                       success: function (data) {
                           checkCouponType = data.isSuccess;
                       },
                       error: function (e) {
                       }
                   });

                   if (!checkCouponType) {
                       alert('사용할 수 없는 쿠폰입니다.');
                       gd_close_layer();
                       return false;
                   }
               }
<?php }?>

            $('input:checkbox[name="orderMemberCouponNo[]"]:checked').each(function (index) {
                couponApplyNoArr.push($(this).val());
                // 결제방식제한쿠폰체크
                if ($(this).attr('data-paytype') == 'bank') {
                    couponPaymentTypeCheck = 'y';
                }
            });
            $('input:checkbox[name="deliveryMemberCouponNo[]"]:checked').each(function (index) {
                couponApplyNoArr.push($(this).val());
                // 결제방식제한쿠폰체크
                if ($(this).attr('data-paytype') == 'bank') {
                    couponPaymentTypeCheck = 'y';
                }
            });
            if (couponApplyNoArr.length > 0) {
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
                // 상품쿠폰 주문에서 적용 여부 기능 추가 - 상품쿠폰번호 제거
                if(goodsCouponApplyNoAllArr.length > 0) {
                    var couponApplyNoTempArr = new Array(); // 배열 가공용 임시 배열
                    $(couponApplyNoArr).each(function (index, data) {
                        if($.inArray(data, goodsCouponApplyNoAllArr) == '-1') {
                            if(data) {
                                couponApplyNoTempArr.push(data);
                            }
                        }
                    });
                    couponApplyNoArr = new Array();
                    couponApplyNoArr = couponApplyNoTempArr; // 배열 대체
                    // 상품쿠폰 주문에서 적용 여부 기능 추가
                    if (goodsCouponApplyNoArr.length > 0) {
                        // 상품쿠폰 사용 시 cart 재선언 호출
                        gd_goods_coupon_cart_call(goodsCartNoCouponParam);
                        var objGoodsCouponPrice = gd_coupon_price_sum();
                        $('input:hidden[name="totalCouponGoodsDcPrice"]').val(objGoodsCouponPrice.goodsSale);
                        $('input:hidden[name="totalCouponGoodsMileage"]').val(objGoodsCouponPrice.goodsAdd);
                    }
                }
<?php }?>
                var couponApplyNoString = couponApplyNoArr.join('<?php echo INT_DIVISION?>');
                var objCouponPrice = gd_coupon_price_sum();
                $('input:hidden[name="couponApplyOrderNo"]').val(couponApplyNoString);
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
                    $('input:hidden[name="totalCouponOrderDcPrice"]').val(objCouponPrice.saleDc - objCouponPrice.goodsSale);
                    $('input:hidden[name="totalCouponOrderMileage"]').val(objCouponPrice.add - objCouponPrice.goodsAdd);
                    $('#useDisplayCouponDcPrice').text(numeral(objCouponPrice.saleDc - objCouponPrice.goodsSale).format());
                    $('#useDisplayCouponMileage').text(numeral(objCouponPrice.add - objCouponPrice.goodsAdd).format());
<?php }else{?>
                    $('input:hidden[name="totalCouponOrderDcPrice"]').val(objCouponPrice.saleDc);
                    $('input:hidden[name="totalCouponOrderMileage"]').val(objCouponPrice.add);
                    $('#useDisplayCouponDcPrice').text(numeral(objCouponPrice.saleDc).format());
                    $('#useDisplayCouponMileage').text(numeral(objCouponPrice.add).format());
<?php }?>
                $('input:hidden[name="totalCouponOrderPrice"]').val(objCouponPrice.sale);
                $('input:hidden[name="totalCouponDeliveryDcPrice"]').val(objCouponPrice.deliveryDc);
                $('input:hidden[name="totalCouponDeliveryPrice"]').val(objCouponPrice.delivery);
                $('#useDisplayCouponDelivery').text(numeral(objCouponPrice.deliveryDc).format());
                $('.order_coupon_benefits').removeClass('dn');
                if ($('input[name="chooseCouponMemberUseType"]').val() == 'coupon') {
                    $('#saleDefault').addClass('dn');
                    $('#saleWithoutMember').removeClass('dn');
                    $('#mileageDefault').addClass('dn');
                    $('#mileageWithoutMember').removeClass('dn');
                }
            } else {
                $('input:hidden[name="couponApplyOrderNo"]').val('');
                $('input:hidden[name="totalCouponOrderDcPrice"]').val('');
                $('input:hidden[name="totalCouponOrderPrice"]').val('');
                $('input:hidden[name="totalCouponOrderMileage"]').val('');
                $('input:hidden[name="totalCouponDeliveryDcPrice"]').val('');
                $('input:hidden[name="totalCouponDeliveryPrice"]').val('');
                $('#useDisplayCouponDcPrice').text(0);
                $('#useDisplayCouponMileage').text(0);
                $('#useDisplayCouponDelivery').text(0);
                $('.order_coupon_benefits').addClass('dn');
                if ($('input[name="chooseCouponMemberUseType"]').val() == 'coupon') {
                    $('#saleDefault').removeClass('dn');
                    $('#saleWithoutMember').addClass('dn');
                    $('#mileageDefault').removeClass('dn');
                    $('#mileageWithoutMember').addClass('dn');
                }
            }

            if (couponPaymentTypeCheck == 'y') {
                $('#payment2').trigger('click');
                $('label[for="payment2"]').addClass('on');

                if ($('#settlekind_payco').length) {
                    $('#settlekind_payco').addClass('dn');
                }
                if ($('#spanPayment3').length) {
                    $('#spanPayment3').addClass('dn').removeClass('inp_rdo');
                    $('label[for="payment3"]').removeClass('on');
                }
                if ($('#spanPayment4').length) {
                    $('#spanPayment4').addClass('dn').removeClass('inp_rdo');
                    $('label[for="payment4"]').removeClass('on');
                }
                $('#settleKind_gb').trigger('click');
                gd_settlekind_toggle();

                $('[id^="settlekind_type_"]').each(function (index) {
                    if ($(this).attr('id') != 'settlekind_type_gb') {
                        var tempId = $(this).attr('id');
                        var sId = tempId.replace('settlekind_type_', '');
                        $(this).addClass('dn');
                        $('label[for="settleKind_' + sId + '"]').removeClass('on');
                    }
                });
            } else {
                if ($('#settlekind_payco').length) {
                    $('#settlekind_payco').removeClass('dn');
                }
                if ($('#spanPayment3').length) {
                    $('#spanPayment3').addClass('inp_rdo').removeClass('dn');
                }
                if ($('#spanPayment4').length) {
                    $('#spanPayment4').addClass('inp_rdo').removeClass('dn');
                }
                $('[id^="settlekind_type_"]').each(function (index) {
                    if ($(this).attr('id') != 'settlekind_type_gb') {
                        $(this).removeClass('dn');
                    }
                });
            }

            // 마일리지, 예치금 초기화

            if ($('input:text[name="useDeposit"]').length) {
                $('input:text[name="useDeposit"]').val('');
            }
            if ($('input:text[name="useMileage"]').length) {
                $('input:text[name="useMileage"]').val('');
            }

            gd_set_recalculation();
            gd_set_real_settle_price();
<?php if($TPL_VAR["mileageUseFl"]=='y'){?>
            gd_mileage_use_check('y', 'n', 'n');
<?php }?>
           $("#popupCouponApply .js_coupon_apply_close").click();
        });
        gd_coupon_apply_setting();
    });

    // 쿠폰 적용 내용 초기화 (설정)
    function gd_coupon_apply_setting() {
        var couponApplyNoString = '<?php echo $TPL_VAR["couponApplyOrderNo"]?>';
        var couponApplyNoArr = new Array();
        if (couponApplyNoString) {
            var couponApplyNoArr = couponApplyNoString.split('<?php echo INT_DIVISION?>');
        }
        $.each(couponApplyNoArr, function (index) {
            $('input:checkbox[name="orderMemberCouponNo[]"][value="' + couponApplyNoArr[index] + '"]').trigger('click');
            $('input:checkbox[name="deliveryMemberCouponNo[]"][value="' + couponApplyNoArr[index] + '"]').trigger('click');
        });
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
        // 상품쿠폰 주문에서 적용 여부 기능 추가
        var goodsCouponNoArrString = '<?php echo $TPL_VAR["cartCouponNoDivisionArr"]?>';
        var goodsCouponApplyNoArr = new Array();
        if (goodsCouponNoArrString) {
            var goodsCouponApplyNoArr = goodsCouponNoArrString.split('<?php echo STR_DIVISION?>');
        }
        $.each(goodsCouponApplyNoArr, function (index, goodsCouponApplyNoArrVal) {
            var cartGoodsSno = goodsCouponApplyNoArrVal.split('<?php echo INT_DIVISION?>');
            $('select[data-goodsNo="' + cartGoodsSno[0] + '"] option[value="' + goodsCouponApplyNoArr[cartGoodsSno[1]] + '"]:selected').trigger('change');
        });
<?php }?>

        var objCouponPrice = gd_coupon_price_sum();
        $('#couponSalePrice').text(numeral(objCouponPrice.saleDc).format());
        $('#couponAddPrice').text(numeral(objCouponPrice.add).format());
        $('#couponDeliveryPrice').text(numeral(objCouponPrice.deliveryDc).format());
    }
    // 선택 쿠폰 금액 계산
    function gd_coupon_price_sum() {
        var salePrice = 0;
        var saleDcPrice = 0;
        var originSalePrice = numeral().unformat($('#totalGoodsPrice').text());
        var addPrice = 0;
        var deliveryPrice = 0;
        var deliveryDcPrice = 0;
        var originDeliveryCharge = numeral().unformat($('#totalDeliveryCharge').text()) + numeral().unformat($('#deliveryAreaCharge').text());
        if (!_.isUndefined($('input[name="deliveryFree"]')) && $('input[name="deliveryFree"]').val() == 'y') {
            originDeliveryCharge -= numeral().unformat($('#totalDeliveryCharge').text());
        }

<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
        // 상품쿠폰 주문에서 적용 여부 기능 추가
        var goodsCouponSalePrice = 0;
        var goodsCouponAddPrice = 0;
        var couponTempPrice = 0;
        var couponPriceArray = []; // 선택박스 상품번호, 가격 저장배열
        var couponSumArray = []; // 총합계금액 계산 배열
        var goodsNoTempArray =[]; // 임시 상품번호 배열
        var couponMaxPriceArray =[]; // 쿠폰최대적용가격 배열
        $('select[name="goodsMemberCouponNo[]"] option:selected').each(function (index) {
            var goodsNo = $(this).parent('select').attr('data-goodsNo');
            var couponPrice = parseFloat($(this).attr('data-price')); // 쿠폰가격
            var couponMaxPrice = parseFloat($(this).parent('select').attr('data-goods-max-price')); // 쿠폰최대적용가격
            if ($(this).data('type') == 'sale') {
                // 비교를 위해 쿠폰임시 배열 삽입 goodsNo, couponPrice
                if(isNaN(couponPrice) == false) {
                    couponPriceArray.push({
                        goodsNo: goodsNo,
                        price:  couponPrice,
                        maxPrice : couponMaxPrice,
                    });

                }
            } else if ($(this).data('type') == 'add') {
                addPrice += parseFloat($(this).data('price'));
                goodsCouponAddPrice += parseFloat($(this).data('price')); // 상품쿠폰 적립액 별도 계산
            }
        });
        for(var key in couponPriceArray) {
            // 상품번호 체크 배열
            if($.inArray(couponPriceArray[key].goodsNo, goodsNoTempArray) == '-1') {
                goodsNoTempArray.push(couponPriceArray[key].goodsNo);
                couponTempPrice = 0; // temp합계변수 초기화
            }
            if(couponPriceArray[key].price) {
                couponTempPrice += couponPriceArray[key].price;
                couponSumArray[couponPriceArray[key].goodsNo] = couponTempPrice;
                couponMaxPriceArray[couponPriceArray[key].goodsNo] = couponPriceArray[key].maxPrice;
            }
        }
        // 상품번호 기준 Max값 비교 계산
        for(var goodsKey in couponSumArray) {
            var goodsMaxPrice = couponMaxPriceArray[goodsKey]; // 부모selectbox에서 max값 추출
            if(couponSumArray[goodsKey]) {  // 값이 있을 경우
                if(couponSumArray[goodsKey] > parseFloat(goodsMaxPrice)) {  // 최대 적용 가격보다 큰 경우
                    goodsCouponSalePrice += parseFloat(goodsMaxPrice); // Max값 더하기
                } else { // 작은 경우
                    if(couponSumArray[goodsKey] > 0 ) {
                        goodsCouponSalePrice += couponSumArray[goodsKey];  // 합산금액 더하기
                    }
                }
            }
        }
        salePrice = goodsCouponSalePrice;
<?php }?>

        $('input:checkbox[name="orderMemberCouponNo[]"]:checked').each(function (index) {
            if ($(this).data('type') == 'sale') {
                salePrice += parseFloat($(this).data('price'));
            } else if ($(this).data('type') == 'add') {
                addPrice += parseFloat($(this).data('price'));
            }
        });
        $('input:checkbox[name="deliveryMemberCouponNo[]"]:checked').each(function (index) {
            if ($(this).data('type') == 'delivery') {
                deliveryPrice += parseFloat($(this).data('price'));
            }
        });
        if (!_.isUndefined(originSalePrice) && deliveryPrice > originSalePrice) {
            saleDcPrice = originSalePrice;
        } else {
            saleDcPrice = salePrice;
        }
        if (!_.isUndefined(originDeliveryCharge) && deliveryPrice > originDeliveryCharge) {
            deliveryDcPrice = originDeliveryCharge;
        } else {
            deliveryDcPrice = deliveryPrice;
        }

        // 상품쿠폰 주문에서 적용 여부 기능 추가
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
        var couponPrice = {
            'sale': salePrice - goodsCouponSalePrice, // 상품쿠폰할인액 제외
            'saleDc': saleDcPrice,
            'add': addPrice, // 상품쿠폰적립액 제외
            'delivery': deliveryPrice, // 쿠폰 자체의 할인가
            'deliveryDc': deliveryDcPrice, // 배송비 금액에 따른 실제 할인가
            'goodsSale': goodsCouponSalePrice, // 상품쿠폰할인액
            'goodsAdd': goodsCouponAddPrice, // 상품쿠폰적립액
        };
<?php }else{?>
        var couponPrice = {
            'sale': salePrice,
            'saleDc': saleDcPrice,
            'add': addPrice,
            'delivery': deliveryPrice, // 쿠폰 자체의 할인가
            'deliveryDc': deliveryDcPrice, // 배송비 금액에 따른 실제 할인가
        };
<?php }?>

        return couponPrice;
    }
<?php if($TPL_VAR["productCouponChangeLimitType"]=='n'){?>
    // 상품쿠폰 주문에서 적용 여부 기능 추가
    function gd_goods_coupon_select(element, selectGoodsNo, mode) {
        if(element) {
            var selectGoodsCouponSno = $(element).val(); // 쿠폰sno
            var selectedCheck = true; // 선택 체크 여부
            var tagFirst = $(element).children('option:first'); // 각 셀렉트 옵션 첫째 값
            var tagLast = $(element).children('option:last'); // 각 셀렉트 옵션 둘째 값
            var tagLastSelected = $(tagLast).attr('selected'); // 태그 선택값
            var goodsCouponSalePrice = 0;
            var goodsCouponAddPrice = 0;
            var useChangeCheck = false; // 장바구니 사용된 쿠폰 사용여부
            var couponUseGoodsNo = '';       // 장바구니 사용된 쿠폰 사용할 상품번호
            var couponUseSalePrice = 0; // 장바구니 사용된 쿠폰의 할인가격
            var minusCouponDcPrice = 0; // 장바구니에 사용된 쿠폰을 사용할 경우 차감될 쿠폰의 할인가
            var minusCouponDcMileage = 0; // 장바구니에 사용된 쿠폰을 사용할 경우 차감될 쿠폰의 마일리지
            var cartCouponIndex; // 장바구니 적용된 쿠폰사용시 기존 선택된 쿠폰 해제시 사용
            var selectCouponState = $(element).children("option:selected").attr('data-use-member-coupon-state'); // 장바구니 사용중인 쿠폰의 상태값

            // 선택해주세요 선택 시 옵션 상태 변경
            if (($(element).children("option:selected").val() == '') && mode !='load') {
                selectedCheck = false; // 선택 체크 여부
                $(tagFirst).attr('selected', true);
                $(tagFirst).trigger("chosen:updated");
                $(tagLast).attr('selected', false);
                $(tagLast).trigger("chosen:updated");
                // 선택 불가 처리 해제
                if (($(tagLast).data('duplicate') == 'n') &&($(tagLast).attr('disabled') != 'disabled')) {
                    $('select[name="goodsMemberCouponNo[]"] option').removeAttr('disabled');
                    $('select[name="goodsMemberCouponNo[]"] option').trigger("chosen:updated");
                }
            }
            // 상품쿠폰 Select Box 제어 - 중복체크 & 사용 조건 체크
            if (selectedCheck == true) {
                // 중복 사용 불가 쿠폰 선택 시 disable 처리 및 변경
                if (((tagLastSelected != 'selected') && ($(tagLast).data('duplicate') == 'n')) || ((mode == 'load') && (tagLastSelected == 'selected') && ($(tagLast).data('duplicate') == 'n'))) {
                    $('select[data-goodsNo="' + selectGoodsNo + '"]').children('option').not($(tagLast)).each(function (index, controlTag) {
                        if (controlTag.value) { // 태그 값(쿠폰sno)
                            // 선택 해제 및 선택 불가 처리
                            $(controlTag).attr('selected', false);
                            $(controlTag).attr('disabled', true);
                            $(controlTag).trigger("chosen:updated");
                        }
                    });
                }
                // select 선택 모드
                if(mode != 'load') {
                    // 기존 선택된 쿠폰 체크 & 최소 상품 구매 금액 제한 처리 및 변경
                    $('select[name="goodsMemberCouponNo[]"] option:selected').each(function (index, tag) {
                        var tagSelected = $(tag).attr('selected'); // 태그 선택 값
                        var tagVal = tag.value; // 태그 값(쿠폰sno)
                        var tagCartSno = $(tag).attr('data-cart-sno'); // 장바구니 번호
                        var tagGoodsNo = $(tag).attr('data-use-goodsno');
                        var tagCouponUsePrice = numeral($(tag).attr('data-use-price')).format(); // 장바구니 사용된 쿠폰금액
                        var tagMemberCouponState = $(tag).attr('data-use-member-coupon-state'); // 쿠폰 사용가능 여부
                        if (tagVal != '') {
                            if ((selectGoodsCouponSno == tagVal) && (tagSelected == 'selected')) { // 기존 선택된 쿠폰 체크
                                // 장바구니 담긴 쿠폰이 아닌 경우에만 체크
                                if (selectCouponState != 'cart') {
                                    alert("<?php echo __('해당 쿠폰은 이미 다른 상품에 적용중입니다. 다시 선택해주세요')?>");
                                    $(tagFirst).attr('selected', true);
                                    $(tagLast).attr('selected', false);
                                    // 선택 불가 처리 해제
                                    $('select[data-goodsNo="' + selectGoodsNo + '"] option').removeAttr('disabled');
                                    $('select[data-goodsNo="' + selectGoodsNo + '"] option').trigger("chosen:updated");
                                    selectGoodsCouponSno = false; // couponSno 초기화
                                    return false;
                                } else {
                                    cartCouponIndex = index;
                                }
                            } else if ((selectGoodsCouponSno == tagVal) && (tagSelected != 'selected')) { // 현재 선택한 값
                                // 쿠폰 사용 가능 여부 체크(사용조건미성립)
                                var goodsCouponUseAble = gd_goods_coupon_able_check(tagVal, tagCartSno); // 사용 체크 ajax
                                if(goodsCouponUseAble == true) {
                                    // 장바구니 사용 상태의 쿠폰 선택시 사용여부 레이어
                                    if (tagMemberCouponState == 'cart' && (selectGoodsCouponSno == tagVal) && tagCouponUsePrice && tagGoodsNo != 0) {
                                        var tagCouponKindType = $(tag).attr('data-type');
                                        var couponKindTypeName = '';

                                        switch (tagCouponKindType) {
                                            case 'sale':
                                                couponKindTypeName = '할인';
                                                break;
                                            case 'add':
                                                couponKindTypeName = '적립';
                                                break;
                                            case 'delivery':
                                                couponKindTypeName = '배송비할인';
                                                break;
                                            case 'deposit':
                                                couponKindTypeName = '예치금지급';
                                                break;
                                        }

                                        if (!confirm(__('장바구니에 담긴 상품에 ' + tagCouponUsePrice + '원 ' + couponKindTypeName + '으로 이미 적용되어있는 쿠폰입니다.\n 취소 후 이 상품에 적용하시겠습니까?'))) {
                                            $(tagFirst).attr('selected', true);
                                            $(tagLast).attr('selected', false);
                                            selectGoodsCouponSno = false;
                                            $('select[data-goodsNo="' + selectGoodsNo + '"] option').removeAttr('disabled');
                                            $('select[data-goodsNo="' + selectGoodsNo + '"] option').trigger("chosen:updated");
                                            return false;
                                        }

                                        // 장바구니 사용 상태의 쿠폰 적용 취소 및 재적용
                                        gd_goods_coupon_cart_use_check(tagVal, tagCartSno);
                                        selectGoodsCouponSno = false;
                                        useChangeCheck = true;
                                        couponUseGoodsNo = $(tag).attr('data-use-goodsno');
                                        couponUseSalePrice = $(tag).attr('data-price');

                                        // 기존 장바구니 사용된 쿠폰 선택해제
                                        $('select[data-goodsNo="' + couponUseGoodsNo + '"] option:selected').each(function (index, tag) {
                                            if (tag.value == tagVal) {
                                                cartCouponIndex = index;
                                            }
                                        });

                                        if (useChangeCheck === true && selectGoodsCouponSno === false && $('input:hidden[name="totalCouponGoodsDcPrice"]').val() != '') {
                                            $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').removeAttr('selected');
                                            $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').trigger("chosen:updated");
                                            $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').attr('data-use-goodsNo', '');
                                            $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').attr('data-use-price', '');
                                            $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').attr('data-use-member-coupon-state', '');
                                            if ($(tag).data('type') == 'sale') {
                                                minusCouponDcPrice += $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').attr('data-price');
                                            }

                                            if ($(tag).data('type') == 'add') {
                                                minusCouponDcMileage += $('select[data-goods-option-key="' + couponUseGoodsNo + '_' + cartCouponIndex + '"] option:eq(1)').attr('data-price');
                                            }
                                        }
                                    }

                                    $(tagLast).attr('selected', true);
                                    $(tagFirst).attr('selected', false);
                                } else {
                                    alert("<?php echo __('사용할 수 없는 쿠폰입니다.')?>");
                                    $(tagFirst).attr('selected', true);
                                    $(tagLast).attr('selected', false);
                                    selectGoodsCouponSno = false;
                                    $('select[data-goodsNo="' + selectGoodsNo + '"] option').removeAttr('disabled');
                                    $('select[data-goodsNo="' + selectGoodsNo + '"] option').trigger("chosen:updated");
                                }
                            }
                        }
                    });
                    $(element).children('option').trigger("chosen:updated");
                }
            }
            //선택된 쿠폰 상품번호 별 금액 계산
            $('select[name="goodsMemberCouponNo[]"] option').each(function (index, tag) {
                var tagSelected = $(tag).attr('selected'); // 태그 선택 값
                var tagGoodsNo = $(tag).parent('select').attr('data-goodsNo'); // 상품번호
                var tagGoodsMaxPrice = $(tag).parent('select').attr('data-goods-max-price'); // 상품최대할인가격
                if(tagSelected == 'selected' && (selectGoodsNo == tagGoodsNo)) {
                    if ($(tag).data('type') == 'sale') {
                        if(tagGoodsMaxPrice < (goodsCouponSalePrice + parseFloat($(tag).data('price')))) {
                            goodsCouponSalePrice = tagGoodsMaxPrice;
                        } else {
                            goodsCouponSalePrice += parseFloat($(tag).data('price')); // 상품쿠폰 할인가 별도 계산
                        }
                    } else if ($(tag).data('type') == 'add') {
                        goodsCouponAddPrice += parseFloat($(tag).data('price')); // 상품쿠폰 적립액 별도 계산
                    }
                }
            });

            // 상품쿠폰 혜택 노출 여부
            if ((goodsCouponSalePrice > 0) || (goodsCouponAddPrice > 0)) {
                // 장바구니에 사용된 쿠폰을 사용할 경우 차감될 쿠폰금액 적용
                if (minusCouponDcPrice > 0) {
                    var originCouponDcPriceSum = parseFloat(numeral().unformat($('#goodsCouponBenefit_' + couponUseGoodsNo + ' > p > strong > .goods-coupon-dc-priceSum').text())) - parseFloat(minusCouponDcPrice);
                    $('#goodsCouponBenefit_' + couponUseGoodsNo + ' > p > strong > .goods-coupon-dc-priceSum').text(numeral(originCouponDcPriceSum).format());
                    $('input:hidden[name="totalCouponGoodsDcPrice"]').val(parseFloat(originCouponDcPriceSum));
                }

                if (minusCouponDcMileage > 0) {
                    var originCouponDcPriceSum = parseFloat(numeral().unformat($('#goodsCouponBenefit_' + couponUseGoodsNo + ' > p > strong > .goods-coupon-add-mileageSum').text())) - parseFloat(minusCouponDcMileage);
                    $('#goodsCouponBenefit_' + couponUseGoodsNo + ' > p > strong > .goods-coupon-add-mileageSum').text(numeral(originCouponDcPriceSum).format());
                    $('input:hidden[name="totalCouponGoodsMileage"]').val(parseFloat(originCouponDcPriceSum));
                }

                $('#goodsCouponBenefit_' + selectGoodsNo + ' > p > strong > .goods-coupon-dc-priceSum').text(numeral(goodsCouponSalePrice).format());
                $('#goodsCouponBenefit_' + selectGoodsNo + ' > p > strong > .goods-coupon-add-mileageSum').text(numeral(goodsCouponAddPrice).format());
                $('#goodsCouponBenefit_' + selectGoodsNo).show(); // 혜택 컨텐츠
                $('#goodsCouponBenefitNot_' + selectGoodsNo).hide();
            } else {
                $('#goodsCouponBenefit_' + selectGoodsNo + ' > p > strong > .goods-coupon-dc-priceSum').text();
                $('#goodsCouponBenefit_' + selectGoodsNo + ' > p > strong > .goods-coupon-add-mileageSum').text();
                $('#goodsCouponBenefitNot_' + selectGoodsNo).show();
                $('#goodsCouponBenefit_' + selectGoodsNo).hide(); // 혜택 컨텐츠
            }

            // 계산 함수 호출
            var objCouponPrice = gd_coupon_price_sum();
            if (minusCouponDcPrice > 0 || minusCouponDcMileage > 0) {
                gd_set_recalculation();
                gd_set_real_settle_price();
            }

            $('#couponSalePrice').text(numeral(objCouponPrice.saleDc).format());
            $('#couponAddPrice').text(numeral(objCouponPrice.add).format());

        }
    }

    // 상품쿠폰 주문에서 적용 시 사용 가능 여부 검증
    function gd_goods_coupon_able_check(memberCouponNo, cartSno) {
        var returnResult = true;
        var cartAllSno = [];
        $('input[name="cartSno[]"]').each(function(){
            cartAllSno.push($(this).val());
        });
        $.ajax({
            method: "POST",
            cache: false,
            async: false,
            url: "../order/cart_ps.php",
            data: {mode: 'goodsCouponOrderApplyUseCheck', memberCouponNo : memberCouponNo, cartSno : cartSno, cartAllSno : cartAllSno },
            success: function (data) {
                if(data) {
                    returnResult = true;
                } else {
                    returnResult = false;
                }
            },
            error: function (e) {
            }
        });
        return returnResult;
    }

    // 장바구니 사용 상태의 쿠폰 적용 취소 및 재적용
    function gd_goods_coupon_cart_use_check(memberCouponNo, cartSno) {
        var returnResult = true;
        $.ajax({
            method: "POST",
            cache: false,
            async: false,
            url: "../order/cart_ps.php",
            data: {mode: 'UserCartCouponDel', memberCouponNo : memberCouponNo, cartSno : cartSno },
            success: function (data) {
            },
            error: function (e) {
            }
        });
        return returnResult;
    }

    // 상품쿠폰 주문에서 적용 시 Cart 재선언
    function gd_goods_coupon_cart_call(goodsCartNoCouponParam) {
        var cartIdx = [];
        $('input[name="cartSno[]"]').each(function(){
            cartIdx.push($(this).val());
        });
        $.ajax({
            method: "POST",
            cache: false,
            async: false,
            url: "../order/cart_ps.php",
            data: {mode: 'goodsCouponOrderApply', cart :goodsCartNoCouponParam, cartIdx : cartIdx },
            success: function (data) {
                if(data) {
                    alert(data.message);
                    return false;
                } else {
                    return true;
                }
            },
            error: function (e) {
                alert(e.responseText);
                return false;
            }
        });
    }

    //chosen select 사이즈 조절
    function gd_resizeOption(optionKey){
        var minWidth = $('select[data-goods-option-key="' + optionKey + '"]').next('div').outerWidth();
        var maxWidth = minWidth;
        var widthGap = 0;
        var nowOptionWidth = $('select[data-goods-option-key="' + optionKey + '"]').next('div').find('div.chosen-drop li:eq(1)')[0].scrollWidth;
        if(minWidth < nowOptionWidth + 5) { // 옵션 값 영역이 더 클 때
            maxWidth = nowOptionWidth;
            widthGap = parseInt(maxWidth) - parseInt(minWidth); // 차이 계산
        }
        if (minWidth != maxWidth) maxWidth = parseInt(minWidth) + parseInt(widthGap) + 10; // border 감안하여 10
        $('select[data-goods-option-key="' + optionKey + '"]').next('div').find('div.chosen-drop').css({'width': maxWidth + 'px'});
    }
<?php }?>
    //-->
</script>
<?php }?>