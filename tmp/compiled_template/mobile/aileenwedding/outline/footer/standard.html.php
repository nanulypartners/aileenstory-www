<?php /* Template_ 2.2.7 2020/05/08 01:21:19 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/outline/footer/standard.html 000003004 */ ?>
</div>
	<!-- //contents_wrap -->
</div>
<!-- //container -->
<div id="footer">
	<footer>
		<div class="ft_btn_top">
            <a href="#" class="btn_top"><span>TOP</span></a>
        </div>
		<div class="ft_button_box">
			<ul>
				<li><a href="tel:<?php echo $TPL_VAR["gMall"]["centerPhone"]?>"><?php echo __('고객센터')?></a></li>
				<li><a href="<?php echo URI_OVERSEAS_HOME?>main/index.php?pcView=y"><?php echo __('PC화면')?></a></li>
			</ul>
		</div>
		<!-- //main_button_box -->
		<div class="ft_info2_box">
			<ul class="ft_menu">
				<li><a href="../service/company.php"><?php echo __('회사소개')?></a></li>
				<li><a href="../service/agreement.php"><?php echo __('이용약관')?></a></li>
				<li><a href="../service/private.php" class="privacy"><?php echo __('개인정보처리방침')?></a></li>
				<li><a href="../service/guide.php"><?php echo __('이용안내')?></a></li>
			</ul>
			<!-- //ft_menu -->
			<p class="ft_address">
				<?php echo __('상호')?> : <?php echo $TPL_VAR["gMall"]["companyNm"]?> <?php echo __('대표')?> : <?php echo $TPL_VAR["gMall"]["ceoNm"]?> <br>
				<?php echo __('주소')?> : <?php echo $TPL_VAR["gMall"]["address"]?> <?php echo $TPL_VAR["gMall"]["addressSub"]?> <br>
				<?php echo __('사업자번호')?> : <?php echo $TPL_VAR["gMall"]["businessNo"]?> <?php echo __('통신판매업신고')?> : <?php echo $TPL_VAR["gMall"]["onlineOrderSerial"]?> <br>
				<?php echo __('대표번호')?> : <?php echo $TPL_VAR["gMall"]["phone"]?> <?php echo __('이메일')?> : <?php echo $TPL_VAR["gMall"]["email"]?><br>
				<?php echo __('호스팅제공 : 엔에이치엔고도(주)')?>

			</p>
			<!-- //ft_address -->
			<!--
			<ul class="ft_sns_btn">
				<li><a href="#"><img src="/data/skin/mobile/aileenwedding/img/icon/icon_facebook.png" alt="FACEBOOK"></a></li>
				<li><a href="#"><img src="/data/skin/mobile/aileenwedding/img/icon/icon_instagram.png" alt="INSTAGRAM"></a></li>
				<li><a href="#"><img src="/data/skin/mobile/aileenwedding/img/icon/icon_blog.png" alt="NAVER BLOG"></a></li>
			</ul>
			-->
			<!-- //ft_sns_btn -->
			<address class="ft_copy">copyrightⓒ2017 <?php echo $TPL_VAR["gMall"]["mallDomain"]?>. All Rights Reserved.<br/></address>
			<div class="shortcut">
				<a href="#footer" class="js_add_favorite" data-title="<?php echo $TPL_VAR["gMall"]["mallNm"]?>" style="display:none;">
					<span style="background-image:url(<?php echo $TPL_VAR["gMobile"]["mobileShopIcon"]?>);"><em><?php echo $TPL_VAR["gMall"]["mallNm"]?></em> <?php echo __('바로가기 아이콘 추가하기')?></span>
					<iframe id="ifrmAddFavoriteLauncher" src="/blank.php" style="display:none" width="0" height="0"></iframe>
				</a>
			</div>
		</div>
		<!-- //ft_info2_box -->
	</footer>
</div>
<a href="#" class="fixed_btn_top" style="display:none;"><span>TOP</span></a>
<!-- //footer -->