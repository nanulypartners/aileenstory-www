<?php /* Template_ 2.2.7 2020/05/08 02:01:51 /www/aileen8919_godomall_com/data/skin/mobile/aileenwedding/outline/header/standard.html 000006920 */  $this->include_("dataCategoryPosition","includeWidget","dataBanner");
if (is_array($TPL_VAR["defaultBoardList"])) $TPL_defaultBoardList_1=count($TPL_VAR["defaultBoardList"]); else if (is_object($TPL_VAR["defaultBoardList"]) && in_array("Countable", class_implements($TPL_VAR["defaultBoardList"]))) $TPL_defaultBoardList_1=$TPL_VAR["defaultBoardList"]->count();else $TPL_defaultBoardList_1=0;?>
<div id="header_wrap">
	<header>
		<div class="header_box">
			<div class="h_logo"><a href="/"><img src="/data/skin/mobile/aileenwedding/img/common/logo.png" alt="" /></a></div> <!-- 로고 -->
			<!-- 좌측 메뉴 -->
			<ul class="left_menu">
				<li><a href="#" class="side_menu"><img src="/data/skin/mobile/aileenwedding/img/common/icon_menu.png" alt="" /></a></li>
			</ul>
			<!-- 우측 메뉴 -->
			<ul class="right_menu">
				<li><a href="../mypage/index.php" class="mypage"><img src="/data/skin/mobile/aileenwedding/img/common/icon_mypage.png" alt="" /></a></li>
			</ul>
			<!-- 네비게이터 -->
<?php if($TPL_VAR["layout"]["current_page"]=='y'&&$TPL_VAR["layout"]["page_name"]){?>
			<div class="navi_g">
				<ul>
					<li><?php echo __('홈')?></li>
<?php if($TPL_VAR["gCurrentPageName"]=='goods/goods_list'){?>
					<li><?php echo dataCategoryPosition($TPL_VAR["cateCd"],'</li>
					<li>',$TPL_VAR["cateType"])?></li>
<?php }else{?>
					<li><?php echo $TPL_VAR["layout"]["page_name"]?></li>
<?php }?>
				</ul>
			</div>
<?php }?>
<?php if($TPL_VAR["gCurrentPageName"]!='main/index'){?>
			<div class="sub_top">
				<div class="sub_top_left">
<?php if($TPL_VAR["gReturnUrl"]){?>
					<a href="<?php echo $TPL_VAR["gReturnUrl"]?>" class="all_page_prev"><?php echo __('이전')?></a>
<?php }else{?>
					<a href="<?php echo $TPL_VAR["gReferer"]?>" class="all_page_prev js_bn_back"><?php echo __('이전')?></a>
<?php }?>
				</div>
				<h2><a href="#" class="js_page_reload "><?php if($TPL_VAR["gPageName"]){?><?php echo $TPL_VAR["gPageName"]?><?php }else{?><?php echo $TPL_VAR["layout"]["page_name"]?><?php }?></a></h2>
<?php if(($TPL_VAR["gCurrentPageName"]=='board/list'||$TPL_VAR["gCurrentPageName"]=='mypage/mypage_qa'||$TPL_VAR["gCurrentPageName"]=='mypage/mypage_goods_qa'||$TPL_VAR["gCurrentPageName"]=='mypage/mypage_goods_review')&&$TPL_VAR["bdList"]["cfg"]["auth"]["write"]=='y'){?>
				<div class="sub_top_right2"><a href="../board/write.php?bdId=<?php echo $TPL_VAR["req"]["bdId"]?>&mypageFl=<?php echo $TPL_VAR["req"]["mypageFl"]?>&goodsNo=<?php echo $TPL_VAR["goodsView"]["goodsNo"]?>&gboard=r&noheader=y" data-toggle="modal" data-target="#popupBoard" class="board_btn"><?php echo __('글쓰기')?></a></div>
<?php }?>
<?php if($TPL_VAR["gCurrentPageName"]=='board/view'){?>
				<div class="sub_top_right2"><a href="../board/list.php?bdId=<?php echo $TPL_VAR["req"]["bdId"]?>&mypageFl=<?php echo $TPL_VAR["req"]["mypageFl"]?>" class="board_btn"><?php echo __('목록가기')?></a></div>
<?php }?>
<?php if($TPL_VAR["gCurrentPageName"]=='board/plus_review_article'||$TPL_VAR["gCurrentPageName"]=='board/plus_review_photo'||$TPL_VAR["gCurrentPageName"]=='board/plus_review_goods'){?>
				<div class="sub_top_right2 btn_plus_review_register"><a href="../board/plus_review_popup_write.php?mode=addPopup" class="ogl_reviewrite" data-toggle="modal" data-target="#popupBoard"><?php echo __('리뷰등록')?></a></div>
<?php }?>
			</div>
<?php }?>
		</div>
	</header>
</div>
<!-- //header_wrap -->
<nav>
	<div class="bg"></div>
	<div class="left_close"></div>
	<div class="nav_bg_box">
		<div class="nav_box">
			<div class="nav_iscroll_box">
				<div class="nav_content_box">
					<div class="nav_language">
						<?php echo includeWidget('proc/_global_select.html')?>

					</div>
					<!-- //nav_language -->
					<div class="nav_banner">
<?php if((is_array($TPL_R1=dataBanner('569534203'))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?><?php echo $TPL_V1["tag"]?><?php }}?>
						<div class="nav_login">
							<ul>
<?php if(gd_is_login()===false){?>
								<li><a href="../member/login.php"><?php echo __('로그인')?></a></li>
								<li><a href="../member/join_method.php"><?php echo __('회원가입')?></a></li>
<?php }else{?>
								<li><a href="../member/logout.php"><?php echo __('로그아웃')?></a></li>
								<li><a href="../mypage/index.php"><?php echo __('마이페이지')?></a></li>
<?php }?>
							</ul>
						</div>
<?php if(gd_is_login()===false){?>
						<div class="left_logo"><?php if((is_array($TPL_R1=dataBanner('1865762709'))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?><?php echo $TPL_V1["tag"]?><?php }}?></div>
<?php }else{?>
						<div class="login_txt">
							<?php echo __('%s님%s방문을 환영합니다.','<p class="name"><strong>'.gd_get_login_name().'</strong>','</p>')?>

						</div>
<?php }?>
					</div>
					<!-- //nav_banner -->
					<div class="nav_link">
						<ul>
							<li><a href="../board/list.php?bdId=goodsqa"><?php echo __('Q&A')?></a></li>
							<li><a href="../goods/goods_today.php"><?php echo __('최근본상품')?></a></li>
							<li><a href="../mypage/wish_list.php"><?php echo __('찜리스트')?></a></li>
						</ul>
					</div>
					<!-- //nav_link -->
					<div class="nav_category">
						<div class="nav_tabmenu_box">
							<ul class="nav_tabmenu">
								<li class="on"><span>CATEGORY</span></li>
								<li><span>BRAND</span></li>
							</ul>
							<div class="tab_menu0">
								<div class="prd_cate">
									<?php echo includeWidget("proc/category_all.html",'cateType','category')?>

								</div>
								<!-- //prd_cate -->
							</div>
							<!-- //tab_menu0 -->
							<div class="tab_menu1" style="display:none;">
								<a href="../goods/brand.php" class="btn_nav_brand_search"><?php echo __('브랜드 검색')?></a>
								<div class="brand_cate">
									<?php echo includeWidget("proc/category_all.html",'cateType','brand')?>

								</div>
								<!-- //brand_cate -->
							</div>
							<!-- //tab_menu1 -->
							<div class="nav_community_box">
								<p class="comm_tit">COMMUNITY</p>
								<ul class="board_cate">
<?php if($TPL_defaultBoardList_1){foreach($TPL_VAR["defaultBoardList"] as $TPL_K1=>$TPL_V1){?>
									<li><a href="../board/list.php?bdId=<?php echo $TPL_K1?>"><?php echo __($TPL_V1)?></a></li>
<?php }}?>
									<li><a href="../service/faq_list.php"><?php echo __('FAQ')?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- //nav_content_box -->
			</div>
		</div>
		<!-- //nav_box -->
	</div>
	<!-- //nav_bg_box -->
</nav>
<div id="container">
	<div id="contents_wrap">