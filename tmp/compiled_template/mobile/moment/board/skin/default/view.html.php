<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/board/skin/default/view.html 000012741 */ ?>
<div class="view_default">
	<div class="goods_area">
		<h3>
<?php if($TPL_VAR["bdView"]["cfg"]["bdCategoryFl"]=='y'&&$TPL_VAR["bdView"]["data"]["category"]){?>
			[<?php echo $TPL_VAR["bdView"]["data"]["category"]?>]
<?php }?>
			<?php echo $TPL_VAR["bdView"]["data"]["subject"]?>

		</h3>
		<div id="boardContent" class="view_wrap">
			<div class="view_box">
				<div class="view_info_box">
					<div class="view_info">
						<span class="writer"><?php echo $TPL_VAR["bdView"]["data"]["writer"]?></span>
						<span class="date"><?php echo $TPL_VAR["bdView"]["data"]["regDt"]?></span>
<?php if($TPL_VAR["bdView"]["cfg"]["bdIpFl"]=='y'&&$TPL_VAR["bdView"]["data"]["writerIp"]){?>
						<span class="ip"><?php echo $TPL_VAR["bdView"]["data"]["writerIp"]?></span>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdReplyStatusFl"]=='y'){?>
						<div><?php echo $TPL_VAR["bdView"]["data"]["replyStatusText"]?></div>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdGoodsPtFl"]=='y'){?>
						<div class="rating"><span style="width:<?php echo $TPL_VAR["bdView"]["data"]["goodsPt"]* 20?>%;"><?php echo __('별')?></span></div>
<?php }?>
					</div>
<?php if($TPL_VAR["bdView"]["data"]["isFile"]=='y'){?>
					<div class="view_info">
						<span><img src="/data/skin/mobile/moment/board/skin/default/img/etc/icon-clip.png" alt="<?php echo __('첨부파일')?>"/></span>
						<strong><?php echo __('첨부파일')?></strong>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["uploadedFile"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
						<a href="./download.php?bdId=<?php echo $TPL_VAR["bdView"]["cfg"]["bdId"]?>&sno=<?php echo $TPL_VAR["req"]["sno"]?>&fid=<?php echo $TPL_V1["fid"]?>">&nbsp;&nbsp;<?php echo $TPL_V1["name"]?></a>
<?php }}?>
					</div>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdLinkFl"]=='y'&&$TPL_VAR["bdView"]["data"]["urlLink"]){?>
					<div class="view_info">
						<span class="urlLink">Link&nbsp;:&nbsp;<a href="<?php echo gd_isset($TPL_VAR["bdView"]["data"]["urlLink"])?>" target="_blank"><?php echo $TPL_VAR["bdView"]["data"]["urlLink"]?></a></span>
					</div>
<?php }?>
				</div>
				<div class="view_body">
<?php if($TPL_VAR["bdView"]["data"]["isViewGoodsInfo"]=='y'){?>
					<div class="selected_goods_wrap">
						<div class="selected_goods">
							<div class="goods_item_cell cell_img">
								<div class="goods_img">
									<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdView"]["data"]["goodsNo"]?>" target="_blank"><img src="<?php echo $TPL_VAR["bdView"]["data"]["viewListImage"]?>" alt=""></a>
								</div>
							</div>
							<div class="goods_item_cell cell_info">
								<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdView"]["data"]["goodsNo"]?>" target="_blank"><?php echo $TPL_VAR["bdView"]["data"]["goodsData"]["goodsNm"]?></a>
							</div>
							<div class="goods_item_cell cell_price">
<?php if($TPL_VAR["bdView"]["data"]["goodsData"]["goodsPriceString"]){?>
								<?php echo $TPL_VAR["bdView"]["data"]["goodsData"]["goodsPriceString"]?>

<?php }else{?>
								<?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_VAR["bdView"]["data"]["goodsData"]["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php }?>
							</div>

						</div>
					</div>
<?php }?>
<?php if($TPL_VAR["bdView"]["data"]["isViewOrderInfo"]=='y'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["extraData"]["arrOrderGoodsData"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
					<div class="selected_goods_wrap">
						<div class="selected_goods">
							<div class="goods_item_cell cell_img">
								<div class="goods_img">
									<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>" target="_blank"><img src="<?php echo $TPL_V1["goodsImageSrc"]?>" alt=""></a>
								</div>
							</div>
							<div class="goods_item_cell cell_info">
								<strong class="goods_titlename"><?php echo $TPL_V1["goodsNm"]?></strong>
								<span class="goods_optionname"> <?php echo $TPL_V1["optionName"]?></span>
							</div>
							<div class="goods_item_cell cell_price">
								<?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V1["totalGoodsPrice"])?><?php echo gd_global_currency_string()?>

							</div>
						</div>
					</div>
<?php }}?>
<?php }?>
				</div>
				<div class="contents_holder"><?php echo $TPL_VAR["bdView"]["data"]["workedContents"]?></div>
			</div>
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'||$TPL_VAR["bdView"]["cfg"]["bdRecommendFl"]=='y'){?>
			<div class="view_comment">
				<div class="comment_count">
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
					<span><?php echo __('%s개의 댓글이 있습니다.','<strong>'.$TPL_VAR["bdView"]["data"]["memoCnt"].'</strong>')?></span>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdRecommendFl"]=='y'){?>
					<span id="recommendCount"><?php echo __('추천')?> : <strong><?php echo $TPL_VAR["bdView"]["data"]["recommend"]?></strong></span>
					<div class="btn_like"><a href="javascript:gd_recommend('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>)" class="board_like_btn"><?php echo __('추천하기')?></a></div>
<?php }?>
				</div>
			</div>
<?php }?>
<?php if($TPL_VAR["bdView"]["cfg"]["bdMemoFl"]=='y'){?>
			<div class="comment_wrap js_comment_area" data-bdId="<?php echo $TPL_VAR["req"]["bdId"]?>" data-sno="<?php echo $TPL_VAR["req"]["sno"]?>">
<?php if((is_array($TPL_R1=$TPL_VAR["bdView"]["data"]["memoList"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
				<div class="js_data_comment_row" data-memosno="<?php echo $TPL_V1["sno"]?>" data-memoauth="<?php echo $TPL_V1["auth"]?>">
					<div class="comment_info">
						<div <?php if($TPL_V1["groupThread"]){?>class="rereply_row"<?php }?>>
						<span class="writer"><?php echo $TPL_V1["writer"]?></span>
						<span class="date"><?php echo $TPL_V1["regDt"]?></span>
					</div>
					<div class="contents_holder">
<?php if($TPL_VAR["bdView"]["data"]["allReplyShow"]=='y'){?>
					<?php echo $TPL_V1["workedMemo"]?>

<?php }else{?>
<?php if($TPL_V1["isSecretReply"]=='y'){?>
<?php if($TPL_V1["isShowSecretReply"]=='y'){?>
					<?php echo $TPL_V1["workedMemo"]?>

<?php }else{?>
					<input type="hidden" name="secretReply" value="y">
						<em><img src="<?php echo $TPL_VAR["bdView"]["cfg"]["iconImageMobile"]["secret"]["url"]?>"><a href="#none" class="js_comment_btn_secret"><?php echo $TPL_VAR["secretReplyCheck"]["secretReplyTitle"]?></a></em>
<?php }?>
<?php }else{?>
					<?php echo $TPL_V1["workedMemo"]?>

<?php }?>
<?php }?>
					</div>
					<div class="comment_options">
<?php if(!$TPL_V1["groupThread"]){?>
						<button type="button" class="comment_reply_btn js_comment_btn_reply"><?php echo __('댓글에 댓글달기')?></button>
<?php }?>
<?php switch($TPL_V1["auth"]){case 'y':case 'c':?>
						<button type="button" class="comment_modify_btn js_comment_btn_modify" title="<?php echo __('댓글 수정')?>"><?php echo __('댓글 수정')?></button>
						<button type="button" class="comment_del_btn js_comment_btn_delete" title="<?php echo __('댓글 삭제')?>"><?php echo __('댓글 삭제')?></button>
<?php }?>
					</div>
				</div>
				<div class="reply_form js_action_form" style="display:none">
<?php if(!gd_is_login()){?>
					<dl class="b_guest_info">
						<dd><input type="text" name="writerNm" placeholder="<?php echo __('이름')?>"/></dd>
						<dd><input type="password" name="password" placeholder="<?php echo __('비밀번호')?>"/></dd>
					</dl>
					<dl class="b_guest">
						<dt>
							<span class="inp_chk">
								<input type="checkbox" id="infoCollectionAgreeAction<?php echo $TPL_V1["sno"]?>" data-dpmaxz-eid="11">
								<label for="infoCollectionAgreeAction<?php echo $TPL_V1["sno"]?>"><strong>(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></strong>
								</label>
								<a href="javascript:void(0)" onclick="gd_redirect_collection_agree()" target="_blank" class="c_red"><?php echo __('전체보기')?>&gt;</a>
							</span>
						</dt>
						<dd><div class="textarea_box"><textarea rows="3"><?php echo $TPL_VAR["bdView"]["private"]?></textarea></div></dd>
					</dl>
<?php }?>
					<span class="inp_chk">
					<?php echo $TPL_VAR["secretReplyCheck"]["replyModify"]?>

					</span>
					<div class="b_memo">
						<div class="comment_input"><textarea class="memo" name="memo" placeholder="<?php echo __('댓글을 작성하세요.')?>"></textarea></div>
						<div class="comment_btn"><button type="button" class="view_comment_btn js_comment_btn_action"><?php echo __('작성 완료')?></button></div>
					</div>
				</div>
			</div>
<?php }}?>

<?php if($TPL_VAR["bdView"]["cfg"]["auth"]["memo"]=='y'){?>
			<div class="reply_form js_form_write">
<?php if(!gd_is_login()){?>
				<dl class="b_guest_info">
					<dd><input type="text" name="writerNm" placeholder="<?php echo __('이름')?>"/></dd>
					<dd><input type="password" name="password" placeholder="<?php echo __('비밀번호')?>"/></dd>
				</dl>
				<dl class="b_guest">
					<dt>
						<span class="inp_chk">
							<input type="checkbox" id="infoCollectionAgreeWrite" data-dpmaxz-eid="11">
							<label for="infoCollectionAgreeWrite"><strong>(<?php echo __('비회원')?>) <?php echo __('개인정보 수집항목 동의')?></strong>
							</label>
							<a href="javascript:void(0)" onclick="gd_redirect_collection_agree()" class="c_red"><?php echo __('전체보기')?>&gt;</a>
						</span>
					</dt>
					<dd><div class="textarea_box"><textarea rows="3"><?php echo $TPL_VAR["bdView"]["private"]?></textarea></div></dd>
				</dl>
<?php }?>
				<span class="inp_chk">
				<?php echo $TPL_VAR["secretReplyCheck"]["replyWrite"]?>

				</span>
				<div class="b_memo">
					<div class="comment_input"><textarea class="memo" name="memo" placeholder="<?php echo __('댓글을 작성하세요.')?>"></textarea></div>
					<div class="comment_btn"><button type="button" class="view_comment_btn js_comment_btn_write"><?php echo __('작성 완료')?></button></div>
				</div>
			</div>
<?php }else{?>
			<div class="reply_form">
<?php if(gd_is_login()){?>
				<p class="no_data"><?php echo __('댓글 권한이 없습니다.')?></p>
<?php }else{?>
				<p class="no_data"><?php echo __('로그인을 하셔야 댓글을 등록하실 수 있습니다.')?></p>
<?php }?>
			</div>
<?php }?>
<?php }?>
		</div>
		<div class="btn_wish_bx">
			<ul class="btn_bx">
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["reply"]){case 'y':case 'c':?>
				<li><button type="button" class="view_reply_btn" onclick="gd_btn_reply_write('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>)"><?php echo __('답글')?></button></li>
<?php }?>
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["modify"]){case 'y':case 'c':?>
				<li><button type="button" class="view_modify_btn" onclick="gd_btn_modify_write('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>,'<?php echo $TPL_VAR["bdView"]["data"]["auth"]["modify"]?>')"><?php echo __('수정')?></button></li>
<?php }?>
<?php switch($TPL_VAR["bdView"]["data"]["auth"]["delete"]){case 'y':case 'c':?>
				<li><button type="button" class="view_del_btn" onclick="gd_btn_delete('<?php echo $TPL_VAR["req"]["bdId"]?>',<?php echo $TPL_VAR["req"]["sno"]?>,'<?php echo $TPL_VAR["bdView"]["data"]["auth"]["delete"]?>')"><?php echo __('삭제')?></button></li>
<?php }?>
			</ul>
		</div>
	</div>
	<!-- 레이어 호출시 딤처리 -->
	<div id="layerDim" class="dn">&nbsp;</div>
	<!-- //레이어 호출시 딤처리 -->
	<!-- 비밀글 클릭시 인증 레이어 -->
	<div class="cite_layer dn js_password_layer">
		<div class="wrap">
			<h4><?php echo __('비밀번호 인증')?></h4>
			<div>
				<p><?php echo __('글 작성시 설정한 비밀번호를 입력해 주세요.')?></p>
				<input type="password" name="writerPw" >
				<div class="btn_box"><a href="javascript:void(0)" class="layer_close_btn js_submit ly_pwok_btn"><?php echo __('확인')?></a></div>
			</div>
			<button type="button" class="close" title="<?php echo __('닫기')?>"><?php echo __('닫기')?></button>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo PATH_MOBILE_SKIN?>js/gd_board_view.js" charset="utf-8"></script>