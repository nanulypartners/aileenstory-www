<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/board/skin/qa/write.html 000017652 */ 
if (is_array($TPL_VAR["emailDomain"])) $TPL_emailDomain_1=count($TPL_VAR["emailDomain"]); else if (is_object($TPL_VAR["emailDomain"]) && in_array("Countable", class_implements($TPL_VAR["emailDomain"]))) $TPL_emailDomain_1=$TPL_VAR["emailDomain"]->count();else $TPL_emailDomain_1=0;?>
<script type="text/javascript">
    var cfgEditorFl = 'n';
    var cfgUploadFl = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdUploadFl"]?>';
    var maxFileNumber = 10;
    var mode = '<?php echo $TPL_VAR["req"]["mode"]?>';
    var mobileSkinPath = '\<?php echo PATH_MOBILE_SKIN?>';
    var prvFilePath = [];
    var prvFileName = [];
<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["saveHttpUrl"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
    prvFilePath.push("<?php echo $TPL_V1?>");
<?php }}?>
<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["uploadFileNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
    prvFileName.push("<?php echo $TPL_V1?>");
<?php }}?>
    var bdId = '<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdId"]?>';
    var bdSno = '<?php echo gd_isset($TPL_VAR["req"]["sno"])?>';
</script>
<script type="text/javascript" src="<?php echo PATH_MOBILE_SKIN?>js/gd_board_write.js" charset="utf-8"></script>
<div class="write_qa">
	<div class="ly_head">
		<h1 class="h_tit"><?php echo __('글쓰기')?></h1>
	</div>
	<div id="boardregister" class="ly_ct">
		<form name="frmWrite" id="frmWrite" action="../board/board_ps.php" method="post" enctype="multipart/form-data" class="frmWrite">
		<input type="hidden" name="gboard" value="<?php echo $TPL_VAR["req"]["gboard"]?>">
		<input type="hidden" name="bdId" value="<?php echo $TPL_VAR["bdWrite"]["cfg"]["bdId"]?>">
		<input type="hidden" name="sno" value="<?php echo gd_isset($TPL_VAR["req"]["sno"])?>">
		<input type="hidden" name="mode" value="<?php echo gd_isset($TPL_VAR["req"]["mode"])?>">
		<input type="hidden" name="goodsNo" value="<?php echo $TPL_VAR["req"]["goodsNo"]?>">
		<input type="hidden" name="returnUrl" value="<?php echo $TPL_VAR["queryString"]?>">
<?php if($TPL_VAR["req"]["orderGoodsNo"]){?>
        <input type="hidden" name="orderGoodsNo" value="<?php echo $TPL_VAR["req"]["orderGoodsNo"]?>">
<?php }?>
<?php if($TPL_VAR["oldPassword"]){?><input type="hidden" name="oldPassword" value="<?php echo $TPL_VAR["oldPassword"]?>"><?php }?>
		<div class="write_wrap">
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdCategoryFl"]=='y'){?>
			<dl>
				<dt><label for="category"><?php echo __('말머리')?></label></dt>
				<dd>
					<div class="inp_sel"><?php echo gd_select_box('category','category',$TPL_VAR["bdWrite"]["cfg"]["arrCategory"],null,gd_isset($TPL_VAR["bdWrite"]["data"]["category"]),$TPL_VAR["bdWrite"]["cfg"]["bdCategoryTitle"],null,'tune select-small')?> </div>
				</dd>
			</dl>
<?php }?>
			<dl>
				<dt><label for="writerNm"><?php echo __('이름')?></label></dt>
				<dd>
<?php if($TPL_VAR["req"]["mode"]=='modify'||gd_is_login()){?>
					<input type="text" id="writerNm" name="writerNm" placeholder="<?php echo __('이름을 입력해주세요.')?>" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerNm"])?>">
<?php }else{?>
					<input type="text" id="writerNm" name="writerNm" placeholder="<?php echo __('이름을 입력해주세요.')?>">
<?php }?>
				</dd>
			</dl>
<?php if(!gd_isset($TPL_VAR["bdWrite"]["data"]["memNo"])&&!$TPL_VAR["oldPassword"]){?>
			<dl>
				<dt><label for="writerPw"><?php echo __('비밀번호')?></label></dt>
				<dd><input type="password" id="writerPw" name="writerPw" placeholder="<?php echo __('비밀번호를 입력해주세요.')?>"></dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["data"]["canWriteGoodsSelect"]=='y'){?>
			<dl>
				<dt><label><?php echo __('상품선택')?></label></dt>
				<dd>
					<div class="itemselect">
<?php if(!$TPL_VAR["bdWrite"]["data"]["goodsNo"]){?>
						<div class="itemselect_comment"><?php echo __('선택된 상품이 없습니다.')?></div>
<?php }?>
						<div class="item_select_btn_box"><a href="javascript:void(0)" class="js_board_layer prd_select_btn" data-type="goods"><?php echo __('상품선택')?></a></div>
					</div>
				</dd>
			</dl>
			<div id="selectGoodsBox">
<?php if($TPL_VAR["bdWrite"]["data"]["goodsNo"]){?>
				<div class="selected_goods_wrap">
					<div class="selected_goods js_select_item">
						<input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["bdWrite"]["data"]["goodsNo"]?>">
						<div class="goods_item_cell cell_img">
							<div class="goods_img">
								<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_VAR["bdWrite"]["data"]["goodsNo"]?>" target="_blank">
									<img src="<?php echo $TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsImageSrc"]?>" alt="">
								</a>
							</div>
						</div>
						<div class="goods_item_cell cell_info">
							<?php echo $TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsNm"]?>

						</div>
						<div class="goods_item_cell cell_price">
							<?php echo gd_currency_display($TPL_VAR["bdWrite"]["data"]["goodsData"]["goodsPrice"])?>

						</div>
						<div class="goods_item_cell cell_delete">
							<button type="button" class="item_remove js_select_remove">
								<img src="<?php echo PATH_MOBILE_SKIN?>img/btn/btn_x.png" alt="">
							</button>
						</div>
					</div>
				</div>
<?php }?>
			</div>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["data"]["canWriteOrderSelect"]=='y'){?>
			<dl>
				<dt><label><?php echo __('주문내역')?></label></dt>
				<dd>
					<div class="itemselect">
<?php if(!$TPL_VAR["bdWrite"]["data"]["extraData"]["arrOrderGoodsData"]){?>
						<div class="itemselect_comment"><?php echo __('선택된 주문이 없습니다.')?></div>
<?php }?>
						<div class="item_select_btn_box"><a href="javascript:void(0)" class="js_board_layer prd_select_btn" data-type="order"><?php echo __('주문선택')?></a></div>
					</div>
				</dd>
			</dl>
			<div id="selectOrderBox">
<?php if($TPL_VAR["bdWrite"]["data"]["extraData"]["arrOrderGoodsData"]){?>
				<div class="selected_goods_wrap">
<?php if((is_array($TPL_R1=$TPL_VAR["bdWrite"]["data"]["extraData"]["arrOrderGoodsData"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
					<div class="selected_goods js_select_item">
						<input type="hidden" name="orderGoodsNo[]" value="<?php echo $TPL_V1["sno"]?>">
						<div class="goods_item_cell cell_img">
							<div class="goods_img">
								<img src="<?php echo $TPL_V1["goodsImageSrc"]?>" alt="">
							</div>
						</div>
						<div class="goods_item_cell cell_info">
							<?php echo $TPL_V1["goodsNm"]?><br>
							<?php echo $TPL_V1["optionName"]?>

						</div>
						<div class="goods_item_cell cell_price">
							<?php echo gd_currency_display($TPL_V1["totalGoodsPrice"])?>

						</div>
						<div class="goods_item_cell cell_delete">
							<button type="button" class="item_remove js_select_remove">
								<img src="<?php echo PATH_MOBILE_SKIN?>img/btn/btn_x.png" alt="">
							</button>
						</div>
					</div>
<?php }}?>
				</div>
<?php }?>
			</div>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdGoodsPtFl"]=='y'){?>
			<dl>
				<dt><label><?php echo __('평가')?></label></dt>
				<dd>
					<input type="hidden" name="goodsPt" value="<?php echo $TPL_VAR["bdWrite"]["data"]["goodsPt"]?>">
					<div class="select_small_outer choice_box">
						<!-- [D] 활성화 시  class="selected" 추가 -->
						<button type="button" class="bn_opt js_option_select">
							<span><span></span></span>
						</button>
						<div class="optbx" style="display:block;display:none">
							<!-- [D] 3행의 높이값 구하여 height적용 -->
							<div class="scroll">
								<ul class="choice_rating">
<?php if((is_array($TPL_R1=range( 5, 1))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
									<li><span class="rating" data-value="<?php echo $TPL_V1?>"><span style="width:<?php echo $TPL_V1* 20?>%;"><?php echo __('별')?><?php echo $TPL_V1?></span></span></li>
<?php }}?>
								</ul>
							</div>
						</div>
					</div>
				</dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdMobileFl"]=='y'){?>
			<dl>
				<dt><label for="writerMobile"><?php echo __('휴대폰')?></label></dt>
				<dd><input type="number" name="writerMobile" id="writerMobile" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["writerMobile"])?>" placeholder="<?php echo __('- 없이 입력하세요')?>" autocomplete="off"></dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdEmailFl"]=='y'){?>
			<dl>
				<dt><label for="writerEmail"><?php echo __('이메일')?></label></dt>
				<dd>
					<input type="text" class="board_input_email" name="writerEmail" id="writerEmail">
					<div class="board_email_select_box">
						<select id="emailDomain" name="emailDomain" class="board_email_select">
<?php if($TPL_emailDomain_1){foreach($TPL_VAR["emailDomain"] as $TPL_K1=>$TPL_V1){?>
							<option value="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></option>
<?php }}?>
						</select>
					</div>
				</dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdLinkFl"]=='y'){?>
			<dl>
				<dt><label for="urlLink"><?php echo __('외부링크')?></label></dt>
				<dd><input type="text" name="urlLink" id="urlLink" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["urlLink"])?>" placeholder="<?php echo __('외부링크')?>"></dd>
			</dl>
<?php }?>
			<dl>
				<dt><label for="subject"><?php echo __('제목')?></label></dt>
				<dd><input type="text" name="subject" id="subject" value="<?php echo gd_isset($TPL_VAR["bdWrite"]["data"]["subject"])?>" placeholder="<?php echo __('제목을 입력하세요')?>"></dd>
			</dl>
			<dl class="b_content">
				<dt>
					<label for="contents"><?php echo __('내용')?></label>
					<div class="b_content_chk">
						<span class="inp_chk">
<?php switch($TPL_VAR["bdWrite"]["cfg"]["bdSecretFl"]){case '1':?>
							<input type="checkbox" id="secret" name="isSecret" value="y" checked>
							<label for="secret"><?php echo __('비공개')?></label>
<?php break;case '2':?>
<?php break;case '3':?>
							<img src="<?php echo $TPL_VAR["bdWrite"]["cfg"]["iconImageMobile"]["secret"]["url"]?>">
							<label><?php echo __('비공개')?></label>
<?php break;default:?>
<?php if($TPL_VAR["req"]["mode"]=='reply'&&gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>    <!--// 비밀글 설정 - 부모글이 비밀글인 답글 작성시 무조건 비밀글-->
							<input type="hidden" name="isSecret" value="y"/> <?php echo __('해당글은 비밀글로만 작성이 됩니다.')?>

<?php }else{?>
							<input type="checkbox" name="isSecret" value="y" id="secret" <?php if(gd_isset($TPL_VAR["bdWrite"]["data"]["isSecret"])=='y'){?>checked<?php }?> >
							<label for="secret"><?php echo __('비공개')?></label>
<?php }?>
<?php }?>
						</span>
					</div>
				</dt>
				<dd>
					<div class="textarea_box"><textarea class="textarea" name="contents" id="contents" placeholder="(<?php echo __('%s자 이상 입력해주세요.','5')?>)"><?php echo $TPL_VAR["bdWrite"]["data"]["contents"]?></textarea></div>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdUploadFl"]=='y'){?>
					<div class="b_file">
						<ul id="boardAttach">
							<li class="item template">
								<button class="file_face" type="button"><?php echo __('파일첨부')?></button>
								<input class="file_hidden" type="file" name="upfiles[]" accept="image/*" />
							</li>
						</ul>
					</div>
<?php }?>
				</dd>
			</dl>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdSpamBoardFl"]& 2){?>
			<div class="input_wrap">
				<div class="capcha">
					<div class="capcha_img"><span><img src="../board/captcha.php" id="captchaImg"/></span></div>
					<div class="capcha_body">
						<p class="capcha_text"><?php echo __('보이는 순서대로 %s숫자 및 문자를 모두 입력해 주세요.','<br />')?></p>
						<div class="capcha_input">
							<input type="text" name="captchaKey" maxlength="5" onKeyUp="this.value=this.value.toUpperCase();" onfocus="this.select()" placeholder="<?php echo __('문자를 입력해주세요.')?>" class="captcha">
						</div>
						<div class="capcha_button"><button type="button" onclick="gd_captcha_reload()" class="captchareload_btn"><img src="/data/skin/mobile/moment/board/skin/qa/img/etc/icon-reset.png" alt=""> <?php echo __('이미지 새로고침')?></button></div>
					</div>
				</div>
			</div>
<?php }?>
<?php if($TPL_VAR["bdWrite"]["cfg"]["bdPrivateYN"]=="y"){?>
				<dl class="b_guest">
					<dt>
						<span class="inp_chk">
							<input type="checkbox" name="private" value="y" id="acceptTerms">
							<label for="acceptTerms" class="check_s"><strong><?php echo __('비회원 글작성에 대한 개인정보 수집 및 이용동의')?></strong>
							</label>
							<a href="../service/private.php" target="_blank" class="c_red"><?php echo __('전체보기')?>&gt;</a>
						</span>
					</dt>
					<dd><div class="textarea_box"><textarea class="policyCollectionTextarea"><?php echo $TPL_VAR["bdWrite"]["private"]?></textarea></div></dd>
				</dl>
<?php }?>
				<ul class="btn_box">
					<li><button type="button" class="write_cancel_btn ly_btn_close" onclick="history.back();"><?php echo __('취 소')?></button></li>
					<li><button type="submit" class="write_save_btn"><?php echo __('저 장')?></button></li>
				</ul>
			</div>
		</form>
	</div>
	<div class="close_btn">
		<button type="button" class="lys_close_btn ly_btn_close"><?php echo __('닫기')?></button>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
		gd_select_email_domain('writerEmail');
		$('#popupBoard .ly_ct').height('auto');

        if (cfgUploadFl == 'y') {
			gd_init_file_upload();
        }

        // 평가 설정
        $(".js_option_select").on('click', function(e){
            if($(this).next('div').is(':hidden')) {
                $(this).addClass("selected");
                $(this).next('div.optbx').show();

            } else {
                $(this).removeClass("selected");
                $(this).next('div.optbx').hide();

            }
        });

        $(".choice_rating li").on('click', function(){
            $(".js_option_select").removeClass("selected");
            $(".js_option_select").next('div.optbx').hide();

            $(".select_small_outer > button > span").addClass('rating');
            $(".js_option_select > .rating > span").css('width', $(this).children().children().css('width'));
            $(".js_option_select > .rating > span").css('height', '15');
            $('input[name="goodsPt"]').val($(this).children().data('value'));
        });

        if ($('input[name="goodsPt"]').val() > 0) {
            $(".select_small_outer > button > span").addClass('rating');
            $(".js_option_select > .rating > span").css('width', $('input[name="goodsPt"]').val() * 20 + '%');
            $(".js_option_select > .rating > span").css('height', '15');
        }

        if ($('input[name="gboard"]').val() == 'r') {
            $('#frmWrite').attr('target', 'ifrmProcess');
        }

        // 글쓰기 모달창 닫을시 패스워드 레이어도 닫음
        $(".bn_cls.ly_btn_close").on('click',function (e){
            if ($('.js_password_layer').length > 0) {
                passwordLayer.close();
            }
        });
    });
</script>
<script id="selectGoodsTblTr" type="text/html">
    <div class="selected_goods_wrap">
        <div class="selected_goods js_select_item">
            <input type="hidden" name="goodsNo[]" value="<%=goodsNo%>">
            <div class="goods_item_cell cell_img">
                <div class="goods_img">
                    <img src="<%=goodsImgageSrc%>" alt="">
                </div>
            </div>
            <div class="goods_item_cell cell_info">
                <%=goodsName%>
            </div>
            <div class="goods_item_cell cell_price">
                <%=goodsPrice%>
            </div>
            <div class="goods_item_cell cell_delete">
                <button type="button" class="item_remove js_select_remove">
                    <img src="<?php echo PATH_MOBILE_SKIN?>img/btn/btn_x.png" alt="">
                </button>
            </div>
        </div>
    </div>
</script>
<script id="selectOrderTblTr" type="text/html">
    <div class="selected_goods_wrap">
        <div class="selected_goods js_select_item">
            <input type="hidden" name="orderGoodsNo[]" value="<%=orderGoodsNo%>">
            <div class="goods_item_cell cell_img">
                <div class="goods_img">
                    <img src="<%=goodsImgageSrc%>" alt="">
                </div>
            </div>
            <div class="goods_item_cell cell_info">
                <%=goodsName%><br>
                <%=optionName%>
            </div>
            <div class="goods_item_cell cell_price">
                <%=goodsPrice%>
            </div>
            <div class="goods_item_cell cell_delete">
                <button type="button" class="item_remove js_select_remove">
                    <img src="<?php echo PATH_MOBILE_SKIN?>img/btn/btn_x.png" alt="">
                </button>
            </div>
        </div>
    </div>
</script>