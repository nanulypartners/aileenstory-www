<?php /* Template_ 2.2.7 2020/01/09 17:00:14 /www/aileen8919_godomall_com/data/skin/mobile/moment/event/couponzone.html 000007390 */ 
if (is_array($TPL_VAR["couponList"])) $TPL_couponList_1=count($TPL_VAR["couponList"]); else if (is_object($TPL_VAR["couponList"]) && in_array("Countable", class_implements($TPL_VAR["couponList"]))) $TPL_couponList_1=$TPL_VAR["couponList"]->count();else $TPL_couponList_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="coupon">
    <!-- 20190522 추가 -->
    <!-- 쿠폰존 -->
    <div class="coupon_content">
<?php if($TPL_VAR["couponConfig"]["mobileContents"]){?>
        <div class="cp_zone_wrap">
            <?php echo $TPL_VAR["couponConfig"]["mobileContents"]?>

        </div>
<?php }?>

<?php if($TPL_VAR["couponList"]){?>
<?php if($TPL_couponList_1){$TPL_I1=-1;foreach($TPL_VAR["couponList"] as $TPL_V1){$TPL_I1++;?>
        <div class="cp_list_wrap <?php if($TPL_I1> 0){?>margin-type<?php }?>">
<?php if($TPL_V1["title"]){?><span class="title"><?php echo $TPL_V1["title"]?></span><?php }?>
            <ul class="cp_list">
<?php if((is_array($TPL_R2=$TPL_V1["list"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                <li>
                    <div class="img_cp">
                        <div class="image"><img src="<?php echo $TPL_VAR["couponConfig"]["mobileCouponImagePath"]?>" alt="쿠폰이미지"></div>
                        <span class="text_dis">
                            <span class="dis"><?php echo $TPL_V2["couponBenefit"]?></span>
                            <em><?php echo $TPL_V2["couponKindTypeShort"]?></em>
                        </span>
                    </div>
                    <div class="text_wrap">
                        <div class="text_info">
                            <span class="tit"><?php echo $TPL_V2["couponNm"]?></span>
                            <span class="date"><?php echo $TPL_V2["useEndDate"]?></span>
                        </div>
                    </div>
<?php if(gd_is_login()===false){?>
<?php if($TPL_V2["couponType"]=='발급중'){?>
                    <div class="btn_wrap"><a href="#;" data-coupon-no="<?php echo $TPL_V2["couponNo"]?>" class="cp_down_2 btn_alert_login"><?php echo __('쿠폰 받기')?></a></div>
<?php }else{?>
                    <div class="btn_wrap"><span class="cp_down_4"><?php echo __('발급종료')?></span></div>
<?php }?>
<?php }else{?>
<?php if($TPL_V2["chkMemberCoupon"]=='DUPLICATE_COUPON'){?>
                    <div class="btn_wrap"><span class="cp_down_3"><?php echo __('발급완료')?></span></div>
<?php }elseif($TPL_V2["chkMemberCoupon"]=='MAX_COUPON'){?>
                    <div class="btn_wrap"><span class="cp_down_4"><?php echo __('발급종료')?></span></div>
<?php }else{?>
<?php if($TPL_V2["couponType"]=='발급중'){?>
                    <div class="btn_wrap"><a href="#;" data-coupon-no="<?php echo $TPL_V2["couponNo"]?>" class="cp_down_2 btn_coupon_down"><?php echo __('쿠폰 받기')?></a></div>
<?php }else{?>
                    <div class="btn_wrap"><span class="cp_down_4"><?php echo __('발급종료')?></span></div>
<?php }?>
<?php }?>
<?php }?>
                </li>
<?php }}?>
            </ul>
<?php if(gd_is_login()===false){?>
            <div class="btn_all"><a href="#" class="cp_down btn_alert_login"><span><?php echo __('쿠폰 모두 받기')?></span></a></div>
<?php }else{?>
            <div class="btn_all"><a href="#" class="cp_down btn_all_coupon_down"><span><?php echo __('쿠폰 모두 받기')?></span></a></div>
<?php }?>
        </div>
<?php }}?>
<?php }?>
        <div class="cp_list_none"><p><?php echo __('발급가능한 쿠폰이 없습니다.')?></p></div>
    </div>
    <!-- //쿠폰존 -->
    <!-- //20190522 추가 -->
</div>
<script type="text/javascript">
    $(document).on('click','.btn_alert_login',function (e){
        var target = $(this).attr('id');
        alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
        target == undefined ? document.location.href = "../member/login.php" : document.location.href = "../member/login.php?id=" + target;
        return false;
    });

    $(document).ready(function(){
        $('.text_dis').each(function(){
            var txtDis = $(this);
            var txtH = txtDis.height();

            $(this).css({
                'margin-top': -(txtH / 2) + 'px',
            })
        });

        // 쿠폰그룹 내 노출되는 쿠폰이 1개도 없는 경우 해당 쿠폰그룹 전체 미노출 처리

        $('.cp_list_wrap').each(function(){
            if($(this).find('li').length <= 0) $(this).hide();
        });

        if($('ul.cp_list li').length <= 0 ) {
            $('.cp_list_none').show();
        }

        var btnDisabled = false;

        // 쿠폰 받기
        $('.btn_coupon_down').click(function (e) {
            if(btnDisabled) return false;
            btnDisabled = true;
            var couponNo = $(this).data('coupon-no');
            $.ajax({
                method: "POST",
                cache: false,
                url: "../mypage/coupon_ps.php",
                data: "mode=couponDown&couponNo=" + couponNo,
                dataType: 'json',
                async: false,
                success: function (data) {
                    alert(data['message']);
                    btnDisabled = false;
                    if(data['code'] > 0)
                        location.reload();
                },
                error: function (e) {
                    console.log(e);
                    btnDisabled = false;
                }
            });
        });

        // 쿠폰 모두 받기
        $('.btn_all_coupon_down').click(function (e) {
            if(btnDisabled) return false;
            btnDisabled = true;
            var $div = $(this).parents('div.cp_list_wrap');
            var couponArrData = [];
            $div.find('.btn_coupon_down').each(function (index, item){
                couponArrData.push($(item).data('coupon-no'));
            });
            var total = $div.find('li').length;
            var countCoupon = couponArrData.length;
            if(countCoupon > 0){
                var params = {
                    total: total,
                    couponNo: couponArrData,
                    mode: 'couponDownAll'
                };
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../mypage/coupon_ps.php",
                    data: params,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        alert(data['message']);
                        btnDisabled = false;
                        if(data['code'] > 0)
                            location.reload();
                    },
                    error: function (e) {
                        console.log(e);
                        btnDisabled = false;
                    }
                });
            } else {
                alert("<?php echo __('발급 가능 쿠폰이 없습니다.')?>");
                btnDisabled = false;
                return false;
            }
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>