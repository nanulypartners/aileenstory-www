<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/goods/goods_list.html 000008561 */  $this->include_("pollViewBanner","dataSubCategory","includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="goods_list">
	<!-- 설문조사 배너 --><?php echo pollViewBanner()?><!-- 설문조사 배너 -->
	<!-- gd5 상단 하위 카테고리 -->
<?php if($TPL_VAR["themeInfo"]["cateHtml1Mobile"]){?><div><?php echo stripslashes(str_replace('&nbsp;',' ',$TPL_VAR["themeInfo"]["cateHtml1Mobile"]))?></div><?php }?>
<?php if($TPL_VAR["naviDisplay"]["naviUse"]=='y'){?>
	<div class="goods_list_category">
		<ul>
<?php if((is_array($TPL_R1=dataSubCategory($TPL_VAR["cateCd"],$TPL_VAR["cateType"]))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
			<li <?php echo $TPL_V1["style"]?> <?php if($TPL_VAR["cateCd"]==$TPL_V1["cateCd"]){?>class="selected" <?php }?>><a href="?<?php echo $TPL_VAR["cateType"]?>Cd=<?php echo $TPL_V1["cateCd"]?>" ><?php echo $TPL_V1["cateNm"]?> <?php if($TPL_VAR["naviDisplay"]["naviCount"]=='y'){?><span>(<?php echo $TPL_V1["goodsCnt"]+ 0?>)</span> <?php }?></a></li>
<?php }}?>
		</ul>
	</div>
<?php }?>
<?php if($TPL_VAR["themeInfo"]["recomDisplayMobileFl"]=='y'&&$TPL_VAR["widgetGoodsList"]){?>
	<div class="goods_list_recom">
<?php if($TPL_VAR["themeInfo"]["cateHtml2Mobile"]){?>
		<div class="user_tune"><?php echo stripslashes(str_replace('&nbsp;',' ',$TPL_VAR["themeInfo"]["cateHtml2Mobile"]))?></div>
<?php }?>

		<h3><?php echo __('추천상품')?>

			<div class="more_btn_box">
				<a class="recommend_more_btn" href="../goods/goods_recom.php?<?php echo $TPL_VAR["cateType"]?>Cd=<?php echo $TPL_VAR["cateCd"]?>"><?php echo __('더보기')?></a>
			</div>
		</h3>
		<!-- 추천상품 -->
		<ul class="recommend_prd_list">
			<?php echo includeWidget('goods/_goods_display.html')?>

		</ul>
		<!-- 추천상품 -->
	</div>
<?php }?>
	<div id="goodslist" class="goods_list_content">
<?php if($TPL_VAR["themeInfo"]["cateHtml3Mobile"]){?><div><?php echo stripslashes(str_replace('&nbsp;',' ',$TPL_VAR["themeInfo"]["cateHtml3Mobile"]))?></div><?php }?>
		<div class="goods_top_box">
			<div class="goods_arr_box">
				<div class="goods_sort">
					<div class="inp_sel">
						<select name="goods_sort" onchange="gd_get_list(1,true);">
							<option value=""><?php echo __('상품정렬')?></option>
							<option value="date"><?php echo __('등록순')?></option>
							<option value="price_asc"><?php echo __('낮은가격순')?></option>
							<option value="price_dsc"><?php echo __('높은가격순')?></option>
						</select>
					</div>
				</div>
				<div class="goods_view_type">
					<input type="hidden" name="displayType" value="<?php echo $TPL_VAR["themeInfo"]["displayType"]?>" >
					<ul>
						<li><button class="<?php if($TPL_VAR["themeInfo"]["displayType"]=='02'){?>on<?php }?>" data-key="02" >list</button></li>
						<li><button class="<?php if($TPL_VAR["themeInfo"]["displayType"]=='09'){?>on<?php }?>" data-key="09">gallery_type1</button></li>
						<li><button class="<?php if($TPL_VAR["themeInfo"]["displayType"]=='01'){?>on<?php }?>" data-key="01">gallery_type2</button></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="goods_list_box">
			<ul class="goods_product_list"><?php $this->print_("goodsTemplate",$TPL_SCP,1);?></ul>
			<div class="btn_box">
				<button type="button" class="more_btn" data-page="2" ><?php echo __('더보기')?></button>
			</div>
			<div class="loading_img"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	<!--
<?php if($TPL_VAR["cateType"]=='cate'){?>
	var keyValue = 'C<?php echo $TPL_VAR["gGlobal"]["locale"]?><?php echo $TPL_VAR["cateCd"]?>';
<?php }else{?>
	var keyValue = 'B<?php echo $TPL_VAR["gGlobal"]["locale"]?><?php echo $TPL_VAR["brandCd"]?>';
<?php }?>

	var key = {
		html: 'html' + keyValue,
		page: 'page' + keyValue,
		viewType: 'viewType' + keyValue,
		sortType: 'sortType' + keyValue,
		endFlag: 'endflag' + keyValue,
	};

	var gdStorage = gd_load_session(key.html);
	var gdPage = gd_load_session(key.page);
	var gdViewType = gd_load_session(key.viewType);
	var gdSortType = gd_load_session(key.sortType);
	var endFlag = gd_load_session(key.endFlag);

	$(document).ready(function(){
		if (gdStorage) {
			$(".goods_product_list").html(gdStorage);
			$('.btn_box button').data('page',parseInt(gdPage)+1);
			$('.goods_view_type ul li button [data-key!="' + gdViewType + '"]').removeClass('on');
			$('.goods_view_type ul li button [data-key="' + gdViewType + '"]').addClass('on');
			$('input[name="displayType"]').val(gdViewType);
			$('select[name="goods_sort"]>option[value="' + gdSortType + '"]').prop('selected', true);
		}
		$('.btn_box button.more_btn').on('click', function(e){
			gd_get_list($(this).data('page'),false);
		});

		$('.goods_view_type ul li').on('click', function(e){
			$(".goods_view_type ul li button").removeClass('on');
			$('input[name="displayType"]').val($(this).find('button').data('key'));
			$(this).find('button').addClass('on');
			gd_get_list('1',true);
    });

		$(document).on('click','.btn_alert_login',function (e){
			alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
			document.location.href = "../member/login.php";
			return false;
		});

		$('body').on('click', '.js_option_layer', function(e){
			var params = {
				type : $(this).data('type'),
				sno: $(this).data('sno'),
				goodsNo: $(this).data('goodsno')
			};

			$('#popupOption').modal({
				remote: '../goods/layer_option.php',
				cache: false,
				params: params,
				type : 'POST',
				show: true
			});
		});

	});

	function before_more_btn() {
        $('.goods_product_list').on('click', function(){return false;})
        $('.btn_box button').hide();
        $('.loading_img').append('<img src="/data/skin/mobile/moment/img/icon/icon_loading.gif">');
	}

	function after_more_btn() {
        $('.goods_product_list').off('click');
        $('.btn_box button').show();
        $('.loading_img img').remove();
	}

	var endflag = false;
	function gd_get_list(page, reloadFl){
		var displayType = $('input[name="displayType"]').val();
		var sort =  $('select[name="goods_sort"]').val();

		$.ajax({
			method		: 'GET',
			url			: './goods_list.php',
			data		: {
			    'mode' : 'data',
				'cateCd' : '<?php echo $TPL_VAR["cateCd"]?>',
				'brandCd' : '<?php echo $TPL_VAR["brandCd"]?>',
				'cateType' : '<?php echo $TPL_VAR["cateType"]?>',
				'displayType' : displayType,
				'page' : page,
				'sort' : sort
			},
			beforeSend	: function(){
                before_more_btn();
			},
			success	: function(data){
                after_more_btn();

                if($(data).filter("li.no_bx").length) {
                    if(page =='1' && endflag == false) {
                        $(".goods_product_list").append(data);
                        gd_save_session(key.endFlag, true);
                        gd_save_session(key.html, $('.goods_product_list').html());
                        endflag = true;
                        if ($(data).find('.goods_list_info').length < <?php echo $TPL_VAR["themeInfo"]["lineCnt"]*$TPL_VAR["themeInfo"]["rowCnt"]?>) {
                            $('.more_btn').hide();
                        }
                    } else {
                        alert("<?php echo __('더이상 상품이 없습니다')?>");
                        $('.more_btn').hide();
                    }

                } else {
                    if(reloadFl === true) $(".goods_product_list").html(data);
                    else  $(".goods_product_list").append(data);

                    $('.btn_box button').data('page',parseInt(page)+1);
                    gd_save_session(key.html, $('.goods_product_list').html());
                    gd_save_session(key.page, parseInt(page));
                    if ($(data).find('.goods_list_info').length < <?php echo $TPL_VAR["themeInfo"]["lineCnt"]*$TPL_VAR["themeInfo"]["rowCnt"]?>) {
                        $('.more_btn').hide();
                    }
                }
			}
		});
		gd_save_session(key.sortType, sort);
		gd_save_session(key.viewType, displayType);
	}

	//-->
</script>

<?php $this->print_("footer",$TPL_SCP,1);?>