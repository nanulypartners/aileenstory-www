<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/goods/goods_main.html 000004013 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<?php if($TPL_VAR["mainData"]["displayFl"]=='y'){?>
<div class="goods_main js_main_wrap_<?php echo $TPL_VAR["mainData"]["sno"]?>_<?php echo $TPL_VAR["mainData"]["groupSno"]?>">
	<div class="goods_list_content">
		<div class="goods_top_box">
			<div class="goods_arr_box">
				<div class="goods_sort">
					<div class="inp_sel">
						<select name="goods_sort" onchange="gd_get_list<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>(1);">
							<option value=""><?php echo __('상품정렬')?></option>
							<option value="date"><?php echo __('등록순')?></option>
							<option value="price_asc"><?php echo __('낮은가격순')?></option>
							<option value="price_dsc"><?php echo __('높은가격순')?></option>
						</select>
					</div>
				</div>
				<div class="goods_view_type">
					<input type="hidden" name="displayType" value="<?php echo $TPL_VAR["mainData"]["displayType"]?>" >
					<ul>
						<li><button class="<?php if($TPL_VAR["mainData"]["displayType"]=='02'){?>on<?php }?>" data-key="02" >list</button></li>
						<li><button class="<?php if($TPL_VAR["mainData"]["displayType"]=='09'){?>on<?php }?>" data-key="09">gallery_type1</button></li>
						<li><button class="<?php if($TPL_VAR["mainData"]["displayType"]=='01'){?>on<?php }?>" data-key="01">gallery_type2</button></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="goods_list_box">
			<ul class="goods_product_list goods_main<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>"></ul>
			<div class="btn_box">
				<button type="button" class="goods_main_more_btn btn_main_bottom_more" data-page="2" ><?php echo __('더보기')?></button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
 <!--
    $(document).ready(function(){

		gd_get_list<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>(1);
        $('.js_main_wrap_<?php echo $TPL_VAR["mainData"]["sno"]?>_<?php echo $TPL_VAR["mainData"]["groupSno"]?> .btn_main_bottom_more').on('click', function(e){
			gd_get_list<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>($(this).data('page'));
        });


        $('.goods_view_type ul li').on('click', function(e){
            $(".goods_view_type ul li button").removeClass('on');
            $('input[name="displayType"]').val($(this).find('button').data('key'));
            $(this).find('button').addClass('on');
			gd_get_list<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>('1');
        });

    });

    function gd_get_list<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>(page){

        var sort =  $('select[name="goods_sort"]').val();
        var displayType = $('input[name="displayType"]').val();

        $.post('../goods/goods_ps.php', {'mode' : 'get_main', 'more' : page, 'mainSno' : '<?php echo $TPL_VAR["mainData"]["sno"]?>', 'sort' : sort, 'displayType' :displayType, 'groupSno' : '<?php echo $TPL_VAR["mainData"]["groupSno"]?>'}, function (data) {

            $(".goods_main<?php echo $TPL_VAR["mainData"]["sno"]?>g<?php echo $TPL_VAR["mainData"]["groupSno"]?>").html(data);

            if($(data).filter("input[name='totalPage']").val() > 1) {
                $('.js_main_wrap_<?php echo $TPL_VAR["mainData"]["sno"]?>_<?php echo $TPL_VAR["mainData"]["groupSno"]?> .btn_main_bottom_more').data('page',parseInt(page)+1);
            } else {
                $('.js_main_wrap_<?php echo $TPL_VAR["mainData"]["sno"]?>_<?php echo $TPL_VAR["mainData"]["groupSno"]?> .btn_main_bottom_more').hide();
            }
        });
    }

    //-->
</script>
<?php }?>

<?php $this->print_("footer",$TPL_SCP,1);?>