<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/goods/goods_today.html 000006858 */ 
if (is_array($TPL_VAR["goodsList"])) $TPL_goodsList_1=count($TPL_VAR["goodsList"]); else if (is_object($TPL_VAR["goodsList"]) && in_array("Countable", class_implements($TPL_VAR["goodsList"]))) $TPL_goodsList_1=$TPL_VAR["goodsList"]->count();else $TPL_goodsList_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="goods_today">
<?php if($TPL_VAR["goodsList"]){?>
	<form id="frmToday" name="frmToday" method="post" target="ifrmProcess">
		<div id="goodsToday" class="today_content_box">
			<input type="hidden"  name="mode" value="">
			<div class="allchk">
				<div class="inp_chk">
					<input type="checkbox" id="checkAll" class="sp gd_checkbox_all"  data-target-name="sno[]" data-target-form="#frmToday" >
					<label for="checkAll"><?php echo __('전체선택')?></label>
				</div>
			</div>
			<ul class="my_goods">
<?php if($TPL_goodsList_1){foreach($TPL_VAR["goodsList"] as $TPL_V1){?>
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
				<li>
					<div class="info">
						<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V2["goodsNo"]?>">
							<div class="itemhead">
								<div class="thmb_box">
									<span class="thmb"><?php echo $TPL_V2["goodsImage"]?></span>
<?php if(gd_isset($TPL_V2["timeSaleFl"])){?>
									<div class="timesale_box">
										<img src="/data/skin/mobile/moment/img/icon/icon_timesale.png" alt="<?php echo __('타임세일')?>">
										<span class="timetext"><?php echo __('타임세일')?></span>
									</div>
<?php }?>
								</div>
							</div>
							<div class="itembody">
<?php if($TPL_V2["soldOutFl"]==='y'){?>
								<div class="soldout-icon display-soldout">
									<img src="/data/icon/goods_icon/icon_soldout.gif">
								</div>
<?php }?>
								<p class="name"><?php echo $TPL_V2["goodsNm"]?></p>
<?php if(gd_isset($TPL_V2["goodsPriceString"])){?>
								<p class="prc"><?php echo $TPL_V2["goodsPriceString"]?></p>
<?php }else{?>
								<p class="prc">
									<?php echo gd_global_currency_symbol()?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?>

									<span class='add_currency'><?php echo gd_global_currency_string()?> <?php echo gd_global_add_currency_display($TPL_V2["goodsPrice"])?></span>
								</p>
<?php }?>
							</div>
						</a>
						<div class="select_del_box"><button type="button" class="today_onedel_btn btn_wish_delete_one" data-sno="<?php echo $TPL_V2["goodsNo"]?>"><?php echo __('삭제')?></button></div>
					</div>
					<div class="chk_box">
						<span class="inp_chk">
							<input type="checkbox" id="toadyGoods<?php echo $TPL_V2["goodsNo"]?>"  name="sno[]" data-goodsno="<?php echo $TPL_V2["goodsNo"]?>" value="<?php echo $TPL_V2["goodsNo"]?>" data-possible="<?php echo $TPL_V2["orderPossible"]?>">
							<label for="toadyGoods<?php echo $TPL_V2["goodsNo"]?>"><?php echo __('상품선택')?></label>
						</span>
					</div>
				</li>
<?php }}?>
<?php }}?>
			</ul>
			<div class="btn_wish_bx">
				<ul class="btn_bx">
					<li><button type="button" class="select_prd_today_del_btn js_today_delete"><?php echo __('선택상품 삭제')?></button></li>
					<li><button type="button" class="today_cart_add_btn js_today_cart"><?php echo __('장바구니 담기')?></button></li>
				</ul>
			</div>
		</div>
	</form>
<?php }else{?>
	<div class="no_bx">
		<p><strong><?php echo __('최근 본 상품이 없습니다')?></strong></p>
	</div>
<?php }?>
	<div id="optionViewLayer"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // 개별 삭제버튼 클릭
        $('.btn_wish_delete_one').on('click', function(){
            $(':checkbox[name="sno[]"]').prop("checked",false);
            $("#toadyGoods"+$(this).data('sno')).prop("checked",true);
            gd_goods_today_delete();
        });

        // 삭제버튼 클릭
        $('.js_today_delete').on('click', function() {
            gd_goods_today_delete();
        });

        $('.js_today_cart').on('click', function() {
            var checkedGoods = $('input:checkbox[name="sno[]"]:checked');

            if (checkedGoods.length === 0) {
                alert("<?php echo __('상품을 선택해 주세요.')?>");
                return;
            } else if (checkedGoods.length > 1) {
                alert("<?php echo __('한개씩 장바구니에 담아주세요.')?>");
                return;
            } else if (checkedGoods.data('possible') == 'n') {
                alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s확인 후 다시 선택해주세요.','\n')?>");
                return;
            }

            $('#goodsToday input[name="mode"]').val('today');

            var params = {
                type : "today",
                goodsNo: checkedGoods.val()
            };

            $('#popupOption').modal({
                remote: '../goods/layer_option.php',
                cache: true,
                params: params,
                type : 'POST',
                show: true
            });

        });
    });

    // 선택된 최근 본 상품 삭제
    function gd_goods_today_delete() {
        if ($('input:checkbox[name="sno[]"]:checked').length === 0) {
            alert("<?php echo __('상품을 선택해 주세요.')?>");
            return;
        }

        $('#goodsToday input[name="mode"]').val('delete_today_goods');

        var goodsNo = new Array();
        $('input:checkbox[name="sno[]"]:checked').each(function (){
            goodsNo.push($(this).val());
        });

        $.ajax({
            method : "POST",
            cache  : false,
            url    : "../goods/goods_ps.php",
            data   : {'mode' : $('#goodsToday input[name="mode"]').val(), 'goodsNo' : goodsNo},
            success: function () {
                location.reload(true);
            },
            error  : function () {
                console.log('request fail');
            }
        });
    }

    function gd_option_view_result(params,sno) {
        params += "&mode=cartIn&sno="+sno;

        $.ajax({
            method: "POST",
            cache: false,
            url: "../order/cart_ps.php",
            data: params,
            success: function () {
                location.href = "../order/cart.php";
            },
            error: function (data) {
                alert(data.message);
            }
        });
    }
</script>



<?php $this->print_("footer",$TPL_SCP,1);?>