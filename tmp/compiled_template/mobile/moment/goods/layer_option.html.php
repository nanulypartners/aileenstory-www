<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/goods/layer_option.html 000085108 */ ?>
<div class="layer_option js_layer_option ly_ct">
<?php if($TPL_VAR["type"]!='goods'){?>
    <div class="ly_head">
        <h1 class="h_tit"><?php echo __('옵션선택')?></h1>
    </div><?php }?>
    <form name="frmView" id='frmView' method="post">
        <input type="hidden" name="mode" value="cartIn" />
        <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
        <input type="hidden" name="cartMode" value="" />
        <input type="hidden" name="set_goods_price"  id="set_goods_price" value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />
        <input type="hidden" name="set_goods_fixedPrice"  id="set_goods_fixedPrice" value="<?php echo gd_isset($TPL_VAR["goodsView"]['fixedPrice'], 0)?>" />
        <input type="hidden" name="set_goods_mileage" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsMileageBasic'], 0)?>" />
        <input type="hidden" name="set_goods_stock" value="<?php echo gd_isset($TPL_VAR["goodsView"]['stockCnt'], 0)?>" />
        <input type="hidden" name="set_coupon_dc_price" value="<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>" />

        <input type="hidden" name="set_goods_total_price" id="set_goods_total_price" value="0" />
        <input type="hidden" name="set_option_price"  id="set_option_price" value="0" />
        <input type="hidden" name="set_option_text_price" id="set_option_text_price" value="0" />
        <input type="hidden" name="set_add_goods_price"  id="set_add_goods_price" value="0" />
        <input type="hidden" name="set_total_price" id="set_total_price"  value="<?php echo gd_global_money_format(gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0),false)?>" />


        <input type="hidden" name="mileageFl" value="<?php echo $TPL_VAR["goodsView"]['mileageFl']?>" />
        <input type="hidden" name="mileageGoods" value="<?php echo $TPL_VAR["goodsView"]['mileageGoods']?>" />
        <input type="hidden" name="mileageGoodsUnit" value="<?php echo $TPL_VAR["goodsView"]['mileageGoodsUnit']?>" />
        <input type="hidden" name="goodsDiscountFl" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountFl']?>" />
        <input type="hidden" name="goodsDiscount" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscount']?>" />
        <input type="hidden" name="goodsDiscountUnit" value="<?php echo $TPL_VAR["goodsView"]['goodsDiscountUnit']?>" />

        <input type="hidden" name="taxFreeFl" value="<?php echo $TPL_VAR["goodsView"]['taxFreeFl']?>" />
        <input type="hidden" name="taxPercent" value="<?php echo $TPL_VAR["goodsView"]['taxPercent']?>" />

        <input type="hidden" name="scmNo" value="<?php echo $TPL_VAR["goodsView"]['scmNo']?>" />
        <input type="hidden" name="brandCd" value="<?php echo $TPL_VAR["goodsView"]['brandCd']?>" />
        <input type="hidden" name="cateCd" value="<?php echo $TPL_VAR["goodsView"]['cateCd']?>" />
        <input type="hidden" name="orderPossible" value="<?php echo $TPL_VAR["goodsView"]['orderPossible']?>" />
        <input type="hidden" name="selectGoodsFl" value="<?php echo $TPL_VAR["selectGoodsFl"]?>" />

        <input type="hidden" id="setDcPrice" value="0" />

        <input type="hidden" id="goodsOptionCnt" value="1" />
        <input type="hidden" name="deliveryCollectFl" value="<?php echo gd_isset($TPL_VAR["optionInfo"]['deliveryCollectFl'],'pre')?>" />
        <input type="hidden" name="optionFl" value="<?php echo $TPL_VAR["goodsView"]['optionFl']?>" />
        <input type="hidden" name="mainSno" value="<?php echo $TPL_VAR["mainSno"]?>" />
		<input type="hidden" name="useBundleGoods" value="1" />
		<input type="hidden" name="goodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['goodsDeliveryFl']?>" />
		<input type="hidden" name="sameGoodsDeliveryFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['sameGoodsDeliveryFl']?>" />
<?php if($TPL_VAR["goodsView"]['timeSaleFl']){?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['mileageFl']=='n'){?>
		<input type="hidden" name="goodsMileageExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['memberDcFl']=='n'){?>
		<input type="hidden" name="memberBenefitExcept" value="y" />
<?php }?>
<?php if($TPL_VAR["goodsView"]['timeSaleInfo']['couponFl']=='n'){?>
		<input type="hidden" name="couponBenefitExcept" value="y" />
<?php }?>
<?php }?>
		<div class="js_option_scroll option_scroll ">
			<div class="ly_buy_info">
				<ul class="ly_opt">
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
					<li class="js_singel_option">
						<!-- [D] 활성화 시  class="selected" 추가 -->
						<button type="button" class="bn_opt js_option_select <?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled<?php }?>">
							[<?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='one'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?>
								<?php echo $TPL_VAR["goodsView"]['optionName']?>

<?php }else{?>
								<?php echo __('옵션')?>

<?php }?>]
							<?php echo __('옵션을 선택하세요.')?>

						</button>
						<div class="optbx" style="display:none;">
							<!-- [D] 3행의 높이값 구하여 height적용 -->
							<div class="scroll scroll_vertical_fix" style="overflow-y:auto;overflow-x:hidden;<?php if($TPL_VAR["type"]!='goods'){?>max-height:150px;<?php }?>">
								<ul class="optlst">
									<li>=
<?php if($TPL_VAR["goodsView"]['optionEachCntFl']=='many'&&empty($TPL_VAR["goodsView"]['optionName'])===false){?><?php echo $TPL_VAR["goodsView"]['optionName']?>

<?php }else{?><?php echo __('옵션')?>

<?php }?>: <?php echo __('가격')?>

<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>  : <?php echo __('재고')?><?php }?>
										=
									</li>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['option'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["optionViewFl"]=='y'){?>
									<li class="<?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['fixedOrderCnt']=='option'&&$TPL_V1["stockCnt"]<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]=='0')||$TPL_V1["optionSellFl"]=='n'||$TPL_V1["optionSellFl"]=='t'){?>disabled<?php }?> <?php if(gd_isset($TPL_VAR["optionInfo"]['optionSno'])&&$TPL_VAR["optionInfo"]['optionSno']==$TPL_V1["sno"]){?>selected <?php }?> <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?>option_add_image<?php }?>"  data-option-name="<?php echo $TPL_V1["optionValue"]?>" data-params="<?php echo $TPL_V1["sno"]?><?php echo INT_DIVISION?><?php echo gd_global_money_format($TPL_V1["optionPrice"],false)?><?php echo INT_DIVISION?><?php echo $TPL_V1["mileage"]?><?php echo INT_DIVISION?><?php echo $TPL_V1["stockCnt"]?><?php echo STR_DIVISION?><?php echo $TPL_V1["optionValue"]?><?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["optionSellFl"]=='t')){?><?php echo INT_DIVISION?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?>]<?php }?><?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]!=''){?>[<?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?>]<?php }?>">
<?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_V1["optionImage"]){?><img src="<?php echo $TPL_V1["optionImage"]?>" ><?php }?> <?php }?>
										<?php echo $TPL_V1["optionValue"]?>

<?php if(gd_isset($TPL_V1["optionPrice"])!='0'){?>  : <?php echo gd_global_currency_symbol()?><?php if(gd_isset($TPL_V1["optionPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V1["optionPrice"])?><?php echo gd_global_currency_string()?> <?php }?>
<?php if($TPL_V1["optionSellFl"]=='t'){?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_V1["optionSellCode"]]?>]
<?php }else{?>
<?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_V1["stockCnt"]=='0')||$TPL_V1["optionSellFl"]=='n'){?>[<?php echo $TPL_VAR["optionSoldOutCode"]['n']?>]
<?php }else{?><?php if(in_array('optionStock',$TPL_VAR["displayAddField"])&&$TPL_VAR["goodsView"]['stockFl']=='y'){?> : <?php echo number_format($TPL_V1["stockCnt"])?><?php echo __('개')?>

<?php }?>
<?php }?><?php }?>
<?php if($TPL_V1["optionDeliveryFl"]=='t'&&$TPL_V1["optionDeliveryCode"]!=''){?>[<?php echo $TPL_VAR["optionDeliveryDelayCode"][$TPL_V1["optionDeliveryCode"]]?>]<?php }?>
									</li>
<?php }?>
<?php }}?>
								</ul>
							</div>
						</div>
					</li>
<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionName'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?>
                    <input type="hidden" name="optionSnoInput" value="<?php if($TPL_VAR["optionInfo"]['optionSnoText']){?><?php echo $TPL_VAR["optionInfo"]['optionSnoText']?><?php }?>" />
                    <input type="hidden" name="optionCntInput" value="<?php echo $TPL_S1?>" />
<?php }?>
                    <li class="js_multi_option optionNo_<?php echo $TPL_I1?>">
                        <!-- [D] 활성화 시  class="selected" 추가 -->
                        <button type="button" class="js_option_select bn_opt<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled<?php }?> <?php if($TPL_I1> 0){?>disabled<?php }?>" data-next-option="<?php echo gd_isset($TPL_VAR["goodsView"]['optionName'][($TPL_I1+ 1)])?>">[<?php echo $TPL_V1?>]<?php echo __('옵션을 선택하세요.')?></button>
                        <div class="optbx" style="display:block;display:none">
                            <!-- [D] 3행의 높이값 구하여 height적용 -->
                            <div class="scroll scroll_vertical_fix" style="overflow-y:auto;overflow-x:hidden;<?php if($TPL_VAR["type"]!='goods'){?>max-height:150px;<?php }?>">
                                <ul class="optlst">
                                    <li class="disabled2">=
<?php if($TPL_I1== 0){?><?php echo $TPL_V1?> <?php echo __('선택')?>

<?php }else{?><?php echo __('%s을 먼저 선택해 주세요',$TPL_VAR["goodsView"]['optionName'][($TPL_I1- 1)])?>

<?php }?> =
                                    </li>
<?php if($TPL_I1== 0){?>
<?php if((is_array($TPL_R2=$TPL_VAR["goodsView"]['optionDivision'])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
                                    <li data-index="<?php echo $TPL_I1?>" data-params="<?php echo $TPL_V2?>" class="<?php if(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['fixedOrderCnt']=='option'&&isset($TPL_VAR["goodsView"]['optionDivisionStock'])&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']<$TPL_VAR["goodsView"]['minOrderCnt'])||($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']=='0')||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='n'||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='t'){?>disabled<?php }?> <?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?>option_add_image<?php }?>" >
<?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage']){?><?php if($TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]){?> <img src="<?php echo $TPL_VAR["goodsView"]['optionIcon']['goodsImage'][$TPL_V2]?>" ><?php }?><?php }?>

                                        <?php echo $TPL_V2?>

<?php if(($TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='t')){?>[<?php echo $TPL_VAR["optionSoldOutCode"][$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellCode']]?>]
<?php }elseif(($TPL_VAR["goodsView"]['stockFl']=='y'&&$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['stockCnt']=='0')||$TPL_VAR["goodsView"]['optionDivisionStock'][$TPL_K2]['optionSellFl']=='n'){?>[<?php echo $TPL_VAR["optionSoldOutCode"]['n']?>]
<?php }?>
                                    </li>
<?php }}?>
<?php }?>
                                </ul>
                            </div>
                        </div>
                    </li>
<?php }}?>
<?php }?>
<?php }?>
<?php if($TPL_VAR["goodsView"]['optionTextFl']=='y'){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['optionText'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_S1=count($TPL_R1);$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
                    <li class="add">
<?php if($TPL_I1== 0){?>
                        <input type="hidden" id="optionTextCnt" value="<?php echo $TPL_S1?>" />
<?php }?>
                        <input type="hidden" name="optionTextNm_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["optionName"]?><?php if($TPL_V1["mustFl"]=='y'){?><em>(필수)</em><?php }?>"> <input type="hidden" name="optionTextMust_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["mustFl"]?>" /> <input type="hidden" name="optionTextLimit_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["inputLimit"]?>" />
                        <div class="option_input optionTextDisplay<?php echo $TPL_V1["sno"]?>">
                            <input type="hidden" name="optionTextSno_<?php echo $TPL_I1?>" value="<?php echo $TPL_V1["sno"]?>"/>
                            <input type="text" name="optionTextInput_<?php echo $TPL_I1?>" value="" class="text js_maxlength_keyup" onkeyup="goodsViewLayerController.max_length_alert(this)" onchange="goodsViewLayerController.option_text_select(this)" placeholder="[<?php echo $TPL_V1["optionName"]?><?php if($TPL_V1["mustFl"]=='y'){?>(<?php echo __('필수')?>)<?php }?>] <?php echo __('%s글자를 입력하세요.',$TPL_V1["inputLimit"])?>  <?php if($TPL_V1["addPrice"]!= 0){?>※ <?php echo __('작성시 %s%s%s 추가',gd_global_currency_symbol(),gd_global_money_format($TPL_V1["addPrice"]),gd_global_currency_string())?><?php }?>" maxlength="<?php echo $TPL_V1["inputLimit"]+ 1?>" style="width:100%" <?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled="disabled"<?php }?>/>
                            <input type="hidden" value="<?php echo $TPL_V1["addPrice"]?>"/>
                            <!-- <span class="txt-field">
                            </span> -->
                        </div>
                    </li>
<?php }}?>
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['addGoods'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>

                    <li class="js_add_goods addGoodsInput<?php echo $TPL_K1?>">
<?php if($TPL_V1["mustFl"]=='y'){?><input type="hidden" name="addGoodsInputMustFl[]" value="<?php echo $TPL_K1?>"><?php }?>
                        <button type="button" class="bn_opt js_add_goods_select<?php if($TPL_VAR["goodsView"]['orderPossible']!='y'){?> disabled<?php }?>">[<?php echo $TPL_V1["title"]?>  <?php if($TPL_V1["mustFl"]=='y'){?>(<?php echo __('필수')?>)<?php }?>] <?php echo __('추가상품을 선택하세요.')?></button>
                        <div class="optbx" style="display:block;display:none">
                            <!-- [D] 3행의 높이값 구하여 height적용 -->
                            <div class="scroll scroll_vertical_fix" style="overflow-y:auto;overflow-x:hidden;<?php if($TPL_VAR["type"]!='goods'){?>max-height:150px;<?php }?>">
                                <ul class="optlst">
<?php if((is_array($TPL_R2=$TPL_V1["addGoodsList"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
                                    <li data-key="<?php echo $TPL_K1?>"  data-add-goods-no="<?php echo $TPL_V2["addGoodsNo"]?>" data-params="<?php echo $TPL_V2["addGoodsNo"]?><?php echo INT_DIVISION?><?php echo $TPL_V2["goodsPrice"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["goodsNm"]?><?php if($TPL_V2["optionNm"]){?>(<?php echo $TPL_V2["optionNm"]?>)<?php }?><?php echo STR_DIVISION?><?php echo rawurlencode(gd_html_add_goods_image($TPL_V2["addGoodsNo"],$TPL_V2["imageNm"],$TPL_V2["imagePath"],$TPL_V2["imageStorage"], 41,$TPL_V2["goodsNm"],'_blank'))?><?php echo STR_DIVISION?><?php echo $TPL_K1?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockUseFl"]?><?php echo STR_DIVISION?><?php echo $TPL_V2["stockCnt"]?>" <?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> class="disabled" <?php }?>>
<?php if($TPL_V1["addGoodsImageFl"]=='y'){?>
<?php if($TPL_V2["imageSrc"]){?>
                                    <img src="<?php echo $TPL_V2["imageSrc"]?>" >
<?php }?>
<?php }?>
                                    <?php echo $TPL_V2["goodsNm"]?>

<?php if($TPL_V2["optionNm"]||gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
                                    (
<?php if($TPL_V2["optionNm"]){?>
                                    <?php echo $TPL_V2["optionNm"]?>

<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
                                    /
<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'){?>
                                    <?php echo gd_global_currency_symbol()?>

<?php if(gd_isset($TPL_V2["goodsPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> / <?php echo __('품절')?><?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
<?php }?>
<?php }elseif(gd_isset($TPL_V2["goodsPrice"])!='0'||($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0'))){?>
<?php if(gd_isset($TPL_V2["goodsPrice"])!='0'){?>
                                    <?php echo gd_global_currency_symbol()?>

<?php if(gd_isset($TPL_V2["goodsPrice"])> 0){?>+<?php }?><?php echo gd_global_money_format($TPL_V2["goodsPrice"])?><?php echo gd_global_currency_string()?>

<?php if($TPL_V2["soldOutFl"]=='y'||($TPL_V2["stockUseFl"]=='1'&&$TPL_V2["stockCnt"]=='0')){?> / <?php echo __('품절')?><?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
<?php }else{?>
                                    <?php echo __('품절')?>

<?php }?>
                                    )
<?php }?></li>
<?php }}?>
								</ul>
							</div>
						</div>
					</li>
<?php }}?>
<?php }?>
				</ul>
			</div>
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
			<div class="option_total_display_area" style="display:none">
				<div class="option_display_area order_goods" ></div>
<?php if($TPL_VAR["type"]!='wish'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])> 0){?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 1){?>
				<input type="hidden" name="deliveryMethodFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['code']?>" />
<?php }else{?>
				<div class="select_box_view js-goods-delivery-methodFl">
					<input type="hidden" name="deliveryMethodFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['code']?>" />
					<button type="button" class="bn_opt js_goods_delivery_methodFl_select"><?php echo __('배송방식')?> : <?php if($TPL_VAR["type"]=='cart'){?><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'][$TPL_VAR["optionInfo"]['deliveryMethodFl']]?><?php }else{?><?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['name']?><?php }?></button>
					<div class="optbx" style="display:none;">
						<div class="scroll">
							<ul class="optlst">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
								<li data-key="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></li>
<?php }}?>
							</ul>
						</div>
					</div>
				</div>
<?php }?>
<?php }?>
<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='pre'){?>
                <input type="hidden" name="deliveryCollectFl" value="pre" />
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='later'){?>
                <input type="hidden" name="deliveryCollectFl" value="later" />
<?php }else{?>
                <div class="select_box_view js_goods_delivery">
                    <button type="button" class="bn_opt js_goods_delivery_select"><?php echo __('배송비')?> : <?php echo $TPL_VAR["deliveryCollectStr"]?></button>
                    <div class="optbx" style="display:none;">
                        <div class="scroll">
                            <ul class="optlst">
                                <li data-key="pre"><?php echo __('주문시결제')?></li>
                                <li data-key="later"><?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)</li>
                            </ul>
                        </div>
                    </div>
                </div>
<?php }?>
<?php }?>
<?php if($TPL_VAR["selectGoodsFl"]===false){?>
                <div class="total_price_box view_end_price <?php if($TPL_VAR["type"]!='goods'){?>popup_price<?php }?>">
                    <ul>
                        <li class="price">
                            <dl>
                                <dt><?php echo __('총 상품금액')?></dt>
                                <dd class="goods_total_price"></dd>
                            </dl>
                        </li>
                        <li class="discount">
                            <dl>
                                <dt><?php echo __('총 할인금액')?></dt>
                                <dd class="total_benefit_price"></dd>
                            </dl>
                        </li>
                        <li class="total">
                            <dl>
                                <dt><?php echo __('총 합계금액')?></dt>
                                <dd class="total_price"></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
<?php }?>
            </div>
<?php }else{?>
            <div class="option_display_area order_goods" >
                <div id="option_display_item_0" class="view_order_goods">
                    <div class="optionKey_0 check">
                        <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>">
                        <input type="hidden" name="optionSno[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['sno'])?>"/>
                        <input type="hidden" name="goodsPriceSum[]" value="0">
                        <input type="hidden" name="addGoodsPriceSum[]" value="0">
<?php if($TPL_VAR["couponUse"]=='y'){?>
                        <input type="hidden" name="couponApplyNo[]" value="">
                        <input type="hidden" name="couponSalePriceSum[]" value="">
                        <input type="hidden" name="couponAddPriceSum[]" value="">
<?php }?>

                        <dl>
                            <dt><?php echo gd_isset($TPL_VAR["goodsView"]['goodsNmDetail'])?></dt>
                            <dd>
                                <div class="option" id="option_text_display_0"></div>
                                <div class="price_info">
									<span class="count">
										<button type="button" class="detail_down_btn goods_cnt" title="<?php echo __('감소')?>"  value="dn<?php echo STR_DIVISION?>0"><?php echo __('감소')?></button>
										<input type="number"  class="text goodsCnt_0" title="<?php echo __('수량')?>" name="goodsCnt[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>"  data-stock="<?php echo $TPL_VAR["goodsView"]['totalStock']?>"  data-key="0" onchange="goodsViewLayerController.input_count_change(this,'1');return false;">
										<button type="button" class="detail_up_btn goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?>0"><?php echo __('증가')?></button>
									</span>
                                    <div class="right">
<?php if($TPL_VAR["couponUse"]=='y'&&($TPL_VAR["type"]=='goods'||$TPL_VAR["type"]=='directCart'||$TPL_VAR["type"]=='directOrder')){?>
                                        <div class="btn_wrap">
<?php if(gd_is_login()===false){?>
                                            <button type="button" class="btn_alert_login coupon_apply_btn"><?php echo __('쿠폰적용')?></button>
<?php }else{?>
<?php if($TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                                            <span id="coupon_apply_0"><button type="button" class="coupon_apply_btn js_coupon_apply" data-key="0" ><?php echo __('쿠폰적용')?></button></span>
<?php }?>
<?php }?>
                                        </div>
<?php }?>
                                        <input type="hidden" value="<?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0)?>" name="option_price_0"><input type="hidden" value="0" name="optionPriceSum[]" ><?php echo gd_global_currency_symbol()?><span class="price option_price_display_0"><?php echo gd_isset($TPL_VAR["goodsView"]['option'][ 0]['optionPrice'], 0)?></span>
                                    </div>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>

<?php if(!$TPL_VAR["gGlobal"]["isFront"]&&$TPL_VAR["type"]!='wish'){?>

<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])> 0){?>
<?php if(count($TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])== 1){?>
            <input type="hidden" name="deliveryMethodFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['code']?>" />
<?php }else{?>
            <div class="select_box_view js-goods-delivery-methodFl">
                <input type="hidden" name="deliveryMethodFl" value="<?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['code']?>" />
                <button type="button" class="bn_opt js_goods_delivery_methodFl_select"><?php echo __('배송방식')?> : <?php echo $TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlFirst']['name']?></button>
                <div class="optbx" style="display:none;">
                    <div class="scroll">
                        <ul class="optlst">
<?php if((is_array($TPL_R1=$TPL_VAR["goodsView"]['delivery']['basic']['deliveryMethodFlData'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
                            <li data-key="<?php echo $TPL_K1?>"><?php echo $TPL_V1?></li>
<?php }}?>
                        </ul>
                    </div>
                </div>
            </div>
<?php }?>
<?php }?>

<?php if(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='pre'){?>
            <input type="hidden" name="deliveryCollectFl" value="pre" />
<?php }elseif(gd_isset($TPL_VAR["goodsView"]['delivery']['basic']['collectFl'])=='later'){?>
            <input type="hidden" name="deliveryCollectFl" value="later" />
<?php }else{?>
            <div class="select_box_view js_goods_delivery">
                <button type="button" class="bn_opt js_goods_delivery_select"><?php echo __('배송비')?> : <?php echo $TPL_VAR["deliveryCollectStr"]?></button>
                <div class="optbx" style="display:none;">
                    <div class="scroll">
                        <ul class="optlst">
                            <li data-key="pre"><?php echo __('주문시결제')?></li>
                            <li data-key="later"><?php echo __('상품수령시결제')?>(<?php echo __('착불')?>)</li>
                        </ul>
                    </div>
                </div>
            </div>
<?php }?>
<?php }?>

<?php if($TPL_VAR["selectGoodsFl"]===false){?>
            <div class="total_price_box view_end_price <?php if($TPL_VAR["type"]!='goods'){?>popup_price<?php }?>">
                <ul>
                    <li class="price">
                        <dl>
                            <dt><?php echo __('총 상품금액')?></dt>
                            <dd class="goods_total_price"></dd>
                        </dl>
                    </li>
                    <li class="discount">
                        <dl>
                            <dt><?php echo __('총 할인금액')?></dt>
                            <dd class="total_benefit_price"></dd>
                        </dl>
                    </li>
                    <li class="total">
                        <dl>
                            <dt><?php echo __('총 합계금액')?></dt>
                            <dd class="total_price"></dd>
                        </dl>
                    </li>
                </ul>
            </div>
<?php }?>
<?php }?>
        </div>

    </form>
    <div class="ly_buy_dn <?php if($TPL_VAR["type"]!='goods'){?> popup_dn <?php }?>">
<?php if($TPL_VAR["type"]!='goods'){?>
        <div class="btn_box2">
			<span>
<?php if($TPL_VAR["goodsView"]['orderPossible']=='y'){?>
<?php if($TPL_VAR["type"]=='directCart'){?>
                <a href="#" class="detail_apply_btn js_btn_add_cart"><?php echo __('장바구니')?></a>
<?php }elseif($TPL_VAR["type"]=='directOrder'){?>
                <a href="#" class="detail_apply_btn js_btn_add_order"><?php echo __('바로구매')?></a>
<?php }elseif($TPL_VAR["type"]=='wish'){?>
				<ul class="btn_wish_box">
					<li><button type="button" class="detail_prd_no_btn ly_btn_close"><?php echo __('취소')?></button></li>
					<li><a href="#" class="detail_apply_btn js_btn_modify_wish"><?php echo __('확인')?></a></li>
				</ul>
<?php }else{?>
                <a href="#" class="detail_apply_btn js_btn_add_cart"><?php if($TPL_VAR["type"]!='cart'){?><?php echo __('장바구니')?> <?php }else{?><?php echo __('적용하기')?><?php }?></a>
<?php }?>
<?php }else{?>
                <a href="#" class="detail_apply_btn"><?php echo __('구매불가')?></a>
<?php }?>
            </span>
        </div>
<?php }else{?>
        <div class="detail_prd_btn st_btn_buy_outer">
            <ul class="js_cart_btn">
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
                <li class="restock"><a class="detail_restock_btn btn_add_restock"><img src="/data/skin/mobile/moment/img/icon/icon_restock_red.png" alt="<?php echo __('재입고알림 아이콘')?>" /><?php echo __('재입고알림')?></a></li>
<?php }?>
<?php if($TPL_VAR["goodsView"]['orderPossible']=='n'){?>
                <li><a class="detail_prd_no_btn"><?php echo __('품절')?></a></li>
<?php }else{?>
                <li><a href="#" class="detail_cart_btn basket js_btn_add_cart"><?php echo __('장바구니')?></a></li>
                <li><a href="#" class="detail_order_btn js_btn_add_order"><?php echo __('바로구매')?></a></li>
<?php }?>
            </ul>
        </div>
<?php }?>
    </div>
<?php if($TPL_VAR["type"]!='goods'){?>
    <div class="close_btn">
        <button type="button" class="lys_close_btn ly_btn_close"><?php echo __('닫기')?></button>
    </div>
<?php }?>
</div>

<script type="text/javascript" src="/data/skin/mobile/moment/js/gd_goods_view.js"></script>
<script type="text/javascript">

    var goodsViewLayerController = new gd_goods_view();
    var scriptParameters = '';
    var goodsTotalCnt;
    var goodsOptionCnt = [];

    // DOM 로드
    $(document).ready(function () {

        //모바일 텍스트입력옵션 설정
        $('[name*="optionTextInput_"]').focus(function () {
            $('#divAddPay, .popup_dn').hide();
        });
        $('[name*="optionTextInput_"]').blur(function () {
            $('#divAddPay, .popup_dn').show();
        });
        //스크롤 높이 지정
<?php if($TPL_VAR["type"]=='goods'){?>
        $(".js_layer_option").css("max-height",($(window).height()/2)+30);
        $(".js_option_scroll").css("max-height",($(window).height()/2)-140);
<?php }?>

        var parameters		= {
            'setControllerName' : goodsViewLayerController,
            'setOptionFl' : '<?php echo $TPL_VAR["goodsView"]['optionFl']?>',
            'setOptionTextFl'	: '<?php echo $TPL_VAR["goodsView"]['optionTextFl']?>',
            'setOptionDisplayFl'	: '<?php echo $TPL_VAR["goodsView"]['optionDisplayFl']?>',
            'setAddGoodsFl'	: '<?php if(is_array($TPL_VAR["goodsView"]['addGoods'])){?>y<?php }else{?>n<?php }?>',
            'setIntDivision'	: '<?php echo INT_DIVISION?>',
            'setStrDivision'	: '<?php echo STR_DIVISION?>',
            'setMileageUseFl'	: '<?php echo $TPL_VAR["mileageData"]['useFl']?>',
            'setCouponUseFl'	: '<?php echo $TPL_VAR["couponUse"]?>',
            'setMinOrderCnt'	: '<?php echo $TPL_VAR["goodsView"]['minOrderCnt']?>',
            'setMaxOrderCnt'	: '<?php echo $TPL_VAR["goodsView"]['maxOrderCnt']?>',
            'setStockFl'	: '<?php echo gd_isset($TPL_VAR["goodsView"]['stockFl'])?>',
            'setSalesUnit' : '<?php echo gd_isset($TPL_VAR["goodsView"]['salesUnit'], 1)?>',
            'setDecimal' : '<?php echo $TPL_VAR["currency"]["decimal"]?>',
            'setGoodsPrice' : '<?php echo gd_isset($TPL_VAR["goodsView"]['goodsPrice'], 0)?>',
            'setGoodsNo' : '<?php echo $TPL_VAR["goodsView"]['goodsNo']?>',
            'setMileageFl' : ' <?php echo $TPL_VAR["goodsView"]['mileageFl']?>',
            'setGoodsNm': "<?php echo $TPL_VAR["goodsView"]['goodsNm']?>",
            'setImage': "<?php echo $TPL_VAR["goodsView"]['social']?>",
            'setFixedSales' : "<?php echo $TPL_VAR["goodsView"]['fixedSales']?>",
            'setFixedOrderCnt' : "<?php echo $TPL_VAR["goodsView"]['fixedOrderCnt']?>",
            'type' : "<?php echo $TPL_VAR["type"]?>",
            'setOptionPriceFl' : '<?php echo $TPL_VAR["optionPriceFl"]?>',
            'setStockCnt' : '<?php echo $TPL_VAR["goodsView"]["stockCnt"]?>'
        };
        scriptParameters = parameters;


<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
        //단일옵션일때
        $(".js_option_select").on('click', function(e){
            // 구매불가 상품 옵션 선택 불가
            if ($(this).hasClass('disabled')) {
                return false;
            }

            if($(this).next('div').is(':hidden')) {
                $(this).addClass("selected");

                var optbx =  $(this).next('div.optbx');
                $(optbx).show();

<?php if($TPL_VAR["type"]=='goods'){?>
                $(".js_option_scroll").height(($(window).height()/2)-140);

                var wrap =  $(this).parent('li.js_singel_option');
                $(wrap).addClass('singel_option_on');
                /* $(wrap).css("position","absolute");
                 $(wrap).css("width","100%");
                 $(wrap).css("top","0");
                 $(wrap).css("z-index","103");*/


                //$(optbx).addClass("optbx-select");
                $(optbx).height($(".js_layer_option").height() -130);
                $(optbx).find('div.scroll').css("max-height",$(".js_layer_option").height() -130);
                $(".js_option_scroll").css("overflow-y",'hidden');
<?php }?>
            } else {
                $(this).removeClass("selected");
                $(this).next('div.optbx').hide();

                var wrap = $(this).closest("li.js_singel_option");
                /*$(wrap).css("position","static");
				$(wrap).css("width","100%");*/
                $(wrap).removeClass('singel_option_on');
                $(".js_option_scroll").css("overflow-y",'auto');

            }
        });

        $(".js_singel_option ul.optlst li:not(.disabled)").on('click', function(e){

<?php if($TPL_VAR["goodsView"]['optionImagePreviewFl']=='y'){?>
            var optionImage = $(this).find("img").attr('src');
            if(typeof optionImage !== 'undefined')  {
                $(".js_option_add_image").remove();
                $(".slick-list").prepend("<div class='js_option_add_image'  style='height:100%'><img src='"+optionImage+"'></div>");
            }
<?php }?>
            goodsViewLayerController.option_price_display(this);

            $(".js_option_select").removeClass("selected");
            $(".js_option_select").next('div.optbx').hide();

            $(".js_option_select").html($(this).data('option-name'));

            var wrap = $(this).closest("li.js_singel_option");
            //$(wrap).css("position","static");
            $(wrap).removeClass('singel_option_on');
            $('.js_option_scroll').css('overflow-y','auto');
        });

<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
        //분리형옵션일때
        $(".js_option_select").on('click', function(e){
            if(!$(this).hasClass('disabled')) {
                if ($(this).next('div').is(':hidden')) {
                    $(this).addClass("selected");

                    var optbx = $(this).next('div.optbx');
                    $(optbx).show();
<?php if($TPL_VAR["type"]=='goods'){?>
                    $(".js_option_scroll").height(($(window).height() / 2) - 140);

                    var wrap = $(this).parent('li.js_multi_option');
                    $(wrap).addClass('multi_option_on');

                    /*$(wrap).css("position", "absolute");
                    $(wrap).css("width","calc(100% - 30px)");
                    $(wrap).css("top", "0");
                    $(wrap).css("z-index", "103");
					*/
                    //$(optbx).addClass("optbx-select");
                    $(optbx).height($(".js_layer_option").height() - 130);
                    $(optbx).find('div.scroll').css("max-height", $(".js_layer_option").height() - 130);
                    $(".js_option_scroll").css("overflow-y",'hidden');
<?php }?>

                } else {
                    $(this).removeClass("selected");
                    $(this).next('div.optbx').hide();

                    var wrap = $(this).closest("li.js_multi_option");
                    $(wrap).removeClass('multi_option_on');
                    $(".js_option_scroll").css("overflow-y",'auto');
                    /*$(wrap).css("position", "static");
					$(wrap).css("width","100%");*/
                }
            } else {
                // 구매불가 상품 옵션 선택 불가
                return false;
            }
        });

        $(".js_multi_option  ul.optlst").on('click',"li:not(.disabled,.disabled2)", function(e){

            $(this).parent("ul.optlst").find("li").removeClass("selected");
            $(this).addClass("selected");

            var index  = $(this).data('index');

<?php if($TPL_VAR["goodsView"]['optionImagePreviewFl']=='y'){?>
            if(index == 0) {
                var optionImage = $(this).find("img").attr('src');
                if(typeof optionImage !== 'undefined')  {
                    $(".js_option_add_image").remove();
                    $(".slick-list").prepend("<div class='js_option_add_image' style='height:100%'><img src='"+optionImage+"'></div>");
                }
            }
<?php }?>

            goodsViewLayerController.option_select(this,index,'<?php if(in_array('optionStock',$TPL_VAR["displayAddField"])){?>y<?php }else{?>n<?php }?>');

            $(".js_option_select").removeClass("selected");
            $(".js_option_select").next('div.optbx').hide();

            $(".optionNo_"+index+" .js_option_select").html($(this).data('params'));

            var wrap = $(this).closest("li.js_multi_option");
            $(wrap).removeClass('multi_option_on');
            $('.js_option_scroll').css('overflow-y','auto');
            //$(wrap).css("position","static");

        });

<?php }?>

        gd_benefit_calculation('frmView');
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
        $(".js_add_goods_select").on('click', function(e){
            // 구매불가 상품 추가상품 선택 불가
            if ($(this).hasClass('disabled')) {
                return false;
            }

            if($(this).next('div').is(':hidden')) {
                $(this).addClass("selected");

                var optbx =  $(this).next('div.optbx');
                $(optbx).show();

<?php if($TPL_VAR["type"]=='goods'){?>
                $(".js_option_scroll").height(($(window).height()/2)-140);

                var wrap =  $(this).parent('li.js_add_goods');
                $(wrap).addClass('add_option_on');
                /*$(wrap).css("position","absolute");
                $(wrap).css("width","100%");
                $(wrap).css("top","0");
                $(wrap).css("z-index","103");*/

                //$(optbx).addClass("optbx-select");
                $(optbx).height($(".js_layer_option").height() -130);
                $(optbx).find('div.scroll').css("max-height",$(".js_layer_option").height() -130);
                $(".js_option_scroll").css("overflow-y",'hidden');
<?php }?>

            } else {
                $(this).removeClass("selected");
                $(this).next('div.optbx').hide();

                var wrap = $(this).closest("li.js_add_goods");
                $(wrap).removeClass('add_option_on');
                $('.js_option_scroll').css('overflow-y','auto');
                //$(wrap).css("position","static");
            }
        });

        $(".js_add_goods  ul.optlst").on('click',"li:not(.disabled)", function(e){
            goodsViewLayerController.add_goods_select(this);

            $(".js_add_goods_select").removeClass("selected");
            $(".js_add_goods_select").next('div.optbx').hide();

            var wrap = $(this).closest("li.js_add_goods");
            //$(wrap).css("position","static");
            $(wrap).removeClass('add_option_on');
            $('.js_option_scroll').css('overflow-y','auto');
        });


<?php }?>


        $(".js_goods_delivery_select").on('click', function(e){
            if($(this).next('div').is(':hidden')) {
                $(this).addClass("selected");
                $(this).next('div.optbx').show();
            } else {
                $(this).removeClass("selected");
                $(this).next('div.optbx').hide();
            }
        });

        $(".js_goods_delivery  ul.optlst").on('click',"li:not(.disabled)", function(e){

            $("input[name='deliveryCollectFl']").val($(this).data('key'));

            $(".js_goods_delivery_select").html(__('배송비') + ' : ' + $(this).html());

            $(".js_goods_delivery_select").removeClass("selected");
            $(".js_goods_delivery_select").next('div.optbx').hide();
        });

        //배송방식 관련 select box
        $(".js_goods_delivery_methodFl_select").on('click', function(e){
            if($(this).next('div').is(':hidden')) {
                $(this).addClass("selected");
                $(this).next('div.optbx').show();
            } else {
                $(this).removeClass("selected");
                $(this).next('div.optbx').hide();
            }
        });
        $(".js-goods-delivery-methodFl  ul.optlst").on('click',"li:not(.disabled)", function(e){
            $("input[name='deliveryMethodFl']").val($(this).data('key'));
            $(".js_goods_delivery_methodFl_select").html(__('배송방식') + ' : ' + $(this).html());
            $(".js_goods_delivery_methodFl_select").removeClass("selected");
            $(".js_goods_delivery_methodFl_select").next('div.optbx').hide();
        });

        goodsViewLayerController.init(parameters);

<?php if($TPL_VAR["goodsView"]['optionFl']=='n'){?>
<?php if(empty($TPL_VAR["goodsView"]['addGoods'])===true&&$TPL_VAR["goodsView"]['optionTextFl']=='n'){?>
        $(".js_option_scroll").css("min-height","0px");
<?php }?>
        goodsViewLayerController.goods_calculate('#frmView',1,0,"<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>");
<?php }?>

        $('button.goods_cnt').on('click', function(e){
            goodsViewLayerController.count_change(this,1);
        });

        $('button.add_goods_cnt').on('click', function(e){
            goodsViewLayerController.count_change(this);
        });

<?php if($TPL_VAR["optionInfo"]['optionSno']){?>

<?php if($TPL_VAR["goodsView"]['optionFl']=='n'){?>
        var optionKey = $('.option_display_area').find("div[id*='option_display_item_0']");
<?php }else{?>
<?php if($TPL_VAR["goodsView"]['optionDisplayFl']=='s'){?>
        goodsViewLayerController.option_price_display( $(".js_singel_option ul.optlst li.selected"));
<?php }elseif($TPL_VAR["goodsView"]['optionDisplayFl']=='d'){?>
        goodsViewLayerController.option_price_display( $("input[name='optionSnoInput']"));
<?php }?>
        var optionKey = $('.option_total_display_area').find("div[id*='option_display_item_<?php echo $TPL_VAR["optionInfo"]['optionSno']?>']");
<?php }?>

        if($(optionKey).attr('id')) {
            optionKey = $(optionKey).attr('id').replace("option_display_item_", "");
            $("#frmView .goodsCnt_"+optionKey).val('<?php echo $TPL_VAR["optionInfo"]['goodsCnt']?>');

<?php if($TPL_VAR["optionInfo"]['optionTextSno']){?>

<?php if((is_array($TPL_R1=$TPL_VAR["optionInfo"]['optionTextStr'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            var optionText = $("#frmView .optionTextDisplay<?php echo $TPL_K1?>").find("input[name*='optionTextInput_']");
            $(optionText).val("<?php echo $TPL_V1?>");
<?php }}?>

            goodsViewLayerController.option_text_select($("#frmView input[name*='optionTextInput_']"));

            $("#frmView input[name*='optionTextInput_']").val('');
<?php }?>

<?php if((is_array($TPL_R1=$TPL_VAR["optionInfo"]['addGoodsNo'])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_K1=>$TPL_V1){?>
            $("#frmView .js_add_goods").each(function (key) {
                $(this).find("li").each(function (key1) {
                    if($(this).data('add-goods-no') =='<?php echo $TPL_V1?>') {
                        goodsViewLayerController.add_goods_select($(this));
                        $("#frmView .addGoodsCnt_"+optionKey + '_<?php echo $TPL_V1?>').val('<?php echo $TPL_VAR["optionInfo"]['addGoodsCnt'][$TPL_K1]?>');
                        goodsViewLayerController.goods_calculate("#frmView",'',optionKey + '<?php echo INT_DIVISION?><?php echo $TPL_V1?>','<?php echo $TPL_VAR["optionInfo"]['addGoodsCnt'][$TPL_K1]?>');
                    }
                });
            });
<?php }}?>

<?php if(!$TPL_VAR["optionInfo"]['optionTextSno']&&!$TPL_VAR["optionInfo"]['addGoodsNo']){?>
            goodsViewLayerController.goods_calculate("#frmView",1, optionKey,'<?php echo $TPL_VAR["optionInfo"]['goodsCnt']?>');
<?php }?>
        }
<?php }?>

        //상품 재입고 알림 팝업 오픈
<?php if($TPL_VAR["goodsView"]['restockUsableFl']==='y'&&!$TPL_VAR["gGlobal"]["isFront"]){?>
        $('.btn_add_restock').on('click', function(e){
            window.open("./popup_goods_restock.php?goodsNo="+goodsNo, "popupRestock", "top=100, left=200, status=0, width=500px, height=600px");
            return false;
        });
<?php }?>


<?php if($TPL_VAR["type"]=='goods'||$TPL_VAR["type"]=='directCart'||$TPL_VAR["type"]=='directOrder'){?>

        $(document).on('click','.js_coupon_apply', function(e){
            gd_layer_coupon_apply($(this).data('key'));
        });


        $(document).on('click','.btn_coupon_cancel', function(e){
            $('#divAddPay').removeClass('dn');
            gd_coupon_cancel($(this).data('key'),'');
        });


        $('.js_btn_add_order').on('click', function(e){
            gd_goods_order('d');
            return false;
        });

        $('.js_btn_add_cart').off('click').on('click', function(e){
            gd_goods_order();
            return false;
        });

<?php }else{?>


<?php if($TPL_VAR["type"]=='wish'||$TPL_VAR["type"]=='today'){?>

        $('.js_btn_add_cart, .js_btn_modify_wish').on('click', function(e){
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
            var goodsInfo		= $('#frmView input[name*=\'optionSno[]\']').length;
<?php }else{?>
            var goodsInfo		= $('#frmView input[name="optionSnoInput"]').val();
<?php }?>
            if (goodsInfo == '') {
                alert("<?php echo __('가격 정보가 없거나 옵션이 선택되지 않았습니다.')?>");
                return false;
            }

<?php if(gd_isset($TPL_VAR["goodsView"]['optionTextFl'])=='y'){?>
            if(!goodsViewLayerController.option_text_valid("#frmView"))
            {
                alert("<?php echo __('입력 옵션을 확인해주세요.')?>");
                return false;
            }
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
            //추가상품
            if(!goodsViewLayerController.add_goods_valid("#frmView"))
            {
                alert("<?php echo __('필수 추가 상품을 확인해주세요.')?>");
                return false;
            }
<?php }?>

            var submitFl = true;
            if (isNaN(goodsTotalCnt)) goodsTotalCnt = 1;
            if (_.isEmpty(goodsOptionCnt)) goodsOptionCnt[0] = 1;
<?php if($TPL_VAR["goodsView"]['fixedSales']=='goods'){?>
            var perSalesCnt = goodsTotalCnt % salesUnit;

            if (perSalesCnt !== 0) {
                alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                submitFl = false;
            }
<?php }else{?>
            for (i in goodsOptionCnt) {
                if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                if (perSalesCnt !== 0) {
                    alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                    submitFl = false;
                    break;
                }
            }
<?php }?>

            if (submitFl == true) {
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
                if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                    alert(__('최소 %s개 이상 구매가능합니다.', minOrderCnt));
                    submitFl = false;
                } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                    alert(__('최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                    submitFl = false;
                }
<?php }else{?>
                for (i in goodsOptionCnt) {
                    if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                    var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                    if (minOrderCnt > 1 && goodsOptionCnt[i] < minOrderCnt) {
                        alert(__('최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                        break;
                    } else if (maxOrderCnt > 0 && goodsOptionCnt[i] > maxOrderCnt) {
                        alert(__('최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                        break;
                    }
                }
<?php }?>
            }

            $('#optionViewLayer').find("input[name='goodsNo[]']").val('<?php echo $TPL_VAR["goodsView"]['goodsNo']?>');

            var params = $( "#frmView" ).serialize();
<?php if($TPL_VAR["type"]=='wish'){?>
            params += "&sno=<?php echo $TPL_VAR["optionInfo"]['sno']?>";
<?php }?>
            gd_option_view_result(params);

        });

<?php }else{?>

        $('.js_btn_add_cart').on('click', function(e){
<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
            var goodsInfo		= $('#frmView input[name*=\'optionSno[]\']').length;
<?php }else{?>
            var goodsInfo		= $('#frmView input[name="optionSnoInput"]').val();
<?php }?>
            if (goodsInfo == '') {
                alert("<?php echo __('가격 정보가 없거나 옵션이 선택되지 않았습니다.')?>");
                return false;
            }

<?php if(gd_isset($TPL_VAR["goodsView"]['optionTextFl'])=='y'){?>
            if(!goodsViewLayerController.option_text_valid("#frmView"))
            {
                alert("<?php echo __('입력 옵션을 확인해주세요.')?>");
                return false;
            }
<?php }?>

<?php if($TPL_VAR["goodsView"]['addGoods']){?>
            //추가상품
            if(!goodsViewLayerController.add_goods_valid("#frmView"))
            {
                alert("<?php echo __('필수 추가 상품을 확인해주세요.')?>");
                return false;
            }
<?php }?>

            var submitFl = true;
            if (isNaN(goodsTotalCnt)) goodsTotalCnt = 0;
<?php if($TPL_VAR["goodsView"]['fixedSales']=='goods'){?>
            var perSalesCnt = goodsTotalCnt % salesUnit;

            if (perSalesCnt !== 0) {
                alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                submitFl = false;
            }
<?php }else{?>
            for (i in goodsOptionCnt) {
                if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                if (perSalesCnt !== 0) {
                    alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                    submitFl = false;
                    break;
                }
            }
<?php }?>

            if (submitFl == true) {
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
                var fixedAlertString = '상품당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='option'){?>
                var fixedAlertString = '옵션당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
                var fixedAlertString = 'ID당';
<?php }?>

<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'||$TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
                if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                    alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                    submitFl = false;
                } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                    alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                    submitFl = false;
                }
<?php }else{?>
                for (i in goodsOptionCnt) {
                    if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                    var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                    if (minOrderCnt > 1 && goodsOptionCnt[i] < minOrderCnt) {
                        alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                        break;
                    } else if (maxOrderCnt > 0 && goodsOptionCnt[i] > maxOrderCnt) {
                        alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                        break;
                    }
                }
<?php }?>
            }

            $('#optionViewLayer').find("input[name='goodsNo[]']").val('<?php echo $TPL_VAR["goodsView"]['goodsNo']?>');

            var params = $( "#frmView" ).serialize()+"&mode=cartUpdate&sno=<?php echo $TPL_VAR["optionInfo"]['sno']?>";

            $.ajax({
                method: "POST",
                cache: false,
                url: "../order/cart_ps.php",
                data: params,
                success: function (data) {
                    document.location.reload();
                },
                error: function (data) {
                    alert(data.message);
                }
            });

        });
<?php }?>

<?php }?>

        /* 최대 입력 길이 제한 */
        $(".js_maxlength_keyup").keyup(function() {
            if ($(this).val().length > $(this).attr('maxlength')) {
                $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
            }
        });
    });


    /**
     * 총 합산
     */
    function gd_total_calculate()
    {
        var goodsPrice = parseFloat($('#frmView input[name="set_goods_price"]').val());

        //총합계 계산
        goodsTotalCnt = 0;
        goodsOptionCnt = [];
        $('#frmView input[name*=\'goodsCnt[]\']').each(function (index) {
            goodsTotalCnt += parseFloat($(this).val());
            goodsOptionCnt[index] = parseFloat($(this).val());
        });
        var goodsTotalPrice = goodsPrice*goodsTotalCnt;

        var setOptionPrice =  0;

        $('#frmView input[name*="optionPriceSum[]"]').each(function () {
            setOptionPrice += parseFloat($(this).val());
        });

        var setOptionTextPrice =  0;
        $('#frmView input[name*="optionTextPriceSum[]"]').each(function () {
            setOptionTextPrice += parseFloat($(this).val());
        });


        var setAddGoodsPrice =  0;
        $('#frmView input[name*="add_goods_total_price["]').each(function () {
            setAddGoodsPrice += parseFloat($(this).val());
        });

        $('#frmView input[name="set_option_price"]').val(setOptionPrice);
        $('#frmView input[name="set_option_text_price"]').val(setOptionTextPrice);
        $('#frmView input[name="set_add_goods_price"]').val(setAddGoodsPrice);


        var totalGoodsPrice = (goodsTotalPrice + setOptionPrice + setOptionTextPrice + setAddGoodsPrice).toFixed(<?php echo $TPL_VAR["currency"]["decimal"]?>);
        $('#frmView input[name="set_total_price"]').val(totalGoodsPrice);
        $(".goods_total_price").html(" <?php echo gd_global_currency_symbol()?> "+gd_money_format(totalGoodsPrice)+"<b><?php echo gd_global_currency_string()?></b>");

<?php if($TPL_VAR["addGlobalCurrency"]){?>
        $(".goods_total_price").append("<p class='add_currency ta_r'><?php echo gd_global_add_currency_symbol()?> "+gd_add_money_format(totalGoodsPrice)+"<?php echo gd_global_add_currency_string()?></p>");
<?php }?>

        if (typeof gd_benefit_calculation !== 'undefined' && $.isFunction(gd_benefit_calculation))  gd_benefit_calculation('frmView');
        else {
            $('button.goods_cnt').attr('disabled', false);
            $('button.add_goods_cnt').attr('disabled', false);
        }
    }


    /*
     * 혜택
     */
    function gd_benefit_calculation(frm)
    {
<?php if($TPL_VAR["goodsView"]['goodsPriceDisplayFl']=='n'){?>
        $('button.goods_cnt').attr('disabled', false);
        $('button.add_goods_cnt').attr('disabled', false);
        return false;
<?php }?>

        $('#'+frm+' input[name="mode"]').val('get_benefit');
        var parameters = $("#"+frm).serialize();

        if($("#"+frm+" input[name*='goodsNo']").length == 0)
        {
            parameters += "&goodsNo%5B%5D=<?php echo $TPL_VAR["goodsView"]['goodsNo']?>&goodsCnt%5B%5D=1";
        }
        $.post('../goods/goods_ps.php', parameters, function (data) {
            var getData = $.parseJSON(data);
            if(getData.totalDcPrice > 0 || getData.totalMileage > 0)
            {
                $(".benefits").removeClass('dn');

                if(getData.totalDcPrice > 0 )
                {
                    $(".benefits .benefits_dc_desc").removeClass('hidden');
                    $(".view_end_price li.discount").removeClass('hidden');
                    var tmp = new Array();
                    if(getData.goodsDcPrice)  tmp.push("<span><?php echo __('상품')?> : <span>" + " <?php echo gd_global_currency_symbol()?>"+gd_money_format(getData.goodsDcPrice)+"<?php echo gd_global_currency_string()?></span></span>");
                    if(getData.memberDcPrice)  tmp.push("<span><?php echo __('회원')?> : <span>" + " <?php echo gd_global_currency_symbol()?>"+ gd_money_format(getData.memberDcPrice)+"<?php echo gd_global_currency_string()?></span></span>");
                    if(getData.couponDcPrice)  tmp.push("<span><?php echo __('쿠폰')?> : <span>" + " <?php echo gd_global_currency_symbol()?>"+ gd_money_format(getData.couponDcPrice)+"<?php echo gd_global_currency_string()?></span></span>");
                    if(getData.myappDcPrice)  tmp.push("<span><?php echo __('모바일앱')?> : <span>" + " <?php echo gd_global_currency_symbol()?>"+ gd_money_format(getData.myappDcPrice)+"<?php echo gd_global_currency_string()?></span></span>");

                    $(".benefit_price").html(tmp.join());

                    $(".total_benefit_price").html("-<?php echo gd_global_currency_symbol()?>"+gd_money_format(getData.totalDcPrice)+"<?php echo gd_global_currency_string()?>");

<?php if($TPL_VAR["addGlobalCurrency"]){?>
                    $(".view_end_price .total_benefit_price").append("<p class='add_currency ta_r'>-<?php echo gd_global_add_currency_symbol()?> "+gd_add_money_format(getData.totalDcPrice)+"<?php echo gd_global_add_currency_string()?></p>");
<?php }?>
                    $("#setDcPrice").val(getData.totalDcPrice);

                }
                else
                {
                    $("#setDcPrice").val('0');
                    $(".benefits .benefits_dc_desc").addClass('hidden');
                    $(".view_end_price li.discount").addClass('hidden');
                }

                if(getData.totalMileage > 0 )
                {
                    $(".benefits .benefits_mileage_desc").removeClass('hidden');

                    var tmp =new Array();
                    if(getData.goodsMileage)  tmp.push("<span><?php echo __('상품')?> : <span>" + gd_money_format(getData.goodsMileage)+"<?php echo $TPL_VAR["mileageData"]['unit']?></span></span>");
                    if(getData.memberMileage)  tmp.push("<span><?php echo __('회원')?> : <span>" + gd_money_format(getData.memberMileage)+"<?php echo $TPL_VAR["mileageData"]['unit']?></span></span>");
                    if(getData.couponMileage)  tmp.push("<span><?php echo __('쿠폰')?> : <span>" + gd_money_format(getData.couponMileage)+"<?php echo $TPL_VAR["mileageData"]['unit']?></span></span>");
                    $(".benefit_mileage").html(tmp.join());

                    $(".total_benefit_mileage").html("+"+getData.totalMileage+"<?php echo $TPL_VAR["mileageData"]['unit']?>");
                }
                else  {
                    $(".benefits .benefits_mileage_desc").addClass('hidden');
                }

            } else {
                $("#setDcPrice").val('0');
                $(".benefits .benefits_dc_desc").addClass('hidden');
                $(".view_end_price li.discount").addClass('hidden');
            }

            if($('#'+frm+' input[name="set_total_price"]').val().trim() =='0') {
                $(".total_price").html("<?php echo gd_global_currency_symbol()?>0<b><?php echo gd_global_currency_string()?></b>");
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                $(".total_price").append("<p class='add_currency ta_r'><?php echo gd_global_add_currency_symbol()?> "+0+"<?php echo gd_global_add_currency_string()?></p>");
<?php }?>

            } else {
                var totalPrice = parseFloat($('#'+frm+' input[name="set_total_price"]').val())-parseFloat(getData.totalDcPrice);
                $(".total_price").html(" <?php echo gd_global_currency_symbol()?> "+gd_money_format(totalPrice)+"<b><?php echo gd_global_currency_string()?></b>");
<?php if($TPL_VAR["addGlobalCurrency"]){?>
                $(".total_price").append("<p class='add_currency ta_r'><?php echo gd_global_add_currency_symbol()?> "+gd_add_money_format(totalPrice)+"<?php echo gd_global_add_currency_string()?></p>");
<?php }?>
            }
            $('button.goods_cnt').attr('disabled', false);
            $('button.add_goods_cnt').attr('disabled', false);
            // 쿠폰 구매금액 제한에 따른 처리
            if (typeof getData.couponAlertKey == 'undefined') {
                // 구매 금액 제한에 걸리지 않음
            } else {
                gd_coupon_cancel(getData.couponAlertKey,'noCalculation');
                alert("<?php echo __('적용 쿠폰이 구매 금액 제한에 걸려 적용 쿠폰이 취소 되었습니다.')?>");
            }

        });

    }


<?php if($TPL_VAR["couponUse"]=='y'&&($TPL_VAR["type"]=='goods'||$TPL_VAR["type"]=='directCart'||$TPL_VAR["type"]=='directOrder')){?>
    // 쿠폰 오픈 레이어에 따른 분기

    function gd_coupon_cancel(optionKey,typeCode) {
        $('#option_display_item_'+optionKey+' input:hidden[name="couponApplyNo[]"]').val('');
        $('#option_display_item_'+optionKey+' input:hidden[name="couponSalePriceSum[]"]').val('');
        $('#option_display_item_'+optionKey+' input:hidden[name="couponAddPriceSum[]"]').val('');
        var couponApplyHtml = "<button class=\"coupon_apply_btn js_coupon_apply\" data-key=\""+optionKey+"\" type=\"button\"><?php echo __('쿠폰적용')?></button>";
        $('#coupon_apply_'+optionKey).html(couponApplyHtml);
        if (typeCode == 'noCalculation') {
            // 재계산 안함
        } else {
            gd_benefit_calculation('frmView');
        }
    }

    function gd_layer_coupon_apply(optionKey) {
        var params = {
            mode: 'coupon_apply',
            goodsNo: <?php echo $TPL_VAR["goodsView"]['goodsNo']?>,
            optionKey: optionKey,
            couponApplyNotNo: $('input:hidden[name="couponApplyNo[]"]').serializeArray(),
            couponApplyNo: $('#option_display_item_'+optionKey+' input:hidden[name="couponApplyNo[]"]').val(),
            goodsCnt: $('#option_display_item_'+optionKey+' input[name="goodsCnt[]"]').val(),
            goodsPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="goodsPriceSum[]"]').val(),
            optionPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="optionPriceSum[]"]').val(),
            optionTextPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="optionTextPriceSum[]"]').val(),
            addGoodsPriceSum: $('#option_display_item_'+optionKey+' input:hidden[name="addGoodsPriceSum[]"]').val(),
        };


        $.ajax({
            method: "POST",
            cache: false,
            url: "../goods/layer_coupon_apply.php",
            data: params,
            success: function (data) {
                $('#popupCouponApply').empty().append(data);
                $('#popupCouponApply').show();
            },
            error: function (data) {
                alert(data.message);
            }
        });

        /*
         $('#popupCouponApply').modal({
         remote: '/goods/layer_coupon_apply.php',
         cache: false,
         type : 'post',
         params: params,
         show: true
         });
         */
    }
<?php }?>



    /**
     * 바로구매, 장바구니, 상품 보관함
     *
     * @param string modeStr 처리 모드
     */
    var salesUnit = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['salesUnit'], 1)?>");
    var minOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['minOrderCnt'], 1)?>");
    var maxOrderCnt = parseInt("<?php echo gd_isset($TPL_VAR["goodsView"]['maxOrderCnt'], 0)?>");
    function gd_goods_order(modeStr)
    {
        $('input[name=\'cartMode\']').val(modeStr);

        if (modeStr == 'w') {
<?php if(gd_is_login()===false){?>
            alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
            document.location.href = "../member/login.php";
            return false;
<?php }else{?>

            var goodsNoCnt = $('input[name*="goodsNo[]"]').length;
            if(goodsNoCnt == 0) {
                $('input[name=\'cartMode\']').val('<?php echo $TPL_VAR["goodsView"]['goodsNo']?>');
            }

            $('input[name=\'mode\']').val('wishIn');
            $('#frmView').attr('action','../mypage/wish_list_ps.php');
<?php }?>
        } else {
            $('input[name=\'mode\']').val('cartIn');
            $('#frmView').attr('action','../order/cart_ps.php');

<?php if($TPL_VAR["goodsView"]['optionFl']=='y'){?>
            var goodsInfo		= $('#frmView input[name*=\'optionSno[]\']').length;
<?php }else{?>
            var goodsInfo		= $('#frmView input[name="optionSnoInput"]').val();
<?php }?>

            if (goodsInfo == '') {
                alert("<?php echo __('가격 정보가 없거나 옵션이 선택되지 않았습니다.')?>");
                return false;
            }

<?php if(gd_isset($TPL_VAR["goodsView"]['optionTextFl'])=='y'){?>
            if(!goodsViewLayerController.option_text_valid("#frmView"))
            {
                alert("<?php echo __('입력 옵션을 확인해주세요.')?>");
                return false;
            }
<?php if(gd_isset($TPL_VAR["goodsView"]['stockFl'])=='y'){?>
                var checkOptionCnt = goodsViewLayerController.option_text_cnt_valid("#frmView");
                if(checkOptionCnt) {
                    alert(__('재고가 부족합니다. 현재 %s개의 재고가 남아 있습니다.', checkOptionCnt));
                    return false;
                }
<?php }?>
<?php }?>


<?php if($TPL_VAR["goodsView"]['addGoods']){?>
            //추가상품
            if(!goodsViewLayerController.add_goods_valid("#frmView"))
            {
                alert("<?php echo __('필수 추가 상품을 확인해주세요.')?>");
                return false;
            }
<?php }?>

        }

        var submitFl = true;
        if (isNaN(goodsTotalCnt)) goodsTotalCnt = 1;
        if (_.isEmpty(goodsOptionCnt)) goodsOptionCnt[0] = 1;
<?php if($TPL_VAR["goodsView"]['fixedSales']=='goods'){?>
        var perSalesCnt = goodsTotalCnt % salesUnit;

        if (perSalesCnt !== 0) {
            alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
            submitFl = false;
        }
<?php }else{?>
        for (i in goodsOptionCnt) {
            if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
            var perSalesCnt = goodsOptionCnt[i] % salesUnit;

            if (perSalesCnt !== 0) {
                alert(__('%s개 단위로 묶음 주문 상품입니다.', salesUnit));
                submitFl = false;
                break;
            }
        }
<?php }?>

        if (submitFl == true) {
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
            var fixedAlertString = '상품당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='option'){?>
            var fixedAlertString = '옵션당';
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
            var fixedAlertString = 'ID당';
<?php }?>

<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'||$TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='goods'){?>
            if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                submitFl = false;
            } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                submitFl = false;
            }
<?php }?>
<?php if($TPL_VAR["goodsView"]['fixedOrderCnt']=='id'){?>
            //ajax로 id구매카운트 체크
            var params = {
                    mode: 'check_memberOrderGoodsCount',
                    goodsNo: <?php echo $TPL_VAR["goodsView"]['goodsNo']?>,
                };
            $.ajax({
                method: "POST",
                async: false,
                cache: false,
                url: '../order/order_ps.php',
                data: params,
                success: function (data) {
                    // error 메시지 예외 처리용
                    if (!_.isUndefined(data.error) && data.error == 1) {
                        alert(data.message);
                        return false;
                    }

                    if (minOrderCnt > 1 && (goodsTotalCnt + data.count) < minOrderCnt) {
                        alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                    } else if (minOrderCnt > 1 && goodsTotalCnt < minOrderCnt) {
                        alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                        submitFl = false;
                    } else if (maxOrderCnt > 0 && (goodsTotalCnt + data.count) > maxOrderCnt) {
                        alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                    } else if (maxOrderCnt > 0 && goodsTotalCnt > maxOrderCnt) {
                        alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                        submitFl = false;
                    }
                },
                error: function (data) {
                    alert(data.message);
                    submitFl = false;
                }
            });
<?php }?>
<?php }else{?>
            for (i in goodsOptionCnt) {
                if (isNaN(goodsOptionCnt[i])) goodsOptionCnt[i] = 0;
                var perSalesCnt = goodsOptionCnt[i] % salesUnit;

                if (minOrderCnt > 1 && goodsOptionCnt[i] < minOrderCnt) {
                    alert(__('최소 구매 수량 미달 : ' + fixedAlertString + ' 최소 %s개 이상 구매가능합니다.', minOrderCnt));
                    submitFl = false;
                    break;
                } else if (maxOrderCnt > 0 && goodsOptionCnt[i] > maxOrderCnt) {
                    alert(__('최대 구매 수량 초과 : ' + fixedAlertString + ' 최대 %s개 이하 구매가능합니다.', maxOrderCnt));
                    submitFl = false;
                    break;
                }
            }
<?php }?>
        }
        if ((modeStr == 'd' || modeStr == 'pa') && submitFl === false) {
            return false;
        }

        if(modeStr == 'pa') {
            return true;
        }
        <?php echo $TPL_VAR["customScript"]?>


        $('#frmView').attr('target','');

        // 쿠폰 사용기간 체크
        if ($('input:hidden[name="couponApplyNo[]"]').val()) {
            var checkCouponType = true;
            var couponApplyNo;
            $.ajax({
                method: "POST",
                cache: false,
                async: false,
                url: "../goods/goods_ps.php",
                data: {mode: 'goodsCheckCouponTypeArr', couponNo : $('input:hidden[name="couponApplyNo[]"]').val() },
                success: function (data) {
                    checkCouponType = data.isSuccess;
                    couponApplyNo = data.setCouponApplyNo.join('<?php echo INT_DIVISION?>');
                },
                error: function (e) {
                }
            });

            if(!checkCouponType) {
                $('input:hidden[name="couponApplyNo[]"]').val(couponApplyNo);
                alert('사용기간이 만료된 쿠폰이 포함되어 있어 제외 후 진행합니다.');
            }
        }

        if (modeStr == 'w' || typeof modeStr == 'undefined') {
            var params = $("#frmView").serialize();

            if (modeStr == 'w') {
                var ajaxUrl = '../mypage/wish_list_ps.php';
                var target = $("#addWishLayer");
            } else {
                var ajaxUrl = '../order/cart_ps.php';
                var target = $("#addCartLayer");
            }

            $.ajax({
                method: "POST",
                cache: false,
                url: ajaxUrl,
                data: params,
                success: function (data) {
                    // error 메시지 예외 처리용
                    if (!_.isUndefined(data.error) && data.error == 1) {
                        alert(data.message);
                        return false;
                    }

<?php if($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='y'||($TPL_VAR["cartInfo"]["wishPageMoveDirectFl"]=='n'&&$TPL_VAR["cartInfo"]["moveWishPageDeviceFl"]=='pc')){?>
                    if (modeStr == 'w') {
                        location.href = "../mypage/wish_list.php";
                        return false;
                    }
<?php }?>
<?php if($TPL_VAR["cartInfo"]["moveCartPageFl"]=='y'||($TPL_VAR["cartInfo"]["moveCartPageFl"]=='n'&&$TPL_VAR["cartInfo"]["moveCartPageDeviceFl"]=='pc')){?>
                    if (typeof modeStr == 'undefined') {
                        location.href = "../order/cart.php";
                        return false;
                    }
<?php }?>

                    if ($('.ly_show').length > 0 && $('.ly_show').attr('id') == 'popupOption') {
                        $('.ly_show .ly_btn_close').trigger('click');
                    } else {
                        if ($('.st-container').hasClass('st_buy_open')) {
                            $('.st_buy_close').trigger('click');
                        }
                    }

                    target.removeClass('dn');
                    $('#layerDim').removeClass('dn');
                },
                error: function (data) {
                    if (!_.isUndefined(data.message)) {
                        alert(data.message);
                    }
                }
            });
        } else {
            $('#frmView').submit();
        }
    }


</script>

<script type="text/html" id="optionTemplate">
    <div id="option_display_item_<%=displayOptionkey%>" class="view_order_goods">
        <div class="option_selected optionKey_<%=optionSno%> check">
            <input type="hidden" name="goodsNo[]" value="<?php echo $TPL_VAR["goodsView"]['goodsNo']?>"><input type="hidden" name="optionSno[]" value="<%=optionSno%>">
            <input type="hidden" name="goodsPriceSum[]" value="0">
            <input type="hidden" name="addGoodsPriceSum[]" value="0">
<?php if($TPL_VAR["couponUse"]=='y'){?>
            <input type="hidden" name="couponApplyNo[]" value="">
            <input type="hidden" name="couponSalePriceSum[]" value="">
            <input type="hidden" name="couponAddPriceSum[]" value="">
<?php }?>
            <dl>
                <dt><%=optionName%><%=optionSellCodeValue%><%=optionDeliveryCodeValue%></dt>
                <dd>
                    <div class="option" id="option_text_display_<%=displayOptionkey%>"></div>
                    <div class="price_info">
						<span class="count">
							<button type="button" class="detail_down_btn down goods_cnt" title="<?php echo __('감소')?>"  value="dn<?php echo STR_DIVISION?><%=displayOptionkey%>"><?php echo __('감소')?></button>
							<input type="number"  class="text goodsCnt_<%=displayOptionkey%>" title="<?php echo __('수량')?>" name="goodsCnt[]" value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>" data-stock="<%=optionStock%>" data-value="<?php echo gd_isset($TPL_VAR["goodsView"]['defaultGoodsCnt'])?>"  data-key="<%=displayOptionkey%>" onchange="goodsViewLayerController.input_count_change(this,'1');return false;">
							 <button type="button" class="detail_up_btn up goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?><%=displayOptionkey%>"><?php echo __('증가')?></button>
						</span>
                        <div class="right">
<?php if($TPL_VAR["couponUse"]=='y'&&($TPL_VAR["type"]=='goods'||$TPL_VAR["type"]=='directCart'||$TPL_VAR["type"]=='directOrder')){?>
                            <div class="btn_wrap">
<?php if(gd_is_login()===false){?>
                                <button type="button" class="btn_alert_login coupon_apply_btn"><?php echo __('쿠폰적용')?></button>
<?php }else{?>
<?php if($TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member'){?>
                                <span id="coupon_apply_<%=displayOptionkey%>"><button type="button" class="coupon_apply_btn js_coupon_apply" data-key="<%=displayOptionkey%>" ><?php echo __('쿠폰적용')?></button></span>
<?php }?>
<?php }?>
                            </div>
<?php }?>
                            <input type="hidden" value="<%=optionPrice%>" name="option_price_<%=displayOptionkey%>"><input type="hidden" value="0" name="optionPriceSum[]" ><?php echo gd_global_currency_symbol()?><span class="price option_price_display_<%=displayOptionkey%>"></span>
<?php if($TPL_VAR["selectGoodsFl"]===false){?>
                            <button type="button" class="detail_apply_del delete_goods" title="<?php echo __('삭제')?>" data-key="option_display_item_<%=displayOptionkey%>" ><?php echo __('삭제')?></button>
<?php }?>
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</script>

<script type="text/html" id="addGoodsTemplate">
    <div id="add_goods_display_item_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" class="view_order_add_goods">
        <dl>
            <dt>
                <input type="hidden" name="addGoodsNo[<%=optionIndex%>][]" value="<%=optionSno%>" data-group="<%=addGoodsGroup%>">
                <div class="img_box"><%=addGoodsimge%></div>
                <div class="addname"><%=addGoodsName%></div>
            </dt>
            <dd>
                <div class="price_info">
                    <span class="count">
                        <button type="button" class="detail_down_btn down add_goods_cnt" title="<?php echo __('감소')?>"  value="dn<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><?php echo __('감소')?></button>
                         <input type="number"  class="text addGoodsCnt_<%=displayOptionkey%>_<%=displayAddGoodsKey%>" title="<?php echo __('수량')?>" name="addGoodsCnt[<%=optionIndex%>][]" value="1"  data-value="1" data-key="<%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>" data-stock-fl="<%=addGoodsStockFl%>"  data-stock="<%=addGoodsStock%>"  onchange="goodsViewLayerController.input_count_change(this);return false;">
                         <button type="button" class="detail_up_btn up add_goods_cnt" title="<?php echo __('증가')?>"  value="up<?php echo STR_DIVISION?><%=displayOptionkey%><?php echo INT_DIVISION?><%=displayAddGoodsKey%>"><?php echo __('증가')?></button>
                    </span>
                    <div class="right">
                        <input type="hidden" value="<%=addGoodsPrice%>" name="add_goods_price_<%=displayOptionkey%>_<%=displayAddGoodsKey%>">
                        <input type="hidden" name="add_goods_total_price[<%=optionIndex%>][]" value="" ><span class="price add_goods_price_display_<%=displayOptionkey%>_<%=displayAddGoodsKey%>"></span>
                        <button type="button" class="detail_apply_del delete_add_goods" title="<?php echo __('삭제')?>" data-key="<%=displayOptionkey%>-<%=displayAddGoodsKey%>" ><?php echo __('삭제')?></button>
                    </div>
                </div>
            </dd>
        </dl>
    </div>
</script>