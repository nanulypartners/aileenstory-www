<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/main/index.html 000003432 */  $this->include_("includeWidget","getArticles","dataBanner","pollViewBanner");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="main">
	<!-- 슬라이더 -->
	<div class="main_slide">
		 <?php echo includeWidget('proc/_slider_banner.html','bannerCode','3606671697')?>

	</div>
	<!-- 최근게시물 -->
	<div class="main_notice">
		<div class="tit">NOTICE</div>
		<ul>
<?php if((is_array($TPL_R1=getArticles('notice', 3, 25))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
			<li><a href="../board/view.php?bdId=<?php echo $TPL_V1["bdId"]?>&sno=<?php echo $TPL_V1["sno"]?>"><?php echo $TPL_V1["subject"]?></a></li>
<?php }}?>
		</ul>
	</div>
	<!-- 메인배너 -->
	<div class="main_banner_box">
<?php if((is_array($TPL_R1=dataBanner('2505697183'))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?><?php echo $TPL_V1["tag"]?><?php }}?>
	</div>
	<ul class="main_banner_box1">
<?php if((is_array($TPL_R1=dataBanner('81393231',true))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
		<li><a href="<?php echo $TPL_V1["bannerLink"]?>" target="<?php echo $TPL_V1["bannerTarget"]?>"><img src="<?php echo $TPL_V1["bannerImageUrl"]?>" alt="<?php echo $TPL_V1["bannerImageAlt"]?>" /></a></li>
<?php }}?>
	</ul>
	<div class="pollview"><!-- 설문조사 배너 --><?php echo pollViewBanner()?><!-- 설문조사 배너 --></div>
	<div class="best_prd">
		<!-- 메인 상품 노출 --><?php echo includeWidget('goods/_goods_display_main.html','sno','4')?><!-- 메인 상품 노출 -->
	</div>
	<div class="main_banner_box2">
<?php if((is_array($TPL_R1=dataBanner('1772932566'))&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?><?php echo $TPL_V1["tag"]?><?php }}?>
	</div>
	<div class="best_prd">
		<!-- 메인 상품 노출 --><?php echo includeWidget('goods/_goods_display_main.html','sno','5')?><!-- 메인 상품 노출 -->
	</div>
	<div class="main_info_box">
		<div class="main_info">
			<dl>
				<dt><span>CS CENTER</span></dt>
				<dd>
					<p class="phone"><a href="tel:<?php echo $TPL_VAR["gMall"]["centerPhone"]?>"><?php echo $TPL_VAR["gMall"]["centerPhone"]?></a></p>
					<p class="business_hours">
						<?php echo nl2br($TPL_VAR["gMall"]["centerHours"])?>

					</p>
				</dd>
			</dl>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
			<dl>
				<dt><span>BANK INFO</span></dt>
				<dd>
					<p class="account_holder"><?php echo __('예금주')?>: <strong><?php echo $TPL_VAR["gBank"]["depositor"]?></strong></p>
					<ul class="main_bank">
						<li><span><?php echo $TPL_VAR["gBank"]["bankName"]?></span> <?php echo $TPL_VAR["gBank"]["accountNumber"]?></li>
					</ul>
					<!-- //main_bank -->
				</dd>
			</dl>
<?php }?>
		</div>
		<!-- //main_info -->
	</div>
	<!-- //main_info_box -->
</div>
<?php echo includeWidget('proc/_populate.html','sno','1')?>

<!-- //main -->
<?php $this->print_("footer",$TPL_SCP,1);?>