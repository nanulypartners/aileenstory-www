<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/member/join_method.html 000002083 */ ?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="join_method">
	<ul class="sns_login">
		<li id="btnJoin"><a href="javascript://"><?php echo __('쇼핑몰 아이디 회원가입')?></a></li>
<?php if($TPL_VAR["usePaycoLogin"]){?>
		<li class="payco js_btn_payco_login"><img src="/data/skin/mobile/moment/img/etc/txt_mo_payco_join.png" alt="<?php echo __('PAYCO')?> <?php echo __('아이디 로그인')?>"></li>
<?php }?>
<?php if($TPL_VAR["useFacebookLogin"]){?>
		<li class="facebook js_btn_facebook_login" data-facebook-url="<?php echo $TPL_VAR["facebookUrl"]?>"><img src="/data/skin/mobile/moment/img/etc/txt_mo_facebook_join.png" alt="<?php echo __('FACEBOOK')?> <?php echo __('아이디 로그인')?>"></li>
<?php }?>
<?php if($TPL_VAR["useNaverLogin"]){?>
		<li class="naver js_btn_naver_login" data-naver-type="join_method"><img src="/data/skin/mobile/moment/img/etc/txt_mo_naver_join.png" alt="<?php echo __('네이버')?> <?php echo __('아이디 로그인')?>" /></li>
<?php }?>
<?php if($TPL_VAR["useKakaoLogin"]){?>
		<li class="kakao js_btn_kakao_login" data-kakao-type="join_method" data-return-url="<?php echo $TPL_VAR["returnUrl"]?>"><img src="/data/skin/mobile/moment/img/etc/txt_mo_kakao_join.png" alt="<?php echo __('카카오')?> <?php echo __('아이디 회원가입')?>"></li>
<?php }?>
<?php if($TPL_VAR["useWonderLogin"]){?>
		<li class="wonder js_btn_wonder_login" data-wonder-type="join_method" data-wonder-url="<?php echo $TPL_VAR["wonderReturnUrl"]?>"><img src="/data/skin/mobile/moment/img/etc/txt_mo_wonder_join.png" alt="<?php echo __('위메프')?> <?php echo __('아이디 회원가입')?>"></li>
<?php }?>
	</ul>
	<!-- //sns_login -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnJoin').click(function () {
            location.href = '../member/join_agreement.php?memberFl=personal';
        });
    });
</script>
<?php $this->print_("footer",$TPL_SCP,1);?>