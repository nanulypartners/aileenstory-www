<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/mypage/_order_goods_list.html 000020656 */ 
if (is_array($TPL_VAR["ordersByRegisterDay"])) $TPL_ordersByRegisterDay_1=count($TPL_VAR["ordersByRegisterDay"]); else if (is_object($TPL_VAR["ordersByRegisterDay"]) && in_array("Countable", class_implements($TPL_VAR["ordersByRegisterDay"]))) $TPL_ordersByRegisterDay_1=$TPL_VAR["ordersByRegisterDay"]->count();else $TPL_ordersByRegisterDay_1=0;?>
<div class="order_goods_list">
	<!--
<?php if($TPL_VAR["ordersByRegisterDay"]){?>
<?php if($TPL_ordersByRegisterDay_1){foreach($TPL_VAR["ordersByRegisterDay"] as $TPL_K1=>$TPL_V1){?>
	-->
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_V2){?>
<?php if($TPL_VAR["pageName"]=='list'){?>
<?php if($TPL_V2["goods"]){?>
<?php if($TPL_V2["orderGoodsCnt"]> 0){?>
	<p class="order_top_date"><?php echo $TPL_K1?> (<?php echo __('총 %s건','<strong>'.$TPL_V2["orderGoodsCnt"].'</strong>')?>)</p>
<?php }?>
<?php }?>
<?php }?>
	<div class="orderlist_wrap">
<?php if((is_array($TPL_R3=$TPL_V2["goods"])&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
<?php if($TPL_I3== 0){?>
		<div class="order_number_box">
			<div class="order_number">
				<div class="order_number_left">
					<?php echo __('주문번호')?>

<?php if($TPL_VAR["mode"]==='refundRegist'||$TPL_VAR["mode"]==='backRegist'||$TPL_VAR["mode"]==='exchangeRegist'){?>
					<a href="../mypage/order_view.php?orderNo=<?php echo $TPL_V2["orderNo"]?>"><?php echo $TPL_V2["orderNo"]?></a>
<?php }else{?>
					<?php echo $TPL_V2["orderNo"]?>

<?php }?>
				</div>
<?php if($TPL_VAR["pageName"]=='view'){?><div class="order_number_right"><span class="order_date"><?php echo $TPL_K1?></span></div><?php }?>
			</div>
			<ul>
<?php if($TPL_VAR["mode"]==='backRegist'||$TPL_VAR["mode"]==='refundRegist'||$TPL_VAR["mode"]==='exchangeRegist'){?>
				<li><button class="order_btn all_check_goods" value="n"><?php echo __('전체선택')?></button></li>
<?php }elseif(substr($TPL_V3["orderStatus"], 0, 1)=='o'){?>
<?php if($TPL_V2["orderChannelFl"]!='naverpay'){?>
				<li><a href="#" order-no="<?php echo $TPL_V2["orderNo"]?>" class="order_btn btn_order_cancel"><?php echo __('주문취소')?></a></li>
<?php }?>
<?php }else{?>
<?php if($TPL_V2["orderSettleButton"]==true){?>
				<li><a href="../mypage/layer_order_settle.php?orderNo=<?php echo $TPL_V2["orderNo"]?>" data-toggle="modal" data-target="#popupSettle" data-type="post" data-cache="false" class="order_btn"><?php echo __('구매확정')?></a></li>
<?php }?>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true&&$TPL_V2["orderChannelFl"]!='naverpay'){?>
<?php if($TPL_V2["canRefund"]==true||$TPL_V2["canBack"]==true||$TPL_V2["canExchange"]==true){?>
<?php if($TPL_V2["canRefund"]==true){?>
				<li><a href="../mypage/layer_order_refund_regist.php?mode=refundRegist&orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&orderStatus=<?php echo $TPL_V3["orderStatus"]?>" data-toggle="modal" data-target="#popupRefund" data-type="post" data-cache="false" class="order_btn"><?php echo __('환불신청')?></a></li>
<?php }?>
<?php if($TPL_V2["canBack"]==true){?>
				<li><a href="../mypage/layer_order_back_regist.php?mode=backRegist&orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&orderStatus=<?php echo $TPL_V3["orderStatus"]?>" data-toggle="modal" data-target="#popupBack" data-type="post" data-cache="false" class="order_btn"><?php echo __('반품신청')?></a></li>
<?php }?>
<?php if($TPL_V2["canExchange"]==true){?>
				<li><a href="../mypage/layer_order_exchange_regist.php?mode=exchangeRegist&orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&orderStatus=<?php echo $TPL_V3["orderStatus"]?>" data-toggle="modal" data-target="#popupExchange" data-type="post" data-cache="false" class="order_btn"><?php echo __('교환신청')?></a></li>
<?php }?>
<?php }?>
<?php }?>
<?php }?>
			</ul>
		</div>
<?php }?>
<?php if($TPL_V3["orderInfoRow"]){?>
		<h3 style="padding:10px 0 0 10px;"><?php if($TPL_V3["orderInfoCd"]== 1){?><?php echo __('메인 배송지')?><?php }else{?><?php echo __('추가 배송지')?><?php echo $TPL_V3["orderInfoCd"]- 1?><?php }?></h3>
<?php }?>
		<ul class="my_goods" data-order-no="<?php echo $TPL_V2["orderNo"]?>" data-order-goodsno="<?php echo $TPL_V2["sno"]?>" data-order-status="<?php echo $TPL_V3["orderStatus"]?>">
			<li>
				<div class="info">
					<a href="<?php if($TPL_VAR["pageName"]=='list'){?>../mypage/order_view.php?orderNo=<?php echo $TPL_V2["orderNo"]?><?php }else{?>../goods/goods_view.php?goodsNo=<?php if($TPL_V3["goodsType"]==='addGoods'){?><?php echo $TPL_V3["parentGoodsNo"]?><?php }else{?><?php echo $TPL_V3["goodsNo"]?><?php }?><?php }?>">
						<div class="itemhead">
							<div class="img"><?php echo $TPL_V3["goodsImage"]?></div>
						</div>
						<div class="itembody">
<?php if($TPL_V3["handleMode"]==='z'){?>
							<div class="exchange_add_info"><span>교환추가</span></div>
<?php }?>

							<dl>
								<dt class="prd_name"><?php echo $TPL_V3["goodsNm"]?></dt>
								<!--
<?php if($TPL_V3["optionInfo"]){?>
<?php if((is_array($TPL_R4=$TPL_V3["optionInfo"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {$TPL_S4=count($TPL_R4);$TPL_I4=-1;foreach($TPL_R4 as $TPL_V4){$TPL_I4++;?>
								-->
								<dd class="prd_option">
									<?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?>

<?php if($TPL_V4["optionRealPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
									(<?php if($TPL_V4["optionRealPrice"]> 0){?>+<?php }?><?php echo gd_global_order_currency_display($TPL_V4["optionRealPrice"])?>)
<?php }?>
<?php if($TPL_I4==$TPL_S4- 1&&$TPL_V4["deliveryInfoStr"]!=""){?>
<?php if(empty($TPL_V4["deliveryInfoStr"])===false){?>[<?php echo $TPL_V4["deliveryInfoStr"]?>]<?php }?>
<?php }?>
								</dd>
								<!--
<?php }}?>
<?php }?>
<?php if($TPL_V3["optionTextInfo"]){?>
<?php if((is_array($TPL_R4=$TPL_V3["optionTextInfo"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
								-->
								<dd class="prd_option">
									<?php echo $TPL_V4["optionName"]?> : <?php echo $TPL_V4["optionValue"]?>

<?php if($TPL_V4["optionTextPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
									(<?php if($TPL_V4["optionTextPrice"]> 0){?>+<?php }?><?php echo gd_global_order_currency_display($TPL_V4["optionTextPrice"])?>)
<?php }?>
								</dd>
								<!--
<?php }}?>
<?php }?>
								-->
							</dl>
						</div>
					</a>
<?php if(empty($TPL_V3["addGoods"])===false){?>
					<div class="add_goods_box">
						<em class="add_title"><?php echo __('추가상품')?></em>
						<ul class="add_goods_list">
<?php if((is_array($TPL_R4=$TPL_V3["addGoods"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
							<li>
								<div class="add_goods_img"><?php echo $TPL_V4["goodsImage"]?></div>
								<div class="add_goods_content">
									<span class="title"><?php echo $TPL_V4["goodsNm"]?></span>
									<div class="add_goods_text">
										<span class="goods_number"><?php echo __('주문수량')?> : <em><?php echo $TPL_V4["goodsCnt"]?></em></span>
										<span class="goods_price"><em><?php echo gd_global_order_currency_display($TPL_V4["goodsPrice"],$TPL_V2["exchangeRate"],$TPL_V2["currencyPolicy"])?></em></span>
									</div>
								</div>
							</li>
<?php }}?>
						</ul>
					</div>
<?php }?>
					<div class="order_info">
						<div class="info_box">
							<div class="info2">
								<?php echo __('수량')?> : <?php echo $TPL_V3["goodsCnt"]?> | <?php echo gd_global_order_currency_display(($TPL_V3["goodsPrice"]+$TPL_V3["optionPrice"]+$TPL_V3["optionTextPrice"])*$TPL_V3["goodsCnt"],$TPL_V2["exchangeRate"],$TPL_V2["currencyPolicy"])?>

							</div>
							<div class="ing_chk">
<?php if($TPL_V3["userHandleSno"]> 0&&$TPL_V3["handleSno"]== 0){?>
<?php if($TPL_V3["userHandleFl"]=='n'){?>
<?php if($TPL_V3["userHandleMode"]=='b'){?>
								<strong class="order_ing_btn">반품거절</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='r'){?>
								<strong class="order_ing_btn">환불거절</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='e'){?>
								<strong class="order_ing_btn">교환거절</strong>
<?php }?>
<?php }elseif($TPL_V3["userHandleFl"]=='y'){?>
<?php if($TPL_V3["userHandleMode"]=='b'){?>
								<strong class="order_ing_btn">반품승인</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='r'){?>
								<strong class="order_ing_btn">환불승인</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='e'){?>
								<strong class="order_ing_btn">교환승인</strong>
<?php }?>
<?php }else{?>
<?php if($TPL_V3["userHandleMode"]=='b'){?>
								<strong class="order_ing_btn">반품신청</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='r'){?>
								<strong class="order_ing_btn">환불신청</strong>
<?php }elseif($TPL_V3["userHandleMode"]=='e'){?>
								<strong class="order_ing_btn">교환신청</strong>
<?php }?>
<?php }?>
<?php }else{?>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===false&&(substr($TPL_V3["orderStatus"], 0, 1)=='c'||$TPL_V3["orderStatus"]=='b4'||$TPL_V3["orderStatus"]=='e5'||$TPL_V3["orderStatus"]=='r3')&&$TPL_V3["handleDetailReasonShowFl"]=='y'){?>
								<a class="refund_comp_btn" href="../mypage/layer_order_refund_reason.php?orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&userHandleSno=<?php echo $TPL_V3["userHandleSno"]?>&orderStatus=<?php echo $TPL_V3["orderStatus"]?>" data-toggle="modal" data-target="#popupReason" data-type="post" data-cache="false">
<?php if($TPL_V3["orderStatus"]=='d2'){?>
									<strong class="order_finish_btn"><?php echo $TPL_V3["orderStatusStr"]?></strong>
<?php }else{?>
									<strong class="order_ing_btn"><?php echo $TPL_V3["orderStatusStr"]?></strong>
<?php }?>
									<span class="ico_blue_arrow"></span>
								</a>
<?php }else{?>
<?php if($TPL_V3["orderStatus"]=='d2'){?>
								<strong class="order_finish_btn"><?php echo $TPL_V3["orderStatusStr"]?></strong>
<?php }else{?>
								<strong class="order_ing_btn"><?php echo $TPL_V3["orderStatusStr"]?></strong>
<?php }?>
<?php }?>
<?php }?>
							</div>
						</div>
<?php if($TPL_V3["naverpayStatus"]["code"]=='DelayProductOrder'){?>
						<p class="d_causation">
							(<?php echo $TPL_V3["naverpayStatus"]["notice"]?> , <a href="../mypage/layer_order_naverpay_reason.php?orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>" data-toggle="modal" data-target="#popupReason" data-type="post" data-cache="false" ><u><?php echo __('지연사유')?></u></a>)
						</p>
<?php }?>
					</div>
					<ul class="btn_bx">
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&($TPL_V3["userHandleSno"]> 0||($TPL_V3["handleSno"]> 0&&$TPL_V3["handleDetailReasonShowFl"]=='y'))){?>
						<li>
							<a href="../mypage/layer_order_refund_reason.php?orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&userHandleSno=<?php echo $TPL_V3["userHandleSno"]?>&orderStatus=<?php echo $TPL_V3["orderStatus"]?>" data-toggle="modal" data-target="#popupReason" data-type="post" data-cache="false" class="order_etc_btn">
<?php if($TPL_V3["userHandleMode"]=='b'||$TPL_V3["handleMode"]=='b'){?>
								<?php echo __('반품사유')?>

<?php }elseif($TPL_V3["userHandleMode"]=='r'||$TPL_V3["handleMode"]=='r'){?>
								<?php echo __('환불사유')?>

<?php }elseif($TPL_V3["userHandleMode"]=='e'||$TPL_V3["handleMode"]=='e'){?>
								<?php echo __('교환사유')?>

<?php }elseif($TPL_V3["userHandleMode"]=='c'||$TPL_V3["handleMode"]=='c'){?>
								<?php echo __('취소사유')?>

<?php }?>
							</a>
						</li>
<?php }else{?>
<?php if(substr($TPL_V3["orderStatus"], 0, 1)=='d'){?>
						<li>
							<a href="#" class="order_ok_btn btn_order_settle" order-no="<?php echo $TPL_V2["orderNo"]?>" order-goodsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('구매확정')?></a>
						</li>
<?php }?>
<?php }?>
<?php if((substr($TPL_V3["orderStatus"], 0, 1)=='d'||substr($TPL_V3["orderStatus"], 0, 1)=='s'||substr($TPL_V3["orderStatus"], 0, 1)=='z')&&$TPL_V3["invoiceNo"]){?>
<?php if($TPL_V3["deliveryMethodFl"]=='delivery'||$TPL_V3["deliveryMethodFl"]=='packet'||!$TPL_V3["deliveryMethodFl"]){?>
<?php if($TPL_V3["deliveryMethodFl"]!='delivery'){?>
						<li>
							<a class="delivery_trace_btn" style="max-width:100px; overflow:hidden;"><?php echo $TPL_V3["deliveryMethodFlText"]?></a>
						</li>
<?php }?>
						<li><a href="../share/delivery_trace.php?invoiceCompanySno=<?php echo $TPL_V3["invoiceCompanySno"]?>&invoiceNo=<?php echo $TPL_V3["invoiceNo"]?>" class="delivery_trace_btn" data-invoice-company-sno="<?php echo $TPL_V3["invoiceCompanySno"]?>" data-invoice-no="<?php echo $TPL_V3["invoiceNo"]?>" target="_blank"><?php echo __('배송추적')?></a></li>
<?php }else{?>
						<li>
							<a href="../mypage/layer_order_delivery_method.php?orderNo=<?php echo $TPL_V2["orderNo"]?>&orderGoodsNo=<?php echo $TPL_V3["sno"]?>" data-toggle="modal" data-target="#popupDeliveryMethod" data-type="post" data-cache="false" class="delivery_trace_btn" style="max-width:100px; overflow:hidden;"><?php echo $TPL_V3["deliveryMethodFlText"]?></a>
						</li>
<?php }?>
<?php }else{?>
						<li>
							<a class="delivery_trace_btn" style="max-width:100px; overflow:hidden;"><?php echo $TPL_V3["deliveryMethodFlText"]?></a>
						</li>
<?php }?>
<?php if($TPL_V3["orderStatus"]=='d1'){?>
						<li><a href="#" class="delivery_order_btn btn_order_delivery" order-no="<?php echo $TPL_V2["orderNo"]?>" order-goodsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('수취확인')?></a></li>
<?php }?>
<?php if($TPL_V3["viewWriteGoodsReview"]){?>
						<li><a href="../board/write.php?bdId=<?php echo $TPL_VAR["goodsReviewId"]?>&gboard=r&orderGoodsNo=<?php echo $TPL_V3["sno"]?>&goodsNo=<?php echo $TPL_V3["goodsNo"]?><?php if($TPL_VAR["guest"]=='y'){?>&guest=y<?php }?>" class="btn btn_y ogl_reviewrite btn_open_layer" data-toggle="modal" data-cache="false" data-target="#popupBoard" ><?php echo __('리뷰쓰기')?></a></li>
<?php }?>

<?php if($TPL_V3["viewWritePlusReview"]){?>
						<li><a href="javascript:gd_popup_plus_review_write('<?php echo $TPL_V2["orderNo"]?>',<?php echo $TPL_V3["sno"]?>, '<?php echo $TPL_V3["goodsNo"]?>')" class="btn btn_y ogl_reviewrite"><?php echo __('리뷰등록')?></a></li>
<?php }?>
					</ul>
				</div>
<?php if(($TPL_VAR["mode"]==='backRegist'&&$TPL_V3["canBack"]==true)||($TPL_VAR["mode"]==='refundRegist'&&$TPL_V3["canRefund"]==true)||($TPL_VAR["mode"]==='exchangeRegist'&&$TPL_V3["canExchange"]==true)){?>
<?php if($TPL_V3["userHandleSno"]== 0&&$TPL_V3["handleSno"]<= 0){?>
<?php if($TPL_V3["canBack"]==true||$TPL_V3["canRefund"]==true||$TPL_V3["canExchange"]==true){?>
				<div class="info claim_info">
					<div class="claim_left">
					<span class="inp_chk">
						<input id="goodsno_<?php echo $TPL_V3["sno"]?>" type="checkbox"  value = "<?php echo $TPL_V3["sno"]?>" name="orderGoodsNo[]" data-goodsno="<?php echo $TPL_V3["goodsNo"]?>" data-order-status="<?php echo $TPL_V3["orderStatus"]?>">
						<label for="goodsno_<?php echo $TPL_V3["sno"]?>">
<?php if($TPL_VAR["mode"]=='backRegist'){?>
							<?php echo __('반품 신청')?>

<?php }elseif($TPL_VAR["mode"]=='refundRegist'){?>
							<?php echo __('환불 신청')?>

<?php }elseif($TPL_VAR["mode"]=='exchangeRegist'){?>
							<?php echo __('교환 신청')?>

<?php }?>
						</label>
					</span>
					</div>
					<div class="claim_right">
						<span class="input_content">
							<div class="price_info">
								<span class="count">
									<button type="button" class="down claim_goods_cnt" title="<?php echo __('감소')?>"><?php echo __('감소')?></button>
									<input type="number" style="width:44px;" class="text" title="<?php echo __('수량')?>" name="claimGoodsCnt[<?php echo $TPL_V3["sno"]?>]" value="<?php echo $TPL_V3["goodsCnt"]?>" min="1" max="<?php echo $TPL_V3["goodsCnt"]?>" data-mode="<?php echo $TPL_VAR["mode"]?>">
									<button type="button" class="up claim_goods_cnt" title="<?php echo __('증가')?>"><?php echo __('증가')?></button>
								</span>
							</div>
						</span>
					</div>
				</div>
<?php }?>
<?php }?>
<?php }?>
			</li>
		</ul>
<?php }}?>

<?php }}?>
	</div>
	<!--
<?php }}?>
<?php }else{?>
	-->

	<div class="my_goods no">
		<p><?php echo __('최근 주문 내역이 없습니다.')?></p>
	</div>
	<!--
<?php }?>
	-->
</div>

<script type="text/javascript">
    $(function () {
        // 구매취소
        $('.btn_order_cancel').click(function (e) {
            e.preventDefault();
            if (confirm("<?php echo __('주문취소 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'cancelRegist',
                    orderNo: $(this).attr('order-no')
                };

                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        // 수취확인
        $('.btn_order_delivery').click(function (e) {
            e.preventDefault();
            if (confirm("<?php echo __('수취확인 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'deliveryCompleteRegist',
                    orderNo: $(this).attr('order-no'),
                    orderGoodsNo: $(this).attr('order-goodsno')
                };

                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        // 구매확정
        $('.btn_order_settle').click(function (e) {
            e.preventDefault();
            if (confirm("<?php echo __('구매확정 처리를 하시겠습니까?')?>")) {
                var params = {
                    mode: 'settleRegist',
                    orderNo: $(this).attr('order-no'),
                    orderGoodsNo: $(this).attr('order-goodsno')
                };
                $.post('../mypage/order_ps.php', params, function (data) {
                    alert(data.message);
                    if (data.code == 200) {
                        location.reload(true);
                    }
                });
            }
        });

        // 클레임신청 수량 증감
        $('.claim_goods_cnt').on('click', function() {
            var selectEl = $(this).closest('.count').children('input[name*="claimGoodsCnt"]');
            var claimGoodsCnt = parseInt(selectEl.val());
            if ($(this).hasClass('up') && selectEl.attr('max') > claimGoodsCnt) {
                selectEl.val(claimGoodsCnt + 1);
            } else if ($(this).hasClass('down') && selectEl.attr('min') < claimGoodsCnt) {
                selectEl.val(claimGoodsCnt - 1);
            }
        });

        // 클래임신청 수량 초과 체크
        $('input[name*="claimGoodsCnt"]').on('keyup', function(){
            var mode = $(this).data('mode');
            var orginCnt = parseInt($(this).attr('max'));
            var claimCnt = parseInt($(this).val());
            switch (mode) {
                case 'refundRegist' :
                    mode = "<?php echo __('환불')?>";
                    break;
                case 'backRegist' :
                    mode = "<?php echo __('반품')?>";
                    break;
                case 'exchangeRegist' :
                    mode = "<?php echo __('교환')?>";
                    break;
            }
            if (claimCnt > orginCnt || claimCnt <= 0) {
                alert(__('%s수량은 주문수량 %s 보다 큰값 또는 0 값을 입력할 수 없습니다.', [mode, orginCnt]));
                $(this).val(orginCnt);
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo PATH_SKIN?>js/gd_board_goods.js" charset="utf-8"></script>