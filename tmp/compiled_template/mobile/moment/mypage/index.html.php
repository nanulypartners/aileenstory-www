<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/mypage/index.html 000004130 */  $this->include_("includeWidget");?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="mypage_index">
	<!-- 마이페이지 회원 요약정보 -->
	<?php echo includeWidget('mypage/_member_summary.html')?>

	<!--// 마이페이지 회원 요약정보 -->
	<div class="mypage_menu">
		<h3 class="tit"><?php echo __('쇼핑정보')?></h2>
		<ul>
			<li><a href="order_list.php"><span><?php echo __('주문목록/배송조회')?></span></a></li>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true){?>
			<li class="depth1_list">
				<a href="#"><span><?php echo __('취소/반품/교환 내역')?></span></a>
				<ul class="dn">
					<li><a href="order_list.php?mode=cancelRequest"><span><?php echo __('취소/반품/교환 신청 내역')?></span></a></li> 
					<li><a href="order_list.php?mode=cancel"><span><?php echo __('취소/반품/교환 처리 내역')?></span></a></li>
				</ul>
			</li>
<?php }else{?>
			<li><a href="order_list.php?mode=cancel"><span><?php echo __('취소/반품/교환 내역')?></span></a></li>
<?php }?>
<?php if(gd_is_plus_shop(PLUSSHOP_CODE_USEREXCHANGE)===true&&$TPL_VAR["userHandleFl"]===true){?>
			<li class="depth1_list">
				<a href="#"><span><?php echo __('환불/입금 내역')?></span></a>
				<ul class="dn">
					<li><a href="order_list.php?mode=refundRequest"><span><?php echo __('환불 신청 내역')?></span></a></li>
					<li><a href="order_list.php?mode=refund"><span><?php echo __('환불/입금 처리 현황')?></span></a></li>
				</ul>
			</li>
<?php }else{?>
			<li><a href="order_list.php?mode=refund"><span><?php echo __('환불/입금 내역')?></span></a></li>
<?php }?>
			<li><a href="wish_list.php"><span><?php echo __('찜 리스트')?></span></a></li>
		</ul>
	</div>
<?php if(gd_use_coupon()||gd_use_couponzone()||gd_use_deposit()||gd_use_mileage()){?>
	<div class="mypage_menu">
		<h3 class="tit"><?php echo __('혜택관리')?></h3>
		<ul>
<?php if(gd_use_coupon()){?>
			<li><a href="coupon.php"><span><?php echo __('쿠폰내역')?></span></a></li>
<?php }?>
<?php if(gd_use_couponzone()){?>
			<li><a href="../event/couponzone.php"><span><?php echo __('쿠폰존')?></span></a></li>
<?php }?>
<?php if(gd_use_deposit()){?>
			<li><a href="deposit.php"><span><?php echo gd_display_deposit('name')?></span></a></li>
<?php }?>
<?php if(gd_use_mileage()){?>
			<li><a href="mileage.php"><span><?php echo gd_display_mileage_name()?></span></a></li>
<?php }?>
		</ul>
	</div>
<?php }?>
<?php if($TPL_VAR["canQa"]){?>
	<div class="mypage_menu">
		<h3 class="tit"><?php echo __('고객센터')?></h3>
		<ul>
			<li><a href="mypage_qa.php"><span><?php echo __('1:1문의내역')?></span></a></li>
		</ul>
	</div>
<?php }?>
	<div class="mypage_menu">
		<h3 class="tit"><?php echo __('회원정보')?></h3>
		<ul>
<?php if($TPL_VAR["canGoodsReview"]){?>
			<li><a href="mypage_goods_review.php"><span><?php echo __('나의 상품후기')?></span></a></li>
<?php }?>
<?php if($TPL_VAR["canGoodsQa"]){?>
			<li><a href="mypage_goods_qa.php"><span><?php echo __('나의 상품문의')?></span></a></li>
<?php }?>
<?php if($TPL_VAR["plusReviewConfig"]["useFl"]=='y'){?>
			<li><a href="../board/plus_review_article.php?isMypage=y"><span><?php echo __('나의 플러스리뷰')?></span></a></li>
<?php }?>
			<li><a href="shipping.php"><span><?php echo __('배송지 관리')?></span></a></li>
			<li><a href="hack_out.php"><span><?php echo __('회원 탈퇴')?></span></a></li>
		</ul>
	</div>
	
	
	
</div>
<!-- //mypage_index -->
<?php $this->print_("footer",$TPL_SCP,1);?>

<script type="text/javascript">
	$(function(){
		$('.depth1_list > a').on({
			'click':function(e){
				e.preventDefault();
				var uldp = $(this).parent().find('ul').css('display')
				if(uldp == 'none'){
					$(this).parent().find('ul').slideDown();
					$(this).parent().addClass('on');
				}else{
					$(this).parent().find('ul').slideUp();
					$(this).parent().removeClass('on');
				}
			}
		});
	});
</script>