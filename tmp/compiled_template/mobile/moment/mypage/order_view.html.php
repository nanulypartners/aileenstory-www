<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/mypage/order_view.html 000032389 */  $this->include_("includeFile");
if (is_array($TPL_VAR["totalRefundData"])) $TPL_totalRefundData_1=count($TPL_VAR["totalRefundData"]); else if (is_object($TPL_VAR["totalRefundData"]) && in_array("Countable", class_implements($TPL_VAR["totalRefundData"]))) $TPL_totalRefundData_1=$TPL_VAR["totalRefundData"]->count();else $TPL_totalRefundData_1=0;
if (is_array($TPL_VAR["exchangeHandleData"])) $TPL_exchangeHandleData_1=count($TPL_VAR["exchangeHandleData"]); else if (is_object($TPL_VAR["exchangeHandleData"]) && in_array("Countable", class_implements($TPL_VAR["exchangeHandleData"]))) $TPL_exchangeHandleData_1=$TPL_VAR["exchangeHandleData"]->count();else $TPL_exchangeHandleData_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<!-- 주문상품 리스트 -->
<?php echo includeFile('mypage/_order_goods_list.html')?>

<!--// 주문상품 리스트 -->
<div class="order_view">
<?php if($TPL_VAR["orderInfo"]["gift"]){?>
	<h3><?php echo __('사은품정보')?></h3>
	<div class="view_box2">
		<ul class="orinfo">
<?php if((is_array($TPL_R1=$TPL_VAR["orderInfo"]["gift"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
			<li>
				<dl>
					<dd class="img"><?php echo $TPL_V1["imageUrl"]?></dd>
					<dt><?php echo $TPL_V1["giftNm"]?> (<?php echo __('%s 개',$TPL_V1["giveCnt"])?>) <?php if($TPL_V1["presentTitle"]){?>|&nbsp;<?php echo $TPL_V1["presentTitle"]?><?php }?></dt>
				</dl>
<?php }}?>
		</ul>
	</div>
<?php }?>
	<h3><?php echo __('결제정보')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('상품가격')?></dt>
			<dd><?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalGoodsPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
		</dl>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
		<dl>
			<dt><?php echo __('총 무게')?></dt>
			<dd>
				<?php echo number_format($TPL_VAR["orderInfo"]["totalDeliveryWeight"], 2)?>kg
				<?php echo __('상품무게 %s kg + 박스무게 %s kg',number_format($TPL_VAR["orderInfo"]["deliveryWeightInfo"]["goods"], 2),number_format($TPL_VAR["orderInfo"]["deliveryWeightInfo"]["box"], 2))?>

			</dd>
		</dl>
<?php }?>
		<dl>
			<dt><?php echo __('배송비')?></dt>
			<dd>
				<?php echo gd_global_order_currency_display($TPL_VAR["orderDeliveryInfo"]["deliveryPolicyCharge"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?>

<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'){?>
<?php if($TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"]){?>
				<ul class="discount"><li><?php if((is_array($TPL_R1=$TPL_VAR["orderDeliveryInfo"]["orderInfoCharge"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1== 0){?><?php echo __('메인 배송지')?><?php }else{?>, <?php echo __('추가 배송지')?><?php echo $TPL_I1?><?php }?> : <?php echo gd_global_currency_display(gd_isset($TPL_V1))?>

<?php }}?></li></ul>
<?php }?>
<?php }?>
			</dd>
		</dl>
<?php if($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]> 0){?>
		<dl>
			<dt><?php echo __('지역별 배송비')?></dt>
			<dd><?php echo gd_global_currency_display(gd_isset($TPL_VAR["orderDeliveryInfo"]["deliveryAreaCharge"]))?></dd>
		</dl>
<?php }?>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
		<dl>
			<dt><?php echo __('해외배송 보험료')?></dt>
			<dd><?php echo gd_global_order_currency_display(gd_isset($TPL_VAR["orderInfo"]["totalDeliveryInsuranceFee"]),$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></dd>
		</dl>
<?php }?>
		<dl>
			<dt><?php echo __('할인금액')?></dt>
			<dd>
				<?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["useMileage"]+$TPL_VAR["orderInfo"]["useDeposit"]+$TPL_VAR["orderInfo"]["totalGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"]+$TPL_VAR["orderInfo"]["totalEnuriDcPrice"]+$TPL_VAR["orderInfo"]["totalMyappDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?>

				<ul class="discount">
					<li><?php echo __('상품')?>

						<strong>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalGoodsDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong>
					</li>
<?php if($TPL_VAR["orderInfo"]["totalMyappDcPrice"]> 0){?>
					<li><?php echo __('모바일앱')?>

						<strong>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalMyappDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong>
					</li>
<?php }?>
<?php if(gd_is_login()==true){?>
<?php if($TPL_VAR["orderInfo"]["useMileage"]> 0){?>
					<li><?php echo $TPL_VAR["mileageUse"]["name"]?>

						<strong>(-) <?php echo gd_global_money_format($TPL_VAR["orderInfo"]["useMileage"])?> <?php echo $TPL_VAR["mileageUse"]["unit"]?></strong>
					</li>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["useDeposit"]> 0){?>
					<li><?php echo $TPL_VAR["depositUse"]["name"]?>

						<strong>(-) <?php echo gd_global_money_format($TPL_VAR["orderInfo"]["useDeposit"])?> <?php echo $TPL_VAR["depositUse"]["unit"]?></strong>
					</li>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"])> 0){?>
					<li><?php echo __('회원')?>

						<strong>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalMemberDcPrice"]+$TPL_VAR["orderInfo"]["totalMemberOverlapDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong>
					</li>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"])> 0){?>
					<li><?php echo __('쿠폰')?>

						<strong>(-) <?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["totalCouponOrderDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponGoodsDcPrice"]+$TPL_VAR["orderInfo"]["totalCouponDeliveryDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong>
					</li>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["totalEnuriDcPrice"]!= 0){?>
					<li><?php echo __('운영자 추가 할인')?>

						<strong>
<?php if($TPL_VAR["orderInfo"]["totalEnuriDcPrice"]< 0){?>
							(+)
<?php }else{?>
							(-)
<?php }?>
							<?php echo gd_global_order_currency_display($TPL_VAR["orderInfo"]["absTotalEnuriDcPrice"],$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?>

						</strong>
					</li>
<?php }?>
<?php }?>
				</ul>
			</dd>
		</dl>
		<dl>
			<dt><?php echo __('결제금액')?></dt>
			<dd>
				<?php echo gd_global_currency_symbol()?>

				<strong><?php echo gd_global_money_format($TPL_VAR["orderInfo"]["settlePrice"])?></strong>
				<?php echo gd_global_currency_string()?>

				<ul class="order_ul">
					<li><strong><?php echo $TPL_VAR["orderInfo"]["settleName"]?></strong></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["settleKind"])=='gb'){?>
					<li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 0])?></li>
					<li><?php echo __('입금계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 1])?></li>
					<li><?php echo __('예금주명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankAccount"][ 2])?></li>
					<li><?php echo __('입금금액')?> :	<strong class="c_red"><?php echo gd_global_order_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]),$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong></li>
					<li><?php echo __('입금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["bankSender"])?></li>
<?php }else{?>

<?php if(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='c'){?>
					<li><?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='b'){?>
					<li><?php echo __('이체은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='v'){?>
					<li><?php echo __('입금은행')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
					<li><?php echo __('가상계좌')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 1])?></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])!=''){?>
					<li><?php echo __('예금자명')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 2])?></li>
<?php }?>

<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
					<li><?php echo __('송금일자')?> : <?php echo gd_date_format('Y-m-d',gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0]))?> <?php echo __('까지')?><li>
<?php }?>
					<li><?php echo __('입금금액')?> : <strong class="c_red"><?php echo gd_global_order_currency_display(gd_isset($TPL_VAR["orderInfo"]["settlePrice"]),$TPL_VAR["orderInfo"]["exchangeRate"],$TPL_VAR["orderInfo"]["currencyPolicy"])?></strong></li>
<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["settleMethod"])=='h'){?>
					<li><?php echo __('통신사')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleNm"][ 0])?></li>
<?php if(gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])!=''){?>
					<li><?php echo __('결제 휴대폰 번호')?> : <?php echo gd_isset($TPL_VAR["orderInfo"]["pgSettleCd"][ 0])?></li>
<?php }?>
<?php }?>
<?php }?>
				</ul>
			</dd>
		</dl>
<?php if(substr($TPL_VAR["orderInfo"]["settleKind"], 0, 1)=='o'){?>
		<dl>
			<dt><?php echo __('승인금액')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["overseasSettleCurrency"]?> <?php echo $TPL_VAR["orderInfo"]["overseasSettlePrice"]?></dd>
		</dl>
<?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["orderStatus"])=='f'&&empty($TPL_VAR["orderInfo"]["pgFailReason"])===false){?>
		<dl>
			<dt><?php echo __('결제 실패 사유')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["pgFailReason"]?></dd>
		</dl>
<?php }?>
<?php if(gd_is_login()==true&&$TPL_VAR["orderInfo"]["totalMileage"]> 0&&gd_use_mileage()){?>
		<dl>
			<dt><?php echo __('적립')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></dt>
			<dd>
				<ul class="order_ul">
					<li><strong><?php echo __('적립 예정')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong></li>
<?php if(($TPL_VAR["orderInfo"]["totalGoodsMileage"]+$TPL_VAR["orderInfo"]["totalMemberMileage"])> 0){?>
					<li><strong><?php echo __('구매')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong> <?php echo gd_global_money_format($TPL_VAR["orderInfo"]["totalGoodsMileage"]+$TPL_VAR["orderInfo"]["totalMemberMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></li>
<?php }?>
<?php if( 0> 0){?>
					<li><strong><?php echo __('후기작성')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong> <?php echo gd_global_money_format( 0)?><?php echo $TPL_VAR["mileageUse"]["unit"]?></li>
<?php }?>
<?php if(($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]+$TPL_VAR["orderInfo"]["totalCouponOrderMileage"])> 0){?>
					<li><strong><?php echo __('추가')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong> <?php echo gd_global_money_format($TPL_VAR["orderInfo"]["totalCouponGoodsMileage"]+$TPL_VAR["orderInfo"]["totalCouponOrderMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></li>
<?php }?>
					<li><strong><?php echo __('총')?> <?php echo $TPL_VAR["mileageUse"]["name"]?></strong> <?php echo gd_global_money_format($TPL_VAR["orderInfo"]["totalMileage"])?><?php echo $TPL_VAR["mileageUse"]["unit"]?></li>
				</ul>
			</dd>
		</dl>
<?php }?>
	</div>
<?php if($TPL_VAR["isHandle"]&&$TPL_VAR["totalRefundPrice"]> 0){?>
	<h3><?php echo __('환불 정보')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('환불 금액')?></dt>
			<dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundPrice"])?></strong><?php echo gd_global_currency_string()?></dd>
		</dl>
		<dl>
			<dt><?php echo __('환불 내역')?></dt>
			<dd>
<?php if($TPL_VAR["totalCompleteCashPrice"]> 0){?>
				<?php echo __('현금')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteCashPrice"])?></strong><?php echo gd_global_currency_string()?><br />
<?php }?>
<?php if($TPL_VAR["totalCompletePgPrice"]> 0){?>
				PG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompletePgPrice"])?></strong><?php echo gd_global_currency_string()?><br />
<?php }?>
<?php if($TPL_VAR["totalCompleteDepositPrice"]> 0){?>
				<?php echo __('예치금')?>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteDepositPrice"])?></strong><?php echo gd_global_currency_string()?><br />(<?php echo __('사용 예치금 환불')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundUseDeposit"])?></strong><?php echo gd_global_currency_string()?>)<br />
<?php }?>
<?php if($TPL_VAR["totalRefundUseMileage"]> 0){?>
				<?php echo __('마일리지')?>&nbsp;:&nbsp;&nbsp;<?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalRefundUseMileage"])?></strong><?php echo gd_global_currency_string()?><br />
<?php }?>
<?php if($TPL_VAR["totalCompleteMileagePrice"]> 0){?>
				<?php echo __('기타')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalCompleteMileagePrice"])?></strong><?php echo gd_global_currency_string()?><br />
<?php }?>
			</dd>
		</dl>
<?php if($TPL_VAR["totalCompleteCashPrice"]> 0){?>
		<dl>
			<dt><?php echo __('환불 계좌정보')?></dt>
<?php if($TPL_VAR["totalRefundData"]){?>
<?php if($TPL_totalRefundData_1){foreach($TPL_VAR["totalRefundData"] as $TPL_V1){?>
<?php if($TPL_V1["account"]){?>
			<dd><?php echo nl2br($TPL_V1["bank"])?> | <?php echo nl2br($TPL_V1["depositor"])?><br /><?php echo nl2br($TPL_V1["account"])?><br /></dd>
<?php }?>
<?php }}?>
<?php }?>
		</dl>
<?php }?>
	</div>
<?php }?>

<?php if($TPL_VAR["isExchangeHandle"]){?>
<?php if($TPL_exchangeHandleData_1){foreach($TPL_VAR["exchangeHandleData"] as $TPL_V1){?>
<?php if($TPL_V1["ehDifferencePrice"]< 0){?>
	<h3><?php echo __('교환 추가결제 정보')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('추가결제 금액')?></dt>
			<dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_V1["ehAbsDifferencePrice"])?></strong><?php echo gd_global_currency_string()?></dd>
		</dl>
		<dl>
			<dt><?php echo __('입금자명')?></dt>
			<dd><?php echo $TPL_V1["ehSettleName"]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('결제계좌정보')?></dt>
			<dd><?php echo $TPL_V1["ehSettleBankAccountInfo"]?></dd>
		</dl>
	</div>
<?php }else{?>
	<h3><?php echo __('교환환불 정보')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('환불금액')?></dt>
			<dd><?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_V1["ehAbsDifferencePrice"])?></strong><?php echo gd_global_currency_string()?></dd>
		</dl>
		<dl>
			<dt><?php echo __('환불수단')?></dt>
			<dd><?php echo $TPL_V1["ehRefundNameStr"]?></dd>
		</dl>
<?php if($TPL_V1["ehRefundMethod"]=='bank'){?>
		<dl>
			<dt><?php echo __('환불계좌정보')?></dt>
			<dd><?php echo $TPL_V1["ehRefundBankName"]?> / <?php echo $TPL_V1["ehRefundBankAccountNumber"]?> / <?php echo $TPL_V1["ehRefundName"]?></dd>
		</dl>
<?php }?>
	</div>
<?php }?>
<?php }}?>
<?php }?>

<?php if(gd_isset($TPL_VAR["orderInfo"]["settleReceipt"])){?>
	<h3><?php echo __('결제영수증')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('영수증 조회')?></dt>
			<dd><button type="button" class="cash_btn1" onclick="gd_pg_receipt_view('<?php echo $TPL_VAR["orderInfo"]["settleReceipt"]?>', '<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>');"><?php echo __('결제영수증 조회')?></button></dd>
		</dl>
	</div>
<?php }?>

<?php if((empty($TPL_VAR["orderInfo"]["cash"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='r')||($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
	<h3><?php echo __('현금영수증')?></h3>
	<div class="view_box">

<?php if((empty($TPL_VAR["orderInfo"]["cash"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='r')){?>
		<dl>
			<dt><?php echo __('발행 용도')?></dt>
			<dd>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["useFl"])=='d'){?><?php echo __('소득공제용')?><?php }?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["useFl"])=='e'){?><?php echo __('지출증빙용')?><?php }?>
			</dd>
		</dl>
		<dl>
			<dt><?php echo __('발행 현황')?></dt>
			<dd>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='r'){?><?php echo __('발급요청')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='y'){?><?php echo __('발행완료')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='c'){?><?php echo __('발행취소')?>

<?php }else{?><?php echo __('발행오류')?>

<?php }?>
			</dd>
		</dl>
<?php if(gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='y'||gd_isset($TPL_VAR["orderInfo"]["cash"]["statusFl"])=='c'){?>
		<dl>
			<dt><?php echo __('영수증 조회')?></dt>
			<dd><button type="button" class="cash_btn1" onclick="gd_pg_receipt_view('cash', '<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>');"><?php echo __('현금영수증 조회')?></button></dd>
		</dl>
<?php }?>
<?php }?>

<?php if(($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
<?php if($TPL_VAR["receipt"]['periodFl']=='y'){?>
		<div class="pay">
			<div class="btn_center">
				<button class="cash_btn btn_cash_receipt_request" data-order-no="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>" data-receipt-cashfl="<?php echo $TPL_VAR["receipt"]['cashFl']?>" data-receipt-periodfl="<?php echo $TPL_VAR["receipt"]['periodFl']?>" data-toggle="modal" data-target="#popupCashReceiptRequest"><?php echo __('현금영수증 발행')?></button>
			</div>
		</div>
<?php }else{?>
		<p><?php echo __('발급불가')?> (※ <?php echo __('발급요청은 결제완료 후 %s일 이내에만 가능합니다.',$TPL_VAR["receipt"]['periodDay'])?>)</p>
<?php }?>
<?php }?>

	</div>
<?php }?>


<?php if((empty($TPL_VAR["orderInfo"]["tax"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='t')||($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
	<h3><?php echo __('세금계산서')?></h3>
	<div class="view_box">
<?php if((empty($TPL_VAR["orderInfo"]["tax"])===false&&$TPL_VAR["orderInfo"]["receiptFl"]=='t')){?>
		<dl>
			<dt><?php echo __('발행현황')?></dt>
			<dd>
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='r'){?><?php echo __('발급요청')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'){?><?php echo __('발행완료')?>

<?php }elseif(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='c'){?><?php echo __('발행취소')?>

<?php }else{?><?php echo __('발행오류')?>

<?php }?>
			</dd>
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'){?>
<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["issueFl"])=='g'){?>
		</dl>
		<dl>
			<dt><?php echo __('증빙조회')?></dt>
			<dd><button type="button" class="tax_invoice_btn1 btn_tax_invoice"><?php echo __('세금계산서 조회')?></button></dd>
		</dl>
<?php }else{?>
		<p>* <?php echo __('전자세금계산서는 주문자정보의 이메일주소로 발송됩니다. %s발행완료된 후 전자세금계산서 메일이 확인되지 않으면 고객센터로 문의주시기 바랍니다.','<br/>&nbsp;&nbsp;')?></p>
<?php }?>
<?php }?>

<?php }?>
<?php if(($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
<?php if(($TPL_VAR["receipt"]["limitDateFl"]=='y')){?>
		<div class="pay">
			<div class="btn_center">
				<button class="tax_invoice_btn btn_tax_invoice_request" data-order-no="<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>" data-receipt-taxfl="<?php echo $TPL_VAR["receipt"]['taxFl']?>" data-toggle="modal" data-target="#popupTaxInvoiceRequest"><?php echo __('세금계산서 발행')?></button>
			</div>
<?php if($TPL_VAR["taxinvoiceInfo"]){?>
			<div><?php echo $TPL_VAR["taxinvoiceInfo"]?></div>
<?php }?>
		</div>
<?php }else{?>
		<p><?php echo __('발급불가')?> (※ <?php echo __('결제완료 기준 다음 달 %s 일까지 신청 가능 가능합니다.',$TPL_VAR["receipt"]['taxInvoiceLimitDate'])?>)</p>
<?php if($TPL_VAR["taxinvoiceDeadline"]){?>
		<div><?php echo $TPL_VAR["taxinvoiceDeadline"]?></div>
<?php }?>
<?php }?>
<?php }?>
	</div>
<?php }?>

	<h3><?php echo __('주문정보')?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('주문자')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["orderName"]?></dd>
		</dl>
<?php if(gd_is_login()==true&&!$TPL_VAR["gGlobal"]["isFront"]){?>
		<dl>
			<dt><?php echo __('주소')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
				(<?php echo gd_isset($TPL_VAR["orderInfo"]["orderZonecode"])?>) <?php echo gd_isset($TPL_VAR["orderInfo"]["orderAddressSub"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderAddress"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderState"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderCity"])?>, <?php echo gd_isset($TPL_VAR["orderInfo"]["orderCountry"])?>

<?php }else{?>
				(<?php echo $TPL_VAR["orderInfo"]["orderZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["orderAddress"]?> <?php echo $TPL_VAR["orderInfo"]["orderAddressSub"]?>

<?php }?>
			</dd>
		</dl>
<?php }?>
		<dl>
			<dt><?php echo __('전화번호')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
				(+<?php echo gd_isset($TPL_VAR["orderInfo"]["orderPhonePrefix"])?>)
<?php }?>
				<?php echo $TPL_VAR["orderInfo"]["orderPhone"]?>

			</dd>
		</dl>
		<dl>
			<dt><?php echo __('휴대폰 번호')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
				(+<?php echo gd_isset($TPL_VAR["orderInfo"]["orderCellPhonePrefix"])?>)
<?php }?>
				<?php echo $TPL_VAR["orderInfo"]["orderCellPhone"]?>

			</dd>
		</dl>
		<dl>
			<dt><?php echo __('이메일')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["orderEmail"]?></dd>
		</dl>
	</div>

	<h3><?php echo __('배송정보')?></h3>
<?php if($TPL_VAR["orderInfo"]["deliveryVisit"]!='y'){?>
<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'&&$TPL_VAR["multiOrderInfo"]){?>
	<h3><?php echo __('메인 배송지')?></h3>
<?php }?>
	<div class="view_box">
		<dl>
			<dt><?php echo __('수령자')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["receiverName"]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('주소')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
				(<?php echo $TPL_VAR["orderInfo"]["receiverZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["receiverAddressSub"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverAddress"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverState"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverCity"]?>, <?php echo $TPL_VAR["orderInfo"]["receiverCountry"]?>

<?php }else{?>
				(<?php echo $TPL_VAR["orderInfo"]["receiverZonecode"]?>) <?php echo $TPL_VAR["orderInfo"]["receiverAddress"]?> <?php echo $TPL_VAR["orderInfo"]["receiverAddressSub"]?>

<?php }?>
			</dd>
		</dl>
		<dl>
			<dt><?php echo __('전화번호')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["orderInfo"]["receiverPhonePrefix"]?>)<?php }?>
				<?php echo $TPL_VAR["orderInfo"]["receiverPhone"]?>

			</dd>
		</dl>
		<dl>
			<dt><?php echo __('휴대폰 번호')?></dt>
			<dd><?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["orderInfo"]["receiverCellPhonePrefix"]?>)<?php }?>
				<?php echo $TPL_VAR["orderInfo"]["receiverCellPhone"]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('남기실 말씀')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["orderMemo"]?></dd>
		</dl>
	</div>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["deliveryVisit"]=='a'||$TPL_VAR["orderInfo"]["deliveryVisit"]=='y'){?>
	<h3>
		<?php echo __('메인 방문수령 정보')?>

		<span class="btn-shipping" style="float:right;"><a href="./layer_visit_address.php?orderNo=<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>&goodsSno=<?php echo json_encode($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["goodsSno"][$TPL_VAR["orderInfo"]["infoSno"]])?>" class="btn-open-layer" data-toggle="modal" data-cache="false" data-target="#popupBoard" >상품정보</a></span></span>
	</h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('방문 수령지 주소')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["orderInfo"]["infoSno"]][ 0]?> <?php if(count($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["orderInfo"]["infoSno"]])> 1){?>외 <?php echo (count($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["orderInfo"]["infoSno"]])- 1)?>건<?php }?></dd>
		</dl>
		<dl>
			<dt><?php echo __('방문자 정보')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["visitName"]?> / <?php echo $TPL_VAR["orderInfo"]["visitPhone"]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('메모')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["visitMemo"]?></dd>
		</dl>
	</div>
<?php }?>
<?php if($TPL_VAR["orderInfo"]["multiShippingFl"]=='y'&&$TPL_VAR["multiOrderInfo"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["multiOrderInfo"]["receiverNm"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {$TPL_I1=-1;foreach($TPL_R1 as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_VAR["multiOrderInfo"]["deliveryVisit"][$TPL_I1]!='y'){?>
	<h3><?php echo __('추가 배송지')?> <?php echo $TPL_I1+ 1?></h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('수령자')?></dt>
			<dd><?php echo $TPL_VAR["multiOrderInfo"]["receiverNm"][$TPL_I1]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('주소')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
				(<?php echo $TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1]?>) <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverState"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverCity"][$TPL_I1]?>, <?php echo $TPL_VAR["multiOrderInfo"]["receiverCountry"][$TPL_I1]?>

<?php }else{?>
				(<?php echo $TPL_VAR["multiOrderInfo"]["receiverZonecode"][$TPL_I1]?>) <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddress"][$TPL_I1]?> <?php echo $TPL_VAR["multiOrderInfo"]["receiverAddressSub"][$TPL_I1]?>

<?php }?>
			</dd>
		</dl>
		<dl>
			<dt><?php echo __('전화번호')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["multiOrderInfo"]["receiverPhonePrefix"][$TPL_I1]?>)<?php }?>
				<?php echo $TPL_VAR["multiOrderInfo"]["receiverPhone"][$TPL_I1]?>

			</dd>
		</dl>
		<dl>
			<dt><?php echo __('휴대폰 번호')?></dt>
			<dd>
<?php if($TPL_VAR["gGlobal"]["isFront"]){?>(+ <?php echo $TPL_VAR["multiOrderInfo"]["receiverCellPhonePrefix"][$TPL_I1]?>)<?php }?>
				<?php echo $TPL_VAR["multiOrderInfo"]["receiverCellPhone"][$TPL_I1]?>

			</dd>
		</dl>
		<dl>
			<dt><?php echo __('남기실 말씀')?></dt>
			<dd><?php echo $TPL_VAR["multiOrderInfo"]["orderMemo"][$TPL_I1]?></dd>
		</dl>
	</div>
<?php }?>
<?php if($TPL_VAR["multiOrderInfo"]["deliveryVisit"][$TPL_I1]!='n'){?>
	<h3>
		<?php echo __('추가 방문수령 정보')?> <?php echo $TPL_I1+ 1?>

		<span class="btn-shipping" style="float:right;"><a href="./layer_visit_address.php?orderNo=<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>&goodsSno=<?php echo json_encode($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["goodsSno"][$TPL_VAR["multiOrderInfo"]["InfoSno"][$TPL_I1]])?>" class="btn-open-layer" data-toggle="modal" data-cache="false" data-target="#popupBoard" >상품정보</a></span></span>
	</h3>
	<div class="view_box">
		<dl>
			<dt><?php echo __('방문 수령지 주소')?></dt>
			<dd><?php echo $TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["multiOrderInfo"]["InfoSno"][$TPL_I1]][ 0]?> <?php if(count($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["multiOrderInfo"]["InfoSno"][$TPL_I1]])> 1){?>외 <?php echo (count($TPL_VAR["orderInfo"]["visitDeliveryInfo"]["address"][$TPL_VAR["multiOrderInfo"]["InfoSno"][$TPL_I1]])- 1)?>건<?php }?></dd>
		</dl>
		<dl>
			<dt><?php echo __('방문자 정보')?></dt>
			<dd><?php echo $TPL_VAR["multiOrderInfo"]["visitName"][$TPL_I1]?> / <?php echo $TPL_VAR["multiOrderInfo"]["visitPhone"][$TPL_I1]?></dd>
		</dl>
		<dl>
			<dt><?php echo __('메모')?></dt>
			<dd><?php echo $TPL_VAR["multiOrderInfo"]["visitMemo"][$TPL_I1]?></dd>
		</dl>
	</div>
<?php }?>
<?php }}?>
<?php }?>

<?php if(empty($TPL_VAR["orderInfo"]["addField"])===false){?>
	<h3><?php echo __('추가정보')?></h3>
	<div class="view_box">
<?php if((is_array($TPL_R1=$TPL_VAR["orderInfo"]["addField"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
<?php if($TPL_V1["process"]=='goods'){?>
<?php if((is_array($TPL_R2=$TPL_V1["data"])&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
		<dl>
			<dt><?php echo $TPL_V1["name"]?> : <?php echo $TPL_V1["goodsNm"][$TPL_K2]?></dt>
			<dd><?php echo $TPL_V2?></dd>
		</dl>
<?php }}?>
<?php }else{?>
		<dl>
			<dt><?php echo $TPL_V1["name"]?></dt>
			<dd><?php echo $TPL_V1["data"]?></dd>
		</dl>
<?php }?>
<?php }}?>
	</div>
<?php }?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
<?php if(($TPL_VAR["receipt"]['cashFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n'&&$TPL_VAR["receipt"]['periodFl']=='y')){?>
        // 현금영수증 신청
        $('.btn_cash_receipt_request').click(function (e) {
            var params = {
                orderNo: $(this).data('order-no'),
                receiptCashFl: $(this).data('receipt-cashfl'),
                receiptPeriodFl: $(this).data('receipt-periodfl'),
            };

            $('#popupCashReceiptRequest').modal({
                remote: '/mypage/layer_cash_receipt_request.php',
                cache: false,
                type : 'post',
                params: params,
                show: true
            });
        });
<?php }?>

<?php if(gd_isset($TPL_VAR["orderInfo"]["tax"]["statusFl"])=='y'&&gd_isset($TPL_VAR["orderInfo"]["tax"]["issueFl"])=='g'){?>
        // 세금계산서 보기
        $('.btn_tax_invoice').click(function (e) {
            var win = gd_popup({
                url: '../share/show_tax_invoice.php?orderNo=<?php echo $TPL_VAR["orderInfo"]["orderNo"]?>'
                , target: 'tax_invoice'
                , width: 750
                , height: 600
                , resizable: 'yes'
                , scrollbars: 'yes'
            });
            win.focus();
            return win;

        });
<?php }?>

<?php if(($TPL_VAR["receipt"]['taxFl']=='y'&&$TPL_VAR["orderInfo"]["receiptFl"]=='n')){?>
        // 세금계산서 신청
        $('.btn_tax_invoice_request').click(function (e) {
            var params = {
                orderNo: $(this).data('order-no'),
                receiptTaxFl: $(this).data('receipt-taxfl'),
            };

            $('#popupTaxInvoiceRequest').modal({
                remote: '/mypage/layer_tax_invoice_request.php',
                cache: false,
                type : 'post',
                params: params,
                show: true
            });
        });
<?php }?>
    });
</script>

<?php $this->print_("footer",$TPL_SCP,1);?>