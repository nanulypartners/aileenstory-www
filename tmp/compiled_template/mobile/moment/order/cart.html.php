<?php /* Template_ 2.2.7 2020/02/14 20:39:50 /www/aileen8919_godomall_com/data/skin/mobile/moment/order/cart.html 000041314 */ 
if (is_array($TPL_VAR["cartInfo"])) $TPL_cartInfo_1=count($TPL_VAR["cartInfo"]); else if (is_object($TPL_VAR["cartInfo"]) && in_array("Countable", class_implements($TPL_VAR["cartInfo"]))) $TPL_cartInfo_1=$TPL_VAR["cartInfo"]->count();else $TPL_cartInfo_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="cart">
	<form id="frmCart" name="frmCart" method="post" target="ifrmProcess">
		<input type="hidden" name="mode" value=""/>
		<input type="hidden" name="cart[cartSno]" value=""/>
		<input type="hidden" name="cart[goodsNo]" value=""/>
		<input type="hidden" name="cart[goodsCnt]" value=""/>
		<input type="hidden" name="cart[addGoodsNo]" value=""/>
		<input type="hidden" name="cart[addGoodsCnt]" value=""/>
<?php if($TPL_VAR["couponUse"]=='y'){?>
		<input type="hidden" name="cart[couponApplyNo]" value=""/>
<?php }?>
		<input type="hidden" name="useBundleGoods" value="1" />
<?php if($TPL_VAR["cartInfo"]){?>
<?php if($TPL_cartInfo_1){foreach($TPL_VAR["cartInfo"] as $TPL_K1=>$TPL_V1){?>
		<div class="cart_content_box">
			<!-- 전체채크 -->
			<div class="allchk">
				<div class="inp_chk">
					<input type="checkbox" id="allCheck_<?php echo $TPL_K1?>"  class="sp gd_select_all_goods" checked="checked" data-key="<?php echo $TPL_K1?>" data-target-id="cartSno<?php echo $TPL_K1?>_" data-target-form="#frmCart" >
<?php if($TPL_VAR["cartScmCnt"]> 1&&!$TPL_VAR["gGlobal"]["isFront"]){?>
					<label class="buyall" for="allCheck_<?php echo $TPL_K1?>"><?php echo $TPL_VAR["cartScmInfo"][$TPL_K1]['companyNm']?> <?php echo __('배송상품')?></label>
<?php }?>
				</div>
			</div>
			<ul class="my_goods">
<?php if((is_array($TPL_R2=$TPL_V1)&&!empty($TPL_R2)) || (is_object($TPL_R2) && in_array("Countable", class_implements($TPL_R2)) && $TPL_R2->count() > 0)) {foreach($TPL_R2 as $TPL_K2=>$TPL_V2){?>
<?php if((is_array($TPL_R3=$TPL_V2)&&!empty($TPL_R3)) || (is_object($TPL_R3) && in_array("Countable", class_implements($TPL_R3)) && $TPL_R3->count() > 0)) {$TPL_I3=-1;foreach($TPL_R3 as $TPL_V3){$TPL_I3++;?>
				<li style="margin-bottom:0;<?php if(($TPL_V3["equalGoodsNo"]!==true&&($TPL_V3["goodsDeliveryFl"]=='y'||($TPL_V3["goodsDeliveryFl"]=='n'&&$TPL_V3["sameGoodsDeliveryFl"]=='y')))===false){?>margin-top:10px;<?php }?>">
					<div class="btn_list">
<?php if(($TPL_VAR["couponUse"]=='y'&&$TPL_VAR["couponConfig"]['chooseCouponMemberUseType']!='member')&&$TPL_V3["couponBenefitExcept"]=='n'){?>
						<div id="coupon_apply_<?php echo $TPL_V3["sno"]?>" class="coupon_btn">
<?php if(gd_is_login()===false){?>
							<button class="cart_coupon_apply btn_alert_login"><?php echo __('쿠폰적용')?></button>
<?php }else{?>
<?php if($TPL_V3["memberCouponNo"]){?>
							<button class="cart_coupon_cancel btn_coupon_cancel"  data-cartsno="<?php echo $TPL_V3["sno"]?>"  ><?php echo __('쿠폰취소')?></button>
							<button data-action="couponApplyLayer" class="cart_coupon_modify js_coupon_order_apply" data-cartsno="<?php echo $TPL_V3["sno"]?>" ><?php echo __('쿠폰변경')?></button>
<?php }else{?>
							<button data-action="couponApplyLayer" class="cart_coupon_apply js_coupon_order_apply" data-cartsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('쿠폰적용')?></button>
<?php }?>
<?php }?>
						</div>
<?php }?>
						<div class="option_btn">
							<button data-key="optionViewLayer" class="cart_option_modify js_option_layer"  <?php if($TPL_V3["memberCouponNo"]){?> data-coupon='use' <?php }else{?> data-goodsno="<?php echo $TPL_V3["goodsNo"]?>" data-sno="<?php echo $TPL_V3["sno"]?>"  <?php }?>>
							<?php echo __('옵션변경')?>

							</button>
						</div>
						<div class="select_del_box"><button type="button" class="cart_onedel_btn del" data-target-id="cartSno<?php echo $TPL_K1?>_<?php echo $TPL_V3["sno"]?>" data-cartsno="<?php echo $TPL_V3["sno"]?>"><?php echo __('삭제')?></button></div>
					</div>
					<div class="mid_box">
						<div class="left_box">
							<div class="inp_chk">
							   <input type="checkbox" name="cartSno[]" id="cartSno<?php echo $TPL_K1?>_<?php echo $TPL_V3["sno"]?>" value="<?php echo $TPL_V3["sno"]?>" class="sp" checked="checked" data-price="<?php echo $TPL_V3["price"]["goodsPriceSubtotal"]?>" data-mileage="<?php echo ($TPL_V3["mileage"]["goodsMileage"]+$TPL_V3["mileage"]["memberMileage"])?>" data-goodsdc="<?php echo $TPL_V3["price"]["goodsDcPrice"]?>" data-memberdc="<?php echo ($TPL_V3["price"]["memberDcPrice"]+$TPL_V3["price"]["memberOverlapDcPrice"])?>" data-coupondc="<?php echo $TPL_V3["price"]["couponGoodsDcPrice"]?>" data-possible="<?php echo $TPL_V3["orderPossible"]?>" data-goods-key="<?php echo $TPL_V3["goodsKey"]?>" data-goods-no="<?php echo $TPL_V3["goodsNo"]?>" data-goods-nm="<?php echo gd_remove_only_tag($TPL_V3["goodsNm"])?>" data-option-nm="<?php echo $TPL_V3["optionNm"]?>" data-fixed-sales="<?php echo $TPL_V3["fixedSales"]?>" data-sales-unit="<?php echo $TPL_V3["salesUnit"]?>" data-fixed-order-cnt="<?php echo $TPL_V3["fixedOrderCnt"]?>" data-min-order-cnt="<?php echo $TPL_V3["minOrderCnt"]?>" data-max-order-cnt="<?php echo $TPL_V3["maxOrderCnt"]?>" data-default-goods-cnt="<?php echo $TPL_V3["goodsCnt"]?>"/>
									<label for="cartSno<?php echo $TPL_K1?>_<?php echo $TPL_V3["sno"]?>" class="blind"><?php echo __('선택')?></label>
							</div>
						</div>
						<div class="right_box">
							<div class="info">
								<a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V3["goodsNo"]?>">
<?php if($TPL_V3["orderPossibleMessageList"]){?>
									<div class="order_possible_list">
										<div class="inner_title">
											<span class="icon_exclamationmark"></span><strong><?php echo __('구매 이용 조건 안내')?></strong><button type="button" class="pm_more_btn js_possible_list_toggle">더보기</button>
											<ul class="dn">
												<li class="title"><?php echo __('결제 제한 조건 사유')?></li>
<?php if((is_array($TPL_R4=$TPL_V3["orderPossibleMessageList"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
												<li><?php echo $TPL_V4?></li>
<?php }}?>
											</ul>
										</div>
									</div>
<?php }?>
									<div>
										<div class="itemhead">
											<div class="thmb_box">
												<div class="thmb"><?php echo $TPL_V3["goodsImage"]?>

<?php if($TPL_V3["timeSaleFl"]){?>
													<div class="timesale_box">
														<img src="/data/skin/mobile/moment/img/icon/icon_timesale.png" alt="<?php echo __('타임세일')?>">
														<span class="timetext"><?php echo __('타임세일')?></span>
													</div>
<?php }?>
												</div>
											</div>
										</div>
										<div class="itembody">
<?php if($TPL_V3["duplicationGoods"]==='y'){?>
											<p class="excl">
												<span class="icon_exclamationmark"></span>
												<strong class="c_red"><?php echo __('중복 상품')?></strong>
											</p>
<?php }?>
											<p class="name"><?php echo $TPL_V3["goodsNm"]?></p>
											<p><?php echo __('주문수량')?> : <?php echo number_format($TPL_V3["goodsCnt"])?></p>
<?php if((is_array($TPL_R4=$TPL_V3["option"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {$TPL_S4=count($TPL_R4);$TPL_I4=-1;foreach($TPL_R4 as $TPL_V4){$TPL_I4++;?>
<?php if($TPL_V4["optionName"]){?>
											<p>
												<?php echo $TPL_V4["optionName"]?> :
												<?php echo $TPL_V4["optionValue"]?>

<?php if((($TPL_I4+ 1)==$TPL_S4)&&$TPL_V4["optionPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
												(<?php if($TPL_V4["optionPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionPrice"])?>)
<?php }?>
<?php if((($TPL_I4+ 1)==$TPL_S4)){?>
<?php if(empty($TPL_V4["optionSellStr"])===false){?>[<?php echo $TPL_V4["optionSellStr"]?>]<?php }?><?php if(empty($TPL_V4["optionDeliveryStr"])===false){?>[<?php echo $TPL_V4["optionDeliveryStr"]?>]<?php }?>
<?php }?>
											</p>
<?php }?>
<?php }}?>
<?php if((is_array($TPL_R4=$TPL_V3["optionText"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
<?php if($TPL_V4["optionValue"]){?>
											<p>
												<?php echo $TPL_V4["optionName"]?> :
												<?php echo $TPL_V4["optionValue"]?>

<?php if($TPL_V4["optionTextPrice"]!= 0&&$TPL_VAR["optionPriceFl"]=='y'){?>
												(<?php if($TPL_V4["optionTextPrice"]> 0){?>+<?php }?><?php echo gd_global_currency_display($TPL_V4["optionTextPrice"])?>)
<?php }?>
											</p>
<?php }?>
<?php }}?>
										<strong class="prc">
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
											<?php echo $TPL_V3["goodsPriceString"]?>

<?php }else{?>
											<?php echo gd_global_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?>

<?php if($TPL_VAR["gGlobal"]["isFront"]){?>
											<br><small><?php echo gd_global_add_currency_display(($TPL_V3["price"]['goodsPriceSum']+$TPL_V3["price"]['optionPriceSum']+$TPL_V3["price"]['optionTextPriceSum']))?></small>
<?php }?>
<?php }?>
										</strong>
											<div style="text-align:right; color:#333;">
<?php if($TPL_V3["goodsDeliveryFl"]==='y'){?>
<?php if($TPL_I3=='0'){?>
												<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethod']?>

<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['fixFl']==='free'){?>
												<?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreeFl']==='y'){?>
												<?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])===false){?>
												<!--(<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectFl']==='later'){?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])===false){?>
												<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])?>

<?php }?>
<?php }else{?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])===true){?>
												<?php echo __('무료배송')?>

<?php }else{?>
												<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText'])===false){?>
												(<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryMethodFlText']?>-<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectFl']==='later'&&empty($TPL_VAR["setDeliveryInfo"][$TPL_K2]['goodsDeliveryCollectPrice'])===false){?><?php echo __('착불')?><?php }else{?><?php echo __('선결제')?><?php }?>)
<?php }?>
<?php }?>
<?php }else{?>
<?php if($TPL_V3["sameGoodsDeliveryFl"]==='y'){?>
<?php if($TPL_V3["equalGoodsNo"]===true){?>
												<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryMethod']?>

<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['fixFl']==='free'){?>
												<?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryWholeFreeFl']==='y'){?>
												<?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryWholeFreePrice'])===false){?>
												<!--(<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryCollectFl']==='later'){?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryCollectPrice'])===false){?>
												<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryCollectPrice'])?>

<?php }?>
<?php }else{?>
<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryPrice'])===true){?>
												<?php echo __('무료배송')?>

<?php }else{?>
												<?php echo gd_global_currency_display($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryMethodFlText'])===false){?>
												(<?php echo $TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryMethodFlText']?>-<?php if($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryCollectFl']==='later'&&empty($TPL_VAR["setDeliveryInfo"][$TPL_K2][$TPL_V3["goodsNo"]]['goodsDeliveryCollectPrice'])===false){?><?php echo __('착불')?><?php }else{?><?php echo __('선결제')?><?php }?>)
<?php }?>
<?php }?>
<?php }else{?>
												<?php echo $TPL_V3["goodsDeliveryMethod"]?>

<?php if($TPL_V3["goodsDeliveryFixFl"]==='free'){?>
												<?php echo __('무료배송')?>

<?php }else{?>
<?php if($TPL_V3["goodsDeliveryWholeFreeFl"]==='y'){?>
												<?php echo __('조건에 따른 배송비 무료')?>

<?php if(empty($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])===false){?>
												<!--<br/>(<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryWholeFreePrice'])?>)-->
<?php }?>
<?php }else{?>
<?php if($TPL_V3["goodsDeliveryCollectFl"]==='later'){?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryCollectPrice'])===false){?>
												<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryCollectPrice'])?>

<?php }?>
<?php }else{?>
<?php if(empty($TPL_V3["price"]['goodsDeliveryPrice'])===true){?>
												<?php echo __('무료배송')?>

<?php }else{?>
												<?php echo gd_global_currency_display($TPL_V3["price"]['goodsDeliveryPrice'])?>

<?php }?>
<?php }?>
<?php }?>
<?php }?>

<?php if(empty($TPL_V3["goodsDeliveryMethodFlText"])===false){?>
												(<?php echo $TPL_V3["goodsDeliveryMethodFlText"]?>-<?php if($TPL_V3["goodsDeliveryCollectFl"]==='later'&&empty($TPL_V3["price"]['goodsDeliveryCollectPrice'])===false){?><?php echo __('착불')?><?php }else{?><?php echo __('선결제')?><?php }?>)
<?php }?>
<?php }?>
<?php }?>
											</div>
										</div>
									</div>
								</a>
<?php if(empty($TPL_V3["addGoods"])===false){?>
								<div class="add_goods_box">
									<p class="add_title"><?php echo __('추가상품')?></p>
									<ul class="add_goods_list">
<?php if((is_array($TPL_R4=$TPL_V3["addGoods"])&&!empty($TPL_R4)) || (is_object($TPL_R4) && in_array("Countable", class_implements($TPL_R4)) && $TPL_R4->count() > 0)) {foreach($TPL_R4 as $TPL_V4){?>
										<li>
											<a href="#goods">
												<div class="add_goods_img"><?php echo $TPL_V4["addGoodsImage"]?></div>
												<div class="add_goods_content">
													<span class="title"><?php echo $TPL_V4["addGoodsNm"]?> <?php echo $TPL_V4["optionNm"]?></span>
<?php if($TPL_V4["optionNm"]){?>
													<div class="add_goods_option">옵션: <?php echo $TPL_V4["optionNm"]?></div>
<?php }?>
													<div class="add_goods_text">
														<span class="goods_number"><?php echo __('주문 수량')?> : <em><?php echo number_format($TPL_V4["addGoodsCnt"])?></em><?php echo __('개')?></span>
														<span class="goods_price"><em>
<?php if(empty($TPL_V3["goodsPriceString"])===false){?>
                                        					<?php echo gd_global_currency_display( 0)?>

<?php }else{?>
															<?php echo gd_global_currency_display(($TPL_V4["addGoodsPrice"]*$TPL_V4["addGoodsCnt"]))?><?php }?></em></span>
													</div>
												</div>
											</a>
										</li>
<?php }}?>
									</ul>
								</div>
<?php }?>
							</div>
						</div>
					</div>
				</li>
<?php }}?>
<?php }}?>
			</ul>
<?php if($TPL_VAR["cartScmCnt"]> 1){?>
			<div class="supplier_total">
				<span><?php echo __('총 %s개의 상품금액','<strong>'.number_format($TPL_VAR["cartScmGoodsCnt"][$TPL_K1]).'</strong>')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsPrice"][$TPL_K1])?></strong><?php echo gd_currency_string()?></span>
<?php if($TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1]> 0){?>
				<span>- <?php echo __('상품할인')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php if(($TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]+$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1])> 0){?>
				<span>- <?php echo __('회원할인')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format(($TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]+$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1]))?></strong><?php echo gd_global_currency_string()?></span>
<?php }?>

<?php if($TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1]> 0){?>
				<span>- <?php echo __('쿠폰할인')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php if($TPL_VAR["totalScmMyappDcPrice"][$TPL_K1]> 0){?>
				<span>- <?php echo __('모바일앱할인')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmMyappDcPrice"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></span>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
				<span> + <?php echo __('배송비')?> <?php echo gd_global_currency_symbol()?><strong><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsDeliveryCharge"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></span>
<?php }?>
				<span class="c_red"> = <?php echo gd_global_currency_symbol()?><strong class="total"><?php echo gd_global_money_format($TPL_VAR["totalScmGoodsPrice"][$TPL_K1]-$TPL_VAR["totalScmGoodsDcPrice"][$TPL_K1]-$TPL_VAR["totalScmMemberDcPrice"][$TPL_K1]-$TPL_VAR["totalScmMemberOverlapDcPrice"][$TPL_K1]-$TPL_VAR["totalScmCouponGoodsDcPrice"][$TPL_K1]-$TPL_VAR["totalScmMyappDcPrice"][$TPL_K1]+$TPL_VAR["totalScmGoodsDeliveryCharge"][$TPL_K1])?></strong><?php echo gd_global_currency_string()?></span>
			</div>
<?php }?>
		</div>
<?php }}?>
		<div class="my_buy">
			<dl>
				<dt><?php echo __('총 상품금액')?></dt>
				<dd><strong><?php echo gd_global_currency_symbol()?><span id="totalGoodsPrice"><?php echo gd_global_money_format($TPL_VAR["totalGoodsPrice"])?></span><?php echo gd_global_currency_string()?></strong></dd>
			</dl>
<?php if($TPL_VAR["totalGoodsDcPrice"]> 0){?>
			<dl>
				<dt><?php echo __('상품할인')?></dt>
				<dd><strong>(-) <?php echo gd_global_currency_symbol()?><span id="totalGoodsDcPrice"><?php echo gd_global_money_format($TPL_VAR["totalGoodsDcPrice"])?></span><?php echo gd_global_currency_string()?></strong></dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["totalSumMemberDcPrice"]> 0){?>
			<dl>
				<dt><?php echo __('회원할인')?></dt>
				<dd><strong>(-) <?php echo gd_global_currency_symbol()?><span id="totalMinusMember"><?php echo gd_global_money_format($TPL_VAR["totalSumMemberDcPrice"])?></span><?php echo gd_global_currency_string()?></strong></dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["totalCouponGoodsDcPrice"]> 0){?>
			<dl>
				<dt><?php echo __('쿠폰할인')?></dt>
				<dd><strong>(-) <?php echo gd_global_currency_symbol()?><span id="totalCouponGoodsDcPrice"><?php echo gd_global_money_format($TPL_VAR["totalCouponGoodsDcPrice"])?></span><?php echo gd_global_currency_string()?></strong></dd>
			</dl>
<?php }?>
<?php if($TPL_VAR["totalMyappDcPrice"]> 0){?>
			<dl>
				<dt><?php echo __('모바일앱할인')?></dt>
				<dd><strong>(-) <?php echo gd_global_currency_symbol()?><span id="totalMyappDcPrice"><?php echo gd_global_money_format($TPL_VAR["totalMyappDcPrice"])?></span><?php echo gd_global_currency_string()?></strong></dd>
			</dl>
<?php }?>
<?php if(!$TPL_VAR["gGlobal"]["isFront"]){?>
			<dl>
				<dt><?php echo __('배송비')?></dt>
				<dd id="deliveryCalculateNone">
					<strong><?php echo gd_global_currency_symbol()?><span id="totalDeliveryCharge"><?php echo gd_global_money_format($TPL_VAR["totalDeliveryCharge"])?></span><?php echo gd_global_currency_string()?></strong>
				</dd>
			</dl>
<?php if(gd_is_login()===true&&$TPL_VAR["mileage"]["useFl"]=='y'){?>
			<dl>

				<dt><?php echo __('적립예정')?> <?php echo $TPL_VAR["mileage"]["name"]?></dt>
				<dd><strong> <span id="totalGoodsMileage"><?php echo gd_global_money_format($TPL_VAR["totalMileage"])?></span> <?php echo $TPL_VAR["mileage"]["unit"]?></strong></dd>
			</dl>
<?php }?>
<?php }?>
			<dl class="total">
				<dt><?php echo __('총 합계금액')?></dt>
				<dd>
					<strong><?php echo gd_global_currency_symbol()?><span id="totalSettlePrice"><?php echo gd_global_money_format($TPL_VAR["totalSettlePrice"])?></span><?php echo gd_global_currency_string()?><br><small><?php echo gd_global_add_currency_display($TPL_VAR["totalSettlePrice"])?></small></strong>
				</dd>
			</dl>
			<span id="deliveryChargeText"></span>
		</div>
		<div class="btn_wish_bx">
			<ul class="btn_bx">
				<li><button type="button" class="cart_select_del_btn" data-action="cartDelete"><?php echo __('선택상품 삭제')?></button></li>
				<li><button type="button" class="cart_select_order_btn" data-action="orderSelect"><?php echo __('선택상품 주문')?></button></li>
			</ul>
			<button type="button" class="cart_all_order_btn" data-action="orderAll"><?php echo __('전체주문')?></button>
		</div>

		<div class="btn_payco">
			<div><?php echo $TPL_VAR["payco"]?></div>
			<div><?php echo $TPL_VAR["naverPay"]?></div>
		</div>
<?php }else{?>
		<div class="no_data">
			<p><strong><?php echo __('장바구니에 담긴 상품이 없습니다.')?></strong></p>
		</div>
<?php }?>
	</form>
</div>

<script type="text/javascript">
    <!--
    $(document).ready(function () {

        $(".my_goods .del").on('click',function (e){
            $('input:checkbox').prop("checked",false);
            $("#"+$(this).data('target-id')).prop("checked",true);
            gd_cart_process('cartDelete');
        });

        $(".btn_wish_bx button").on('click',function (e){
            if($(this).data('action') =='orderAll') {
                gd_order_all();
            } else {
                gd_cart_process($(this).data('action'));
            }
        });
<?php if($TPL_VAR["couponUse"]=='y'){?>
        // 쿠폰 적용/변경 레이어
        $('.js_coupon_order_apply').on('click', function(e){

            var params = {
                mode: 'coupon_apply',
                cartSno: $(this).data('cartsno'),
            };

            $('#popupCouponApply').modal({
                remote: '/order/layer_coupon_apply.php',
                cache: false,
                type : 'post',
                params: params,
                show: true
            });
        });

        // 쿠폰 취소
        $('.btn_coupon_cancel').bind('click', function(e){
            var cartSno = $(this).data('cartsno');
            $('[name="cart[cartSno]"]').val(cartSno);
            $('#frmCart input[name=\'mode\']').val('couponDelete');
            $('#frmCart').attr('method', 'post');
            $('#frmCart').attr('target', 'ifrmProcess');
            $('#frmCart').attr('action', '../order/cart_ps.php');
            $('#frmCart').submit();

            return false;
        });
<?php }?>
        // 로그인 서비스에 대한 체크
        $(document).on('click','.btn_alert_login',function (e){
            alert("<?php echo __('로그인하셔야 본 서비스를 이용하실 수 있습니다.')?>");
            document.location.href = "../member/login.php";
            return false;
        });
        // 숫자 체크
        //$('input[name*=\'goodsCnt\']').number_only();

<?php if(empty($TPL_VAR["cartCnt"])===false){?>
        // 선택한 상품에 따른 금액 계산
        var totalDeliveryCharge = numeral().unformat($('#totalDeliveryCharge').text());
        $('.gd_select_all_goods, input:checkbox[name="cartSno[]"]').click(function () {
            // 체크박스 전체 선택상태에 따른 체크박스 변경처리
            var checkedCount = 0;
            var $eachCheckBox = $(this).closest('.cart_content_box').find('ul.my_goods li input[name="cartSno[]"]:checkbox');
            // 전체 및 개별 상품 선택 처리
            if ($(this).hasClass('gd_select_all_goods')) {
                var allCheckFl = $(this).prop('checked');
                $eachCheckBox.each(function() {
                    $(this).prop('checked', allCheckFl);
                });
            } else {
                $eachCheckBox.each(function(idx) {
                    if ($(this).prop('checked') === true) {
                        checkedCount++;
                    }
                });
                if ($eachCheckBox.length == checkedCount) {
                    $(this).closest('.cart_content_box').find('.allchk input[id*=allCheck_]').prop('checked', true);
                } else {
                    $(this).closest('.cart_content_box').find('.allchk input[id*=allCheck_]').prop('checked', false);
                }
            }

            window.setTimeout(function(){
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: "../order/cart_ps.php",
                    data: "mode=cartSelectCalculation&" + $('#frmCart input:checkbox[name="cartSno[]"]:checked').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                        $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', true);
                    }
                }).success(function (data) {
                    $('#totalGoodsCnt').html(numeral(data.cartCnt).format());
                    $('#totalGoodsPrice').html(gd_money_format(data.totalGoodsPrice));
                    $('#totalGoodsDcPrice').html(gd_money_format(data.totalGoodsDcPrice));
                    $('#totalMinusMember').html(gd_money_format(data.totalMemberDcPrice));
                    $('#totalCouponGoodsDcPrice').html(gd_money_format(data.totalCouponGoodsDcPrice));
                    $('#totalMyappDcPrice').html(gd_money_format(data.totalMyappDcPrice));
                    $('#totalSettlePrice').html(gd_money_format(data.totalSettlePrice));
                    $('#totalGoodsMileage').html(gd_money_format(data.totalMileage));
                    $('#deliveryChargeText').html('');
                    $('#totalDeliveryCharge').html(gd_money_format(data.totalDeliveryCharge));
                    $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', false);
                }).error(function (e) {
                    alert(e);
                    $('input[name="cartSno[]"], .gd_select_all_goods').prop('disabled', false);
                });
            }, 200);
        });

<?php }?>


        $('.js_option_layer').on('click', function(e){
            if($(this).data('coupon') == 'use') {
                alert("<?php echo __('쿠폰 적용 취소 후 옵션 변경 가능합니다.')?>")
                return false;
            } else {
                var params = {
                    type : 'cart',
                    sno: $(this).data('sno'),
                    goodsNo: $(this).data('goodsno')
                };

                $('#popupOption').modal({
                    remote: '../goods/layer_option.php',
                    cache: false,
                    params: params,
                    type : 'POST',
                    show: true
                });

            }
        });

		$('.js_possible_list_toggle').bind('click',function (e) {
			e.preventDefault();
			if($(this).closest('div').find('ul').is(':visible')){
				$(this).removeClass('active');
			}
			else {
				$(this).addClass('active');
			}

			$(this).closest('div').find('ul').toggle();
		})

    });



    /**
     * 선택 상품 처리
     */
    function gd_cart_process(mode) {
<?php if(empty($TPL_VAR["cartCnt"])===true){?>
        alert("<?php echo __('장바구니에 담겨있는 상품이 없습니다.')?>");
<?php }else{?>
        // 선택한 상품 개수
        var checkedCnt = $('input:checkbox[name="cartSno[]"]:checked').length;

        console.log(mode);
        console.log(checkedCnt);

        // 모드에 따른 메시지 및 처리
        if (mode == 'cartDelete') {
            msg = "<?php echo __('상품을 장바구니에서 삭제 하시겠습니까?')?>";
        } else if (mode == 'cartToWish') {
            msg = "<?php echo __('상품을 찜리스트에 담으시겠습니까?')?>";
        } else if (mode == 'orderSelect') {
            msg = "<?php echo __('상품만 주문합니다.')?>";

            var alertMsg = gd_cart_cnt_info();
            if (alertMsg) {
                alert(alertMsg);
                return false;
            }

            // 구매 불가 체크
            var orderPossible = 'y';
            var chkCartSno = []; // 쿠폰 유효성 체크시 사용
            $('input:checkbox[name="cartSno[]"]:checked').each(function() {
                if ($(this).data('possible') == 'n') {
                    orderPossible = 'n';
                } else {
                    chkCartSno.push($(this).val());
                }
            });
            if (orderPossible == 'n') {
                alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s장바구니 상품을 확인해 주세요!','\n')?>");
                return false;
            }

            if (parseInt(checkedCnt) == parseInt(<?php echo $TPL_VAR["cartCnt"]?>)) {
                // 쿠폰 사용기간 체크
                if ($('.btn_coupon_cancel').length > 0) {
                    var checkCouponType = false;
                    $.ajax({
                        method: "POST",
                        cache: false,
                        async: false,
                        url: "../order/cart_ps.php",
                        data: {mode: 'CheckCouponTypeArr', cartSno : chkCartSno },
                        success: function (data) {
                            checkCouponType = data.isSuccess;
                        },
                        error: function (e) {
                        }
                    });

                    if(checkCouponType) {
                        alert('사용기간이 만료된 쿠폰이 포함되어 있어 제외 후 진행합니다.');
                    }
                }

<?php if(gd_is_login()===false){?>
                $('#frmCart input[name=\'mode\']').val(mode);
                $('#frmCart').attr('method', 'post');
                $('#frmCart').attr('target', 'ifrmProcess');
                $('#frmCart').attr('action', '../order/cart_ps.php');
                $('#frmCart').submit();
<?php }else{?>
                location.href='../order/order.php';
<?php }?>
                return true;
            }
        } else {
            return false;
        }

        if (checkedCnt == 0) {
            alert("<?php echo __('선택하신 상품이 없습니다.')?>");
            return false;
        } else {
            if (confirm(__('선택하신 %i개', checkedCnt) + " " + msg) === true) {
                $('#frmCart input[name=\'mode\']').val(mode);
                $('#frmCart').attr('method', 'post');
                $('#frmCart').attr('target', 'ifrmProcess');
                $('#frmCart').attr('action', '../order/cart_ps.php');
                $('#frmCart').submit();
            }
            return true;
        }
<?php }?>
    }

    /**
     * 전체 상품 주문
     *
     */
    function gd_order_all() {
<?php if(empty($TPL_VAR["cartCnt"])===true){?>
        alert("<?php echo __('장바구니에 담겨있는 상품이 없습니다.')?>");
<?php }else{?>
        var alertMsg = gd_cart_cnt_info('all');
        if (alertMsg) {
            alert(alertMsg);
            return false;
        }
<?php if($TPL_VAR["orderPossible"]===true){?>
        // 쿠폰 유효성 체크시 사용
        var chkCartSno = [];
        $('#frmCart  input:checkbox[name="cartSno[]"]:checked').each(function() {
            chkCartSno.push($(this).val());
        });

        // 쿠폰 사용기간 체크
        if ($('.btn_coupon_cancel').length > 0) {
            var checkCouponType = false;
            $.ajax({
                method: "POST",
                cache: false,
                async: false,
                url: "../order/cart_ps.php",
                data: {mode: 'CheckCouponTypeArr', cartSno : chkCartSno },
                success: function (data) {
                    checkCouponType = data.isSuccess;
                },
                error: function (e) {
                }
            });

            if(checkCouponType) {
                alert('사용기간이 만료된 쿠폰이 포함되어 있어 제외 후 진행합니다.');
            }
        }

<?php if(gd_is_login()===false){?>
        $('input[name="cartSno[]"]').prop('checked', true);
        $('#frmCart input[name=\'mode\']').val('orderSelect');
        $('#frmCart').attr('method', 'post');
        $('#frmCart').attr('target', 'ifrmProcess');
        $('#frmCart').attr('action', '../order/cart_ps.php');
        $('#frmCart').submit();
<?php }else{?>
        location.href='../order/order.php';
<?php }?>
<?php }else{?>
<?php if($TPL_VAR["orderPossibleMessage"]){?>
        alert("<?php echo __($TPL_VAR["orderPossibleMessage"])?>");
<?php }else{?>
        alert("<?php echo __('구매 불가능한 상품이 존재합니다.%s장바구니 상품을 확인해 주세요!','\n')?>");
<?php }?>
<?php }?>
<?php }?>
    }

	function gd_cart_cnt_info(mode) {
		var target = 'input[name="cartSno[]"]';
		if (mode != 'all') target += ':checked';
		var stockCheckFl = false;
		var cartSno = [];

		var goodsCntData = [];
		$.each($(target), function(){
			var $goodsCnt = $(this);
			var goodsKey = $goodsCnt.data('goods-key');
			if (goodsCntData[goodsKey]) {
				stockCheckFl = true;
				goodsCntData[goodsKey] += $goodsCnt.data('default-goods-cnt');
			} else {
				cartSno[goodsKey] = [];
				goodsCntData[goodsKey] = $goodsCnt.data('default-goods-cnt');
			}
			cartSno[goodsKey].push($(this).val());
		});

		var msgByUnit = [];
		var msgByCnt = [];
		var msg;
		$.each(goodsCntData, function(index, value){
			if (_.isUndefined(value)) return true;

			var $data = $(target + '[data-goods-key="' + index + '"]');

			if ($data.data('fixed-sales') == 'goods') {
				if (value % $data.data('sales-unit') > 0) {
					msg = $data.data('goods-nm') + ' ' + $data.data('sales-unit') + __('개');
					msgByUnit['goods'] = msgByUnit['goods'] ? msgByUnit['goods'] + '\n' + msg : msg;
				}
			} else {
				$.each($data, function(){
					if ($(this).data('default-goods-cnt') % $(this).data('sales-unit') > 0) {
						msg = $(this).data('goods-nm') + '(' + $(this).data('option-nm') + ')' + ' ' + $(this).data('sales-unit') + __('개');
						msgByUnit['option'] = msgByUnit['option'] ? msgByUnit['option'] + '\n' + msg : msg;
					}
				});
			}
			if ($data.data('fixed-order-cnt') == 'goods') {
				if ($data.data('min-order-cnt') > 1 && $data.data('min-order-cnt') > value) {
					msg = __('%1$s 상품당 최소 %2$s개 이상', [$data.data('goods-nm'), $data.data('min-order-cnt')]);
					msgByCnt['goods'] = msgByCnt['goods'] ? msgByCnt['goods'] + '\n' + msg : msg;
				}
				if ($data.data('max-order-cnt') > 0 && $data.data('max-order-cnt') < value) {
					msg = __('%1$s 상품당 최대 %2$s개 이하', [$data.data('goods-nm'), $data.data('max-order-cnt')]);
					msgByCnt['goods'] = msgByCnt['goods'] ? msgByCnt['goods'] + '\n' + msg : msg;
				}
			} else if ($data.data('fixed-order-cnt') == 'id') {
				var params = {
					mode: 'check_memberOrderGoodsCount',
					goodsNo: $data.data('goods-no'),
				};
				$.ajax({
					method: "POST",
					async: false,
					cache: false,
					url: '../order/order_ps.php',
					data: params,
					success: function (data) {
						// error 메시지 예외 처리용
						if (!_.isUndefined(data.error) && data.error == 1) {
							alert(data.message);
							return false;
						}

						if ($data.data('min-order-cnt') > 1 && $data.data('min-order-cnt') > (value + data.count)) {
							msg = __('%1$s ID당 최소 %2$s개 이상', [$data.data('goods-nm'), $data.data('min-order-cnt')]);
							msgByCnt['id'] = msgByCnt['id'] ?  msgByCnt['id'] + '\n' + msg : msg;
						} else if ($data.data('min-order-cnt') > 1 && $data.data('min-order-cnt') > value) {
							msg = __('%1$s ID당 최소 %2$s개 이상', [$data.data('goods-nm'), $data.data('min-order-cnt')]);
							msgByCnt['id'] = msgByCnt['id'] ?  msgByCnt['id'] + '\n' + msg : msg;
						} else if ($data.data('max-order-cnt') > 0 && $data.data('max-order-cnt') < (value + data.count)) {
							msg = __('%1$s ID당 최대 %2$s개 이하', [$data.data('goods-nm'), $data.data('max-order-cnt')]);
							msgByCnt['id'] = msgByCnt['id'] ?  msgByCnt['id'] + '\n' + msg : msg;
						} else if ($data.data('max-order-cnt') > 0 && $data.data('max-order-cnt') < value) {
							msg = __('%1$s ID당 최대 %2$s개 이하', [$data.data('goods-nm'), $data.data('max-order-cnt')]);
							msgByCnt['id'] = msgByCnt['id'] ?  msgByCnt['id'] + '\n' + msg : msg;
						}
					},
					error: function (data) {
						alert(data.message);
					}
				});
			} else {
				$.each($data, function(){
					if ($(this).data('min-order-cnt') > 1 && $(this).data('min-order-cnt') > $(this).data('default-goods-cnt')) {
						msg = __('%1$s(%2$s) 옵션당 최소 %3$s개 이상', [$(this).data('goods-nm'), $(this).data('option-nm'), $(this).data('min-order-cnt')]);
						msgByCnt['option'] = msgByCnt['option'] ?  msgByCnt['option'] + '\n' + msg : msg;
					}
					if ($(this).data('max-order-cnt') > 0 && $(this).data('max-order-cnt') < $(this).data('default-goods-cnt')) {
						msg = __('%1$s(%2$s) 옵션당 최대 %3$s개 이하', [$(this).data('goods-nm'), $(this).data('option-nm'), $(this).data('max-order-cnt')]);
						msgByCnt['option'] = msgByCnt['option'] ?  msgByCnt['option'] + '\n' + msg : msg;
					}
				});
			}
		});

		var alertMsg = [];
		var msg;
		if (msgByUnit['option']) {
			msg = __('옵션기준');
			msg += '\n';
			msg += __('%1$s단위로 묶음 주문 상품입니다.', msgByUnit['goods']);
			alertMsg.push(msg);
		}
		if (msgByUnit['goods']) {
			msg = __('상품기준');
			msg += '\n';
			msg += __('%1$s단위로 묶음 주문 상품입니다.', msgByUnit['goods']);
			alertMsg.push(msg);
		}
		if (alertMsg.length) {
			return alertMsg.join('\n\n');
		}

		if (msgByCnt['option']) {
			if (msgByCnt['goods'] || msgByCnt['id']) {
				alertMsg.push(__('%1$s', msgByCnt['option']));
			} else {
				alertMsg.push(__('%1$s구매가능합니다.', msgByCnt['option']));
			}
		}
		if (msgByCnt['goods']) {
			if (msgByCnt['id']) {
				alertMsg.push(__('%1$s', msgByCnt['goods']));
			} else {
				alertMsg.push(__('%1$s구매가능합니다.', msgByCnt['goods']));
			}
		}
		if (msgByCnt['id']) {
			alertMsg.push(__('%1$s구매가능합니다.', msgByCnt['id']));
		}
		if (alertMsg.length) {
			return alertMsg.join('\n');
		}
		if(stockCheckFl) {
			var _cartSno = null;
			for(var i in cartSno) {
				if(cartSno[i].length > 1) {
					if(_cartSno) _cartSno += ','+cartSno[i].join(',');
					else _cartSno = cartSno[i].join(',');
				}
			}
			if(_cartSno) {
				$.ajax({
					method: "POST",
					cache: false,
					url: "../order/cart_ps.php",
					async: false,
					data: {'mode': 'cartSelectStock', 'sno': _cartSno},
					success: function (cnt) {
						if (cnt) {
							alertMsg.push(__('재고가 부족합니다. 현재 %s개의 재고가 남아 있습니다.', cnt));
						}
					},
					error: function (data) {
						alert(data.message);
					}
				});
			}
		}
		if (alertMsg.length) {
			return alertMsg.join('\n\n');
		}
		if(stockCheckFl) {
			var _cartSno = null;
			for(var i in cartSno) {
				if(cartSno[i].length > 1) {
					if(_cartSno) _cartSno += ','+cartSno[i].join(',');
					else _cartSno = cartSno[i].join(',');
				}
			}
			if(_cartSno) {
				$.ajax({
					method: "POST",
					cache: false,
					url: "../order/cart_ps.php",
					async: false,
					data: {'mode': 'cartSelectStock', 'sno': _cartSno},
					success: function (cnt) {
						if (cnt) {
							alertMsg.push(__('재고가 부족합니다. 현재 %s개의 재고가 남아 있습니다.', cnt));
						}
					},
					error: function (data) {
						alert(data.message);
					}
				});
			}
		}
		if (alertMsg.length) {
			return alertMsg.join('\n\n');
		}
	}
    //-->
</script>
<?php echo $TPL_VAR["fbCartScript"]?>

<?php $this->print_("footer",$TPL_SCP,1);?>