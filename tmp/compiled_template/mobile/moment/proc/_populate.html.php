<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/proc/_populate.html 000002840 */ 
if (is_array($TPL_VAR["data"])) $TPL_data_1=count($TPL_VAR["data"]); else if (is_object($TPL_VAR["data"]) && in_array("Countable", class_implements($TPL_VAR["data"]))) $TPL_data_1=$TPL_VAR["data"]->count();else $TPL_data_1=0;?>
<?php if($TPL_VAR["data"]){?>
<div class="_populate">
    <div class="rank_top_box">
        <div class="rank_visible_sec">
            <span class="hot_item_tit"><strong>HOT</strong> ITEMS</span>
            <div class="rolling_rank_list">
                <ul>
<?php if($TPL_data_1){$TPL_I1=-1;foreach($TPL_VAR["data"] as $TPL_V1){$TPL_I1++;?>
                    <li class="<?php if($TPL_I1<= 2){?>top<?php echo $TPL_I1+ 1?><?php }?> <?php if($TPL_VAR["config"]["template"]=='01'){?> hide<?php }?>"><em><?php echo $TPL_I1+ 1?></em><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>"><?php echo $TPL_V1["goodsNm"]?></a></li>
<?php }}?>
                </ul>
            </div>
            <!-- //rolling_rank_list -->
<?php if($TPL_VAR["config"]["useFl"]=='y'){?><span class="popular_more<?php echo $TPL_VAR["config"]["template"]?> btn_box"><a href="../goods/populate.php" class="popular_more_btn"><?php echo __('더보기')?></a></span><?php }?>
<?php if($TPL_VAR["config"]["template"]=='02'){?><button><?php echo __('열기')?>/<?php echo __('닫기')?></button><?php }?>
        </div>
        <div class="layer_rank_list"<?php if($TPL_VAR["config"]["template"]=='01'){?> style="display:block;"<?php }?>>
        <ul>
<?php if($TPL_data_1){$TPL_I1=-1;foreach($TPL_VAR["data"] as $TPL_V1){$TPL_I1++;?>
            <li<?php if($TPL_I1<= 2){?> class="top<?php echo $TPL_I1+ 1?>"<?php }?>><em><?php echo $TPL_I1+ 1?></em><a href="../goods/goods_view.php?goodsNo=<?php echo $TPL_V1["goodsNo"]?>"><?php echo $TPL_V1["goodsNm"]?></a></li>
<?php }}?>
        </ul>
    </div>
    <!-- //layer_rank_list -->
</div>
<!-- //rank_top_box -->
</div>
<!-- //_populate -->

<script src="/data/skin/mobile/moment/js/jquery/vticker/jquery.vticker.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $('.rank_visible_sec button').click(function(){
        if($('.layer_rank_list').hasClass('on')){
            $('.layer_rank_list').removeClass('on');
            $('.rank_top_box button').removeClass('on');
            $('.rolling_rank_list li').removeClass('hide');
        }else{
            $('.layer_rank_list').addClass('on');
            $('.rank_top_box button').addClass('on');
            $('.rolling_rank_list li').addClass('hide');
        }
    });
    $(function() {
        var $scroller = $('.rolling_rank_list');
        $scroller.vTicker();
    });
</script>
<?php }?>