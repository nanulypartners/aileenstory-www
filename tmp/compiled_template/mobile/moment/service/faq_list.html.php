<?php /* Template_ 2.2.7 2020/01/06 11:20:05 /www/aileen8919_godomall_com/data/skin/mobile/moment/service/faq_list.html 000005749 */ 
if (is_array($TPL_VAR["faqCode"])) $TPL_faqCode_1=count($TPL_VAR["faqCode"]); else if (is_object($TPL_VAR["faqCode"]) && in_array("Countable", class_implements($TPL_VAR["faqCode"]))) $TPL_faqCode_1=$TPL_VAR["faqCode"]->count();else $TPL_faqCode_1=0;?>
<?php $this->print_("header",$TPL_SCP,1);?>

<div class="faq_list">
	<div id="boardlist" class="boardlist">
		<form name="frmList" id="frmList" action="faq_list.php" method="get">
			<input type="hidden" name="searchField" value="all">
			<input type="hidden" name="isBest" value="<?php echo $TPL_VAR["req"]["isBest"]?>">
			<input type="hidden" name="category" value="<?php echo $TPL_VAR["req"]["category"]?>">
			<div class="board_search">
				<div class="search_input"><input type="search" name="searchWord" value="<?php echo $TPL_VAR["req"]["searchWord"]?>" placeholder="<?php echo __('검색 단어를 입력해 주세요.')?>"/></div>
				<div class="search_btn"><button type="submit" class="faq_search_btn"><?php echo __('검 색')?></button></div>
			</div>
		</form>
		<div class="sel_box">
			<div class="inp_sel">
				<select name="category">
<?php if($TPL_faqCode_1){foreach($TPL_VAR["faqCode"] as $TPL_K1=>$TPL_V1){?><option value="<?php echo $TPL_K1?>" <?php if($TPL_VAR["req"]["category"]==$TPL_K1){?>selected<?php }?>><?php echo __($TPL_V1)?></option><?php }}?>
				</select>
			</div>
		</div>
<?php if($TPL_VAR["req"]["isBest"]=='y'){?>
		<h3>BEST FAQ</h3>
<?php }?>
		<div class="board_faq_box">
			<ul class="board_faq">
<?php if($TPL_VAR["faqList"]["data"]){?>
<?php if((is_array($TPL_R1=$TPL_VAR["faqList"]["data"])&&!empty($TPL_R1)) || (is_object($TPL_R1) && in_array("Countable", class_implements($TPL_R1)) && $TPL_R1->count() > 0)) {foreach($TPL_R1 as $TPL_V1){?>
				<li class="data_row"><div class="faq_subject js_link_subject" data-sno="<?php echo $TPL_V1["sno"]?>">[<?php echo $TPL_V1["categoryNm"]?>] <?php echo $TPL_V1["subject"]?></div></li>
<?php }}?>
<?php }else{?>
				<li class="data_row"><div class="no_data"><?php echo __('검색결과가 없습니다.')?></div></li>
<?php }?>
			</ul>
		</div>
<?php if($TPL_VAR["faqList"]["totalCount"]> 10){?>
		<div class="btn_box">
			<button type="button" class="faq_more_btn js_more_btn" data-page="1"><?php echo __('더보기')?></button>
		</div>
<?php }?>
	</div>
</div>
<?php $this->print_("footer",$TPL_SCP,1);?>

<script>
    $(document).ready(function () {
        var totalCount = <?php echo $TPL_VAR["faqList"]["totalCount"]?>;
        var loaderImg = "<p class='faq_loader'><img src='<?php echo PATH_MOBILE_SKIN?>js/slider/slick/ajax-loader.gif' class='js_ajax_loader'></p>";
        $('.js_more_btn').bind('click', function () {
            var thisObj = $(this);
            var currentPage = thisObj.data('page') + 1;
            params = $("#frmList").serialize() + '&page=' + currentPage;
            $('.board_faq').append(loaderImg);
            $.ajax({
                method: "get",
                url: "./faq_list.php",
                data: params,
                async: true,
                dataType: 'json'
            }).success(function (data) {
                if (data == '') {
                    alert("<?php echo __('더 이상 FAQ가 없습니다.')?>");
                    return;
                }
                thisObj.data('page', currentPage);

                $.each(data, function (key, val) {
                    var faqRow = _.template($('#faq_row').html())(val);
                    $('.board_faq').append(faqRow);
                })
				$('.js_link_subject').css('cursor','pointer');
                $('.js_ajax_loader').remove();

                if (parseInt(totalCount / 10) < currentPage) {
                    $('.js_more_btn').hide();
                }
            }).error(function (e) {
                alert(e.responseText);
            });
        });


		$('body').on('click', '.js_link_subject', function () {

			var thisObj = $(this);
			var sno = thisObj.data('sno');


			if (thisObj.closest('.data_row').find('.js_contents').length > 0) {
				if(thisObj.closest('.data_row').find('.js_contents').is(':visible')){
					thisObj.closest('.data_row').find('.js_contents').hide();
					thisObj.removeClass('on');
				}
				else {
					thisObj.closest('.data_row').find('.js_contents').show();
					thisObj.addClass('on');
				}

				return;
			}
			else {
				thisObj.after(loaderImg);
				thisObj.addClass('on');
			}
			$.ajax({
				method: "get",
				url: "./faq_list.php",
				data: {mode: 'getAnswer', sno: sno},
				async: true,
				dataType: 'json'
			}).success(function (data) {
				var faqContents = _.template($('#faq_contents').html())(data);
				thisObj.after(faqContents);
				$('.js-smart-img').css('max-width', '100%');
				$('.js_ajax_loader').remove();
			}).error(function (e) {
				alert(e.responseText);
			});

		});

		$('.js_link_subject').css('cursor','pointer');

        $('select[name=category]').bind('change', function () {
            self.location.href='faq_list.php?category='+$(this).val();
        });
    });
</script>
<script type="text/template" id="faq_contents">
	<div class="faq_row_box js_contents">
		<dl>
			<dt><span class="icon_q">Q</span><%=questionContents%></dt>
			<dd><span class="icon_a">A</span><%=answerContents%></dd>
		</dl>
	</div>
</script>

<script type="text/template" id="faq_row">
	<li class="data_row">
		<div class="faq_subject js_link_subject" data-sno="<%=sno%>">[<%=categoryNm%>]<%=subject%></div>
	</li>
</script>