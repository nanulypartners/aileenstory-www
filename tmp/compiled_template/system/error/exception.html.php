<?php /* Template_ 2.2.7 2018/05/08 13:17:45 /www/system/error/exception.html 000006459 */ ?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>요청하신 작업을 처리할 수 없습니다.</title>
    <style type="text/css">
        .blackout {
            width: 578px;
            margin: 12% auto 0;
            padding: 210px 0 90px;
            background: url('/admin/gd_share/img/icon_error.png') no-repeat center 69px;
            text-align: center;
        }

        .blackout > strong {
            color: #222222;
            font-size: 28px;
        }

        .blackout > strong > em {
            font-style: normal;
            color: #EC1C23;
        }

        * {
            font-family: Malgun Gothic, "맑은 고딕", AppleGothic, Dotum, "돋움", sans-serif;
        }

        .blackout p {
            padding: 20px 0 0;
            color: #222222;
            font-size: 13px;
            line-height: 1.5;
            color: #666666;
        }

        .blackout-admin {
            background-color: #f3f6f3;
            padding: 20px;
        }

        .blackout-admin h1 {
            margin-bottom: 40px;
        }

        .blackout-admin hr {
            margin: 10px auto 20px;
            background-color: #f9f9f9;
        }

        .blackout-admin pre {
            background-color: #ffe;
        }

        .blackout-admin .code {
            font-size: 0.8em;
            line-height: 1.5em;
            padding: 10px;
            background-color: #ffe;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        .blackout-admin .code * {
            background-color: #ffe;
        }

        .blackout-admin .code table {
            width: 100%;
            table-layout: fixed;
        }

        .blackout-admin .code table td {
            white-space: pre;
            text-overflow: ellipsis;
            overflow: hidden;
            background-color: #ffe;
        }

        .blackout-admin .code table td.current {
            background-color: #fee000;
        }

        /*20160510 윤태건 404error*/
        * {
            margin: 0;
            padding: 0;
        }

        body {
            -webkit-text-size-adjust: none;
        }

        .c-point {
            color: #EC1C23;
        }

        .submitbtn .skinbtn {
            width: 130px;
        }

        .skinbtn {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            height: 40px;
            padding: 0 5px;
            text-align: center;
            vertical-align: top;
            box-sizing: border-box;
            cursor: pointer;
            font-size: 14px;

            border: 1px solid #B1B1B1;
            background: #FFFFFF;
            color: #777777;
        }

        .btn-m2 {
            height: 44px;
        }

        .submitbtn {
            margin: 30px 0 0;
        }

        @media (max-width: 768px) {
            .blackout,
            .blackout-admin {
                width: auto;
                padding: 190px 0 90px;
                /*background-size: 97px 84px;*/
            }

            .blackout > strong {
                font-size: 20px;
            }

            .blackout p {
                padding: 10px 0 0;
                font-size: 14px;
                line-height: 18px;
                letter-spacing: -0.05em;
            }

            .submitbtn {
                margin: 20px 0 0;
            }
        }
        /*20160510 윤태건 404error // */
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="/admin/gd_share/script/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoBack').click(function(e){
                window.history.back();
            });
            $('#btnGoHome').click(function(e){
                location.replace('<?php echo $TPL_VAR["homeUri"]?>');
            });
        });
    </script>
</head>
<body>
    <div class="blackout">
        <strong>요청하신 작업을 <em>처리할 수 없습니다.</em></strong>
        <p>
            오류 관련 문의는 <strong><?php echo $TPL_VAR["centerPhone"]?></strong>로 연락하시기 바랍니다.<br>
            서비스 이용에 불편을 드려서 죄송합니다.
        </p>
        <div class="submitbtn">
            <button type="button" class="skinbtn default btn-m2" id="btnGoBack"><b>뒤로가기</b></button>
            <button type="button" class="skinbtn default btn-m2" id="btnGoHome"><b>홈 바로가기</b></button>
        </div>
    </div>

<?php if($TPL_VAR["isPro"]===true||$TPL_VAR["isGodoIp"]===true){?>
    <div class="blackout-admin">
        <p>
            <small>
                <font color="red">
<?php if($TPL_VAR["isGodoIp"]===true){?>
                    하단의 오류내역은 고도내부 IP대역에서만 출력됩니다.
<?php }else{?>
<?php if($TPL_VAR["isPro"]===true){?>
                    관리자 > 기본설정 > 운영자관리에서 디버그 권한을 설정한 경우만 출력됩니다.
<?php }?>
<?php }?>
                </font>
            </small>
        </p>
        <h1><?php echo $TPL_VAR["exceptionName"]?></h1>

        <h2>Message</h2>
        <p><strong>[<?php echo $TPL_VAR["exceptionCode"]?>]</strong> <?php echo $TPL_VAR["exceptionMessage"]?></p>

<?php if(isset($TPL_VAR["databaseQuery"])&&$TPL_VAR["databaseQuery"]!=''){?>
        <hr>
        <a name="query"></a>
        <h2>Query</h2>
        <?php echo $TPL_VAR["databaseQuery"]?>

<?php }?>

        <hr>
        <a name="time"></a>
        <h2>Time</h2>
        <p><?php echo $TPL_VAR["exceptionTime"]?></p>

        <hr>
        <a name="file"></a>
        <h2>File</h2>
        <p><?php echo $TPL_VAR["exceptionFile"]?></p>
        <div class="code"><?php echo $TPL_VAR["exceptionSourceCode"]?></div>

        <hr>
        <a name="trace"></a>
        <h2>Trace</h2>
        <div class="code"><?php echo $TPL_VAR["exceptionTrace"]?></div>
    </div>
<?php }?>
</body>
</html>